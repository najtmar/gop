#!/bin/bash

SRC_PATH="$(dirname $0)"
DEST_DIR="${SRC_PATH}/final"
INSTALL_DIR="${SRC_PATH}/../../assets/game_rules"

for file in ${SRC_PATH}/*.svg; do
    filename=$(basename $file .svg)
    inkscape ${file} -e ${DEST_DIR}/${filename}.png
done

for file in ${DEST_DIR}/*.png; do
    filename=$(basename $file .png)
    pngtopnm ${DEST_DIR}/${filename}.png | pnmtopng > ${INSTALL_DIR}/${filename}.png
done