#!/bin/bash

SRC_DIR=$(dirname $0)
DEST_DIR=${SRC_DIR}/../../merged

function convert {
    fileid=$1

    rm ${SRC_DIR}/${fileid}_?.pnm 2>/dev/null
    for file in ${SRC_DIR}/${fileid}_?.png; do
	pnmfile=$(basename ${file} .png).pnm
	pngtopnm ${file} > ${SRC_DIR}/${pnmfile}
    done

    pnmcat -topbottom ${SRC_DIR}/${fileid}_?.pnm | pnmtopng > ${DEST_DIR}/${fileid}.png
    rm ${SRC_DIR}/${fileid}_?.pnm 2>/dev/null
}

for file in ${SRC_DIR}/*.png; do
    filebase=$(basename $file .png)
    fileid="${filebase:0:-2}"
    convert "$fileid"
done
