#!/bin/bash

LEFT_START=54
TOP_START=100
RIGHT_END=455
BOTTOM_END=745

SRC_PATH=$(dirname $0)
DEST_DIR=${SRC_PATH}/../../cut/vertical

for file in ${SRC_PATH}/*.png; do
    filename=$(basename $file)
    pngtopnm $file |\
 pnmcut -left ${LEFT_START} -top ${TOP_START}\
 -right ${RIGHT_END} -bottom ${BOTTOM_END} |\
 pnmtopng > ${DEST_DIR}/${filename}
done
