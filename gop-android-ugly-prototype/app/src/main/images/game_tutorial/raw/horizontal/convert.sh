#!/bin/bash

LEFT_START=72
TOP_START=81
RIGHT_END=743
BOTTOM_END=462

SRC_PATH=$(dirname $0)
DEST_DIR=${SRC_PATH}/../../cut/horizontal

for file in ${SRC_PATH}/*.png; do
    filename=$(basename $file)
    pngtopnm $file |\
 pnmcut -left ${LEFT_START} -top ${TOP_START}\
 -right ${RIGHT_END} -bottom ${BOTTOM_END} |\
 pnmtopng > ${DEST_DIR}/${filename}
done
