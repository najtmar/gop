#!/bin/bash

SRC_PATH=$(dirname $0)

for dir in ${SRC_PATH}/raw/vertical ${SRC_PATH}/raw/horizontal ${SRC_PATH}/cut/vertical ${SRC_PATH}/cut/horizontal ${SRC_PATH}/merged; do
    echo "Running ${dir}/convert.sh"
    ${dir}/convert.sh
done
