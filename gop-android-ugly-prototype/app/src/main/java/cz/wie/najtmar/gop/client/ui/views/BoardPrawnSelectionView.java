package cz.wie.najtmar.gop.client.ui.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import cz.wie.najtmar.gop.client.ui.AndroidEngine;
import cz.wie.najtmar.gop.client.ui.GoP;
import cz.wie.najtmar.gop.client.ui.InputManager;
import cz.wie.najtmar.gop.client.ui.R;
import cz.wie.najtmar.gop.client.view.PrawnView;
import cz.wie.najtmar.gop.common.Point2D;

/**
 * Created by najtmar on 27.10.16.
 */

public class BoardPrawnSelectionView extends View {

  /**
   * Central model object.
   */
  private GoP mGoP;

  /**
   * Shared instance of class AndroidEngine, which may call back on the fragment.
   */
  private AndroidEngine mAndroidEngine;

  // Prawn selection parameters.
  private int mPrawnSelectionWidth;
  private int mPrawnSelectionHeight;
  private float mPrawnBoxSide;
  private float mCityBoxMargin;
  private int mCityColor;
  private int mCityAccentColor;

  private boolean mIsCity;

  /**
   * Used to react to the gestures.
   */
  private final GestureDetector mGestureDetector;

  /**
   * Given a dimension id, returns the value of the dimension.
   * @param dimensionId the dimension id
   * @param context the Context to read the dimensions from
   * @return the value of the dimension with a given dimensionId
   */
  private float getDimension(int dimensionId, Context context) {
    return context.getResources().getDimension(dimensionId);
  }

  public BoardPrawnSelectionView(Context context, AttributeSet attrs) {
    super(context, attrs);
    if (isInEditMode()) {
      mGestureDetector = null;
      return;
    }
    mGoP = GoP.get(context);

    final GestureListener gestureListener = new GestureListener();
    mGestureDetector = new GestureDetector(context, gestureListener, null, true);

    mAndroidEngine = (AndroidEngine) mGoP.getInjector().provideOutputEngine();

    // Read dimensions.
    mPrawnBoxSide = getDimension(R.dimen.prawn_box_side, context);
    final float prawnSelectionBoxWidth = getDimension(R.dimen.prawn_selection_box_width, context);
    final float prawnSelectionBoxHeight = getDimension(R.dimen.prawn_selection_box_height, context);
    mPrawnSelectionWidth = (int) Math.floor(prawnSelectionBoxWidth / mPrawnBoxSide);
    mPrawnSelectionHeight = (int) Math.floor(prawnSelectionBoxHeight / mPrawnBoxSide);

    mCityBoxMargin = context.getResources().getDimension(R.dimen.city_element_margin);
    mCityColor = context.getResources().getColor(R.color.colorCity);
    mCityAccentColor = context.getResources().getColor(R.color.colorCityAccent);

    // Read styleable attributes.
    TypedArray attrsArr =
        context.obtainStyledAttributes(attrs, R.styleable.BoardPrawnSelectionView);
    mIsCity = attrsArr.getBoolean(R.styleable.BoardPrawnSelectionView_city, false);
  }

  @Override
  public boolean onTouchEvent(MotionEvent event) {
    boolean result = false;
    if ((mIsCity && mAndroidEngine.isCityViewOn())
        || (!mIsCity && mAndroidEngine.isPrawnSelectionViewOn())) {
      result = mGestureDetector.onTouchEvent(event);
    }
    return result;
  }

  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);

    if (isInEditMode()) return;
    final PrawnView[][] prawnSelection;
    if (mIsCity) {
      prawnSelection = mAndroidEngine.getCityViewPrawnSelection();
    } else {
      prawnSelection = mAndroidEngine.getPrawnSelection();
    }
    final Paint paint = new Paint();
    paint.setColor(mCityColor);
    final Paint paintAccent = new Paint();
    paintAccent.setColor(mCityAccentColor);
    for (int j = 0; j < mPrawnSelectionHeight; ++j) {
      for (int i = 0; i < mPrawnSelectionWidth; ++i) {
        final Paint paintUsed;
        if (i == mAndroidEngine.getPrawnSelectedForRepairX()
            && j == mAndroidEngine.getPrawnSelectedForRepairY()) {
          paintUsed = paintAccent;
        } else {
          paintUsed = paint;
        }
        canvas.drawRect(i * mPrawnBoxSide, j * mPrawnBoxSide,
            (i + 1) * mPrawnBoxSide - mCityBoxMargin,
            (j + 1) * mPrawnBoxSide - mCityBoxMargin, paintUsed);
        final PrawnView prawn = prawnSelection[i][j];
        if (prawn != null) {
          double x = (i + 0.5) * mPrawnBoxSide / mAndroidEngine.getAndroidMagnifyX()
              - AndroidEngine.TRANSLATE_X;
          double y = (j + 0.5) * mPrawnBoxSide / mAndroidEngine.getAndroidMagnifyY()
              - AndroidEngine.TRANSLATE_Y;
          double radius = mPrawnBoxSide / 2 * 0.6;
          if (prawn.getType().equals(PrawnView.Type.WARRIOR)) {
            mAndroidEngine.drawPlayerWarriorShape(x, y, prawn.getPlayer().getIndex(), radius,
                getResources().getDimension(R.dimen.prawn_border_width),
                prawn.getWarriorDirection(), canvas);
          } else {
            mAndroidEngine.drawPlayerCircle(x, y, radius,
                getResources().getDimension(R.dimen.prawn_border_width),
                prawn.getPlayer().getIndex(), canvas);
          }
          int prawnEnergy = (int) Math.ceil(prawn.getEnergy() * 10);
          mAndroidEngine.drawPlayerCenteredText(x, y, "" + prawnEnergy, radius,
              prawn.getPlayer().getIndex(), false, canvas);
        }
      }
    }
  }

  private class GestureListener extends GestureDetector.SimpleOnGestureListener {
    @Override
    public boolean onDown(MotionEvent event) {
      return true;
    }

    @Override
    public void onShowPress(MotionEvent e) {
      if (e.getX() < mPrawnSelectionWidth * Math.floor(mPrawnBoxSide)
          && e.getY() < mPrawnSelectionHeight * Math.floor(mPrawnBoxSide)) {
        if (mIsCity) {
          if (mAndroidEngine.getState().equals(InputManager.State.CITY)) {
            mAndroidEngine.selectCityPrawnSelectionPoint(new Point2D.Double(e.getX(), e.getY()));
          } else if (mAndroidEngine.getState().equals(InputManager.State.PRAWN_REPAIR)) {
            mAndroidEngine.selectPrawnRepairSelectionPoint(new Point2D.Double(e.getX(), e.getY()));
          }
        } else {
          mAndroidEngine.selectPrawnSelectionPoint(new Point2D.Double(e.getX(), e.getY()));
        }
      }
    }
  }
}