package cz.wie.najtmar.gop.client.ui.fragments;

import android.view.View;
import android.widget.Button;

import cz.wie.najtmar.gop.client.ui.R;

/**
 * Created by najtmar on 04.11.16.
 */

public class ControlPanelBoardPrawnSelectionFragment  extends ControlPanelBoardFragment {

  // UI elements.
  private Button mCloseButton;

  @Override
  protected void initializeControls(View view) {
    mCloseButton = (Button) view.findViewById(R.id.board_prawn_selection_close);
    mCloseButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getAndroidEngine().closePrawnSelection();
      }
    });
  }

  @Override
  protected int getControlPanelLayout() {
    return R.layout.control_panel_board_prawn_selection;
  }

  @Override
  public void onControlPanelBoardFragmentResume() {
    getAndroidEngine().setControlPanelBoardPrawnSelectionFragment(this);
  }

  @Override
  public void onControlPanelBoardFragmentDestroy() {
  }

  @Override
  public void updateState() {
  }

  public void finishBoardPrawnSelectionActivity() {
    getActivity().finish();
    getAndroidEngine().setControlPanelBoardPrawnSelectionFragment(null);
  }
}
