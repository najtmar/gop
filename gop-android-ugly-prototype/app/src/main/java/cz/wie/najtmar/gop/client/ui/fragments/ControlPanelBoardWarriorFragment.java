package cz.wie.najtmar.gop.client.ui.fragments;

import android.view.View;

import cz.wie.najtmar.gop.client.ui.R;

/**
 * Created by najtmar on 21.10.16.
 */

public class ControlPanelBoardWarriorFragment extends ControlPanelBoardPrawnFragment {

  @Override
  protected void initializePrawnSpecificControls(View view) {}

  @Override
  protected int getControlPanelLayout() {
    return R.layout.control_panel_board_warrior;
  }
}
