package cz.wie.najtmar.gop.client.ui.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import cz.wie.najtmar.gop.client.ui.AndroidEngine;
import cz.wie.najtmar.gop.client.ui.GoP;
import cz.wie.najtmar.gop.client.ui.R;

/**
 * Created by najtmar on 15.12.16.
 */

public class ForceQuitGameInfoDialogFragment extends RobustDialogFragment {

  /**
   * Shared instance of class AndroidEngine, which may call back on the fragment.
   */
  private AndroidEngine mAndroidEngine;

  public static ForceQuitGameInfoDialogFragment newInstance() {
    ForceQuitGameInfoDialogFragment fragment = new ForceQuitGameInfoDialogFragment();

    return fragment;
  }

  @NonNull
  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    return new AlertDialog.Builder(getActivity())
        .setTitle(R.string.force_quit_game_info_prompt_title)
        .setIcon(R.drawable.ic_error_black_24dp)
        .setMessage(R.string.force_quit_game_info_prompt_message)
        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            new Thread(new Runnable() {
              @Override
              public void run() {
                mAndroidEngine.forceQuitGame();
              }
            }).start();
          }
        })
        .create();
  }

  public ForceQuitGameInfoDialogFragment() {
    final GoP gop = GoP.get(getContext());
    mAndroidEngine = (AndroidEngine) gop.getInjector().provideOutputEngine();
  }

  @Override
  protected void fallback() {
    new Thread(new Runnable() {
      @Override
      public void run() {
        mAndroidEngine.forceQuitGame();
      }
    }).start();
  }
}
