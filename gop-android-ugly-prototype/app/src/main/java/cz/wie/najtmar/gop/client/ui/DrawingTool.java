package cz.wie.najtmar.gop.client.ui;

import android.graphics.Paint;
import android.graphics.PathEffect;
import android.graphics.Typeface;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import cz.wie.najtmar.gop.client.view.RoadSectionView;
import cz.wie.najtmar.gop.common.Point2D;

/**
 * Created by najtmar on 18.11.16.
 */

public class DrawingTool {

  private List<DrawBottomLine> mDrawBottomLine = new LinkedList<>();
  private List<DrawHighlightLine> mDrawHighlightLine = new LinkedList<>();
  private List<DrawCircle> mDrawCircle = new LinkedList<>();
  private List<DrawPlayerCircle> mDrawPlayerCircle = new LinkedList<>();
  private List<DrawOctangle> mDrawOctangle = new LinkedList<>();
  private List<DrawPlayerOctangle> mDrawPlayerOctangle = new LinkedList<>();
  private List<DrawStar> mDrawStar = new LinkedList<>();
  private List<DrawEmptyCircle> mDrawEmptyCircle = new LinkedList<>();
  private List<DrawPlayerCenteredText> mDrawPlayerCenteredText = new LinkedList<>();
  private List<DrawCenteredText> mDrawCenteredText = new LinkedList<>();
  private List<DrawText> mDrawText = new LinkedList<>();
  private List<DrawRoadSectionHalfView> mDrawRoadSectionHalfView = new LinkedList<>();
  private List<DrawWarriorShape> mDrawWarriorShape = new LinkedList<>();
  private List<DrawEmptyWarriorShape> mDrawEmptyWarriorShape = new LinkedList<>();
  private List<DrawPlayerWarriorShape> mDrawPlayerWarriorShape = new LinkedList<>();
  private List<DrawCityDescription> mDrawCityDescription = new LinkedList<>();

  private Map<Object, DrawPlayerCenteredText> mObjectDescriptionMap = new HashMap<>();

  private boolean mInvalidated = true;

  public static class DrawBottomLine {

    public double startX;
    public double startY;
    public double stopX;
    public double stopY;
    public Paint paint;

    public DrawBottomLine(double startX, double startY, double stopX, double stopY,
                          @NonNull Paint paint) {
      this.startX = startX;
      this.startY = startY;
      this.stopX = stopX;
      this.stopY = stopY;
      this.paint = paint;
    }
  }

  public static class DrawHighlightLine {
    public double startX;
    public double startY;
    public double stopX;
    public double stopY;
    public Paint paint;

    public DrawHighlightLine(double startX, double startY, double stopX, double stopY,
                             @NonNull Paint paint) {
      this.startX = startX;
      this.startY = startY;
      this.stopX = stopX;
      this.stopY = stopY;
      this.paint = paint;
    }
  }

  public static class DrawCircle {
    public double x;
    public double y;
    public double radius;
    public int fillColor;
    public int strokeColor;
    public boolean applyGradient;
    public double strokeWidth;

    public DrawCircle(double x, double y, double radius, int fillColor, int strokeColor,
                      boolean applyGradient, double strokeWidth) {
      this.x = x;
      this.y = y;
      this.radius = radius;
      this.fillColor = fillColor;
      this.strokeColor = strokeColor;
      this.applyGradient = applyGradient;
      this.strokeWidth = strokeWidth;
    }
  }

  public static class DrawPlayerCircle {
    public double x;
    public double y;
    public double radius;
    public double strokeWidth;
    public int playerIndex;

    public DrawPlayerCircle(double x, double y, double radius, double strokeWidth,
                            int playerIndex) {
      this.x = x;
      this.y = y;
      this.radius = radius;
      this.strokeWidth = strokeWidth;
      this.playerIndex = playerIndex;
    }
  }

  public static class DrawOctangle {
    public double x;
    public double y;
    public double radius;
    public int fillColor;
    public int strokeColor;
    public boolean applyGradient;
    public double strokeWidth;

    public DrawOctangle(double x, double y, double radius, int fillColor, int strokeColor,
                        boolean applyGradient, double strokeWidth) {
      this.x = x;
      this.y = y;
      this.radius = radius;
      this.fillColor = fillColor;
      this.strokeColor = strokeColor;
      this.applyGradient = applyGradient;
      this.strokeWidth = strokeWidth;
    }
  }

  public static class DrawPlayerOctangle {
    public double x;
    public double y;
    public double radius;
    public double strokeWidth;
    public int playerIndex;

    public DrawPlayerOctangle(double x, double y, double radius, double strokeWidth,
                              int playerIndex) {
      this.x = x;
      this.y = y;
      this.radius = radius;
      this.strokeWidth = strokeWidth;
      this.playerIndex = playerIndex;
    }
  }

  public static class DrawStar {
    public double x;
    public double y;
    public double radius;
    public int fillColor;
    public int strokeColor;
    public double strokeWidth;

    public DrawStar(double x, double y, double radius, int fillColor, int strokeColor,
                    double strokeWidth) {
      this.x = x;
      this.y = y;
      this.radius = radius;
      this.fillColor = fillColor;
      this.strokeColor = strokeColor;
      this.strokeWidth = strokeWidth;
    }
  }

  public static class DrawEmptyCircle {
    public double x;
    public double y;
    public double radius;
    public int color;
    public double strokeWidth;

    public DrawEmptyCircle(double x, double y, double radius, int color, double strokeWidth) {
      this.x = x;
      this.y = y;
      this.radius = radius;
      this.color = color;
      this.strokeWidth = strokeWidth;
    }
  }

  public static class DrawPlayerCenteredText {
    public double x;
    public double y;
    public String text;
    public double size;
    public int playerIndex;
    public boolean primaryColor;

    public DrawPlayerCenteredText(double x, double y, String text, double size, int playerIndex,
                                       boolean primaryColor) {
      this.x = x;
      this.y = y;
      this.text = text;
      this.size = size;
      this.playerIndex = playerIndex;
      this.primaryColor = primaryColor;
    }
  }

  public static class DrawCenteredText {
    public double x;
    public double y;
    public String text;
    public double size;
    public int color;

    public DrawCenteredText(double x, double y, String text, double size, int color) {
      this.x = x;
      this.y = y;
      this.text = text;
      this.size = size;
      this.color = color;
    }
  }

  public static class DrawText {
    public double x;
    public double y;
    public String text;
    public double size;
    public Paint.Align align;
    public Typeface typeface;
    public int color;

    public DrawText(double x, double y, String text, double size, Paint.Align align,
                         Typeface typeface, int color) {
      this.x = x;
      this.y = y;
      this.text = text;
      this.size = size;
      this.align = align;
      this.typeface = typeface;
      this.color = color;
    }
  }

  public static class DrawRoadSectionHalfView {
    public RoadSectionView roadSection;
    public RoadSectionView.Direction end;
    public int color;
    public float strokeWidth;
    public PathEffect pathEffect;

    public DrawRoadSectionHalfView(RoadSectionView roadSection, RoadSectionView.Direction end,
                                   int color, float strokeWidth, PathEffect pathEffect) {
      this.roadSection = roadSection;
      this.end = end;
      this.color = color;
      this.strokeWidth = strokeWidth;
      this.pathEffect = pathEffect;
    }
  }

  public static class DrawWarriorShape {
    public double x;
    public double y;
    public double radius;
    public int fillColor;
    public int strokeColor;
    public boolean applyGradient;
    public double strokeWidth;
    public double angle;

    public DrawWarriorShape(double x, double y, double radius, int fillColor, int strokeColor,
                            boolean applyGradient, double strokeWidth, double angle) {
      this.x = x;
      this.y = y;
      this.radius = radius;
      this.fillColor = fillColor;
      this.strokeColor = strokeColor;
      this.applyGradient = applyGradient;
      this.strokeWidth = strokeWidth;
      this.angle = angle;
    }
  }

  public static class DrawEmptyWarriorShape {
    public double x;
    public double y;
    public double radius;
    public int color;
    public double strokeWidth;
    public double angle;

    public DrawEmptyWarriorShape(double x, double y, double radius, int color, double strokeWidth,
                                 double angle) {
      this.x = x;
      this.y = y;
      this.radius = radius;
      this.color = color;
      this.strokeWidth = strokeWidth;
      this.angle = angle;
    }
  }

  public static class DrawPlayerWarriorShape {
    public double x;
    public double y;
    public int playerIndex;
    public double radius;
    public double strokeWidth;
    public double angle;

    public DrawPlayerWarriorShape(double x, double y, int playerIndex, double radius,
                                  double strokeWidth, double angle) {
      this.x = x;
      this.y = y;
      this.playerIndex = playerIndex;
      this.radius = radius;
      this.strokeWidth = strokeWidth;
      this.angle = angle;
    }
  }

  public static class DrawCityDescription {
    public Point2D.Double coord;
    public double radius;
    public String cityName;

    public DrawCityDescription(Point2D.Double coord, double radius, String cityName) {
      this.coord = coord;
      this.radius = radius;
      this.cityName = cityName;
    }
  }

  public List<DrawBottomLine> getDrawBottomLine() {
    mInvalidated = true;
    return mDrawBottomLine;
  }

  public List<DrawCenteredText> getDrawCenteredText() {
    mInvalidated = true;
    return mDrawCenteredText;
  }

  public List<DrawCircle> getDrawCircle() {
    mInvalidated = true;
    return mDrawCircle;
  }

  public List<DrawCityDescription> getDrawCityDescription() {
    mInvalidated = true;
    return mDrawCityDescription;
  }

  public List<DrawEmptyCircle> getDrawEmptyCircle() {
    mInvalidated = true;
    return mDrawEmptyCircle;
  }

  public List<DrawHighlightLine> getDrawHighlightLine() {
    mInvalidated = true;
    return mDrawHighlightLine;
  }

  public List<DrawOctangle> getDrawOctangle() {
    mInvalidated = true;
    return mDrawOctangle;
  }

  public List<DrawPlayerCenteredText> getDrawPlayerCenteredText() {
    mInvalidated = true;
    final List<DrawPlayerCenteredText> result = new LinkedList<>(mDrawPlayerCenteredText);
    result.removeAll(mObjectDescriptionMap.values());
    return result;
  }

  public List<DrawPlayerCircle> getDrawPlayerCircle() {
    mInvalidated = true;
    return mDrawPlayerCircle;
  }

  public List<DrawPlayerOctangle> getDrawPlayerOctangle() {
    mInvalidated = true;
    return mDrawPlayerOctangle;
  }

  public List<DrawPlayerWarriorShape> getDrawPlayerWarriorShape() {
    mInvalidated = true;
    return mDrawPlayerWarriorShape;
  }

  public List<DrawRoadSectionHalfView> getDrawRoadSectionHalfView() {
    mInvalidated = true;
    return mDrawRoadSectionHalfView;
  }

  public List<DrawStar> getDrawStar() {
    mInvalidated = true;
    return mDrawStar;
  }

  public List<DrawText> getDrawText() {
    mInvalidated = true;
    return mDrawText;
  }

  public List<DrawWarriorShape> getDrawWarriorShape() {
    mInvalidated = true;
    return mDrawWarriorShape;
  }

  public List<DrawEmptyWarriorShape> getDrawEmptyWarriorShape() {
    mInvalidated = true;
    return mDrawEmptyWarriorShape;
  }

  public DrawPlayerCenteredText getDescription(Object object) {
    return mObjectDescriptionMap.get(object);
  }

  public void drawBottomLine(double startX, double startY, double stopX, double stopY,
                             @NonNull Paint paint) {
    mInvalidated = true;
    mDrawBottomLine.add(new DrawBottomLine(startX, startY, stopX, stopY, paint));
  }

  public void drawHighlightLine(double startX, double startY, double stopX, double stopY,
                                @NonNull Paint paint) {
    mInvalidated = true;
    mDrawHighlightLine.add(new DrawHighlightLine(startX, startY, stopX, stopY, paint));
  }

  public void drawCircle(double x, double y, double radius, int fillColor, int strokeColor,
                         boolean applyGradient, double strokeWidth) {
    mInvalidated = true;
    mDrawCircle.add(
        new DrawCircle(x, y, radius, fillColor, strokeColor, applyGradient, strokeWidth));
  }

  public void drawPlayerCircle(double x, double y, double radius, double strokeWidth,
                          int playerIndex) {
    mInvalidated = true;
    mDrawPlayerCircle.add(new DrawPlayerCircle(x, y, radius, strokeWidth, playerIndex));
  }

  public void drawOctangle(double x, double y, double radius, int fillColor, int strokeColor,
                           boolean applyGradient, double strokeWidth) {
    mInvalidated = true;
    mDrawOctangle.add(
        new DrawOctangle(x, y, radius, fillColor, strokeColor, applyGradient, strokeWidth));
  }

  public void drawPlayerOctangle(double x, double y, double radius, double strokeWidth,
                            int playerIndex) {
    mInvalidated = true;
    mDrawPlayerOctangle.add( new DrawPlayerOctangle(x, y, radius, strokeWidth, playerIndex));
  }

  public void drawStar(double x, double y, double radius, int fillColor, int strokeColor,
                  double strokeWidth) {
    mInvalidated = true;
    mDrawStar.add(new DrawStar(x, y, radius, fillColor, strokeColor, strokeWidth));
  }

  public void drawEmptyCircle(double x, double y, double radius, int color, double strokeWidth) {
    mInvalidated = true;
    mDrawEmptyCircle.add(new DrawEmptyCircle(x, y, radius, color, strokeWidth));
  }

  public void drawPlayerCenteredText(double x, double y, String text, double size, int playerIndex,
                                     boolean primaryColor, @Nullable Object object) {
    mInvalidated = true;
    final DrawPlayerCenteredText centeredText =
        new DrawPlayerCenteredText(x, y, text, size, playerIndex, primaryColor);
    mDrawPlayerCenteredText.add(centeredText);
    if (object != null) {
      mObjectDescriptionMap.put(object, centeredText);
    }
  }

  public void drawCenteredText(double x, double y, String text, double size, int color) {
    mInvalidated = true;
    mDrawCenteredText.add(new DrawCenteredText(x, y, text, size, color));
  }

  public void drawText(double x, double y, String text, double size, Paint.Align align,
                       Typeface typeface, int color) {
    mInvalidated = true;
    mDrawText.add(new DrawText(x, y, text, size, align, typeface, color));
  }

  public void drawRoadSectionHalfView(RoadSectionView roadSection, RoadSectionView.Direction end,
                                      int color, float strokeWidth, PathEffect pathEffect) {
    mInvalidated = true;
    mDrawRoadSectionHalfView.add(
        new DrawRoadSectionHalfView(roadSection, end, color, strokeWidth, pathEffect));
  }

  public void drawWarriorShape(double x, double y, double radius, int fillColor, int strokeColor,
                               boolean applyGradient, double strokeWidth, double angle) {
    mInvalidated = true;
    mDrawWarriorShape.add(
        new DrawWarriorShape(x, y, radius, fillColor, strokeColor, applyGradient, strokeWidth,
            angle));
  }

  public void drawEmptyWarriorShape(double x, double y, double radius, int color,
                               double strokeWidth, double angle) {
    mInvalidated = true;
    mDrawEmptyWarriorShape.add(
        new DrawEmptyWarriorShape(x, y, radius, color, strokeWidth, angle));
  }

  public void drawPlayerWarriorShape(double x, double y, int playerIndex, double radius,
                                     double strokeWidth, double angle) {
    mInvalidated = true;
    mDrawPlayerWarriorShape.add(
        new DrawPlayerWarriorShape(x, y, playerIndex, radius, strokeWidth, angle));
  }

  public void drawCityDescription(Point2D.Double coord, double radius, String cityName) {
    mInvalidated = true;
    mDrawCityDescription.add(
        new DrawCityDescription(coord, radius, cityName));
  }

  public void clear() {
    mDrawBottomLine.clear();
    mDrawHighlightLine.clear();
    mDrawCircle.clear();
    mDrawPlayerCircle.clear();
    mDrawOctangle.clear();
    mDrawPlayerOctangle.clear();
    mDrawStar.clear();
    mDrawEmptyCircle.clear();
    mDrawPlayerCenteredText.clear();
    mDrawCenteredText.clear();
    mDrawText.clear();
    mDrawRoadSectionHalfView.clear();
    mDrawWarriorShape.clear();
    mDrawEmptyWarriorShape.clear();
    mDrawPlayerWarriorShape.clear();
    mDrawCityDescription.clear();
    mObjectDescriptionMap.clear();
    mInvalidated = true;
  }

  public void validate() {
    mInvalidated = false;
  }

  public boolean isInvalidated() {
    return mInvalidated;
  }
}
