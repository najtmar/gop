package cz.wie.najtmar.gop.client.ui.activities;

import androidx.fragment.app.Fragment;
import cz.wie.najtmar.gop.client.ui.fragments.ControlPanelBoardGameFinishedFragment;

public class BoardGameFinishedActivity extends GameFragmentsActivity {

  @Override
  protected Fragment createControlPanelFragment() {
    return new ControlPanelBoardGameFinishedFragment();
  }
}
