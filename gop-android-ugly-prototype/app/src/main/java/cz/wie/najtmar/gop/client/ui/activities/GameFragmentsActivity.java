package cz.wie.najtmar.gop.client.ui.activities;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import cz.wie.najtmar.gop.client.ui.R;
import cz.wie.najtmar.gop.client.ui.fragments.BoardFragment;
import cz.wie.najtmar.gop.client.ui.fragments.StatusBarFragment;

/**
 * Created by najtmar on 17.10.16.
 */

public abstract class GameFragmentsActivity extends AppCompatActivity {


  private LinearLayout mBoardLayout;

  protected abstract Fragment createControlPanelFragment();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_game_fragments);

    FragmentManager fm = getSupportFragmentManager();

    // Status bar.
    Fragment statusBarFragment = fm.findFragmentById(R.id.status_bar_container);
    if (statusBarFragment == null) {
      statusBarFragment = new StatusBarFragment();
      fm.beginTransaction()
          .add(R.id.status_bar_container, statusBarFragment)
          .commit();
    }

    // Board.
    Fragment boardFragment = fm.findFragmentById(R.id.board_container);
    if (boardFragment == null) {
      boardFragment = new BoardFragment();
      fm.beginTransaction()
          .add(R.id.board_container, boardFragment)
          .commit();
    }

    // Control panel.
    Fragment controlPanelFragment = fm.findFragmentById(R.id.control_panel_container);
    if (controlPanelFragment == null) {
      controlPanelFragment = createControlPanelFragment();
      fm.beginTransaction()
          .add(R.id.control_panel_container, controlPanelFragment)
          .commit();
    }

    // Sliding panel.
    mBoardLayout = (LinearLayout) findViewById(R.id.board_layout);
  }

  @Override
  public boolean onKeyDown(int keyCode, KeyEvent event) {
    if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
      this.moveTaskToBack(true);
      return true;
    }
    return super.onKeyDown(keyCode, event);
  }

}
