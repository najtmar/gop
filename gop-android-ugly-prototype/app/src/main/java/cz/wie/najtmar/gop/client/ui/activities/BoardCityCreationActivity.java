package cz.wie.najtmar.gop.client.ui.activities;

import androidx.fragment.app.Fragment;

import cz.wie.najtmar.gop.client.ui.fragments.ControlPanelBoardCityCreationFragment;

/**
 * Created by najtmar on 24.10.16.
 */

public class BoardCityCreationActivity extends GameFragmentsActivity {

  @Override
  protected Fragment createControlPanelFragment() {
    return new ControlPanelBoardCityCreationFragment();
  }
}
