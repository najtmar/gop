package cz.wie.najtmar.gop.client.ui.fragments;

import androidx.fragment.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import cz.wie.najtmar.gop.client.ui.AndroidEngine;
import cz.wie.najtmar.gop.client.ui.GoP;

/**
 * Created by najtmar on 24.10.16.
 */

public abstract class ControlPanelBoardFragment extends Fragment {

  /**
   * Central model object.
   */
  private GoP mGoP;

  /**
   * Shared instance of class AndroidEngine, which may call back on the fragment.
   */
  private AndroidEngine mAndroidEngine;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mGoP = GoP.get(getContext());
  }

  /**
   * Initializes controls visible in the Fragment .
   * @param view the Fragment View
   */
  protected abstract void initializeControls(View view);

  /**
   * Returns the layout for the control panel.
   * @return the layout for the control panel
   */
  protected abstract int getControlPanelLayout();

  /**
   * Invoked in the onResume() method.
   */
  protected abstract void onControlPanelBoardFragmentResume();

  /**
   * Invoked in the onDestroy() method.
   */
  protected abstract void onControlPanelBoardFragmentDestroy();

  /**
   * Invoked when the state of controls might have changed.
   */
  public abstract void updateState();

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(getControlPanelLayout(), container, false);
    initializeControls(view);
    mAndroidEngine = (AndroidEngine) mGoP.getInjector().provideOutputEngine();

    return view;
  }

  @Override
  public final void onResume() {
    super.onResume();
    onControlPanelBoardFragmentResume();
    mAndroidEngine.invalidateViews();
  }

  @Override
  public final void onDestroy() {
    super.onDestroy();
    onControlPanelBoardFragmentDestroy();
  }

  protected GoP getGoP() {
    return mGoP;
  }

  protected AndroidEngine getAndroidEngine() {
    return mAndroidEngine;
  }

  /**
   * Returns a float value encoded in a resource defined by dimensionId .
   * @param dimensionId the dimension to read the value from
   * @return float value encoded in a resource defined by itemId
   */
  protected float getDimension(int dimensionId) {
    return getContext().getResources().getDimension(dimensionId);
  }
}
