package cz.wie.najtmar.gop.client.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.List;

import cz.wie.najtmar.gop.client.ui.AndroidGameManagementEngine;
import cz.wie.najtmar.gop.client.ui.AsyncGameManager;
import cz.wie.najtmar.gop.client.ui.GameManager;
import cz.wie.najtmar.gop.client.ui.GoP;
import cz.wie.najtmar.gop.client.ui.R;
import cz.wie.najtmar.gop.client.ui.activities.GameJoinedActivity;
import cz.wie.najtmar.gop.client.ui.activities.InitialActivity;
import cz.wie.najtmar.gop.server.KryoNetCommunicator;

/**
 * Created by najtmar on 11.10.16.
 */

public class JoinFragment extends MenuFragment {

  // UI elements.
  private ListView mJoinAvailableGamesList;
  private Button mJoinRefreshButton;
  private Button mJoinCancelButton;

  /**
   * Central model object.
   */
  private GoP mGoP;

  /**
   * Game manager.
   */
  private AsyncGameManager mAsyncGameManager;

  /**
   * Shared instance of class mAndroidGameManagementEngine, which may call back on the fragment.
   */
  private AndroidGameManagementEngine mAndroidGameManagementEngine;

  @Override
  public synchronized void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mGoP = GoP.get(getContext());
    mAsyncGameManager = new AsyncGameManager(mGoP.getInjector().provideGameManager());
    mAndroidGameManagementEngine =
        (AndroidGameManagementEngine) mGoP.getInjector().provideGameManagementEngine();
    mAndroidGameManagementEngine.setJoinFragment(this);
  }

  private void scheduleDelayedEnableRefreshCancelAndGamesList() {
    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {
        if (mJoinRefreshButton != null) {
          mJoinRefreshButton.setEnabled(true);
        }
        if (mJoinCancelButton != null) {
          mJoinCancelButton.setEnabled(true);
        }
        if (mJoinAvailableGamesList != null) {
          mJoinAvailableGamesList.setEnabled(true);
        }
      }
    }, KryoNetCommunicator.getConnectionTimeoutMillis() * 2);
  }

  private void initializeControls(View view) {
    mJoinAvailableGamesList = (ListView) view.findViewById(R.id.join_available_games);
    mJoinAvailableGamesList.setScrollbarFadingEnabled(false);
    mJoinAvailableGamesList.setAdapter(new ArrayAdapter<String>(getContext(),
        android.R.layout.simple_list_item_1));

    mJoinAvailableGamesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mAsyncGameManager.asyncJoinPendingGame(position);
      }
    });

    mJoinRefreshButton = (Button) view.findViewById(R.id.join_refresh);
    mJoinRefreshButton.setEnabled(false);
    mJoinRefreshButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        mAsyncGameManager.asyncRefreshPendingGamesList();
        mJoinRefreshButton.setEnabled(false);
        mJoinCancelButton.setEnabled(false);
        mJoinAvailableGamesList.setEnabled(false);
        scheduleDelayedEnableRefreshCancelAndGamesList();
      }
    });

    mJoinCancelButton = (Button) view.findViewById(R.id.join_cancel);
    mJoinCancelButton.setEnabled(false);
    mJoinCancelButton.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
        mAsyncGameManager.asyncLeaveGamesListed();
        mJoinRefreshButton.setEnabled(false);
        mJoinCancelButton.setEnabled(false);
            mJoinAvailableGamesList.setEnabled(false);
        scheduleDelayedEnableRefreshCancelAndGamesList();
      }
    });

    scheduleDelayedEnableRefreshCancelAndGamesList();
  }

  @Nullable
  @Override
  public synchronized View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.activity_join, container, false);
    initializeControls(view);

    return view;
  }

  /**
   * Updates the list of available Games .
   * @param availableGamesList the list of available Games to be set
   */
  public synchronized void updateAvailableGamesList(
      final List<GameManager.GameInfo> availableGamesList) {
    if (mJoinAvailableGamesList == null) {
      return;
    }
    final ArrayAdapter<GameManager.GameInfo>
        listAdapter = (ArrayAdapter) mJoinAvailableGamesList.getAdapter();
    while (getActivity() == null) {
      try {
        Thread.sleep(10);
      } catch (InterruptedException exception) {
        Log.e("JointFragment", "Waiting for the Activity to attach interrupted.");
      }
    }
    getActivity().runOnUiThread(new Runnable() {
      @Override
      public void run() {
        listAdapter.clear();
        listAdapter.addAll(availableGamesList);
      }
    });
  }

  /**
   * Initializes UI for State GAME_JOINED.
   */
  public synchronized void initializeGameJoined() {
    Intent intent = new Intent(getActivity(), GameJoinedActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

  public synchronized void startInitialActivity() {
    Intent intent = new Intent(getActivity(), InitialActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

  public ListView getJoinAvailableGamesList() {
    return mJoinAvailableGamesList;
  }

  public Button getJoinRefreshButton() {
    return mJoinRefreshButton;
  }

  public Button getJoinCancelButton() {
    return mJoinCancelButton;
  }
}
