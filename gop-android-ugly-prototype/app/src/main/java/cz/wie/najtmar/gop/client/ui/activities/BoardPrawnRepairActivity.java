package cz.wie.najtmar.gop.client.ui.activities;

import androidx.fragment.app.Fragment;

import cz.wie.najtmar.gop.client.ui.fragments.ControlPanelBoardPrawnRepairFragment;

/**
 * Created by najtmar on 04.11.16.
 */

public class BoardPrawnRepairActivity extends GameFragmentsActivity {

  @Override
  protected Fragment createControlPanelFragment() {
    return new ControlPanelBoardPrawnRepairFragment();
  }
}
