package cz.wie.najtmar.gop.client.ui.fragments;

import android.view.View;
import android.widget.Button;

import cz.wie.najtmar.gop.client.ui.R;

/**
 * Created by najtmar on 24.10.16.
 */

public class ControlPanelBoardCityCreationFragment extends ControlPanelBoardFragment {

  // UI elements.
  private Button mCreateCityButton;
  private Button mCancelButton;

  @Override
  protected void initializeControls(View view) {
    mCreateCityButton = (Button) view.findViewById(R.id.board_city_creation_create_city);
    mCreateCityButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getAndroidEngine().confirmCityCreation();
      }
    });
    mCancelButton = (Button) view.findViewById(R.id.board_city_creation_cancel);
    mCancelButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getAndroidEngine().abandonCityCreation();
      }
    });
  }

  @Override
  protected int getControlPanelLayout() {
    return R.layout.control_panel_board_city_creation;
  }

  @Override
  protected void onControlPanelBoardFragmentResume() {
    getAndroidEngine().setControlPanelBoardCityCreationFragment(this);
    getAndroidEngine().showCityCreationPrompt();
    getAndroidEngine().setCityCreationDefaultName();
  }

  @Override
  protected void onControlPanelBoardFragmentDestroy() {
  }

  @Override
  public void updateState() {
  }

  public void finishBoardCityCreationActivity() {
    getActivity().finish();
    getAndroidEngine().setControlPanelBoardCityCreationFragment(null);
  }
}
