package cz.wie.najtmar.gop.client.ui;

import android.util.Log;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cz.wie.najtmar.gop.client.ui.fragments.GameJoinedFragment;
import cz.wie.najtmar.gop.client.ui.fragments.GameInitiatedFragment;
import cz.wie.najtmar.gop.client.ui.fragments.InitialFragment;
import cz.wie.najtmar.gop.client.ui.fragments.JoinFragment;
import cz.wie.najtmar.gop.client.view.ClientViewException;

/**
 * Created by najtmar on 12.10.16.
 */

public class AndroidGameManagementEngine implements GameManagementEngine {

  private static AndroidGameManagementEngine sAndroidGameManagementEngine;
  private GameManager mGameManager;

  // Event-handling UI Fragments .
  private InitialFragment mInitialFragment;
  private JoinFragment mJoinFragment;
  private GameJoinedFragment mGameJoinedFragment;
  private GameInitiatedFragment mGameInitiatedFragment;

  @Override
  public void initializeGameManagementEngine(@Nonnull GameManager gameManager)
      throws ClientViewException {
    mGameManager = gameManager;
  }

  @Override
  public void onGameManagerStateChange(@Nullable GameManager.State oldState)
      throws ClientViewException {
    Log.d("AGME", oldState + " -> " + mGameManager.getState());
    switch (mGameManager.getState()) {
      case GAME_INITIATED:
        mInitialFragment.initializeGameInitiated();
        break;
      case GAMES_LISTED:
        if (oldState == GameManager.State.GAME_JOINED && mGameJoinedFragment != null) {
          mGameJoinedFragment.startJoinActivity();
        }
        break;
      case GAME_JOINED:
        mJoinFragment.initializeGameJoined();
        break;
      case JOINED_GAME_STARTED:
        mGameJoinedFragment.initializeBoardInitialized();
        break;
      case OWN_GAME_STARTED:
        mGameInitiatedFragment.initializeBoardInitialized();
        break;
      case INITIAL_MENU:
        if (oldState == GameManager.State.GAMES_LISTED) {
          mJoinFragment.startInitialActivity();
        } else if (oldState == GameManager.State.GAME_INITIATED) {
          mGameInitiatedFragment.startInitialActivity();
        } else if (oldState == GameManager.State.JOINED_GAME_STARTED) {
          mGameJoinedFragment.startInitialActivity();
        } else if (oldState == GameManager.State.GAME_JOINED) {
          while (mGameJoinedFragment == null) {
            try {
              Thread.sleep(10);
            } catch (InterruptedException exception) {
              throw new RuntimeException(exception);
            }
          }
          mGameJoinedFragment.startInitialActivity();
        } else if(oldState == GameManager.State.OWN_GAME_STARTED) {
          mGameInitiatedFragment.startInitialActivity();
        }
        break;
      default:
        // TODO(najtmar): Implement other cases.
        break;
    }
  }

  @Override
  public void updatePendingGamesList() {
    final List<GameManager.GameInfo> pendingGamesList = mGameManager.getPendingGamesList();
    if (mJoinFragment != null) {
      mJoinFragment.updateAvailableGamesList(pendingGamesList);
    }
  }

  @Override
  public void notifyActionNotFinished(@Nonnull GameManager.Action action) {
    // TODO(najtmar): Add an info dialog.
    try {
      mGameManager.reset(false);
    } catch (ClientViewException exception) {
      throw new RuntimeException(exception);
    }
  }

  @Override
  public void setOwnGameEnabled(boolean enabled, @Nonnull List<String> participantsList) {
    mGameInitiatedFragment.updateParticipantsList(enabled, participantsList);
  }

  public void setInitialFragment(InitialFragment initialFragment) {
    mInitialFragment = initialFragment;
  }

  public void setJoinFragment(JoinFragment joinFragment) {
    mJoinFragment = joinFragment;
  }

  public void setGameJoinedFragment(GameJoinedFragment gameJoinedFragment) {
    mGameJoinedFragment = gameJoinedFragment;
  }

  public void setGameInitiatedFragment(GameInitiatedFragment gameInitiatedFragment) {
    mGameInitiatedFragment = gameInitiatedFragment;
  }

  public static AndroidGameManagementEngine create() {
    if (sAndroidGameManagementEngine == null) {
      sAndroidGameManagementEngine = new AndroidGameManagementEngine();
    }
    return sAndroidGameManagementEngine;
  }

  private AndroidGameManagementEngine() {}
}
