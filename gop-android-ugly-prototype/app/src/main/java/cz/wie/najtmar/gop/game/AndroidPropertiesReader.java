package cz.wie.najtmar.gop.game;

import android.content.Context;
import android.util.Log;
import android.util.TypedValue;

import java.io.IOException;
import java.util.Properties;

import cz.wie.najtmar.gop.client.ui.R;
import cz.wie.najtmar.gop.client.view.ClientViewProperties;

/**
 * Reads Properties from *.properties files.
 * @author najtmar
 */
public class AndroidPropertiesReader implements PropertiesReader {

  private final Context mContext;

  AndroidPropertiesReader(Context context) {
    mContext = context;
  }

  @Override
  public Properties getBoardProperties() throws IOException {
    final Properties boardPropertiesSet = new Properties();
    boardPropertiesSet.load(mContext.getResources().getAssets().open("board.properties"));
    return boardPropertiesSet;
  }

  @Override
  public Properties getGameProperties() throws IOException {
    final Properties gamePropertiesSet = new Properties();
    gamePropertiesSet.load(mContext.getResources().getAssets().open("game.properties"));
    return gamePropertiesSet;
  }

  /**
   * Given a dimension id, returns the value of the dimension.
   * @param dimensionId the dimension id
   * @return the value of the dimension with a given dimensionId
   */
  private float getDimension(int dimensionId) {
    return mContext.getResources().getDimension(dimensionId);
  }

  /**
   * Returns a float value encoded in a resource defined by dimensionId .
   * @param dimensionId the resource to read the value from
   * @return float value encoded in a resource defined by dimensionId
   */
  private float getFloat(int dimensionId) {
    final TypedValue outValue = new TypedValue();
    mContext.getResources().getValue(dimensionId, outValue, true);
    return outValue.getFloat();
  }

  @Override
  public Properties getClientViewProperties() throws IOException {
    final Properties clientViewPropertiesSet = new Properties();
    clientViewPropertiesSet.load(mContext.getResources().getAssets().open(
        "client-view.properties"));

    // Overwrite properties with resources.
    final float clickDistance = getDimension(R.dimen.click_distance);
    final float distinctivePositionRadius = getFloat(R.dimen.distinctive_position_radius);

    clientViewPropertiesSet.setProperty(ClientViewProperties.CLICK_DISTANCE_PROPERTY_NAME,
        Float.toString(clickDistance));
    clientViewPropertiesSet.setProperty(
        ClientViewProperties.DISTINCTIVE_POSITION_RADIUS_PROPERTY_NAME,
        Float.toString(distinctivePositionRadius));

    final float prawnBoxSide = getDimension(R.dimen.prawn_box_side);
    final float prawnSelectionBoxWidth = getDimension(R.dimen.prawn_selection_box_width);
    final float prawnSelectionBoxHeight = getDimension(R.dimen.prawn_selection_box_height);
    final int prawnSelectionWidth = (int) Math.floor(prawnSelectionBoxWidth / prawnBoxSide);
    final int prawnSelectionHeight = (int) Math.floor(prawnSelectionBoxHeight / prawnBoxSide);

    clientViewPropertiesSet.setProperty(
        ClientViewProperties.PRAWN_SELECTION_WIDTH_PROPERTY_NAME,
        Integer.toString(prawnSelectionWidth));
    clientViewPropertiesSet.setProperty(
        ClientViewProperties.PRAWN_SELECTION_HEIGHT_PROPERTY_NAME,
        Integer.toString(prawnSelectionHeight));
    clientViewPropertiesSet.setProperty(
        ClientViewProperties.PRAWN_BOX_SIDE_PROPERTY_NAME,
        Integer.toString((int) Math.floor(prawnBoxSide)));

    final float cityPrawnBoxSide = getDimension(R.dimen.city_prawn_box_side);
    final float cityBoxWidth = getDimension(R.dimen.city_box_width);
    final float adsBoxWidth = getDimension(R.dimen.ads_box_width);
    final float cityBoxMargin = getDimension(R.dimen.city_element_margin);
    final int cityPrawnSelectionWidth =
        (int) Math.floor((cityBoxWidth + adsBoxWidth + 2 * cityBoxMargin) / cityPrawnBoxSide);
    final float cityPrawnSelectionBoxHeight = getDimension(R.dimen.city_prawn_selection_box_height);
    final int cityPrawnSelectionHeight =
        (int) Math.floor(cityPrawnSelectionBoxHeight / cityPrawnBoxSide);

    if (prawnBoxSide != cityPrawnBoxSide || prawnSelectionWidth != cityPrawnSelectionWidth
        || prawnSelectionHeight != cityPrawnSelectionHeight) {
      throw new IOException("Prawn box and City Prawn box parameters must be equals (Prawn box == ("
          + prawnBoxSide + ", " + prawnSelectionWidth + ", " + prawnSelectionHeight
          + "), City Prawn box == (" + cityPrawnBoxSide + ", " + cityPrawnSelectionWidth + ", "
          + cityPrawnSelectionHeight + ").");
    }

    clientViewPropertiesSet.setProperty(
        ClientViewProperties.CITY_PRAWN_SELECTION_WIDTH_PROPERTY_NAME,
        Integer.toString(cityPrawnSelectionWidth));
    clientViewPropertiesSet.setProperty(
        ClientViewProperties.CITY_PRAWN_SELECTION_HEIGHT_PROPERTY_NAME,
        Integer.toString(cityPrawnSelectionHeight));
    clientViewPropertiesSet.setProperty(
        ClientViewProperties.CITY_PRAWN_BOX_SIDE_PROPERTY_NAME,
        Integer.toString((int) Math.floor(cityPrawnBoxSide)));
    clientViewPropertiesSet.setProperty(
        ClientViewProperties.CITY_BOX_SIDE_PROPERTY_NAME,
        Integer.toString((int) Math.floor(cityBoxWidth + cityBoxMargin)));

    final float cityFactoryBoxWidth = cityBoxWidth + adsBoxWidth + 2 * cityBoxMargin;
    final float cityFactoryBoxHeight = getDimension(R.dimen.city_factory_box_height);
    final float cityFactorySelectionBoxHeight =
        getDimension(R.dimen.city_factory_selection_box_height);
    final int cityFactoryMaxBoxCount =
        (int) Math.floor(cityFactorySelectionBoxHeight / cityFactoryBoxHeight);
    final int warriorDirectionSelectionBoxSide =
        (int) Math.floor(getDimension(R.dimen.warrior_direction_selection_box_side));

    clientViewPropertiesSet.setProperty(
        ClientViewProperties.CITY_FACTORY_BOX_MAX_COUNT,
        Integer.toString(cityFactoryMaxBoxCount));
    clientViewPropertiesSet.setProperty(
        ClientViewProperties.CITY_FACTORY_BOX_WIDTH,
        Integer.toString((int) Math.floor(cityFactoryBoxWidth)));
    clientViewPropertiesSet.setProperty(
        ClientViewProperties.CITY_FACTORY_BOX_HEIGHT,
        Integer.toString((int) Math.floor(cityFactoryBoxHeight)));
    clientViewPropertiesSet.setProperty(
        ClientViewProperties.WARRIOR_DIRECTION_SELECTION_BOX_SIDE_PROPERTY_NAME,
        Integer.toString(warriorDirectionSelectionBoxSide));

    clientViewPropertiesSet.setProperty(
        ClientViewProperties.MAGNIFICATION_RATIO_PROPERTY_NAME,
        Float.toString(getFloat(R.dimen.magnification_ratio)));

    return clientViewPropertiesSet;
  }

  @Override
  public Properties getTestProperties() {
    final Properties testPropertiesSet = new Properties();
    try {
      testPropertiesSet.load(mContext.getResources().getAssets().open(
          "test.properties"));
      Log.i("GoP", "test.properties found. Test run started.");
    } catch (IOException exception) {
      return null;
    }
    return testPropertiesSet;
  }


}
