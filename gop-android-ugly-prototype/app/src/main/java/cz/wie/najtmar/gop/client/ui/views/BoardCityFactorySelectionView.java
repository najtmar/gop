package cz.wie.najtmar.gop.client.ui.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import cz.wie.najtmar.gop.client.ui.AndroidEngine;
import cz.wie.najtmar.gop.client.ui.GoP;
import cz.wie.najtmar.gop.client.ui.R;
import cz.wie.najtmar.gop.client.view.CityView;
import cz.wie.najtmar.gop.client.view.ClientViewException;
import cz.wie.najtmar.gop.client.view.FactoryView;
import cz.wie.najtmar.gop.client.view.PrawnView;
import cz.wie.najtmar.gop.common.Point2D;

/**
 * Created by najtmar on 28.10.16.
 */

public class BoardCityFactorySelectionView extends View {

  /**
   * Central model object.
   */
  private GoP mGoP;

  /**
   * Shared instance of class AndroidEngine, which may call back on the fragment.
   */
  private AndroidEngine mAndroidEngine;

  // City Factory parameters.
  private float mCityFactoryBoxWidth;
  private float mCityFactoryBoxHeight;
  private int mCityFactoryMaxBoxCount;
  private float mCityBoxMargin;
  private int mCityColor;
  private int mCityColorAccent;
  private float mProgressBarWidth;

  /**
   * Used to react to the gestures.
   */
  private final GestureDetector mGestureDetector;

  public BoardCityFactorySelectionView(Context context, AttributeSet attrs) {
    super(context, attrs);
    if (isInEditMode()) {
      mGestureDetector = null;
      return;
    }
    mGoP = GoP.get(context);
    final GestureListener gestureListener = new GestureListener();
    mGestureDetector = new GestureDetector(context, gestureListener, null, true);
    mAndroidEngine = (AndroidEngine) mGoP.getInjector().provideOutputEngine();

    // Read dimensions.
    mCityBoxMargin = context.getResources().getDimension(R.dimen.city_element_margin);
    final float cityBoxWidth = context.getResources().getDimension(R.dimen.city_box_width);
    final float adsBoxWidth = context.getResources().getDimension(R.dimen.ads_box_width);
    mCityFactoryBoxWidth = cityBoxWidth + adsBoxWidth + 2 * mCityBoxMargin;
    mCityFactoryBoxHeight = context.getResources().getDimension(R.dimen.city_factory_box_height);
    final float cityFactorySelectionBoxHeight =
        context.getResources().getDimension(R.dimen.city_factory_selection_box_height);
    mCityFactoryMaxBoxCount =
        (int) Math.floor(cityFactorySelectionBoxHeight / mCityFactoryBoxHeight);
    mCityColor = context.getResources().getColor(R.color.colorCity);
    mCityColorAccent = context.getResources().getColor(R.color.colorCityAccent);
    mProgressBarWidth = context.getResources().getDimension(R.dimen.progress_bar_width);
  }

  @Override
  public boolean onTouchEvent(MotionEvent event) {
    boolean result = false;
    if (mAndroidEngine.isCityViewOn()) {
      result = mGestureDetector.onTouchEvent(event);
    }
    return result;
  }

  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);

    if (isInEditMode()) return;
    final Paint paint = new Paint();
    final CityView citySelected = mAndroidEngine.getCitySelected();
    for (int i = 0; i < citySelected.getFactories().size(); ++i) {
      if (i != mAndroidEngine.getCityFactoryHighlightedIndex()) {
        paint.setColor(mCityColor);
      } else {
        paint.setColor(mCityColorAccent);
      }
      canvas.drawRect(0f, i * mCityFactoryBoxHeight, mCityFactoryBoxWidth - mCityBoxMargin,
          (i + 1) * mCityFactoryBoxHeight - mCityBoxMargin, paint);

      FactoryView factory = citySelected.getFactories().get(i);

      // Description.
      Point2D.Double iconCenter =
          new Point2D.Double(
              mCityFactoryBoxHeight / 2 / mAndroidEngine.getAndroidMagnifyX()
                  - AndroidEngine.TRANSLATE_X,
              (i + 0.5) * mCityFactoryBoxHeight / mAndroidEngine.getAndroidMagnifyY()
                  - AndroidEngine.TRANSLATE_Y);
      Point2D.Double textStart =
          new Point2D.Double(
              (mCityFactoryBoxHeight + mCityBoxMargin) / mAndroidEngine.getAndroidMagnifyX()
                  - AndroidEngine.TRANSLATE_X,
              (i + 0.5) * mCityFactoryBoxHeight / mAndroidEngine.getAndroidMagnifyY()
                  - AndroidEngine.TRANSLATE_Y);
      double textSize = mCityFactoryBoxHeight / 3;
      if (factory.getState().equals(FactoryView.State.IN_ACTION)) {
        double radius = mCityFactoryBoxHeight / 2 * 0.6;
        switch (factory.getActionType()) {
          case PRODUCE_SETTLERS_UNIT:
            mAndroidEngine.drawPlayerCircle(iconCenter.getX(), iconCenter.getY(), radius,
                getResources().getDimension(R.dimen.prawn_border_width),
                citySelected.getPlayer().getIndex(), canvas);
            mAndroidEngine.drawText(textStart.getX(), textStart.getY(),
                getResources().getString(R.string.produce_text), textSize, Paint.Align.LEFT,
                Typeface.defaultFromStyle(Typeface.BOLD),
                getResources().getColor(R.color.colorLightAccent), canvas);
            break;
          case PRODUCE_WARRIOR:
            try {
              mAndroidEngine.drawPlayerWarriorShape(iconCenter.getX(), iconCenter.getY(),
                  citySelected.getPlayer().getIndex(), radius,
                  getResources().getDimension(R.dimen.prawn_border_width),
                  factory.getWarriorDirection(), canvas);
              mAndroidEngine.drawText(textStart.getX(), textStart.getY(),
                  getResources().getString(R.string.produce_text), textSize,
                  Paint.Align.LEFT, Typeface.defaultFromStyle(Typeface.BOLD),
                  getResources().getColor(R.color.colorLightAccent), canvas);
            } catch (ClientViewException exception) {
              throw new RuntimeException("Failed to get Warrior direction.", exception);
            }
            break;
          case REPAIR_PRAWN:
            final PrawnView prawnRepaired;
            try {
              prawnRepaired = factory.getPrawnRepaired();
              if (prawnRepaired.getType().equals(PrawnView.Type.SETTLERS_UNIT)) {
                mAndroidEngine.drawPlayerCircle(iconCenter.getX(), iconCenter.getY(), radius,
                    getResources().getDimension(R.dimen.prawn_border_width),
                    citySelected.getPlayer().getIndex(), canvas);
              } else {
                mAndroidEngine.drawPlayerWarriorShape(iconCenter.getX(), iconCenter.getY(),
                    citySelected.getPlayer().getIndex(), radius,
                    getResources().getDimension(R.dimen.prawn_border_width),
                    prawnRepaired.getWarriorDirection(), canvas);
              }
            } catch (ClientViewException exception) {
              throw new RuntimeException(exception);
            }
            int prawnEnergy = (int) Math.ceil(prawnRepaired.getEnergy() * 10);
            mAndroidEngine.drawPlayerCenteredText(iconCenter.getX(), iconCenter.getY(),
                "" + prawnEnergy, radius, prawnRepaired.getPlayer().getIndex(), false, canvas);

            mAndroidEngine.drawText(textStart.getX(), textStart.getY(),
                getResources().getString(R.string.repair_text), textSize,
                Paint.Align.LEFT, Typeface.defaultFromStyle(Typeface.BOLD),
                getResources().getColor(R.color.colorLightAccent), canvas);
            break;
          case GROW:
            mAndroidEngine.drawText(textStart.getX(), textStart.getY(),
                getResources().getString(R.string.grow_city_text), textSize,
                Paint.Align.LEFT, Typeface.defaultFromStyle(Typeface.BOLD),
                getResources().getColor(R.color.colorLightAccent), canvas);
            break;
        }

        // Progress bar.
        Point2D.Double progressBarStart = new Point2D.Double(
            ((mCityFactoryBoxWidth + mCityFactoryBoxHeight) / 2 + mCityBoxMargin),
            (i + 0.5) * mCityFactoryBoxHeight - mCityBoxMargin / 2);
        double progressBarLength =
            (mCityFactoryBoxWidth - 3 * mCityBoxMargin) - progressBarStart.getX();
        final double elapsedProgressBarLength;
        try {
          elapsedProgressBarLength = progressBarLength * factory.getProgress();
        } catch (ClientViewException exception) {
          throw new RuntimeException("Failed to get Factory progress.", exception);
        }
        Paint progressBarPaint = new Paint();
        progressBarPaint.setStrokeWidth(mProgressBarWidth);
        progressBarPaint.setColor(getResources().getColor(R.color.colorWarning));
        canvas.drawLine((float) progressBarStart.getX(), (float) progressBarStart.getY(),
            (float) (progressBarStart.getX() + elapsedProgressBarLength),
            (float) progressBarStart.getY(),
            progressBarPaint);
        progressBarPaint.setColor(getResources().getColor(R.color.colorAccent));
        canvas.drawLine((float) (progressBarStart.getX() + elapsedProgressBarLength),
            (float) progressBarStart.getY(), (float) (progressBarStart.getX() + progressBarLength),
            (float) progressBarStart.getY(), progressBarPaint);
      } else {
        Drawable warningIcon = getResources().getDrawable(R.drawable.ic_warning_black_80dp);
        warningIcon.setTint(getResources().getColor(R.color.colorWarning));
        warningIcon.setBounds(
            (int) mCityBoxMargin,
            (int) (i * mCityFactoryBoxHeight + mCityBoxMargin),
            (int) (mCityFactoryBoxHeight - 2 * mCityBoxMargin),
            (int) (i * mCityFactoryBoxHeight + mCityFactoryBoxHeight - 2 * mCityBoxMargin));
        warningIcon.draw(canvas);

        mAndroidEngine.drawText(textStart.getX(), textStart.getY(),
            getResources().getString(R.string.idle_factory_text), textSize,
            Paint.Align.LEFT, Typeface.defaultFromStyle(Typeface.BOLD),
            getResources().getColor(R.color.colorLightAccent), canvas);
      }
    }
  }

  private class GestureListener extends GestureDetector.SimpleOnGestureListener {
    @Override
    public boolean onDown(MotionEvent event) {
      return true;
    }

    @Override
    public void onShowPress(MotionEvent e) {
      if (e.getX() < Math.floor(mCityFactoryBoxWidth)
          && e.getY() < mCityFactoryMaxBoxCount * Math.floor(mCityFactoryBoxHeight)) {
        mAndroidEngine.selectCityFactoryPoint(new Point2D.Double(e.getX(), e.getY()));
      }
    }
  }
}
