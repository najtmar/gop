package cz.wie.najtmar.gop.server;

/**
 * Provider for class CrossBoardPrawnDeployer .
 */

public class CrossBoardPrawnDeployerProvider {

  public static InitialPrawnDeployer provide() {
    return new CrossBoardPrawnDeployer();
  }

}
