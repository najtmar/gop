package cz.wie.najtmar.gop.client.ui;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathEffect;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import android.util.Log;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;

import cz.wie.najtmar.gop.board.BoardException;
import cz.wie.najtmar.gop.board.BoardProperties;
import cz.wie.najtmar.gop.client.ui.fragments.BoardFragment;
import cz.wie.najtmar.gop.client.ui.fragments.ControlPanelBoardCityCreationFragment;
import cz.wie.najtmar.gop.client.ui.fragments.ControlPanelBoardBoardFragment;
import cz.wie.najtmar.gop.client.ui.fragments.ControlPanelBoardCityFragment;
import cz.wie.najtmar.gop.client.ui.fragments.ControlPanelBoardFactoryFragment;
import cz.wie.najtmar.gop.client.ui.fragments.ControlPanelBoardFragment;
import cz.wie.najtmar.gop.client.ui.fragments.ControlPanelBoardGameAbandoningFragment;
import cz.wie.najtmar.gop.client.ui.fragments.ControlPanelBoardGameFinishedFragment;
import cz.wie.najtmar.gop.client.ui.fragments.ControlPanelBoardInitializedFragment;
import cz.wie.najtmar.gop.client.ui.fragments.ControlPanelBoardItineraryBuiltFragment;
import cz.wie.najtmar.gop.client.ui.fragments.ControlPanelBoardMagnifiedFragment;
import cz.wie.najtmar.gop.client.ui.fragments.ControlPanelBoardPrawnFragment;
import cz.wie.najtmar.gop.client.ui.fragments.ControlPanelBoardPrawnRepairFragment;
import cz.wie.najtmar.gop.client.ui.fragments.ControlPanelBoardPrawnSelectionFragment;
import cz.wie.najtmar.gop.client.ui.fragments.ControlPanelBoardSettlersUnitFragment;
import cz.wie.najtmar.gop.client.ui.fragments.ControlPanelBoardWarriorDirectionSelectionFragment;
import cz.wie.najtmar.gop.client.ui.fragments.ForceQuitGameInfoDialogFragment;
import cz.wie.najtmar.gop.client.ui.fragments.StatusBarFragment;
import cz.wie.najtmar.gop.client.view.BattleView;
import cz.wie.najtmar.gop.client.view.CityView;
import cz.wie.najtmar.gop.client.view.ClientViewException;
import cz.wie.najtmar.gop.client.view.FactoryView;
import cz.wie.najtmar.gop.client.view.GameView;
import cz.wie.najtmar.gop.client.view.JointView;
import cz.wie.najtmar.gop.client.view.PlayerView;
import cz.wie.najtmar.gop.client.view.PositionView;
import cz.wie.najtmar.gop.client.view.PrawnView;
import cz.wie.najtmar.gop.client.view.RoadSectionView;
import cz.wie.najtmar.gop.client.view.TextManager;
import cz.wie.najtmar.gop.common.Point2D;
import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.game.PropertiesReader;

/**
 * Created by najtmar on 12.10.16.
 */

public class AndroidEngine implements InputEngine, OutputEngine, GameManagerManagedEngine {

  private final static double[] OCTANGLE_ANGLES = new double[]{
      2 * Math.PI / 16, 3 * 2 * Math.PI / 16, 5 * 2 * Math.PI / 16, 7 * 2 * Math.PI / 16,
      9 * 2 * Math.PI / 16, 11 * 2 * Math.PI / 16, 13 * 2 * Math.PI / 16, 15 * 2 * Math.PI / 16,
  };

  private final static double[] PENTAGRAM_ANGLES = new double[]{
      2 * Math.PI - Math.PI / 2, Math.PI / 2 + 3 * 2 * Math.PI / 5,
      2 * Math.PI - Math.PI / 2 + 2 * Math.PI / 5, Math.PI / 2 + 4 * 2 * Math.PI / 5,
      2 * Math.PI - Math.PI / 2 + 2 * 2 * Math.PI / 5, Math.PI / 2 + 5 * 2 * Math.PI / 5,
      2 * Math.PI - Math.PI / 2 + 3 * 2 * Math.PI / 5, Math.PI / 2 + 6 * 2 * Math.PI / 5,
      2 * Math.PI - Math.PI / 2 + 4 * 2 * Math.PI / 5, Math.PI / 2 + 7 * 2 * Math.PI / 5,
  };

  public static final int EMPTY_COLOR = -1234567890;

  // Translation constants.
  public static final float TRANSLATE_X = 25.0f;
  public static final float TRANSLATE_Y = 25.0f;
  public static final float TRANSLATE_RADIUS = (float) Math.sqrt(TRANSLATE_X * TRANSLATE_Y);

  private static AndroidEngine sAndroidEngine;

  // Properties.
  private PropertiesReader mPropertiesReader;
  private BoardProperties mBoardProperties;

  // The reason why we are in state MAGNIFIED .
  private InputManager.State mMagnificationReason;

  /**
   * The Game processed.
   */
  private GameView mGame;
  private boolean mGameViewportInitialized;

  /**
   * Central model object.
   */
  private final GoP mGoP;

  private GameManager mGameManager;
  private InputManager mInputManager;

  private TextManager mTextManager;

  // Event-handling UI Fragments .
  private StatusBarFragment mStatusBarFragment;
  private BoardFragment mBoardFragment;
  private ControlPanelBoardInitializedFragment mControlPanelBoardInitializedFragment;
  private ControlPanelBoardBoardFragment mControlPanelBoardBoardFragment;
  private ControlPanelBoardMagnifiedFragment mControlPanelBoardMagnifiedFragment;
  private ControlPanelBoardPrawnFragment mControlPanelBoardPrawnFragment;
  private ControlPanelBoardItineraryBuiltFragment mControlPanelBoardItineraryBuiltFragment;
  private ControlPanelBoardCityCreationFragment mControlPanelBoardCityCreationFragment;
  private ControlPanelBoardCityFragment mControlPanelBoardCityFragment;
  private ControlPanelBoardFactoryFragment mControlPanelBoardFactoryFragment;
  private ControlPanelBoardWarriorDirectionSelectionFragment
      mControlPanelBoardWarriorDirectionSelectionFragment;
  private ControlPanelBoardPrawnSelectionFragment mControlPanelBoardPrawnSelectionFragment;
  private ControlPanelBoardPrawnRepairFragment mControlPanelBoardPrawnRepairFragment;
  private ControlPanelBoardGameAbandoningFragment mControlPanelBoardGameAbandoningFragment;
  private ControlPanelBoardGameFinishedFragment mControlPanelBoardGameFinishedFragment;

  // Bitmaps.
  private Bitmap mBoardBackgroundBitmap;

  // Android magnification.
  private double mAndroidMagnifyX;
  private double mAndroidMagnifyY;
  private double mAndroidMagnifyRadius;

  // Magnification parameters.
  private double mMagnifyCenterX;
  private double mMagnifyCenterY;
  private boolean mIsMagnifyOn;

  private Map<PrawnView, Point2D.Double> mDistinctivePrawns;
  private Map<CityView, Point2D.Double> mDistinctiveCities;
  private Map<PositionView, Point2D.Double> mDistinctivePositions;

  // City view parameters.
  private double mCityViewPrawnBoxSide;
  private int mCityViewPrawnSelectionWidth;
  private int mCityViewPrawnSelectionHeight;

  private PrawnView[][] mCityViewPrawnSelection;

  private int[] mCityBoxCapturedHalves;
  private double mCityProductivity;

  // City view parameters.
  private boolean mIsCityViewOn;
  private int mCityFactoryHighlightedIndex;

  // Prawn selection view parameters.
  private boolean mIsPrawnSelectionViewOn;

  private PrawnView[][] mPrawnSelection;

  private int mPrawnSelectedForRepairX;
  private int mPrawnSelectedForRepairY;

  // Warrior direction selection parameters.
  private boolean mIsWarriorDirectionSelectionOn;
  private double mWarriorDirection;

  // Game abandoning parameters.
  private boolean mIsGameAbandoningPromptShown;

  /**
   * X coordinate of the real upper-left corner of the Board.
   */
  private float mRealBoardMinX;
  /**
   * Y coordinate of the real upper-left corner of the Board.
   */
  private float mRealBoardMinY;

  // Help item positions.
  private int mGameTutorialItemPos = 0;
  private int mGameRulesItemPos = 0;

  private String mTimeInfo;
  private String mStateLabel;
  private String[] mPeriodicInfo;

  private List<Point2D.Double> mCityToCreatePositions;
  private List<String> mCityToCreateNames;

  private Bitmap mSharedBitmap;

  private DrawingTool mBoardDrawingTool = new DrawingTool();
  private DrawingTool mCityDrawingTool = new DrawingTool();
  private DrawingTool mPrawnDrawingTool = new DrawingTool();
  private DrawingTool mBattleDrawingTool = new DrawingTool();
  private DrawingTool mHighlightDrawingTool = new DrawingTool();

  private volatile boolean mOwnConnectionUp = true;
  private volatile boolean mOthersConnectionUp = true;
  private volatile boolean mOwnConnectionUpSwitched = true;
  private volatile boolean mOthersConnectionUpSwitched = true;

  @Override
  public void initializeInput(@NonNull GameView gameView, @NonNull InputManager inputManager)
      throws ClientViewException {
    mInputManager = inputManager;
  }

  private String getStateLabel(InputManager.State state) {
    try {
      switch (state) {
        case PRAWN:
          return getStringResourceByName(
              "State_" + mInputManager.getPrawnSelected().getType().name());
        case CITY:
          return getStringResourceByName("State_" + state.name()) + " "
              + mInputManager.getCitySelected().getName();
        case GAME_FINISHED:
          return getStringResourceByName("State_" + getCurrentPlayer().getState().name());
        case MAGNIFIED:
          if (mMagnificationReason == InputManager.State.ITINERARY_BUILT) {
            return getStringResourceByName("State_" + InputManager.State.ITINERARY_BUILT.name());
          } else {
            return getStringResourceByName("State_" + state.name());
          }
        default:
          return getStringResourceByName("State_" + state.name());
      }
    } catch (ClientViewException exception) {
      throw new RuntimeException(exception);
    }
  }

  @Override
  public void onStateChange(InputManager.State oldState) throws ClientViewException {
    Log.d("AndroidEngine", "" + oldState + " -> " + mInputManager.getState());

    switch (oldState) {
      case BOARD:
        if (mInputManager.getState() == InputManager.State.INITIALIZED) {
          while (mControlPanelBoardBoardFragment == null) {
            try {
              Thread.sleep(10);
            } catch (InterruptedException exception) {
              throw new ClientViewException("Interrupted while sleep()ing.", exception);
            }
          }
          mControlPanelBoardBoardFragment.finishBoardBoardActivity();
        }
        break;

      case PRAWN:
        if (mInputManager.getState().equals(InputManager.State.BOARD)
            || mInputManager.getState().equals(InputManager.State.CITY)) {
          mControlPanelBoardPrawnFragment.finishBoardPrawnActivity();
        }
        break;

      case MAGNIFIED:
        if (mControlPanelBoardMagnifiedFragment != null) {
          mControlPanelBoardMagnifiedFragment.finishBoardMagnifiedActivity();
        }
        break;

      case ITINERARY_BUILT:
        if (mControlPanelBoardItineraryBuiltFragment != null
            && !mInputManager.getState().equals(InputManager.State.MAGNIFIED)) {
          mControlPanelBoardItineraryBuiltFragment.finishBoardItineraryBuiltActivity();
        }
        break;

      case CITY_CREATION:
        if (mControlPanelBoardCityCreationFragment != null) {
          mControlPanelBoardCityCreationFragment.finishBoardCityCreationActivity();
        }
        break;

      case CITY:
        if (mControlPanelBoardCityFragment != null
            && !mInputManager.getState().equals(InputManager.State.FACTORY)
            && !mInputManager.getState().equals(InputManager.State.PRAWN)) {
          mControlPanelBoardCityFragment.finishBoardCityActivity();
        }
        break;

      case FACTORY:
        if (mControlPanelBoardFactoryFragment != null
            && mInputManager.getState() != InputManager.State.WARRIOR_DIRECTION
            && mInputManager.getState() != InputManager.State.PRAWN_REPAIR) {
          mControlPanelBoardFactoryFragment.finishBoardFactoryActivity();
        }
        break;

      case WARRIOR_DIRECTION:
        if (mControlPanelBoardWarriorDirectionSelectionFragment != null) {
          mControlPanelBoardWarriorDirectionSelectionFragment
              .finishBoardWarriorDirectionSelectionActivity();
        }
        break;

      case PRAWN_SELECTION:
        if (mControlPanelBoardPrawnSelectionFragment != null
            && mInputManager.getState() != InputManager.State.PRAWN) {
          mControlPanelBoardPrawnSelectionFragment.finishBoardPrawnSelectionActivity();
        }
        break;

      case PRAWN_REPAIR:
        if (mControlPanelBoardPrawnRepairFragment != null) {
          mControlPanelBoardPrawnRepairFragment.finishBoardPrawnRepairActivity();
        }
        break;

      case GAME_ABANDONING:
        // NOTE: No Activity started.
        break;

      case GAME_FINISHED:
        if (mControlPanelBoardGameFinishedFragment != null) {
          mControlPanelBoardGameFinishedFragment.finishBoardGameFinishedActivity();
        }
        if (mControlPanelBoardInitializedFragment != null) {
          mControlPanelBoardInitializedFragment.finishBoardBoardInitializedActivity();
        }
        break;

      default:
        // Do nothing.
        break;
    }

    // Must be invoked before setting the State label.
    if (mInputManager.getState() == InputManager.State.MAGNIFIED) {
      mMagnificationReason = oldState;
    }

    // Set the State label.
    mStateLabel = getStateLabel(mInputManager.getState());

    switch (mInputManager.getState()) {
      case BOARD:
        if (oldState.equals(InputManager.State.INITIALIZED)) {
          while (mControlPanelBoardBoardFragment != null) {
            try {
              Thread.sleep(10);
            } catch (InterruptedException exception) {
              throw new ClientViewException("Interrupted while sleep()ing.", exception);
            }
          }
        }
        if (mControlPanelBoardBoardFragment == null) {
          mControlPanelBoardInitializedFragment.startBoardBoardActivity();
        }

        // Wait for the User .
        if (oldState.equals(InputManager.State.INITIALIZED)) {
          mGoP.acquireUiSemaphore();
        }
        break;

      case PRAWN:
        // NOTE: We always start an Activity, irrespective of the value of
        // mControlPanelBoardPrawnFragment . This handles the case of going directly from
        // State MAGNIFIED to State PRAWN .
        switch (mInputManager.getPrawnSelected().getType()) {
          case WARRIOR:
            mControlPanelBoardBoardFragment.startBoardWarriorActivity();
            break;
          case SETTLERS_UNIT:
            mControlPanelBoardBoardFragment.startBoardSettlersUnitActivity();
            break;
          default:
            throw new ClientViewException("Unknown Prawn Type: "
                + mInputManager.getPrawnSelected().getType());
        }

        mInputManager.onPrawnSelected();
        break;

      case ITINERARY_BUILT:
        if (mControlPanelBoardItineraryBuiltFragment == null) {
          mControlPanelBoardPrawnFragment.initializeItineraryBuilt();
        }
        if (oldState.equals(InputManager.State.MAGNIFIED)) {
          mInputManager.onPrawnItineraryBuildResumed();
        }
        break;

      case MAGNIFIED:
        mBoardFragment.startBoardMagnifiedActivity();
        break;

      case CITY_CREATION:
        if (mControlPanelBoardCityCreationFragment == null) {
          ((ControlPanelBoardSettlersUnitFragment) mControlPanelBoardPrawnFragment)
              .startCityCreationActivity();
        }
        break;

      case CITY:
        // NOTE: We always start an Activity, irrespective of the value of
        // mControlPanelBoardCityFragment . This handles the case of going directly from
        // State PRAWN_REPAIR to State CITY .
        mControlPanelBoardBoardFragment.startBoardCityActivity();
        break;

      case FACTORY:
        if (mControlPanelBoardFactoryFragment == null) {
          mControlPanelBoardCityFragment.startBoardFactoryActivity();
        }
        break;

      case WARRIOR_DIRECTION:
        if (mControlPanelBoardWarriorDirectionSelectionFragment == null) {
          mControlPanelBoardFactoryFragment.startBoardWarriorDirectionSelectionActivity();
        }
        break;

      case PRAWN_SELECTION:
        // NOTE: We always start an Activity, irrespective of the value of
        // mControlPanelBoardBoardFragment . This handles the case of going directly from
        // State PRAWN to State PRAWN_SELECTION .
        mControlPanelBoardBoardFragment.startBoardPrawnSelectionActivity();
        break;

      case PRAWN_REPAIR:
        if (mControlPanelBoardPrawnRepairFragment == null) {
          mControlPanelBoardFactoryFragment.startBoardPrawnRepairActivity();
        }
        break;

      case GAME_ABANDONING:
        // NOTE: No Activity started.
        break;

      case GAME_FINISHED:
        if (mControlPanelBoardGameFinishedFragment == null) {
          mControlPanelBoardInitializedFragment.startBoardGameFinishedActivity();
        }
        break;

      default:
        // TODO(najtmar): Implement the other cases.
        break;
    }
  }

  /**
   * Handles events of short pressing of the screen.
   * @param pressX the X coordinate of the press point
   * @param pressY the Y coordinate of the press point
   */
  public void onBoardShortPress(float pressX, float pressY) {
    switch (mInputManager.getState()) {
      case BOARD:
        mInputManager.onBoardPointSelected(transformPressPoint(pressX, pressY));
        break;
      case PRAWN:
        mInputManager.onPrawnItineraryBuildProgress(transformPressPoint(pressX, pressY));
        break;
      case ITINERARY_BUILT:
        mInputManager.onPrawnItineraryBuildProgress(transformPressPoint(pressX, pressY));
        break;
      case MAGNIFIED:
        if (!mIsMagnifyOn) {
          mInputManager.onPrawnItineraryBuildProgress(transformPressPoint(pressX, pressY));
        }
        break;
    }
  }

  /**
   * Handles events of short pressing of the magnified screen.
   * @param pressX the X coordinate of the press point, relative to mMagnifyCenterX
   * @param pressY the Y coordinate of the press point, relative to mMagnifyCenterY
   */
  public void onMagnifiedShortPress(float pressX, float pressY) {
    switch (mInputManager.getState()) {
      case MAGNIFIED:
        if (mIsMagnifyOn) {
          mInputManager.onMagnifiedPointSelected(transformMagnifiedPressPoint(pressX, pressY));
        }
        break;
    }
  }

  /**
   * Given a type and a name, returns the resource represented by it.
   * @param type the type of the resource to read
   * @param name the name of the resource to read
   * @return the resource represented by type and name
   * @throws ClientViewException when reading the resource fails
   */
  private int getResourceByTypeAndName(String type, String name) throws ClientViewException {
    try {
      final Class resource = Class.forName(getClass().getPackage().getName() + ".R$" + type);
      final Field field = resource.getField(name);
      return field.getInt(null);
    } catch (ClassNotFoundException exception) {
      throw new ClientViewException(exception);
    } catch (IllegalAccessException exception) {
      throw new ClientViewException(exception);
    } catch (NoSuchFieldException exception) {
      throw new ClientViewException(exception);
    }
  }

  /**
   * Given a name of a string resource, returns the string
   * @param name the name of the string resource
   * @return the string resource with a given name
   * @throws ClientViewException when reading the resource fails
   */
  private String getStringResourceByName(String name) throws ClientViewException {
    int resourceId = getResourceByTypeAndName("string", name);
    return mBoardFragment.getResources().getString(resourceId);
  }

  /**
   * Given a name, returns the id of the Drawable represented by it.
   * @param name the name of the drawable to read
   * @return the id of the Drawable represented by name
   * @throws ClientViewException when reading the drawable fails
   */
  private int getDrawableIdByName(String name) throws ClientViewException {
    return getResourceByTypeAndName("drawable", name);
  }

  /**
   * Given a Drawable id, returns the Drawable .
   * @param drawableId the id of the Drawable
   * @return the Drawable with a given drawableId
   */
  private Drawable getDrawable(int drawableId) {
    return mBoardFragment.getResources().getDrawable(drawableId);
  }

  /**
   * Given a Color Resource, returns the Color represented by it.
   * @param resource the Color Resource
   * @return the Color represented by resource
   */
  private int getColor(int resource) {
    return mBoardFragment.getResources().getColor(resource);
  }

  /**
   * Given the Player index, returns the primary Color for the Player
   * @param index the index of the Player
   * @param defaultColor the default Color
   * @return the Color of the Player with given index, or defaultColor if the color is not found
   */
  private int getPlayerColor(int index, int defaultColor) {
    try {
      return getColor(getResourceByTypeAndName("color", "colorPlayer" + index));
    } catch (ClientViewException exception) {
      Log.d("AndroidEngine", "Primary Color not defined for Player " + index);
      return defaultColor;
    }
  }

  /**
   * Given the Player index, returns the complementary Color for the Player
   * @param index the index of the Player
   * @param defaultColor the default Color
   * @return the complementary Color of the Player with given index, or defaultColor if the color is
   *         not found
   */
  private int getComplementaryPlayerColor(int index, int defaultColor) {
    try {
      return getColor(getResourceByTypeAndName("color", "colorComplementaryPlayer" + index));
    } catch (ClientViewException exception) {
      Log.d("AndroidEngine", "Complementary Color not defined for Player " + index);
      return defaultColor;
    }
  }

  private float getDimension(int dimensionId) {
    return mBoardFragment.getContext().getResources().getDimension(dimensionId);
  }

  @Override
  public synchronized void initializeOutput(@NonNull GameView gameView) throws ClientViewException {
    while (mBoardFragment == null) {
      try {
        Thread.sleep(10);
      } catch (InterruptedException exception) {
        throw new ClientViewException("Interrupted while sleep()ing.", exception);
      }
    }

    // Read bounds and the bitmap.
    BitmapFactory.Options optionsIO = new BitmapFactory.Options();
    optionsIO.inJustDecodeBounds = true;
    BitmapFactory.decodeResource(mBoardFragment.getContext().getResources(),
        getDrawableIdByName(mBoardProperties.getBackgroundImage()), optionsIO);
    mBoardBackgroundBitmap =
        BitmapFactory.decodeResource(mBoardFragment.getContext().getResources(), R.drawable.poland);
    mAndroidMagnifyX = (double) mBoardBackgroundBitmap.getWidth() / optionsIO.outWidth;
    mAndroidMagnifyY = (double) mBoardBackgroundBitmap.getHeight() / optionsIO.outHeight;

    mAndroidMagnifyRadius = Math.sqrt(mAndroidMagnifyX * mAndroidMagnifyY);

    mCityViewPrawnBoxSide = getDimension(R.dimen.city_prawn_box_side);
    final float cityBoxWidth = getDimension(R.dimen.city_box_width);
    final float adsBoxWidth = getDimension(R.dimen.ads_box_width);
    final float cityBoxMargin = getDimension(R.dimen.city_element_margin);
    mCityViewPrawnSelectionWidth =
        (int) Math.floor((cityBoxWidth + adsBoxWidth + 2 * cityBoxMargin) / mCityViewPrawnBoxSide);
    final float cityPrawnSelectionBoxHeight = getDimension(R.dimen.city_prawn_selection_box_height);
    mCityViewPrawnSelectionHeight =
        (int) Math.floor(cityPrawnSelectionBoxHeight / mCityViewPrawnBoxSide);

    mGame = gameView;
    mGameViewportInitialized = false;
    mCityFactoryHighlightedIndex = -1;

    mPrawnSelectedForRepairX = -1;
    mPrawnSelectedForRepairY = -1;

    mCityToCreatePositions = new LinkedList<>();
    mCityToCreateNames = new LinkedList<>();

    mOwnConnectionUp = true;
    mOthersConnectionUp = true;
    mOwnConnectionUpSwitched = true;
    mOthersConnectionUpSwitched = true;

    mIsMagnifyOn = false;
    mIsCityViewOn = false;
    mIsPrawnSelectionViewOn = false;
    mIsWarriorDirectionSelectionOn = false;
  }

  @Override
  public void onTimeUpdated(long time) {
    mTimeInfo = mTextManager.pseudoTimestampToDateString(time);
  }

  @Override
  public void onPeriodicInfoUpdated(@NonNull NavigableMap<String, Integer> relevantEvents)
      throws ClientViewException {
    mPeriodicInfo = new String[relevantEvents.size()];
    int i = 0;
    for (NavigableMap.Entry<String, Integer> infoEntry : relevantEvents.entrySet()) {
      final String infoText = getStringResourceByName("Event_" + infoEntry.getKey());
      if (infoEntry.getValue() == 1) {
        mPeriodicInfo[i] = " \u2022 " + infoText;
      } else {
        mPeriodicInfo[i] = " \u2022 " + infoText + " x " + infoEntry.getValue();
      }
      ++i;
    }
  }

  // ********************************** Graphic helper methods ********************************** //

  /**
   * Transforms dp float to the pixel value. Used for dimensions specified in legacy properties.
   * @param x the x value specified in dp
   * @return x converted to a pixel value
   */
  public float legacyTransfromDpToPxX(float x) {
    return (float) (x * mAndroidMagnifyX);
  }

  /**
   * Transforms dp float to the pixel value. Used for dimensions specified in legacy properties.
   * @param y the y value specified in dp
   * @return y converted to a pixel value
   */
  public float legacyTransfromDpToPxY(float y) {
    return (float) (y * mAndroidMagnifyY);
  }

  private float transformX(double x) {
    return (float) ((x + TRANSLATE_X) * mAndroidMagnifyX);
  }

  private float transformY(double y) {
    return (float) ((y + TRANSLATE_Y) * mAndroidMagnifyY);
  }

  private Point2D.Double transformPressPoint(double pressX, double pressY) {
    return new Point2D.Double((pressX - mRealBoardMinX) / mAndroidMagnifyX - TRANSLATE_X,
        (pressY - mRealBoardMinY) / mAndroidMagnifyY - TRANSLATE_Y);
  }

  private Point2D.Double transformMagnifiedPressPoint(double pressX, double pressY) {
    return new Point2D.Double((pressX + mMagnifyCenterX) / mAndroidMagnifyX - TRANSLATE_X,
        (pressY + mMagnifyCenterY) / mAndroidMagnifyY - TRANSLATE_Y);
  }

  private void drawBottomLine(double startX, double startY, double stopX, double stopY,
                              @NonNull Paint paint, Canvas canvas) {
    canvas.drawLine(transformX(startX), transformY(startY), transformX(stopX), transformY(stopY),
        paint);
  }

  private void drawHighlightLine(double startX, double startY, double stopX, double stopY,
                                 @NonNull Paint paint, Canvas canvas) {
    canvas.drawLine(transformX(startX), transformY(startY), transformX(stopX),
        transformY(stopY), paint);
  }

  private Shader createLinearGradient(double x, double y, double radius, int color0, int color1) {
    final double angle0 = 7 * Math.PI / 4;
    float x0 = (float) (x + 2 * radius * Math.cos(angle0));
    float y0 = (float) (y + 2 * radius * Math.sin(angle0));
    final double angle1 = 2 * Math.PI / 4;
    float x1 = (float) (x + 2 * radius * Math.cos(angle1));
    float y1 = (float) (y + 2 * radius * Math.sin(angle1));
    return new LinearGradient(x0, y0, x1, y1, new int[]{color1, color0, color1},
        new float[]{0f, 0.5f, 1f}, Shader.TileMode.MIRROR);
  }

  private void drawCircle(double x, double y, double radius, int fillColor, int strokeColor,
                          boolean applyGradient, double strokeWidth, Canvas canvas) {
    float transformX = transformX(x);
    float transformY = transformY(y);
    Paint fillPaint = new Paint();
    fillPaint.setColor(fillColor);
    fillPaint.setStyle(Paint.Style.FILL);
    if (applyGradient) {
      fillPaint.setShader(
          createLinearGradient(transformX, transformY, radius, fillColor, strokeColor));
    }
    Paint borderPaint = new Paint();
    borderPaint.setColor(strokeColor);
    borderPaint.setStyle(Paint.Style.STROKE);
    borderPaint.setAntiAlias(true);
    borderPaint.setStrokeWidth((float) strokeWidth);
    canvas.drawCircle(transformX, transformY, (float) radius, fillPaint);
    canvas.drawCircle(transformX, transformY, (float) radius, borderPaint);
  }

  public void drawPlayerCircle(double x, double y, double radius, double strokeWidth,
                                int playerIndex, Canvas canvas) {
    drawCircle(x, y, radius, getPlayerColor(playerIndex, Color.WHITE),
        getComplementaryPlayerColor(playerIndex, Color.BLACK), true, strokeWidth, canvas);
  }

  private void drawOctangle(double x, double y, double radius, int fillColor, int strokeColor,
                            boolean applyGradient, double strokeWidth, Canvas canvas) {
    float transformX = transformX(x);
    float transformY = transformY(y);
    Paint fillPaint = null;
    if (fillColor != EMPTY_COLOR) {
      fillPaint = new Paint();
      fillPaint.setColor(fillColor);
      fillPaint.setAlpha(Color.alpha(fillColor));
      fillPaint.setStyle(Paint.Style.FILL);
      if (applyGradient) {
        fillPaint.setShader(
            createLinearGradient(transformX, transformY, radius, fillColor, strokeColor));
      }
    }
    Paint borderPaint = new Paint();
    borderPaint.setColor(strokeColor);
    borderPaint.setStyle(Paint.Style.STROKE);
    borderPaint.setStrokeWidth((float) strokeWidth);
    borderPaint.setAntiAlias(true);

    final Path octangleShape = new Path();
    octangleShape.moveTo(
        (float) (transformX + radius * Math.cos(OCTANGLE_ANGLES[OCTANGLE_ANGLES.length - 1])),
        (float) (transformY + radius * Math.sin(OCTANGLE_ANGLES[OCTANGLE_ANGLES.length - 1])));
    for (double angle : OCTANGLE_ANGLES) {
      octangleShape.lineTo(
          (float) (transformX + radius * Math.cos(angle)),
          (float) (transformY + radius * Math.sin(angle)));
    }

    if (fillPaint != null) {
      canvas.drawPath(octangleShape, fillPaint);
    }
    canvas.drawPath(octangleShape, borderPaint);
  }

  public void drawPlayerOctangle(double x, double y, double radius, double strokeWidth,
                                  int playerIndex, Canvas canvas) {
    int fillColor = getPlayerColor(playerIndex, Color.WHITE);
    int strokeColor = getComplementaryPlayerColor(playerIndex, Color.BLACK);
    drawOctangle(x, y, radius, fillColor, strokeColor, true, strokeWidth, canvas);
  }

  private void drawStar(double x, double y, double radius, int fillColor,
                        int strokeColor, double strokeWidth, Canvas canvas) {
    Paint fillPaint = null;
    if (fillColor != EMPTY_COLOR) {
      fillPaint = new Paint();
      fillPaint.setColor(fillColor);
      fillPaint.setAlpha(Color.alpha(fillColor));
      fillPaint.setStyle(Paint.Style.FILL);
    }
    Paint borderPaint = new Paint();
    borderPaint.setColor(strokeColor);
    borderPaint.setStyle(Paint.Style.STROKE);
    borderPaint.setStrokeWidth((float) strokeWidth);
    borderPaint.setAntiAlias(true);

    final Path pentagramShape = new Path();
    pentagramShape.moveTo(
        (float) (transformX(x) + radius * 0.382 * Math.cos(PENTAGRAM_ANGLES[PENTAGRAM_ANGLES.length - 1])),
        (float) (transformY(y) + radius * 0.382 * Math.sin(PENTAGRAM_ANGLES[PENTAGRAM_ANGLES.length - 1])));
    boolean isOuterRadius = true;
    for (double angle : PENTAGRAM_ANGLES) {
      pentagramShape.lineTo(
          (float) (transformX(x) + (isOuterRadius ? 1.0 : 0.382) * radius * Math.cos(angle)),
          (float) (transformY(y) + (isOuterRadius ? 1.0 : 0.382) * radius * Math.sin(angle)));
      isOuterRadius = !isOuterRadius;
    }

    if (fillPaint != null) {
      canvas.drawPath(pentagramShape, fillPaint);
    }
    canvas.drawPath(pentagramShape, borderPaint);
  }

  public void drawEmptyCircle(double x, double y, double radius, int color, double strokeWidth,
                               Canvas canvas) {
    Paint borderPaint = new Paint();
    borderPaint.setColor(color);
    borderPaint.setStyle(Paint.Style.STROKE);
    borderPaint.setAntiAlias(true);
    borderPaint.setStrokeWidth((float) strokeWidth);
    canvas.drawCircle(transformX(x), transformY(y), (float) radius, borderPaint);
  }

  public void drawPlayerCenteredText(double x, double y, String text, double size, int playerIndex,
                                      boolean primaryColor, Canvas canvas) {
    final int color;
    if (primaryColor) {
      color = getPlayerColor(playerIndex, Color.WHITE);
    } else {
      color = getComplementaryPlayerColor(playerIndex, Color.BLACK);
    }
    drawCenteredText(x, y, text, size, color, canvas);
  }

  public void drawCenteredText(double x, double y, String text, double size, int color,
                                Canvas canvas) {
    drawText(x, y, text, size, Paint.Align.CENTER, Typeface.defaultFromStyle(Typeface.NORMAL),
        color, canvas);
  }

  public void drawText(double x, double y, String text, double size, Paint.Align align,
                        Typeface typeface, int color, Canvas canvas) {
    Paint paint = new Paint();
    paint.setTypeface(typeface);
    paint.setColor(color);
    paint.setTextSize((float) size);
    paint.setTextAlign(align);
    paint.setAntiAlias(true);
    float translatedX = transformX(x);
    float translatedY = transformY(y) - (paint.descent() + paint.ascent()) / 2;
    canvas.drawText(text, translatedX, translatedY, paint);
  }

  // ******************************************* ROAD ******************************************* //

  /**
   * Draws a RoadSection.Half with given parameters.
   * @param roadSection the RoadSection
   * @param end the part of roadSection to draw
   * @param color the Color
   * @param strokeWidth the width of the stroke
   * @param pathEffect the PathEffect
   * @param canvas the Canvas to draw on
   */
  private void drawRoadSectionHalfView(RoadSectionView roadSection,
                                       RoadSectionView.Direction end, int color,
                                       float strokeWidth, PathEffect pathEffect, Canvas canvas) {
    // Coordinates.
    final int index;
    if (end == RoadSectionView.Direction.BACKWARD) {
      index = 0;
    } else {
      index = 1;
    }
    final JointView[] joints = roadSection.getJoints();
    final PositionView midpoint = roadSection.getPositions()[roadSection.getMidpointIndex()];
    final Point2D.Double startPoint;
    final Point2D.Double endPoint;
    if (end == RoadSectionView.Direction.BACKWARD) {
      startPoint = joints[0].getCoordinatesPair();
      endPoint = midpoint.getCoordinatesPair();
    } else {
      startPoint = joints[1].getCoordinatesPair();
      endPoint = midpoint.getCoordinatesPair();
    }

    // Paint.
    final Paint paint = new Paint();
    paint.setColor(color);
    paint.setStrokeWidth(strokeWidth);
    paint.setAntiAlias(true);
    if (pathEffect != null) {
      paint.setPathEffect(pathEffect);
    }

    // Draw.
    drawBottomLine(startPoint.getX(), startPoint.getY(), endPoint.getX(), endPoint.getY(), paint,
        canvas);
    final float roadMidpointRadius = getDimension(R.dimen.road_midpoint_radius);
    drawCircle(endPoint.getX(), endPoint.getY(), roadMidpointRadius, color, color, false,
        roadMidpointRadius, canvas);
  }

  @Override
  public void onRoadSectionHalfVisible(@NonNull RoadSectionView roadSectionView,
                                       @NonNull RoadSectionView.Direction direction) {
    mBoardDrawingTool.drawRoadSectionHalfView(roadSectionView, direction, Color.BLACK,
        getDimension(R.dimen.visible_road_width), null);
  }

  @Override
  public void onRoadSectionHalfDiscovered(@NonNull RoadSectionView roadSectionView,
                                          @NonNull RoadSectionView.Direction direction) {
    mBoardDrawingTool.drawRoadSectionHalfView(roadSectionView, direction, Color.BLACK,
        getDimension(R.dimen.discovered_road_width), new DashPathEffect(new float[]{
            getDimension(R.dimen.road_dash_length), getDimension(R.dimen.road_dash_length)}, 0));
  }

  @Override
  public void onRoadSectionHalfInvisible(@NonNull RoadSectionView roadSectionView,
                                         @NonNull RoadSectionView.Direction direction) {

  }

  public void closePrawnActivity() {
    mInputManager.onPrawnViewClosed();
  }

  private void drawWarriorShape(double x, double y, double radius, int fillColor, int strokeColor,
                                boolean applyGradient, double strokeWidth, double angle,
                                Canvas canvas) {
    float transformX = transformX(x);
    float transformY = transformY(y);
    Paint fillPaint = new Paint();
    fillPaint.setColor(fillColor);
    fillPaint.setStyle(Paint.Style.FILL);
    if (applyGradient) {
      fillPaint.setShader(
          createLinearGradient(transformX, transformY, radius, fillColor, strokeColor));
    }
    Paint borderPaint = new Paint();
    borderPaint.setColor(strokeColor);
    borderPaint.setStyle(Paint.Style.STROKE);
    borderPaint.setStrokeWidth((float) strokeWidth);
    borderPaint.setAntiAlias(true);

    final double extraAngleDegree = 40;
    final double extraAngle = extraAngleDegree * Math.PI / 180;
    final Path warriorShape = new Path();
    warriorShape.addArc(
        (float) (transformX - radius), (float) (transformY - radius),
        (float) (transformX + radius), (float) (transformY + radius),
        (float) (90 - extraAngleDegree + angle * 180 / Math.PI),
            (float) (180 + 2 * extraAngleDegree));
    warriorShape.lineTo(
        (float) (transformX + radius * 1.5 * Math.cos(angle)),
        (float) (transformY + radius * 1.5 * Math.sin(angle)));
    warriorShape.lineTo(
        (float) (transformX + radius * Math.cos(Math.PI / 2 - extraAngle + angle)),
        (float) (transformY + radius * Math.sin(Math.PI / 2 - extraAngle + angle)));

    canvas.drawPath(warriorShape, fillPaint);
    canvas.drawPath(warriorShape, borderPaint);
  }

  public void drawEmptyWarriorShape(double x, double y, double radius, int color,
                                    double strokeWidth, double angle, Canvas canvas) {
    float transformX = transformX(x);
    float transformY = transformY(y);
    Paint borderPaint = new Paint();
    borderPaint.setColor(color);
    borderPaint.setStyle(Paint.Style.STROKE);
    borderPaint.setStrokeWidth((float) strokeWidth);
    borderPaint.setAntiAlias(true);

    final double extraAngleDegree = 40;
    final double extraAngle = extraAngleDegree * Math.PI / 180;
    final Path warriorShape = new Path();
    warriorShape.addArc(
        (float) (transformX - radius), (float) (transformY - radius),
        (float) (transformX + radius), (float) (transformY + radius),
        (float) (90 - extraAngleDegree + angle * 180 / Math.PI),
        (float) (180 + 2 * extraAngleDegree));
    warriorShape.lineTo(
        (float) (transformX + radius * 1.5 * Math.cos(angle)),
        (float) (transformY + radius * 1.5 * Math.sin(angle)));
    warriorShape.lineTo(
        (float) (transformX + radius * Math.cos(Math.PI / 2 - extraAngle + angle)),
        (float) (transformY + radius * Math.sin(Math.PI / 2 - extraAngle + angle)));

    canvas.drawPath(warriorShape, borderPaint);
  }


  /**
   * Draws the shape of a warrior for Player with index playerIndex, with a given radius and angle.
   * @param x the X coordinate of the center of the shape
   * @param y the Y coordinate of the center of the shape
   * @param playerIndex the index of the Player
   * @param radius the radius of the Warrior shape
   * @param strokeWidth the width of the shape stroke
   * @param angle the angle of the Warrior shape, in radians
   * @param canvas the Canvas to draw the Shape on
   */
  public void drawPlayerWarriorShape(double x, double y, int playerIndex, double radius,
                                      double strokeWidth, double angle, Canvas canvas) {
    drawWarriorShape(x, y, radius, getPlayerColor(playerIndex, Color.WHITE),
        getComplementaryPlayerColor(playerIndex, Color.BLACK), true, strokeWidth, angle, canvas);
  }

  @Override
  public void onPrawnVisible(@NonNull PrawnView prawn) {
    final float radius = getDimension(R.dimen.prawn_radius);
    final Point2D.Double coord = prawn.getCurrentPosition().getCoordinatesPair();
    final int playerIndex = prawn.getPlayer().getIndex();
    final int prawnEnergy = (int) Math.ceil(prawn.getEnergy() * 10);
    final Object prawnShape;
    if (prawn.getType().equals(PrawnView.Type.WARRIOR)) {
      mPrawnDrawingTool.drawPlayerWarriorShape(coord.getX(), coord.getY(), playerIndex, radius,
          getDimension(R.dimen.prawn_border_width), prawn.getWarriorDirection());
      prawnShape =
          mPrawnDrawingTool.getDrawPlayerWarriorShape().get(mPrawnDrawingTool.getDrawPlayerWarriorShape().size() - 1);
    } else {
      mPrawnDrawingTool.drawPlayerCircle(coord.getX(), coord.getY(), radius,
          getDimension(R.dimen.prawn_border_width), playerIndex);
      prawnShape =
          mPrawnDrawingTool.getDrawPlayerCircle().get(mPrawnDrawingTool.getDrawPlayerCircle().size() - 1);
    }
    if (prawn.getPlayer().getIndex() == mInputManager.getGame().getCurrentPlayerIndex()) {
      mPrawnDrawingTool.drawPlayerCenteredText(coord.getX(), coord.getY(), "" + prawnEnergy, radius,
          playerIndex, false, prawnShape);
    }
  }

  @Override
  public void onPrawnInvisible(@NonNull PrawnView prawnView) {

  }

  @Override
  public void onPrawnJustRemoved(@NonNull PrawnView prawn) {
    final float radius = getDimension(R.dimen.prawn_radius);
    final Point2D.Double coord = prawn.getCurrentPosition().getCoordinatesPair();
    final int playerIndex = prawn.getPlayer().getIndex();
    if (prawn.getType().equals(PrawnView.Type.WARRIOR)) {
      mHighlightDrawingTool.drawWarriorShape(coord.getX(), coord.getY(), radius,
          getPlayerColor(prawn.getPlayer().getIndex(), Color.WHITE),
          getComplementaryPlayerColor(prawn.getPlayer().getIndex(), Color.BLACK), true,
          getDimension(R.dimen.prawn_border_width), prawn.getWarriorDirection());
      mHighlightDrawingTool.drawEmptyWarriorShape(coord.getX(), coord.getY(), radius,
          getColor(R.color.colorStrongWarning),
          getDimension(R.dimen.highlighted_prawn_border_width), prawn.getWarriorDirection());
    } else {
      mHighlightDrawingTool.drawCircle(coord.getX(), coord.getY(), radius,
          getPlayerColor(prawn.getPlayer().getIndex(), Color.WHITE),
          getComplementaryPlayerColor(prawn.getPlayer().getIndex(), Color.BLACK), true,
          getDimension(R.dimen.prawn_border_width));
      mHighlightDrawingTool.drawEmptyCircle(coord.getX(), coord.getY(), radius,
          getColor(R.color.colorStrongWarning),
          getDimension(R.dimen.highlighted_prawn_border_width));
    }
  }

  public void confirmPrawnItinerary() {
    mInputManager.onPrawnItineraryBuildConfirmed();
  }

  public void clearPrawnItinerary() {
    mInputManager.onPrawnItineraryCleared();
  }

  public void abandonPrawnItinerary() {
    mInputManager.onPrawnItineraryBuildAbandoned();
  }

  public void setGrowCity() {
    mInputManager.onSetGrowCity();
  }

  @Override
  public void onCityDiscovered(@NonNull CityView cityView) {
    final float radius = getDimension(R.dimen.city_radius);
    final Point2D.Double coord = cityView.getJoint().getCoordinatesPair();
    int playerIndex = cityView.getPlayer().getIndex();
    mCityDrawingTool.drawPlayerOctangle(coord.getX(), coord.getY(), radius,
        getDimension(R.dimen.city_border_width), playerIndex);
    if (cityView.getPlayer().getIndex() == mInputManager.getGame().getCurrentPlayerIndex()) {
      mCityDrawingTool.drawPlayerCenteredText(coord.getX(),
          coord.getY(), "" + cityView.getFactories().size(), radius, playerIndex, false, null);
    }
  }

  @Override
  public void onCityInvisible(@NonNull CityView cityView) {

  }

  @Override
  public void onCityDestroyed(@NonNull CityView cityView) {

  }

  @Override
  public void onBattleVisible(@NonNull BattleView battle) {
    final double prawnRadius = getDimension(R.dimen.prawn_radius);
    final Point2D.Double point = battle.getPoint();
    mBattleDrawingTool.drawStar(point.getX(), point.getY(), 1.6 * prawnRadius,
        getColor(R.color.colorWarning), getColor(R.color.colorStrongWarning),
        getDimension(R.dimen.prawn_border_width));
  }

  @Override
  public void onBattleInvisible(@NonNull BattleView battle) {

  }

  @Override
  public void onMagnifiedCityDrawn(@NonNull CityView cityView, @NonNull Point2D.Double point) {
    mDistinctiveCities.put(cityView, point);
  }

  @Override
  public void onMagnifiedPrawnDrawn(@NonNull PrawnView prawnView, @NonNull Point2D.Double point,
                                    int index) {
    mDistinctivePrawns.put(prawnView,
        new Point2D.Double(point.getX() + index * 1.0, point.getY() + index * 1.0));
  }

  @Override
  public void onMagnifiedPositionDrawn(@NonNull PositionView positionView,
                                       @NonNull Point2D.Double point) {
    mDistinctivePositions.put(positionView, point);
  }

  public void showCityCreationPrompt() {
    mBoardFragment.showCityCreationPrompt();
  }

  public void setCityCreationDefaultName() {
    mBoardFragment.setCityCreationDefaultName(
        mInputManager.getPrawnSelected().getCurrentPosition().getJoint().getName());
  }

  public void startCityCreation() {
    mInputManager.onCityCreationStarted();
  }

  public void confirmCityCreation() {
    mInputManager.onCityCreationConfirmed(mBoardFragment.getCityCreationName());
  }

  public void abandonCityCreation() {
    mInputManager.onCityCreationAbandoned();
  }

  @Override
  public void onCityCreationHighlighted(@NonNull PositionView position, @NonNull String name) {
    mCityToCreatePositions.add(position.getCoordinatesPair());
    mCityToCreateNames.add(name);
    final Point2D.Double coord = position.getCoordinatesPair();
    final double radius = getDimension(R.dimen.city_radius);
    mHighlightDrawingTool.drawOctangle(coord.getX(), coord.getY(), radius,
        getColor(R.color.colorTransparentAccent), getColor(R.color.colorAccent), false,
        getDimension(R.dimen.city_border_width));
    mHighlightDrawingTool.drawCityDescription(coord, radius, name);
  }

  @Override
  public void onSelectableLineSectionHighlighted(@NonNull PositionView start,
                                                 @NonNull PositionView end) {
    final Paint paint = new Paint();
    paint.setColor(getColor(R.color.colorSelectableRoadHighlight));
    paint.setStrokeWidth(getDimension(R.dimen.visible_road_width));
    paint.setAntiAlias(true);
    mHighlightDrawingTool.drawHighlightLine(start.getCoordinatesPair().getX(),
        start.getCoordinatesPair().getY(), end.getCoordinatesPair().getX(),
        end.getCoordinatesPair().getY(), paint);
  }

  @Override
  public void onLineSectionHighlighted(@NonNull PositionView start,
                                       @NonNull PositionView end) {
    final Paint paint = new Paint();
    paint.setColor(getColor(R.color.colorRoadHighlight));
    paint.setStrokeWidth(getDimension(R.dimen.highlighted_road_width));
    paint.setAntiAlias(true);
    mHighlightDrawingTool.drawHighlightLine(start.getCoordinatesPair().getX(),
        start.getCoordinatesPair().getY(), end.getCoordinatesPair().getX(),
        end.getCoordinatesPair().getY(), paint);
    mHighlightDrawingTool.drawCircle(end.getCoordinatesPair().getX(), end.getCoordinatesPair().getY(),
        getDimension(R.dimen.highlighted_road_width), getColor(R.color.colorRoadHighlight),
        getColor(R.color.colorRoadHighlight), false, getDimension(R.dimen.visible_road_width));
  }

  @Override
  public synchronized void onHighlightingRemoved() {
    mHighlightDrawingTool.clear();
//    if (mFinishedHighlightBitmap != null && !mFinishedHighlightBitmap.isRecycled()) {
//      mFinishedHighlightBitmap.recycle();
//    }
//    mFinishedHighlightBitmap = Bitmap.createBitmap(mBoardBackgroundBitmap.getWidth(),
//        mBoardBackgroundBitmap.getHeight(), Bitmap.Config.ARGB_4444);
//    mHighlightCanvas = new Canvas(mFinishedHighlightBitmap);
    mCityFactoryHighlightedIndex = -1;
    mCityToCreatePositions = new LinkedList<>();
    mCityToCreateNames = new LinkedList<>();
    invalidateViews();
  }

  @Override
  public void onMagnificationShown(@NonNull Point2D.Double point) {
    mMagnifyCenterX = transformX(point.getX());
    mMagnifyCenterY = transformY(point.getY());
    mIsMagnifyOn = true;
    mDistinctivePrawns = new HashMap<>();
    mDistinctiveCities = new HashMap<>();
    mDistinctivePositions = new HashMap<>();
  }

  public void closeMagnification() {
    mInputManager.onMagnificationOff();
  }

  @Override
  public void onMagnificationHidden() {
    mIsMagnifyOn = false;
  }

  public void setProduceSettlersUnit() {
    mInputManager.onSetProduceSettlersUnit();
  }

  public void setProduceWarrior() {
    mInputManager.onSetProduceWarrior(mWarriorDirection);
  }

  public void closeProduceWarrior() {
    mInputManager.onWarriorDirectionSelectionOff();
  }

  public void closePrawnSelection() {
    mInputManager.onPrawnSelectionOff();
  }

  @Override
  public void onPrawnSelectionShown() {
    mPrawnSelection = new PrawnView[mCityViewPrawnSelectionWidth][mCityViewPrawnSelectionHeight];
    mIsPrawnSelectionViewOn = true;
  }

  @Override
  public void onPrawnSelectionHidden() {
    mIsPrawnSelectionViewOn = false;
  }

  @Override
  public void onPrawnSelectionPrawnDrawn(@NonNull PrawnView prawn, @NonNull Point2D.Double point) {
    final int x = (int) Math.floor(point.getX() / mCityViewPrawnBoxSide);
    final int y = (int) Math.floor(point.getY() / mCityViewPrawnBoxSide);
    mPrawnSelection[x][y] = prawn;
  }

  @Override
  public void onPrawnSelectionLeftSlider(@NonNull Point2D.Double aDouble, int i) {

  }

  @Override
  public void onPrawnSelectionRightSlider(@NonNull Point2D.Double aDouble, int i) {

  }

  public void selectPrawnSelectionPoint(Point2D.Double point) {
    if (mInputManager.getState().equals(InputManager.State.PRAWN_SELECTION)) {
      mInputManager.onPrawnSelectionPointSelected(point);
    }
  }

  public void closeCityView() {
    mInputManager.onCityViewOff();
  }

  @Override
  public void onCityViewShown() {
    mIsCityViewOn = true;
  }

  @Override
  public void onCityViewHidden() {
    mIsCityViewOn = false;
  }

  public void selectCityFactoryPoint(Point2D.Double point) {
    if (mInputManager.getState().equals(InputManager.State.CITY)) {
      mInputManager.onCityFactoryPointSelected(point);
    }
  }

  public void closeCityFactoryView() {
    mInputManager.onCityFactoryViewOff();
  }

  @Override
  public void onCityFactoryShown(@NonNull FactoryView factoryView, int i) {

  }

  @Override
  public void onCityFactoryUpperSliderShown(int i) {

  }

  @Override
  public void onCityFactoryLowerSliderShown(int i) {

  }

  @Override
  public void onCityFactoryHighlighted(int index) {
    mCityFactoryHighlightedIndex = index;
    invalidateViews();
  }

  public void selectCityPrawnSelectionPoint(Point2D.Double point) {
    if (mInputManager.getState().equals(InputManager.State.CITY)) {
      mInputManager.onCityPrawnSelectionPointSelected(point);
    }
  }

  @Override
  public void onCityPrawnSelectionShown() {
    mCityViewPrawnSelection =
        new PrawnView[mCityViewPrawnSelectionWidth][mCityViewPrawnSelectionHeight];
  }

  @Override
  public void onCityPrawnSelectionPrawnDrawn(@NonNull PrawnView prawn,
                                             @NonNull Point2D.Double point) {
    final int x = (int) Math.floor(point.getX() / mCityViewPrawnBoxSide);
    final int y = (int) Math.floor(point.getY() / mCityViewPrawnBoxSide);
    mCityViewPrawnSelection[x][y] = prawn;
  }

  @Override
  public void onCityPrawnSelectionLeftSlider(@NonNull Point2D.Double aDouble, int i) {

  }

  @Override
  public void onCityPrawnSelectionRightSlider(@NonNull Point2D.Double aDouble, int i) {

  }

  @Override
  public void onCityBoxDrawn(@NonNull CityView cityView, @NonNull int[] cityBoxCapturedHalves,
                             double cityProductivity) {
    mCityBoxCapturedHalves = cityBoxCapturedHalves;
    mCityProductivity = cityProductivity;
    invalidateViews();
  }

  public void showWarriorDirectionSelection() {
    mInputManager.onWarriorDirectionSelectionShown();
  }

  @Override
  public void onWarriorDirectionSelectionShown() {
    mWarriorDirection = -1.0;
    mIsWarriorDirectionSelectionOn = true;
  }

  @Override
  public void onWarriorDirectionSelectionHidden() {
    mIsWarriorDirectionSelectionOn = false;
  }

  public void showGameAbandoningPrompt() {
    mInputManager.onGameAbandoningPromptShown();
  }

  public void abandonGame() {
    mInputManager.onGameAbandoned();
  }

  public void forceQuitGame() {
    mIsGameAbandoningPromptShown = false;
    mOwnConnectionUp = true;
    mOthersConnectionUp = true;
    mOwnConnectionUpSwitched = true;
    mOthersConnectionUpSwitched = true;
    if (mInputManager != null) {
      mInputManager.onGameForceQuit();
    } else {
      onGameForceQuit();
    }
  }

  public void closeGameAbandoningPrompt() {
    mInputManager.onGameAbandoningPromptOff();
    invalidateViews();
  }

  @Override
  public void onGameAbandoningPromptShown() {
  }

  @Override
  public void onGameAbandoningPromptHidden() {
    mIsGameAbandoningPromptShown = false;
  }

  @Override
  public void onGameAbandoned() {
    mIsGameAbandoningPromptShown = false;
    mIsGameAbandoningPromptShown = false;
    mGoP.releaseUiSemaphore();
  }

  @Override
  public void onGameForceQuit() {
    final InputManager.State oldState;
    if (mInputManager != null) {
      oldState = mInputManager.getState();
    } else {
      oldState = null;
    }
    try {
      if (oldState != null) {
        mInputManager.reset();
        onStateChange(oldState);
      }
      mGameManager.notifyForceQuit();
      mGameManager.reset(oldState == InputManager.State.NO_GAME);
    } catch (ClientViewException exception) {
      throw new RuntimeException(exception);
    }
    if (oldState != InputManager.State.NO_GAME) {
      mGoP.releaseUiSemaphore();
    }
  }

  public void finishGame() throws ClientViewException {
    final InputManager.State oldState = mInputManager.getState();
    mInputManager.reset();
    onStateChange(oldState);
    mGameManager.reset(false);
  }

  public void showPrawnRepairSelection() {
    mInputManager.onPrawnRepairSelectionShown();
  }

  public void setRepairPrawn() {
    mInputManager.onSetRepairPrawn();
  }

  public void selectPrawnRepairSelectionPoint(Point2D.Double point) {
    mInputManager.onPrawnRepairSelectionPointSelected(point);
  }

  public void closeRepairSelection() {
    mInputManager.onPrawnRepairSelectionOff();
  }

  @Override
  public void onPrawnRepairSelectionShown() {
    mPrawnSelectedForRepairX = -1;
    mPrawnSelectedForRepairY = -1;
  }

  @Override
  public void onPrawnRepairSelectionHighlighted(int indexX, int indexY) {
    mPrawnSelectedForRepairX = indexX;
    mPrawnSelectedForRepairY = indexY;
  }

  @Override
  public void onPrawnRepairSelectionHidden() {
    mPrawnSelectedForRepairX = -1;
    mPrawnSelectedForRepairY = -1;
  }

  @Override
  public void setGameManager(@NonNull GameManager gameManager) {
    mGameManager = gameManager;
  }

  @Override
  public void notifyOwnGameServerConnection(boolean connected) throws ClientViewException {
    mOwnConnectionUp = connected;
    mOwnConnectionUpSwitched = false;
    invalidateViews();
  }

  @Override
  public void notifyOthersGameServerConnection(boolean connected) throws ClientViewException {
    mOthersConnectionUp = connected;
    mOthersConnectionUpSwitched = false;
    invalidateViews();
  }

  @Override
  public void notifyGameUndecided() throws ClientViewException {
    if (mInputManager.getState() != InputManager.State.NO_GAME) {
      if (mStatusBarFragment != null) {
        mStatusBarFragment.forceQuitGameWithInfoBox();
      } else {
        Log.e("AndroidEngine", "StatusBarFragment is null. Quitting without info box.");
        forceQuitGame();
      }
    }
  }

  @Override
  @Deprecated
  public Event[] interact(@NonNull Event[] events) throws ClientViewException {
    return null;
  }

  public synchronized void setBoardFragment(BoardFragment boardFragment) {
    mBoardFragment = boardFragment;
  }

  public void setStatusBarFragment(StatusBarFragment statusBarFragment) {
    mStatusBarFragment = statusBarFragment;
  }

  public synchronized void setControlPanelBoardMagnifiedFragment(
      ControlPanelBoardMagnifiedFragment controlPanelBoardMagnifiedFragment) {
    mControlPanelBoardMagnifiedFragment = controlPanelBoardMagnifiedFragment;
  }

  public void setControlPanelBoardPrawnFragment(
      ControlPanelBoardPrawnFragment controlPanelBoardPrawnFragment) {
    mControlPanelBoardPrawnFragment = controlPanelBoardPrawnFragment;
  }

  public void setControlPanelBoardInitializedFragment(
      ControlPanelBoardInitializedFragment controlPanelBoardInitializedFragment) {
    mControlPanelBoardInitializedFragment = controlPanelBoardInitializedFragment;
  }

  public void setControlPanelBoardBoardFragment(
      ControlPanelBoardBoardFragment controlPanelBoardBoardFragment) {
    mControlPanelBoardBoardFragment = controlPanelBoardBoardFragment;
  }

  public void setControlPanelBoardItineraryBuiltFragment(
      ControlPanelBoardItineraryBuiltFragment controlPanelBoardItineraryBuiltFragment) {
    mControlPanelBoardItineraryBuiltFragment = controlPanelBoardItineraryBuiltFragment;
  }

  public void setControlPanelBoardCityCreationFragment(
      ControlPanelBoardCityCreationFragment controlPanelBoardCityCreationFragment) {
    mControlPanelBoardCityCreationFragment = controlPanelBoardCityCreationFragment;
  }

  public void setControlPanelBoardCityFragment(
      ControlPanelBoardCityFragment controlPanelBoardCityFragment) {
    mControlPanelBoardCityFragment = controlPanelBoardCityFragment;
  }

  public void setControlPanelBoardFactoryFragment(
      ControlPanelBoardFactoryFragment controlPanelBoardFactoryFragment) {
    mControlPanelBoardFactoryFragment = controlPanelBoardFactoryFragment;
  }

  public void setControlPanelBoardWarriorDirectionSelectionFragment(
      ControlPanelBoardWarriorDirectionSelectionFragment
          controlPanelBoardWarriorDirectionSelectionFragment) {
    mControlPanelBoardWarriorDirectionSelectionFragment = controlPanelBoardWarriorDirectionSelectionFragment;
  }

  public void setControlPanelBoardPrawnSelectionFragment(
      ControlPanelBoardPrawnSelectionFragment controlPanelBoardPrawnSelectionFragment) {
    mControlPanelBoardPrawnSelectionFragment = controlPanelBoardPrawnSelectionFragment;
  }

  public void setControlPanelBoardPrawnRepairFragment(
      ControlPanelBoardPrawnRepairFragment controlPanelBoardPrawnRepairFragment) {
    mControlPanelBoardPrawnRepairFragment = controlPanelBoardPrawnRepairFragment;
  }

  public void setControlPanelBoardGameAbandoningFragment(
      ControlPanelBoardGameAbandoningFragment controlPanelBoardGameAbandoningFragment) {
    mControlPanelBoardGameAbandoningFragment = controlPanelBoardGameAbandoningFragment;
  }

  public void setControlPanelBoardGameFinishedFragment(
      ControlPanelBoardGameFinishedFragment controlPanelBoardGameFinishedFragment) {
    mControlPanelBoardGameFinishedFragment = controlPanelBoardGameFinishedFragment;
  }

  /**
   * Initializes the Bitmaps to draw on.
   */
  public synchronized void initializeDrawableBitmaps() {
    mBoardDrawingTool.clear();
    mCityDrawingTool.clear();
    mBattleDrawingTool.clear();
    mPrawnDrawingTool.clear();
    mHighlightDrawingTool.clear();
//    if (mWorkingBottomBitmap != null && !mWorkingBottomBitmap.isRecycled()) {
//      mWorkingBottomBitmap.recycle();
//    }
//    if (mFinishedBottomBitmap != null && !mFinishedBottomBitmap.isRecycled()) {
//      mFinishedBottomBitmap.recycle();
//    }
//    mWorkingBottomBitmap = Bitmap.createBitmap(mBoardBackgroundBitmap.getWidth(),
//        mBoardBackgroundBitmap.getHeight(), Bitmap.Config.ARGB_4444);
//    mBottomCanvas = new Canvas(mWorkingBottomBitmap);
//    mBottomCanvas.drawBitmap(mBoardBackgroundBitmap, 0, 0, null);
//
//    if (mFinishedHighlightBitmap != null && !mFinishedHighlightBitmap.isRecycled()) {
//      mFinishedHighlightBitmap.recycle();
//    }
//    mFinishedHighlightBitmap = Bitmap.createBitmap(mBoardBackgroundBitmap.getWidth(),
//        mBoardBackgroundBitmap.getHeight(), Bitmap.Config.ARGB_4444);
//    mHighlightCanvas = new Canvas(mFinishedHighlightBitmap);
//
//    if (mWorkingItemBitmap != null && !mWorkingItemBitmap.isRecycled()) {
//      mWorkingItemBitmap.recycle();
//    }
//    if (mFinishedItemBitmap != null && !mFinishedItemBitmap.isRecycled()) {
//      mFinishedItemBitmap.recycle();
//    }
//    mWorkingItemBitmap = Bitmap.createBitmap(mBoardBackgroundBitmap.getWidth(),
//        mBoardBackgroundBitmap.getHeight(), Bitmap.Config.ARGB_4444);
//    mItemCanvas = new Canvas(mWorkingItemBitmap);
  }

  /**
   * Returns the last bottom Bitmap prepared.
   * @return the last bottom Bitmap prepared
   */
//  public synchronized Bitmap getBottomBitmap() {
//    return mFinishedBottomBitmap;
//  }

  public synchronized Bitmap generateBottomBitmap() {
    if (mBoardBackgroundBitmap == null) {
      return null;
    }
    if (mSharedBitmap != null && !mSharedBitmap.isRecycled()) {
      if (!mBoardDrawingTool.isInvalidated()
          && !mCityDrawingTool.isInvalidated()
          && !mBattleDrawingTool.isInvalidated()
          && !mPrawnDrawingTool.isInvalidated()
          && !mHighlightDrawingTool.isInvalidated()) {
        return mSharedBitmap;
      }
      mSharedBitmap.recycle();
    }
    mSharedBitmap = Bitmap.createBitmap(mBoardBackgroundBitmap.getWidth(),
        mBoardBackgroundBitmap.getHeight(), Bitmap.Config.ARGB_4444);
    final Canvas canvas = new Canvas(mSharedBitmap);

    canvas.drawBitmap(mBoardBackgroundBitmap, 0, 0, null);
    for (DrawingTool drawingTool : new DrawingTool[]{mBoardDrawingTool, mCityDrawingTool,
        mBattleDrawingTool, mPrawnDrawingTool, mHighlightDrawingTool,}) {
      for (DrawingTool.DrawRoadSectionHalfView item : drawingTool.getDrawRoadSectionHalfView()) {
        drawRoadSectionHalfView(item.roadSection, item.end, item.color, item.strokeWidth,
            item.pathEffect, canvas);
      }
      for (DrawingTool.DrawStar item : drawingTool.getDrawStar()) {
        drawStar(item.x, item.y, item.radius, item.fillColor, item.strokeColor, item.strokeWidth,
            canvas);
      }
      for (DrawingTool.DrawPlayerWarriorShape item : drawingTool.getDrawPlayerWarriorShape()) {
        drawPlayerWarriorShape(item.x, item.y, item.playerIndex, item.radius, item.strokeWidth,
            item.angle, canvas);
        DrawingTool.DrawPlayerCenteredText description = drawingTool.getDescription(item);
        if (description != null) {
          drawPlayerCenteredText(description.x, description.y, description.text, description.size,
              description.playerIndex, description.primaryColor, canvas);
        }
      }
      for (DrawingTool.DrawPlayerCircle item : drawingTool.getDrawPlayerCircle()) {
        drawPlayerCircle(item.x, item.y, item.radius, item.strokeWidth, item.playerIndex, canvas);
        DrawingTool.DrawPlayerCenteredText description = drawingTool.getDescription(item);
        if (description != null) {
          drawPlayerCenteredText(description.x, description.y, description.text, description.size,
              description.playerIndex, description.primaryColor, canvas);
        }
      }
      for (DrawingTool.DrawWarriorShape item : drawingTool.getDrawWarriorShape()) {
        drawWarriorShape(item.x, item.y, item.radius, item.fillColor, item.strokeColor,
            item.applyGradient, item.strokeWidth, item.angle, canvas);
      }
      for (DrawingTool.DrawEmptyWarriorShape item : drawingTool.getDrawEmptyWarriorShape()) {
        drawEmptyWarriorShape(item.x, item.y, item.radius, item.color, item.strokeWidth, item.angle,
            canvas);
      }
      for (DrawingTool.DrawCircle item : drawingTool.getDrawCircle()) {
        drawCircle(item.x, item.y, item.radius, item.fillColor, item.strokeColor,
            item.applyGradient, item.strokeWidth, canvas);
      }
      for (DrawingTool.DrawPlayerOctangle item : drawingTool.getDrawPlayerOctangle()) {
        drawPlayerOctangle(item.x, item.y, item.radius, item.strokeWidth, item.playerIndex, canvas);
      }
      for (DrawingTool.DrawOctangle item : drawingTool.getDrawOctangle()) {
        drawOctangle(item.x, item.y, item.radius, item.fillColor, item.strokeColor,
            item.applyGradient, item.strokeWidth, canvas);
      }
      for (DrawingTool.DrawHighlightLine item : drawingTool.getDrawHighlightLine()) {
        drawHighlightLine(item.startX, item.startY, item.stopX, item.stopY, item.paint, canvas);
      }
      for (DrawingTool.DrawPlayerCenteredText item : drawingTool.getDrawPlayerCenteredText()) {
        drawPlayerCenteredText(item.x, item.y, item.text, item.size, item.playerIndex,
            item.primaryColor, canvas);
      }
      for (DrawingTool.DrawCityDescription item : drawingTool.getDrawCityDescription()) {
        drawCityDescription(item.coord, item.radius, item.cityName, canvas);
      }
    }

    final float cityRadius = getDimension(R.dimen.city_radius);
    // City Descriptions.
    for (PlayerView player : mGame.getPlayers()) {
      for (CityView city : mGame.getPlayerCities(player)) {
        if (!city.getVisibility().equals(CityView.Visibility.DISCOVERED)) break;
        final Point2D.Double coord = city.getJoint().getCoordinatesPair();
        drawCityDescription(coord, cityRadius, city.getName(), canvas);
      }
    }

    // City warning signs.
    while (mInputManager == null || mInputManager.getGame() == null) {
      try {
        Thread.sleep(100);
      } catch (InterruptedException e) {
        throw new RuntimeException(e);
      }
    }
    GameView game = mInputManager.getGame();
    PlayerView currentPlayer = game.getPlayers()[game.getCurrentPlayerIndex()];
    for (CityView city : mInputManager.getGame().getPlayerCities(currentPlayer)) {
      boolean isIdle = false;
      for (FactoryView factory : city.getFactories()) {
        if (factory.getState().equals(FactoryView.State.IDLE)) {
          isIdle = true;
          break;
        }
      }
      if (isIdle) {
        final Point2D.Double coord = city.getJoint().getCoordinatesPair();
        Point2D.Double iconStart =
            new Point2D.Double(
                (coord.getX() + TRANSLATE_X) * mAndroidMagnifyX + cityRadius * Math.sqrt(2) / 2 * 0.8,
                (coord.getY() + TRANSLATE_Y) * mAndroidMagnifyY + cityRadius * Math.sqrt(2) / 2 * 0.8);
        final Drawable warningIcon = getDrawable(R.drawable.ic_warning_black_80dp);
        warningIcon.setTint(getColor(R.color.colorWarning));
        warningIcon.setBounds((int) iconStart.getX(), (int) iconStart.getY(),
            (int) (iconStart.getX() + cityRadius),
            (int) (iconStart.getY() + cityRadius));
        warningIcon.draw(canvas);
      }
    }

    // Highlight Prawn selected.
    if (mInputManager.getState() == InputManager.State.PRAWN
        || mInputManager.getState() == InputManager.State.ITINERARY_BUILT
        || mInputManager.getState() == InputManager.State.MAGNIFIED) {
      final PrawnView prawnSelected = mInputManager.getPrawnSelected();
      if (prawnSelected != null) {
        final Point2D.Double prawnPosition =
            prawnSelected.getCurrentPosition().getCoordinatesPair();
        if (prawnSelected.getType() == PrawnView.Type.SETTLERS_UNIT) {
          drawCircle(prawnPosition.getX(), prawnPosition.getY(),
              getDimension(R.dimen.prawn_radius),
              getColor(R.color.colorTransparentAccent), getColor(R.color.colorAccent), false,
              getDimension(R.dimen.city_border_width), canvas);
        } else {
          drawWarriorShape(prawnPosition.getX(), prawnPosition.getY(),
              getDimension(R.dimen.prawn_radius),
              getColor(R.color.colorTransparentAccent), getColor(R.color.colorAccent), false,
              getDimension(R.dimen.city_border_width), prawnSelected.getWarriorDirection(), canvas);
        }
      }
    }

    mBoardDrawingTool.validate();
    mCityDrawingTool.validate();
    mBattleDrawingTool.validate();
    mPrawnDrawingTool.validate();
    mHighlightDrawingTool.validate();

    return mSharedBitmap;
  }

  /**
   * Returns the last highlight Bitmap prepared.
   * @return the last highlight Bitmap prepared
   */
//  public synchronized Bitmap getHighlightBitmap() {
//    return mFinishedHighlightBitmap;
//  }

  private void drawCityDescription(Point2D.Double coord, double radius, String cityName,
                                   Canvas canvas) {
    final int primaryColor = getColor(R.color.colorPrimary);
    final int foregroundColor = getColor(R.color.colorLightAccent);
    Point2D.Double textStart =
        new Point2D.Double(
            coord.getX() + radius / mAndroidMagnifyRadius * Math.sqrt(2) / 2,
            coord.getY() - radius / mAndroidMagnifyRadius);
    drawText(textStart.getX() + 0.75, textStart.getY() + 0.75, cityName, radius * 0.8,
        Paint.Align.LEFT, Typeface.defaultFromStyle(Typeface.NORMAL), primaryColor, canvas);
    drawText(textStart.getX(), textStart.getY(), cityName, radius * 0.8,
        Paint.Align.LEFT, Typeface.defaultFromStyle(Typeface.NORMAL), foregroundColor, canvas);
  }

  // Invalidates the underlying views.
  public void invalidateViews() {
    if (mBoardFragment != null) {
      mBoardFragment.invalidate();
    }
    if (mStatusBarFragment != null) {
      mStatusBarFragment.invalidate();
    }
    for (ControlPanelBoardFragment fragment : new ControlPanelBoardFragment[]{
        mControlPanelBoardInitializedFragment, mControlPanelBoardBoardFragment,
        mControlPanelBoardMagnifiedFragment, mControlPanelBoardPrawnFragment,
        mControlPanelBoardItineraryBuiltFragment, mControlPanelBoardCityCreationFragment,
        mControlPanelBoardCityFragment, mControlPanelBoardFactoryFragment,
        mControlPanelBoardWarriorDirectionSelectionFragment,
        mControlPanelBoardPrawnSelectionFragment, mControlPanelBoardPrawnRepairFragment,
        mControlPanelBoardGameAbandoningFragment, mControlPanelBoardGameFinishedFragment}) {
      if (fragment != null) {
        fragment.updateState();
      }
    }
  }

  public double getAndroidMagnifyRadius() {
    return mAndroidMagnifyRadius;
  }

  public double getAndroidMagnifyX() {
    return mAndroidMagnifyX;
  }

  public double getAndroidMagnifyY() {
    return mAndroidMagnifyY;
  }

  public float getRealBoardMinX() {
    return mRealBoardMinX;
  }

  public void setRealBoardMinX(float realBoardMinX) {
    this.mRealBoardMinX = realBoardMinX;
  }

  public float getRealBoardMinY() {
    return mRealBoardMinY;
  }

  public void setRealBoardMinY(float realBoardMinY) {
    this.mRealBoardMinY = realBoardMinY;
  }

  public String[] getPeriodicInfo() {
    return mPeriodicInfo;
  }

  public String getStateLabel() {
    return mStateLabel;
  }

  public String getTimeInfo() {
    return mTimeInfo;
  }

  public double getMagnifyCenterX() {
    return mMagnifyCenterX;
  }

  public double getMagnifyCenterY() {
    return mMagnifyCenterY;
  }

  public boolean isMagnifyOn() {
    return mIsMagnifyOn;
  }

  public Map<PrawnView, Point2D.Double> getDistinctivePrawns() {
    return mDistinctivePrawns;
  }

  public Map<CityView, Point2D.Double> getDistinctiveCities() {
    return mDistinctiveCities;
  }

  public Map<PositionView, Point2D.Double> getDistinctivePositions() {
    return mDistinctivePositions;
  }

  public boolean isCityViewOn() {
    return mIsCityViewOn;
  }

  public boolean isPrawnSelectionViewOn() {
    return mIsPrawnSelectionViewOn;
  }

  public PrawnView[][] getCityViewPrawnSelection() {
    return mCityViewPrawnSelection;
  }

  public PrawnView[][] getPrawnSelection() {
    return mPrawnSelection;
  }

  public int getPrawnSelectedForRepairX() {
    return mPrawnSelectedForRepairX;
  }

  public int getPrawnSelectedForRepairY() {
    return mPrawnSelectedForRepairY;
  }

  public CityView getCitySelected() {
    return mInputManager.getCitySelected();
  }

  public PrawnView getPrawnSelected() {
    return mInputManager.getPrawnSelected();
  }

  public int getCurrentPlayerIndex() { return mInputManager.getGame().getCurrentPlayerIndex(); }

  private PlayerView getCurrentPlayer() {
    return mInputManager.getGame().getPlayers()[getCurrentPlayerIndex()];
  }

  public boolean isWarriorDirectionSelectionOn() {
    return mIsWarriorDirectionSelectionOn;
  }

  public boolean isGameAbandoningPromptShown() {
    return mIsGameAbandoningPromptShown;
  }

  public double getWarriorDirection() {
    return mWarriorDirection;
  }

  public void setWarriorDirection(double warriorDirection) {
    mWarriorDirection = warriorDirection;
  }

  public InputManager.State getState() {
    return mInputManager.getState();
  }

  public int getCityFactoryHighlightedIndex() {
    return mCityFactoryHighlightedIndex;
  }

  public boolean isOwnConnectionUp() {
    return mOwnConnectionUp;
  }

  public boolean isOwnConnectionUpSwitched() {
    return mOwnConnectionUpSwitched;
  }

  public void setOwnConnectionUpSwitched() {
    mOwnConnectionUpSwitched = true;
  }

  public boolean isOthersConnectionUp() {
    return mOthersConnectionUp;
  }

  public boolean isOthersConnectionUpSwitched() {
    return mOthersConnectionUpSwitched;
  }

  public void setOthersConnectionUpSwitched() {
    mOthersConnectionUpSwitched = true;
  }

  public int[] getCityBoxCapturedHalves() {
    return mCityBoxCapturedHalves;
  }

  public double getCityProductivity() {
    return mCityProductivity;
  }

  public int getGameTutorialItemPos() {
    return mGameTutorialItemPos;
  }

  public void setGameTutorialItemPos(int gameTutorialItemPos) {
    mGameTutorialItemPos = gameTutorialItemPos;
  }

  public int getGameRulesItemPos() {
    return mGameRulesItemPos;
  }

  public void setGameRulesItemPos(int gameRulesItemPos) {
    mGameRulesItemPos = gameRulesItemPos;
  }

  public GameView getGame() {
    if (mInputManager == null) {
      return null;
    }
    return mInputManager.getGame();
  }

  /**
   * Initializes the Game viewport, if it hasn't been done before.
   * @param viewportWidth the X coordinate of the viewport rectangle
   * @param viewportHeight the Y coordinate of the viewport rectangle
   */
  public synchronized void tryInitializeGameViewport(int viewportWidth, int viewportHeight) {
    if (!mGameViewportInitialized) {
      final Point2D.Double initialScrollValue = mGame.calculateScrolledBoardBarInitialSetup(
          (int) Math.floor(viewportWidth / mAndroidMagnifyX),
          (int) Math.floor(viewportHeight / mAndroidMagnifyY));
      mRealBoardMinX =
          - (float) (((mGame.getXsize() + 2 * TRANSLATE_X) * mAndroidMagnifyX - viewportWidth)
              * initialScrollValue.getX());
      mRealBoardMinY =
          - (float) (((mGame.getYsize() + 2 * TRANSLATE_Y) * mAndroidMagnifyY - viewportHeight)
              * initialScrollValue.getY());
      mGameViewportInitialized = true;
    }
  }

  public static AndroidEngine create() {
    if (sAndroidEngine == null) {
      sAndroidEngine = new AndroidEngine();
    }
    return sAndroidEngine;
  }

  private AndroidEngine() {
    mGoP = GoP.get(null);

    // Read properties.
    mPropertiesReader = mGoP.getInjector().providePropertiesReader();
    try {
      mBoardProperties = new BoardProperties(mPropertiesReader.getBoardProperties());
    } catch (BoardException exception) {
      throw new IllegalArgumentException(exception);
    } catch (IOException exception) {
      throw new IllegalArgumentException(exception);
    }

    mTextManager = mGoP.getInjector().provideTextManager();
  }
}
