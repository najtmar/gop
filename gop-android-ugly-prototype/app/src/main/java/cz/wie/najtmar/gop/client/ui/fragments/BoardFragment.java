package cz.wie.najtmar.gop.client.ui.fragments;

import androidx.fragment.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import cz.wie.najtmar.gop.client.ui.AndroidEngine;
import cz.wie.najtmar.gop.client.ui.GoP;
import cz.wie.najtmar.gop.client.ui.R;
import cz.wie.najtmar.gop.client.ui.activities.BoardMagnifiedActivity;
import cz.wie.najtmar.gop.client.ui.views.BoardCityFactorySelectionView;
import cz.wie.najtmar.gop.client.ui.views.BoardMagnifiedView;
import cz.wie.najtmar.gop.client.ui.views.BoardPrawnSelectionView;
import cz.wie.najtmar.gop.client.ui.views.BoardView;
import cz.wie.najtmar.gop.client.ui.views.BoardWarriorDirectionSelectionView;
import cz.wie.najtmar.gop.client.view.CityView;

/**
 * Created by najtmar on 17.10.16.
 */

public class BoardFragment extends Fragment {

  // UI elements.
  private FrameLayout mBoardLayout;
  private BoardView mBoardView;
  private BoardMagnifiedView mBoardMagnifiedView;
  private BoardWarriorDirectionSelectionView mBoardWarriorDirectionSelectionView;
  private FrameLayout mBoardCityCreationView;
  private EditText mCityNameField;
  private LinearLayout mCityView;
  private BoardPrawnSelectionView mPrawnSelectionView;
  private BoardPrawnSelectionView mCityViewPrawnSelectionView;
  private BoardCityFactorySelectionView mBoardCityFactorySelectionView;
  private LinearLayout mGameAbandoningPrompt;

  /**
   * Central model object.
   */
  private GoP mGoP;

  /**
   * Shared instance of class AndroidEngine, which may call back on the fragment.
   */
  private AndroidEngine mAndroidEngine;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mGoP = GoP.get(getContext());
  }

  private void initializeBoardBackground() {
  }

  private void initializeControls(View view) {
    mBoardLayout = (FrameLayout) view.findViewById(R.id.game_board);
    mBoardView = (BoardView) view.findViewById(R.id.board_content);
    mBoardMagnifiedView = (BoardMagnifiedView) view.findViewById(R.id.board_magnified_content);
    mBoardWarriorDirectionSelectionView =
        (BoardWarriorDirectionSelectionView) view.findViewById(
            R.id.board_warrior_direction_selection_content);
    mCityNameField = (EditText) view.findViewById(R.id.board_city_creation_name_input);
    mBoardCityCreationView = (FrameLayout) view.findViewById(R.id.board_city_creation_content);
    mCityView = (LinearLayout) view.findViewById(R.id.city_view_content);
    mPrawnSelectionView = (BoardPrawnSelectionView) view.findViewById(R.id.prawn_selection_content);
    mCityViewPrawnSelectionView =
        (BoardPrawnSelectionView) view.findViewById(R.id.city_prawns_content);
    mBoardCityFactorySelectionView =
        (BoardCityFactorySelectionView) view.findViewById(R.id.city_factories_content);
    mGameAbandoningPrompt = (LinearLayout) view.findViewById(R.id.game_abandoning_content);
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.board, container, false);
    initializeBoardBackground();
    initializeControls(view);
    mAndroidEngine = (AndroidEngine) mGoP.getInjector().provideOutputEngine();

    return view;
  }

  @Override
  public void onResume() {
    super.onResume();
    mAndroidEngine.setBoardFragment(this);
    if (mAndroidEngine.isCityViewOn()) {
      mCityView.setVisibility(View.VISIBLE);
      CityView citySelected = mAndroidEngine.getCitySelected();
      if (citySelected != null) {
        ViewGroup.LayoutParams layoutParams = mBoardCityFactorySelectionView.getLayoutParams();
        layoutParams.height = (int) (citySelected.getFactories().size()
                * getResources().getDimension(R.dimen.city_factory_box_height));
        mBoardCityFactorySelectionView.setLayoutParams(layoutParams);
        mBoardLayout.invalidate();
      }
    } else {
      mCityView.setVisibility(View.INVISIBLE);
    }
    if (mAndroidEngine.isPrawnSelectionViewOn()) {
      mPrawnSelectionView.setVisibility(View.VISIBLE);
    } else {
      mPrawnSelectionView.setVisibility(View.INVISIBLE);
    }
    if (mAndroidEngine.isWarriorDirectionSelectionOn()) {
      mBoardWarriorDirectionSelectionView.setVisibility(View.VISIBLE);
    } else {
      mBoardWarriorDirectionSelectionView.setVisibility(View.INVISIBLE);
    }
    if (mAndroidEngine.isGameAbandoningPromptShown()) {
      mGameAbandoningPrompt.setVisibility(View.VISIBLE);
    } else {
      mGameAbandoningPrompt.setVisibility(View.INVISIBLE);
    }
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    // Don't detach the Fragment from AndroidEngine.
  }

  /**
   * Starts Activity BoardMagnifiedActivity .
   */
  public void startBoardMagnifiedActivity() {
    Intent intent = new Intent(getActivity(), BoardMagnifiedActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

  public void showCityCreationPrompt() {
    mBoardCityCreationView.setVisibility(View.VISIBLE);
  }

  public void setCityCreationDefaultName(String name) {
    mCityNameField.setText(name);
  }

  public String getCityCreationName() {
    return mCityNameField.getText().toString();
  }

  /**
   * Invalidates the underlying BoardView.
   */
  public void invalidate() {
    if (getActivity() != null) {
      getActivity().runOnUiThread(new Runnable() {
        @Override
        public void run() {
          mBoardView.invalidate();
          mBoardMagnifiedView.invalidate();
          mBoardWarriorDirectionSelectionView.invalidate();
          mBoardCityCreationView.invalidate();
          mPrawnSelectionView.invalidate();
          mCityViewPrawnSelectionView.invalidate();
        }
      });
    }
  }
}
