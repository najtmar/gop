package cz.wie.najtmar.gop.client.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import cz.wie.najtmar.gop.client.ui.AndroidGameManagementEngine;
import cz.wie.najtmar.gop.client.ui.AsyncGameManager;
import cz.wie.najtmar.gop.client.ui.GoP;
import cz.wie.najtmar.gop.client.ui.R;
import cz.wie.najtmar.gop.client.ui.activities.BoardInitializedActivity;
import cz.wie.najtmar.gop.client.ui.activities.InitialActivity;
import cz.wie.najtmar.gop.client.ui.activities.JoinActivity;
import cz.wie.najtmar.gop.server.KryoNetCommunicator;

/**
 * Created by najtmar on 11.10.16.
 */

public class GameJoinedFragment extends MenuFragment {

  // UI elements.
  private Button mGameJoinedCancelButton;

  /**
   * Central model object.
   */
  private GoP mGoP;

  /**
   * Game manager.
   */
  private AsyncGameManager mAsyncGameManager;

  /**
   * Shared instance of class mAndroidGameManagementEngine, which may call back on the fragment.
   */
  private AndroidGameManagementEngine mAndroidGameManagementEngine;

  @Override
  public synchronized void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mGoP = GoP.get(getContext());
    mAsyncGameManager = new AsyncGameManager(mGoP.getInjector().provideGameManager());
    mAndroidGameManagementEngine =
        (AndroidGameManagementEngine) mGoP.getInjector().provideGameManagementEngine();
    mAndroidGameManagementEngine.setGameJoinedFragment(this);
  }

  private void scheduleDelayedEnableCancel() {
    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {
        if (mGameJoinedCancelButton != null) {
          mGameJoinedCancelButton.setEnabled(true);
        }
      }
    }, KryoNetCommunicator.getConnectionTimeoutMillis());
  }


  private void initializeControls(View view) {
    mGameJoinedCancelButton = (Button) view.findViewById(R.id.game_joined_cancel);
    mGameJoinedCancelButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        mAsyncGameManager.asyncLeaveGameIntent();
        mGameJoinedCancelButton.setEnabled(false);
        scheduleDelayedEnableCancel();
      }
    });
  }

  @Nullable
  @Override
  public synchronized View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.activity_game_joined, container, false);
    initializeControls(view);

    return view;
  }

  public synchronized void initializeBoardInitialized() {
    Intent intent = new Intent(getActivity(), BoardInitializedActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

  public synchronized void startInitialActivity() {
    Intent intent = new Intent(getActivity(), InitialActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

  public synchronized void startJoinActivity() {
    Intent intent = new Intent(getActivity(), JoinActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

  public Button getGameJoinedCancelButton() {
    return mGameJoinedCancelButton;
  }
}
