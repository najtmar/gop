package cz.wie.najtmar.gop.board;

import javax.annotation.Nonnull;

import cz.wie.najtmar.gop.game.PropertiesReader;

/**
 * Provider for class MapBoardGenerator .
 */

public class MapBoardGeneratorProvider {

  public static BoardGenerator provide(@Nonnull PropertiesReader propertiesReader) {
    return new MapBoardGenerator(propertiesReader);
  }

}
