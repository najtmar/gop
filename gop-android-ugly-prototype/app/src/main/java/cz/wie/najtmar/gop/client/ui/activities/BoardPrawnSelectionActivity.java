package cz.wie.najtmar.gop.client.ui.activities;

import androidx.fragment.app.Fragment;

import cz.wie.najtmar.gop.client.ui.fragments.ControlPanelBoardPrawnSelectionFragment;

/**
 * Created by najtmar on 04.11.16.
 */

public class BoardPrawnSelectionActivity extends GameFragmentsActivity {

  @Override
  protected Fragment createControlPanelFragment() {
    return new ControlPanelBoardPrawnSelectionFragment();
  }
}
