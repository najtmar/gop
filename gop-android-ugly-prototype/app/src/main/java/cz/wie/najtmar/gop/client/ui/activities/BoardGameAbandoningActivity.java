package cz.wie.najtmar.gop.client.ui.activities;

import androidx.fragment.app.Fragment;

import cz.wie.najtmar.gop.client.ui.fragments.ControlPanelBoardBoardFragment;
import cz.wie.najtmar.gop.client.ui.fragments.ControlPanelBoardGameAbandoningFragment;

/**
 * Created by najtmar on 17.10.16.
 */

public class BoardGameAbandoningActivity extends GameFragmentsActivity {

  @Override
  protected Fragment createControlPanelFragment() {
    return new ControlPanelBoardGameAbandoningFragment();
  }
}
