package cz.wie.najtmar.gop.client.ui.fragments;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import cz.wie.najtmar.gop.client.ui.AndroidEngine;
import cz.wie.najtmar.gop.client.ui.GoP;
import cz.wie.najtmar.gop.client.ui.R;
import cz.wie.najtmar.gop.client.view.CityView;
import cz.wie.najtmar.gop.client.view.FactoryView;
import cz.wie.najtmar.gop.client.view.GameView;
import cz.wie.najtmar.gop.client.view.PlayerView;

/**
 * Created by najtmar on 17.10.16.
 */

public class StatusBarFragment extends MenuFragment {

  public static final String DIALOG_CONNECTION_BROKEN = "ConnectionBroken";
  public static final String DIALOG_FORCE_QUIT_GAME = "ForceQuitGame";
  public static final String DIALOG_FORCE_QUIT_INFO_GAME = "ForceQuitInfoGame";

  /**
   * Central model object.
   */
  private GoP mGoP;

  /**
   * Shared instance of class AndroidEngine, which may call back on the fragment.
   */
  private AndroidEngine mAndroidEngine;

  private TextView mStateLabel;
  private TextView mTimeInfo;
  private TextView mEventInfo;

  private LinearLayout mStatusBar;
  private LinearLayout mForceQuitButtonLayout;
  private LinearLayout mGlobalWarningSign;
  private LinearLayout mConnectionBrokenSign;
  private LinearLayout mConnectionProblemSign;

  private Button mForceQuitButton;

  private FragmentManager mFragmentManager;

  private volatile boolean mUnfolded;

  private ConnectionBrokenDialogFragment mOwnConnectionBrokenFragment;
  private ConnectionBrokenDialogFragment mOthersConnectionBrokenFragment;
  private ForceQuitGameDialogFragment mForceQuitGameDialogFragment;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    mGoP = GoP.get(getContext());
    mUnfolded = false;
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    final View view = inflater.inflate(R.layout.status_bar, container, false);
    mStateLabel = (TextView) view.findViewById(R.id.status_bar_state_label);
    mTimeInfo = (TextView) view.findViewById(R.id.status_bar_time);
    mEventInfo = (TextView) view.findViewById(R.id.status_bar_event_info);
    mStatusBar = (LinearLayout) view.findViewById(R.id.status_bar);
    mForceQuitButtonLayout = (LinearLayout) view.findViewById(R.id.force_quit_game_layout);
    mForceQuitButton = (Button) view.findViewById(R.id.force_quit_game);
    mGlobalWarningSign = (LinearLayout) view.findViewById(R.id.global_warning_sign);
    mConnectionBrokenSign = (LinearLayout) view.findViewById(R.id.connection_broken_sign);
    mConnectionProblemSign = (LinearLayout) view.findViewById(R.id.connection_problem_sign);
    mAndroidEngine = (AndroidEngine) mGoP.getInjector().provideOutputEngine();
    mAndroidEngine.setStatusBarFragment(this);
    mFragmentManager = getFragmentManager();

    mForceQuitButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        mForceQuitGameDialogFragment = ForceQuitGameDialogFragment.newInstance();
        mForceQuitGameDialogFragment.show(mFragmentManager, DIALOG_FORCE_QUIT_GAME);
      }
    });

    return view;
  }

  public void forceQuitGameWithInfoBox() {
    ForceQuitGameInfoDialogFragment.newInstance().show(mFragmentManager, DIALOG_FORCE_QUIT_INFO_GAME);
  }

  @Override
  public void onResume() {
    super.onResume();
    mAndroidEngine.setStatusBarFragment(this);
    updateContent();
  }

  private void updateContent() {
    // Global warning sign.
    boolean isCityIdle = false;
    final GameView game = mAndroidEngine.getGame();
    if (game != null) {
      final PlayerView currentPlayer = game.getPlayers()[game.getCurrentPlayerIndex()];
      isCityIdleLoop:
      for (CityView city : game.getPlayerCities(currentPlayer)) {
        for (FactoryView factory : city.getFactories()) {
          if (factory.getState().equals(FactoryView.State.IDLE)) {
            isCityIdle = true;
            break isCityIdleLoop;
          }
        }
      }
      if (isCityIdle) {
        mGlobalWarningSign.setVisibility(View.VISIBLE);
      } else {
        mGlobalWarningSign.setVisibility(View.GONE);
      }
    }

    // Connection problem sign.
    if (mAndroidEngine.isOwnConnectionUp()) {
      mConnectionBrokenSign.setVisibility(View.GONE);
    } else {
      mConnectionBrokenSign.setVisibility(View.VISIBLE);
    }
    if (mAndroidEngine.isOthersConnectionUp()) {
      mConnectionProblemSign.setVisibility(View.GONE);
    } else {
      mConnectionProblemSign.setVisibility(View.VISIBLE);
    }
    if (mAndroidEngine.isOwnConnectionUp() && mAndroidEngine.isOthersConnectionUp()) {
      mForceQuitButtonLayout.setVisibility(View.GONE);
    } else {
      mForceQuitButtonLayout.setVisibility(View.VISIBLE);
    }

    // Notification Dialogs .
    if (!mAndroidEngine.isOwnConnectionUpSwitched()) {
      if (!mAndroidEngine.isOwnConnectionUp()) {
        mOwnConnectionBrokenFragment =
            ConnectionBrokenDialogFragment.newInstance(
                ConnectionBrokenDialogFragment.Type.OWN_CONNECTION_BROKEN);
        mOwnConnectionBrokenFragment.show(mFragmentManager, DIALOG_CONNECTION_BROKEN);
      } else {
        if (mOwnConnectionBrokenFragment != null && mOwnConnectionBrokenFragment.getDialog() != null) {
          mOwnConnectionBrokenFragment.getDialog().dismiss();
          mOwnConnectionBrokenFragment = null;
        }
      }
      mAndroidEngine.setOwnConnectionUpSwitched();
    }
    if (!mAndroidEngine.isOthersConnectionUpSwitched()) {
      if (!mAndroidEngine.isOthersConnectionUp()) {
        mOthersConnectionBrokenFragment =
            ConnectionBrokenDialogFragment.newInstance(
                ConnectionBrokenDialogFragment.Type.OTHERS_CONNECTION_BROKEN);
        mOthersConnectionBrokenFragment.show(mFragmentManager, DIALOG_CONNECTION_BROKEN);
      } else {
        if (mOthersConnectionBrokenFragment != null && mOthersConnectionBrokenFragment.getDialog() != null) {
          mOthersConnectionBrokenFragment.getDialog().dismiss();
          mOthersConnectionBrokenFragment = null;
        }
      }
      mAndroidEngine.setOthersConnectionUpSwitched();
    }

    mStateLabel.setText(mAndroidEngine.getStateLabel());
    String[] periodicInfoArray = mAndroidEngine.getPeriodicInfo();
    StringBuilder timeInfoBuilder = new StringBuilder();
    timeInfoBuilder
        .append("<b>" + getResources().getString(R.string.date) + "</b>")
        .append(" ")
        .append(mAndroidEngine.getTimeInfo())
        .append(", ");
    if (periodicInfoArray != null && periodicInfoArray.length != 0) {
      timeInfoBuilder.append("<b>" + getResources().getString(R.string.recent_events) + "</b>");
    } else {
      timeInfoBuilder.append("<b>" + getResources().getString(R.string.no_recent_events) + "</b>");
    }
    mTimeInfo.setText(Html.fromHtml(timeInfoBuilder.toString()));
    StringBuilder periodicInfoBuilder = new StringBuilder();
    if (periodicInfoArray != null && periodicInfoArray.length != 0) {
      for (int i = 0; i < periodicInfoArray.length; ++i) {
        periodicInfoBuilder.append("\n").append(periodicInfoArray[i]);
      }
    }
    mEventInfo.setText(periodicInfoBuilder.toString());
  }

  /**
   * Invalidates underlying views.
   */
  public void invalidate() {
    if (getActivity() != null) {
      getActivity().runOnUiThread(new Runnable() {
        @Override
        public void run() {
          updateContent();
        }
      });
    }
  }

}
