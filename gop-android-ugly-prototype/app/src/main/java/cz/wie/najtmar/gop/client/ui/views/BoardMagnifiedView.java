package cz.wie.najtmar.gop.client.ui.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import java.util.Map;

import cz.wie.najtmar.gop.client.ui.AndroidEngine;
import cz.wie.najtmar.gop.client.ui.GoP;
import cz.wie.najtmar.gop.client.ui.R;
import cz.wie.najtmar.gop.client.view.CityView;
import cz.wie.najtmar.gop.client.view.PositionView;
import cz.wie.najtmar.gop.client.view.PrawnView;
import cz.wie.najtmar.gop.common.Point2D;

/**
 * Created by najtmar on 20.10.16.
 */

public class BoardMagnifiedView extends View {

  /**
   * Central model object.
   */
  private GoP mGoP;

  // Magnification parameters.
  private double mMagnifyRatio;
  private Matrix mMagnifyMatrix;
  private Paint mMagnifyPaint;
  private double mCanvasWidth;
  private double mCanvasHeight;
  private double mMagnifiedDistinctivePositionRadius;

  /**
   * Shared instance of class AndroidEngine, which may call back on the fragment.
   */
  private AndroidEngine mAndroidEngine;

  /**
   * Used to react to the gestures.
   */
  private final GestureDetector mGestureDetector;

  public BoardMagnifiedView(Context context, AttributeSet attrs) {
    super(context, attrs);
    if (isInEditMode()) {
      mGestureDetector = null;
      return;
    }
    mGoP = GoP.get(context);
    final GestureListener gestureListener = new GestureListener();
    mGestureDetector = new GestureDetector(context, gestureListener, null, true);
    mAndroidEngine = (AndroidEngine) mGoP.getInjector().provideOutputEngine();
    mMagnifyMatrix = new Matrix();
    mMagnifyPaint = new Paint();

    mMagnifyRatio = getFloat(R.dimen.magnification_ratio, context);
    mMagnifiedDistinctivePositionRadius =
        getFloat(R.dimen.distinctive_position_radius, context) * mMagnifyRatio
            * mAndroidEngine.getAndroidMagnifyRadius();
  }

  /**
   * Returns a dimension value encoded in a resource defined by itemId .
   * @param dimensionId the resource to read the value from
   * @param context the Context to read the resources from
   * @return float value encoded in a resource defined by itemId
   */
  protected float getDimension(int dimensionId, Context context) {
    return context.getResources().getDimension(dimensionId);
  }

  /**
   * Returns a float value encoded in a resource defined by dimensionId .
   * @param dimensionId the resource to read the value from
   * @param context the Context to read the resources from
   * @return float value encoded in a resource defined by dimensionId
   */
  private float getFloat(int dimensionId, Context context) {
    final TypedValue outValue = new TypedValue();
    context.getResources().getValue(dimensionId, outValue, true);
    return outValue.getFloat();
  }

  @Override
  public boolean onTouchEvent(MotionEvent event) {
    boolean result = false;
    if (mAndroidEngine.isMagnifyOn()) {
      result = mGestureDetector.onTouchEvent(event);
    }
    return result;
  }

  private Point2D.Double reverseTransformPoint(Point2D.Double point, RectF src) {
    /*
    return new Point2D.Double(
        (point.getX() - (src.left / mAndroidEngine.getAndroidMagnifyX()
            - AndroidEngine.TRANSLATE_X)) * mMagnifyRatio - AndroidEngine.TRANSLATE_X,
        (point.getY() - (src.top / mAndroidEngine.getAndroidMagnifyY()
            - AndroidEngine.TRANSLATE_Y)) * mMagnifyRatio - AndroidEngine.TRANSLATE_Y);
            */
    return new Point2D.Double(
        (point.getX() - (src.left / mAndroidEngine.getAndroidMagnifyX()
            - AndroidEngine.TRANSLATE_X)) * mMagnifyRatio - AndroidEngine.TRANSLATE_X,
        (point.getY() - (src.top / mAndroidEngine.getAndroidMagnifyY()
            - AndroidEngine.TRANSLATE_Y)) * mMagnifyRatio - AndroidEngine.TRANSLATE_Y);
  }

  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);
    if (isInEditMode()) return;
    if (mAndroidEngine.isMagnifyOn()) {
      final Bitmap bottomBitmap = mAndroidEngine.generateBottomBitmap();
      if (bottomBitmap != null) {
        mMagnifyMatrix.reset();

        // Magnified Board .
        final double magnifyCenterX = mAndroidEngine.getMagnifyCenterX();
        final double magnifyCenterY = mAndroidEngine.getMagnifyCenterY();
        mCanvasWidth = canvas.getWidth();
        mCanvasHeight = canvas.getHeight();
        RectF src = new RectF(
            (float) (magnifyCenterX - mCanvasWidth / 2 / mMagnifyRatio),
            (float) (magnifyCenterY - mCanvasHeight / 2 / mMagnifyRatio),
            (float) (magnifyCenterX + mCanvasWidth / 2 / mMagnifyRatio),
            (float) (magnifyCenterY + mCanvasHeight / 2 / mMagnifyRatio));
        RectF dest = new RectF(0f, 0f, (float) mCanvasWidth, (float) mCanvasHeight);
        if (mMagnifyMatrix.setRectToRect(src, dest, Matrix.ScaleToFit.CENTER)) {
          final Shader bottomShader =
              new BitmapShader(bottomBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
          bottomShader.setLocalMatrix(mMagnifyMatrix);
          mMagnifyPaint.setShader(bottomShader);
          canvas.drawRect(0, 0, canvas.getWidth(), canvas.getHeight(), mMagnifyPaint);
        }

        // Prawns.
        for (Map.Entry<PrawnView, Point2D.Double> prawnEntry :
            mAndroidEngine.getDistinctivePrawns().entrySet()) {
          final PrawnView prawn = prawnEntry.getKey();
          final Point2D.Double reverseTransformedPosition =
              reverseTransformPoint(prawnEntry.getValue(), src);
          if (prawn.getType().equals(PrawnView.Type.WARRIOR)) {
            mAndroidEngine.drawPlayerWarriorShape(
                reverseTransformedPosition.getX(), reverseTransformedPosition.getY(),
                prawn.getPlayer().getIndex(), mMagnifiedDistinctivePositionRadius,
                getResources().getDimension(R.dimen.prawn_border_width),
                prawn.getWarriorDirection(), canvas);
          } else {
            mAndroidEngine.drawPlayerCircle(
                reverseTransformedPosition.getX(), reverseTransformedPosition.getY(),
                mMagnifiedDistinctivePositionRadius,
                getResources().getDimension(R.dimen.prawn_border_width),
                prawn.getPlayer().getIndex(), canvas);
          }
          if (prawn.getPlayer().getIndex() == mAndroidEngine.getCurrentPlayerIndex()) {
            final int prawnEnergy = (int) Math.ceil(prawn.getEnergy() * 10);
            mAndroidEngine.drawPlayerCenteredText(
                reverseTransformedPosition.getX(), reverseTransformedPosition.getY(),
                "" + prawnEnergy, mMagnifiedDistinctivePositionRadius, prawn.getPlayer().getIndex(),
                false, canvas);
          }
        }

        // Cities.
        for (Map.Entry<CityView, Point2D.Double> cityEntry :
            mAndroidEngine.getDistinctiveCities().entrySet()) {
          final CityView city = cityEntry.getKey();
          final Point2D.Double reverseTransformedPosition =
              reverseTransformPoint(cityEntry.getValue(), src);
          mAndroidEngine.drawPlayerOctangle(
              reverseTransformedPosition.getX(), reverseTransformedPosition.getY(),
              mMagnifiedDistinctivePositionRadius * 1.2,
              getResources().getDimension(R.dimen.city_border_width), city.getPlayer().getIndex(),
              canvas);
          if (city.getPlayer().getIndex() == mAndroidEngine.getCurrentPlayerIndex()) {
            mAndroidEngine.drawPlayerCenteredText(
                reverseTransformedPosition.getX(), reverseTransformedPosition.getY(),
                "" + city.getFactories().size(), mMagnifiedDistinctivePositionRadius,
                city.getPlayer().getIndex(), false, canvas);
          }
        }

        // Other Positions.
        for (Map.Entry<PositionView, Point2D.Double> positionEntry :
            mAndroidEngine.getDistinctivePositions().entrySet()) {
          final PositionView position = positionEntry.getKey();
          final Point2D.Double reverseTransformedPosition =
              reverseTransformPoint(positionEntry.getValue(), src);
          mAndroidEngine.drawEmptyCircle(
              reverseTransformedPosition.getX(), reverseTransformedPosition.getY(),
              mMagnifiedDistinctivePositionRadius,
              getResources().getColor(R.color.colorLightAccent),
              getResources().getDimension(R.dimen.distinctive_position_border_width), canvas);
        }
      }
    }
  }

  private class GestureListener extends GestureDetector.SimpleOnGestureListener {
    @Override
    public boolean onDown(MotionEvent event) {
      return true;
    }

    @Override
    public void onShowPress(MotionEvent e) {
      final float pressX = (float) ((e.getX() - mCanvasWidth / 2) / mMagnifyRatio);
      final float pressY = (float) ((e.getY() - mCanvasHeight / 2) / mMagnifyRatio);
      mAndroidEngine.onMagnifiedShortPress(pressX, pressY);
    }
  }
}
