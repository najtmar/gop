package cz.wie.najtmar.gop.server;

import com.google.inject.Provider;

import cz.wie.najtmar.gop.game.PropertiesReader;

/**
 * Provider for class KryoNetCommunicator .
 */

public class KryoNetCommunicatorProvider implements Provider<KryoNetCommunicator> {

  private static Provider<KryoNetCommunicator> sProvider;
  private static KryoNetCommunicator mKryoNetCommunicator;

  @Override
  public KryoNetCommunicator get() {
    return mKryoNetCommunicator;
  }

  public static Provider<KryoNetCommunicator> provideSingleton(PropertiesReader propertiesReader) {
    if (sProvider == null) {
      sProvider = new KryoNetCommunicatorProvider(propertiesReader);
    }
    return sProvider;
  }

  private KryoNetCommunicatorProvider(PropertiesReader propertiesReader) {
    mKryoNetCommunicator = new KryoNetCommunicator(propertiesReader);
  }
}
