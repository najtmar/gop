package cz.wie.najtmar.gop.client.ui.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import cz.wie.najtmar.gop.client.ui.AndroidEngine;
import cz.wie.najtmar.gop.client.ui.GoP;
import cz.wie.najtmar.gop.client.ui.InputManager;
import cz.wie.najtmar.gop.client.ui.R;
import cz.wie.najtmar.gop.common.Point2D;

/**
 * Created by najtmar on 02.11.16.
 */

public class BoardWarriorDirectionSelectionView extends View {

  /**
   * Central model object.
   */
  private GoP mGoP;

  /**
   * Shared instance of class AndroidEngine, which may call back on the fragment.
   */
  private AndroidEngine mAndroidEngine;

  /**
   * Used to react to the gestures.
   */
  private final GestureDetector mGestureDetector;

  private float mCanvasWidth;
  private float mCanvasHeight;
  private float mCanvasDiagonal;

  public BoardWarriorDirectionSelectionView(Context context, AttributeSet attrs) {
    super(context, attrs);
    final GestureListener gestureListener = new GestureListener();
    mGestureDetector = new GestureDetector(context, gestureListener, null, true);
    if (isInEditMode()) return;
    mGoP = GoP.get(context);
    mAndroidEngine = (AndroidEngine) mGoP.getInjector().provideOutputEngine();
  }

  @Override
  public boolean onTouchEvent(MotionEvent event) {
    boolean result = false;
    result = mGestureDetector.onTouchEvent(event);
    return result;
  }

  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);
    mCanvasWidth = canvas.getWidth();
    mCanvasHeight = canvas.getHeight();
    mCanvasDiagonal =
        (float) Math.sqrt(mCanvasWidth * mCanvasWidth + mCanvasHeight * mCanvasHeight);
    final double width = canvas.getWidth() / mAndroidEngine.getAndroidMagnifyX();
    final double height = canvas.getHeight() / mAndroidEngine.getAndroidMagnifyY();
    final double centerX = width / 2 - AndroidEngine.TRANSLATE_X;
    final double centerY = height / 2 - AndroidEngine.TRANSLATE_Y;
    double warriorAngle = mAndroidEngine.getWarriorDirection();
    if (warriorAngle < 0.0) {
      warriorAngle = 0f;
    }
    mAndroidEngine.drawPlayerWarriorShape(centerX, centerY, mAndroidEngine.getCurrentPlayerIndex(),
        canvas.getWidth() / 2 * 0.3, getResources().getDimension(R.dimen.prawn_border_width), warriorAngle,
        canvas);

    final Paint linePaint = new Paint();
    linePaint.setColor(getResources().getColor(R.color.colorAccent));
    linePaint.setStrokeWidth(
        getResources().getDimension(R.dimen.warrior_selection_circle_stroke_width));
    linePaint.setAntiAlias(true);
    canvas.drawLine((float) (canvas.getWidth() / 2), (float) (canvas.getHeight() / 2),
        (float) (canvas.getWidth() / 2 + canvas.getWidth() / 2 * Math.cos(warriorAngle)),
        (float) (canvas.getHeight() / 2 + canvas.getHeight() / 2 * Math.sin(warriorAngle)),
        linePaint);

    final float warriorSelectionCircleStrokeWidth =
        getResources().getDimension(R.dimen.warrior_selection_circle_stroke_width);
    mAndroidEngine.drawEmptyCircle(centerX, centerY,
        canvas.getWidth() / 2 - warriorSelectionCircleStrokeWidth,
        getResources().getColor(R.color.colorLightAccent),
        warriorSelectionCircleStrokeWidth, canvas);
    final double textSize = getResources().getDimension(R.dimen.info_text_size);
    final double textY =
        mCanvasHeight / 7 / mAndroidEngine.getAndroidMagnifyY() - AndroidEngine.TRANSLATE_Y;
    mAndroidEngine.drawCenteredText(centerX, textY,
        getResources().getString(R.string.select_direction), textSize,
        getResources().getColor(R.color.colorLightAccent), canvas);
  }

  private class GestureListener extends GestureDetector.SimpleOnGestureListener {
    @Override
    public boolean onDown(MotionEvent event) {
      return true;
    }

    @Override
    public boolean onScroll(MotionEvent event0, MotionEvent event1, float distanceX,
                            float distanceY) {
      // TODO(najtmar): Implement this.
      return true;
    }

    @Override
    public boolean onFling(MotionEvent event0, MotionEvent event1, final float velocityX,
                           final float velocityY) {
      boolean result = super.onFling(event0, event1, velocityX, velocityY);
      return result;
    }

    @Override
    public void onShowPress(MotionEvent e) {
      if (!mAndroidEngine.getState().equals(InputManager.State.WARRIOR_DIRECTION)) return;
      final Point2D.Double warriorSelectionCenter =
          new Point2D.Double(mCanvasWidth / 2, mCanvasHeight / 2);
      final Point2D.Double pointSelected = new Point2D.Double(e.getX(), e.getY());
      if (warriorSelectionCenter.distance(pointSelected) < mCanvasDiagonal / 2
          && warriorSelectionCenter.distance(pointSelected) > 2) {
        final double vectorAtan2 = Math.atan2(pointSelected.getY() - warriorSelectionCenter.getY(),
            pointSelected.getX() - warriorSelectionCenter.getX());
        if (vectorAtan2 >= 0.0) {
          mAndroidEngine.setWarriorDirection(vectorAtan2);
        } else {
          mAndroidEngine.setWarriorDirection(vectorAtan2 + 2.0 * Math.PI);
        }
        mAndroidEngine.invalidateViews();
      }
    }
  }
}
