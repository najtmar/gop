package cz.wie.najtmar.gop.client.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.ColorRes;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.graphics.drawable.DrawableCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import cz.wie.najtmar.gop.client.ui.R;
import cz.wie.najtmar.gop.client.ui.activities.GameRulesPagerActivity;
import cz.wie.najtmar.gop.client.ui.activities.GameTutorialPagerActivity;

/**
 * Created by najtmar on 04.01.17.
 */

abstract public class MenuFragment extends Fragment {

  private static void tintMenuIcon(Context context, MenuItem item, @ColorRes int color) {
    Drawable normalDrawable = item.getIcon();
    Drawable wrapDrawable = DrawableCompat.wrap(normalDrawable);
    DrawableCompat.setTint(wrapDrawable, context.getResources().getColor(color));

    item.setIcon(wrapDrawable);
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
  }

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    super.onCreateOptionsMenu(menu, inflater);
    inflater.inflate(R.menu.game_help, menu);

    for (int menuId : new int[]{R.id.menu_item_game_tutorial, R.id.menu_item_game_rules}) {
      final MenuItem menuItem = menu.findItem(menuId);
      if (menuItem != null) {
        tintMenuIcon(getActivity(), menuItem, R.color.colorLightAccent);
      }
    }
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.menu_item_game_tutorial: {
        Intent intent = new Intent(getActivity(), GameTutorialPagerActivity.class);
        startActivity(intent);
        return true;
      }
      case R.id.menu_item_game_rules: {
        Intent intent = new Intent(getActivity(), GameRulesPagerActivity.class);
        startActivity(intent);
        return true;
      }
      default: {
        return super.onOptionsItemSelected(item);
      }
    }
  }
}
