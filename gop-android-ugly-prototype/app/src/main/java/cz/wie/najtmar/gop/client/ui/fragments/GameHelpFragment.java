package cz.wie.najtmar.gop.client.ui.fragments;

import androidx.fragment.app.Fragment;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;

import androidx.annotation.Nullable;
import cz.wie.najtmar.gop.client.ui.R;

/**
 * Created by najtmar on 05.01.17.
 */

public abstract class GameHelpFragment extends Fragment {

  protected static final String ARG_ASSETS_NAME = "assets_name";
  protected static final String ARG_NAME = "name";
  protected static final String ARG_POS = "pos";
  protected static final String ARG_TOTAL = "total";

  private AssetManager mAssets;

  private String mAssetsName;
  private String mName;
  private int mPos;
  private int mTotal;

  private TextView mTitle;
  private TextView mPageCounter;
  private ImageView mImage;

  private TextView mHelpText;
  private Button mClose;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    mAssets = getActivity().getAssets();

    mAssetsName = getArguments().getString(ARG_ASSETS_NAME);
    mName = getArguments().getString(ARG_NAME);
    mPos = getArguments().getInt(ARG_POS);
    mTotal = getArguments().getInt(ARG_TOTAL);
  }

  /**
   * Given the name of an info item, returns its value.
   * @param itemName the name of an info item, equal to the image name without extension
   * @return the info string for an help item
   */
  private String getHelpItemInfo(String itemName) {
    try {
      final String classPackage = getClass().getPackage().getName();
      final Class resource = Class.forName(classPackage.substring(0, classPackage.lastIndexOf('.')) + ".R$string");
      final Field field = resource.getField(mAssetsName + "_" + itemName);
      final int infoId = field.getInt(null);
      return getResources().getString(infoId);
    } catch (ClassNotFoundException exception) {
      Log.e("GHF", "", exception);
      return "";
    } catch (IllegalAccessException exception) {
      Log.e("GHF", "", exception);
      return "";
    } catch (NoSuchFieldException exception) {
      Log.e("GHF", "", exception);
      return "";
    }
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.game_help, container, false);
    mTitle = (TextView) view.findViewById(R.id.game_help_title);
    mPageCounter = (TextView) view.findViewById(R.id.game_help_page_count);
    mImage = (ImageView) view.findViewById(R.id.game_help_view);

    mTitle.setText(mName);
    mPageCounter.setText("(" + (mPos + 1) + "/" + mTotal + ")");
    String infoItemName = null;
    try {
      String bitmapFileName = mAssets.list(mAssetsName)[mPos];
      if (bitmapFileName.lastIndexOf('.') > 0) {
        infoItemName = bitmapFileName.substring(0, bitmapFileName.lastIndexOf('.'));
      }
      InputStream is = mAssets.open(mAssetsName + "/" + bitmapFileName);
      Bitmap bitmap = BitmapFactory.decodeStream(is);
      mImage.setImageBitmap(bitmap);
    } catch (IOException exception) {
      Log.e("GHF", "Cannot read bitmap from assets.", exception);
    }

    mHelpText = (TextView) view.findViewById(R.id.help_text);
    mClose = (Button) view.findViewById(R.id.game_help_close);
    mClose.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getActivity().finish();
      }
    });
    mHelpText.setText(Html.fromHtml(getHelpItemInfo(infoItemName)));

    return view;
  }

}
