package cz.wie.najtmar.gop.client.ui.fragments;

import android.content.Intent;
import android.view.View;
import android.widget.Button;

import cz.wie.najtmar.gop.client.ui.R;
import cz.wie.najtmar.gop.client.ui.activities.BoardFactoryActivity;

/**
 * Created by najtmar on 27.10.16.
 */

public class ControlPanelBoardCityFragment extends ControlPanelBoardFragment {

  // UI elements.
  private Button mCloseButton;

  @Override
  protected void initializeControls(View view) {
    mCloseButton = (Button) view.findViewById(R.id.board_city_close);
    mCloseButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getAndroidEngine().closeCityView();
      }
    });
  }

  @Override
  protected int getControlPanelLayout() {
    return R.layout.control_panel_board_city;
  }

  @Override
  public void onControlPanelBoardFragmentResume() {
    getAndroidEngine().setControlPanelBoardCityFragment(this);
  }

  @Override
  public void onControlPanelBoardFragmentDestroy() {
  }

  @Override
  public void updateState() {
  }

  /**
   * Starts Activity BoardFactory .
   */
  public void startBoardFactoryActivity() {
    Intent intent = new Intent(getActivity(), BoardFactoryActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

  public void finishBoardCityActivity() {
    getActivity().finish();
    getAndroidEngine().setControlPanelBoardCityFragment(null);
  }
}
