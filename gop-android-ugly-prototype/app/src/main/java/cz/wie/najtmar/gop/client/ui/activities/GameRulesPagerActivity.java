package cz.wie.najtmar.gop.client.ui.activities;

import android.content.res.AssetManager;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;

import java.io.IOException;

import cz.wie.najtmar.gop.client.ui.AndroidEngine;
import cz.wie.najtmar.gop.client.ui.GoP;
import cz.wie.najtmar.gop.client.ui.R;
import cz.wie.najtmar.gop.client.ui.fragments.GameRulesFragment;

/**
 * Created by najtmar on 10.01.17.
 */

public class GameRulesPagerActivity extends FragmentActivity {

  private static final String GAME_RULES = "game_rules";

  private AssetManager mAssets;

  private ViewPager mViewPager;

  /**
   * Central model object.
   */
  private GoP mGoP;

  private AndroidEngine mAndroidEngine;

  private int getAssetCount() {
    try {
      return mAssets.list(GAME_RULES).length;
    } catch (IOException exception) {
      Log.e("GTPA", "Could not lists assets " + GAME_RULES, exception);
      return 0;
    }
  }

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.game_help_pager);

    mGoP = GoP.get(this);
    mAndroidEngine = (AndroidEngine) mGoP.getInjector().provideOutputEngine();

    mAssets = getAssets();
    mViewPager = (ViewPager) findViewById(R.id.game_help_view_pager);
    FragmentManager fragmentManager = getSupportFragmentManager();
    mViewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
      @Override
      public Fragment getItem(int position) {
        return GameRulesFragment.newInstance(GAME_RULES, getResources().getString(R.string.game_rules),
            position, getAssetCount());
      }

      @Override
      public int getCount() {
        return getAssetCount();
      }
    });
    mViewPager.setCurrentItem(mAndroidEngine.getGameRulesItemPos());
  }

  @Override
  protected void onStop() {
    super.onStop();
    mAndroidEngine.setGameRulesItemPos(mViewPager.getCurrentItem());
  }
}
