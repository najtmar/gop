package cz.wie.najtmar.gop.client.ui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import cz.wie.najtmar.gop.client.ui.R;

/**
 * Created by najtmar on 16.12.16.
 */

public class StatusBarLayout extends LinearLayout {

  /**
   * Used to react to the gestures.
   */
  private GestureDetector mGestureDetector;

  public StatusBarLayout(Context context, AttributeSet attrs) {
    super(context, attrs);
    if (isInEditMode()) return;
    final GestureListener gestureListener = new GestureListener(this);
    mGestureDetector = new GestureDetector(context, gestureListener, null, true);
  }

  @Override
  public boolean onTouchEvent(MotionEvent event) {
    boolean result = false;
    result = mGestureDetector.onTouchEvent(event);
    return result;
  }

  public void onMove(float dx, float dy) {
    final ViewGroup.LayoutParams layoutParams = getLayoutParams();
    final int minHeight = (int) getResources().getDimension(R.dimen.status_bar_height);
    if (Math.abs(dy) > Math.abs(dx) && dy > getResources().getDimension(R.dimen.status_bar_height) / 3) {
      layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
    } else if (Math.abs(dy) > Math.abs(dx) && -dy > getResources().getDimension(R.dimen.status_bar_height) / 3) {
      layoutParams.height = minHeight;
    }
    setLayoutParams(layoutParams);

    invalidate();
  }

  private class GestureListener extends GestureDetector.SimpleOnGestureListener {

    private final StatusBarLayout mView;

    public GestureListener(StatusBarLayout view) {
      mView = view;
    }

    @Override
    public boolean onDown(MotionEvent event) {
      return true;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
      mView.onMove(-distanceX, -distanceY);
      return true;
    }

  }
}
