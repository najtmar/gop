package cz.wie.najtmar.gop.client.ui.fragments;

import android.view.View;
import android.widget.Button;

import cz.wie.najtmar.gop.client.ui.R;

/**
 * Created by najtmar on 04.11.16.
 */

public class ControlPanelBoardPrawnRepairFragment  extends ControlPanelBoardFragment {

  // UI elements.
  private Button mConfirmButton;
  private Button mCancelButton;

  @Override
  protected void initializeControls(View view) {
    mConfirmButton = (Button) view.findViewById(R.id.board_prawn_repair_confirm);
    mConfirmButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getAndroidEngine().setRepairPrawn();
      }
    });
    mCancelButton = (Button) view.findViewById(R.id.board_prawn_repair_cancel);
    mCancelButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getAndroidEngine().closeRepairSelection();
      }
    });
  }

  @Override
  protected int getControlPanelLayout() {
    return R.layout.control_panel_board_prawn_repair;
  }

  @Override
  public void onControlPanelBoardFragmentResume() {
    getAndroidEngine().setControlPanelBoardPrawnRepairFragment(this);
  }

  @Override
  public void onControlPanelBoardFragmentDestroy() {
  }

  @Override
  public void updateState() {
  }

  public void finishBoardPrawnRepairActivity() {
    getActivity().finish();
    getAndroidEngine().setControlPanelBoardPrawnRepairFragment(null);
  }
}
