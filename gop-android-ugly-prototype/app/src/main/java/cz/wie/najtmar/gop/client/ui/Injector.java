package cz.wie.najtmar.gop.client.ui;

import android.content.Context;

import com.google.inject.Provider;

import java.util.Locale;
import java.util.Random;
import java.util.UUID;

import cz.wie.najtmar.gop.board.BoardGenerator;
import cz.wie.najtmar.gop.board.MapBoardGeneratorProvider;
import cz.wie.najtmar.gop.client.view.TextManager;
import cz.wie.najtmar.gop.client.view.UserView;
import cz.wie.najtmar.gop.game.AndroidPropertiesReaderProvider;
import cz.wie.najtmar.gop.game.PropertiesReader;
import cz.wie.najtmar.gop.server.ConnectionManager;
import cz.wie.najtmar.gop.server.CrossBoardPrawnDeployerProvider;
import cz.wie.najtmar.gop.server.GameSessionProvider;
import cz.wie.najtmar.gop.server.InitialPrawnDeployer;
import cz.wie.najtmar.gop.server.KryoNetCommunicator;
import cz.wie.najtmar.gop.server.KryoNetCommunicatorProvider;
import cz.wie.najtmar.gop.server.KryoNetComunicatorFactoryProvider;

/**
 * Provides a functionality equivalent to a Guice injector.
 */

public class Injector {

  private static final String[] USER_NAMES = {
      "Attila the Hun",
      "Bolesław Chrobry",
      "Charlemagne",
      "Donald Duck",
      "Erik the Red",
      "Francis Drake",
      "George Washington",
      "Hammurabi",
      "Ivan the Terrible",
      "Julius Caesar",
      "Kim Il-sung",
      "Louis XIV",
      "Mahatma Gandhi",
      "Napoleon Bonaparte",
      "Otto von Bismarck",
      "Pericles",
      "Quentin Tarantino",
      "Robert E. Lee",
      "Suleiman the Magnificent",
      "Thomas Jefferson",
      "Ulrich von Jungingen",
      "Vladimir Lenin",
      "William the Conqueror",
      "Xerxes",
      "Yasser Arafat",
      "Zardoz"
  };

  /**
   * Local user.
   */
  private final UserView mUser;

  /**
   * The user name of the device.
   */
  private String mUserName;

  private final Context mContext;

  private final Locale mLocale;

  private final Random mRandom;

  public Injector(Context context, Locale locale) {
    mContext = context;
    mLocale = locale;
    mRandom = new Random();
    mUser = new UserView(getUsername(), UUID.nameUUIDFromBytes(getUsername().getBytes()));
  }

  /**
   * Generates the user name.
   * @return the user name of the device, or a generated name if the name cannot be obtained
   */
  private String getUsername() {
    return USER_NAMES[mRandom.nextInt(USER_NAMES.length)];
  }

  public GameManager provideGameManager() {
    return GameManagerProvider.provideGameManagerSingleton(provideGameManagementEngine(),
        ClientProvider.provideClient(
            providePropertiesReader(), provideOutputEngine(), provideInputEngine()),
        provideGameManagerManagedEngine(), providePropertiesReader(), provideSession(),
        provideKryoNetCommunicatorProvider().get(), provideUser(),
        new GameSessionProvider(providePropertiesReader(), provideBoardGenerator(),
            provideInitialPrawnDeployer(), provideConnectionManagerFactory(), mRandom.nextLong()),
        null);
  }

  public PropertiesReader providePropertiesReader() {
    return AndroidPropertiesReaderProvider.provide(mContext);
  }

  private BoardGenerator provideBoardGenerator() {
    return MapBoardGeneratorProvider.provide(providePropertiesReader());
  }

  private InitialPrawnDeployer provideInitialPrawnDeployer() {
    return CrossBoardPrawnDeployerProvider.provide();
  }

  private Provider<KryoNetCommunicator> provideKryoNetCommunicatorProvider() {
    return KryoNetCommunicatorProvider.provideSingleton(providePropertiesReader());
  }

  private ConnectionManager.Factory provideConnectionManagerFactory() {
    return KryoNetComunicatorFactoryProvider.provide(provideKryoNetCommunicatorProvider());
  }

  public GameManagementEngine provideGameManagementEngine() {
    return AndroidGameManagementEngine.create();
  }

  public OutputEngine provideOutputEngine() {
    return AndroidEngine.create();
  }

  private InputEngine provideInputEngine() {
    return AndroidEngine.create();
  }

  private GameManagerManagedEngine provideGameManagerManagedEngine() {
    return AndroidEngine.create();
  }

  private Session provideSession() {
    return new AndroidSession();
  }

  private UserView provideUser() {
    return mUser;
  }

  public TextManager provideTextManager() { return new TextManager(mLocale); }
}
