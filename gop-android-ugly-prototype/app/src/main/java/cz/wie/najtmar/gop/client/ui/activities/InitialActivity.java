package cz.wie.najtmar.gop.client.ui.activities;

import androidx.fragment.app.Fragment;

import cz.wie.najtmar.gop.client.ui.fragments.InitialFragment;

public class InitialActivity extends SingleFragmentActivity {

  @Override
  protected Fragment createFragment() {
    return new InitialFragment();
  }
}
