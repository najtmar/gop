package cz.wie.najtmar.gop.client.ui.fragments;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import android.util.Log;

/**
 * Created by najtmar on 17.12.16.
 */

public abstract class RobustDialogFragment extends DialogFragment {

  /**
   * Fallback action performed in case the DialogFragment cannot be shown.
   */
  protected abstract void fallback();

  @Override
  public void show(FragmentManager manager, String tag) {
    try {
      super.show(manager, tag);
    } catch (IllegalStateException exception) {
      Log.e("RobustDialogFragment", "Exception", exception);
    }
  }
}
