package cz.wie.najtmar.gop.client.ui.fragments;

import android.content.Intent;
import android.view.View;

import cz.wie.najtmar.gop.client.ui.R;
import cz.wie.najtmar.gop.client.ui.activities.BoardBoardActivity;
import cz.wie.najtmar.gop.client.ui.activities.BoardGameFinishedActivity;

/**
 * Created by najtmar on 27.10.16.
 */

public class ControlPanelBoardInitializedFragment extends ControlPanelBoardFragment {

  // UI elements.

  @Override
  protected void initializeControls(View view) {
  }

  @Override
  protected int getControlPanelLayout() {
    return R.layout.control_panel_board_initialized;
  }

  @Override
  public void onControlPanelBoardFragmentResume() {
    getAndroidEngine().setControlPanelBoardInitializedFragment(this);
  }

  @Override
  public void onControlPanelBoardFragmentDestroy() {
    getAndroidEngine().setControlPanelBoardInitializedFragment(null);
  }

  @Override
  public void updateState() {
  }

  /**
   * Starts Activity BoardBoard .
   */
  public void startBoardBoardActivity() {
    Intent intent = new Intent(getActivity(), BoardBoardActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

  /**
   * Starts Activity GameFinished .
   */
  public void startBoardGameFinishedActivity() {
    Intent intent = new Intent(getActivity(), BoardGameFinishedActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

  public void finishBoardBoardInitializedActivity() {
    getActivity().finish();
  }
}
