package cz.wie.najtmar.gop.client.ui.activities;

import androidx.fragment.app.Fragment;

import cz.wie.najtmar.gop.client.ui.fragments.ControlPanelBoardWarriorFragment;

/**
 * Created by najtmar on 21.10.16.
 */

public class BoardWarriorActivity extends GameFragmentsActivity {

  @Override
  protected Fragment createControlPanelFragment() {
    return new ControlPanelBoardWarriorFragment();
  }
}