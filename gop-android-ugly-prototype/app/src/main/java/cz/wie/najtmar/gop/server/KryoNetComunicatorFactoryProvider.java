package cz.wie.najtmar.gop.server;

import com.google.inject.Provider;

/**
 * Provider for class KryoNetComunicator.Factory .
 */

public class KryoNetComunicatorFactoryProvider {

  public static ConnectionManager.Factory provide(
      Provider<KryoNetCommunicator> kryoNetCommunicatorProvider) {
    return new KryoNetCommunicator.Factory(kryoNetCommunicatorProvider);
  }

}
