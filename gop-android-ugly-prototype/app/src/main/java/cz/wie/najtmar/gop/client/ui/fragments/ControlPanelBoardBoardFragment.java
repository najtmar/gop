package cz.wie.najtmar.gop.client.ui.fragments;

import android.content.Intent;
import androidx.fragment.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import cz.wie.najtmar.gop.client.ui.R;
import cz.wie.najtmar.gop.client.ui.activities.BoardCityActivity;
import cz.wie.najtmar.gop.client.ui.activities.BoardGameAbandoningActivity;
import cz.wie.najtmar.gop.client.ui.activities.BoardPrawnSelectionActivity;
import cz.wie.najtmar.gop.client.ui.activities.BoardSettlersUnitActivity;
import cz.wie.najtmar.gop.client.ui.activities.BoardWarriorActivity;

/**
 * Created by najtmar on 17.10.16.
 */

public class ControlPanelBoardBoardFragment extends ControlPanelBoardFragment {

  public static final String DIALOG_ABANDON_GAME = "AbandonGame";

  // UI elements.
  private Button mPlayButton;
  private Button mAbandonGameButton;

  private FragmentManager mFragmentManager;

  private AbandonGameDialogFragment mAbandonGameDialogFragment;

  @Override
  protected void initializeControls(View view) {
    mPlayButton = (Button) view.findViewById(R.id.board_board_play);
    mPlayButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getGoP().releaseUiSemaphore();
      }
    });

    mAbandonGameButton = (Button) view.findViewById(R.id.board_board_abandon_game);
    mAbandonGameButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getAndroidEngine().showGameAbandoningPrompt();
        mAbandonGameDialogFragment = AbandonGameDialogFragment.newInstance();
        mAbandonGameDialogFragment.show(mFragmentManager, DIALOG_ABANDON_GAME);
      }
    });

    mFragmentManager = getFragmentManager();
  }

  @Override
  protected int getControlPanelLayout() {
    return R.layout.control_panel_board_board;
  }

  @Override
  public void onControlPanelBoardFragmentResume() {
    getAndroidEngine().setControlPanelBoardBoardFragment(this);
  }

  @Override
  public void onControlPanelBoardFragmentDestroy() {
  }

  @Override
  public void updateState() {
  }

  /**
   * Starts Activity BoardMagnifiedWarrior .
   */
  public void startBoardWarriorActivity() {
    Intent intent = new Intent(getActivity(), BoardWarriorActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

  /**
   * Starts Activity BoardMagnifiedSettlersUnit .
   */
  public void startBoardSettlersUnitActivity() {
    Intent intent = new Intent(getActivity(), BoardSettlersUnitActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

  /**
   * Starts Activity BoardCity .
   */
  public void startBoardCityActivity() {
    Intent intent = new Intent(getActivity(), BoardCityActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

  /**
   * Starts Activity PrawnSelection .
   */
  public void startBoardPrawnSelectionActivity() {
    Intent intent = new Intent(getActivity(), BoardPrawnSelectionActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

  /**
   * Starts Activity GameAbandoning .
   */
  public void startBoardGameAbandoningActivity() {
    Intent intent = new Intent(getActivity(), BoardGameAbandoningActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

  public void finishBoardBoardActivity() {
    getActivity().finish();
    getAndroidEngine().setControlPanelBoardBoardFragment(null);
  }
}
