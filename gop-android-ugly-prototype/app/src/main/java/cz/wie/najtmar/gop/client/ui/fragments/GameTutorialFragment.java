package cz.wie.najtmar.gop.client.ui.fragments;

import android.os.Bundle;

/**
 * Created by najtmar on 05.01.17.
 */

public class GameTutorialFragment extends GameHelpFragment {

  public static GameHelpFragment newInstance(String assetsName, String name, int pos, int total) {
    Bundle args = new Bundle();
    args.putSerializable(ARG_ASSETS_NAME, assetsName);
    args.putSerializable(ARG_NAME, name);
    args.putSerializable(ARG_POS, pos);
    args.putSerializable(ARG_TOTAL, total);

    GameTutorialFragment fragment = new GameTutorialFragment();
    fragment.setArguments(args);
    return fragment;
  }

}
