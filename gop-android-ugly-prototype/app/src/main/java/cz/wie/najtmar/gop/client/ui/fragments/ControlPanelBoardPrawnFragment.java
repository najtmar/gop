package cz.wie.najtmar.gop.client.ui.fragments;

import android.content.Intent;
import android.view.View;
import android.widget.Button;

import cz.wie.najtmar.gop.client.ui.R;
import cz.wie.najtmar.gop.client.ui.activities.BoardItineraryBuiltActivity;

/**
 * Created by najtmar on 21.10.16.
 */

public abstract class ControlPanelBoardPrawnFragment extends ControlPanelBoardFragment {

  // UI elements.
  private Button mClearItineraryButton;
  private Button mCloseButton;

  /**
   * Initializes extra controls.
   * @param view the Fragment view
   */
  protected abstract void initializePrawnSpecificControls(View view);

  @Override
  protected final void initializeControls(View view) {
    mClearItineraryButton = (Button) view.findViewById(R.id.board_prawn_clear_itinerary);
    mClearItineraryButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getAndroidEngine().clearPrawnItinerary();
      }
    });
    mCloseButton = (Button) view.findViewById(R.id.board_prawn_close);
    mCloseButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getAndroidEngine().closePrawnActivity();
      }
    });
    initializePrawnSpecificControls(view);
  }

  @Override
  public void onControlPanelBoardFragmentResume() {
    getAndroidEngine().setControlPanelBoardPrawnFragment(this);
  }

  @Override
  public void onControlPanelBoardFragmentDestroy() {
  }

  @Override
  public void updateState() {
  }

  /**
   * Initializes ITINERARY_BUILT UI.
   */
  public void initializeItineraryBuilt() {
    Intent intent = new Intent(getActivity(), BoardItineraryBuiltActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

  public void finishBoardPrawnActivity() {
    getActivity().finish();
    getAndroidEngine().setControlPanelBoardPrawnFragment(null);
  }

}
