package cz.wie.najtmar.gop.client.ui;

import cz.wie.najtmar.gop.game.PropertiesReader;

/**
 * Provider for class Client .
 */

public class ClientProvider {

  public static Client provideClient(PropertiesReader propertiesReader, OutputEngine outputEngine,
                                     InputEngine inputEngine) {
    return new Client(propertiesReader, outputEngine, inputEngine);
  }
}
