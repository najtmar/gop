package cz.wie.najtmar.gop.client.ui.activities;

import androidx.fragment.app.Fragment;

import cz.wie.najtmar.gop.client.ui.fragments.JoinFragment;

/**
 * Created by najtmar on 11.10.16.
 */

public class JoinActivity extends SingleFragmentActivity {
  @Override
  protected Fragment createFragment() {
    return new JoinFragment();
  }
}
