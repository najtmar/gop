package cz.wie.najtmar.gop.client.ui.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;

import cz.wie.najtmar.gop.client.ui.R;

/**
 * Created by najtmar on 07.12.16.
 */

public class ConnectionBrokenDialogFragment extends RobustDialogFragment {

  /**
   * Defines the type of the Dialog .
   */
  public static enum Type {
    /**
     * Own client broke connection to the server.
     */
    OWN_CONNECTION_BROKEN,

    /**
     * Other client(s) broke connection to the server.
     */
    OTHERS_CONNECTION_BROKEN,
  }

  /**
   * The type of the Dialog .
   */
  private Type mType;

  public static ConnectionBrokenDialogFragment newInstance(@NonNull Type type) {
    ConnectionBrokenDialogFragment fragment = new ConnectionBrokenDialogFragment();
    fragment.mType = type;

    return fragment;
  }

  @NonNull
  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    if (mType == Type.OWN_CONNECTION_BROKEN) {
      return new AlertDialog.Builder(getActivity())
          .setTitle(R.string.own_connection_broken_title)
          .setMessage(R.string.own_connection_broken_message)
          .setIcon(R.drawable.ic_connection_broken_80dp)
          .setPositiveButton(android.R.string.ok, null)
          .create();
    } else {
      return new AlertDialog.Builder(getActivity())
          .setTitle(R.string.others_connection_broken_title)
          .setMessage(R.string.others_connection_broken_message)
          .setIcon(R.drawable.ic_connection_problem_80dp)
          .setPositiveButton(android.R.string.ok, null)
          .create();
    }
  }

  @Override
  protected void fallback() {
    // null implementation.
  }
}
