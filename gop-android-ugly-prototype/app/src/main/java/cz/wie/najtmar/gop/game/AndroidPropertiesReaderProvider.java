package cz.wie.najtmar.gop.game;

import android.content.Context;

/**
 * Provider for class StandardPropertiesReader .
 */

public class AndroidPropertiesReaderProvider {

  public static PropertiesReader provide(Context context) {
    return new AndroidPropertiesReader(context);
  }

}
