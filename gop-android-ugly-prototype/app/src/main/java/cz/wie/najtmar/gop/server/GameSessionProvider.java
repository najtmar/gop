package cz.wie.najtmar.gop.server;

import android.util.Log;

import com.google.inject.Provider;
import com.google.inject.name.Named;

import cz.wie.najtmar.gop.board.BoardGenerator;
import cz.wie.najtmar.gop.game.PropertiesReader;

/**
 * Provider for class GameSession .
 */

public class GameSessionProvider implements Provider<GameSession> {

  private final PropertiesReader mPropertiesReader;
  private final BoardGenerator mBoardGenerator;
  private final InitialPrawnDeployer mPrawnDeployer;
  private final ConnectionManager.Factory mConnectionManagerFactory;
  private final long mSeed;

  public GameSessionProvider(PropertiesReader propertiesReader, BoardGenerator boardGenerator,
                              InitialPrawnDeployer prawnDeployer,
                              ConnectionManager.Factory connectionManagerFactory, long seed) {
    mPropertiesReader = propertiesReader;
    mBoardGenerator = boardGenerator;
    mPrawnDeployer = prawnDeployer;
    mConnectionManagerFactory = connectionManagerFactory;
    mSeed = seed;
  }

  @Override
  public GameSession get() {
    return new GameSession(mPropertiesReader, mBoardGenerator, mPrawnDeployer,
        mConnectionManagerFactory, mSeed);
  }
}
