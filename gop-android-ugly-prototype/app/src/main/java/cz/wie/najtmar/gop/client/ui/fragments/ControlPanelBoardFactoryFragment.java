package cz.wie.najtmar.gop.client.ui.fragments;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import cz.wie.najtmar.gop.client.ui.R;
import cz.wie.najtmar.gop.client.ui.activities.BoardPrawnRepairActivity;
import cz.wie.najtmar.gop.client.ui.activities.BoardWarriorDirectionSelectionActivity;
import cz.wie.najtmar.gop.client.view.CityView;
import cz.wie.najtmar.gop.client.view.FactoryView;
import cz.wie.najtmar.gop.client.view.PositionView;

/**
 * Created by najtmar on 28.10.16.
 */

public class ControlPanelBoardFactoryFragment extends ControlPanelBoardFragment {

  // UI elements.
  private Button mCloseButton;
  private Button mProduceWarriorButton;
  private Button mProduceSettlersUnitButton;
  private Button mRepairPrawnButton;
  private Button mGrowCityButton;

  @Override
  protected void initializeControls(View view) {
    mCloseButton = (Button) view.findViewById(R.id.board_factory_close);
    mCloseButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getAndroidEngine().closeCityFactoryView();
      }
    });
    mProduceWarriorButton = (Button) view.findViewById(R.id.board_factory_produce_warrior);
    mProduceWarriorButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getAndroidEngine().showWarriorDirectionSelection();
      }
    });
    mProduceSettlersUnitButton =
        (Button) view.findViewById(R.id.board_factory_produce_settlers_unit);
    mProduceSettlersUnitButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getAndroidEngine().setProduceSettlersUnit();
      }
    });
    mRepairPrawnButton = (Button) view.findViewById(R.id.board_factory_repair_prawn);
    mRepairPrawnButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getAndroidEngine().showPrawnRepairSelection();
      }
    });
    mGrowCityButton = (Button) view.findViewById(R.id.board_factory_grow_city);
    mGrowCityButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getAndroidEngine().setGrowCity();
      }
    });
  }

  @Override
  protected int getControlPanelLayout() {
    return R.layout.control_panel_board_factory;
  }

  @Override
  protected void onControlPanelBoardFragmentResume() {
    getAndroidEngine().setControlPanelBoardFactoryFragment(this);
  }

  @Override
  protected void onControlPanelBoardFragmentDestroy() {
  }

  @Override
  public void updateState() {
    final CityView city = getAndroidEngine().getCitySelected();
    final PositionView position = city.getJoint().getNormalizedPosition();
    mRepairPrawnButton.setEnabled(position.getPrawns().size() >= 1);
    mGrowCityButton.setEnabled(city.getFactories().size() < position.getRoadSections().size());
  }

  /**
   * Starts Activity WarriorDirectionSelection .
   */
  public void startBoardWarriorDirectionSelectionActivity() {
    Intent intent = new Intent(getActivity(), BoardWarriorDirectionSelectionActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

  public void startBoardPrawnRepairActivity() {
    Intent intent = new Intent(getActivity(), BoardPrawnRepairActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

  public void finishBoardFactoryActivity() {
    getActivity().finish();
    getAndroidEngine().setControlPanelBoardFactoryFragment(null);
  }
}
