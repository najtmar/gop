package cz.wie.najtmar.gop.client.ui.fragments;

import android.view.View;
import android.widget.Button;

import cz.wie.najtmar.gop.client.ui.R;
import cz.wie.najtmar.gop.client.view.ClientViewException;

/**
 * Created by najtmar on 15.11.16.
 */

public class ControlPanelBoardGameFinishedFragment extends ControlPanelBoardFragment {

  // UI elements.
  private Button mCloseButton;

  @Override
  protected void initializeControls(View view) {
    mCloseButton = (Button) view.findViewById(R.id.board_game_finished_close);
    mCloseButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        try {
          getAndroidEngine().finishGame();
        } catch (ClientViewException exception) {
          throw new RuntimeException(exception);
        }
      }
    });
  }

  @Override
  protected int getControlPanelLayout() {
    return R.layout.control_panel_board_game_finished;
  }

  @Override
  public void onControlPanelBoardFragmentResume() {
    getAndroidEngine().setControlPanelBoardGameFinishedFragment(this);
  }

  @Override
  public void onControlPanelBoardFragmentDestroy() {
    getAndroidEngine().setControlPanelBoardGameFinishedFragment(null);
  }

  @Override
  public void updateState() {
  }

  public void finishBoardGameFinishedActivity() {
    getActivity().finish();
  }
}
