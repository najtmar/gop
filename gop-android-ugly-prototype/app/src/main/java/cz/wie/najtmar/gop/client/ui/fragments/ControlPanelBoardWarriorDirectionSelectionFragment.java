package cz.wie.najtmar.gop.client.ui.fragments;

import android.view.View;
import android.widget.Button;

import cz.wie.najtmar.gop.client.ui.R;

/**
 * Created by najtmar on 02.11.16.
 */

public class ControlPanelBoardWarriorDirectionSelectionFragment extends ControlPanelBoardFragment  {

  // UI elements.
  private Button mConfirmWarriorButton;
  private Button mAbandonWarriorButton;

  @Override
  protected void initializeControls(View view) {
    mConfirmWarriorButton =
        (Button) view.findViewById(R.id.board_warrior_direction_selection_confirm);
    mConfirmWarriorButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getAndroidEngine().setProduceWarrior();
      }
    });
    mAbandonWarriorButton =
        (Button) view.findViewById(R.id.board_warrior_direction_selection_abandon);
    mAbandonWarriorButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getAndroidEngine().closeProduceWarrior();
      }
    });
  }

  @Override
  protected int getControlPanelLayout() {
    return R.layout.control_panel_warrior_direction_selection;
  }

  @Override
  protected void onControlPanelBoardFragmentResume() {
    getAndroidEngine().setControlPanelBoardWarriorDirectionSelectionFragment(this);
  }

  @Override
  protected void onControlPanelBoardFragmentDestroy() {
  }

  @Override
  public void updateState() {
    mConfirmWarriorButton.setEnabled(getAndroidEngine().getWarriorDirection() >= 0.0);
  }

  public void finishBoardWarriorDirectionSelectionActivity() {
    getActivity().finish();
    getAndroidEngine().setControlPanelBoardWarriorDirectionSelectionFragment(null);
  }

}
