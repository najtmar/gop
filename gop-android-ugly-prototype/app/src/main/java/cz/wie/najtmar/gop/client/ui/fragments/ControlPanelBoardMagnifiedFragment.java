package cz.wie.najtmar.gop.client.ui.fragments;

import android.view.View;
import android.widget.Button;

import cz.wie.najtmar.gop.client.ui.R;

/**
 * Created by najtmar on 21.10.16.
 */

public class ControlPanelBoardMagnifiedFragment extends ControlPanelBoardFragment {

  // UI elements.
  private Button mCloseButton;

  @Override
  protected void initializeControls(View view) {
    mCloseButton = (Button) view.findViewById(R.id.board_magnified_close);
    mCloseButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getAndroidEngine().closeMagnification();
      }
    });
  }

  @Override
  protected int getControlPanelLayout() {
    return R.layout.control_panel_board_magnified;
  }

  @Override
  public void onControlPanelBoardFragmentResume() {
    getAndroidEngine().setControlPanelBoardMagnifiedFragment(this);
  }

  @Override
  public void onControlPanelBoardFragmentDestroy() {
  }

  @Override
  public void updateState() {
  }

  public void finishBoardMagnifiedActivity() {
    getActivity().finish();
    getAndroidEngine().setControlPanelBoardMagnifiedFragment(null);
  }

}
