package cz.wie.najtmar.gop.client.ui.fragments;

import android.view.View;
import android.widget.Button;

import cz.wie.najtmar.gop.client.ui.R;

/**
 * Created by najtmar on 22.10.16.
 */

public class ControlPanelBoardItineraryBuiltFragment extends ControlPanelBoardFragment {

  // UI elements.
  private Button mConfirmItineraryButton;
  private Button mAbandonItineraryButton;

  @Override
  protected void initializeControls(View view) {
    mConfirmItineraryButton =
        (Button) view.findViewById(R.id.board_itinerary_built_confirm_itinerary);
    mConfirmItineraryButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getAndroidEngine().confirmPrawnItinerary();
      }
    });

    mAbandonItineraryButton =
      (Button) view.findViewById(R.id.board_itinerary_built_abandon_itinerary);
    mAbandonItineraryButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getAndroidEngine().abandonPrawnItinerary();
      }
    });
  }

  @Override
  protected int getControlPanelLayout() {
    return R.layout.control_panel_board_itinerary_built;
  }

  @Override
  public void onControlPanelBoardFragmentResume() {
    getAndroidEngine().setControlPanelBoardItineraryBuiltFragment(this);
  }

  @Override
  public void onControlPanelBoardFragmentDestroy() {
  }

  @Override
  public void updateState() {
  }

  public void finishBoardItineraryBuiltActivity() {
    getActivity().finish();
    getAndroidEngine().setControlPanelBoardItineraryBuiltFragment(null);
  }

}
