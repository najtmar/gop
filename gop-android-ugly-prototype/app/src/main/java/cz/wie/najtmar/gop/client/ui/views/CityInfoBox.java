package cz.wie.najtmar.gop.client.ui.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;

import cz.wie.najtmar.gop.client.ui.AndroidEngine;
import cz.wie.najtmar.gop.client.ui.GoP;
import cz.wie.najtmar.gop.client.ui.R;
import cz.wie.najtmar.gop.client.view.CityView;
import cz.wie.najtmar.gop.common.Point2D;

/**
 * Created by najtmar on 19.12.16.
 */

public class CityInfoBox extends View {

  /**
   * Central model object.
   */
  private GoP mGoP;

  /**
   * Shared instance of class AndroidEngine, which may call back on the fragment.
   */
  private AndroidEngine mAndroidEngine;

  public CityInfoBox(Context context, AttributeSet attrs) {
    super(context, attrs);
    if (isInEditMode()) {
      return;
    }
    mGoP = GoP.get(context);
    mAndroidEngine = (AndroidEngine) mGoP.getInjector().provideOutputEngine();
  }

  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);

    if (isInEditMode()) return;

    final float boxWidth = canvas.getWidth();
    final float boxHeight = canvas.getHeight();
    final Point2D.Double boxCenter =
        new Point2D.Double(
            boxWidth / 2 / mAndroidEngine.getAndroidMagnifyX()
                - AndroidEngine.TRANSLATE_X,
            boxHeight / 2 / mAndroidEngine.getAndroidMagnifyY()
                - AndroidEngine.TRANSLATE_Y);
    final CityView city = mAndroidEngine.getCitySelected();
    final int[] cityBoxCapturedHalves = mAndroidEngine.getCityBoxCapturedHalves();
    if (cityBoxCapturedHalves == null || cityBoxCapturedHalves.length != city.getRoadSectionAngles().length) {
      return;
    }
    final double cityProductivity = mAndroidEngine.getCityProductivity();
    // Roads.
    for (int i = 0; i < city.getRoadSectionAngles().length; ++i) {
      final double roadSectionAngle = city.getRoadSectionAngles()[i];
      final double cos = Math.cos(roadSectionAngle);
      final double sin = Math.sin(roadSectionAngle);
      final Point2D.Double roadStart = new Point2D.Double(
          boxWidth / 2 + cos * boxWidth / 2 / 7, boxWidth / 2 + sin * boxWidth / 2 / 7);
      final Point2D.Double roadMiddle = new Point2D.Double(
          boxWidth / 2 + cos * boxWidth / 2 / 2, boxWidth / 2 + sin * boxWidth / 2 / 2);
      final Point2D.Double roadEnd = new Point2D.Double(
          boxWidth / 2 + cos * boxWidth / 2 * 6 / 7, boxWidth / 2 + sin * boxWidth / 2 * 6 / 7);
      final Paint paint0 = new Paint();
      paint0.setColor(Color.BLACK);
      paint0.setAntiAlias(true);
      final Paint paint1 = new Paint();
      paint1.setColor(Color.BLACK);
      paint1.setAntiAlias(true);
      switch (cityBoxCapturedHalves[i]) {
        case 0:
          paint0.setStrokeWidth(getContext().getResources().getDimension(R.dimen.city_box_road_width));
          paint1.setStrokeWidth(getContext().getResources().getDimension(R.dimen.city_box_road_width));
          break;
        case 1:
          paint0.setStrokeWidth(getContext().getResources().getDimension(R.dimen.city_box_road_width));
          paint1.setStrokeWidth(getContext().getResources().getDimension(R.dimen.discovered_road_width));
          break;
        case 2:
          paint0.setStrokeWidth(getContext().getResources().getDimension(R.dimen.discovered_road_width));
          paint1.setStrokeWidth(getContext().getResources().getDimension(R.dimen.discovered_road_width));
          break;
        default:
          paint0.setStrokeWidth(0f);
          paint1.setStrokeWidth(0f);
          break;
      }
      canvas.drawLine((float) roadStart.getX(), (float) roadStart.getY(),
          (float) roadMiddle.getX(), (float) roadMiddle.getY(), paint0);
      canvas.drawLine((float) roadMiddle.getX(), (float) roadMiddle.getY(),
          (float) roadEnd.getX(), (float) roadEnd.getY(), paint1);
    }
    // City description.
    final float textSize = getContext().getResources().getDimension(R.dimen.small_info_text_size);
    final float textX = (float) (textSize / 4 / mAndroidEngine.getAndroidMagnifyX() - AndroidEngine.TRANSLATE_X);
    final float line0TextY = (float) (textSize / mAndroidEngine.getAndroidMagnifyY() - AndroidEngine.TRANSLATE_Y);
    final float line1TextY = (float) (2 * textSize / mAndroidEngine.getAndroidMagnifyY() - AndroidEngine.TRANSLATE_Y);
    final float line2TextY = (float) ((boxHeight - textSize) / mAndroidEngine.getAndroidMagnifyY() - AndroidEngine.TRANSLATE_Y);
    mAndroidEngine.drawText(textX, line0TextY, getResources().getString(R.string.size) + ": " + city.getFactories().size(),
        textSize, Paint.Align.LEFT, Typeface.defaultFromStyle(Typeface.NORMAL), getResources().getColor(R.color.colorLightAccent),
        canvas);
    mAndroidEngine.drawText(textX, line1TextY, "(" + getResources().getString(R.string.max_size) + ": " + city.getRoadSectionAngles().length + ")",
        textSize, Paint.Align.LEFT, Typeface.defaultFromStyle(Typeface.NORMAL), getResources().getColor(R.color.colorLightAccent),
        canvas);
    mAndroidEngine.drawText(textX, line2TextY,
        getResources().getString(R.string.productivity) + (mAndroidEngine.getCityProductivity() < 1.0 ? ": " : ":")
            + (int) (mAndroidEngine.getCityProductivity() * 100) + "%",
        textSize, Paint.Align.LEFT, Typeface.defaultFromStyle(Typeface.NORMAL), getResources().getColor(R.color.colorLightAccent),
        canvas);
    // City.
    mAndroidEngine.drawPlayerOctangle(
        boxCenter.getX(), boxCenter.getY(), boxWidth / 7,
        getContext().getResources().getDimension(R.dimen.city_border_width),
    mAndroidEngine.getCurrentPlayerIndex(), canvas);
  }
}
