package cz.wie.najtmar.gop.client.ui.activities;

import androidx.fragment.app.Fragment;

import cz.wie.najtmar.gop.client.ui.fragments.ControlPanelBoardWarriorDirectionSelectionFragment;

/**
 * Created by najtmar on 02.11.16.
 */

public class BoardWarriorDirectionSelectionActivity extends GameFragmentsActivity {

  @Override
  protected Fragment createControlPanelFragment() {
    return new ControlPanelBoardWarriorDirectionSelectionFragment();
  }
}
