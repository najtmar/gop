package cz.wie.najtmar.gop.client.ui.activities;

import androidx.fragment.app.Fragment;

import cz.wie.najtmar.gop.client.ui.fragments.ControlPanelBoardInitializedFragment;

/**
 * Created by najtmar on 27.10.16.
 */

public class BoardInitializedActivity extends GameFragmentsActivity {

  @Override
  protected Fragment createControlPanelFragment() {
    return new ControlPanelBoardInitializedFragment();
  }
}
