package cz.wie.najtmar.gop.client.ui.views;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

import cz.wie.najtmar.gop.client.ui.AndroidEngine;
import cz.wie.najtmar.gop.client.ui.GoP;

/**
 * Created by najtmar on 24.10.16.
 */

public class BoardCityCreationView extends View {

  /**
   * Central model object.
   */
  private GoP mGoP;

  /**
   * Shared instance of class AndroidEngine, which may call back on the fragment.
   */
  private AndroidEngine mAndroidEngine;

  public BoardCityCreationView(Context context, AttributeSet attrs) {
    super(context, attrs);
    mGoP = GoP.get(context);
    mAndroidEngine = (AndroidEngine) mGoP.getInjector().provideOutputEngine();
  }

  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);
  }
}