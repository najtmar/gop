package cz.wie.najtmar.gop.client.ui;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.Semaphore;

import cz.wie.najtmar.gop.client.view.UserView;

/**
 * Central model class.
 */

public class GoP {
  private static GoP sGoP;

  /**
   * Equivalent of a Guice injector.
   */
  private Injector mInjector;

  private final Context mContext;

  /**
   * Central Semaphore used for UI Actions.
   */
  private final Semaphore mUiSemaphore;

  public static GoP get(Context context) {
    if (sGoP == null) {
      if (context == null) {
        throw new NullPointerException("GoP object not initialized.");
      }
      sGoP = new GoP(context);
    }
    return sGoP;
  }

  /*
  private final Injector mInjector;

  private AbstractModule createGopModule() {
    return new AbstractModule() {
      @Override
      protected void configure() {
        bind(PropertiesReader.class).to(StandardPropertiesReader.class);
        bind(BoardGenerator.class).to(MapBoardGenerator.class);
        // TODO(najtmar): Change this.
        bind(InitialPrawnDeployer.class).to(CrossBoardPrawnDeployer.class);
        bind(ConnectionManager.Factory.class).to(KryoNetCommunicator.Factory.class);
        bind(GameManagementEngine.class).to(AndroidGameManagementEngine.class);
        bind(InputEngine.class).to(AndroidEngine.class);
        bind(OutputEngine.class).to(AndroidEngine.class);
        bind(GameManagerManagedEngine.class).to(AndroidEngine.class);
        bind(Session.class).to(AndroidSession.class);
        bind(UserView.class).toInstance(mUser);
        bind(Locale.class).toInstance(Locale.UK);
      }

      @Provides
      @Named("random seed")
      Long provideRandomSeed() {
        return mRandom.nextLong();
      }
    };
  }
  */

  public Injector getInjector() {
    return mInjector;
  }

  public Context getContext() {
    return mContext;
  }

  /**
   * Acquires the central UI Semaphore .
   */
  public void acquireUiSemaphore() {
    try {
      mUiSemaphore.acquire();
    } catch (InterruptedException exception) {
      throw new RuntimeException(exception);
    }
  }

  /**
   * Releases the central UI Semaphore .
   */
  public void releaseUiSemaphore() {
    mUiSemaphore.release();
  }

  /**
   * Returns an instance of class GameManager .
   * @return an instance of class GameManager
   */

  /*
  public GameManager getGameManager() {
    return mInjector.getInstance(GameManager.class);
  }
  */

  private GoP(Context context) {
    mContext = context;
    mInjector = new Injector(context, Locale.UK);
    mUiSemaphore = new Semaphore(0);
  }
}
