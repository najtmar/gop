package cz.wie.najtmar.gop.client.ui.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import cz.wie.najtmar.gop.client.ui.AndroidEngine;
import cz.wie.najtmar.gop.client.ui.GoP;
import cz.wie.najtmar.gop.client.ui.R;

/**
 * Created by najtmar on 12.12.16.
 */

public class AbandonGameDialogFragment extends RobustDialogFragment {

  /**
   * Shared instance of class AndroidEngine, which may call back on the fragment.
   */
  private AndroidEngine mAndroidEngine;

  public static AbandonGameDialogFragment newInstance() {
    AbandonGameDialogFragment fragment = new AbandonGameDialogFragment();

    return fragment;
  }

  @NonNull
  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    return new AlertDialog.Builder(getActivity())
        .setTitle(R.string.abandon_game_prompt_title)
        .setMessage(R.string.abandon_game_prompt_message)
        .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            mAndroidEngine.closeGameAbandoningPrompt();
          }
        })
        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            mAndroidEngine.abandonGame();
          }
        })
        .create();
  }

  public AbandonGameDialogFragment() {
    final GoP gop = GoP.get(getContext());
    mAndroidEngine = (AndroidEngine) gop.getInjector().provideOutputEngine();
  }

  @Override
  protected void fallback() {
    mAndroidEngine.abandonGame();
  }
}
