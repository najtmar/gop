package cz.wie.najtmar.gop.client.ui.activities;

import androidx.fragment.app.Fragment;

import cz.wie.najtmar.gop.client.ui.fragments.ControlPanelBoardFactoryFragment;

/**
 * Created by najtmar on 28.10.16.
 */

public class BoardFactoryActivity extends GameFragmentsActivity {

  @Override
  protected Fragment createControlPanelFragment() {
    return new ControlPanelBoardFactoryFragment();
  }
}
