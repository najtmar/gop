package cz.wie.najtmar.gop.client.ui;

import android.util.Log;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.concurrent.GuardedBy;

import cz.wie.najtmar.gop.client.view.ClientViewException;

/**
 * A wrapper around class GameManager, which runs operations asynchronously in separate Threads .
 */

public class AsyncGameManager {

  /**
   * Wrapped instance of GameManager .
   */
  private final GameManager mGameManager;

  /**
   * The List of Exceptions thrown during the execution.
   */
  @GuardedBy("this")
  private final List<ClientViewException> mExceptions;

  public AsyncGameManager(GameManager gameManager) {
    mGameManager = gameManager;
    mExceptions = new LinkedList<>();
  }

  /**
   * Waits for the GameManager to become initialized.
   * @throws ClientViewException when waiting is interrupted
   */
  private void waitForInitialized () throws ClientViewException {
    if (mGameManager.getState() == GameManager.State.NOT_INITIALIZED) {
      Log.i("AsyncGameManager", "Waiting for mGameManager to get initialized.");
    }
    while (mGameManager.getState() == GameManager.State.NOT_INITIALIZED) {
      synchronized (this) {
        try {
          wait(10);
        } catch (InterruptedException exception) {
          throw new ClientViewException("Interrupted while waiting for mInitialized .", exception);
        }
      }
    }
  }

  /**
   * Waits for the Game intent to become initiated.
   */
  private void waitForGameIntentInitiated () throws ClientViewException {
    if (mGameManager.getState() != GameManager.State.GAME_INITIATED) {
      Log.i("AsyncGameManager", "Waiting for mGameManager to enter State GAME_INITIATED .");
    }
    while (mGameManager.getState() != GameManager.State.GAME_INITIATED) {
      synchronized (this) {
        try {
          wait(10);
        } catch (InterruptedException exception) {
          throw new ClientViewException("Interrupted while waiting for mGameIntentInitiated .", exception);
        }
      }
    }
  }

  /**
   * Reports an exception by printing it out and adding to the List .
   * @param exception the Exception to report
   */
  private void reportException(ClientViewException exception) {
    Log.e("Async", "GameManager", exception);
    synchronized (this) {
      mExceptions.add(exception);
    }
  }

  public void asyncInitialize() {
    new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          mGameManager.initialize();
        } catch (ClientViewException exception) {
          reportException(exception);
        }
      }
    }).start();
  }

  public void close() {
    mGameManager.close();
  }

  public void asyncInitiateGameIntent() {
    new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          waitForInitialized();
          mGameManager.initiateGameIntent();
        } catch (ClientViewException exception) {
          synchronized (this) {
            reportException(exception);
          }
        }
      }
    }).start();
  }

  public void asyncInitializeGame() {
    new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          waitForInitialized();
          waitForGameIntentInitiated();
          mGameManager.initializeGame();
        } catch (ClientViewException exception) {
          synchronized (this) {
            reportException(exception);
          }
        }
      }
    }).start();
  }

  public void asyncCancelGameIntent() {
    new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          waitForInitialized();
          waitForGameIntentInitiated();
          mGameManager.cancelGameIntent();
        } catch (ClientViewException exception) {
          synchronized (this) {
            reportException(exception);
          }
        }
      }
    }).start();
  }

  public void asyncRefreshPendingGamesList() {
    new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          waitForInitialized();
          mGameManager.refreshPendingGamesList();
        } catch (ClientViewException exception) {
          synchronized (this) {
            reportException(exception);
          }
        }
      }
    }).start();
  }

  public void asyncGetPendingGames() {
    new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          waitForInitialized();
          mGameManager.getPendingGames();
        } catch (ClientViewException exception) {
          synchronized (this) {
            reportException(exception);
          }
        }
      }
    }).start();
  }

  public void asyncJoinPendingGame(final int index) {
    new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          waitForInitialized();
          mGameManager.joinPendingGame(index);
        } catch (ClientViewException exception) {
          synchronized (this) {
            reportException(exception);
          }
        }
      }
    }).start();
  }

  public void asyncLeaveGameIntent() {
    new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          waitForInitialized();
          mGameManager.leaveGameIntent();
        } catch (ClientViewException exception) {
          synchronized (this) {
            reportException(exception);
          }
        }
      }
    }).start();
  }

  public void asyncLeaveGamesListed() {
    new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          waitForInitialized();
          mGameManager.leaveGamesListed();
        } catch (ClientViewException exception) {
          synchronized (this) {
            reportException(exception);
          }
        }
      }
    }).start();
  }

  public GameManager getGameManager() {
    return mGameManager;
  }

  public GameManager.State getState() { return mGameManager.getState(); }

  public synchronized List<ClientViewException> getExceptions() {
    return mExceptions;
  }

  public synchronized void clearExceptions() {
    mExceptions.clear();
  }
}
