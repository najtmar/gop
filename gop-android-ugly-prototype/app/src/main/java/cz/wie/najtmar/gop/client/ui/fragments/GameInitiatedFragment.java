package cz.wie.najtmar.gop.client.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.List;

import androidx.annotation.Nullable;
import cz.wie.najtmar.gop.client.ui.AndroidGameManagementEngine;
import cz.wie.najtmar.gop.client.ui.AsyncGameManager;
import cz.wie.najtmar.gop.client.ui.GoP;
import cz.wie.najtmar.gop.client.ui.R;
import cz.wie.najtmar.gop.client.ui.activities.BoardInitializedActivity;
import cz.wie.najtmar.gop.client.ui.activities.InitialActivity;
import cz.wie.najtmar.gop.server.KryoNetCommunicator;

public class GameInitiatedFragment extends MenuFragment {

  // UI elements.
  private ListView mGameInitiatedParticipantsList;
  private Button mGameInitiatedStartButton;
  private Button mGameInitiatedCancelButton;

  /**
   * Central model object.
   */
  private GoP mGoP;

  /**
   * Game manager.
   */
  private AsyncGameManager mAsyncGameManager;

  /**
   * Shared instance of class mAndroidGameManagementEngine, which may call back on the fragment.
   */
  private AndroidGameManagementEngine mAndroidGameManagementEngine;

  @Override
  public synchronized void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mGoP = GoP.get(getContext());
    mAsyncGameManager = new AsyncGameManager(mGoP.getInjector().provideGameManager());
    mAndroidGameManagementEngine =
        (AndroidGameManagementEngine) mGoP.getInjector().provideGameManagementEngine();
    mAndroidGameManagementEngine.setGameInitiatedFragment(this);
  }

  private void scheduleDelayedEnableStartAndCancel() {
    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {
        if (mGameInitiatedStartButton != null) {
          mGameInitiatedStartButton.setEnabled(true);
        }
        if (mGameInitiatedCancelButton != null) {
          mGameInitiatedCancelButton.setEnabled(true);
        }
      }
    }, KryoNetCommunicator.getConnectionTimeoutMillis() * 2);
  }

  private void initializeControls(View view) {
    mGameInitiatedParticipantsList = (ListView) view.findViewById(R.id.init_game_participants);
    mGameInitiatedParticipantsList.setAdapter(new ArrayAdapter<String>(getContext(),
        android.R.layout.simple_list_item_1));

    mGameInitiatedStartButton = (Button) view.findViewById(R.id.init_game_start);
    mGameInitiatedStartButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        mAsyncGameManager.asyncInitializeGame();
        mGameInitiatedStartButton.setEnabled(false);
        mGameInitiatedCancelButton.setEnabled(false);
        scheduleDelayedEnableStartAndCancel();
      }
    });
    mGameInitiatedStartButton.setEnabled(false);

    mGameInitiatedCancelButton = (Button) view.findViewById(R.id.init_game_cancel);
    mGameInitiatedCancelButton.setOnClickListener(
        new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            mAsyncGameManager.asyncCancelGameIntent();
            mGameInitiatedStartButton.setEnabled(false);
            mGameInitiatedCancelButton.setEnabled(false);
            scheduleDelayedEnableStartAndCancel();
          }
        });
  }

  @Nullable
  @Override
  public synchronized View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                                        @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.activity_init_game, container, false);
    initializeControls(view);

    return view;
  }

  /**
   * Updates the list of participants .
   * @param enabled whether the Game start is enabled
   * @param participantsList the list of participants to be set
   */
  public synchronized void updateParticipantsList(final boolean enabled,
                                                  final List<String> participantsList) {
    if (mGameInitiatedParticipantsList == null) {
      return;
    }
    final ArrayAdapter<String>
        listAdapter = (ArrayAdapter) mGameInitiatedParticipantsList.getAdapter();
    getActivity().runOnUiThread(new Runnable() {
      @Override
      public void run() {
        mGameInitiatedStartButton.setEnabled(enabled);
        listAdapter.clear();
        listAdapter.addAll(participantsList);
        mGameInitiatedStartButton.setEnabled(enabled);
      }
    });
  }

  public synchronized void initializeBoardInitialized() {
    Intent intent = new Intent(getActivity(), BoardInitializedActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

  public synchronized void startInitialActivity() {
    Intent intent = new Intent(getActivity(), InitialActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }
}
