package cz.wie.najtmar.gop.client.ui.fragments;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import cz.wie.najtmar.gop.client.ui.R;
import cz.wie.najtmar.gop.client.ui.activities.BoardCityCreationActivity;
import cz.wie.najtmar.gop.client.view.JointView;

/**
 * Created by najtmar on 21.10.16.
 */

public class ControlPanelBoardSettlersUnitFragment extends ControlPanelBoardPrawnFragment {

  // UI elements.
  private Button mBuildCityButton;

  @Override
  protected void initializePrawnSpecificControls(View view) {
    mBuildCityButton = (Button) view.findViewById(R.id.board_prawn_build_city);
    mBuildCityButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getAndroidEngine().startCityCreation();
      }
    });
    mBuildCityButton.setEnabled(false);
  }

  @Override
  protected int getControlPanelLayout() {
    return R.layout.control_panel_board_settlers_unit;
  }

  @Override
  public void updateState() {
    final JointView joint = getAndroidEngine().getPrawnSelected().getCurrentPosition().getJoint();
    mBuildCityButton.setEnabled(joint != null && joint.getCity() == null);
  }

  /**
   * Starts Activity CityCreation .
   */
  public void startCityCreationActivity() {
    Intent intent = new Intent(getActivity(), BoardCityCreationActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

}
