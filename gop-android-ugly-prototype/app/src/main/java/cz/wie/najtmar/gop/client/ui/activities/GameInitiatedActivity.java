package cz.wie.najtmar.gop.client.ui.activities;

import androidx.fragment.app.Fragment;

import cz.wie.najtmar.gop.client.ui.fragments.GameInitiatedFragment;

public class GameInitiatedActivity extends SingleFragmentActivity {
  @Override
  protected Fragment createFragment() {
    return new GameInitiatedFragment();
  }
}
