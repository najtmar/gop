package cz.wie.najtmar.gop.client.ui.fragments;

import android.os.Bundle;

/**
 * Created by najtmar on 10.01.17.
 */

public class GameRulesFragment extends GameHelpFragment {

  public static GameRulesFragment newInstance(String assetsName, String name, int pos, int total) {
    Bundle args = new Bundle();
    args.putSerializable(ARG_ASSETS_NAME, assetsName);
    args.putSerializable(ARG_NAME, name);
    args.putSerializable(ARG_POS, pos);
    args.putSerializable(ARG_TOTAL, total);

    GameRulesFragment fragment = new GameRulesFragment();
    fragment.setArguments(args);
    return fragment;
  }


}
