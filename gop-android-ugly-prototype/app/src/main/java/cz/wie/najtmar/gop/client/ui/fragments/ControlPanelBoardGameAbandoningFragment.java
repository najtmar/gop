package cz.wie.najtmar.gop.client.ui.fragments;

import android.view.View;
import android.widget.Button;

import cz.wie.najtmar.gop.client.ui.R;

/**
 * Created by najtmar on 15.11.16.
 */

public class ControlPanelBoardGameAbandoningFragment extends ControlPanelBoardFragment {

  // UI elements.
  private Button mOkButton;
  private Button mCancelButton;

  @Override
  protected void initializeControls(View view) {
    mOkButton = (Button) view.findViewById(R.id.board_game_abandoning_ok);
    mOkButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getAndroidEngine().abandonGame();
      }
    });
    mCancelButton = (Button) view.findViewById(R.id.board_game_abandoning_cancel);
    mCancelButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getAndroidEngine().closeGameAbandoningPrompt();
      }
    });
  }

  @Override
  protected int getControlPanelLayout() {
    return R.layout.control_panel_board_game_abandoning;
  }

  @Override
  public void onControlPanelBoardFragmentResume() {
    getAndroidEngine().setControlPanelBoardGameAbandoningFragment(this);
  }

  @Override
  public void onControlPanelBoardFragmentDestroy() {
    getAndroidEngine().setControlPanelBoardGameAbandoningFragment(null);
  }

  @Override
  public void updateState() {
  }

  public void finishBoardGameAbandoningActivity() {
    getActivity().finish();
  }
}
