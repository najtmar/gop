package cz.wie.najtmar.gop.client.ui.activities;

import androidx.fragment.app.Fragment;

import cz.wie.najtmar.gop.client.ui.fragments.ControlPanelBoardItineraryBuiltFragment;

/**
 * Created by najtmar on 22.10.16.
 */

public class BoardItineraryBuiltActivity extends GameFragmentsActivity {

  @Override
  protected Fragment createControlPanelFragment() {
    return new ControlPanelBoardItineraryBuiltFragment();
  }
}
