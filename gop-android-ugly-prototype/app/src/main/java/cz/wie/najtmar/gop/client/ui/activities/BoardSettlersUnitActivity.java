package cz.wie.najtmar.gop.client.ui.activities;

import androidx.fragment.app.Fragment;

import cz.wie.najtmar.gop.client.ui.fragments.ControlPanelBoardSettlersUnitFragment;

/**
 * Created by najtmar on 21.10.16.
 */

public class BoardSettlersUnitActivity extends GameFragmentsActivity {

  @Override
  protected Fragment createControlPanelFragment() {
    return new ControlPanelBoardSettlersUnitFragment();
  }
}