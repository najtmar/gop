package cz.wie.najtmar.gop.client.ui;

import com.google.inject.Provider;

import java.util.Random;

import cz.wie.najtmar.gop.client.view.TextManager;
import cz.wie.najtmar.gop.client.view.UserView;
import cz.wie.najtmar.gop.game.PropertiesReader;
import cz.wie.najtmar.gop.server.GameSession;
import cz.wie.najtmar.gop.server.KryoNetCommunicator;

/**
 * Provider for class GameManager .
 */

public class GameManagerProvider {

  private static GameManager sGameManager;

  public static GameManager provideGameManagerSingleton(GameManagementEngine gameManagerEngine,
                                                        Client gameClient,
                                                        GameManagerManagedEngine gameEngine,
                                                        PropertiesReader propertiesReader,
                                                        Session uiSession,
                                                        KryoNetCommunicator communicationServer,
                                                        UserView user,
                                                        Provider<GameSession> gameSessionProvider,
                                                        TextManager textManager) {
    if (sGameManager == null) {
      sGameManager = new GameManager(gameManagerEngine, gameClient, gameEngine, propertiesReader,
          uiSession,
          communicationServer, user, gameSessionProvider, textManager);
    }
    return sGameManager;
  }
}
