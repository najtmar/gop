package cz.wie.najtmar.gop.client.ui;

import javax.annotation.Nonnull;

import cz.wie.najtmar.gop.client.view.ClientViewException;
import cz.wie.najtmar.gop.client.view.GameView;
import cz.wie.najtmar.gop.client.view.PlayerView;
import cz.wie.najtmar.gop.event.Event;

/**
 * UI Session to run Games on Android .
 */

public class AndroidSession implements Session {

  /**
   * The UI Client managing the User view of the Game .
   */
  private Client mClient;

  /**
   * Central model object.
   */
  private GoP mGoP;

  /**
   * The AndroidEngine instance used for actual drawing.
   */
  private AndroidEngine mAndroidEngine;

  @Override
  public void initialize(@Nonnull Client client, @Nonnull GameView game) throws ClientViewException {
    mClient = client;
    mClient.initialize(game);
    mGoP = GoP.get(null);
    mAndroidEngine = (AndroidEngine) mGoP.getInjector().provideOutputEngine();
  }

  @Override
  public Event[] processEvents(@Nonnull Event[] inputEvents, @Nonnull PlayerView player)
      throws ClientViewException {
    final Event[] userEvents;
    if (inputEvents.length > 0) {
      mClient.snapshot(inputEvents);

      // Paint.
      mAndroidEngine.initializeDrawableBitmaps();
      mClient.paint();
      mAndroidEngine.invalidateViews();

      userEvents = mClient.interact(inputEvents);
    } else {
      userEvents = new Event[]{};
    }

    return userEvents;
  }
}
