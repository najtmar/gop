package cz.wie.najtmar.gop.client.ui.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;

import androidx.annotation.NonNull;
import cz.wie.najtmar.gop.client.ui.AndroidEngine;
import cz.wie.najtmar.gop.client.ui.GoP;

/**
 * Created by najtmar on 18.10.16.
 */

public class BoardView extends View {

  /**
   * Central model object.
   */
  private GoP mGoP;

  /**
   * Shared instance of class AndroidEngine, which may call back on the fragment.
   */
  private AndroidEngine mAndroidEngine;

  // Animation parameters.
  private Matrix mAnimateStart;
  private Matrix mTranslate;
  private Interpolator mAnimateInterpolator;
  private long mStartTime;
  private long mEndTime;
  private float mTotalAnimDx;
  private float mTotalAnimDy;

  /**
   * Used to react to the gestures.
   */
  private final GestureDetector mGestureDetector;

  public BoardView(Context context, AttributeSet attrs) {
    super(context, attrs);
    mTranslate = new Matrix();
    final GestureListener gestureListener = new GestureListener();
    mGestureDetector = new GestureDetector(context, gestureListener, null, true);
    if (isInEditMode()) return;
    mGoP = GoP.get(context);
    mAndroidEngine = (AndroidEngine) mGoP.getInjector().provideOutputEngine();
  }

  /**
   * Calculates and returns the X-coordinate of the real upper-left corner of the Board
   * @param bitmap the full Bitmap to be drawn
   * @param canvas the Canvas on which Bitmap will be drawn
   * @return the X-coordinate of the real upper-left corner of the Board
   */
  private float calculateRealBoardMinX(Bitmap bitmap, @NonNull Canvas canvas) {
    float realBoardMinX = mAndroidEngine.getRealBoardMinX();
    if (bitmap.getWidth() - canvas.getWidth() < -realBoardMinX) {
      realBoardMinX = -(bitmap.getWidth() - canvas.getWidth());
    }
    if (-realBoardMinX < 0) {
      realBoardMinX = 0;
    }
    mAndroidEngine.setRealBoardMinX(realBoardMinX);
    return realBoardMinX;
  }

  /**
   * Calculates and returns the Y-coordinate of the real upper-left corner of the Board
   * @param bitmap the full Bitmap to be drawn
   * @param canvas the Canvas on which Bitmap will be drawn
   * @return the Y-coordinate of the real upper-left corner of the Board
   */
  private float calculateRealBoardMinY(Bitmap bitmap, @NonNull Canvas canvas) {
    float realBoardMinY = mAndroidEngine.getRealBoardMinY();
    if (bitmap.getHeight() - canvas.getHeight() < -realBoardMinY) {
      realBoardMinY = -(bitmap.getHeight() - canvas.getHeight());
    }
    if (-realBoardMinY < 0) {
      realBoardMinY = 0;
    }
    mAndroidEngine.setRealBoardMinY(realBoardMinY);
    return realBoardMinY;
  }

  /**
   * Handles moving the Board .
   * @param distanceX the X part of the move
   * @param distanceY the Y part of the move
   */
  private void onMove(float distanceX, float distanceY) {
    mAndroidEngine.setRealBoardMinX(mAndroidEngine.getRealBoardMinX() + distanceX);
    mAndroidEngine.setRealBoardMinY(mAndroidEngine.getRealBoardMinY() + distanceY);
    invalidate();
  }

  /**
   * Handles moving the Board in an animated way.
   * @param dx the X part of the move
   * @param dy the Y part of the move
   * @param duration the duration in milliseconds of the move
   */
  private void onAnimateMove(float dx, float dy, long duration) {
    mAnimateStart = new Matrix(mTranslate);
    mAnimateInterpolator = new OvershootInterpolator();
    mStartTime = android.os.SystemClock.elapsedRealtime();
    mEndTime = mStartTime + duration;
    mTotalAnimDx = dx;
    mTotalAnimDy = dy;
    post(new Runnable() {
      @Override
      public void run() {
        onAnimateStep();
      }
    });
  }

  /**
   * Handles a single animation step.
   */
  private void onAnimateStep() {
    long currTime = android.os.SystemClock.elapsedRealtime();
    float percentTime = (float) (currTime - mStartTime) / (float) (mEndTime - mStartTime);
    float percentDistance = mAnimateInterpolator.getInterpolation(percentTime);
    float currDx = percentDistance * mTotalAnimDx;
    float currDy = percentDistance * mTotalAnimDy;
    mTranslate.set(mAnimateStart);
    onMove(currDx, currDy);

    if (percentTime < 1.0f) {
      post(new Runnable() {
        @Override
        public void run() {
          onAnimateStep();
        }
      });
    }
  }

  @Override
  public boolean onTouchEvent(MotionEvent event) {
    boolean result = false;
    result = mGestureDetector.onTouchEvent(event);
    return result;
  }

  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);
    if (isInEditMode()) return;
    final Bitmap bottomBitmap = mAndroidEngine.generateBottomBitmap();
    if (bottomBitmap != null) {
      mAndroidEngine.tryInitializeGameViewport(canvas.getWidth(), canvas.getHeight());
      final float realBoardMinX = calculateRealBoardMinX(bottomBitmap, canvas);
      final float realBoardMinY = calculateRealBoardMinY(bottomBitmap, canvas);
      canvas.drawBitmap(bottomBitmap, realBoardMinX, realBoardMinY, null);
    }
  }

  private class GestureListener extends GestureDetector.SimpleOnGestureListener {
    @Override
    public boolean onDown(MotionEvent event) {
      return true;
    }

    @Override
    public boolean onScroll(MotionEvent event0, MotionEvent event1, float distanceX,
                            float distanceY) {
      onMove(-distanceX, -distanceY);
      return true;
    }

    @Override
    public boolean onFling(MotionEvent event0, MotionEvent event1, final float velocityX,
                           final float velocityY) {
      final float distanceTimeFactor = 0.004f;
      final float totalDx = (distanceTimeFactor * velocityX / 2);
      final float totalDy = (distanceTimeFactor * velocityY / 2);
      onAnimateMove(totalDx, totalDy, (long) (1000 * distanceTimeFactor));
      return true;
    }

    @Override
    public void onShowPress(MotionEvent e) {
      mAndroidEngine.onBoardShortPress(e.getX(), e.getY());
    }
  }
}
