package cz.wie.najtmar.gop.client.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import cz.wie.najtmar.gop.client.ui.AndroidGameManagementEngine;
import cz.wie.najtmar.gop.client.ui.AsyncGameManager;
import cz.wie.najtmar.gop.client.ui.GameManager;
import cz.wie.najtmar.gop.client.ui.activities.GameJoinedActivity;
import cz.wie.najtmar.gop.client.ui.GoP;
import cz.wie.najtmar.gop.client.ui.activities.GameInitiatedActivity;
import cz.wie.najtmar.gop.client.ui.activities.JoinActivity;
import cz.wie.najtmar.gop.client.ui.R;

/**
 * Created by najtmar on 11.10.16.
 */

public class InitialFragment extends MenuFragment {

  // UI elements.
  private Button mInitGameButton;
  private Button mJoinGameButton;
  private Button mExitButton;

  /**
   * Central model object.
   */
  private GoP mGoP;

  /**
   * Game manager.
   */
  private AsyncGameManager mAsyncGameManager;

  /**
   * Shared instance of class AndroidGameManagementEngine, which may call back on the fragment.
   */
  private AndroidGameManagementEngine mAndroidGameManagementEngine;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mGoP = GoP.get(getContext());
    mAsyncGameManager = new AsyncGameManager(mGoP.getInjector().provideGameManager());
    if (mAsyncGameManager.getState() == GameManager.State.NOT_INITIALIZED) {
      mAsyncGameManager.asyncInitialize();
    }
  }

  private void initializeControls(View view) {
    mInitGameButton = (Button) view.findViewById(R.id.initial_init_game);
    mInitGameButton.setOnClickListener(
        new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            mAsyncGameManager.asyncInitiateGameIntent();
          }
        });

    mJoinGameButton = (Button) view.findViewById(R.id.initial_join_game);
    mJoinGameButton.setOnClickListener(
        new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            Intent intent = new Intent(getActivity(), JoinActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            mAsyncGameManager.asyncGetPendingGames();
          }
        });

    mExitButton = (Button) view.findViewById(R.id.initial_exit);
    mExitButton.setOnClickListener(
        new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            InitialFragment.this.getActivity().finish();
          }
        });
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.activity_initial, container, false);
    initializeControls(view);
    mAndroidGameManagementEngine =
        (AndroidGameManagementEngine) mGoP.getInjector().provideGameManagementEngine();
    mAndroidGameManagementEngine.setInitialFragment(this);

    return view;
  }

  /**
   * Initializes GAME_JOINED UI.
   */
  public void initializeJoinGame() {
    Intent intent = new Intent(getActivity(), GameJoinedActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

  /**
   * Initializes UI for State GAME_INITIATED.
   */
  public synchronized void initializeGameInitiated() {
    Intent intent = new Intent(getActivity(), GameInitiatedActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }
}
