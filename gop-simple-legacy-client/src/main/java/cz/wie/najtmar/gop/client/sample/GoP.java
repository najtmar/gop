package cz.wie.najtmar.gop.client.sample;

import cz.wie.najtmar.gop.client.view.GameView;
import cz.wie.najtmar.gop.client.view.JointView;
import cz.wie.najtmar.gop.client.view.RoadSectionView;
import cz.wie.najtmar.gop.common.Point2D;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;

import javax.json.Json;
import javax.json.JsonObject;

/**
 * Sample client of Game of Prawns. Used for testing and development.
 * @author najtmar
 */
public class GoP extends Application {

  final GameView game;

  /**
   * Constructor.
   * @throws Exception when the object creation fails
   */
  public GoP() throws Exception {
    String sampleBoardSerializedString = null;
    try (final BufferedReader reader =
        new BufferedReader(
          new InputStreamReader(
              GoP.class.getResourceAsStream("/cz/wie/najtmar/gop/client/sample/sample-board.json")))) {
      StringBuilder stringBuilder = new StringBuilder();
      String line;

      while ((line = reader.readLine()) != null) {
        stringBuilder.append(line);
      }
      sampleBoardSerializedString = stringBuilder.toString();
    }

    JsonObject sampleBoardSerialized = Json.createReader(new StringReader(sampleBoardSerializedString)).readObject();
    this.game = new GameView(sampleBoardSerialized, 0);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    System.out.println("Launching sample JavaFX client of Game of Prawns:\n" + game);

    primaryStage.setTitle("Game of Prawns");

    FlowPane rootNode = new FlowPane();

    Scene rootScene = new Scene(rootNode, game.getXsize(), game.getYsize());

    primaryStage.setScene(rootScene);

    // Draw the initial scene.
    Canvas canvas = new Canvas(game.getXsize(), game.getYsize());
    GraphicsContext gc = canvas.getGraphicsContext2D();
    for (RoadSectionView roadSectionView : game.getRoadSections()) {
      final JointView[] joints = roadSectionView.getJoints();
      final Point2D.Double startPoint = joints[0].getCoordinatesPair();
      final Point2D.Double endPoint = joints[1].getCoordinatesPair();
      gc.strokeLine(startPoint.getX(), startPoint.getY(), endPoint.getX(), endPoint.getY());
    }

    rootNode.getChildren().add(canvas);

    primaryStage.show();
  }

}
