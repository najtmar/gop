package cz.wie.najtmar.gop.game

import cz.wie.najtmar.gop.board.Board
import cz.wie.najtmar.gop.board.Joint
import cz.wie.najtmar.gop.board.Position
import cz.wie.najtmar.gop.board.RoadSection
import cz.wie.najtmar.gop.common.Point2D
import cz.wie.najtmar.gop.event.Event
import spock.lang.Specification

import javax.json.Json

class EventFactorySpec extends Specification {

    static final int START_TIME = 123456

    EventFactory objectUnderTest

    def setup() {
        Prawn.firstAvailableId = 0
        City.firstAvailableId = 0
        Properties simpleBoardPropertiesSet = new Properties()
        simpleBoardPropertiesSet.load(GameSpec.class.getResourceAsStream(
                "/cz/wie/najtmar/gop/board/simple-board.properties"))
        Board board = new Board(simpleBoardPropertiesSet)
        Joint joint0 = (new Joint.Builder())
                .setBoard(board)
                .setIndex(0)
                .setName("joint0")
                .setCoordinatesPair(new Point2D.Double(0.0, 0.0))
                .build()
        Joint joint1 = (new Joint.Builder())
                .setBoard(board)
                .setIndex(1)
                .setName("joint1")
                .setCoordinatesPair(new Point2D.Double(3.0, 0.0))
                .build()
        Joint joint2 = (new Joint.Builder())
                .setBoard(board)
                .setIndex(2)
                .setName("joint2")
                .setCoordinatesPair(new Point2D.Double(3.0, 3.0))
                .build()
        board.addJoint(joint0)
        board.addJoint(joint1)
        board.addJoint(joint2)
        RoadSection roadSection0 = (new RoadSection.Builder())
                .setBoard(board)
                .setIndex(0)
                .setJoints(joint0, joint1)
                .build()
        RoadSection roadSection1 = (new RoadSection.Builder())
                .setBoard(board)
                .setIndex(1)
                .setJoints(joint1, joint2)
                .build()
        board.addRoadSection(roadSection0)
        board.addRoadSection(roadSection1)

        Properties simpleGamePropertiesSet = new Properties()
        simpleGamePropertiesSet.load(GameSpec.class.getResourceAsStream(
                "/cz/wie/najtmar/gop/game/simple-game.properties"))
        simpleGamePropertiesSet.setProperty(GameProperties.START_TIME_PROPERTY_NAME, "" + START_TIME)
        Game game = new Game(simpleGamePropertiesSet)
        Player playerA = new Player(new User("playerA", new UUID(12345, 67890)), 0, game)
        Player playerB = new Player(new User("playerB", new UUID(98765, 43210)), 1, game)
        Player playerC = new Player(new User("playerC", new UUID(12345, 54321)), 2, game)
        game.addPlayer(playerA)
        game.addPlayer(playerB)
        game.addPlayer(playerC)

        Warrior warriorA0 = (new Warrior.Builder())
                .setCreationTime(START_TIME)
                .setGameProperties(game.getGameProperties())
                .setPlayer(playerA)
                .setCurrentPosition(new Position(roadSection0, 0))
                .setDirection(0.0)
                .build()
        warriorA0.setCurrentPosition(new Position(roadSection0, 1))
        Warrior warriorB0 = (new Warrior.Builder())
                .setCreationTime(START_TIME)
                .setGameProperties(game.getGameProperties())
                .setPlayer(playerB)
                .setCurrentPosition(new Position(roadSection0, 0))
                .setDirection(0.0)
                .build()
        warriorB0.setCurrentPosition(new Position(roadSection0, 2))

        Battle battle0 = new Battle(warriorA0, new Position(roadSection0, 1), new Position(roadSection0, 2),
                warriorB0, new Position(roadSection0, 2), new Position(roadSection0, 1))

        game.setBoard(board);
        game.initialize();
        game.addPrawn(warriorA0)
        game.addPrawn(warriorB0)
        game.addBattle(battle0);
        new City.Builder()
                .setGame(game)
                .setJoint(joint0)
                .setName("cityA")
                .setPlayer(playerA)
                .build()
        new City.Builder()
                .setGame(game)
                .setJoint(joint1)
                .setName("cityB")
                .setPlayer(playerB)
                .build()
        game.start()

        ObjectSerializer objectSerializer = new ObjectSerializer(game)
        objectUnderTest = new EventFactory(objectSerializer)
    }

    def "should create restore game event"() {
        when: "creating restore game event"
        def restoreGameEvent = objectUnderTest.createRestoreGameEvent()

        then: "the event has expected format"
        restoreGameEvent.getType() == Event.Type.RESTORE_GAME
        restoreGameEvent.getTime() == START_TIME
        restoreGameEvent.body == Json.createReader(new StringReader(
                """
{
  "type": "RESTORE_GAME",
  "time": 123456,
  "game": {
    "gameProperties": {
      "gamePropertiesSet": {
        "City.growActionProgressFactor": "0.2",
        "City.produceActionProgressFactor": "0.667",
        "City.repairActionProgressFactor": "0.333",
        "Factory.maxProductionDelta": "0.125",
        "Game.interruptTimePeriod": "100",
        "Game.maxPlayerCount": "4",
        "Game.startTime": "123456",
        "Game.timeDelta": "1",
        "Prawn.attackSuccessProbability": "0.8",
        "Prawn.basicVelocity": "1.0",
        "Prawn.velocityDirContribFactor": "2.0",
        "Warrior.maxAttackStrength": "0.3"
      },
      "maxPlayerCount": 4,
      "startTime": 123456,
      "timeDelta": 1,
      "interruptTimePeriod": 100,
      "basicVelocity": 1.0,
      "velocityDirContribFactor": 2.0,
      "produceActionProgressFactor": 0.667,
      "repairActionProgressFactor": 0.333,
      "growActionProgressFactor": 0.2,
      "attackSuccessProbability": 0.8,
      "maxAttackStrength": 0.3,
      "maxProductionDelta": 0.125
    },
    "state": "RUNNING",
    "id": "${objectUnderTest.game.id.toString()}",
    "timestamp": ${objectUnderTest.game.timestamp},
    "time": 123456,
    "result": null,
    "playersByState": null,
    "board": {
      "boardPropertiesSet": {
        "propertiesSet": {
          "Board.backgroundImage": "/images/gop-background.jpg",
          "Board.stepSize": "1.0",
          "Board.xsize": "15",
          "Board.ysize": "10"
        },
        "stepSize": 1.0,
        "backgroundImage": "/images/gop-background.jpg",
        "xsize": 15,
        "ysize": 10,
        "libGdx": false
      },
      "joints": [
        {
          "index": 0,
          "name": "joint0",
          "coordinatesPair": {
            "xcoord": 0.0,
            "ycoord": 0.0
          }
        },
        {
          "index": 1,
          "name": "joint1",
          "coordinatesPair": {
            "xcoord": 3.0,
            "ycoord": 0.0
          }
        },
        {
          "index": 2,
          "name": "joint2",
          "coordinatesPair": {
            "xcoord": 3.0,
            "ycoord": 3.0
          }
        }
      ],
      "roadSections": [
        {
          "index": 0,
          "joints": [
            0,
            1
          ],
          "positionsCount": 4,
          "length": 3.0,
          "stepSize": 1.0,
          "angle": 0.0,
          "halves": [
            {
              "end": "BACKWARD",
              "hashCode": 962
            },
            {
              "end": "FORWARD",
              "hashCode": 961
            }
          ]
        },
        {
          "index": 1,
          "joints": [
            1,
            2
          ],
          "positionsCount": 4,
          "length": 3.0,
          "stepSize": 1.0,
          "angle": 1.5707963267948966,
          "halves": [
            {
              "end": "BACKWARD",
              "hashCode": 993
            },
            {
              "end": "FORWARD",
              "hashCode": 992
            }
          ]
        }
      ]
    },
    "players": [
      {
        "user": {
          "name": "playerA",
          "id": "00000000-0000-3039-0000-000000010932"
        },
        "state": "IN_GAME",
        "index": 0
      },
      {
        "user": {
          "name": "playerB",
          "id": "00000000-0001-81cd-0000-00000000a8ca"
        },
        "state": "IN_GAME",
        "index": 1
      },
      {
        "user": {
          "name": "playerC",
          "id": "00000000-0000-3039-0000-00000000d431"
        },
        "state": "IN_GAME",
        "index": 2
      }
    ],
    "prawnMap": {
      "0": 0,
      "1": 1
    },
    "removedPrawnMap": {},
    "prawns": [
      {
        "player": 0,
        "id": 0,
        "currentPosition": {
          "roadSection": 0,
          "index": 1,
          "joint": null,
          "hashCode": 962
        },
        "itinerary": null,
        "lastMoveTime": 123456,
        "energy": 1.0,
        "type": "Warrior",
        "warriorDirection": 0.0
      },
      {
        "player": 1,
        "id": 1,
        "currentPosition": {
          "roadSection": 0,
          "index": 2,
          "joint": null,
          "hashCode": 963
        },
        "itinerary": null,
        "lastMoveTime": 123456,
        "energy": 1.0,
        "type": "Warrior",
        "warriorDirection": 0.0
      }
    ],
    "playerPrawns": {
      "0": [
        0
      ],
      "1": [
        1
      ]
    },
    "cityMap": {
      "0": {
        "name": "cityA",
        "id": 0,
        "joint": 0,
        "factories": [
          {
            "state": "ENABLED",
            "index": 0,
            "action": null
          }
        ],
        "player": 0,
        "size": 1
      },
      "1": {
        "name": "cityB",
        "id": 1,
        "joint": 1,
        "factories": [
          {
            "state": "ENABLED",
            "index": 0,
            "action": null
          },
          {
            "state": "DISABLED",
            "index": 1,
            "action": null
          }
        ],
        "player": 1,
        "size": 1
      }
    },
    "removedCityMap": {},
    "playerCities": {
      "0": [
        0
      ],
      "1": [
        1
      ]
    },
    "jointToCityMap": {
      "0": 0,
      "1": 1
    },
    "battles": [
      {
        "prawns": [
          0,
          1
        ],
        "normalizedPositions": [
          {
            "roadSection": 0,
            "index": 1,
            "joint": null,
            "hashCode": 962
          },
          {
            "roadSection": 0,
            "index": 2,
            "joint": null,
            "hashCode": 963
          }
        ],
        "attackDirections": [
          "FORWARD",
          "BACKWARD"
        ],
        "hashCode": 955267,
        "state": "PLANNED"
      }
    ],
    "removedBattles": [],
    "playerVisibleRoadSectionHalves": {},
    "playerDiscoveredRoadSectionHalves": {},
    "playerVisiblePrawns": {},
    "playerDiscoveredCities": {},
    "playerVisibleCities": {}
  }
}
"""
        )).readObject()
    }

}
