package cz.wie.najtmar.gop.common

import com.google.gson.JsonElement

class TestUtils {

    static String toString(Persistable object) {
        return Persistable.GSON.toJson(object.snapshot());
    }

    static String toString(JsonElement object) {
        return Persistable.GSON.toJson(object);
    }
}
