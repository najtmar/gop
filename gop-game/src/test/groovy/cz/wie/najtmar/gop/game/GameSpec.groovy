package cz.wie.najtmar.gop.game

import cz.wie.najtmar.gop.board.Board
import cz.wie.najtmar.gop.board.Joint
import cz.wie.najtmar.gop.board.Position
import cz.wie.najtmar.gop.board.RoadSection
import cz.wie.najtmar.gop.common.Point2D
import spock.lang.Specification
import spock.lang.Unroll

import static cz.wie.najtmar.gop.common.TestUtils.toString

class GameSpec extends Specification {

    static final long START_TIME = 966;

    Board board;
    Joint joint0;
    Joint joint1;
    Joint joint2;
    RoadSection roadSection0;
    RoadSection roadSection1;
    Game game;
    GameProperties gameProperties;
    Player playerA;
    Player playerB;
    Player playerB1;
    Player playerC;
    Warrior warriorA0;
    Warrior warriorB0;
    Warrior warriorB1;
    Battle battle0;
    Battle battle10;
    Battle battle11;

    def setup() {
        Prawn.resetFirstAvailableId()
        City.resetFirstAvailableId()
        final Properties simpleBoardPropertiesSet = new Properties();
        simpleBoardPropertiesSet.load(GameSpec.class.getResourceAsStream(
                "/cz/wie/najtmar/gop/board/simple-board.properties"));
        board = new Board(simpleBoardPropertiesSet);
        joint0 = (new Joint.Builder())
                .setBoard(board)
                .setIndex(0)
                .setName("joint0")
                .setCoordinatesPair(new Point2D.Double(0.0, 0.0))
                .build();
        joint1 = (new Joint.Builder())
                .setBoard(board)
                .setIndex(1)
                .setName("joint1")
                .setCoordinatesPair(new Point2D.Double(3.0, 0.0))
                .build();
        joint2 = (new Joint.Builder())
                .setBoard(board)
                .setIndex(2)
                .setName("joint2")
                .setCoordinatesPair(new Point2D.Double(3.0, 3.0))
                .build();
        board.addJoint(joint0);
        board.addJoint(joint1);
        board.addJoint(joint2);
        roadSection0 = (new RoadSection.Builder())
                .setBoard(board)
                .setIndex(0)
                .setJoints(joint0, joint1)
                .build();
        roadSection1 = (new RoadSection.Builder())
                .setBoard(board)
                .setIndex(1)
                .setJoints(joint1, joint2)
                .build();
        board.addRoadSection(roadSection0);
        board.addRoadSection(roadSection1);
        final Properties simpleGamePropertiesSet = new Properties();
        simpleGamePropertiesSet.load(GameSpec.class.getResourceAsStream(
                "/cz/wie/najtmar/gop/game/simple-game.properties"));
        simpleGamePropertiesSet.setProperty(GameProperties.START_TIME_PROPERTY_NAME, "" + START_TIME);
        game = new Game(simpleGamePropertiesSet);
        gameProperties = game.getGameProperties();
        playerA = new Player(new User("playerA", new UUID(12345, 67890)), 0, game);
        playerB = new Player(new User("playerB", new UUID(98765, 43210)), 1, game);
        game.addPlayer(playerA);
        game.addPlayer(playerB);
        playerB1 = new Player(new User("playerB1", new UUID(98765, 43210)), 1, game);
        warriorA0 = (new Warrior.Builder())
                .setCreationTime(START_TIME)
                .setGameProperties(gameProperties)
                .setPlayer(playerA)
                .setCurrentPosition(new Position(roadSection0, 0))
                .setDirection(0.0)
                .build();
        warriorA0.setCurrentPosition(new Position(roadSection0, 1));
        warriorB0 = (new Warrior.Builder())
                .setCreationTime(START_TIME)
                .setGameProperties(gameProperties)
                .setPlayer(playerB)
                .setCurrentPosition(new Position(roadSection0, 0))
                .setDirection(0.0)
                .build();
        warriorB0.setCurrentPosition(new Position(roadSection0, 2));
        warriorB1 = (new Warrior.Builder())
                .setCreationTime(START_TIME)
                .setGameProperties(gameProperties)
                .setPlayer(playerB)
                .setCurrentPosition(new Position(roadSection0, 0))
                .setDirection(0.0)
                .build();
        warriorB1.setCurrentPosition(new Position(roadSection0, 0));
        battle0 = new Battle(warriorA0, new Position(roadSection0, 1), new Position(roadSection0, 2),
                warriorB0, new Position(roadSection0, 2), new Position(roadSection0, 1));
        battle10 = new Battle(warriorA0, new Position(roadSection0, 1), new Position(roadSection0, 2),
                warriorB1, new Position(roadSection0, 0), new Position(roadSection0, 1));
        battle11 = new Battle(warriorA0, new Position(roadSection0, 1), new Position(roadSection0, 2),
                warriorB1, new Position(roadSection0, 0), new Position(roadSection0, 1));
    }

    def "should correctly create, initialize, and start a game"() {
        expect:
        game.getState() == Game.State.NOT_INITIALIZED
        game.getPlayers() == [playerA, playerB]
        game.getBattles() == [] as Set

        when: "creating game"
        game.setBoard(board);
        game.removePlayer(playerA);
        game.addPlayer(playerB1);

        then: "game created correctly"
        game.getPlayers() == [playerB, playerB1]
        playerB.getIndex() == 0
        playerB1.getIndex() == 1

        when: "initializing game"
        game.initialize();

        then: "game correctly initialized"
        game.getState() == Game.State.INITIALIZED
        game.getTime() == START_TIME

        when: "starting game"
        game.start();

        then: "game correctly started"
        game.getState() == Game.State.RUNNING
    }

    @Unroll
    def "should correctly identify that a game state is #expectedState"() {
        given: "a three player game"
        createThreePlayerGame();

        when: "players' state indicating that the game is won"
        playerA.setState(playerAState);
        playerB.setState(playerBState);
        playerC.setState(playerCState);

        then: "the game is correctly identified as won"
        game.calculateResult() == expectedState

        where:
        playerAState           | playerBState           | playerCState           || expectedState
        Player.State.LOST      | Player.State.WON       | Player.State.LOST      || Game.Result.WON
        Player.State.LOST      | Player.State.WON       | Player.State.ABANDONED || Game.Result.WON
        Player.State.IN_GAME   | Player.State.UNDECIDED | Player.State.IN_GAME   || Game.Result.UNDECIDED
        Player.State.ABANDONED | Player.State.UNDECIDED | Player.State.IN_GAME   || Game.Result.UNDECIDED
        Player.State.IN_GAME   | Player.State.IN_GAME   | Player.State.LOST      || null
    }

    def "game should correctly be finished"() {
        given: "a game"
        game.setBoard(board);
        game.initialize();
        game.start();

        when: "the game is finished"
        playerA.setState(Player.State.LOST);
        playerB.setState(Player.State.WON);
        game.finish();

        then: "the state correctly identified as finished"
        game.getState() == Game.State.FINISHED
        game.getResult() == Game.Result.WON
        game.getPlayersByState() == [
                (Player.State.LOST): [playerA] as Set,
                (Player.State.WON): [playerB] as Set,
        ]
    }

//    @Test
//    public void testFinishedCorrectly1() throws Exception {
//      createThreePlayerGame();
//      game.setBoard(board);
//      game.initialize();
//      game.start();
//      playerA.setState(Player.State.ABANDONED);
//      playerB.setState(Player.State.IN_GAME);
//      playerC.setState(Player.State.IN_GAME);
//      game.finish();
//      assertEquals(Game.State.FINISHED, game.getState());
//      assertEquals(Game.Result.UNDECIDED, game.getResult());
//      assertEquals(new HashMap<Player.State,
//          Set<Player>>() {
//            private static final long serialVersionUID = -4126233879245443929L;
//            {
//              put(Player.State.ABANDONED, new HashSet<>(Arrays.asList(playerA)));
//              put(Player.State.IN_GAME, new HashSet<>(Arrays.asList(playerB, playerC)));
//            }
//          },
//          game.getPlayersByState());
//    }

    def "should fail when adding players out of order"() {
        when: "players added out of order"
        game.addPlayer(playerB);

        then: "GameException is thrown"
        thrown(GameException)
    }

    def "game with too few players should fail"() {
        given: "a game with just one player"
        game.removePlayer(playerB);

        when: "trying to initialize the game"
        game.initialize()

        then: "GameException is thrown"
        def exception = thrown(GameException)
        exception.getMessage() == "The Game must have at least 2 Players (found 1)."
    }

    def "battle should be executed correctly"() {
        expect: "no battle going on"
        game.getBattles() == [] as Set

        when: "adding one battle"
        game.addBattle(battle0);

        then: "exactly one battle is added"
        game.getBattles() == [battle0] as Set
        game.getBattle(battle0, false) == battle0
        game.getBattle(battle10, false) == null
        game.getBattle(battle0, true) == battle0
        game.getBattle(battle10, true) == null

        when: "adding another battle"
        game.addBattle(battle10);

        then: "the second battle is added"
        game.getBattles() == [battle0, battle10] as Set
        game.getBattle(battle10, false) == battle10
        game.getBattle(battle10, true) == battle10

        and: "an identical battle is identified as such"
        game.getBattles().equals([battle0, battle11] as Set)
        game.getBattle(battle10, false).equals(battle11)
        game.getBattle(battle10, true).equals(battle11)

        when: "a battle identical with the second battle is removed"
        game.removeBattle(battle11);

        then: "only the first battle is present"
        game.getBattles() == [battle0] as Set
        game.getBattle(battle10, false) == null

        and: "the second battle is found as removed"
        game.getBattle(battle10, true) == battle10

        when: "adding the second battle again"
        game.addBattle(battle11)

        then: "both battles are present again"
        game.getBattles() == [battle0, battle11] as Set
        game.getBattle(battle11, false) == battle11
        game.getBattle(battle11, true) == battle11

        when: "the first battle is removed"
        game.removeBattle(battle0);

        then: "only the second battle is present"
        game.getBattles() == [battle11] as Set
        game.getBattle(battle0, false) == null

        and: "the first battle is found as removed"
        game.getBattle(battle0, true) == battle0
    }

    def "praws should be added and removed correctly"() {
        expect: "there are no prawns"
        game.getPlayerPrawns(playerA) == [] as Set
        game.getPlayerPrawns(playerB) == [] as Set
        game.getPrawns() == [] as Set
        game.lookupPrawnById(warriorA0.getId(), false) == null
        game.lookupPrawnById(warriorB0.getId(), false) == null
        game.lookupPrawnById(warriorB1.getId(), false) == null
        game.lookupPrawnById(warriorA0.getId(), true) == null
        game.lookupPrawnById(warriorB0.getId(), true) == null
        game.lookupPrawnById(warriorB1.getId(), true) == null

        when: "a prawn is added"
        game.addPrawn(warriorA0)

        then: "the prawn can be found"
        game.lookupPrawnById(warriorA0.getId(), false) == warriorA0
        game.lookupPrawnById(warriorA0.getId(), true) == warriorA0
        game.getPlayerPrawns(playerA) == [warriorA0] as Set
        game.getPrawns() == [warriorA0] as Set

        when: "two other prawns are added"
        game.addPrawn(warriorB0);
        game.addPrawn(warriorB1);

        then: "the prawns can be found"
        game.lookupPrawnById(warriorB0.getId(), false) == warriorB0
        game.lookupPrawnById(warriorB1.getId(), false) == warriorB1
        game.lookupPrawnById(warriorB0.getId(), true) == warriorB0
        game.lookupPrawnById(warriorB1.getId(), true) == warriorB1
        game.getPlayerPrawns(playerB) == [warriorB0, warriorB1] as Set
        game.getPrawns() == [warriorA0, warriorB0, warriorB1] as Set

        when: "a prawn is removed"
        game.removePrawn(warriorB0)

        then: "only the remaining prawns can be found"
        game.lookupPrawnById(warriorB0.getId(), false) == null
        game.lookupPrawnById(warriorB0.getId(), true) == warriorB0
        game.getPlayerPrawns(playerB) == [warriorB1] as Set
        game.getPrawns() == [warriorA0, warriorB1] as Set

        when: "another prawn is removed"
        game.removePrawn(warriorA0);

        then: "only the remaining prawns can be found"
        game.lookupPrawnById(warriorA0.getId(), false) == null
        game.lookupPrawnById(warriorA0.getId(), true) == warriorA0
        game.getPlayerPrawns(playerA) == [] as Set
        game.getPrawns() == [warriorB1] as Set
    }

    def "should correctly add and remove cities"() {
        expect: "players have no cities"
        game.getPlayerCities(playerA) == [] as Set
        game.getPlayerCities(playerB) == [] as Set

        when: "a city of playerA is created"
        City cityA = (new City.Builder())
                .setGame(game)
                .setJoint(joint0)
                .setName("cityA")
                .setPlayer(playerA)
                .build()

        then: "the city can be found as playerA city only"
        game.getPlayerCities(playerA) == [cityA] as Set
        game.getPlayerCities(playerB) == [] as Set

        when: "a city of playerB is created"
        City cityB = (new City.Builder())
                .setGame(game)
                .setJoint(joint1)
                .setName("cityB")
                .setPlayer(playerB)
                .build();

        then: "the corresponding cities can be found for both players"
        game.getPlayerCities(playerA) == [cityA] as Set
        game.getPlayerCities(playerB) == [cityB] as Set
        game.lookupCityByJoint(joint0) == cityA
        game.lookupCityByJoint(joint1) == cityB
        game.lookupCityByJoint(joint2) == null
        game.lookupCityById(cityA.getId(), false) == cityA
        game.lookupCityById(cityB.getId(), false) == cityB
        game.lookupCityById(cityA.getId(), true) == cityA
        game.lookupCityById(cityB.getId(), true) == cityB

        when: "the city of playerB is captured by playerA"
        cityB.grow();
        cityB.captureOrDestroy(playerA);

        then: "the city is recognized as such"
        game.getPlayerCities(playerA) == [cityA, cityB] as Set
        game.getPlayerCities(playerB) == [] as Set
        game.lookupCityByJoint(joint0) == cityA
        game.lookupCityByJoint(joint1) == cityB
        game.lookupCityById(cityA.getId(), false) == cityA
        game.lookupCityById(cityB.getId(), false) == cityB
        game.lookupCityById(cityA.getId(), true) == cityA
        game.lookupCityById(cityB.getId(), true) == cityB

        when: "a city is destroyed"
        cityA.shrinkOrDestroy(cityA.getSize() - 1);

        then: "the city can no longer be found"
        game.getPlayerCities(playerA) == [cityB] as Set
        game.getPlayerCities(playerB) == [] as Set
        game.lookupCityByJoint(joint0) == null
        game.lookupCityByJoint(joint1) == cityB
        game.lookupCityById(cityA.getId(), false) == null
        game.lookupCityById(cityB.getId(), false) == cityB
        game.lookupCityById(cityA.getId(), true) == cityA
        game.lookupCityById(cityB.getId(), true) == cityB

        when: "the other city is destroyed"
        cityB.captureOrDestroy(playerB);

        then: "the city can no longer be found"
        game.getPlayerCities(playerA) == [] as Set
        game.getPlayerCities(playerB) == [] as Set
        game.lookupCityByJoint(joint0) == null
        game.lookupCityByJoint(joint1) == null
        game.lookupCityById(cityA.getId(), false) == null
        game.lookupCityById(cityB.getId(), false) == null
        game.lookupCityById(cityA.getId(), true) == cityA
        game.lookupCityById(cityB.getId(), true) == cityB
    }

    def "should properly serialize and deserialize a game"() {
        given: "a finished game"
        createThreePlayerGame()
        game.setBoard(board);
        game.initialize();
        game.addPrawn(warriorA0)
        game.addPrawn(warriorB0)
        game.addBattle(battle0);
        new City.Builder()
                .setGame(game)
                .setJoint(joint0)
                .setName("cityA")
                .setPlayer(playerA)
                .build()
        new City.Builder()
                .setGame(game)
                .setJoint(joint1)
                .setName("cityB")
                .setPlayer(playerB)
                .build()
        game.start()
        playerA.setState(Player.State.ABANDONED)
        playerB.setState(Player.State.LOST)
        playerC.setState(Player.State.WON)
        game.finish()
        game.calculateResult()

        when: "the game is snapshot and restored"
        def serializedGame = game.snapshot()
        def restoredGame = new Game(serializedGame)

        then: "deserialized game is equivalent to the original game"
        toString(restoredGame) == toString(game)

        and: "the game is initialized"
        restoredGame.state

        and: "first available ids are updated accordingly"
        Prawn.firstAvailableId == 2
        City.firstAvailableId == 2
    }

    private void createThreePlayerGame() throws Exception {
        final Properties simpleGamePropertiesSet = new Properties();
        simpleGamePropertiesSet.load(GameSpec.class.getResourceAsStream(
                "/cz/wie/najtmar/gop/game/simple-game.properties"));
        game = new Game(simpleGamePropertiesSet);
        playerA = new Player(new User("playerA", new UUID(12345, 67890)), 0, game);
        playerB = new Player(new User("playerB", new UUID(98765, 43210)), 1, game);
        playerC = new Player(new User("playerC", new UUID(12345, 54321)), 2, game);
        game.addPlayer(playerA);
        game.addPlayer(playerB);
        game.addPlayer(playerC);
    }

}
