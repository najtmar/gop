package cz.wie.najtmar.gop.game


import cz.wie.najtmar.gop.board.Board
import cz.wie.najtmar.gop.board.Joint
import cz.wie.najtmar.gop.board.RoadSection
import cz.wie.najtmar.gop.common.Point2D
import cz.wie.najtmar.gop.event.Event
import spock.lang.Specification

import javax.json.Json

import static org.mockito.Mockito.spy;

class InteractiveEventExecutorSpec extends Specification {

  static final long START_TIME = 966;
  static final double ENERGY_DELTA = 0.1;
  static Board BOARD;
  static Joint JOINT_0_0;
  static Joint JOINT_3_0;
  static Joint JOINT_6_0;
  static Joint JOINT_3_3;
  static RoadSection ROAD_SECTION_0;
  static RoadSection ROAD_SECTION_1;
  static RoadSection ROAD_SECTION_2;
  static Event TEST_EVENT;

  Game game;
  GameProperties gameProperties;
  EventFactory eventFactory;
  Player playerA;
  Player playerB;
  Player playerC;
  Warrior warriorA;
  Warrior warriorB;
  SettlersUnit settlersUnit;
  City cityA;
  City cityB;
  InteractiveEventExecutor executor;

  /**
   * setUpBeforeClass method.
   * @throws Exception when something unexpected happens
   */
  def setupSpec() throws Exception {
    final Properties simpleBoardPropertiesSet = new Properties();
    simpleBoardPropertiesSet.load(InteractiveEventExecutorSpec.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/board/simple-board.properties"));
    BOARD = new Board(simpleBoardPropertiesSet);
    JOINT_0_0 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setName("JOINT_0_0")
        .setCoordinatesPair(new Point2D.Double(0.0, 0.0))
        .build();
    JOINT_3_0 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(1)
        .setName("JOINT_3_0")
        .setCoordinatesPair(new Point2D.Double(3.0, 0.0))
        .build();
    JOINT_6_0 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(2)
        .setName("JOINT_6_0")
        .setCoordinatesPair(new Point2D.Double(6.0, 0.0))
        .build();
    JOINT_3_3 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(3)
        .setName("JOINT_3_3")
        .setCoordinatesPair(new Point2D.Double(3.0, 3.0))
        .build();
    BOARD.addJoint(JOINT_0_0);
    BOARD.addJoint(JOINT_3_0);
    BOARD.addJoint(JOINT_6_0);
    BOARD.addJoint(JOINT_3_3);
    ROAD_SECTION_0 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(JOINT_0_0, JOINT_3_0)
        .build();
    ROAD_SECTION_1 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(1)
        .setJoints(JOINT_3_0, JOINT_6_0)
        .build();
    ROAD_SECTION_2 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(2)
        .setJoints(JOINT_3_3, JOINT_3_0)
        .build();
    BOARD.addRoadSection(ROAD_SECTION_0);
    BOARD.addRoadSection(ROAD_SECTION_1);
    BOARD.addRoadSection(ROAD_SECTION_2);
    TEST_EVENT = new Event(Json.createObjectBuilder()
        .add("type", "TEST")
        .add("time", START_TIME)
        .add("whatever", "value")
        .build());
  }

  /**
   * Sets up the test case.
   * @throws Exception when test case cannot be set up
   */
  def setup() throws Exception {
    Prawn.resetFirstAvailableId();
    final Properties simpleGamePropertiesSet = new Properties();
    simpleGamePropertiesSet.load(InteractiveEventExecutorSpec.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/game/simple-game.properties"));
    simpleGamePropertiesSet.setProperty(GameProperties.START_TIME_PROPERTY_NAME, "" + START_TIME);
    game = new Game(simpleGamePropertiesSet);
    gameProperties = game.getGameProperties();
    playerA = new Player(new User("playerA", new UUID(12345, 67890)), 0, game);
    playerB = new Player(new User("playerB", new UUID(98765, 43210)), 1, game);
    playerC = new Player(new User("playerC", new UUID(12345, 54321)), 2, game);
    game.addPlayer(playerA);
    game.addPlayer(playerB);
    game.addPlayer(playerC);
    warriorA = spy((new Warrior.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(JOINT_0_0.getNormalizedPosition())
        .setDirection(0.0)
        .setGameProperties(gameProperties)
        .setPlayer(playerA)
        .build());
    warriorB = spy((new Warrior.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(JOINT_3_0.getNormalizedPosition())
        .setDirection(Math.PI / 2.0)
        .setGameProperties(gameProperties)
        .setPlayer(playerB)
        .build());
    settlersUnit = spy((new SettlersUnit.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(JOINT_3_3.getNormalizedPosition())
        .setGameProperties(gameProperties)
        .setPlayer(playerC)
        .build());
    City.resetFirstAvailableId();
    cityA = (new City.Builder())
        .setGame(game)
        .setJoint(JOINT_0_0)
        .setName("cityA")
        .setPlayer(playerA)
        .build();
    cityB = (new City.Builder())
        .setGame(game)
        .setJoint(JOINT_3_0)
        .setName("cityB")
        .setPlayer(playerB)
        .build();
    game.addPrawn(warriorA);
    game.addPrawn(warriorB);
    game.addPrawn(settlersUnit);
    game.setBoard(BOARD);
    game.initialize();
    final ObjectSerializer objectSerializer = new ObjectSerializer(game);
    eventFactory = new EventFactory(objectSerializer);
    executor = (new InteractiveEventExecutor.Factory(game, eventFactory)).create();
  }

  def "order city creation should work"() throws Exception {
    given:
    final Battle battle0 = Mock(Battle);
    final Battle battle1 = Mock(Battle);

    when:
    battle0.getPrawns() >> [warriorA, warriorB]
    battle0.getNormalizedPositions() >> [JOINT_0_0.getNormalizedPosition(), JOINT_3_0.getNormalizedPosition()]
    battle1.getPrawns() >> [warriorA, settlersUnit]
    battle1.getNormalizedPositions() >> [JOINT_0_0.getNormalizedPosition(), JOINT_3_3.getNormalizedPosition()]

    and:
    game.addBattle(battle0);
    game.addBattle(battle1);

    then:
    executor.getState() == EventGeneratingExecutor.State.NOT_PREPARED

    when:
    game.advanceTimeTo(START_TIME + 10);
    executor.prepare(Arrays.asList(
        TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "ORDER_CITY_CREATION")
            .add("time", START_TIME + 10)
            .add("settlersUnit", settlersUnit.getId())
            .add("name", "Sieradz")
            .build())));

    then:
    executor.getState() == EventGeneratingExecutor.State.PREPARED
    game.getBattles() == [battle0, battle1] as Set

    game.lookupCityByJoint(JOINT_3_3) == null
    game.lookupPrawnById(settlersUnit.getId(), false) == settlersUnit
    game.lookupPrawnById(settlersUnit.getId(), true) == settlersUnit

    when:
    executor.executeAndGenerate();

    then:
    game.lookupCityByJoint(JOINT_3_3) != null

    when:
    final City city = game.lookupCityByJoint(JOINT_3_3);

    then:
    city.getPlayer() == playerC
    city.getName() == "Sieradz"
    executor.getEventsGenerated() == [
            new Event(Json.createObjectBuilder()
                    .add("type", "REMOVE_BATTLE")
                    .add("time", START_TIME + 10)
                    .add("battle", Json.createObjectBuilder()
                            .add("prawn0", 0)
                            .add("prawn1", 2)
                            .add("normalizedPosition0", Json.createObjectBuilder()
                                    .add("roadSection", 0)
                                    .add("index", 0)
                                    .build())
                            .add("normalizedPosition1", Json.createObjectBuilder()
                                    .add("roadSection", 2)
                                    .add("index", 0)
                                    .build())
                            .build()
                    )
                    .build()),
            new Event(Json.createObjectBuilder()
                    .add("type", "CREATE_CITY")
                    .add("time", START_TIME + 10)
                    .add("settlersUnit", settlersUnit.getId())
                    .add("id", city.getId())
                    .add("name", "Sieradz")
                    .build())]
    game.lookupPrawnById(settlersUnit.getId(), false) == null
    game.lookupPrawnById(settlersUnit.getId(), true) == settlersUnit
    game.getBattles() == [battle0] as Set
  }

}
