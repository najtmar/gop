package cz.wie.najtmar.gop.game

import cz.wie.najtmar.gop.board.*
import cz.wie.najtmar.gop.common.Constants
import cz.wie.najtmar.gop.common.Point2D
import cz.wie.najtmar.gop.event.Event
import spock.lang.Specification

import javax.json.Json

import static cz.wie.najtmar.gop.common.TestUtils.toString
import static org.junit.Assert.assertEquals
import static spock.util.matcher.HamcrestMatchers.closeTo
import static spock.util.matcher.HamcrestSupport.that

class BattleEventGeneratingExecutorSpec extends Specification {

    static Board BOARD;
    static Joint JOINT_0;
    static Joint JOINT_1;
    static RoadSection ROAD_SECTION;
    static Position POSITION_0;
    static final long START_TIME = 1000;
    static final double ATTACK_SUCCEEDS_PROBABILITY = 0.799;
    static final double ATTACK_FAILS_PROBABILITY = 0.8;
    static final double ATTACK_STRENGTH = 0.3;
    static final double MAX_ENERGY = 1.0;
    static Event TEST_EVENT;

    Game game;
    GameProperties gameProperties;
    EventFactory eventFactory;
    Player playerA;
    Player playerB;
    Warrior warriorA0;
    Warrior warriorA1;
    Warrior warriorB;
    Battle battle;
    Random mockRandom;
    BattleEventGeneratingExecutor executor;

    def setupSpec() {
        final Properties simpleBoardPropertiesSet = new Properties();
        simpleBoardPropertiesSet.load(BattleEventGeneratingExecutorSpec.class.getResourceAsStream(
                "/cz/wie/najtmar/gop/board/simple-board.properties"));
        BOARD = new Board(simpleBoardPropertiesSet);
        JOINT_0 = (new Joint.Builder())
                .setBoard(BOARD)
                .setIndex(0)
                .setName("JOINT_0")
                .setCoordinatesPair(new Point2D.Double(1.0, 0.0))
                .build();
        JOINT_1 = (new Joint.Builder())
                .setBoard(BOARD)
                .setIndex(1)
                .setName("JOINT_1")
                .setCoordinatesPair(new Point2D.Double(4.0, 0.0))
                .build();
        BOARD.addJoint(JOINT_0)
        BOARD.addJoint(JOINT_1)
        ROAD_SECTION = (new RoadSection.Builder())
                .setBoard(BOARD)
                .setIndex(0)
                .setJoints(JOINT_0, JOINT_1)
                .build();
        BOARD.addRoadSection(ROAD_SECTION)
        POSITION_0 = new Position(ROAD_SECTION, 0);
        TEST_EVENT = new Event(Json.createObjectBuilder()
                .add("type", "TEST")
                .add("time", START_TIME)
                .add("whatever", "value")
                .build());
    }

    def setup() {
        final Properties simpleGamePropertiesSet = new Properties();
        simpleGamePropertiesSet.load(BattleEventGeneratingExecutorSpec.class.getResourceAsStream(
                "/cz/wie/najtmar/gop/game/simple-game.properties"));
        simpleGamePropertiesSet.setProperty(GameProperties.START_TIME_PROPERTY_NAME, "" + START_TIME);
        game = new Game(simpleGamePropertiesSet);
        game.setBoard(BOARD);
        gameProperties = game.getGameProperties();
        playerA = new Player(new User("playerA", new UUID(12345, 67890)), 0, game);
        playerB = new Player(new User("playerB", new UUID(98765, 43210)), 1, game);
        game.addPlayer(playerA);
        game.addPlayer(playerB);

        Prawn.resetFirstAvailableId();
        warriorA0 = (new Warrior.Builder())
                .setCreationTime(START_TIME)
                .setCurrentPosition(POSITION_0)
                .setDirection(0.0)
                .setGameProperties(gameProperties)
                .setPlayer(playerA)
                .build();
        warriorA1 = (new Warrior.Builder())
                .setCreationTime(START_TIME)
                .setCurrentPosition(POSITION_0)
                .setDirection(0.0)
                .setGameProperties(gameProperties)
                .setPlayer(playerA)
                .build();
        warriorB = (new Warrior.Builder())
                .setCreationTime(START_TIME)
                .setCurrentPosition(POSITION_0)
                .setDirection(Math.PI)
                .setGameProperties(gameProperties)
                .setPlayer(playerB)
                .build();
        warriorA0.setCurrentPosition(new Position(ROAD_SECTION, 1));
        warriorA0.setItinerary((new Itinerary.Builder())
                .addPosition(new Position(ROAD_SECTION, 1))
                .addPosition(new Position(ROAD_SECTION, 2))
                .build());
        warriorB.setCurrentPosition(new Position(ROAD_SECTION, 2));
        warriorB.setItinerary((new Itinerary.Builder())
                .addPosition(new Position(ROAD_SECTION, 2))
                .addPosition(new Position(ROAD_SECTION, 1))
                .build());
        game.addPrawn(warriorA0);
        game.addPrawn(warriorA1);
        game.addPrawn(warriorB);

        battle = new Battle(warriorA0, new Position(ROAD_SECTION, 1), new Position(ROAD_SECTION, 2),
                warriorB, new Position(ROAD_SECTION, 2), new Position(ROAD_SECTION, 1));
        battle.start();
        game.addBattle(battle);
        mockRandom = Mock(Random);
        assertEquals(ATTACK_STRENGTH, warriorA0.calculateAttackStrength(
                new DirectedPosition(ROAD_SECTION, 1, RoadSection.Direction.FORWARD)), Constants.DOUBLE_DELTA);
        assertEquals(ATTACK_STRENGTH, warriorB.calculateAttackStrength(
                new DirectedPosition(ROAD_SECTION, 2, RoadSection.Direction.BACKWARD)), Constants.DOUBLE_DELTA);

        game.initialize();
        final ObjectSerializer objectSerializer = new ObjectSerializer(game);
        eventFactory = new EventFactory(objectSerializer);
        executor = (new BattleEventGeneratingExecutor.Factory(game, eventFactory, mockRandom))
                .create();
    }

    def "first warrior attacks first and both attacks succeed"() {
        when:
        1 * mockRandom.nextBoolean() >> true
        2 * mockRandom.nextDouble() >> ATTACK_SUCCEEDS_PROBABILITY

        then:
        executor.getState() == EventGeneratingExecutor.State.NOT_PREPARED

        when:
        game.advanceTimeTo(START_TIME + 10);
        executor.prepare([TEST_EVENT]);

        then:
        executor.getState() == EventGeneratingExecutor.State.PREPARED

        when:
        executor.executeAndGenerate();
        def gameSnapshot = game.snapshot()
        def restoredGame = new Game(gameSnapshot)

        then:
        executor.getState() == EventGeneratingExecutor.State.GENERATED
        that warriorA0.getEnergy(), closeTo(MAX_ENERGY - ATTACK_STRENGTH, Constants.DOUBLE_DELTA)
        that warriorA1.getEnergy(), closeTo(MAX_ENERGY, Constants.DOUBLE_DELTA)
        that warriorB.getEnergy(), closeTo(MAX_ENERGY - ATTACK_STRENGTH, Constants.DOUBLE_DELTA)
        game.getPlayerPrawns(playerA) == [warriorA0, warriorA1] as Set
        game.getPlayerPrawns(playerB) == [warriorB] as Set
        game.getBattles() == [battle] as Set
        executor.getEventsGenerated() == [TEST_EVENT,
                new Event(Json.createObjectBuilder()
                        .add("type", "DECREASE_PRAWN_ENERGY")
                        .add("time", START_TIME + 10)
                        .add("prawn", 2)
                        .add("delta", ATTACK_STRENGTH)
                        .build()),
                new Event(Json.createObjectBuilder()
                        .add("type", "DECREASE_PRAWN_ENERGY")
                        .add("time", START_TIME + 10)
                        .add("prawn", 0)
                        .add("delta", ATTACK_STRENGTH)
                        .build())
        ]

        and:
        gameSnapshot.getAsJsonPrimitive("time").asLong == START_TIME + 10
        gameSnapshot.getAsJsonArray("prawns")
                .collect { it.asJsonObject.getAsJsonPrimitive("energy").asDouble } as Set == [
                MAX_ENERGY - ATTACK_STRENGTH, MAX_ENERGY - ATTACK_STRENGTH, 1.0d] as Set
        gameSnapshot.getAsJsonArray("battles").size() == 1

        and:
        toString(restoredGame) == toString(game)
    }

    def "first warrior attacks and first attack fails"() {
        when:
        1 * mockRandom.nextBoolean() >> true
        mockRandom.nextDouble() >> ATTACK_FAILS_PROBABILITY >> ATTACK_SUCCEEDS_PROBABILITY
        game.advanceTimeTo(START_TIME + 10);
        executor.prepare(new LinkedList<>(Arrays.asList(TEST_EVENT)));
        executor.executeAndGenerate();
        def gameSnapshot = game.snapshot()

        then:
        that warriorA0.getEnergy(), closeTo(MAX_ENERGY - ATTACK_STRENGTH, Constants.DOUBLE_DELTA)
        that warriorA1.getEnergy(), closeTo(MAX_ENERGY, Constants.DOUBLE_DELTA)
        that warriorB.getEnergy(), closeTo(MAX_ENERGY, Constants.DOUBLE_DELTA)
        game.getPlayerPrawns(playerA) == [warriorA0, warriorA1] as Set
        game.getPlayerPrawns(playerB) == [warriorB] as Set
        game.getBattles() == [battle] as Set
        executor.getEventsGenerated() == [TEST_EVENT,
                new Event(Json.createObjectBuilder()
                        .add("type", "DECREASE_PRAWN_ENERGY")
                        .add("time", START_TIME + 10)
                        .add("prawn", 0)
                        .add("delta", ATTACK_STRENGTH)
                        .build())
        ]
        gameSnapshot.getAsJsonArray("prawns")
                .collect { it.asJsonObject.getAsJsonPrimitive("energy").asDouble } as Set == [
                MAX_ENERGY - ATTACK_STRENGTH, 1.0d, 1.0d] as Set
    }

    def "second warrior attacks first and both attacks succeed"() {
        when:
        1 * mockRandom.nextBoolean() >> false
        2 * mockRandom.nextDouble() >> ATTACK_SUCCEEDS_PROBABILITY
        game.advanceTimeTo(START_TIME + 10);
        executor.prepare(new LinkedList<>(Arrays.asList(TEST_EVENT)));
        executor.executeAndGenerate();
        def gameSnapshot = game.snapshot()

        then:
        that warriorA0.getEnergy(), closeTo(MAX_ENERGY - ATTACK_STRENGTH, Constants.DOUBLE_DELTA)
        that warriorA1.getEnergy(), closeTo(MAX_ENERGY, Constants.DOUBLE_DELTA)
        that warriorB.getEnergy(), closeTo(MAX_ENERGY - ATTACK_STRENGTH, Constants.DOUBLE_DELTA)
        game.getPlayerPrawns(playerA) == [warriorA0, warriorA1] as Set
        game.getPlayerPrawns(playerB) == [warriorB] as Set
        game.getBattles() == [battle] as Set
        executor.getEventsGenerated() == [
                TEST_EVENT,
                new Event(Json.createObjectBuilder()
                        .add("type", "DECREASE_PRAWN_ENERGY")
                        .add("time", START_TIME + 10)
                        .add("prawn", 0)
                        .add("delta", ATTACK_STRENGTH)
                        .build()),
                new Event(Json.createObjectBuilder()
                        .add("type", "DECREASE_PRAWN_ENERGY")
                        .add("time", START_TIME + 10)
                        .add("prawn", 2)
                        .add("delta", ATTACK_STRENGTH)
                        .build())]
        gameSnapshot.getAsJsonArray("prawns")
                .collect { it.asJsonObject.getAsJsonPrimitive("energy").asDouble } as Set == [
                MAX_ENERGY - ATTACK_STRENGTH, MAX_ENERGY - ATTACK_STRENGTH, 1.0d] as Set
    }

    def "second warrior attacks first and first attack fails"() {
        when:
        mockRandom.nextBoolean() >> false
        mockRandom.nextDouble() >> ATTACK_FAILS_PROBABILITY >> ATTACK_SUCCEEDS_PROBABILITY
        game.advanceTimeTo(START_TIME + 10);
        executor.prepare(new LinkedList<>(Arrays.asList(TEST_EVENT)));
        executor.executeAndGenerate();
        def gameSnapshot = game.snapshot()

        then:
        that warriorA0.getEnergy(), closeTo(MAX_ENERGY, Constants.DOUBLE_DELTA)
        that warriorA1.getEnergy(), closeTo(MAX_ENERGY, Constants.DOUBLE_DELTA)
        that warriorB.getEnergy(), closeTo(MAX_ENERGY - ATTACK_STRENGTH, Constants.DOUBLE_DELTA)
        game.getPlayerPrawns(playerA) == [warriorA0, warriorA1] as Set
        game.getPlayerPrawns(playerB) == [warriorB] as Set
        game.getBattles() == [battle] as Set
        executor.getEventsGenerated() == [
                TEST_EVENT,
                new Event(Json.createObjectBuilder()
                        .add("type", "DECREASE_PRAWN_ENERGY")
                        .add("time", START_TIME + 10)
                        .add("prawn", 2)
                        .add("delta", ATTACK_STRENGTH)
                        .build())]
        gameSnapshot.getAsJsonArray("prawns")
                .collect { it.asJsonObject.getAsJsonPrimitive("energy").asDouble } as Set == [
                MAX_ENERGY - ATTACK_STRENGTH, 1.0d, 1.0d] as Set
    }

    def "first warrior attacks first and destroys second warrior"()  {
        given:
        warriorB.decreaseEnergy(MAX_ENERGY - ATTACK_STRENGTH + 0.001)

        when:
        1 * mockRandom.nextBoolean() >> true
        1 * mockRandom.nextDouble() >> ATTACK_SUCCEEDS_PROBABILITY
        game.advanceTimeTo(START_TIME + 10);
        executor.prepare([TEST_EVENT]);
        executor.executeAndGenerate()
        def gameSnapshot = game.snapshot()

        then:
        that warriorA0.getEnergy(), closeTo(MAX_ENERGY, Constants.DOUBLE_DELTA)
        that warriorA1.getEnergy(), closeTo(MAX_ENERGY, Constants.DOUBLE_DELTA)
        that warriorB.getEnergy(), closeTo(0.0, Constants.DOUBLE_DELTA)
        game.getPlayerPrawns(playerA) == [warriorA0, warriorA1] as Set
        game.getPlayerPrawns(playerB) == [] as Set
        game.getBattles() == [] as Set
        executor.getEventsGenerated() == [TEST_EVENT,
                new Event(Json.createObjectBuilder()
                        .add("type", "DECREASE_PRAWN_ENERGY")
                        .add("time", START_TIME + 10)
                        .add("prawn", 2)
                        .add("delta", ATTACK_STRENGTH)
                        .build()),
                new Event(Json.createObjectBuilder()
                        .add("type", "DESTROY_PRAWN")
                        .add("time", START_TIME + 10)
                        .add("prawn", 2)
                        .build()),
                new Event(Json.createObjectBuilder()
                        .add("type", "REMOVE_BATTLE")
                        .add("time", START_TIME + 10)
                        .add("battle", Json.createObjectBuilder()
                                .add("prawn0", 0)
                                .add("prawn1", 2)
                                .add("normalizedPosition0", Json.createObjectBuilder()
                                        .add("roadSection", 0)
                                        .add("index", 1)
                                        .build())
                                .add("normalizedPosition1", Json.createObjectBuilder()
                                        .add("roadSection", 0)
                                        .add("index", 2)
                                        .build())
                                .build())
                        .build())]
        gameSnapshot.getAsJsonArray("prawns")
                .collect { it.asJsonObject.getAsJsonPrimitive("energy").asDouble } as Set == [
                1.0d, 1.0d] as Set
        gameSnapshot.getAsJsonArray("battles").size() == 0
    }

    def "first warrior attacks second and destroys second warrior"() {
        given:
        warriorB.decreaseEnergy(MAX_ENERGY - ATTACK_STRENGTH + 0.001);

        when:
        mockRandom.nextBoolean() >> false
        2 * mockRandom.nextDouble() >> ATTACK_SUCCEEDS_PROBABILITY
        game.advanceTimeTo(START_TIME + 10);
        executor.prepare([TEST_EVENT]);
        executor.executeAndGenerate()
        def gameSnapshot = game.snapshot()

        then:
        that warriorA0.getEnergy(), closeTo(MAX_ENERGY - ATTACK_STRENGTH, Constants.DOUBLE_DELTA)
        that warriorA1.getEnergy(), closeTo(MAX_ENERGY, Constants.DOUBLE_DELTA)
        that warriorB.getEnergy(), closeTo(0.0, Constants.DOUBLE_DELTA)
        game.getPlayerPrawns(playerA) == [warriorA0, warriorA1] as Set
        game.getPlayerPrawns(playerB) == [] as Set
        game.getBattles() == [] as Set
        executor.getEventsGenerated() == [
                TEST_EVENT,
                new Event(Json.createObjectBuilder()
                        .add("type", "DECREASE_PRAWN_ENERGY")
                        .add("time", START_TIME + 10)
                        .add("prawn", 0)
                        .add("delta", ATTACK_STRENGTH)
                        .build()),
                new Event(Json.createObjectBuilder()
                        .add("type", "DECREASE_PRAWN_ENERGY")
                        .add("time", START_TIME + 10)
                        .add("prawn", 2)
                        .add("delta", ATTACK_STRENGTH)
                        .build()),
                new Event(Json.createObjectBuilder()
                        .add("type", "DESTROY_PRAWN")
                        .add("time", START_TIME + 10)
                        .add("prawn", 2)
                        .build()),
                new Event(Json.createObjectBuilder()
                        .add("type", "REMOVE_BATTLE")
                        .add("time", START_TIME + 10)
                        .add("battle", Json.createObjectBuilder()
                                .add("prawn0", 0)
                                .add("prawn1", 2)
                                .add("normalizedPosition0", Json.createObjectBuilder()
                                        .add("roadSection", 0)
                                        .add("index", 1)
                                        .build())
                                .add("normalizedPosition1", Json.createObjectBuilder()
                                        .add("roadSection", 0)
                                        .add("index", 2)
                                        .build())
                                .build())
                        .build())]
        gameSnapshot.getAsJsonArray("prawns")
                .collect { it.asJsonObject.getAsJsonPrimitive("energy").asDouble } as Set == [
                MAX_ENERGY - ATTACK_STRENGTH, 1.0d] as Set
        gameSnapshot.getAsJsonArray("battles").size() == 0
    }

    def "defender destroyed and second battle ignored"() {
        given:
        final Warrior warriorA2 = (new Warrior.Builder())
                .setCreationTime(START_TIME)
                .setCurrentPosition(POSITION_0)
                .setDirection(0.0)
                .setGameProperties(gameProperties)
                .setPlayer(playerA)
                .build();
        warriorA2.setCurrentPosition(new Position(ROAD_SECTION, 1));
        warriorA2.setItinerary((new Itinerary.Builder())
                .addPosition(new Position(ROAD_SECTION, 1))
                .addPosition(new Position(ROAD_SECTION, 2))
                .build());
        game.addPrawn(warriorA2);

        when:
        final Battle secondBattle = new Battle(warriorA2, new Position(ROAD_SECTION, 1), new Position(ROAD_SECTION, 2),
                warriorB, new Position(ROAD_SECTION, 2), new Position(ROAD_SECTION, 1));
        secondBattle.start();
        game.addBattle(secondBattle)

        then:
        game.snapshot().getAsJsonArray("battles").size() == 2
        that warriorA2.calculateAttackStrength(
                new DirectedPosition(ROAD_SECTION, 1, RoadSection.Direction.FORWARD)), closeTo(Constants.DOUBLE_DELTA, ATTACK_STRENGTH)

        when:
        warriorB.decreaseEnergy(MAX_ENERGY - ATTACK_STRENGTH + 0.001);
        2 * mockRandom.nextInt(2) >> 0
        1 * mockRandom.nextBoolean() >> false
        2 * mockRandom.nextDouble() >> ATTACK_SUCCEEDS_PROBABILITY
        game.advanceTimeTo(START_TIME + 10);
        executor.prepare([TEST_EVENT])
        executor.executeAndGenerate()
        def gameSnapshot = game.snapshot()

        then:
        that warriorA0.getEnergy(), closeTo(MAX_ENERGY - ATTACK_STRENGTH, Constants.DOUBLE_DELTA)
        that warriorA1.getEnergy(), closeTo(MAX_ENERGY, Constants.DOUBLE_DELTA)
        that warriorA2.getEnergy(), closeTo(MAX_ENERGY, Constants.DOUBLE_DELTA)
        that warriorB.getEnergy(), closeTo(0.0, Constants.DOUBLE_DELTA)
        game.getPlayerPrawns(playerA) == [warriorA0, warriorA1, warriorA2] as Set
        game.getPlayerPrawns(playerB) == [] as Set
        game.getBattles() == [] as Set
        executor.getEventsGenerated() == [
                TEST_EVENT,
                new Event(Json.createObjectBuilder()
                        .add("type", "DECREASE_PRAWN_ENERGY")
                        .add("time", START_TIME + 10)
                        .add("prawn", 0)
                        .add("delta", ATTACK_STRENGTH)
                        .build()),
                new Event(Json.createObjectBuilder()
                        .add("type", "DECREASE_PRAWN_ENERGY")
                        .add("time", START_TIME + 10)
                        .add("prawn", 2)
                        .add("delta", ATTACK_STRENGTH)
                        .build()),
                new Event(Json.createObjectBuilder()
                        .add("type", "DESTROY_PRAWN")
                        .add("time", START_TIME + 10)
                        .add("prawn", 2)
                        .build()),
                new Event(Json.createObjectBuilder()
                        .add("type", "REMOVE_BATTLE")
                        .add("time", START_TIME + 10)
                        .add("battle", Json.createObjectBuilder()
                                .add("prawn0", 0)
                                .add("prawn1", 2)
                                .add("normalizedPosition0", Json.createObjectBuilder()
                                        .add("roadSection", 0)
                                        .add("index", 1)
                                        .build())
                                .add("normalizedPosition1", Json.createObjectBuilder()
                                        .add("roadSection", 0)
                                        .add("index", 2)
                                        .build())
                                .build())
                        .build()),
                new Event(Json.createObjectBuilder()
                        .add("type", "REMOVE_BATTLE")
                        .add("time", START_TIME + 10)
                        .add("battle", Json.createObjectBuilder()
                                .add("prawn0", 2)
                                .add("prawn1", 3)
                                .add("normalizedPosition0", Json.createObjectBuilder()
                                        .add("roadSection", 0)
                                        .add("index", 2)
                                        .build())
                                .add("normalizedPosition1", Json.createObjectBuilder()
                                        .add("roadSection", 0)
                                        .add("index", 1)
                                        .build())
                                .build())
                        .build())]
        gameSnapshot.getAsJsonArray("prawns")
                .collect { it.asJsonObject.getAsJsonPrimitive("energy").asDouble } as Set == [
                MAX_ENERGY - ATTACK_STRENGTH, 1.0d] as Set
        gameSnapshot.getAsJsonArray("battles").size() == 0
    }

    def "defender destroyed in second battle and first battle also removed"() {
        given:
        final Warrior warriorA2 = (new Warrior.Builder())
                .setCreationTime(START_TIME)
                .setCurrentPosition(POSITION_0)
                .setDirection(0.0)
                .setGameProperties(gameProperties)
                .setPlayer(playerA)
                .build();
        warriorA2.setCurrentPosition(new Position(ROAD_SECTION, 1));
        warriorA2.setItinerary((new Itinerary.Builder())
                .addPosition(new Position(ROAD_SECTION, 1))
                .addPosition(new Position(ROAD_SECTION, 2))
                .build());
        game.addPrawn(warriorA2);

        when:
        final Battle secondBattle = new Battle(warriorA2, new Position(ROAD_SECTION, 1), new Position(ROAD_SECTION, 2),
                warriorB, new Position(ROAD_SECTION, 2), new Position(ROAD_SECTION, 1));
        secondBattle.start();
        game.addBattle(secondBattle);

        then:
        game.snapshot().getAsJsonArray("battles").size() == 2
        that warriorA2.calculateAttackStrength(
                new DirectedPosition(ROAD_SECTION, 1, RoadSection.Direction.FORWARD)), closeTo(ATTACK_STRENGTH, Constants.DOUBLE_DELTA)

        when:
        warriorB.decreaseEnergy(MAX_ENERGY - ATTACK_STRENGTH + 0.001);
        mockRandom.nextInt(2) >> 0 >> 1
        mockRandom.nextBoolean() >> false
        mockRandom.nextDouble() >> ATTACK_FAILS_PROBABILITY >> ATTACK_SUCCEEDS_PROBABILITY

        game.advanceTimeTo(START_TIME + 10);
        executor.prepare(new LinkedList<>(Arrays.asList(TEST_EVENT)));
        executor.executeAndGenerate();
        def gameSnapshot = game.snapshot()

        then:
        that warriorA0.getEnergy(), closeTo(MAX_ENERGY, Constants.DOUBLE_DELTA)
        that warriorA1.getEnergy(), closeTo(MAX_ENERGY, Constants.DOUBLE_DELTA)
        that warriorA2.getEnergy(), closeTo(MAX_ENERGY, Constants.DOUBLE_DELTA)
        that warriorB.getEnergy(), closeTo(0.0, Constants.DOUBLE_DELTA)
        game.getPlayerPrawns(playerA) == [warriorA0, warriorA1, warriorA2] as Set
        game.getPlayerPrawns(playerB) == [] as Set
        game.getBattles() == [] as Set
        executor.getEventsGenerated() == [
                TEST_EVENT,
                new Event(Json.createObjectBuilder()
                        .add("type", "DECREASE_PRAWN_ENERGY")
                        .add("time", START_TIME + 10)
                        .add("prawn", 2)
                        .add("delta", ATTACK_STRENGTH)
                        .build()),
                new Event(Json.createObjectBuilder()
                        .add("type", "DESTROY_PRAWN")
                        .add("time", START_TIME + 10)
                        .add("prawn", 2)
                        .build()),
                new Event(Json.createObjectBuilder()
                        .add("type", "REMOVE_BATTLE")
                        .add("time", START_TIME + 10)
                        .add("battle", Json.createObjectBuilder()
                                .add("prawn0", 0)
                                .add("prawn1", 2)
                                .add("normalizedPosition0", Json.createObjectBuilder()
                                        .add("roadSection", 0)
                                        .add("index", 1)
                                        .build())
                                .add("normalizedPosition1", Json.createObjectBuilder()
                                        .add("roadSection", 0)
                                        .add("index", 2)
                                        .build())
                                .build())
                        .build()),
                new Event(Json.createObjectBuilder()
                        .add("type", "REMOVE_BATTLE")
                        .add("time", START_TIME + 10)
                        .add("battle", Json.createObjectBuilder()
                                .add("prawn0", 2)
                                .add("prawn1", 3)
                                .add("normalizedPosition0", Json.createObjectBuilder()
                                        .add("roadSection", 0)
                                        .add("index", 2)
                                        .build())
                                .add("normalizedPosition1", Json.createObjectBuilder()
                                        .add("roadSection", 0)
                                        .add("index", 1)
                                        .build())
                                .build())
                        .build())]
        gameSnapshot.getAsJsonArray("prawns")
                .collect { it.asJsonObject.getAsJsonPrimitive("energy").asDouble } as Set == [
                1.0d, 1.0d] as Set
        gameSnapshot.getAsJsonArray("battles").size() == 0
    }

    def "second warrior attacks first and destroys first warrior"() {
        when:
        warriorA0.decreaseEnergy(MAX_ENERGY - ATTACK_STRENGTH + 0.001);
        mockRandom.nextBoolean() >> false
        mockRandom.nextDouble() >> ATTACK_SUCCEEDS_PROBABILITY
        game.advanceTimeTo(START_TIME + 10);
        executor.prepare([TEST_EVENT]);
        executor.executeAndGenerate()
        def gameSnapshot = game.snapshot()

        then:
        that warriorA0.getEnergy(), closeTo(0.0, Constants.DOUBLE_DELTA)
        that warriorA1.getEnergy(), closeTo(MAX_ENERGY, Constants.DOUBLE_DELTA)
        that warriorB.getEnergy(), closeTo(MAX_ENERGY, Constants.DOUBLE_DELTA)
        game.getPlayerPrawns(playerA) == [warriorA1] as Set
        game.getPlayerPrawns(playerB) == [warriorB] as Set
        game.getBattles() == [] as Set
        executor.getEventsGenerated() == [
                TEST_EVENT,
                new Event(Json.createObjectBuilder()
                        .add("type", "DECREASE_PRAWN_ENERGY")
                        .add("time", START_TIME + 10)
                        .add("prawn", 0)
                        .add("delta", ATTACK_STRENGTH)
                        .build()),
                new Event(Json.createObjectBuilder()
                        .add("type", "DESTROY_PRAWN")
                        .add("time", START_TIME + 10)
                        .add("prawn", 0)
                        .build()),
                new Event(Json.createObjectBuilder()
                        .add("type", "REMOVE_BATTLE")
                        .add("time", START_TIME + 10)
                        .add("battle", Json.createObjectBuilder()
                                .add("prawn0", 0)
                                .add("prawn1", 2)
                                .add("normalizedPosition0", Json.createObjectBuilder()
                                        .add("roadSection", 0)
                                        .add("index", 1)
                                        .build())
                                .add("normalizedPosition1", Json.createObjectBuilder()
                                        .add("roadSection", 0)
                                        .add("index", 2)
                                        .build())
                                .build())
                        .build())]
        gameSnapshot.getAsJsonArray("prawns")
                .collect { it.asJsonObject.getAsJsonPrimitive("energy").asDouble } as Set == [
                1.0d, 1.0d] as Set
        gameSnapshot.getAsJsonArray("battles").size() == 0
    }

    def "second warrior attacks second and destroys first warrior"() {
        when:
        warriorA0.decreaseEnergy(MAX_ENERGY - ATTACK_STRENGTH + 0.001);
        1 * mockRandom.nextBoolean() >> true
        2 * mockRandom.nextDouble() >> ATTACK_SUCCEEDS_PROBABILITY
        game.advanceTimeTo(START_TIME + 10);
        executor.prepare([TEST_EVENT])
        executor.executeAndGenerate()
        def gameSnapshot = game.snapshot()

        then:
        that warriorA0.getEnergy(), closeTo(0.0, Constants.DOUBLE_DELTA)
        that warriorA1.getEnergy(), closeTo(MAX_ENERGY, Constants.DOUBLE_DELTA)
        that warriorB.getEnergy(), closeTo(MAX_ENERGY - ATTACK_STRENGTH, Constants.DOUBLE_DELTA)
        game.getPlayerPrawns(playerA) == [warriorA1] as Set
        game.getPlayerPrawns(playerB) == [warriorB] as Set
        game.getBattles() == [] as Set
        executor.getEventsGenerated() == [
                TEST_EVENT,
                new Event(Json.createObjectBuilder()
                        .add("type", "DECREASE_PRAWN_ENERGY")
                        .add("time", START_TIME + 10)
                        .add("prawn", 2)
                        .add("delta", ATTACK_STRENGTH)
                        .build()),
                new Event(Json.createObjectBuilder()
                        .add("type", "DECREASE_PRAWN_ENERGY")
                        .add("time", START_TIME + 10)
                        .add("prawn", 0)
                        .add("delta", ATTACK_STRENGTH)
                        .build()),
                new Event(Json.createObjectBuilder()
                        .add("type", "DESTROY_PRAWN")
                        .add("time", START_TIME + 10)
                        .add("prawn", 0)
                        .build()),
                new Event(Json.createObjectBuilder()
                        .add("type", "REMOVE_BATTLE")
                        .add("time", START_TIME + 10)
                        .add("battle", Json.createObjectBuilder()
                                .add("prawn0", 0)
                                .add("prawn1", 2)
                                .add("normalizedPosition0", Json.createObjectBuilder()
                                        .add("roadSection", 0)
                                        .add("index", 1)
                                        .build())
                                .add("normalizedPosition1", Json.createObjectBuilder()
                                        .add("roadSection", 0)
                                        .add("index", 2)
                                        .build())
                                .build())
                        .build())]
        gameSnapshot.getAsJsonArray("prawns")
                .collect { it.asJsonObject.getAsJsonPrimitive("energy").asDouble } as Set == [
                MAX_ENERGY - ATTACK_STRENGTH, 1.0d] as Set
        gameSnapshot.getAsJsonArray("battles").size() == 0
    }

    def "removed battles should be serialized correctly"() {
        when:
        game.removeBattle(battle)
        def gameSnapshot = game.snapshot()
        def restoredGame = new Game(gameSnapshot)

        then:
        toString(restoredGame) == toString(game)
    }
}
