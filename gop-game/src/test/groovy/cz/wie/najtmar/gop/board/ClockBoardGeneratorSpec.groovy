package cz.wie.najtmar.gop.board

import com.google.inject.AbstractModule
import com.google.inject.Guice
import com.google.inject.Injector
import com.google.inject.Module
import cz.wie.najtmar.gop.game.PropertiesReader
import cz.wie.najtmar.gop.game.SimplePropertiesReader
import spock.lang.Specification

import javax.json.Json

class ClockBoardGeneratorSpec extends Specification {

    ClockBoardGenerator generator;

    /**
     * Method setUp().
     * @throws Exception when something goes wrong
     */
    void setup() throws Exception {
        final Injector injector = Guice.createInjector(getTestModule());
        generator = injector.getInstance(ClockBoardGenerator.class);
    }

    def "should generate test board"() {
        when: "board generated"
        def board = generator.generate();

        then: "board should be correctly created"
        board.serialize() == Json.createReader(new StringReader(
                        "{\"properties\":[ "
                                + "  {\"key\":\"Board.xsize\",\"value\":\"1000\"}, "
                                + "  {\"key\":\"Board.stepSize\",\"value\":\"1.0\"}, "
                                + "  {\"key\":\"Board.backgroundImage\",\"value\":\"/images/gop-background.jpg\"},"
                                + "  {\"key\":\"Board.ysize\",\"value\":\"100\"} "
                                + "], "
                                + "\"joints\":[ "
                                + "  {\"index\":0,\"name\":\"joint0\",\"x\":500.00000000000006,\"y\":99.0}, "
                                + "  {\"index\":1,\"name\":\"joint1\",\"x\":749.5,\"y\":92.43524478543749}, "
                                + "  {\"index\":2,\"name\":\"joint2\",\"x\":932.1466764884349,\"y\":74.5}, "
                                + "  {\"index\":3,\"name\":\"joint3\",\"x\":999.0,\"y\":50.0}, "
                                + "  {\"index\":4,\"name\":\"joint4\",\"x\":932.1466764884349,\"y\":25.500000000000004}, "
                                + "  {\"index\":5,\"name\":\"joint5\",\"x\":749.5,\"y\":7.56475521456251}, "
                                + "  {\"index\":6,\"name\":\"joint6\",\"x\":500.00000000000006,\"y\":1.0}, "
                                + "  {\"index\":7,\"name\":\"joint7\",\"x\":250.5000000000001,\"y\":7.564755214562503}, "
                                + "  {\"index\":8,\"name\":\"joint8\",\"x\":67.85332351156518,\"y\":25.499999999999982}, "
                                + "  {\"index\":9,\"name\":\"joint9\",\"x\":1.0,\"y\":49.99999999999999}, "
                                + "  {\"index\":10,\"name\":\"joint10\",\"x\":67.85332351156501,\"y\":74.49999999999999}, "
                                + "  {\"index\":11,\"name\":\"joint11\",\"x\":250.49999999999977,\"y\":92.43524478543748}, "
                                + "  {\"index\":12,\"name\":\"joint12\",\"x\":500.0,\"y\":74.0}, "
                                + "  {\"index\":13,\"name\":\"joint13\",\"x\":624.5,\"y\":70.78460969082653}, "
                                + "  {\"index\":14,\"name\":\"joint14\",\"x\":715.6403255423252,\"y\":62.0}, "
                                + "  {\"index\":15,\"name\":\"joint15\",\"x\":749.0,\"y\":50.0}, "
                                + "  {\"index\":16,\"name\":\"joint16\",\"x\":715.6403255423252,\"y\":38.0}, "
                                + "  {\"index\":17,\"name\":\"joint17\",\"x\":624.5,\"y\":29.215390309173472}, "
                                + "  {\"index\":18,\"name\":\"joint18\",\"x\":500.0,\"y\":26.0}, "
                                + "  {\"index\":19,\"name\":\"joint19\",\"x\":375.50000000000006,\"y\":29.215390309173472}, "
                                + "  {\"index\":20,\"name\":\"joint20\",\"x\":284.3596744576748,\"y\":37.99999999999999}, "
                                + "  {\"index\":21,\"name\":\"joint21\",\"x\":251.0,\"y\":50.0}, "
                                + "  {\"index\":22,\"name\":\"joint22\",\"x\":284.3596744576747,\"y\":61.99999999999999}, "
                                + "  {\"index\":23,\"name\":\"joint23\",\"x\":375.4999999999999,\"y\":70.78460969082653}, "
                                + "  {\"index\":24,\"name\":\"joint24\",\"x\":500.0,\"y\":50.0}], "
                                + "\"roadSections\":[ "
                                + "  {\"index\":0,\"joint0\":0,\"joint1\":12}, "
                                + "  {\"index\":1,\"joint0\":1,\"joint1\":13}, "
                                + "  {\"index\":2,\"joint0\":2,\"joint1\":14}, "
                                + "  {\"index\":3,\"joint0\":3,\"joint1\":15}, "
                                + "  {\"index\":4,\"joint0\":4,\"joint1\":16}, "
                                + "  {\"index\":5,\"joint0\":5,\"joint1\":17}, "
                                + "  {\"index\":6,\"joint0\":6,\"joint1\":18}, "
                                + "  {\"index\":7,\"joint0\":7,\"joint1\":19}, "
                                + "  {\"index\":8,\"joint0\":8,\"joint1\":20}, "
                                + "  {\"index\":9,\"joint0\":9,\"joint1\":21}, "
                                + "  {\"index\":10,\"joint0\":10,\"joint1\":22}, "
                                + "  {\"index\":11,\"joint0\":11,\"joint1\":23}, "
                                + "  {\"index\":12,\"joint0\":0,\"joint1\":1}, "
                                + "  {\"index\":13,\"joint0\":1,\"joint1\":2}, "
                                + "  {\"index\":14,\"joint0\":2,\"joint1\":3}, "
                                + "  {\"index\":15,\"joint0\":3,\"joint1\":4}, "
                                + "  {\"index\":16,\"joint0\":4,\"joint1\":5}, "
                                + "  {\"index\":17,\"joint0\":5,\"joint1\":6}, "
                                + "  {\"index\":18,\"joint0\":6,\"joint1\":7}, "
                                + "  {\"index\":19,\"joint0\":7,\"joint1\":8}, "
                                + "  {\"index\":20,\"joint0\":8,\"joint1\":9}, "
                                + "  {\"index\":21,\"joint0\":9,\"joint1\":10}, "
                                + "  {\"index\":22,\"joint0\":10,\"joint1\":11}, "
                                + "  {\"index\":23,\"joint0\":11,\"joint1\":0}, "
                                + "  {\"index\":24,\"joint0\":12,\"joint1\":24}, "
                                + "  {\"index\":25,\"joint0\":13,\"joint1\":24}, "
                                + "  {\"index\":26,\"joint0\":14,\"joint1\":24}, "
                                + "  {\"index\":27,\"joint0\":15,\"joint1\":24}, "
                                + "  {\"index\":28,\"joint0\":16,\"joint1\":24}, "
                                + "  {\"index\":29,\"joint0\":17,\"joint1\":24}, "
                                + "  {\"index\":30,\"joint0\":18,\"joint1\":24}, "
                                + "  {\"index\":31,\"joint0\":19,\"joint1\":24}, "
                                + "  {\"index\":32,\"joint0\":20,\"joint1\":24}, "
                                + "  {\"index\":33,\"joint0\":21,\"joint1\":24}, "
                                + "  {\"index\":34,\"joint0\":22,\"joint1\":24}, "
                                + "  {\"index\":35,\"joint0\":23,\"joint1\":24}, "
                                + "  {\"index\":36,\"joint0\":12,\"joint1\":13}, "
                                + "  {\"index\":37,\"joint0\":13,\"joint1\":14}, "
                                + "  {\"index\":38,\"joint0\":14,\"joint1\":15}, "
                                + "  {\"index\":39,\"joint0\":15,\"joint1\":16}, "
                                + "  {\"index\":40,\"joint0\":16,\"joint1\":17}, "
                                + "  {\"index\":41,\"joint0\":17,\"joint1\":18}, "
                                + "  {\"index\":42,\"joint0\":18,\"joint1\":19}, "
                                + "  {\"index\":43,\"joint0\":19,\"joint1\":20}, "
                                + "  {\"index\":44,\"joint0\":20,\"joint1\":21}, "
                                + "  {\"index\":45,\"joint0\":21,\"joint1\":22}, "
                                + "  {\"index\":46,\"joint0\":22,\"joint1\":23}, "
                                + "  {\"index\":47,\"joint0\":23,\"joint1\":12}] "
                                + "} ")).readObject()
    }

    static class TestPropertiesReader extends SimplePropertiesReader {

        TestPropertiesReader() {
            super();
        }

        @Override
        Properties getBoardProperties() throws IOException {
            final Properties simpleBoardPropertiesSet = super.getBoardProperties();
            simpleBoardPropertiesSet.setProperty("Board.xsize", "1000");
            simpleBoardPropertiesSet.setProperty("Board.ysize", "100");
            return simpleBoardPropertiesSet;
        }

    }

    private Module getTestModule() {
        return new AbstractModule() {
            @Override protected void configure() {
                bind(PropertiesReader.class).to(ClockBoardGeneratorSpec.TestPropertiesReader.class);
            }
        };
    }
}
