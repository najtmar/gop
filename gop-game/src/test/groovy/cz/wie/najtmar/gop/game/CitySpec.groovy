package cz.wie.najtmar.gop.game


import com.google.gson.JsonParser
import cz.wie.najtmar.gop.board.Board
import cz.wie.najtmar.gop.board.Joint
import cz.wie.najtmar.gop.board.Position
import cz.wie.najtmar.gop.board.RoadSection
import cz.wie.najtmar.gop.common.Constants
import cz.wie.najtmar.gop.common.Point2D
import cz.wie.najtmar.gop.common.TestUtils
import cz.wie.najtmar.gop.event.Event
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

import javax.json.Json

import static spock.util.matcher.HamcrestMatchers.closeTo
import static spock.util.matcher.HamcrestSupport.that

class CitySpec extends Specification {

    static final long START_MOVE_TIME = 1000;

    Board board;
    Joint joint00;
    Joint joint30;
    Joint joint33;
    Joint joint60;
    @Shared
    RoadSection roadSection0;
    RoadSection roadSection1;
    @Shared
    RoadSection roadSection2;

    Game game;
    Player playerA;
    Player playerB;
    City cityA;
    City cityB;
    CityFactory cityAFactory0;
    CityFactory cityAFactory1;
    CityFactory cityAFactory2;

    EventFactory eventFactory;
    LinkedList<Event> eventsGenerated;

    def setup() throws Exception {
        final Properties simpleBoardPropertiesSet = new Properties();
        simpleBoardPropertiesSet.load(CitySpec.class.getResourceAsStream(
                "/cz/wie/najtmar/gop/board/simple-board.properties"));
        board = new Board(simpleBoardPropertiesSet);
        joint00 = (new Joint.Builder())
                .setBoard(board)
                .setIndex(0)
                .setName("joint00")
                .setCoordinatesPair(new Point2D.Double(0.0, 0.0))
                .build();
        joint30 = (new Joint.Builder())
                .setBoard(board)
                .setIndex(1)
                .setName("joint30")
                .setCoordinatesPair(new Point2D.Double(3.0, 0.0))
                .build();
        joint33 = (new Joint.Builder())
                .setBoard(board)
                .setIndex(2)
                .setName("joint33")
                .setCoordinatesPair(new Point2D.Double(3.0, 3.0))
                .build();
        joint60 = (new Joint.Builder())
                .setBoard(board)
                .setIndex(3)
                .setName("joint60")
                .setCoordinatesPair(new Point2D.Double(6.0, 0.0))
                .build();
        roadSection0 = (new RoadSection.Builder())
                .setBoard(board)
                .setIndex(0)
                .setJoints(joint00, joint30)
                .build();
        roadSection1 = (new RoadSection.Builder())
                .setBoard(board)
                .setIndex(1)
                .setJoints(joint30, joint33)
                .build();
        roadSection2 = (new RoadSection.Builder())
                .setBoard(board)
                .setIndex(2)
                .setJoints(joint30, joint60)
                .build();

        final Properties simpleGamePropertiesSet = new Properties();
        simpleGamePropertiesSet.load(CitySpec.class.getResourceAsStream(
                "/cz/wie/najtmar/gop/game/simple-game.properties"));
        simpleGamePropertiesSet.setProperty(GameProperties.START_TIME_PROPERTY_NAME, "" + START_MOVE_TIME);
        game = new Game(simpleGamePropertiesSet);
        game.setBoard(board);
        playerA = new Player(new User("playerA", new UUID(12345, 67890)), 0, game);
        playerB = new Player(new User("playerB", new UUID(98765, 43210)), 1, game);
        game.addPlayer(playerA);
        game.addPlayer(playerB);
        City.resetFirstAvailableId();
        cityA = (new City.Builder())
                .setGame(game)
                .setName("cityA")
                .setPlayer(playerA)
                .setJoint(joint30)
                .build();
        cityAFactory0 = cityA.getFactory(0);
        cityAFactory1 = cityA.getFactory(1);
        cityAFactory2 = cityA.getFactory(2);
        cityB = (new City.Builder())
                .setGame(game)
                .setName("cityB")
                .setPlayer(playerB)
                .setJoint(joint33)
                .build();
        game.initialize();

        final ObjectSerializer objectSerializer = new ObjectSerializer(game);
        eventFactory = new EventFactory(objectSerializer);
        game.advanceTimeTo(START_MOVE_TIME + 10);
        eventsGenerated = new LinkedList<>();
    }

    def "should create cities correctly"() {
        expect: "two cities created correctly"
        // cityA
        cityA.getGame() == game
        cityA.getId() == 0
        cityA.getJoint() == joint30
        cityA.getName() == "cityA"
        cityA.getPlayer() == playerA
        cityA.getSize() == 1
        game.getPlayerCities(playerA) == [cityA] as Set
        // cityB
        cityB.getGame() == game
        cityB.getId() == 1
        cityB.getJoint() == joint33
        cityB.getName() == "cityB"
        cityB.getPlayer() == playerB
        cityB.getSize() == 1
        game.getPlayerCities(playerB) == [cityB] as Set
    }

    def "should correctly grow and shrink city"() {
        expect: "a city created as expected"
        game.getPlayerCities(playerA) == [cityA] as Set
        cityAFactory0.getState() == CityFactory.State.ENABLED
        cityAFactory1.getState() == CityFactory.State.DISABLED
        cityA.getSize() == 1

        when: "the city grows"
        cityA.grow();

        then: "the city grown as expected"
        cityA.getSize() == 2
        cityAFactory0.getState() == CityFactory.State.ENABLED
        cityAFactory1.getState() == CityFactory.State.ENABLED
        cityAFactory0.getIndex() == 0
        cityAFactory1.getIndex() == 1

        when: "factory actions set"
        cityAFactory0.setAction(new CityFactory.ProduceWarriorAction(cityAFactory0, 0.0))
        cityAFactory0.getAction().increaseProgressByUniversalDelta(
                0.5 / game.getGameProperties().getProduceActionProgressFactor(), eventFactory, eventsGenerated);
        cityAFactory1.setAction(new CityFactory.ProduceSettlersUnitAction(cityAFactory1))
        cityAFactory1.getAction().increaseProgressByUniversalDelta(
                0.7 / game.getGameProperties().getProduceActionProgressFactor(), eventFactory, eventsGenerated);

        then: "events generated correctly"
        eventsGenerated == [
                new Event(Json.createReader(new StringReader(
                        "{ "
                                + "  \"type\": \"SET_FACTORY_PROGRESS\", "
                                + "  \"time\": " + (START_MOVE_TIME + 10) + ", "
                                + "  \"factory\": { "
                                + "    \"city\": 0, "
                                + "    \"index\": 0 "
                                + "  }, "
                                + "  \"progress\": 0.5 "
                                + "}")).readObject()),
                new Event(Json.createReader(new StringReader(
                        "{ "
                                + "  \"type\": \"SET_FACTORY_PROGRESS\", "
                                + "  \"time\": " + (START_MOVE_TIME + 10) + ", "
                                + "  \"factory\": { "
                                + "    \"city\": 0, "
                                + "    \"index\": 1 "
                                + "  }, "
                                + "  \"progress\": 0.7 "
                                + "}")).readObject())]

        and: "factories snapshot correctly"
        TestUtils.toString(cityA.snapshot().getAsJsonArray("factories")) == """[
  {
    "state": "ENABLED",
    "index": 0,
    "action": {
      "state": "INITIALIZED",
      "progress": 0.5,
      "type": "ProduceWarriorAction",
      "builder": {
        "currentPosition": {
          "roadSection": 0,
          "index": 3,
          "joint": 1,
          "hashCode": 899
        },
        "direction": 0.0,
        "creationTime": 1010
      },
      "direction": 0.0,
      "progressFactor": 0.667
    }
  },
  {
    "state": "ENABLED",
    "index": 1,
    "action": {
      "state": "INITIALIZED",
      "progress": 0.7,
      "type": "ProduceSettlersUnitAction",
      "builder": {
        "currentPosition": {
          "roadSection": 0,
          "index": 3,
          "joint": 1,
          "hashCode": 899
        },
        "creationTime": 1010
      },
      "progressFactor": 0.667
    }
  },
  {
    "state": "DISABLED",
    "index": 2,
    "action": null
  }
]"""

        when: "the city shrinks with the last factory"
        cityA.shrinkOrDestroy(cityA.getSize() - 1);

        then: "the last city factory gets disabled"
        cityA.getSize() == 1
        cityAFactory0.getState() == CityFactory.State.ENABLED
        cityA.getFactory(0) == cityAFactory0
        cityAFactory0.getAction() != null
        that cityAFactory0.getAction().getProgress(), closeTo(0.5, Constants.DOUBLE_DELTA)
        cityAFactory1.getState() == CityFactory.State.DISABLED
        game.getPlayerCities(playerA) == [cityA] as Set

        when: "the city grows again"
        cityA.grow();

        then: "the factory is created and reset"
        cityA.getSize() == 2
        cityAFactory0.getState() == CityFactory.State.ENABLED
        cityA.getFactory(0) == cityAFactory0
        cityAFactory0.getAction() != null
        that cityAFactory0.getAction().getProgress(), closeTo (Constants.DOUBLE_DELTA, 0.5)
        cityAFactory1.getState() == CityFactory.State.ENABLED
        cityA.getFactory(1) == cityAFactory1
        cityAFactory1.getAction() == null
        game.getPlayerCities(playerA) == [cityA] as Set

        when: "the city gets destroyed"
        cityA.shrinkOrDestroy(cityA.getSize() - 1);
        cityA.shrinkOrDestroy(cityA.getSize() - 1);

        then: "the city is recognized as such"
        cityA.getSize() == 0
        cityAFactory0.getState() == CityFactory.State.DISABLED
        cityAFactory1.getState() == CityFactory.State.DISABLED
        game.getPlayerCities(playerA) == [] as Set

        when: "trying to grow a destroyed city"
        cityA.grow();

        then: "a GameException is thrown"
        def exception = thrown(GameException)
        exception.getMessage() == "Destroyed cities (of size 0) cannot grow."
    }

    def "should correctly shrink city by not last factory"() {
        expect: "city exists"
        game.getPlayerCities(playerA) == [cityA] as Set
        cityAFactory0.getState() == CityFactory.State.ENABLED
        cityAFactory0.getIndex() == 0
        cityAFactory1.getState() == CityFactory.State.DISABLED
        cityAFactory1.getIndex() == 1
        cityA.getSize() == 1

        when: "the city grows"
        cityA.grow();

        then: "the seconf factory is enabled"
        cityA.getSize() == 2
        cityAFactory0.getState() == CityFactory.State.ENABLED
        cityAFactory1.getState() == CityFactory.State.ENABLED
        cityAFactory0.getIndex() == 0
        cityAFactory1.getIndex() == 1

        when: "factory actions set"
        cityAFactory0.setAction(new CityFactory.ProduceWarriorAction(cityAFactory0, 0.0));
        cityAFactory0.getAction().increaseProgressByUniversalDelta(
                0.5 / game.getGameProperties().getProduceActionProgressFactor(), eventFactory, eventsGenerated);
        cityAFactory1.setAction(new CityFactory.ProduceSettlersUnitAction(cityAFactory1));
        cityAFactory1.getAction().increaseProgressByUniversalDelta(
                0.7 / game.getGameProperties().getProduceActionProgressFactor(), eventFactory, eventsGenerated);

        then: "proper events generated"
        eventsGenerated == [new Event(Json.createReader(new StringReader(
                        "{ "
                                + "  \"type\": \"SET_FACTORY_PROGRESS\", "
                                + "  \"time\": " + (START_MOVE_TIME + 10) + ", "
                                + "  \"factory\": { "
                                + "    \"city\": 0, "
                                + "    \"index\": 0 "
                                + "  }, "
                                + "  \"progress\": 0.5 "
                                + "}")).readObject()),
                new Event(Json.createReader(new StringReader(
                        "{ "
                                + "  \"type\": \"SET_FACTORY_PROGRESS\", "
                                + "  \"time\": " + (START_MOVE_TIME + 10) + ", "
                                + "  \"factory\": { "
                                + "    \"city\": 0, "
                                + "    \"index\": 1 "
                                + "  }, "
                                + "  \"progress\": 0.7 "
                                + "}")).readObject())]

        and: "factories snapshot correctly"
        cityA.snapshot().getAsJsonArray("factories") == new JsonParser().parseString("""
  [
    {
      "state": "ENABLED",
      "index": 0,
      "action": {
        "state": "INITIALIZED",
        "progress": 0.5,
        "type": "ProduceWarriorAction",
        "builder": {
          "currentPosition": {
            "roadSection": 0,
            "index": 3,
            "joint": 1,
            "hashCode": 899
          },
          "direction": 0.0,
          "creationTime": 1010
        },
        "direction": 0.0,
        "progressFactor": 0.667
      }
    },
    {
      "state": "ENABLED",
      "index": 1,
      "action": {
        "state": "INITIALIZED",
        "progress": 0.7,
        "type": "ProduceSettlersUnitAction",
        "builder": {
          "currentPosition": {
            "roadSection": 0,
            "index": 3,
            "joint": 1,
            "hashCode": 899
          },
          "creationTime": 1010
        },
        "progressFactor": 0.667
      }
    },
    {
      "state": "DISABLED",
      "index": 2,
      "action": null
    }
  ]""")

        when: "the city shrinks on a not last factory"
        cityA.shrinkOrDestroy(0);

        then: "only the shrunk factory gets disabled"
        cityA.getSize() == 1
        cityAFactory1.getState() == CityFactory.State.ENABLED
        cityA.getFactory(0) == cityAFactory1
        cityAFactory1.getAction() != null
        cityAFactory1.getIndex() == 0
        cityAFactory0.getIndex() == 1
        that cityAFactory1.getAction().getProgress(), closeTo(0.7, Constants.DOUBLE_DELTA)
        cityAFactory0.getState() == CityFactory.State.DISABLED
        game.getPlayerCities(playerA) == [cityA] as Set

        when: "the city grows again"
        cityA.grow()

        then: "the previously shrunk factory gets re-enabled and reset"
        cityA.getSize() == 2
        cityAFactory0.getState() == CityFactory.State.ENABLED
        cityA.getFactory(1) == cityAFactory0
        cityAFactory0.getAction() == null
    }

    def "should capture city correctly"() {
        expect: "cityA belongs to playerA"
        cityA.getPlayer() == playerA

        when: "the city grows"
        cityA.grow();

        then: "the city size is correctly recognized"
        cityA.getSize() == 2

        when: "factory actions are set"
        cityAFactory0.setAction(new CityFactory.ProduceWarriorAction(cityAFactory0, 0.0));
        cityAFactory0.getAction().increaseProgressByUniversalDelta(
                0.5 / game.getGameProperties().getProduceActionProgressFactor(), eventFactory, eventsGenerated);
        cityAFactory1.setAction(new CityFactory.ProduceSettlersUnitAction(cityAFactory1));
        cityAFactory1.getAction().increaseProgressByUniversalDelta(
                0.7 / game.getGameProperties().getProduceActionProgressFactor(), eventFactory, eventsGenerated);

        then: "correct events are created"
        eventsGenerated == [
                new Event(Json.createReader(new StringReader(
                        "{ "
                                + "  \"type\": \"SET_FACTORY_PROGRESS\", "
                                + "  \"time\": " + (START_MOVE_TIME + 10) + ", "
                                + "  \"factory\": { "
                                + "    \"city\": 0, "
                                + "    \"index\": 0 "
                                + "  }, "
                                + "  \"progress\": 0.5 "
                                + "}")).readObject()),
                new Event(Json.createReader(new StringReader(
                        "{ "
                                + "  \"type\": \"SET_FACTORY_PROGRESS\", "
                                + "  \"time\": " + (START_MOVE_TIME + 10) + ", "
                                + "  \"factory\": { "
                                + "    \"city\": 0, "
                                + "    \"index\": 1 "
                                + "  }, "
                                + "  \"progress\": 0.7 "
                                + "}")).readObject())]

        and: "factories snapshot correctly"
        cityA.snapshot().getAsJsonArray("factories") == new JsonParser().parseString("""
  [
    {
      "state": "ENABLED",
      "index": 0,
      "action": {
        "state": "INITIALIZED",
        "progress": 0.5,
        "type": "ProduceWarriorAction",
        "builder": {
          "currentPosition": {
            "roadSection": 0,
            "index": 3,
            "joint": 1,
            "hashCode": 899
          },
          "direction": 0.0,
          "creationTime": 1010
        },
        "direction": 0.0,
        "progressFactor": 0.667
      }
    },
    {
      "state": "ENABLED",
      "index": 1,
      "action": {
        "state": "INITIALIZED",
        "progress": 0.7,
        "type": "ProduceSettlersUnitAction",
        "builder": {
          "currentPosition": {
            "roadSection": 0,
            "index": 3,
            "joint": 1,
            "hashCode": 899
          },
          "creationTime": 1010
        },
        "progressFactor": 0.667
      }
    },
    {
      "state": "DISABLED",
      "index": 2,
      "action": null
    }
  ]""")

        when: "the city is captured by playerB"
        cityA.captureOrDestroy(playerB);

        then: "the city belongs to playerB"
        cityA.getPlayer() == playerB

        and: "the city has shrunk"
        cityA.getSize() == 1

        and: "the factory has been reset"
        cityA.getFactory(0) == cityAFactory0
        cityAFactory0.getState() == CityFactory.State.ENABLED
        cityAFactory0.getAction() == null

        when: "the city grows"
        cityA.grow();

        then: "the city has grown and the state of the new factory is reset"
        cityA.getFactory(1) == cityAFactory1
        cityAFactory1.getState() == CityFactory.State.ENABLED
        cityAFactory1.getAction() == null

        when: "the city shinks"
        cityA.shrinkOrDestroy(cityA.getSize() - 1);

        and: "is captured and destroyed by playerA"
        cityA.captureOrDestroy(playerA);

        then: "the city belongs to playerA and is destroyed"
        cityA.getPlayer() == playerA
        cityA.getSize() == 0
    }

    def "cultivable area of the opponent's city should be calculated currectly"() throws Exception {
        expect: "cultivable area of the opponent's city is calculated currectly"
        cityA.calculateCultivableArea() == 3.0d - 0.5d
    }

    @Unroll
    def "cultivable area are should be calculated correctly"() {
        when: "a prawn is added"
        def prawn = new TestPrawn(game.getGameProperties(), player == "A" ? playerA : playerB, currentPosition, START_MOVE_TIME)
        if (newPosition != null) {
            prawn.setCurrentPosition(newPosition)
        }
        game.addPrawn(prawn)

        then: "cultivable are calculated as expected"
        cityA.calculateCultivableArea() == expectedCultivableArea as double

        where:
        player | currentPosition               | newPosition                   || expectedCultivableArea
        "B"    | new Position(roadSection2, 0) | new Position(roadSection2, 1) || (3.0 - 0.5) - 1.0
        "A"    | new Position(roadSection2, 0) | new Position(roadSection2, 1) || (3.0 - 0.5)
        "B"    | new Position(roadSection0, 0) | new Position(roadSection0, 2) || (3.0 - 0.5) - 1.0
        "B"    | new Position(roadSection2, 0) | new Position(roadSection2, 3) || (3.0 - 0.5) - 0.5
        "B"    | new Position(roadSection0, 0) | null                          || (3.0 - 0.5) - 0.5
    }

    def "cultivable area with multiple opponents should be calculated correctly"() {
        when: "two opponent's prawns are added"
        def opponentsPrawn0 = new TestPrawn(game.getGameProperties(), playerB, new Position(roadSection2, 0),
                START_MOVE_TIME);
        opponentsPrawn0.setCurrentPosition(new Position(roadSection2, 3));
        game.addPrawn(opponentsPrawn0);
        def opponentsPrawn1 = new TestPrawn(game.getGameProperties(), playerB, new Position(roadSection0, 0),
                START_MOVE_TIME);
        game.addPrawn(opponentsPrawn1);

        then: "cultivable area is calculated correctly"
        cityA.calculateCultivableArea() == (3.0 - 0.5) - 0.5 - 0.5 as double
    }

    def "cultivable area near another own city is calculated correctly"() {
        when: "an own city created nearby"
        (new City.Builder())
                .setGame(game)
                .setName("cityA1")
                .setPlayer(playerA)
                .setJoint(joint00)
                .build()

        then: "cultivable are calculated correctly"
        cityA.calculateCultivableArea() == (3.0 - 0.5) - 0.5 as double
    }

    def "cultivable are new two other cities calculated correctly"() {
        when: "two other cities created nearby"
        (new City.Builder())
                .setGame(game)
                .setName("cityA1")
                .setPlayer(playerA)
                .setJoint(joint00)
                .build();
        (new City.Builder())
                .setGame(game)
                .setName("cityB1")
                .setPlayer(playerB)
                .setJoint(joint60)
                .build();

        then: "cultivable area calculated correctly"
        cityA.calculateCultivableArea() == (3.0 - 0.5) - 0.5 - 0.5 as double
    }

}
