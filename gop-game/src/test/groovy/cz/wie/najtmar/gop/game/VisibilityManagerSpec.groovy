package cz.wie.najtmar.gop.game

import cz.wie.najtmar.gop.board.Board
import cz.wie.najtmar.gop.board.Joint
import cz.wie.najtmar.gop.board.Position
import cz.wie.najtmar.gop.board.RoadSection
import cz.wie.najtmar.gop.common.Point2D
import cz.wie.najtmar.gop.event.Event
import spock.lang.Specification

import javax.json.Json

import static cz.wie.najtmar.gop.common.TestUtils.toString

class VisibilityManagerSpec extends Specification {

  static final long START_TIME = 966;
  static final long INTERRUPT_TIME_PERIOD = 100;
  static Event TEST_EVENT;

  Board board;
  Joint joint0;
  Joint joint1;
  Joint joint2;
  Joint joint3;
  RoadSection roadSection0;
  RoadSection roadSection1;
  RoadSection roadSection2;

  Game game;
  GameProperties gameProperties;
  EventFactory eventFactory;
  Player playerA;
  Player playerB;
  Player playerC;
  Warrior warriorA;
  Warrior warriorB;
  Warrior warriorC;
  SettlersUnit settlersUnitA;
  SettlersUnit settlersUnitB;
  City cityA;
  City cityA1;
  City cityB;
  City cityC;
  VisibilityManager.Factory visibilityManagerAFactory;
  VisibilityManager.Factory visibilityManagerBFactory;
  VisibilityManager.Factory visibilityManagerCFactory;
  VisibilityManager visibilityManagerA;
  VisibilityManager visibilityManagerB;
  VisibilityManager visibilityManagerC;

  /**
   * Method setUpBeforeClass() .
   * @throws Exception when the method execution fails
   */
  def setupSpec() throws Exception {
    TEST_EVENT = new Event(Json.createObjectBuilder()
        .add("type", "TEST")
        .add("time", START_TIME)
        .add("whatever", "value")
        .build());
  }

  /**
   * Method setUp().
   * @throws Exception when something unexpected happens
   */
  def setup() throws Exception {
    final Properties simpleBoardPropertiesSet = new Properties();
    simpleBoardPropertiesSet.load(VisibilityManagerSpec.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/board/simple-board.properties"));
    board = new Board(simpleBoardPropertiesSet);
    joint0 = (new Joint.Builder())
        .setBoard(board)
        .setIndex(0)
        .setName("joint0")
        .setCoordinatesPair(new Point2D.Double(1.0, 0.0))
        .build();
    joint1 = (new Joint.Builder())
        .setBoard(board)
        .setIndex(1)
        .setName("joint1")
        .setCoordinatesPair(new Point2D.Double(4.0, 0.0))
        .build();
    joint2 = (new Joint.Builder())
        .setBoard(board)
        .setIndex(2)
        .setName("joint2")
        .setCoordinatesPair(new Point2D.Double(4.0, 4.0))
        .build();
    joint3 = (new Joint.Builder())
        .setBoard(board)
        .setIndex(3)
        .setName("joint3")
        .setCoordinatesPair(new Point2D.Double(0.0, 4.0))
        .build();
    roadSection0 = (new RoadSection.Builder())
        .setBoard(board)
        .setIndex(0)
        .setJoints(joint0, joint1)
        .build();
    roadSection1 = (new RoadSection.Builder())
        .setBoard(board)
        .setIndex(1)
        .setJoints(joint1, joint2)
        .build();
    roadSection2 = (new RoadSection.Builder())
        .setBoard(board)
        .setIndex(2)
        .setJoints(joint2, joint3)
        .build();
    [joint0, joint1, joint2, joint3].forEach { board.addJoint(it) }
    [roadSection0, roadSection1, roadSection2].forEach { board.addRoadSection(it) }

    final Properties simpleGamePropertiesSet = new Properties();
    simpleGamePropertiesSet.load(VisibilityManagerSpec.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/game/simple-game.properties"));
    simpleGamePropertiesSet.setProperty(GameProperties.START_TIME_PROPERTY_NAME, "" + START_TIME);
    game = new Game(simpleGamePropertiesSet);
    game.setBoard(board);
    gameProperties = game.getGameProperties();
    playerA = new Player(new User("playerA", new UUID(12345, 67890)), 0, game);
    playerB = new Player(new User("playerB", new UUID(98765, 43210)), 1, game);
    playerC = new Player(new User("playerC", new UUID(12345, 54321)), 2, game);
    game.addPlayer(playerA);
    game.addPlayer(playerB);
    game.addPlayer(playerC);
    Prawn.resetFirstAvailableId();
    warriorA = (new Warrior.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(joint2.getNormalizedPosition())
        .setDirection(0.0)
        .setGameProperties(gameProperties)
        .setPlayer(playerA)
        .build();
    warriorB = (new Warrior.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(joint1.getNormalizedPosition())
        .setDirection(Math.PI)
        .setGameProperties(gameProperties)
        .setPlayer(playerB)
        .build();
    warriorC = (new Warrior.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(joint0.getNormalizedPosition())
        .setDirection(3 * Math.PI / 2)
        .setGameProperties(gameProperties)
        .setPlayer(playerC)
        .build();
    settlersUnitA = (new SettlersUnit.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(joint2.getNormalizedPosition())
        .setGameProperties(gameProperties)
        .setPlayer(playerA)
        .build();
    settlersUnitB = (new SettlersUnit.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(joint1.getNormalizedPosition())
        .setGameProperties(gameProperties)
        .setPlayer(playerB)
        .build();
    game.addPrawn(warriorA);
    game.addPrawn(warriorB);
    game.addPrawn(warriorC);
    game.addPrawn(settlersUnitA);
    game.addPrawn(settlersUnitB);
    warriorB.setCurrentPosition(new Position(roadSection0, 2));
    warriorC.setCurrentPosition(new Position(roadSection0, 1));
    settlersUnitA.setCurrentPosition(new Position(roadSection2, 4));
    City.resetFirstAvailableId();
    cityA = (new City.Builder())
        .setGame(game)
        .setName("city")
        .setPlayer(playerA)
        .setJoint(joint2)
        .build();
    cityA1 = (new City.Builder())
        .setGame(game)
        .setName("City A")
        .setPlayer(playerA)
        .setJoint(joint3)
        .build();
    cityB = (new City.Builder())
        .setGame(game)
        .setName("City B")
        .setPlayer(playerB)
        .setJoint(joint1)
        .build();
    cityC = (new City.Builder())
        .setGame(game)
        .setName("City C")
        .setPlayer(playerC)
        .setJoint(joint0)
        .build();
    game.initialize();
    game.start();

    final ObjectSerializer objectSerializer = new ObjectSerializer(game);
    eventFactory = new EventFactory(objectSerializer);
    game.advanceTimeTo(START_TIME);
    visibilityManagerAFactory = new VisibilityManager.Factory(game, eventFactory, 0);
    visibilityManagerBFactory = new VisibilityManager.Factory(game, eventFactory, 1);
    visibilityManagerCFactory = new VisibilityManager.Factory(game, eventFactory, 2);
    visibilityManagerA = visibilityManagerAFactory.create();
    visibilityManagerB = visibilityManagerBFactory.create();
    visibilityManagerC = visibilityManagerCFactory.create();
  }

  def "visible road section halves should be found correctly"() throws Exception {
    expect:
    visibilityManagerA.getVisibleRoadSectionHalves() == null

    when:
    visibilityManagerA.initializeVisibleRoadSectionHalves();

    then:
    visibilityManagerA.getVisibleRoadSectionHalves() == [
            roadSection1.getHalves()[1], roadSection1.getHalves()[0], roadSection2.getHalves()[0], roadSection2.getHalves()[1]] as Set
    visibilityManagerB.getVisibleRoadSectionHalves() == null

    when:
    visibilityManagerB.initializeVisibleRoadSectionHalves();

    then:
    visibilityManagerB.getVisibleRoadSectionHalves() == [
            roadSection0.getHalves()[1], roadSection0.getHalves()[0], roadSection1.getHalves()[0],
            roadSection1.getHalves()[1]] as Set
    visibilityManagerC.getVisibleRoadSectionHalves() == null

    when:
    visibilityManagerC.initializeVisibleRoadSectionHalves();

    then:
    visibilityManagerC.getVisibleRoadSectionHalves() == [roadSection0.getHalves()[0], roadSection0.getHalves()[1]] as Set

    when:
    def gameSnapshot = game.snapshot()
    def restoredGame = new Game(gameSnapshot)
    assert restoredGame.getState() == Game.State.INITIALIZED
    restoredGame.state = Game.State.RUNNING

    then:
    toString(restoredGame) == toString(game)
  }

  def "prawns and cities already visible or discovered"() {
    given:
    game.putPlayerVisibleRoadSectionHalves(playerA,
            [roadSection1.getHalves()[0], roadSection1.getHalves()[1], roadSection2.getHalves()[0], roadSection2.getHalves()[1]] as Set);
    game.putPlayerVisibleRoadSectionHalves(playerB,
            [roadSection0.getHalves()[0], roadSection0.getHalves()[1], roadSection1.getHalves()[0], roadSection1.getHalves()[1]] as Set);
    game.putPlayerVisibleRoadSectionHalves(playerC, [roadSection0.getHalves()[0], roadSection0.getHalves()[1]] as Set);
    game.addPlayerDiscoveredRoadSectionHalves(playerA,
            [roadSection1.getHalves()[0], roadSection1.getHalves()[1], roadSection2.getHalves()[0], roadSection2.getHalves()[1]] as Set);
    game.addPlayerDiscoveredRoadSectionHalves(playerB,
            [roadSection0.getHalves()[0], roadSection0.getHalves()[1], roadSection1.getHalves()[0], roadSection1.getHalves()[1]] as Set);
    game.addPlayerDiscoveredRoadSectionHalves(playerC, [roadSection0.getHalves()[0], roadSection0.getHalves()[1]] as Set);

    game.putPlayerVisiblePrawns(playerA, new HashSet<>(Arrays.asList(warriorA, settlersUnitA, settlersUnitB)));
    game.putPlayerVisiblePrawns(playerB, new HashSet<>(Arrays.asList(warriorB, settlersUnitB, warriorA, warriorC)));
    game.putPlayerVisiblePrawns(playerC, new HashSet<>(Arrays.asList(warriorC, warriorB, settlersUnitB)));

    game.putPlayerDiscoveredCities(playerA, new HashSet<>(Arrays.asList(cityA, cityA1, cityB)));
    game.putPlayerDiscoveredCities(playerB, new HashSet<>(Arrays.asList(cityB, cityA, cityC)));
    game.putPlayerDiscoveredCities(playerC, new HashSet<>(Arrays.asList(cityC, cityB)));
    game.putPlayerVisibleCities(playerA, new HashSet<>(Arrays.asList(cityA, cityA1, cityB)));
    game.putPlayerVisibleCities(playerB, new HashSet<>(Arrays.asList(cityB, cityA, cityC)));
    game.putPlayerVisibleCities(playerC, new HashSet<>(Arrays.asList(cityC, cityB)));

    when:
    visibilityManagerA.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    visibilityManagerB.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    visibilityManagerC.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));

    visibilityManagerA.executeAndGenerate();
    visibilityManagerB.executeAndGenerate();
    visibilityManagerC.executeAndGenerate();

    then:
    visibilityManagerA.getState() == EventGeneratingExecutor.State.GENERATED
    visibilityManagerB.getState() == EventGeneratingExecutor.State.GENERATED
    visibilityManagerC.getState() == EventGeneratingExecutor.State.GENERATED

    visibilityManagerA.getEventsGenerated() == [TEST_EVENT]
    visibilityManagerB.getEventsGenerated() == [TEST_EVENT]
    visibilityManagerC.getEventsGenerated() == [TEST_EVENT]

    game.getPlayerVisibleRoadSectionHalves(playerA) == [
            roadSection1.getHalves()[0], roadSection1.getHalves()[1], roadSection2.getHalves()[0], roadSection2.getHalves()[1]] as Set
    game.getPlayerVisibleRoadSectionHalves(playerB) == [
            roadSection0.getHalves()[0], roadSection0.getHalves()[1], roadSection1.getHalves()[0], roadSection1.getHalves()[1]] as Set
    game.getPlayerVisibleRoadSectionHalves(playerC) == [roadSection0.getHalves()[0], roadSection0.getHalves()[1]] as Set
    game.getPlayerDiscoveredRoadSectionHalves(playerA) == [
            roadSection1.getHalves()[0], roadSection1.getHalves()[1], roadSection2.getHalves()[0], roadSection2.getHalves()[1]] as Set
    game.getPlayerDiscoveredRoadSectionHalves(playerB) == [
            roadSection0.getHalves()[0], roadSection0.getHalves()[1], roadSection1.getHalves()[0], roadSection1.getHalves()[1]] as Set
    game.getPlayerDiscoveredRoadSectionHalves(playerC) == [roadSection0.getHalves()[0], roadSection0.getHalves()[1]] as Set

    game.getPlayerVisiblePrawns(playerA) == [warriorA, settlersUnitA, settlersUnitB] as Set
    game.getPlayerVisiblePrawns(playerB) == [warriorB, settlersUnitB, warriorA, warriorC] as Set
    game.getPlayerVisiblePrawns(playerC) == [warriorC, warriorB, settlersUnitB] as Set

    game.getPlayerDiscoveredCities(playerA) == [cityA, cityA1, cityB] as Set
    game.getPlayerDiscoveredCities(playerB) == [cityB, cityA, cityC] as Set
    game.getPlayerDiscoveredCities(playerC) == [cityC, cityB] as Set
    game.getPlayerVisibleCities(playerA) == [cityA, cityA1, cityB] as Set
    game.getPlayerVisibleCities(playerB) == [cityB, cityA, cityC] as Set
    game.getPlayerVisibleCities(playerC) == [cityC, cityB] as Set

    when:
    def gameSnapshot = game.snapshot()
    def restoredGame = new Game(gameSnapshot)
    assert restoredGame.getState() == Game.State.INITIALIZED
    restoredGame.state = Game.State.RUNNING

    then:
    toString(restoredGame) == toString(game)
  }

}
