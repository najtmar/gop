package cz.wie.najtmar.gop.board


import cz.wie.najtmar.gop.common.Point2D
import spock.lang.Specification

import javax.json.Json

class BoardSpec extends Specification {

    Board board;
    Joint joint0;
    Joint joint1;
    Joint joint2;
    RoadSection roadSection0;
    RoadSection roadSection1;

    void setup() throws Exception {
        final Properties simpleBoardPropertiesSet = new Properties();
        simpleBoardPropertiesSet.load(BoardSpec.class.getResourceAsStream(
                "/cz/wie/najtmar/gop/board/simple-board.properties"));
        board = new Board(simpleBoardPropertiesSet);
        joint0 = (new Joint.Builder())
                .setBoard(board)
                .setIndex(0)
                .setName("joint0")
                .setCoordinatesPair(new Point2D.Double(0.0, 0.0))
                .build();
        joint1 = (new Joint.Builder())
                .setBoard(board)
                .setIndex(1)
                .setName("joint1")
                .setCoordinatesPair(new Point2D.Double(3.0, 0.0))
                .build();
        joint2 = (new Joint.Builder())
                .setBoard(board)
                .setIndex(2)
                .setName("joint2")
                .setCoordinatesPair(new Point2D.Double(0.0, 3.0))
                .build();
        roadSection0 = (new RoadSection.Builder())
                .setBoard(board)
                .setIndex(0)
                .setJoints(joint0, joint1)
                .build();
        roadSection1 = (new RoadSection.Builder())
                .setBoard(board)
                .setIndex(1)
                .setJoints(joint0, joint2)
                .build();
    }

    def "should create board"() {
        when: "creating board"
        board.addJoint(joint0);
        board.addJoint(joint1);
        board.addJoint(joint2);
        board.addRoadSection(roadSection0);
        board.addRoadSection(roadSection1);

        then: "board created as expected"
        board.getJoints() == [joint0, joint1, joint2]
        board.getRoadSections() == [roadSection0, roadSection1]
    }

    def "creating board with out of order joints should fail"() {
        when: "creating board with out of order joints"
        board.addJoint(joint0);
        board.addJoint(joint2);

        then: "BoardException is thrown"
        def exception = thrown(BoardException)
        exception.getMessage() == "Joint with index 2 cannot be added at position 1"
    }

    def "creating board with out-of-order road sections should fail"() {
        when: "creating board with out-of-order road section"
        board.addRoadSection(roadSection1);

        then: "BoardException is thrown"
        def exception = thrown(BoardException)
        exception.getMessage() == "RoadSection with index 1 cannot be added at position 0"
    }

    def "should serialize and deserialize board"() {
        given: "a board"
        board.addJoint(joint0);
        board.addJoint(joint1);
        board.addJoint(joint2);
        board.addRoadSection(roadSection0);
        board.addRoadSection(roadSection1);

        when: "serializing the board"
        def boardSerialized = board.serialize();

        then: "the board should be serialized as expected"
        boardSerialized == Json.createReader(new StringReader(
                "{ "
                        + "  \"properties\": [ "
                        + "    { "
                        + "      \"key\": \"Board.xsize\", "
                        + "      \"value\":\"15\" "
                        + "    }, "
                        + "    { "
                        + "      \"key\": \"Board.stepSize\", "
                        + "      \"value\":\"1.0\" "
                        + "    }, "
                        + "    { "
                        + "      \"key\": \"Board.backgroundImage\", "
                        + "      \"value\": \"/images/gop-background.jpg\" "
                        + "    }, "
                        + "    { "
                        + "      \"key\": \"Board.ysize\", "
                        + "      \"value\":\"10\" "
                        + "    } "
                        + "  ], "
                        + "  \"joints\": [ "
                        + "    { "
                        + "      \"index\": 0, "
                        + "      \"name\": \"joint0\", "
                        + "      \"x\": 0.0, "
                        + "      \"y\": 0.0 "
                        + "    }, "
                        + "    { "
                        + "      \"index\": 1, "
                        + "      \"name\": \"joint1\", "
                        + "      \"x\": 3.0, "
                        + "      \"y\": 0.0 "
                        + "    }, "
                        + "    { "
                        + "      \"index\": 2, "
                        + "      \"name\": \"joint2\", "
                        + "      \"x\": 0.0, "
                        + "      \"y\": 3.0 "
                        + "    } "
                        + "  ], "
                        + "  \"roadSections\": [ "
                        + "    { "
                        + "      \"index\": 0, "
                        + "      \"joint0\": 0, "
                        + "      \"joint1\": 1 "
                        + "    }, "
                        + "    { "
                        + "      \"index\": 1, "
                        + "      \"joint0\": 0, "
                        + "      \"joint1\": 2 "
                        + "    } "
                        + "  ] "
                        + "}"))
                .readObject()

        when: "deserializing the board"
        def boardDeserialized = Board.deserialize(boardSerialized);

        then: "board deserialized as expected"
        boardDeserialized == board


/*
        boardDeserialized.getBoardPropertiesSet().getPropertiesSet() == board.getBoardPropertiesSet().getPropertiesSet()
        assertEquals(board.getJoints().size(), boardDeserialized.getJoints().size());
        for (int i = 0; i < board.getJoints().size(); ++i) {
            assertEquals("Index " + i, board.getJoints().get(i), boardDeserialized.getJoints().get(i));
            assertEquals("Index " + i, board.getJoints().get(i).getCoordinatesPair(),
                    boardDeserialized.getJoints().get(i).getCoordinatesPair());
        }
        assertEquals(board.getRoadSections().size(), boardDeserialized.getRoadSections().size());
        for (int i = 0; i < board.getRoadSections().size(); ++i) {
            assertEquals("Index " + i, board.getRoadSections().get(i), boardDeserialized.getRoadSections().get(i));
            assertEquals("Index " + i, board.getRoadSections().get(i).getAngle(),
                    boardDeserialized.getRoadSections().get(i).getAngle(), 0.0);
        }
*/
    }

}
