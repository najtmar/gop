package cz.wie.najtmar.gop.board

import com.google.inject.AbstractModule
import com.google.inject.Guice
import com.google.inject.Injector
import com.google.inject.Module
import cz.wie.najtmar.gop.game.PropertiesReader
import cz.wie.najtmar.gop.game.SimplePropertiesReader
import spock.lang.Specification

import javax.json.Json

class SimpleBoardGeneratorSpec extends Specification {

    SimpleBoardGenerator generator;

    def setup() {
        final Injector injector = Guice.createInjector(getTestModule());
        generator = injector.getInstance(SimpleBoardGenerator.class);
    }

    def "should generate board"() {
        when: "board is generated"
        def board = generator.generate();

        then: "the board is generated as expected"
        board.serialize() == Json.createReader(new StringReader(
                "{ "
                        + "  \"properties\":[ "
                        + "    {\"key\":\"Board.xsize\",\"value\":\"15\"}, "
                        + "    {\"key\":\"Board.stepSize\",\"value\":\"1.0\"}, "
                        + "    {\"key\":\"Board.backgroundImage\",\"value\":\"/images/gop-background.jpg\"},"
                        + "    {\"key\":\"Board.ysize\",\"value\":\"10\"} "
                        + "  ], "
                        + "  \"joints\":[ "
                        + "    {\"index\":0,\"name\":\"joint0\",\"x\":5.0,\"y\":2.0}, "
                        + "    {\"index\":1,\"name\":\"joint1\",\"x\":10.0,\"y\":2.0}, "
                        + "    {\"index\":2,\"name\":\"joint2\",\"x\":10.0,\"y\":8.0}, "
                        + "    {\"index\":3,\"name\":\"joint3\",\"x\":5.0,\"y\":8.0} "
                        + "  ], "
                        + "  \"roadSections\":[ "
                        + "    {\"index\":0,\"joint0\":0,\"joint1\":1}, "
                        + "    {\"index\":1,\"joint0\":2,\"joint1\":1}, "
                        + "    {\"index\":2,\"joint0\":3,\"joint1\":2}, "
                        + "    {\"index\":3,\"joint0\":3,\"joint1\":0}, "
                        + "    {\"index\":4,\"joint0\":2,\"joint1\":0} "
                        + "  ] "
                        + "} ")).readObject()
    }

    private Module getTestModule() {
        return new AbstractModule() {
            @Override protected void configure() {
                bind(PropertiesReader.class).to(SimplePropertiesReader.class);
            }
        };
    }
}
