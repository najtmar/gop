package cz.wie.najtmar.gop.game;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import cz.wie.najtmar.gop.board.Board;
import cz.wie.najtmar.gop.board.BoardException;
import cz.wie.najtmar.gop.board.DirectedPosition;
import cz.wie.najtmar.gop.board.Joint;
import cz.wie.najtmar.gop.board.Position;
import cz.wie.najtmar.gop.board.RoadSection;
import cz.wie.najtmar.gop.common.Constants;
import cz.wie.najtmar.gop.common.Point2D;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Properties;
import java.util.UUID;

/**
 * Tests for class Warrior.
 * @author najtmar
 */
public class WarriorTest {

  static final long CREATION_TIME = 476;
  static final Game DUMMY_GAME = mock(Game.class);
  static final Player PLAYER_0 = new Player(new User("playerA", new UUID(12345, 67890)), 0, DUMMY_GAME);
  static final Player PLAYER_1 = new Player(new User("playerB", new UUID(98765, 43210)), 1, DUMMY_GAME);
  static final double BASIC_VELOCITY = 1.25;
  static final double VELOCITY_DIR_CONTRIB_FACTOR = 1.75;
  static final double MAX_ATTACK_STRENGTH = 3.0;

  static Board BOARD;
  static Joint JOINT_0_0;
  static Joint JOINT_3_0;
  static Joint JOINT_3_3;
  static RoadSection ROAD_SECTION_0;
  static RoadSection ROAD_SECTION_1;
  static Position POSITION_0;
  static Position POSITION_1;

  GameProperties mockedGameProperties;
  Warrior warrior0;
  Warrior warrior1;
  Warrior warrior2;

  /**
   * Executed before the class is initialized.
   * @throws Exception when tests cannot be initialized
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    final Properties simpleBoardPropertiesSet = new Properties();
    simpleBoardPropertiesSet.load(WarriorTest.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/board/simple-board.properties"));
    BOARD = new Board(simpleBoardPropertiesSet);
    JOINT_0_0 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setName("JOINT_0_0")
        .setCoordinatesPair(new Point2D.Double(0.0, 0.0))
        .build();
    JOINT_3_0 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(1)
        .setName("JOINT_3_0")
        .setCoordinatesPair(new Point2D.Double(3.0, 0.0))
        .build();
    JOINT_3_3 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(2)
        .setName("JOINT_3_#")
        .setCoordinatesPair(new Point2D.Double(3.0, 3.0))
        .build();
    ROAD_SECTION_0 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(JOINT_0_0, JOINT_3_0)
        .build();
    ROAD_SECTION_1 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(1)
        .setJoints(JOINT_3_0, JOINT_3_3)
        .build();
    POSITION_0 = new Position(ROAD_SECTION_0, 0);
    POSITION_1 = new Position(ROAD_SECTION_0, 3);
  }

  /**
   * Sets up test.
   * @throws Exception when test cannot be set up
   */
  @Before
  public void setUp() throws Exception {
    mockedGameProperties = mock(GameProperties.class);
    Warrior.resetFirstAvailableId();
    warrior0 = (new Warrior.Builder())
        .setCreationTime(CREATION_TIME)
        .setCurrentPosition(POSITION_0)
        .setDirection(0.0)
        .setGameProperties(mockedGameProperties)
        .setPlayer(PLAYER_0)
        .build();
    warrior1 = (new Warrior.Builder())
        .setCreationTime(CREATION_TIME)
        .setCurrentPosition(POSITION_0)
        .setDirection(2.0 * Math.PI - 0.001)
        .setGameProperties(mockedGameProperties)
        .setPlayer(PLAYER_1)
        .build();
    warrior2 = (new Warrior.Builder())
        .setCreationTime(CREATION_TIME)
        .setCurrentPosition(POSITION_1)
        .setDirection(3.0 * Math.PI / 4.0)
        .setGameProperties(mockedGameProperties)
        .setPlayer(PLAYER_0)
        .build();
  }

  @Test
  public void testWarriorCreated() {
    assertEquals(POSITION_0, warrior0.getCurrentPosition());
    assertEquals(0.0, warrior0.getWarriorDirection(), 0.0);
    assertEquals(Prawn.MAX_ENERGY, warrior0.getEnergy(), 0.0);
    assertEquals(mockedGameProperties, warrior0.getGameProperties());
    assertEquals(0, warrior0.getId());
    assertEquals(PLAYER_0, warrior0.getPlayer());
    assertEquals(CREATION_TIME, warrior0.getLastMoveTime());
    assertEquals(POSITION_0, warrior1.getCurrentPosition());
    assertEquals(2.0 * Math.PI - 0.001, warrior1.getWarriorDirection(), Constants.DOUBLE_DELTA);
    assertEquals(Prawn.MAX_ENERGY, warrior1.getEnergy(), 0.0);
    assertEquals(mockedGameProperties, warrior1.getGameProperties());
    assertEquals(1, warrior1.getId());
    assertEquals(PLAYER_1, warrior1.getPlayer());
    assertEquals(CREATION_TIME, warrior1.getLastMoveTime());
  }

  @Test
  public void testWarriorVelocity() throws BoardException, GameException {
    when(mockedGameProperties.getBasicVelocity()).thenReturn(BASIC_VELOCITY);
    when(mockedGameProperties.getVelocityDirContribFactor()).thenReturn(VELOCITY_DIR_CONTRIB_FACTOR);
    assertEquals(BASIC_VELOCITY + VELOCITY_DIR_CONTRIB_FACTOR * 1.0,
        warrior0.calculateVelocity(new DirectedPosition(ROAD_SECTION_0, 0, RoadSection.Direction.FORWARD)),
        Constants.DOUBLE_DELTA);
    assertEquals(BASIC_VELOCITY + 0.0,
        warrior0.calculateVelocity(new DirectedPosition(ROAD_SECTION_0, 3, RoadSection.Direction.BACKWARD)),
            Constants.DOUBLE_DELTA);
    verify(mockedGameProperties, times(2)).getBasicVelocity();
    verify(mockedGameProperties, times(2)).getVelocityDirContribFactor();
  }

  @Test
  public void testWarriorAttackStrength() throws BoardException, GameException {
    when(mockedGameProperties.getMaxAttackStrength()).thenReturn(MAX_ATTACK_STRENGTH);
    assertEquals(0.0, warrior2.calculateAttackStrength(new DirectedPosition(new Position(ROAD_SECTION_0, 3),
        RoadSection.Direction.FORWARD)), 0.0);
    assertEquals(MAX_ATTACK_STRENGTH * Math.sqrt(2.0) / 2.0,
        warrior2.calculateAttackStrength(new DirectedPosition(new Position(ROAD_SECTION_0, 3),
        RoadSection.Direction.BACKWARD)), Constants.DOUBLE_DELTA);
    assertEquals(MAX_ATTACK_STRENGTH * Math.sqrt(2.0) / 2.0,
        warrior2.calculateAttackStrength(new DirectedPosition(new Position(ROAD_SECTION_1, 0),
        RoadSection.Direction.FORWARD)), Constants.DOUBLE_DELTA);
    assertEquals(0.0, warrior2.calculateAttackStrength(new DirectedPosition(new Position(ROAD_SECTION_1, 0),
        RoadSection.Direction.BACKWARD)), 0.0);
    verify(mockedGameProperties, times(4)).getMaxAttackStrength();
  }

}
