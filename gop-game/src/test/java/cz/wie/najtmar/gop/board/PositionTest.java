package cz.wie.najtmar.gop.board;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import cz.wie.najtmar.gop.common.Point2D;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;

public class PositionTest {

  static Board BOARD;
  static Joint JOINT_0_0;
  static Joint JOINT_3_0;
  static Joint JOINT_3_3;
  static RoadSection ROAD_SECTION_0;
  static RoadSection ROAD_SECTION_1;
  static RoadSection ROAD_SECTION_2;

  Position position0;
  Position position1;
  Position position11;
  Position position2;

  /**
   * Executed before the class is initialized.
   * @throws Exception when tests cannot be initialized
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    final Properties simpleBoardPropertiesSet = new Properties();
    simpleBoardPropertiesSet.load(RoadSectionTest.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/board/simple-board.properties"));
    BOARD = new Board(simpleBoardPropertiesSet);
    JOINT_0_0 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setName("JOINT_0_0")
        .setCoordinatesPair(new Point2D.Double(0.0, 0.0))
        .build();
    JOINT_3_0 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(1)
        .setName("JOINT_3_0")
        .setCoordinatesPair(new Point2D.Double(3.0, 0.0))
        .build();
    JOINT_3_3 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(2)
        .setName("JOINT_3_3")
        .setCoordinatesPair(new Point2D.Double(3.0, 3.0))
        .build();
    ROAD_SECTION_0 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(JOINT_0_0, JOINT_3_0)
        .build();
    ROAD_SECTION_1 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(1)
        .setJoints(JOINT_3_0, JOINT_3_3)
        .build();
    ROAD_SECTION_2 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(2)
        .setJoints(JOINT_0_0, JOINT_3_3)
        .build();
  }

  /**
   * @throws java.lang.Exception when test cannot be set up
   */
  @Before
  public void setUp() throws Exception {
    position0 = new Position(ROAD_SECTION_0, 0);
    position1 = new Position(ROAD_SECTION_0, 1);
    position11 = new Position(ROAD_SECTION_0, 1);
    position2 = new Position(ROAD_SECTION_0, 3);
  }

  @Test
  public void testPositionCreationSucceeds() throws BoardException {
    assertEquals(0, position0.getIndex());
    assertEquals(ROAD_SECTION_0, position0.getRoadSection());
    assertEquals(3, position2.getIndex());
    assertEquals(930, position0.hashCode());
    assertEquals(962, position1.hashCode());
    assertEquals(position11.hashCode(), position1.hashCode());
    assertEquals(899, position2.hashCode());
  }

  @Test
  public void testPositionOutOfBoundFails() {
    try {
      new Position(ROAD_SECTION_0, -1);
      fail("Should have thrown an exception.");
    } catch (BoardException exception) {
      assertEquals("index must be between 0 and 3 (found -1).", exception.getMessage());
    }
    try {
      new Position(ROAD_SECTION_0, 4);
      fail("Should have thrown an exception.");
    } catch (BoardException exception) {
      assertEquals("index must be between 0 and 3 (found 4).", exception.getMessage());
    }
  }

  @Test
  public void testGetJointWorks() {
    assertEquals(JOINT_0_0, position0.getJoint());
    assertEquals(JOINT_3_0, position2.getJoint());
    assertEquals(null, position1.getJoint());
  }

  @Test
  public void testComparisonWorks() throws BoardException {
    assertTrue(position1.equals(position1));
    assertTrue(position1.equals(position11));
    assertFalse(position1.equals(position0));
    assertFalse(position1.equals(JOINT_0_0));
  }

  @Test
  public void testComparisonOnJointWorks() throws BoardException {
    Position position3 = new Position(ROAD_SECTION_1, 0);
    assertTrue(position2.equals(position3));
    assertEquals(899, position3.hashCode());
    assertEquals(position2.hashCode(), position3.hashCode());
  }

  @Test
  public void testAdjacencyCheckingWorks() throws BoardException {
    // Same RoadSection.
    assertTrue((new Position(ROAD_SECTION_2, 1)).adjacentTo(new Position(ROAD_SECTION_2, 2)));
    assertTrue((new Position(ROAD_SECTION_2, 2)).adjacentTo(new Position(ROAD_SECTION_2, 1)));
    assertFalse((new Position(ROAD_SECTION_2, 1)).adjacentTo(new Position(ROAD_SECTION_2, 1)));
    assertFalse((new Position(ROAD_SECTION_2, 1)).adjacentTo(new Position(ROAD_SECTION_2, 3)));
    assertFalse((new Position(ROAD_SECTION_2, 3)).adjacentTo(new Position(ROAD_SECTION_2, 1)));
    assertTrue((new Position(ROAD_SECTION_2, 0)).adjacentTo(new Position(ROAD_SECTION_2, 1)));
    assertTrue((new Position(ROAD_SECTION_2, 1)).adjacentTo(new Position(ROAD_SECTION_2, 0)));
    assertFalse((new Position(ROAD_SECTION_2, 0)).adjacentTo(new Position(ROAD_SECTION_2, 2)));
    assertFalse((new Position(ROAD_SECTION_2, 2)).adjacentTo(new Position(ROAD_SECTION_2, 0)));
    assertTrue((new Position(ROAD_SECTION_2, 3)).adjacentTo(new Position(ROAD_SECTION_2, 4)));
    assertTrue((new Position(ROAD_SECTION_2, 4)).adjacentTo(new Position(ROAD_SECTION_2, 3)));
    assertFalse((new Position(ROAD_SECTION_2, 2)).adjacentTo(new Position(ROAD_SECTION_2, 4)));
    assertFalse((new Position(ROAD_SECTION_2, 4)).adjacentTo(new Position(ROAD_SECTION_2, 2)));

    // Different RoadSections.
    assertFalse((new Position(ROAD_SECTION_2, 1)).adjacentTo(new Position(ROAD_SECTION_0, 1)));
    assertTrue((new Position(ROAD_SECTION_2, 0)).adjacentTo(new Position(ROAD_SECTION_0, 1)));
    assertTrue((new Position(ROAD_SECTION_2, 1)).adjacentTo(new Position(ROAD_SECTION_0, 0)));
    assertFalse((new Position(ROAD_SECTION_2, 0)).adjacentTo(new Position(ROAD_SECTION_0, 0)));
    assertFalse((new Position(ROAD_SECTION_2, 0)).adjacentTo(new Position(ROAD_SECTION_0, 3)));
    assertFalse((new Position(ROAD_SECTION_0, 3)).adjacentTo(new Position(ROAD_SECTION_2, 0)));
    assertTrue((new Position(ROAD_SECTION_0, 2)).adjacentTo(new Position(ROAD_SECTION_1, 0)));
    assertTrue((new Position(ROAD_SECTION_1, 0)).adjacentTo(new Position(ROAD_SECTION_0, 2)));
    assertTrue((new Position(ROAD_SECTION_0, 3)).adjacentTo(new Position(ROAD_SECTION_1, 1)));
    assertTrue((new Position(ROAD_SECTION_1, 1)).adjacentTo(new Position(ROAD_SECTION_0, 3)));
    assertTrue((new Position(ROAD_SECTION_2, 3)).adjacentTo(new Position(ROAD_SECTION_1, 3)));
    assertTrue((new Position(ROAD_SECTION_1, 3)).adjacentTo(new Position(ROAD_SECTION_2, 3)));
    assertTrue((new Position(ROAD_SECTION_2, 4)).adjacentTo(new Position(ROAD_SECTION_1, 2)));
    assertTrue((new Position(ROAD_SECTION_1, 2)).adjacentTo(new Position(ROAD_SECTION_2, 4)));
  }

  @Test
  public void testRoadSectionHalvesMatchingWorks() throws Exception {
    assertEquals(new HashSet<>(Arrays.asList(ROAD_SECTION_0.getHalves()[0], ROAD_SECTION_2.getHalves()[0])),
        (new Position(ROAD_SECTION_0, 0)).findRoadSectionHalves());
    assertEquals(new HashSet<>(Arrays.asList(ROAD_SECTION_0.getHalves()[0], ROAD_SECTION_2.getHalves()[0])),
        (new Position(ROAD_SECTION_2, 0)).findRoadSectionHalves());
    assertEquals(new HashSet<>(Arrays.asList(ROAD_SECTION_0.getHalves()[1], ROAD_SECTION_1.getHalves()[0])),
        (new Position(ROAD_SECTION_0, 3)).findRoadSectionHalves());
    assertEquals(new HashSet<>(Arrays.asList(ROAD_SECTION_0.getHalves()[1], ROAD_SECTION_1.getHalves()[0])),
        (new Position(ROAD_SECTION_1, 0)).findRoadSectionHalves());
    assertEquals(new HashSet<>(Arrays.asList(ROAD_SECTION_0.getHalves()[0])),
        (new Position(ROAD_SECTION_0, 1)).findRoadSectionHalves());
    assertEquals(new HashSet<>(Arrays.asList(ROAD_SECTION_0.getHalves()[0], ROAD_SECTION_0.getHalves()[1])),
        (new Position(ROAD_SECTION_0, 2)).findRoadSectionHalves());
  }

  @Test
  public void testVisibleRoadSectionHalvesCheckingWorks() throws Exception {
    assertEquals(new HashSet<>(Arrays.asList(ROAD_SECTION_0.getHalves()[0], ROAD_SECTION_0.getHalves()[1],
        ROAD_SECTION_2.getHalves()[0], ROAD_SECTION_2.getHalves()[1])),
        (new Position(ROAD_SECTION_0, 0).findVisibleRoadSectionHalves()));
    assertEquals(new HashSet<>(Arrays.asList(ROAD_SECTION_0.getHalves()[0], ROAD_SECTION_0.getHalves()[1],
        ROAD_SECTION_2.getHalves()[0], ROAD_SECTION_2.getHalves()[1])),
        (new Position(ROAD_SECTION_2, 0).findVisibleRoadSectionHalves()));
    assertEquals(new HashSet<>(Arrays.asList(ROAD_SECTION_0.getHalves()[0], ROAD_SECTION_0.getHalves()[1],
        ROAD_SECTION_2.getHalves()[0])), (new Position(ROAD_SECTION_0, 1).findVisibleRoadSectionHalves()));
    assertEquals(new HashSet<>(Arrays.asList(ROAD_SECTION_0.getHalves()[0], ROAD_SECTION_0.getHalves()[1],
        ROAD_SECTION_2.getHalves()[0], ROAD_SECTION_1.getHalves()[0])),
        (new Position(ROAD_SECTION_0, 2).findVisibleRoadSectionHalves()));
    assertEquals(new HashSet<>(Arrays.asList(ROAD_SECTION_0.getHalves()[0], ROAD_SECTION_0.getHalves()[1],
        ROAD_SECTION_1.getHalves()[0], ROAD_SECTION_1.getHalves()[1])),
        (new Position(ROAD_SECTION_0, 3).findVisibleRoadSectionHalves()));
    assertEquals(new HashSet<>(Arrays.asList(ROAD_SECTION_0.getHalves()[0], ROAD_SECTION_0.getHalves()[1],
        ROAD_SECTION_1.getHalves()[0], ROAD_SECTION_1.getHalves()[1])),
        (new Position(ROAD_SECTION_1, 0).findVisibleRoadSectionHalves()));
  }

  @Test
  public void testPositionDoesNotKeepTransitoryState() throws Exception {
    final Properties simpleBoardPropertiesSet = new Properties();
    simpleBoardPropertiesSet.load(RoadSectionTest.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/board/simple-board.properties"));
    final Board board = new Board(simpleBoardPropertiesSet);
    final Joint joint00 = (new Joint.Builder())
        .setBoard(board)
        .setIndex(0)
        .setName("joint00")
        .setCoordinatesPair(new Point2D.Double(0.0, 0.0))
        .build();
    final Joint joint30 = (new Joint.Builder())
        .setBoard(board)
        .setIndex(1)
        .setName("joint30")
        .setCoordinatesPair(new Point2D.Double(3.0, 0.0))
        .build();
    final Joint joint33 = (new Joint.Builder())
        .setBoard(board)
        .setIndex(2)
        .setName("joint33")
        .setCoordinatesPair(new Point2D.Double(3.0, 3.0))
        .build();
    final RoadSection roadSection0 = (new RoadSection.Builder())
        .setBoard(board)
        .setIndex(0)
        .setJoints(joint00, joint30)
        .build();
    final Position joint30Position = joint30.getNormalizedPosition();
    final RoadSection roadSection1 = (new RoadSection.Builder())
        .setBoard(board)
        .setIndex(1)
        .setJoints(joint30, joint33)
        .build();
    assertEquals(new HashSet<>(Arrays.asList(roadSection0.getHalves()[1], roadSection1.getHalves()[0])),
        joint30Position.findRoadSectionHalves());
  }

  @Test
  public void testCommonRoadSectionCorrectlyFound() throws Exception {
    assertEquals(ROAD_SECTION_0,
        Position.findCommonRoadSection(new Position(ROAD_SECTION_0, 1), new Position(ROAD_SECTION_0, 2)));
    assertEquals(ROAD_SECTION_0,
        Position.findCommonRoadSection(new Position(ROAD_SECTION_0, 0), new Position(ROAD_SECTION_0, 2)));
    assertEquals(ROAD_SECTION_0,
        Position.findCommonRoadSection(new Position(ROAD_SECTION_0, 1), new Position(ROAD_SECTION_0, 3)));
    assertEquals(ROAD_SECTION_0,
        Position.findCommonRoadSection(new Position(ROAD_SECTION_0, 1), new Position(ROAD_SECTION_1, 0)));
    assertEquals(ROAD_SECTION_0,
        Position.findCommonRoadSection(new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_0, 2)));
    assertEquals(ROAD_SECTION_0,
        Position.findCommonRoadSection(new Position(ROAD_SECTION_2, 0), new Position(ROAD_SECTION_1, 0)));
    assertEquals(ROAD_SECTION_1,
        Position.findCommonRoadSection(new Position(ROAD_SECTION_0, 3), new Position(ROAD_SECTION_2, 4)));
  }

}
