package cz.wie.najtmar.gop.game;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import cz.wie.najtmar.gop.board.Board;
import cz.wie.najtmar.gop.board.BoardException;
import cz.wie.najtmar.gop.board.DirectedPosition;
import cz.wie.najtmar.gop.board.Joint;
import cz.wie.najtmar.gop.board.Position;
import cz.wie.najtmar.gop.board.RoadSection;
import cz.wie.najtmar.gop.common.Point2D;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Properties;
import java.util.UUID;

/**
 * Tests for class Warrior.
 * @author najtmar
 */
public class SettlersUnitTest {

  static final long CREATION_TIME = 476;
  static final Game DUMMY_GAME = mock(Game.class);
  static final Player PLAYER_0 = new Player(new User("playerA", new UUID(12345, 67890)), 0, DUMMY_GAME);
  static final Player PLAYER_1 = new Player(new User("playerB", new UUID(98765, 43210)), 1, DUMMY_GAME);
  static final double BASIC_VELOCITY = 1.25;

  static Board BOARD;
  static Joint JOINT_0_0;
  static Joint JOINT_3_0;
  static Joint JOINT_3_3;
  static RoadSection ROAD_SECTION_0;
  static RoadSection ROAD_SECTION_1;
  static Position POSITION_0;
  static Position POSITION_1;

  GameProperties mockedGameProperties;
  SettlersUnit settlersUnit0;
  SettlersUnit settlersUnit1;
  SettlersUnit settlersUnit2;

  /**
   * Executed before the class is initialized.
   * @throws Exception when tests cannot be initialized
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    final Properties simpleBoardPropertiesSet = new Properties();
    simpleBoardPropertiesSet.load(SettlersUnitTest.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/board/simple-board.properties"));
    BOARD = new Board(simpleBoardPropertiesSet);
    JOINT_0_0 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setName("JOINT_0_0")
        .setCoordinatesPair(new Point2D.Double(0.0, 0.0))
        .build();
    JOINT_3_0 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(1)
        .setName("JOINT_3_0")
        .setCoordinatesPair(new Point2D.Double(3.0, 0.0))
        .build();
    JOINT_3_3 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(2)
        .setName("JOINT_3_3")
        .setCoordinatesPair(new Point2D.Double(3.0, 3.0))
        .build();
    ROAD_SECTION_0 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(JOINT_0_0, JOINT_3_0)
        .build();
    ROAD_SECTION_1 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(1)
        .setJoints(JOINT_3_0, JOINT_3_3)
        .build();
    POSITION_0 = new Position(ROAD_SECTION_0, 0);
    POSITION_1 = new Position(ROAD_SECTION_0, 3);
  }

  /**
   * Sets up test.
   * @throws Exception when test cannot be set up
   */
  @Before
  public void setUp() throws Exception {
    mockedGameProperties = mock(GameProperties.class);
    SettlersUnit.resetFirstAvailableId();
    settlersUnit0 = (new SettlersUnit.Builder())
        .setCreationTime(CREATION_TIME)
        .setCurrentPosition(POSITION_0)
        .setGameProperties(mockedGameProperties)
        .setPlayer(PLAYER_0)
        .build();
    settlersUnit1 = (new SettlersUnit.Builder())
        .setCreationTime(CREATION_TIME)
        .setCurrentPosition(POSITION_0)
        .setGameProperties(mockedGameProperties)
        .setPlayer(PLAYER_1)
        .build();
    settlersUnit2 = (new SettlersUnit.Builder())
        .setCreationTime(CREATION_TIME)
        .setCurrentPosition(POSITION_1)
        .setGameProperties(mockedGameProperties)
        .setPlayer(PLAYER_0)
        .build();
  }

  @Test
  public void testSettlersUnitsCreated() {
    assertEquals(POSITION_0, settlersUnit0.getCurrentPosition());
    assertEquals(Prawn.MAX_ENERGY, settlersUnit0.getEnergy(), 0.0);
    assertEquals(mockedGameProperties, settlersUnit0.getGameProperties());
    assertEquals(0, settlersUnit0.getId());
    assertEquals(PLAYER_0, settlersUnit0.getPlayer());
    assertEquals(CREATION_TIME, settlersUnit0.getLastMoveTime());
    assertEquals(POSITION_0, settlersUnit1.getCurrentPosition());
    assertEquals(Prawn.MAX_ENERGY, settlersUnit1.getEnergy(), 0.0);
    assertEquals(mockedGameProperties, settlersUnit1.getGameProperties());
    assertEquals(1, settlersUnit1.getId());
    assertEquals(PLAYER_1, settlersUnit1.getPlayer());
    assertEquals(CREATION_TIME, settlersUnit1.getLastMoveTime());
  }

  @Test
  public void testSettlersUnitVelocity() throws BoardException, GameException {
    when(mockedGameProperties.getBasicVelocity()).thenReturn(BASIC_VELOCITY);
    assertEquals(BASIC_VELOCITY,
        settlersUnit0.calculateVelocity(new DirectedPosition(ROAD_SECTION_0, 0, RoadSection.Direction.FORWARD)), 0.0);
    assertEquals(BASIC_VELOCITY,
        settlersUnit0.calculateVelocity(new DirectedPosition(ROAD_SECTION_0, 0, RoadSection.Direction.FORWARD)), 0.0);
    verify(mockedGameProperties, times(2)).getBasicVelocity();
  }

  @Test
  public void testSettlersUnitAttackStrength() throws BoardException, GameException {
    assertEquals(0.0, settlersUnit2.calculateAttackStrength(new DirectedPosition(new Position(ROAD_SECTION_0, 3),
        RoadSection.Direction.FORWARD)), 0.0);
    assertEquals(0.0, settlersUnit2.calculateAttackStrength(new DirectedPosition(new Position(ROAD_SECTION_0, 3),
        RoadSection.Direction.BACKWARD)), 0.0);
    assertEquals(0.0, settlersUnit2.calculateAttackStrength(new DirectedPosition(new Position(ROAD_SECTION_1, 0),
        RoadSection.Direction.FORWARD)), 0.0);
    assertEquals(0.0, settlersUnit2.calculateAttackStrength(new DirectedPosition(new Position(ROAD_SECTION_1, 0),
        RoadSection.Direction.BACKWARD)), 0.0);
    verify(mockedGameProperties, times(0)).getMaxAttackStrength();
  }

}
