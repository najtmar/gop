package cz.wie.najtmar.gop.game;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyDouble;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import cz.wie.najtmar.gop.board.Board;
import cz.wie.najtmar.gop.board.Joint;
import cz.wie.najtmar.gop.board.RoadSection;
import cz.wie.najtmar.gop.common.Point2D;
import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.game.CityFactory.Action;
import cz.wie.najtmar.gop.game.CityFactory.ActionState;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import javax.json.Json;

public class CityProductionEventGeneratingExecutorTest {

  static final long START_TIME = 1000;
  static final double MAX_PRODUCTION_DELTA = 0.125;
  static Event TEST_EVENT;

  Board board;
  Joint joint00;
  Joint joint30;
  Joint joint33;
  Joint joint60;
  Joint joint63;
  RoadSection roadSection0;
  RoadSection roadSection1;
  RoadSection roadSection2;
  RoadSection roadSection3;

  Game game;
  EventFactory mockedEventFactory;
  Event dummyEvent;
  Player playerA;
  Player playerB;
  City cityA;
  City cityB;
  CityFactory factory0;
  CityFactory factory1;
  CityFactory factory2;
  Action action0;
  Action action1;
  Action action2;
  CityProductionEventGeneratingExecutor executor;

  /**
   * Method setUpBeforeClass() .
   * @throws Exception when the method execution fails
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    TEST_EVENT = new Event(Json.createObjectBuilder()
        .add("type", "TEST")
        .add("time", START_TIME)
        .add("whatever", "value")
        .build());
  }

  /**
   * Method setUp().
   * @throws Exception when method execution fails
   */
  @Before
  public void setUp() throws Exception {
    final Properties simpleBoardPropertiesSet = new Properties();
    simpleBoardPropertiesSet.load(CityProductionEventGeneratingExecutorTest.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/board/simple-board.properties"));
    board = new Board(simpleBoardPropertiesSet);
    joint00 = (new Joint.Builder())
        .setBoard(board)
        .setIndex(0)
        .setName("joint00")
        .setCoordinatesPair(new Point2D.Double(0.0, 0.0))
        .build();
    joint30 = (new Joint.Builder())
        .setBoard(board)
        .setIndex(1)
        .setName("joint30")
        .setCoordinatesPair(new Point2D.Double(3.0, 0.0))
        .build();
    joint33 = (new Joint.Builder())
        .setBoard(board)
        .setIndex(2)
        .setName("joint33")
        .setCoordinatesPair(new Point2D.Double(3.0, 3.0))
        .build();
    joint60 = (new Joint.Builder())
        .setBoard(board)
        .setIndex(3)
        .setName("joint60")
        .setCoordinatesPair(new Point2D.Double(6.0, 0.0))
        .build();
    joint63 = (new Joint.Builder())
        .setBoard(board)
        .setIndex(4)
        .setName("joint63")
        .setCoordinatesPair(new Point2D.Double(6.0, 3.0))
        .build();
    roadSection0 = (new RoadSection.Builder())
        .setBoard(board)
        .setIndex(0)
        .setJoints(joint00, joint30)
        .build();
    roadSection1 = (new RoadSection.Builder())
        .setBoard(board)
        .setIndex(1)
        .setJoints(joint30, joint33)
        .build();
    roadSection2 = (new RoadSection.Builder())
        .setBoard(board)
        .setIndex(2)
        .setJoints(joint30, joint60)
        .build();
    roadSection3 = (new RoadSection.Builder())
        .setBoard(board)
        .setIndex(3)
        .setJoints(joint30, joint63)
        .build();

    final Properties simpleGamePropertiesSet = new Properties();
    simpleGamePropertiesSet.load(CityProductionEventGeneratingExecutorTest.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/game/simple-game.properties"));
    game = new Game(simpleGamePropertiesSet);
    playerA = new Player(new User("playerA", new UUID(12345, 67890)), 0, game);
    playerB = new Player(new User("playerB", new UUID(98765, 43210)), 1, game);
    game.addPlayer(playerA);
    game.addPlayer(playerB);
    mockedEventFactory = mock(EventFactory.class);
    dummyEvent = mock(Event.class);

    City.resetFirstAvailableId();
    cityA = (new City.Builder())
        .setGame(game)
        .setName("cityA")
        .setPlayer(playerA)
        .setJoint(joint30)
        .build();
    cityA.grow();
    cityA.grow();
    cityB = (new City.Builder())
        .setGame(game)
        .setName("cityB")
        .setPlayer(playerB)
        .setJoint(joint60)
        .build();
    factory0 = cityA.getFactory(0);
    factory1 = cityA.getFactory(1);
    factory2 = cityA.getFactory(2);
    action0 = null;
    action1 = mock(Action.class);
    action2 = mock(Action.class);
    factory0.setAction(action0);
    factory1.setAction(action1);
    factory2.setAction(action2);
    executor = (new CityProductionEventGeneratingExecutor.Factory(
        game, mockedEventFactory)).create();
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testProductionRoundExecuted() throws Exception {
    when(action1.getState()).thenReturn(ActionState.FINISHED);
    when(action2.getState()).thenReturn(ActionState.INITIALIZED);
    when(action1.revise(any(EventFactory.class), any(List.class))).thenReturn(true);
    when(action2.revise(any(EventFactory.class), any(List.class))).thenReturn(true);
    when(mockedEventFactory.createIncreaseProgressByUniversalDeltaEvent(
        factory2, (4.0 - 0.5) / 4.0 * MAX_PRODUCTION_DELTA)).thenReturn(dummyEvent);

    game.advanceTimeTo(START_TIME + 10);
    executor.prepare(new LinkedList<>(Arrays.asList(TEST_EVENT)));
    executor.executeAndGenerate();

    assertEquals(Arrays.asList(TEST_EVENT, dummyEvent), executor.getEventsGenerated());
    verify(action1, times(1)).getState();
    verify(action2, times(1)).getState();
    verify(action1, times(0)).increaseProgressByUniversalDelta(anyDouble(), any(EventFactory.class), any(List.class));
    verify(action2, times(1)).increaseProgressByUniversalDelta((4.0 - 0.5) / 4.0 * MAX_PRODUCTION_DELTA,
        mockedEventFactory, executor.getEventsGenerated());
    verify(action1, times(0)).revise(any(EventFactory.class), any(List.class));
    verify(action2, times(1)).revise(mockedEventFactory, executor.getEventsGenerated());
    verify(mockedEventFactory, times(1)).createIncreaseProgressByUniversalDeltaEvent(
        factory2, (4.0 - 0.5) / 4.0 * MAX_PRODUCTION_DELTA);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testActionRevisionNegative() throws Exception {
    when(action1.getState()).thenReturn(ActionState.INITIALIZED);
    when(action2.getState()).thenReturn(ActionState.INITIALIZED);
    when(action1.revise(any(EventFactory.class), any(List.class))).thenReturn(true);
    when(action2.revise(any(EventFactory.class), any(List.class))).thenReturn(false);
    when(mockedEventFactory.createIncreaseProgressByUniversalDeltaEvent(
        factory1, (4.0 - 0.5) / 4.0 * MAX_PRODUCTION_DELTA)).thenReturn(dummyEvent);
    when(mockedEventFactory.createIncreaseProgressByUniversalDeltaEvent(
        factory2, (4.0 - 0.5) / 4.0 * MAX_PRODUCTION_DELTA)).thenReturn(dummyEvent);

    game.advanceTimeTo(START_TIME + 10);
    executor.prepare(new LinkedList<>(Arrays.asList(TEST_EVENT)));
    executor.executeAndGenerate();

    assertEquals(Arrays.asList(TEST_EVENT, dummyEvent), executor.getEventsGenerated());
    verify(action1, times(1)).getState();
    verify(action2, times(1)).getState();
    verify(action1, times(1)).increaseProgressByUniversalDelta((4.0 - 0.5) / 4.0 * MAX_PRODUCTION_DELTA,
        mockedEventFactory, executor.getEventsGenerated());
    verify(action2, times(0)).increaseProgressByUniversalDelta(anyDouble(), any(EventFactory.class), any(List.class));
    verify(action1, times(1)).revise(mockedEventFactory, executor.getEventsGenerated());
    verify(action2, times(1)).revise(mockedEventFactory, executor.getEventsGenerated());
    verify(mockedEventFactory, times(1)).createIncreaseProgressByUniversalDeltaEvent(
        factory1, (4.0 - 0.5) / 4.0 * MAX_PRODUCTION_DELTA);
  }

}
