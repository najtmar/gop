package cz.wie.najtmar.gop.game;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import cz.wie.najtmar.gop.board.Board;
import cz.wie.najtmar.gop.board.Joint;
import cz.wie.najtmar.gop.board.Position;
import cz.wie.najtmar.gop.board.RoadSection;
import cz.wie.najtmar.gop.common.Constants;
import cz.wie.najtmar.gop.common.Point2D;
import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.game.CityFactory.ActionState;

import org.junit.Before;
import org.junit.Test;

import java.io.StringReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import javax.json.Json;

public class CityFactoryTest {

  static final long START_TIME = 1000;

  Board board;
  Joint joint00;
  Joint joint30;
  Joint joint33;
  Joint joint60;
  RoadSection roadSection0;
  RoadSection roadSection1;
  RoadSection roadSection2;

  Game game;
  GameProperties gameProperties;
  Player playerA;
  Player playerB;
  City cityA;
  City cityB;
  CityFactory cityAFactory0;
  CityFactory cityAFactory1;
  CityFactory cityAFactory2;

  EventFactory eventFactory;
  LinkedList<Event> eventsGenerated;

  /**
   * Method setUp.
   * @throws Exception when method execution fails
   */
  @Before
  public void setUp() throws Exception {
    final Properties simpleBoardPropertiesSet = new Properties();
    simpleBoardPropertiesSet.load(CityFactoryTest.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/board/simple-board.properties"));
    board = new Board(simpleBoardPropertiesSet);
    joint00 = (new Joint.Builder())
        .setBoard(board)
        .setIndex(0)
        .setName("joint00")
        .setCoordinatesPair(new Point2D.Double(0.0, 0.0))
        .build();
    joint30 = (new Joint.Builder())
        .setBoard(board)
        .setIndex(1)
        .setName("joint30")
        .setCoordinatesPair(new Point2D.Double(3.0, 0.0))
        .build();
    joint33 = (new Joint.Builder())
        .setBoard(board)
        .setIndex(2)
        .setName("joint33")
        .setCoordinatesPair(new Point2D.Double(3.0, 3.0))
        .build();
    joint60 = (new Joint.Builder())
        .setBoard(board)
        .setIndex(3)
        .setName("joint60")
        .setCoordinatesPair(new Point2D.Double(6.0, 0.0))
        .build();
    roadSection0 = (new RoadSection.Builder())
        .setBoard(board)
        .setIndex(0)
        .setJoints(joint00, joint30)
        .build();
    roadSection1 = (new RoadSection.Builder())
        .setBoard(board)
        .setIndex(1)
        .setJoints(joint30, joint33)
        .build();
    roadSection2 = (new RoadSection.Builder())
        .setBoard(board)
        .setIndex(2)
        .setJoints(joint30, joint60)
        .build();

    final Properties simpleGamePropertiesSet = new Properties();
    simpleGamePropertiesSet.load(CityFactoryTest.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/game/simple-game.properties"));
    simpleGamePropertiesSet.setProperty(GameProperties.START_TIME_PROPERTY_NAME, "" + START_TIME);
    game = new Game(simpleGamePropertiesSet);
    game.setBoard(board);
    gameProperties = game.getGameProperties();
    playerA = new Player(new User("playerA", new UUID(12345, 67890)), 0, game);
    playerB = new Player(new User("playerB", new UUID(98765, 43210)), 1, game);
    game.addPlayer(playerA);
    game.addPlayer(playerB);
    City.resetFirstAvailableId();
    cityA = (new City.Builder())
        .setGame(game)
        .setName("cityA")
        .setPlayer(playerA)
        .setJoint(joint30)
        .build();
    cityB = (new City.Builder())
        .setGame(game)
        .setName("cityB")
        .setPlayer(playerB)
        .setJoint(joint33)
        .build();
    cityAFactory0 = cityA.getFactory(0);
    cityAFactory1 = cityA.getFactory(1);
    cityAFactory2 = cityA.getFactory(2);
    Prawn.resetFirstAvailableId();
    game.initialize();

    final ObjectSerializer objectSerializer = new ObjectSerializer(game);
    eventFactory = new EventFactory(objectSerializer);
    game.advanceTimeTo(START_TIME + 10);
    eventsGenerated = new LinkedList<>();
  }

  @Test
  public void testFactoryCityBindingWorks() throws GameException {
    assertEquals(cityA, cityAFactory0.getCity());
    assertEquals(0, cityAFactory0.getIndex());
    assertEquals(1, cityAFactory1.getIndex());
    assertEquals(2, cityAFactory2.getIndex());
  }

  @Test
  public void testFactoryBasicOperationsWork() throws GameException {
    assertEquals(CityFactory.State.DISABLED, cityAFactory1.getState());
    cityAFactory1.enable();
    assertEquals(null, cityAFactory1.getAction());
    cityAFactory1.setAction(cityAFactory1.new ProduceWarriorAction(Math.PI / 4.0));
    assertNotEquals(null, cityAFactory1.getAction());
    assertEquals(0.0, cityAFactory1.getAction().getProgress(), 0.0);
    assertEquals(ActionState.INITIALIZED, cityAFactory1.getAction().getState());
    assertEquals(Math.PI / 4.0, ((CityFactory.ProduceWarriorAction) cityAFactory1.getAction()).getDirection(), 0.0);
    cityAFactory1.disable();
    cityAFactory1.enable();
    assertEquals(null, cityAFactory1.getAction());
  }

  /**
   * Runs the body of testWarriorCreationWorks() . Depending on the value of generatesEvents, either generates Events or
   * not.
   * @param generateEvents determines whether Events should be generated
   * @throws Exception when something unexpected happens
   */
  private void runTestWarriorCreationWorks(boolean generateEvents) throws Exception {
    EventFactory eventFactoryArg;
    List<Event> eventsGeneratedArg;
    if (generateEvents) {
      eventFactoryArg = eventFactory;
      eventsGeneratedArg = eventsGenerated;
    } else {
      eventFactoryArg = null;
      eventsGeneratedArg = null;
    }
    final Warrior expectedWarrior = (new Warrior.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(joint30.getNormalizedPosition())
        .setDirection(Math.PI / 4.0)
        .setGameProperties(gameProperties)
        .setPlayer(playerA)
        .build();
    Prawn.resetFirstAvailableId();

    assertEquals(new HashSet<>(), game.getPlayerPrawns(playerA));
    cityAFactory0.setAction(cityAFactory0.new ProduceWarriorAction(Math.PI / 4.0));
    assertNotEquals(null, cityAFactory0.getAction());
    assertEquals(0.0, cityAFactory0.getAction().getProgress(), 0.0);
    assertEquals(ActionState.INITIALIZED, cityAFactory0.getAction().getState());
    cityAFactory0.getAction().increaseProgressByUniversalDelta(
        0.4 / game.getGameProperties().getProduceActionProgressFactor(), eventFactoryArg, eventsGeneratedArg);
    assertEquals(0.4, cityAFactory0.getAction().getProgress(), Constants.DOUBLE_DELTA);
    assertEquals(ActionState.INITIALIZED, cityAFactory0.getAction().getState());
    cityAFactory0.getAction().increaseProgressByUniversalDelta(
        0.4 / game.getGameProperties().getProduceActionProgressFactor(), eventFactoryArg, eventsGeneratedArg);
    assertEquals(0.8, cityAFactory0.getAction().getProgress(), Constants.DOUBLE_DELTA);
    if (generateEvents) {
      assertEquals(Arrays.asList(
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  }, "
              + "  \"progress\": 0.4 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  }, "
              + "  \"progress\": 0.8 "
              + "}")).readObject())), eventsGenerated);
    }
    cityAFactory0.getAction().increaseProgressByUniversalDelta(
        0.4 / game.getGameProperties().getProduceActionProgressFactor(), eventFactoryArg, eventsGeneratedArg);
    assertEquals(1.0, cityAFactory0.getAction().getProgress(), Constants.DOUBLE_DELTA);
    assertEquals(ActionState.FINISHED, cityAFactory0.getAction().getState());

    assertEquals(new HashSet<>(Arrays.asList(expectedWarrior)), game.getPlayerPrawns(playerA));
    assertEquals(START_TIME + 10, game.getPlayerPrawns(playerA).iterator().next().getLastMoveTime());
    if (generateEvents) {
      assertEquals(Arrays.asList(
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  }, "
              + "  \"progress\": 0.4 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  }, "
              + "  \"progress\": 0.8 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  }, "
              + "  \"progress\": 1.0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"PRODUCE_WARRIOR\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"id\": 0, "
              + "  \"position\": { "
              + "    \"roadSection\": 0, "
              + "    \"index\": 3 "
              + "  }, "
              + "  \"direction\": " + (Math.PI / 4.0) + ", "
              + "  \"player\": 0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"ACTION_FINISHED\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  } "
              + "}")).readObject())), eventsGenerated);
    } else {
      assertEquals(Arrays.asList(), eventsGenerated);
    }
  }

  @Test
  public void testWarriorCreationWorks() throws Exception {
    runTestWarriorCreationWorks(true);
  }

  @Test
  public void testWarriorCreationWorksNoEvents() throws Exception {
    runTestWarriorCreationWorks(false);
  }

  /**
   * Runs the body of testWarriorWithBattleCreationWorks() . Depending on the value of generatesEvents, either generates
   * Events or not.
   * @param generateEvents determines whether Events should be generated
   * @throws Exception when something unexpected happens
   */
  private void runTestWarriorWithBattleCreationWorks(boolean generateEvents) throws Exception {
    EventFactory eventFactoryArg;
    List<Event> eventsGeneratedArg;
    if (generateEvents) {
      eventFactoryArg = eventFactory;
      eventsGeneratedArg = eventsGenerated;
    } else {
      eventFactoryArg = null;
      eventsGeneratedArg = null;
    }
    final Warrior expectedWarrior = (new Warrior.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(joint30.getNormalizedPosition())
        .setDirection(Math.PI / 4.0)
        .setGameProperties(gameProperties)
        .setPlayer(playerA)
        .build();

    // Add a relevant Battle .
    final Warrior companionWarrior = (new Warrior.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(joint30.getNormalizedPosition())
        .setDirection(Math.PI / 2.0)
        .setGameProperties(gameProperties)
        .setPlayer(playerA)
        .build();
    game.addPrawn(companionWarrior);
    final Warrior opponentWarrior = (new Warrior.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(joint33.getNormalizedPosition())
        .setDirection(3 * Math.PI / 2.0)
        .setGameProperties(gameProperties)
        .setPlayer(playerB)
        .build();
    game.addPrawn(opponentWarrior);
    opponentWarrior.setCurrentPosition(new Position(roadSection1, 1));
    opponentWarrior.setItinerary((new Itinerary.Builder()
        .addPosition(new Position(roadSection1, 1))
        .addPosition(new Position(roadSection1, 0))
        .build()));
    final Battle oldBattle = new Battle(companionWarrior, companionWarrior.getCurrentPosition(), null, opponentWarrior,
        opponentWarrior.getCurrentPosition(), companionWarrior.getCurrentPosition());
    game.addBattle(oldBattle);

    Prawn.resetFirstAvailableId();

    assertEquals(new HashSet<>(Arrays.asList(companionWarrior)), game.getPlayerPrawns(playerA));
    assertEquals(new HashSet<>(Arrays.asList(oldBattle)), game.getBattles());
    cityAFactory0.setAction(cityAFactory0.new ProduceWarriorAction(Math.PI / 4.0));
    assertNotEquals(null, cityAFactory0.getAction());
    assertEquals(0.0, cityAFactory0.getAction().getProgress(), 0.0);
    assertEquals(ActionState.INITIALIZED, cityAFactory0.getAction().getState());
    cityAFactory0.getAction().increaseProgressByUniversalDelta(
        1.0 / game.getGameProperties().getProduceActionProgressFactor(), eventFactoryArg, eventsGeneratedArg);
    assertEquals(1.0, cityAFactory0.getAction().getProgress(), Constants.DOUBLE_DELTA);
    assertEquals(ActionState.FINISHED, cityAFactory0.getAction().getState());

    assertEquals(new HashSet<>(Arrays.asList(companionWarrior, expectedWarrior)), game.getPlayerPrawns(playerA));
    assertEquals(new HashSet<>(Arrays.asList(oldBattle,
        new Battle(expectedWarrior, expectedWarrior.getCurrentPosition(), null, opponentWarrior,
            opponentWarrior.getCurrentPosition(), expectedWarrior.getCurrentPosition()))),
        game.getBattles());
    assertEquals(START_TIME + 10, game.getPlayerPrawns(playerA).iterator().next().getLastMoveTime());
    if (generateEvents) {
      assertEquals(Arrays.asList(
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  }, "
              + "  \"progress\": 1.0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"PRODUCE_WARRIOR\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"id\": 0, "
              + "  \"position\": { "
              + "    \"roadSection\": 0, "
              + "    \"index\": 3 "
              + "  }, "
              + "  \"direction\": " + (Math.PI / 4.0) + ", "
              + "  \"player\": 0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"ACTION_FINISHED\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  } "
              + "}")).readObject()),
          new Event(Json.createObjectBuilder()
              .add("type", "ADD_BATTLE")
              .add("time", START_TIME + 10)
              .add("battle", Json.createObjectBuilder()
                  .add("prawn0", expectedWarrior.getId())
                  .add("prawn1", opponentWarrior.getId())
                  .add("normalizedPosition0", Json.createObjectBuilder()
                      .add("roadSection", 1)
                      .add("index", 0)
                      .build())
                  .add("normalizedPosition1", Json.createObjectBuilder()
                      .add("roadSection", 1)
                      .add("index", 1)
                      .build())
                  .build())
              .build())), eventsGenerated);
    } else {
      assertEquals(Arrays.asList(), eventsGenerated);
    }
  }

  @Test
  public void testWarriorWithBattleCreationWorks() throws Exception {
    runTestWarriorWithBattleCreationWorks(true);
  }

  @Test
  public void testWarriorWithBattleCreationWorksNoEvents() throws Exception {
    runTestWarriorWithBattleCreationWorks(false);
  }

  /**
   * Runs the body of testWarriorWithoutBattleCreationWorks() . Depending on the value of generatesEvents, either
   * generates Events or not.
   * @param generateEvents determines whether Events should be generated
   * @throws Exception when something unexpected happens
   */
  private void runTestWarriorWithoutBattleCreationWorks(boolean generateEvents) throws Exception {
    EventFactory eventFactoryArg;
    List<Event> eventsGeneratedArg;
    if (generateEvents) {
      eventFactoryArg = eventFactory;
      eventsGeneratedArg = eventsGenerated;
    } else {
      eventFactoryArg = null;
      eventsGeneratedArg = null;
    }
    final Warrior expectedWarrior = (new Warrior.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(joint30.getNormalizedPosition())
        .setDirection(Math.PI / 4.0)
        .setGameProperties(gameProperties)
        .setPlayer(playerA)
        .build();

    // Add an irrelevant Battle .
    final Warrior companionWarrior = (new Warrior.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(joint30.getNormalizedPosition())
        .setDirection(Math.PI / 2.0)
        .setGameProperties(gameProperties)
        .setPlayer(playerA)
        .build();
    game.addPrawn(companionWarrior);
    companionWarrior.setItinerary((new Itinerary.Builder()
        .addPosition(new Position(roadSection1, 0))
        .addPosition(new Position(roadSection1, 1))
        .build()));
    final Warrior opponentWarrior = (new Warrior.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(joint33.getNormalizedPosition())
        .setDirection(3 * Math.PI / 2.0)
        .setGameProperties(gameProperties)
        .setPlayer(playerB)
        .build();
    game.addPrawn(opponentWarrior);
    opponentWarrior.setCurrentPosition(new Position(roadSection1, 1));
    final Battle oldBattle = new Battle(companionWarrior, companionWarrior.getCurrentPosition(),
        opponentWarrior.getCurrentPosition(), opponentWarrior, opponentWarrior.getCurrentPosition(), null);
    game.addBattle(oldBattle);

    Prawn.resetFirstAvailableId();

    assertEquals(new HashSet<>(Arrays.asList(companionWarrior)), game.getPlayerPrawns(playerA));
    assertEquals(new HashSet<>(Arrays.asList(oldBattle)), game.getBattles());
    cityAFactory0.setAction(cityAFactory0.new ProduceWarriorAction(Math.PI / 4.0));
    assertNotEquals(null, cityAFactory0.getAction());
    assertEquals(0.0, cityAFactory0.getAction().getProgress(), 0.0);
    assertEquals(ActionState.INITIALIZED, cityAFactory0.getAction().getState());
    cityAFactory0.getAction().increaseProgressByUniversalDelta(
        1.0 / game.getGameProperties().getProduceActionProgressFactor(), eventFactoryArg, eventsGeneratedArg);
    assertEquals(1.0, cityAFactory0.getAction().getProgress(), Constants.DOUBLE_DELTA);
    assertEquals(ActionState.FINISHED, cityAFactory0.getAction().getState());

    assertEquals(new HashSet<>(Arrays.asList(companionWarrior, expectedWarrior)), game.getPlayerPrawns(playerA));
    assertEquals(new HashSet<>(Arrays.asList(oldBattle)), game.getBattles());
    assertEquals(START_TIME + 10, game.getPlayerPrawns(playerA).iterator().next().getLastMoveTime());
    if (generateEvents) {
      assertEquals(Arrays.asList(
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  }, "
              + "  \"progress\": 1.0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"PRODUCE_WARRIOR\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"id\": 0, "
              + "  \"position\": { "
              + "    \"roadSection\": 0, "
              + "    \"index\": 3 "
              + "  }, "
              + "  \"direction\": " + (Math.PI / 4.0) + ", "
              + "  \"player\": 0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"ACTION_FINISHED\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  } "
              + "}")).readObject())), eventsGenerated);
    } else {
      assertEquals(Arrays.asList(), eventsGenerated);
    }
  }

  @Test
  public void testWarriorWithoutBattleCreationWorks() throws Exception {
    runTestWarriorWithoutBattleCreationWorks(true);
  }

  @Test
  public void testWarriorWithoutBattleCreationWorksNoEvents() throws Exception {
    runTestWarriorWithoutBattleCreationWorks(false);
  }

  /**
   * Runs the body of testSettlersUnitCreationWorks() . Depending on the value of generatesEvents, either generates
   * Events or not.
   * @param generateEvents determines whether Events should be generated
   * @throws Exception when something unexpected happens
   */
  private void runTestSettlersUnitCreationWorks(boolean generateEvents) throws Exception {
    EventFactory eventFactoryArg;
    List<Event> eventsGeneratedArg;
    if (generateEvents) {
      eventFactoryArg = eventFactory;
      eventsGeneratedArg = eventsGenerated;
    } else {
      eventFactoryArg = null;
      eventsGeneratedArg = null;
    }
    final SettlersUnit expectedSettlersUnit = (new SettlersUnit.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(joint30.getNormalizedPosition())
        .setGameProperties(gameProperties)
        .setPlayer(playerA)
        .build();

    // Add a relevant Battle .
    final Warrior companionWarrior = (new Warrior.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(joint30.getNormalizedPosition())
        .setDirection(Math.PI / 2.0)
        .setGameProperties(gameProperties)
        .setPlayer(playerA)
        .build();
    game.addPrawn(companionWarrior);
    final Warrior opponentWarrior = (new Warrior.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(joint33.getNormalizedPosition())
        .setDirection(3 * Math.PI / 2.0)
        .setGameProperties(gameProperties)
        .setPlayer(playerB)
        .build();
    game.addPrawn(opponentWarrior);
    opponentWarrior.setCurrentPosition(new Position(roadSection1, 1));
    opponentWarrior.setItinerary((new Itinerary.Builder()
        .addPosition(new Position(roadSection1, 1))
        .addPosition(new Position(roadSection1, 0))
        .build()));
    final Battle oldBattle = new Battle(companionWarrior, companionWarrior.getCurrentPosition(), null, opponentWarrior,
        opponentWarrior.getCurrentPosition(), companionWarrior.getCurrentPosition());
    game.addBattle(oldBattle);

    Prawn.resetFirstAvailableId();

    assertEquals(new HashSet<>(Arrays.asList(cityA)), game.getPlayerCities(playerA));
    cityA.grow();
    assertEquals(2, cityA.getSize());
    assertEquals(new HashSet<>(Arrays.asList(companionWarrior)), game.getPlayerPrawns(playerA));
    assertEquals(new HashSet<>(Arrays.asList(oldBattle)), game.getBattles());
    cityAFactory0.setAction(cityAFactory0.new ProduceSettlersUnitAction());
    assertNotEquals(null, cityAFactory0.getAction());
    assertEquals(0.0, cityAFactory0.getAction().getProgress(), 0.0);
    assertEquals(ActionState.INITIALIZED, cityAFactory0.getAction().getState());
    cityAFactory0.getAction().increaseProgressByUniversalDelta(
        1.0 / game.getGameProperties().getProduceActionProgressFactor(), eventFactoryArg, eventsGeneratedArg);
    assertEquals(1, cityAFactory0.getIndex());
    assertEquals(CityFactory.State.DISABLED, cityAFactory0.getState());

    assertEquals(new HashSet<>(Arrays.asList(companionWarrior, expectedSettlersUnit)), game.getPlayerPrawns(playerA));
    assertEquals(new HashSet<>(Arrays.asList(oldBattle,
        new Battle(expectedSettlersUnit, expectedSettlersUnit.getCurrentPosition(), null, opponentWarrior,
            opponentWarrior.getCurrentPosition(), expectedSettlersUnit.getCurrentPosition()))),
        game.getBattles());
    assertEquals(START_TIME + 10, game.getPlayerPrawns(playerA).iterator().next().getLastMoveTime());
    assertEquals(1, cityA.getSize());
    assertEquals(new HashSet<>(Arrays.asList(cityA)), game.getPlayerCities(playerA));
    if (generateEvents) {
      assertEquals(Arrays.asList(
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  }, "
              + "  \"progress\": 1.0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"PRODUCE_SETTLERS_UNIT\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"id\": 0, "
              + "  \"position\": { "
              + "    \"roadSection\": 0, "
              + "    \"index\": 3 "
              + "  }, "
              + "  \"player\": 0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SHRINK_OR_DESTROY_CITY\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"city\": 0, "
              + "  \"factoryIndex\": 0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"ACTION_FINISHED\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 1 "
              + "  } "
              + "}")).readObject()),
          new Event(Json.createObjectBuilder()
              .add("type", "ADD_BATTLE")
              .add("time", START_TIME + 10)
              .add("battle", Json.createObjectBuilder()
                  .add("prawn0", expectedSettlersUnit.getId())
                  .add("prawn1", opponentWarrior.getId())
                  .add("normalizedPosition0", Json.createObjectBuilder()
                      .add("roadSection", 1)
                      .add("index", 0)
                      .build())
                  .add("normalizedPosition1", Json.createObjectBuilder()
                      .add("roadSection", 1)
                      .add("index", 1)
                      .build())
                  .build())
              .build())), eventsGenerated);
    } else {
      assertEquals(Arrays.asList(), eventsGenerated);
    }
  }

  @Test
  public void testSettlersUnitWithBattleCreationWorks() throws Exception {
    runTestSettlersUnitWithBattleCreationWorks(true);
  }

  @Test
  public void testSettlersUnitWithBattleCreationWorksNoEvents() throws Exception {
    runTestSettlersUnitWithBattleCreationWorks(false);
  }

  /**
   * Runs the body of testSettlersUnitWithBattleCreationWorks() . Depending on the value of generatesEvents, either
   * generates Events or not.
   * @param generateEvents determines whether Events should be generated
   * @throws Exception when something unexpected happens
   */
  private void runTestSettlersUnitWithBattleCreationWorks(boolean generateEvents) throws Exception {
    EventFactory eventFactoryArg;
    List<Event> eventsGeneratedArg;
    if (generateEvents) {
      eventFactoryArg = eventFactory;
      eventsGeneratedArg = eventsGenerated;
    } else {
      eventFactoryArg = null;
      eventsGeneratedArg = null;
    }
    final SettlersUnit expectedSettlersUnit0 = (new SettlersUnit.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(joint30.getNormalizedPosition())
        .setGameProperties(gameProperties)
        .setPlayer(playerA)
        .build();
    final SettlersUnit expectedSettlersUnit1 = (new SettlersUnit.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(joint30.getNormalizedPosition())
        .setGameProperties(gameProperties)
        .setPlayer(playerA)
        .build();
    Prawn.resetFirstAvailableId();

    assertEquals(new HashSet<>(Arrays.asList(cityA)), game.getPlayerCities(playerA));
    cityA.grow();
    assertEquals(2, cityA.getSize());
    assertEquals(new HashSet<>(), game.getPlayerPrawns(playerA));
    cityAFactory0.setAction(cityAFactory0.new ProduceSettlersUnitAction());
    assertNotEquals(null, cityAFactory0.getAction());
    assertEquals(0.0, cityAFactory0.getAction().getProgress(), 0.0);
    assertEquals(ActionState.INITIALIZED, cityAFactory0.getAction().getState());
    cityAFactory0.getAction().increaseProgressByUniversalDelta(
        0.4 / game.getGameProperties().getProduceActionProgressFactor(), eventFactoryArg, eventsGeneratedArg);
    assertEquals(0.4, cityAFactory0.getAction().getProgress(), Constants.DOUBLE_DELTA);
    assertEquals(ActionState.INITIALIZED, cityAFactory0.getAction().getState());
    cityAFactory0.getAction().increaseProgressByUniversalDelta(
        0.4 / game.getGameProperties().getProduceActionProgressFactor(), eventFactoryArg, eventsGeneratedArg);
    assertEquals(0.8, cityAFactory0.getAction().getProgress(), Constants.DOUBLE_DELTA);
    if (generateEvents) {
      assertEquals(Arrays.asList(
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  }, "
              + "  \"progress\": 0.4 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  }, "
              + "  \"progress\": 0.8 "
              + "}")).readObject())), eventsGenerated);
    }
    cityAFactory0.getAction().increaseProgressByUniversalDelta(
        0.4 / game.getGameProperties().getProduceActionProgressFactor(), eventFactoryArg, eventsGeneratedArg);
    assertEquals(1, cityAFactory0.getIndex());
    assertEquals(CityFactory.State.DISABLED, cityAFactory0.getState());

    assertEquals(new HashSet<>(Arrays.asList(expectedSettlersUnit0)), game.getPlayerPrawns(playerA));
    assertEquals(START_TIME + 10, game.getPlayerPrawns(playerA).iterator().next().getLastMoveTime());
    assertEquals(1, cityA.getSize());
    assertEquals(new HashSet<>(Arrays.asList(cityA)), game.getPlayerCities(playerA));
    if (generateEvents) {
      assertEquals(Arrays.asList(
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  }, "
              + "  \"progress\": 0.4 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  }, "
              + "  \"progress\": 0.8 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  }, "
              + "  \"progress\": 1.0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"PRODUCE_SETTLERS_UNIT\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"id\": 0, "
              + "  \"position\": { "
              + "    \"roadSection\": 0, "
              + "    \"index\": 3 "
              + "  }, "
              + "  \"player\": 0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SHRINK_OR_DESTROY_CITY\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"city\": 0, "
              + "  \"factoryIndex\": 0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"ACTION_FINISHED\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 1 "
              + "  } "
              + "}")).readObject())), eventsGenerated);
    } else {
      assertEquals(Arrays.asList(), eventsGenerated);
    }

    game.advanceTimeTo(START_TIME + 20);

    assertEquals(0, cityAFactory1.getIndex());
    assertEquals(CityFactory.State.ENABLED, cityAFactory1.getState());
    cityAFactory1.setAction(cityAFactory1.new ProduceSettlersUnitAction());
    assertEquals(0.0, cityAFactory1.getAction().getProgress(), 0.0);
    assertEquals(ActionState.INITIALIZED, cityAFactory1.getAction().getState());
    cityAFactory1.getAction().increaseProgressByUniversalDelta(
        1.0 / game.getGameProperties().getProduceActionProgressFactor(), eventFactoryArg, eventsGeneratedArg);
    assertEquals(CityFactory.State.DISABLED, cityAFactory1.getState());

    assertEquals(new HashSet<>(Arrays.asList(expectedSettlersUnit0, expectedSettlersUnit1)),
        game.getPlayerPrawns(playerA));
    assertEquals(0, cityA.getSize());
    assertEquals(new HashSet<>(), game.getPlayerCities(playerA));
    if (generateEvents) {
      assertEquals(Arrays.asList(
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  }, "
              + "  \"progress\": 0.4 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  }, "
              + "  \"progress\": 0.8 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  }, "
              + "  \"progress\": 1.0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"PRODUCE_SETTLERS_UNIT\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"id\": 0, "
              + "  \"position\": { "
              + "    \"roadSection\": 0, "
              + "    \"index\": 3 "
              + "  }, "
              + "  \"player\": 0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SHRINK_OR_DESTROY_CITY\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"city\": 0, "
              + "  \"factoryIndex\": 0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"ACTION_FINISHED\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 1 "
              + "  } "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 20) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  }, "
              + "  \"progress\": 1.0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"PRODUCE_SETTLERS_UNIT\", "
              + "  \"time\": " + (START_TIME + 20) + ", "
              + "  \"id\": 1, "
              + "  \"position\": { "
              + "    \"roadSection\": 0, "
              + "    \"index\": 3 "
              + "  }, "
              + "  \"player\": 0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SHRINK_OR_DESTROY_CITY\", "
              + "  \"time\": " + (START_TIME + 20) + ", "
              + "  \"city\": 0, "
              + "  \"factoryIndex\": 0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"ACTION_FINISHED\", "
              + "  \"time\": " + (START_TIME + 20) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  } "
              + "}")).readObject())),
          eventsGenerated);
    } else {
      assertEquals(Arrays.asList(), eventsGenerated);
    }
  }

  @Test
  public void testSettlersUnitCreationWorks() throws Exception {
    runTestSettlersUnitCreationWorks(true);
  }

  @Test
  public void testSettlersUnitCreationWorksNoEvents() throws Exception {
    runTestSettlersUnitCreationWorks(false);
  }

  /**
   * Runs the body of testPrawnRepairingWorks() . Depending on the value of generatesEvents, either generates Events or
   * not.
   * @param generateEvents determines whether Events should be generated
   * @throws Exception when something unexpected happens
   */
  private void runTestPrawnRepairingWorks(boolean generateEvents) throws Exception {
    EventFactory eventFactoryArg;
    List<Event> eventsGeneratedArg;
    if (generateEvents) {
      eventFactoryArg = eventFactory;
      eventsGeneratedArg = eventsGenerated;
    } else {
      eventFactoryArg = null;
      eventsGeneratedArg = null;
    }
    final Prawn prawn = new TestPrawn(gameProperties, playerA, new Position(roadSection0, 3), START_TIME);
    game.addPrawn(prawn);
    prawn.decreaseEnergy(Prawn.MAX_ENERGY * 2.0 / 3.0);
    cityAFactory0.setAction(cityAFactory0.new RepairPrawnAction(prawn));
    assertEquals(1.0 / 3.0, cityAFactory0.getAction().getProgress(), Constants.DOUBLE_DELTA);
    assertEquals((1.0 / 3.0) * Prawn.MAX_ENERGY, prawn.getEnergy(), Constants.DOUBLE_DELTA);
    assertEquals(ActionState.INITIALIZED, cityAFactory0.getAction().getState());
    assertTrue(cityAFactory0.getAction().revise(eventFactoryArg, eventsGeneratedArg));
    assertEquals(Arrays.asList(), eventsGenerated);
    cityAFactory0.getAction().increaseProgressByUniversalDelta(
        0.5 / game.getGameProperties().getRepairActionProgressFactor(), eventFactoryArg, eventsGeneratedArg);
    assertEquals(1.0 / 3.0 + 0.5, cityAFactory0.getAction().getProgress(), Constants.DOUBLE_DELTA);
    assertEquals((1.0 / 3.0 + 0.5) * Prawn.MAX_ENERGY, prawn.getEnergy(), Constants.DOUBLE_DELTA);
    assertEquals(ActionState.INITIALIZED, cityAFactory0.getAction().getState());
    if (generateEvents) {
      assertEquals(Arrays.asList(
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  }, "
              + "  \"progress\": 0.8333333333333334 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"REPAIR_PRAWN\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"delta\": 0.5,"
              + "  \"prawn\": 0 "
              + "}")).readObject())), eventsGenerated);
    } else {
      assertEquals(Arrays.asList(), eventsGenerated);
    }
    game.advanceTimeTo(START_TIME + 20);
    assertTrue(cityAFactory0.getAction().revise(eventFactoryArg, eventsGeneratedArg));
    if (generateEvents) {
      assertEquals(2, eventsGenerated.size());
    } else {
      assertEquals(Arrays.asList(), eventsGenerated);
    }
    cityAFactory0.getAction().increaseProgressByUniversalDelta(
        0.5 / game.getGameProperties().getRepairActionProgressFactor(), eventFactoryArg, eventsGeneratedArg);
    assertEquals(1.0, cityAFactory0.getAction().getProgress(), Constants.DOUBLE_DELTA);
    assertEquals(ActionState.FINISHED, cityAFactory0.getAction().getState());
    assertEquals(Prawn.MAX_ENERGY, prawn.getEnergy(), 0.0);
    if (generateEvents) {
      assertEquals(Arrays.asList(
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  }, "
              + "  \"progress\": 0.8333333333333334 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"REPAIR_PRAWN\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"delta\": 0.5,"
              + "  \"prawn\": 0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 20) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  }, "
              + "  \"progress\": 1.0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"REPAIR_PRAWN\", "
              + "  \"time\": " + (START_TIME + 20) + ", "
              + "  \"delta\": 0.5,"
              + "  \"prawn\": 0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"REPAIR_PRAWN\", "
              + "  \"time\": " + (START_TIME + 20) + ", "
              + "  \"delta\": 1.0,"
              + "  \"prawn\": 0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"ACTION_FINISHED\", "
              + "  \"time\": " + (START_TIME + 20) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  } "
              + "}")).readObject())), eventsGenerated);
    } else {
      assertEquals(Arrays.asList(), eventsGenerated);
    }
  }

  @Test
  public void testPrawnRepairingWorks() throws Exception {
    runTestPrawnRepairingWorks(true);
  }

  @Test
  public void testPrawnRepairingWorksNoEvents() throws Exception {
    runTestPrawnRepairingWorks(false);
  }

  /**
   * Runs the body of testPrawnRepairingRevisionWorks() . Depending on the value of generatesEvents, either generates
   * Events or not.
   * @param generateEvents determines whether Events should be generated
   * @throws Exception when something unexpected happens
   */
  private void runTestPrawnRepairingRevisionWorks(boolean generateEvents) throws Exception {
    EventFactory eventFactoryArg;
    List<Event> eventsGeneratedArg;
    if (generateEvents) {
      eventFactoryArg = eventFactory;
      eventsGeneratedArg = eventsGenerated;
    } else {
      eventFactoryArg = null;
      eventsGeneratedArg = null;
    }
    final Prawn prawn = new TestPrawn(gameProperties, playerA, new Position(roadSection0, 3), START_TIME);
    game.addPrawn(prawn);
    prawn.decreaseEnergy(Prawn.MAX_ENERGY * 2.0 / 3.0);
    cityAFactory0.setAction(cityAFactory0.new RepairPrawnAction(prawn));
    assertEquals(1.0 / 3.0, cityAFactory0.getAction().getProgress(), Constants.DOUBLE_DELTA);
    assertEquals((1.0 / 3.0) * Prawn.MAX_ENERGY, prawn.getEnergy(), Constants.DOUBLE_DELTA);
    assertEquals(ActionState.INITIALIZED, cityAFactory0.getAction().getState());
    assertTrue(cityAFactory0.getAction().revise(eventFactoryArg, eventsGeneratedArg));
    assertEquals(Arrays.asList(), eventsGenerated);
    cityAFactory0.getAction().increaseProgressByUniversalDelta(
        0.5 / game.getGameProperties().getRepairActionProgressFactor(), eventFactoryArg, eventsGeneratedArg);
    assertEquals(1.0 / 3.0 + 0.5, cityAFactory0.getAction().getProgress(), Constants.DOUBLE_DELTA);
    assertEquals((1.0 / 3.0 + 0.5) * Prawn.MAX_ENERGY, prawn.getEnergy(), Constants.DOUBLE_DELTA);
    assertEquals(ActionState.INITIALIZED, cityAFactory0.getAction().getState());
    if (generateEvents) {
      assertEquals(Arrays.asList(
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  }, "
              + "  \"progress\": 0.8333333333333334 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"REPAIR_PRAWN\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"delta\": 0.5,"
              + "  \"prawn\": 0 "
              + "}")).readObject())), eventsGenerated);
    } else {
      assertEquals(Arrays.asList(), eventsGenerated);
    }
    game.advanceTimeTo(START_TIME + 20);

    // prawn moves away.
    prawn.setCurrentPosition(new Position(roadSection0, 2));

    assertFalse(cityAFactory0.getAction().revise(eventFactoryArg, eventsGeneratedArg));
    if (generateEvents) {
      assertEquals(Arrays.asList(
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"ACTION_FINISHED\", "
              + "  \"time\": " + (START_TIME + 20) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  } "
              + "}")).readObject())),
          eventsGenerated.subList(2, eventsGenerated.size()));
    } else {
      assertEquals(Arrays.asList(), eventsGenerated);
    }
    assertEquals(ActionState.FINISHED, cityAFactory0.getAction().getState());
  }

  @Test
  public void testPrawnRepairingRevisionWorks() throws Exception {
    runTestPrawnRepairingRevisionWorks(true);
  }

  @Test
  public void testPrawnRepairingRevisionWorksNoEvents() throws Exception {
    runTestPrawnRepairingRevisionWorks(false);
  }

  /**
   * Runs the body of testGrowWorks() . Depending on the value of generatesEvents, either generates Events or not.
   * @param generateEvents determines whether Events should be generated
   * @throws Exception when something unexpected happens
   */
  private void runTestGrowWorks(boolean generateEvents) throws Exception {
    EventFactory eventFactoryArg;
    List<Event> eventsGeneratedArg;
    if (generateEvents) {
      eventFactoryArg = eventFactory;
      eventsGeneratedArg = eventsGenerated;
    } else {
      eventFactoryArg = null;
      eventsGeneratedArg = null;
    }
    assertEquals(1, cityA.getSize());
    assertEquals(CityFactory.State.ENABLED, cityAFactory0.getState());
    assertEquals(null, cityAFactory0.getAction());
    assertEquals(CityFactory.State.DISABLED, cityAFactory1.getState());
    assertEquals(CityFactory.State.DISABLED, cityAFactory2.getState());

    cityAFactory0.setAction(cityAFactory0.new GrowAction());
    assertEquals(CityFactory.State.ENABLED, cityAFactory0.getState());
    assertEquals(ActionState.INITIALIZED, cityAFactory0.getAction().getState());
    assertEquals(0.0, cityAFactory0.getAction().getProgress(), 0.0);
    cityAFactory0.getAction().increaseProgressByUniversalDelta(
        0.5 / game.getGameProperties().getGrowActionProgressFactor(), eventFactoryArg, eventsGeneratedArg);
    assertEquals(1, cityA.getSize());
    assertEquals(CityFactory.State.ENABLED, cityAFactory0.getState());
    assertEquals(ActionState.INITIALIZED, cityAFactory0.getAction().getState());
    assertEquals(0.5, cityAFactory0.getAction().getProgress(), Constants.DOUBLE_DELTA);
    assertEquals(CityFactory.State.DISABLED, cityAFactory1.getState());
    assertEquals(CityFactory.State.DISABLED, cityAFactory2.getState());
    if (generateEvents) {
      assertEquals(Arrays.asList(
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  }, "
              + "  \"progress\": 0.5 "
              + "}")).readObject())), eventsGenerated);
    }
    cityAFactory0.getAction().increaseProgressByUniversalDelta(
        0.5 / game.getGameProperties().getGrowActionProgressFactor(), eventFactoryArg, eventsGeneratedArg);
    assertEquals(2, cityA.getSize());
    assertEquals(CityFactory.State.ENABLED, cityAFactory0.getState());
    assertEquals(ActionState.FINISHED, cityAFactory0.getAction().getState());
    assertEquals(CityFactory.State.ENABLED, cityAFactory1.getState());
    assertEquals(null, cityAFactory1.getAction());
    assertEquals(CityFactory.State.DISABLED, cityAFactory2.getState());
    if (generateEvents) {
      assertEquals(Arrays.asList(
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  }, "
              + "  \"progress\": 0.5 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  }, "
              + "  \"progress\": 1.0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"GROW_CITY\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"city\": 0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"ACTION_FINISHED\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  } "
              + "}")).readObject())), eventsGenerated);
    } else {
      assertEquals(Arrays.asList(), eventsGenerated);
    }

    game.advanceTimeTo(START_TIME + 20);
    cityAFactory1.setAction(cityAFactory1.new GrowAction());
    assertEquals(CityFactory.State.ENABLED, cityAFactory1.getState());
    assertEquals(ActionState.INITIALIZED, cityAFactory1.getAction().getState());
    assertEquals(0.0, cityAFactory1.getAction().getProgress(), 0.0);
    cityAFactory1.getAction().increaseProgressByUniversalDelta(
        1.0 / game.getGameProperties().getGrowActionProgressFactor(), eventFactoryArg, eventsGeneratedArg);
    assertEquals(2, cityA.getSize());
    assertEquals(CityFactory.State.ENABLED, cityAFactory0.getState());
    assertEquals(ActionState.FINISHED, cityAFactory0.getAction().getState());
    assertEquals(CityFactory.State.ENABLED, cityAFactory1.getState());
    assertEquals(ActionState.INITIALIZED, cityAFactory1.getAction().getState());
    assertEquals(1.0, cityAFactory0.getAction().getProgress(), Constants.DOUBLE_DELTA);
    assertEquals(CityFactory.State.DISABLED, cityAFactory2.getState());

    cityAFactory0.setAction(cityAFactory0.new GrowAction());
    assertEquals(CityFactory.State.ENABLED, cityAFactory0.getState());
    assertEquals(ActionState.INITIALIZED, cityAFactory0.getAction().getState());
    assertEquals(0.0, cityAFactory0.getAction().getProgress(), 0.0);
    cityAFactory0.getAction().increaseProgressByUniversalDelta(
        1.0 / game.getGameProperties().getGrowActionProgressFactor(), eventFactoryArg, eventsGeneratedArg);
    assertEquals(3, cityA.getSize());
    assertEquals(CityFactory.State.ENABLED, cityAFactory0.getState());
    assertEquals(ActionState.FINISHED, cityAFactory0.getAction().getState());
    assertEquals(CityFactory.State.ENABLED, cityAFactory1.getState());
    assertEquals(ActionState.FINISHED, cityAFactory1.getAction().getState());
    assertEquals(CityFactory.State.ENABLED, cityAFactory2.getState());
    assertEquals(null, cityAFactory2.getAction());
    if (generateEvents) {
      assertEquals(Arrays.asList(
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  }, "
              + "  \"progress\": 0.5 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  }, "
              + "  \"progress\": 1.0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"GROW_CITY\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"city\": 0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"ACTION_FINISHED\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  } "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
             "{ "
             + "  \"type\": \"SET_FACTORY_PROGRESS\", "
             + "  \"time\": " + (START_TIME + 20) + ", "
             + "  \"factory\": { "
             + "    \"city\": 0, "
             + "    \"index\": 1 "
             + "  }, "
             + "  \"progress\": 1.0 "
             + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 20) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  }, "
              + "  \"progress\": 1.0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"GROW_CITY\", "
              + "  \"time\": " + (START_TIME + 20) + ", "
              + "  \"city\": 0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"ACTION_FINISHED\", "
              + "  \"time\": " + (START_TIME + 20) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  } "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"ACTION_FINISHED\", "
              + "  \"time\": " + (START_TIME + 20) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 1 "
              + "  } "
              + "}")).readObject())), eventsGenerated);
    } else {
      assertEquals(Arrays.asList(), eventsGenerated);
    }
  }

  @Test
  public void testGrowWorks() throws Exception {
    runTestGrowWorks(true);
  }

  @Test
  public void testGrowWorksNoEvents() throws Exception {
    runTestGrowWorks(false);
  }

  /**
   * Runs the body of testGrowWithShringWorks() . Depending on the value of generatesEvents, either generates Events or
   * not.
   * @param generateEvents determines whether Events should be generated
   * @throws Exception when something unexpected happens
   */
  private void runTestGrowWithShrinkWorks(boolean generateEvents) throws Exception {
    cityA.grow();
    EventFactory eventFactoryArg;
    List<Event> eventsGeneratedArg;
    if (generateEvents) {
      eventFactoryArg = eventFactory;
      eventsGeneratedArg = eventsGenerated;
    } else {
      eventFactoryArg = null;
      eventsGeneratedArg = null;
    }
    assertEquals(2, cityA.getSize());
    assertEquals(CityFactory.State.ENABLED, cityAFactory0.getState());
    assertEquals(null, cityAFactory0.getAction());
    assertEquals(CityFactory.State.ENABLED, cityAFactory1.getState());
    assertEquals(null, cityAFactory1.getAction());
    assertEquals(CityFactory.State.DISABLED, cityAFactory2.getState());

    cityAFactory0.setAction(cityAFactory0.new GrowAction());
    assertEquals(CityFactory.State.ENABLED, cityAFactory0.getState());
    assertEquals(ActionState.INITIALIZED, cityAFactory0.getAction().getState());
    assertEquals(0.0, cityAFactory0.getAction().getProgress(), 0.0);
    cityAFactory1.setAction(cityAFactory1.new ProduceSettlersUnitAction());
    assertEquals(CityFactory.State.ENABLED, cityAFactory1.getState());
    assertEquals(ActionState.INITIALIZED, cityAFactory1.getAction().getState());
    assertEquals(0.0, cityAFactory1.getAction().getProgress(), 0.0);
    cityAFactory0.getAction().increaseProgressByUniversalDelta(
        1.1 / game.getGameProperties().getGrowActionProgressFactor(), eventFactoryArg, eventsGeneratedArg);
    assertEquals(2, cityA.getSize());
    assertEquals(CityFactory.State.ENABLED, cityAFactory0.getState());
    assertEquals(ActionState.INITIALIZED, cityAFactory0.getAction().getState());
    assertEquals(1.0, cityAFactory0.getAction().getProgress(), Constants.DOUBLE_DELTA);
    assertEquals(CityFactory.State.ENABLED, cityAFactory1.getState());
    assertEquals(ActionState.INITIALIZED, cityAFactory1.getAction().getState());
    assertEquals(0.0, cityAFactory1.getAction().getProgress(), 0.0);
    assertEquals(CityFactory.State.DISABLED, cityAFactory2.getState());
    if (generateEvents) {
      assertEquals(Arrays.asList(
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  }, "
              + "  \"progress\": 1.0 "
              + "}")).readObject())), eventsGenerated);
    }
    cityAFactory1.getAction().increaseProgressByUniversalDelta(
        1.0 / game.getGameProperties().getProduceActionProgressFactor(), eventFactoryArg, eventsGeneratedArg);
    assertEquals(1, cityA.getSize());
    assertEquals(CityFactory.State.ENABLED, cityAFactory0.getState());
    assertEquals(ActionState.INITIALIZED, cityAFactory0.getAction().getState());
    assertEquals(1.0, cityAFactory0.getAction().getProgress(), Constants.DOUBLE_DELTA);
    assertEquals(CityFactory.State.DISABLED, cityAFactory1.getState());
    assertEquals(CityFactory.State.DISABLED, cityAFactory2.getState());
    if (generateEvents) {
      assertEquals(Arrays.asList(
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  }, "
              + "  \"progress\": 1.0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 1 "
              + "  }, "
              + "  \"progress\": 1.0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"PRODUCE_SETTLERS_UNIT\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"id\": 0, "
              + "  \"position\": { "
              + "    \"roadSection\": 0, "
              + "    \"index\": 3 "
              + "  }, "
              + "  \"player\": 0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SHRINK_OR_DESTROY_CITY\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"city\": 0, "
              + "  \"factoryIndex\": 1 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"ACTION_FINISHED\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 1 "
              + "  } "
              + "}")).readObject())), eventsGenerated);
    } else {
      assertEquals(Arrays.asList(), eventsGenerated);
    }

    game.advanceTimeTo(START_TIME + 20);
    cityAFactory0.getAction().increaseProgressByUniversalDelta(0.0, eventFactoryArg, eventsGeneratedArg);
    assertEquals(2, cityA.getSize());
    assertEquals(CityFactory.State.ENABLED, cityAFactory0.getState());
    assertEquals(ActionState.FINISHED, cityAFactory0.getAction().getState());
    assertEquals(CityFactory.State.ENABLED, cityAFactory1.getState());
    assertNull(cityAFactory1.getAction());
    if (generateEvents) {
      assertEquals(Arrays.asList(
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  }, "
              + "  \"progress\": 1.0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 1 "
              + "  }, "
              + "  \"progress\": 1.0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"PRODUCE_SETTLERS_UNIT\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"id\": 0, "
              + "  \"position\": { "
              + "    \"roadSection\": 0, "
              + "    \"index\": 3 "
              + "  }, "
              + "  \"player\": 0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SHRINK_OR_DESTROY_CITY\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"city\": 0, "
              + "  \"factoryIndex\": 1 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"ACTION_FINISHED\", "
              + "  \"time\": " + (START_TIME + 10) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 1 "
              + "  } "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"SET_FACTORY_PROGRESS\", "
              + "  \"time\": " + (START_TIME + 20) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  }, "
              + "  \"progress\": 1.0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"GROW_CITY\", "
              + "  \"time\": " + (START_TIME + 20) + ", "
              + "  \"city\": 0 "
              + "}")).readObject()),
          new Event(Json.createReader(new StringReader(
              "{ "
              + "  \"type\": \"ACTION_FINISHED\", "
              + "  \"time\": " + (START_TIME + 20) + ", "
              + "  \"factory\": { "
              + "    \"city\": 0, "
              + "    \"index\": 0 "
              + "  } "
              + "}")).readObject())), eventsGenerated);
    } else {
      assertEquals(Arrays.asList(), eventsGenerated);
    }
  }

  @Test
  public void testGrowWithShrinkWorks() throws Exception {
    runTestGrowWithShrinkWorks(true);
  }

  @Test
  public void testGrowWithShrinkWorksNoEvents() throws Exception {
    runTestGrowWithShrinkWorks(false);
  }

}
