package cz.wie.najtmar.gop.board;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import static org.mockito.Mockito.mock;

import cz.wie.najtmar.gop.common.Point2D;
import cz.wie.najtmar.gop.game.Game;
import cz.wie.najtmar.gop.game.Player;
import cz.wie.najtmar.gop.game.User;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Properties;
import java.util.UUID;

public class JointTest {

  static final Game DUMMY_GAME = mock(Game.class);
  static final Player PLAYER_A = new Player(new User("playerA", new UUID(12345, 67890)), 0, DUMMY_GAME);
  static final Player PLAYER_B = new Player(new User("playerB", new UUID(98765, 43210)), 1, DUMMY_GAME);

  static Board BOARD;

  Joint joint00;
  Joint joint001;
  Joint joint30;
  Joint joint33;
  Joint joint60;
  RoadSection roadSection0;

  /**
   * setUpBeforeClass method.
   * @throws Exception when something unexpected happens
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    final Properties simpleBoardPropertiesSet = new Properties();
    simpleBoardPropertiesSet.load(JointTest.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/board/simple-board.properties"));
    BOARD = new Board(simpleBoardPropertiesSet);
  }

  /**
   * @throws java.lang.Exception when test cannot be set up
   */
  @Before
  public void setUp() throws Exception {
    joint00 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setName("joint00")
        .setCoordinatesPair(new Point2D.Double(0.0, 0.0))
        .build();
    joint001 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setName("joint001")
        .setCoordinatesPair(new Point2D.Double(0.0, 0.0))
        .build();
    joint30 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(1)
        .setName("joint30")
        .setCoordinatesPair(new Point2D.Double(3.0, 0.0))
        .build();
    joint33 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(3)
        .setName("joint33")
        .setCoordinatesPair(new Point2D.Double(3.0, 3.0))
        .build();
    joint60 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(4)
        .setName("")
        .setCoordinatesPair(new Point2D.Double(6.0, 0.0))
        .build();
    roadSection0 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(joint30, joint33)
        .build();
  }

  @Test
  public void testJointInitialized() {
    assertEquals(1, joint30.getIndex());
    assertEquals("joint30", joint30.getName());
    assertEquals(new Point2D.Double(3.0, 0.0), joint30.getCoordinatesPair());
    assertEquals("", joint60.getName());
  }

  @Test
  public void testComparisonWorks() {
    assertTrue(joint00.equals(joint00));
    assertTrue(joint00.equals(joint001));
    assertFalse(joint00.equals(joint30));
    assertFalse(joint00.equals(BOARD));
    assertEquals(0, joint00.hashCode());
    assertEquals(0, joint001.hashCode());
    assertEquals(1, joint30.hashCode());
  }

  @Test
  public void testDeterminingNormalizedPositionWorks() throws Exception {
    assertEquals(new Position(roadSection0, 3), joint33.getNormalizedPosition());
    assertEquals(null, joint00.getNormalizedPosition());
    final RoadSection roadSection1 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(joint00, joint30)
        .build();
    assertEquals(new Position(roadSection1, 0), joint00.getNormalizedPosition());
    assertEquals(roadSection1, joint00.getNormalizedPosition().getRoadSection());
    @SuppressWarnings("unused")
    final RoadSection roadSection2 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(1)
        .setJoints(joint00, joint33)
        .build();
    assertEquals(new Position(roadSection1, 0), joint00.getNormalizedPosition());
    assertEquals(roadSection1, joint00.getNormalizedPosition().getRoadSection());
  }

}
