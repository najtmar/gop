package cz.wie.najtmar.gop.game;

import cz.wie.najtmar.gop.board.DirectedPosition;
import cz.wie.najtmar.gop.board.Position;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;

/**
 * Non-abstract Prawn used in tests.
 * @author najtmar
 */
public class TestPrawn extends Prawn {

  public TestPrawn(@Nonnull GameProperties gameProperties, @Nonnull Player player, @Nonnull Position currentPosition,
      @Nonnegative long creationTime)
      throws GameException {
    super(gameProperties, player, currentPosition, creationTime);
  }

  @Override
  protected double calculateVelocity(@Nonnull DirectedPosition directedPosition) {
    return 0.0;
  }

  @Override
  public double calculateAttackStrength(DirectedPosition directedPosition) {
    return 0.0;
  }

}
