package cz.wie.najtmar.gop.board;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import cz.wie.najtmar.gop.board.RoadSection.Direction;
import cz.wie.najtmar.gop.common.Constants;
import cz.wie.najtmar.gop.common.Point2D;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;

public class RoadSectionTest {

  static Board BOARD;

  Joint joint11;
  Joint joint14;
  Joint joint21;
  Joint joint41;
  Joint joint4P1;
  Joint joint4M1;
  Joint joint44;

  /**
   * setUpBeforeClass method.
   * @throws Exception when something unexpected happens
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    final Properties simpleBoardPropertiesSet = new Properties();
    simpleBoardPropertiesSet.load(RoadSectionTest.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/board/simple-board.properties"));
    BOARD = new Board(simpleBoardPropertiesSet);
  }

  /**
   * setUp method.
   * @throws Exception when something unexpected happens
   */
  @Before
  public void setUp() throws Exception {
    joint11 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setName("joint11")
        .setCoordinatesPair(new Point2D.Double(1.0, 1.0))
        .build();
    joint14 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(1)
        .setName("joint14")
        .setCoordinatesPair(new Point2D.Double(1.0, 4.0))
        .build();
    joint21 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(2)
        .setName("joint21")
        .setCoordinatesPair(new Point2D.Double(2.0, 1.0))
        .build();
    joint41 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(3)
        .setName("joint41")
        .setCoordinatesPair(new Point2D.Double(4.0, 1.0))
        .build();
    joint4P1 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(4)
        .setName("joint4P1")
        .setCoordinatesPair(new Point2D.Double(4.0 + 0.1, 1.0))
        .build();
    joint4M1 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(5)
        .setName("joint4M1")
        .setCoordinatesPair(new Point2D.Double(4.0 - 0.1, 1.0))
        .build();
    joint44 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(6)
        .setName("joint44")
        .setCoordinatesPair(new Point2D.Double(4.0, 4.0))
        .build();
  }

  @Test
  public void testHorizontalLine() throws BoardException {
    assertEquals(new ArrayList<>(), joint11.getRoadSections());
    assertEquals(new ArrayList<>(), joint41.getRoadSections());
    RoadSection horizontalLine = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(joint11, joint41)
        .build();
    assertEquals(new ArrayList<>(Arrays.asList(horizontalLine)), joint11.getRoadSections());
    assertEquals(new ArrayList<>(Arrays.asList(horizontalLine)), joint41.getRoadSections());
    assertEquals(BOARD, horizontalLine.getBoard());
    assertEquals(0, horizontalLine.getIndex());
    assertArrayEquals(new Joint[]{joint11, joint41}, horizontalLine.getJoints());
    assertEquals(4, horizontalLine.getPositionsCount());
    assertEquals(3.0, horizontalLine.getLength(), 0.0);
    assertEquals(1.0, horizontalLine.getStepSize(), 0.0);
    assertArrayEquals(new int[]{0, 3}, horizontalLine.getJointPositionIndexes());
    assertArrayEquals(new int[]{1, 2}, horizontalLine.getJointAdjacentPointIndexes());
    assertEquals(2, horizontalLine.getMidpointIndex());
    assertEquals(0, horizontalLine.hashCode());

    assertEquals(RoadSection.Direction.BACKWARD, horizontalLine.getJointDirection(joint11));
    assertEquals(RoadSection.Direction.FORWARD, horizontalLine.getJointDirection(joint41));
    assertEquals(null, horizontalLine.getJointDirection(joint44));
    assertEquals(null, horizontalLine.getJointDirection(null));
  }

  @Test
  public void testHalfSectionsCreatedCorrectly() throws BoardException {
    assertEquals(new ArrayList<>(), joint11.getRoadSections());
    assertEquals(new ArrayList<>(), joint41.getRoadSections());
    final RoadSection roadSection0 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(joint11, joint41)
        .build();
    final RoadSection.Half[] halfSections0 = roadSection0.getHalves();
    assertEquals(2, halfSections0.length);
    assertEquals(roadSection0, halfSections0[0].getRoadSection());
    assertEquals(roadSection0, halfSections0[1].getRoadSection());
    assertEquals(RoadSection.Direction.BACKWARD, halfSections0[0].getEnd());
    assertEquals(RoadSection.Direction.FORWARD, halfSections0[1].getEnd());
    assertNotEquals(halfSections0[0], halfSections0[1]);
    assertNotEquals(halfSections0[0].hashCode(), halfSections0[1].hashCode());
    assertEquals(962, halfSections0[0].hashCode());
    assertEquals(961, halfSections0[1].hashCode());

    final RoadSection roadSection1 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(1)
        .setJoints(joint11, joint14)
        .build();
    final RoadSection.Half[] halfSections1 = roadSection1.getHalves();
    assertEquals(2, halfSections1.length);
    assertEquals(roadSection1, halfSections1[0].getRoadSection());
    assertEquals(roadSection1, halfSections1[1].getRoadSection());
    assertEquals(RoadSection.Direction.BACKWARD, halfSections1[0].getEnd());
    assertEquals(RoadSection.Direction.FORWARD, halfSections1[1].getEnd());
    assertNotEquals(halfSections1[0], halfSections1[1]);
    assertNotEquals(halfSections1[0].hashCode(), halfSections1[1].hashCode());
    assertEquals(993, halfSections1[0].hashCode());
    assertEquals(992, halfSections1[1].hashCode());

    assertNotEquals(halfSections0[0], halfSections1[0]);
    assertNotEquals(halfSections1[0], halfSections0[0]);
    assertNotEquals(halfSections0[1], halfSections1[1]);
    assertNotEquals(halfSections1[1], halfSections0[1]);
    assertNotEquals(halfSections0[0].hashCode(), halfSections1[0].hashCode());
    assertNotEquals(halfSections0[1].hashCode(), halfSections1[1].hashCode());
  }

  @Test
  public void testRoadSectionHalfContainCheckingWorks() throws Exception {
    final RoadSection roadSection = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(joint11, joint44)
        .build();
    // BACKWARD
    for (int i = 0; i <= 2; ++i) {
      assertTrue("Index " + i, roadSection.getHalves()[0].contains(new Position(roadSection, i)));
    }
    for (int i = 3; i <= roadSection.getJointPositionIndexes()[1]; ++i) {
      assertFalse("Index " + i, roadSection.getHalves()[0].contains(new Position(roadSection, i)));
    }
    // FORWARD
    for (int i = 0; i < 2; ++i) {
      assertFalse("Index " + i, roadSection.getHalves()[1].contains(new Position(roadSection, i)));
    }
    for (int i = 2; i <= roadSection.getJointPositionIndexes()[1]; ++i) {
      assertTrue("Index " + i, roadSection.getHalves()[1].contains(new Position(roadSection, i)));
    }

    final RoadSection roadSection1 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(1)
        .setJoints(joint11, joint14)
        .build();
    final RoadSection roadSection2 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(2)
        .setJoints(joint41, joint44)
        .build();
    assertTrue(roadSection.getHalves()[0].contains(new Position(roadSection1, 0)));
    assertFalse(roadSection.getHalves()[0].contains(new Position(roadSection1, 1)));
    assertFalse(roadSection.getHalves()[0].contains(new Position(roadSection1, 3)));
    assertFalse(roadSection.getHalves()[1].contains(new Position(roadSection1, 0)));
    assertTrue(roadSection.getHalves()[1].contains(new Position(roadSection2, 3)));
    assertFalse(roadSection.getHalves()[1].contains(new Position(roadSection2, 2)));
    assertFalse(roadSection.getHalves()[1].contains(new Position(roadSection2, 0)));
  }

  @Test
  public void testRoadSectionHalfAdjacencyCheckingWorks() throws Exception {
    final RoadSection roadSection0 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(joint11, joint41)
        .build();
    final RoadSection roadSection1 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(1)
        .setJoints(joint41, joint44)
        .build();
    final RoadSection roadSection2 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(2)
        .setJoints(joint11, joint44)
        .build();
    assertEquals(new HashSet<RoadSection.Half>(Arrays.asList(roadSection0.getHalves()[1], roadSection2.getHalves()[0])),
        roadSection0.getHalves()[0].getAdjacentHalves());
    assertEquals(new HashSet<RoadSection.Half>(Arrays.asList(roadSection0.getHalves()[0], roadSection1.getHalves()[0])),
        roadSection0.getHalves()[1].getAdjacentHalves());
    assertEquals(new HashSet<RoadSection.Half>(Arrays.asList(roadSection0.getHalves()[1], roadSection1.getHalves()[1])),
        roadSection1.getHalves()[0].getAdjacentHalves());
    assertEquals(new HashSet<RoadSection.Half>(Arrays.asList(roadSection1.getHalves()[0], roadSection2.getHalves()[1])),
        roadSection1.getHalves()[1].getAdjacentHalves());
    assertEquals(new HashSet<RoadSection.Half>(Arrays.asList(roadSection1.getHalves()[1], roadSection2.getHalves()[0])),
        roadSection2.getHalves()[1].getAdjacentHalves());
    assertEquals(new HashSet<RoadSection.Half>(Arrays.asList(roadSection0.getHalves()[0], roadSection2.getHalves()[1])),
        roadSection2.getHalves()[0].getAdjacentHalves());
  }

  @Test
  public void testTwoLines() throws BoardException {
    assertEquals(new ArrayList<>(), joint11.getRoadSections());
    assertEquals(new ArrayList<>(), joint14.getRoadSections());
    assertEquals(new ArrayList<>(), joint41.getRoadSections());
    RoadSection horizontalLine = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(joint11, joint41)
        .build();
    RoadSection verticalLine = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(joint11, joint14)
        .build();
    assertEquals(new ArrayList<>(Arrays.asList(horizontalLine, verticalLine)), joint11.getRoadSections());
    assertEquals(new ArrayList<>(Arrays.asList(verticalLine)), joint14.getRoadSections());
    assertEquals(new ArrayList<>(Arrays.asList(horizontalLine)), joint41.getRoadSections());
  }

  @Test
  public void testReverseHorizontalLine() throws BoardException {
    RoadSection reverseHorizontalLine = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(joint41, joint11)
        .build();
    assertEquals(BOARD, reverseHorizontalLine.getBoard());
    assertEquals(0, reverseHorizontalLine.getIndex());
    assertArrayEquals(new Joint[]{joint41, joint11}, reverseHorizontalLine.getJoints());
    assertEquals(4, reverseHorizontalLine.getPositionsCount());
    assertArrayEquals(new int[]{0, 3}, reverseHorizontalLine.getJointPositionIndexes());
    assertArrayEquals(new int[]{1, 2}, reverseHorizontalLine.getJointAdjacentPointIndexes());
    assertEquals(2, reverseHorizontalLine.getMidpointIndex());
  }

  @Test
  public void testVerticalLine() throws BoardException {
    RoadSection verticalLine = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(joint11, joint14)
        .build();
    assertEquals(BOARD, verticalLine.getBoard());
    assertEquals(0, verticalLine.getIndex());
    assertArrayEquals(new Joint[]{joint11, joint14}, verticalLine.getJoints());
    assertEquals(4, verticalLine.getPositionsCount());
    assertArrayEquals(new int[]{0, 3}, verticalLine.getJointPositionIndexes());
    assertArrayEquals(new int[]{1, 2}, verticalLine.getJointAdjacentPointIndexes());
    assertEquals(2, verticalLine.getMidpointIndex());
  }

  @Test
  public void testReverseVerticalLine() throws BoardException {
    RoadSection reverseVerticalLine = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(joint14, joint11)
        .build();
    assertEquals(BOARD, reverseVerticalLine.getBoard());
    assertEquals(0, reverseVerticalLine.getIndex());
    assertArrayEquals(new Joint[]{joint14, joint11}, reverseVerticalLine.getJoints());
    assertEquals(4, reverseVerticalLine.getPositionsCount());
    assertArrayEquals(new int[]{0, 3}, reverseVerticalLine.getJointPositionIndexes());
    assertArrayEquals(new int[]{1, 2}, reverseVerticalLine.getJointAdjacentPointIndexes());
    assertEquals(2, reverseVerticalLine.getMidpointIndex());
  }

  @Test
  public void testLongerLine() throws BoardException {
    RoadSection longerLine = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(joint11, joint4P1)
        .build();
    assertArrayEquals(new Joint[]{joint11, joint4P1}, longerLine.getJoints());
    assertEquals(3 + 1, longerLine.getPositionsCount());
    assertArrayEquals(new int[]{0, 3}, longerLine.getJointPositionIndexes());
    assertArrayEquals(new int[]{1, 2}, longerLine.getJointAdjacentPointIndexes());
    assertEquals(2, longerLine.getMidpointIndex());
  }

  @Test
  public void testShorterLine() throws BoardException {
    RoadSection shorterLine = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(joint11, joint4M1)
        .build();
    assertArrayEquals(new Joint[]{joint11, joint4M1}, shorterLine.getJoints());
    assertEquals(3 - 0, shorterLine.getPositionsCount());
    assertArrayEquals(new int[]{0, 2}, shorterLine.getJointPositionIndexes());
    assertArrayEquals(new int[]{1, 1}, shorterLine.getJointAdjacentPointIndexes());
    assertEquals(1, shorterLine.getMidpointIndex());
  }

  @Test
  public void testObliqueLine() throws BoardException {
    RoadSection obliqueLine = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(joint11, joint44)
        .build();
    assertArrayEquals(new Joint[]{joint11, joint44}, obliqueLine.getJoints());
    assertEquals(4 + 1, obliqueLine.getPositionsCount());  // length = ceil(sqrt(18))
    assertEquals(Math.sqrt(18.0), obliqueLine.getLength(), Constants.DOUBLE_DELTA);
    assertEquals(Math.sqrt(18.0) / ((4 + 1) - 1), obliqueLine.getStepSize(), Constants.DOUBLE_DELTA);
    assertArrayEquals(new int[]{0, 4}, obliqueLine.getJointPositionIndexes());
    assertArrayEquals(new int[]{1, 3}, obliqueLine.getJointAdjacentPointIndexes());
    assertEquals(2, obliqueLine.getMidpointIndex());
  }

  @Test
  public void testReverseObliqueLine() throws BoardException {
    RoadSection reverseObliqueLine = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(joint44, joint11)
        .build();
    assertArrayEquals(new Joint[]{joint44, joint11}, reverseObliqueLine.getJoints());
    assertEquals(4 + 1, reverseObliqueLine.getPositionsCount());  // length = ceil(sqrt(18))
    assertArrayEquals(new int[]{0, 4}, reverseObliqueLine.getJointPositionIndexes());
    assertArrayEquals(new int[]{1, 3}, reverseObliqueLine.getJointAdjacentPointIndexes());
    assertEquals(2, reverseObliqueLine.getMidpointIndex());
  }

  @Test
  public void testIllegalLineZeroLength() {
    try {
      (new RoadSection.Builder())
          .setBoard(BOARD)
          .setIndex(0)
          .setJoints(joint11, joint11)
          .build();
      fail("Should have thrown an exception.");
    } catch (BoardException exception) {
      assertEquals("A RoadSection must have a non-zero length.", exception.getMessage());
    }
  }

  @Test
  public void testIllegalLineTooShort() {
    try {
      (new RoadSection.Builder())
          .setBoard(BOARD)
          .setIndex(0)
          .setJoints(joint11, joint21)
          .build();
      fail("Should have thrown an exception.");
    } catch (BoardException exception) {
      assertTrue(exception.getMessage().contains("A RoadSection must have at least 3 positions."));
    }
  }

  @Test
  public void testComparisonWorks() throws BoardException {
    RoadSection roadSection0 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(joint11, joint41)
        .build();
    RoadSection roadSection01 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(joint11, joint41)
        .build();
    RoadSection roadSection1 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(1)
        .setJoints(joint11, joint44)
        .build();
    assertTrue(roadSection0.equals(roadSection0));
    assertTrue(roadSection0.equals(roadSection01));
    assertFalse(roadSection0.equals(roadSection1));
    assertFalse(roadSection0.equals(joint11));
    assertEquals(0, roadSection0.hashCode());
    assertEquals(0, roadSection01.hashCode());
    assertEquals(1, roadSection1.hashCode());
  }

  @Test
  public void testHorizontalLineAngles() throws BoardException {
    RoadSection horizontalLine = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(joint11, joint41)
        .build();
    assertEquals(0.0, horizontalLine.getAngle(), Constants.DOUBLE_DELTA);

    // FORWARD
    assertEquals(1.0, horizontalLine.calculateDirContrib(0.0, Direction.FORWARD), Constants.DOUBLE_DELTA);
    assertEquals(Math.sqrt(2.0) / 2.0, horizontalLine.calculateDirContrib(Math.PI / 4.0, Direction.FORWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(0.0, horizontalLine.calculateDirContrib(Math.PI / 2.0, Direction.FORWARD), Constants.DOUBLE_DELTA);
    assertEquals(0.0, horizontalLine.calculateDirContrib(3.0 * Math.PI / 4.0, Direction.FORWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(0.0, horizontalLine.calculateDirContrib(Math.PI, Direction.FORWARD), Constants.DOUBLE_DELTA);
    assertEquals(0.0, horizontalLine.calculateDirContrib(5.0 * Math.PI / 4.0, Direction.FORWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(0.0, horizontalLine.calculateDirContrib(3.0 * Math.PI / 2.0, Direction.FORWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(Math.sqrt(2.0) / 2.0, horizontalLine.calculateDirContrib(7.0 * Math.PI / 4.0, Direction.FORWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(1.0, horizontalLine.calculateDirContrib(2.0 * Math.PI, Direction.FORWARD), Constants.DOUBLE_DELTA);

    // BACKWARD
    assertEquals(0.0, horizontalLine.calculateDirContrib(0.0, Direction.BACKWARD), Constants.DOUBLE_DELTA);
    assertEquals(0.0, horizontalLine.calculateDirContrib(Math.PI / 4.0, Direction.BACKWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(0.0, horizontalLine.calculateDirContrib(Math.PI / 2.0, Direction.BACKWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(Math.sqrt(2.0) / 2.0, horizontalLine.calculateDirContrib(3.0 * Math.PI / 4.0, Direction.BACKWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(1.0, horizontalLine.calculateDirContrib(Math.PI, Direction.BACKWARD), Constants.DOUBLE_DELTA);
    assertEquals(Math.sqrt(2.0) / 2.0, horizontalLine.calculateDirContrib(5.0 * Math.PI / 4.0, Direction.BACKWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(0.0, horizontalLine.calculateDirContrib(3.0 * Math.PI / 2.0, Direction.BACKWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(0.0, horizontalLine.calculateDirContrib(7.0 * Math.PI / 4.0, Direction.BACKWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(0.0, horizontalLine.calculateDirContrib(2.0 * Math.PI, Direction.BACKWARD),
        Constants.DOUBLE_DELTA);
  }

  @Test
  public void testReverseHorizontalLineAngles() throws BoardException {
    RoadSection reverseHorizontalLine = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(joint41, joint11)
        .build();
    assertEquals(Math.PI, reverseHorizontalLine.getAngle(), Constants.DOUBLE_DELTA);

    // FORWARD
    assertEquals(0.0, reverseHorizontalLine.calculateDirContrib(0.0, Direction.FORWARD), Constants.DOUBLE_DELTA);
    assertEquals(0.0, reverseHorizontalLine.calculateDirContrib(Math.PI / 4.0, Direction.FORWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(0.0, reverseHorizontalLine.calculateDirContrib(Math.PI / 2.0, Direction.FORWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(Math.sqrt(2.0) / 2.0, reverseHorizontalLine.calculateDirContrib(3.0 * Math.PI / 4.0,
        Direction.FORWARD), Constants.DOUBLE_DELTA);
    assertEquals(1.0, reverseHorizontalLine.calculateDirContrib(Math.PI, Direction.FORWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(Math.sqrt(2.0) / 2.0, reverseHorizontalLine.calculateDirContrib(5.0 * Math.PI / 4.0,
        Direction.FORWARD), Constants.DOUBLE_DELTA);
    assertEquals(0.0, reverseHorizontalLine.calculateDirContrib(3.0 * Math.PI / 2.0, Direction.FORWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(0.0, reverseHorizontalLine.calculateDirContrib(7.0 * Math.PI / 4.0, Direction.FORWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(0.0, reverseHorizontalLine.calculateDirContrib(2.0 * Math.PI, Direction.FORWARD),
        Constants.DOUBLE_DELTA);

    // BACKWARD
    assertEquals(1.0, reverseHorizontalLine.calculateDirContrib(0.0, Direction.BACKWARD), Constants.DOUBLE_DELTA);
    assertEquals(Math.sqrt(2.0) / 2.0, reverseHorizontalLine.calculateDirContrib(Math.PI / 4.0, Direction.BACKWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(0.0, reverseHorizontalLine.calculateDirContrib(Math.PI / 2.0, Direction.BACKWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(0.0, reverseHorizontalLine.calculateDirContrib(3.0 * Math.PI / 4.0, Direction.BACKWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(0.0, reverseHorizontalLine.calculateDirContrib(Math.PI, Direction.BACKWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(0.0, reverseHorizontalLine.calculateDirContrib(5.0 * Math.PI / 4.0, Direction.BACKWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(0.0, reverseHorizontalLine.calculateDirContrib(3.0 * Math.PI / 2.0, Direction.BACKWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(Math.sqrt(2.0) / 2.0,
        reverseHorizontalLine.calculateDirContrib(7.0 * Math.PI / 4.0, Direction.BACKWARD), Constants.DOUBLE_DELTA);
    assertEquals(1.0, reverseHorizontalLine.calculateDirContrib(2.0 * Math.PI, Direction.BACKWARD),
        Constants.DOUBLE_DELTA);
  }

  @Test
  public void testVerticalLineAngles() throws BoardException {
    RoadSection verticalLine = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(joint11, joint14)
        .build();
    assertEquals(Math.PI / 2.0, verticalLine.getAngle(), Constants.DOUBLE_DELTA);

    // FORWARD
    assertEquals(0.0, verticalLine.calculateDirContrib(0.0, Direction.FORWARD), Constants.DOUBLE_DELTA);
    assertEquals(Math.sqrt(2.0) / 2.0, verticalLine.calculateDirContrib(Math.PI / 4.0, Direction.FORWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(1.0, verticalLine.calculateDirContrib(Math.PI / 2.0, Direction.FORWARD), Constants.DOUBLE_DELTA);
    assertEquals(Math.sqrt(2.0) / 2.0, verticalLine.calculateDirContrib(3.0 * Math.PI / 4.0, Direction.FORWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(0.0, verticalLine.calculateDirContrib(Math.PI, Direction.FORWARD), Constants.DOUBLE_DELTA);
    assertEquals(0.0, verticalLine.calculateDirContrib(5.0 * Math.PI / 4.0, Direction.FORWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(0.0, verticalLine.calculateDirContrib(3.0 * Math.PI / 2.0, Direction.FORWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(0.0, verticalLine.calculateDirContrib(7.0 * Math.PI / 4.0, Direction.FORWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(0.0, verticalLine.calculateDirContrib(2.0 * Math.PI, Direction.FORWARD), Constants.DOUBLE_DELTA);

    // BACKWARD
    assertEquals(0.0, verticalLine.calculateDirContrib(0.0, Direction.BACKWARD), Constants.DOUBLE_DELTA);
    assertEquals(0.0, verticalLine.calculateDirContrib(Math.PI / 4.0, Direction.BACKWARD), Constants.DOUBLE_DELTA);
    assertEquals(0.0, verticalLine.calculateDirContrib(Math.PI / 2.0, Direction.BACKWARD), Constants.DOUBLE_DELTA);
    assertEquals(0.0, verticalLine.calculateDirContrib(3.0 * Math.PI / 4.0, Direction.BACKWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(0.0, verticalLine.calculateDirContrib(Math.PI, Direction.BACKWARD), Constants.DOUBLE_DELTA);
    assertEquals(Math.sqrt(2.0) / 2.0, verticalLine.calculateDirContrib(5.0 * Math.PI / 4.0, Direction.BACKWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(1.0, verticalLine.calculateDirContrib(3.0 * Math.PI / 2.0, Direction.BACKWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(Math.sqrt(2.0) / 2.0, verticalLine.calculateDirContrib(7.0 * Math.PI / 4.0, Direction.BACKWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(0.0, verticalLine.calculateDirContrib(2.0 * Math.PI, Direction.BACKWARD),
        Constants.DOUBLE_DELTA);
  }

  @Test
  public void testReverseVerticalLineAngles() throws BoardException {
    RoadSection reverseVerticalLine = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(joint14, joint11)
        .build();
    assertEquals(3.0 * Math.PI / 2.0, reverseVerticalLine.getAngle(), Constants.DOUBLE_DELTA);
  }

  @Test
  public void testObliqueLineAngles() throws BoardException {
    RoadSection obliqueLine = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(joint11, joint44)
        .build();
    assertEquals(Math.PI / 4.0, obliqueLine.getAngle(), Constants.DOUBLE_DELTA);
  }

  @Test
  public void testReverseObliqueLineAngles() throws BoardException {
    RoadSection reverseObliqueLine = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(joint44, joint11)
        .build();
    assertEquals(5.0 * Math.PI / 4.0, reverseObliqueLine.getAngle(), Constants.DOUBLE_DELTA);

    // FORWARD
    assertEquals(0.0, reverseObliqueLine.calculateDirContrib(0.0, Direction.FORWARD), Constants.DOUBLE_DELTA);
    assertEquals(0.0, reverseObliqueLine.calculateDirContrib(Math.PI / 4.0, Direction.FORWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(0.0, reverseObliqueLine.calculateDirContrib(Math.PI / 2.0, Direction.FORWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(0.0, reverseObliqueLine.calculateDirContrib(3.0 * Math.PI / 4.0, Direction.FORWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(Math.sqrt(2.0) / 2.0, reverseObliqueLine.calculateDirContrib(Math.PI, Direction.FORWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(1.0, reverseObliqueLine.calculateDirContrib(5.0 * Math.PI / 4.0, Direction.FORWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(Math.sqrt(2.0) / 2.0, reverseObliqueLine.calculateDirContrib(3.0 * Math.PI / 2.0, Direction.FORWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(0.0, reverseObliqueLine.calculateDirContrib(7.0 * Math.PI / 4.0, Direction.FORWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(0.0, reverseObliqueLine.calculateDirContrib(2.0 * Math.PI, Direction.FORWARD),
        Constants.DOUBLE_DELTA);

    // BACKWARD
    assertEquals(Math.sqrt(2.0) / 2.0, reverseObliqueLine.calculateDirContrib(0.0, Direction.BACKWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(1.0, reverseObliqueLine.calculateDirContrib(Math.PI / 4.0, Direction.BACKWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(Math.sqrt(2.0) / 2.0, reverseObliqueLine.calculateDirContrib(Math.PI / 2.0, Direction.BACKWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(0.0, reverseObliqueLine.calculateDirContrib(3.0 * Math.PI / 4.0, Direction.BACKWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(0.0, reverseObliqueLine.calculateDirContrib(Math.PI, Direction.BACKWARD), Constants.DOUBLE_DELTA);
    assertEquals(0.0, reverseObliqueLine.calculateDirContrib(5.0 * Math.PI / 4.0, Direction.BACKWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(0.0, reverseObliqueLine.calculateDirContrib(3.0 * Math.PI / 2.0, Direction.BACKWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(0.0, reverseObliqueLine.calculateDirContrib(7.0 * Math.PI / 4.0, Direction.BACKWARD),
        Constants.DOUBLE_DELTA);
    assertEquals(Math.sqrt(2.0) / 2.0, reverseObliqueLine.calculateDirContrib(2.0 * Math.PI, Direction.BACKWARD),
        Constants.DOUBLE_DELTA);
  }

  @Test
  public void testAdjacentToJointWorks() throws BoardException {
    RoadSection roadSection = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(joint11, joint41)
        .build();
    assertTrue(roadSection.adjacentTo(joint11));
    assertTrue(roadSection.adjacentTo(joint41));
    assertFalse(roadSection.adjacentTo(joint14));
  }

  @Test
  public void testGetJointIndexWorks() throws BoardException {
    RoadSection roadSection = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(joint11, joint41)
        .build();
    assertEquals(0, roadSection.getJointIndex(joint11));
    assertEquals(3, roadSection.getJointIndex(joint41));
    try {
      roadSection.getJointIndex(joint14);
      fail("Should have thrown an exception.");
    } catch (BoardException exception) {
      assertEquals("Joint Point2D.Double[1.0, 4.0] is not on "
          + "(0, (Point2D.Double[1.0, 1.0], Point2D.Double[4.0, 1.0]), 4, 0.0) .", exception.getMessage());
    }
  }

  @Test
  public void testPositionProjectionWorks() throws BoardException {
    RoadSection roadSection0 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(joint11, joint41)
        .build();
    RoadSection roadSection1 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(1)
        .setJoints(joint41, joint44)
        .build();
    RoadSection roadSection2 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(2)
        .setJoints(joint11, joint44)
        .build();
    // Same RoadSection.
    {
      Position position = roadSection0.getProjectedPosition(new Position(roadSection0, 0));
      assertTrue(position.equals(new Position(roadSection0, 0)));
      assertTrue(position.getRoadSection().equals(roadSection0));
    }
    {
      Position position = roadSection0.getProjectedPosition(new Position(roadSection0, 3));
      assertTrue(position.equals(new Position(roadSection0, 3)));
      assertTrue(position.getRoadSection().equals(roadSection0));
    }
    {
      Position position = roadSection0.getProjectedPosition(new Position(roadSection0, 1));
      assertTrue(position.equals(new Position(roadSection0, 1)));
      assertTrue(position.getRoadSection().equals(roadSection0));
    }

    // Different RoadSections.
    {
      Position position = roadSection0.getProjectedPosition(new Position(roadSection2, 0));
      assertTrue(position.equals(new Position(roadSection2, 0)));
      assertTrue(position.getRoadSection().equals(roadSection0));
    }
    {
      Position position = roadSection2.getProjectedPosition(new Position(roadSection0, 0));
      assertTrue(position.equals(new Position(roadSection0, 0)));
      assertTrue(position.getRoadSection().equals(roadSection2));
    }
    {
      Position position = roadSection0.getProjectedPosition(new Position(roadSection1, 0));
      assertTrue(position.equals(new Position(roadSection1, 0)));
      assertTrue(position.getRoadSection().equals(roadSection0));
    }
    {
      Position position = roadSection1.getProjectedPosition(new Position(roadSection0, 3));
      assertTrue(position.equals(new Position(roadSection0, 3)));
      assertTrue(position.getRoadSection().equals(roadSection1));
    }
    {
      Position position = roadSection1.getProjectedPosition(new Position(roadSection2, 4));
      assertTrue(position.equals(new Position(roadSection2, 4)));
      assertTrue(position.getRoadSection().equals(roadSection1));
    }
    {
      Position position = roadSection2.getProjectedPosition(new Position(roadSection1, 3));
      assertTrue(position.equals(new Position(roadSection1, 3)));
      assertTrue(position.getRoadSection().equals(roadSection2));
    }
    assertEquals(null, roadSection0.getProjectedPosition(new Position(roadSection1, 1)));
    assertEquals(null, roadSection1.getProjectedPosition(new Position(roadSection0, 2)));
    assertEquals(null, roadSection0.getProjectedPosition(new Position(roadSection1, 3)));
    assertEquals(null, roadSection1.getProjectedPosition(new Position(roadSection0, 0)));
  }

}
