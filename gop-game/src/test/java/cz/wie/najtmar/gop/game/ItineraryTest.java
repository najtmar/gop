package cz.wie.najtmar.gop.game;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import cz.wie.najtmar.gop.board.Board;
import cz.wie.najtmar.gop.board.DirectedPosition;
import cz.wie.najtmar.gop.board.Joint;
import cz.wie.najtmar.gop.board.Position;
import cz.wie.najtmar.gop.board.RoadSection;
import cz.wie.najtmar.gop.common.Point2D;
import cz.wie.najtmar.gop.game.Itinerary.ItineraryBuildingException;

import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;
import java.util.Properties;

public class ItineraryTest {

  Board board;
  Joint joint00;
  Joint joint30;
  Joint joint33;
  Joint joint03;
  RoadSection roadSection0;
  RoadSection roadSection1;
  RoadSection roadSection2;
  RoadSection roadSection3;

  /**
   * @throws java.lang.Exception when field creation fails
   */
  @Before
  public void setUp() throws Exception {
    final Properties simpleBoardPropertiesSet = new Properties();
    simpleBoardPropertiesSet.load(ItineraryTest.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/board/simple-board.properties"));
    board = new Board(simpleBoardPropertiesSet);
    joint00 = (new Joint.Builder())
        .setBoard(board)
        .setIndex(0)
        .setName("joint00")
        .setCoordinatesPair(new Point2D.Double(0.0, 0.0))
        .build();
    joint30 = (new Joint.Builder())
        .setBoard(board)
        .setIndex(1)
        .setName("joint30")
        .setCoordinatesPair(new Point2D.Double(3.0, 0.0))
        .build();
    joint33 = (new Joint.Builder())
        .setBoard(board)
        .setIndex(2)
        .setName("joint33")
        .setCoordinatesPair(new Point2D.Double(3.0, 3.0))
        .build();
    joint03 = (new Joint.Builder())
        .setBoard(board)
        .setIndex(3)
        .setName("joint03")
        .setCoordinatesPair(new Point2D.Double(0.0, 3.0))
        .build();
    roadSection0 = (new RoadSection.Builder())
        .setBoard(board)
        .setIndex(0)
        .setJoints(joint00, joint30)
        .build();
    roadSection1 = (new RoadSection.Builder())
        .setBoard(board)
        .setIndex(1)
        .setJoints(joint33, joint30)
        .build();
    roadSection2 = (new RoadSection.Builder())
        .setBoard(board)
        .setIndex(2)
        .setJoints(joint33, joint03)
        .build();
    roadSection3 = (new RoadSection.Builder())
        .setBoard(board)
        .setIndex(3)
        .setJoints(joint00, joint03)
        .build();
  }

  @Test
  public void testOneRoadSectionItineraryBuilt() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(roadSection0, 1))
        .addPosition(new Position(roadSection0, 2))
        .build();
    Itinerary.Iterator iter = itinerary.itineraryIterator();
    assertTrue(iter.hasNext());
    assertFalse(iter.hasPrevious());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 2, RoadSection.Direction.FORWARD)));
    assertFalse(iter.hasNext());
    assertTrue(iter.hasPrevious());
    assertTrue(iter.previous().equals(new DirectedPosition(roadSection0, 2, RoadSection.Direction.FORWARD)));
  }

  @Test
  public void testStandardIteratorWorks() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(roadSection0, 1))
        .addPosition(new Position(roadSection0, 2))
        .build();
    Iterator<DirectedPosition> iter = itinerary.iterator();
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 2, RoadSection.Direction.FORWARD)));
    assertFalse(iter.hasNext());
  }

  @Test
  public void testIntermediateToIntermediateItineraryBuilt() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(roadSection0, 1))
        .addPosition(new Position(roadSection1, 3))
        .addPosition(new Position(roadSection2, 0))
        .addPosition(new Position(roadSection2, 2))
        .build();
    Iterator<DirectedPosition> iter = itinerary.iterator();
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 2, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 3, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 2, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 1, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 0, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 2, RoadSection.Direction.FORWARD)));
    assertFalse(iter.hasNext());
  }

  @Test
  public void testRemovingSuperflousPositionsWorks2() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(roadSection0, 1))
        .addPosition(new Position(roadSection0, 2))
        .addPosition(new Position(roadSection1, 3))
        .addPosition(new Position(roadSection2, 0))
        .addPosition(new Position(roadSection2, 2))
        .build();
    Iterator<DirectedPosition> iter = itinerary.iterator();
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 2, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 3, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 2, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 1, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 0, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 2, RoadSection.Direction.FORWARD)));
    assertFalse(iter.hasNext());
  }

  @Test
  public void testStartingToIntermediateItineraryBuilt() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(roadSection0, 0))
        .addPosition(new Position(roadSection1, 3))
        .addPosition(new Position(roadSection2, 0))
        .addPosition(new Position(roadSection2, 2))
        .build();
    Iterator<DirectedPosition> iter = itinerary.iterator();
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 0, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 2, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 3, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 2, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 1, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 0, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 2, RoadSection.Direction.FORWARD)));
    assertFalse(iter.hasNext());
  }

  @Test
  public void testRemovingSuperflousPositionsWorks3() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(roadSection0, 0))
        .addPosition(new Position(roadSection1, 3))
        .addPosition(new Position(roadSection1, 1))
        .addPosition(new Position(roadSection2, 0))
        .addPosition(new Position(roadSection2, 2))
        .build();
    Iterator<DirectedPosition> iter = itinerary.iterator();
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 0, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 2, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 3, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 2, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 1, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 0, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 2, RoadSection.Direction.FORWARD)));
    assertFalse(iter.hasNext());
  }

  @Test
  public void testRemovingSuperflousPositionsWorks4() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(roadSection0, 0))
        .addPosition(new Position(roadSection0, 3))
        .addPosition(new Position(roadSection1, 1))
        .addPosition(new Position(roadSection2, 0))
        .addPosition(new Position(roadSection2, 2))
        .build();
    Iterator<DirectedPosition> iter = itinerary.iterator();
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 0, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 2, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 3, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 2, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 1, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 0, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 2, RoadSection.Direction.FORWARD)));
    assertFalse(iter.hasNext());
  }

  @Test
  public void testRemovingLastWorks() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(roadSection0, 0))
        .addPosition(new Position(roadSection1, 3))
        .addPosition(new Position(roadSection1, 1))
        .removeLastPosition()
        .removeLastPosition()
        .addPosition(new Position(roadSection0, 3))
        .addPosition(new Position(roadSection1, 1))
        .addPosition(new Position(roadSection2, 0))
        .addPosition(new Position(roadSection2, 2))
        .build();
    Iterator<DirectedPosition> iter = itinerary.iterator();
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 0, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 2, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 3, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 2, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 1, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 0, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 2, RoadSection.Direction.FORWARD)));
    assertFalse(iter.hasNext());
  }

  @Test
  public void testRemovingLastWorks1() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(roadSection0, 0))
        .addPosition(new Position(roadSection0, 3))
        .addPosition(new Position(roadSection1, 1))
        .removeLastPosition()
        .removeLastPosition()
        .addPosition(new Position(roadSection1, 3))
        .addPosition(new Position(roadSection1, 1))
        .addPosition(new Position(roadSection2, 0))
        .addPosition(new Position(roadSection2, 2))
        .build();
    Iterator<DirectedPosition> iter = itinerary.iterator();
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 0, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 2, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 3, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 2, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 1, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 0, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 2, RoadSection.Direction.FORWARD)));
    assertFalse(iter.hasNext());
  }

  @Test
  public void testRemovingLastWorks2() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(roadSection0, 1))
        .addPosition(new Position(roadSection1, 3))
        .addPosition(new Position(roadSection2, 0))
        .addPosition(new Position(roadSection2, 2))
        .removeLastPosition()
        .build();
    Iterator<DirectedPosition> iter = itinerary.iterator();
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 2, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 3, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 2, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 1, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 0, RoadSection.Direction.BACKWARD)));
    assertFalse(iter.hasNext());
  }

  @Test
  public void testRemovingLastWorks3() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(roadSection0, 1))
        .addPosition(new Position(roadSection1, 3))
        .addPosition(new Position(roadSection1, 0))
        .addPosition(new Position(roadSection2, 2))
        .removeLastPosition()
        .build();
    Iterator<DirectedPosition> iter = itinerary.iterator();
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 2, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 3, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 2, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 1, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 0, RoadSection.Direction.BACKWARD)));
    assertFalse(iter.hasNext());
  }

  @Test
  public void testStarting2ToIntermediateItineraryBuilt() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(roadSection1, 3))
        .addPosition(new Position(roadSection2, 0))
        .addPosition(new Position(roadSection2, 2))
        .build();
    Iterator<DirectedPosition> iter = itinerary.iterator();
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 3, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 2, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 1, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 0, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 2, RoadSection.Direction.FORWARD)));
    assertFalse(iter.hasNext());
  }

  @Test
  public void testIntermediateToEndingItineraryBuilt() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(roadSection0, 1))
        .addPosition(new Position(roadSection1, 3))
        .addPosition(new Position(roadSection2, 0))
        .addPosition(new Position(roadSection2, 3))
        .build();
    Iterator<DirectedPosition> iter = itinerary.iterator();
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 2, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 3, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 2, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 1, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 0, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 2, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 3, RoadSection.Direction.FORWARD)));
    assertFalse(iter.hasNext());
  }

  @Test
  public void testIntermediateToEnding1ItineraryBuilt() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(roadSection0, 1))
        .addPosition(new Position(roadSection1, 3))
        .addPosition(new Position(roadSection1, 0))
        .build();
    Iterator<DirectedPosition> iter = itinerary.iterator();
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 2, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 3, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 2, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 1, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 0, RoadSection.Direction.BACKWARD)));
    assertFalse(iter.hasNext());
  }

  @Test
  public void testCyclicItineraryBuilt() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(roadSection0, 0))
        .addPosition(new Position(roadSection1, 3))
        .addPosition(new Position(roadSection2, 0))
        .addPosition(new Position(roadSection3, 3))
        .addPosition(new Position(roadSection3, 0))
        .build();
    Iterator<DirectedPosition> iter = itinerary.iterator();
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 0, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 2, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 3, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 2, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 1, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 0, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 2, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection3, 3, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection3, 2, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection3, 1, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection3, 0, RoadSection.Direction.BACKWARD)));
    assertFalse(iter.hasNext());
  }

  @Test
  public void testThereAndBackItineraryBuilt() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(roadSection0, 0))
        .addPosition(new Position(roadSection0, 3))
        .addPosition(new Position(roadSection0, 1))
        .build();
    Iterator<DirectedPosition> iter = itinerary.iterator();
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 0, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 2, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 3, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 2, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 1, RoadSection.Direction.BACKWARD)));
    assertFalse(iter.hasNext());
  }

  @Test
  public void testThereAndBackItinerary1Built() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(roadSection0, 0))
        .addPosition(new Position(roadSection0, 2))
        .addPosition(new Position(roadSection0, 1))
        .build();
    Iterator<DirectedPosition> iter = itinerary.iterator();
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 0, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 2, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    DirectedPosition nextPosition = iter.next();
    assertTrue(nextPosition.equals(new DirectedPosition(roadSection0, 1, RoadSection.Direction.BACKWARD)));
    assertFalse(iter.hasNext());
  }

  @Test
  public void testOneNodeItineraryFails() throws Exception {
    try {
      (new Itinerary.Builder())
        .addPosition(new Position(roadSection0, 0))
        .build();
      fail("Should have thrown an exception.");
    } catch (ItineraryBuildingException exception) {
      assertEquals("Too few Positions specified to build an Itinerary (1).", exception.getMessage());
    }
  }

  @Test
  public void testPositionProjectionHelps() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(roadSection0, 0))
        .addPosition(new Position(roadSection1, 3))
        .addPosition(new Position(roadSection2, 0))
        .addPosition(new Position(roadSection2, 1))
        .build();
    Iterator<DirectedPosition> iter = itinerary.iterator();
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 0, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 2, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 3, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 2, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 1, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 0, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    DirectedPosition nextPosition = iter.next();
    assertTrue(nextPosition.equals(new DirectedPosition(roadSection2, 1, RoadSection.Direction.FORWARD)));
    assertFalse(iter.hasNext());
  }

  @Test
  public void testPositionProjectionHelps1() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(roadSection0, 0))
        .addPosition(new Position(roadSection1, 3))
        .addPosition(new Position(roadSection2, 0))
        .addPosition(new Position(roadSection3, 3))
        .addPosition(new Position(roadSection3, 0))
        .build();
    Iterator<DirectedPosition> iter = itinerary.iterator();
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 0, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 2, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 3, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 2, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 1, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 0, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 2, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection3, 3, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection3, 2, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection3, 1, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection3, 0, RoadSection.Direction.BACKWARD)));
    assertFalse(iter.hasNext());
  }

  @Test
  public void testSamePositionTwiceIgnored() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(roadSection0, 3))
        .addPosition(new Position(roadSection1, 3))
        .addPosition(new Position(roadSection2, 0))
        .addPosition(new Position(roadSection3, 3))
        .addPosition(new Position(roadSection3, 0))
        .build();
    Iterator<DirectedPosition> iter = itinerary.iterator();
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 3, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 2, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 1, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 0, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 2, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection3, 3, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection3, 2, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection3, 1, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection3, 0, RoadSection.Direction.BACKWARD)));
    assertFalse(iter.hasNext());
  }

  @Test
  public void testSamePositionTwiceIgnored1() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(roadSection1, 0))
        .addPosition(new Position(roadSection2, 0))
        .addPosition(new Position(roadSection3, 3))
        .addPosition(new Position(roadSection3, 0))
        .build();
    Iterator<DirectedPosition> iter = itinerary.iterator();
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 0, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 2, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection3, 3, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection3, 2, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection3, 1, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection3, 0, RoadSection.Direction.BACKWARD)));
    assertFalse(iter.hasNext());
  }

  @Test
  public void testOneRoadSectionBackwardItineraryBuilt() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(roadSection0, 2))
        .addPosition(new Position(roadSection0, 1))
        .build();
    Iterator<DirectedPosition> iter = itinerary.iterator();
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 2, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 1, RoadSection.Direction.BACKWARD)));
    assertFalse(iter.hasNext());
  }

  @Test
  public void testNonContinuousItineraryFails() throws Exception {
    try {
      (new Itinerary.Builder())
          .addPosition(new Position(roadSection0, 1))
          .addPosition(new Position(roadSection1, 2))
          .addPosition(new Position(roadSection1, 1))
          .build();
      fail("Should have thrown an exception.");
    } catch (ItineraryBuildingException exception) {
      assertEquals("Invalid Position pair: ("
          + "Position 0: ((0, (Point2D.Double[0.0, 0.0], Point2D.Double[3.0, 0.0]), 4, 0.0), 1), "
          + "Position 1: ((1, (Point2D.Double[3.0, 3.0], Point2D.Double[3.0, 0.0]), 4, 4.71238898038469), 2)).",
          exception.getMessage());
    }
  }

  @Test
  public void testNonContinuousItineraryFails1() throws Exception {
    try {
      (new Itinerary.Builder())
          .addPosition(new Position(roadSection0, 1))
          .addPosition(new Position(roadSection1, 3))
          .addPosition(new Position(roadSection2, 1))
          .build();
      fail("Should have thrown an exception.");
    } catch (ItineraryBuildingException exception) {
      assertEquals("Invalid Position pair: ("
          + "Position 1: ((0, (Point2D.Double[0.0, 0.0], Point2D.Double[3.0, 0.0]), 4, 0.0), 3), "
          + "Position 2: ((2, (Point2D.Double[3.0, 3.0], Point2D.Double[0.0, 3.0]), 4, 3.141592653589793), 1)).",
          exception.getMessage());
    }
  }

  @Test
  public void testRemovingSuperflousPositionsWorks() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(roadSection0, 0))
        .addPosition(new Position(roadSection0, 1))
        .addPosition(new Position(roadSection0, 2))
        .addPosition(new Position(roadSection0, 3))
        .build();
    Iterator<DirectedPosition> iter = itinerary.iterator();
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 0, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 2, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 3, RoadSection.Direction.FORWARD)));
    assertFalse(iter.hasNext());
  }

  @Test
  public void testRemovingSuperflousPositionsWorks1() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(roadSection0, 3))
        .addPosition(new Position(roadSection0, 2))
        .addPosition(new Position(roadSection0, 1))
        .addPosition(new Position(roadSection0, 0))
        .build();
    Iterator<DirectedPosition> iter = itinerary.iterator();
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 3, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 2, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 1, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 0, RoadSection.Direction.BACKWARD)));
    assertFalse(iter.hasNext());
  }

  @Test
  public void testPositionProjectionHelps2() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(roadSection0, 0))
        .addPosition(new Position(roadSection1, 3))
        .addPosition(new Position(roadSection2, 0))
        .addPosition(new Position(roadSection3, 3))
        .build();
    Iterator<DirectedPosition> iter = itinerary.iterator();
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 0, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 2, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 3, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 2, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 1, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 0, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 2, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 3, RoadSection.Direction.FORWARD)));
    assertFalse(iter.hasNext());
  }

  @Test
  public void testZeroLengthItineraryFails() throws Exception {
    try {
      (new Itinerary.Builder())
          .addPosition(new Position(roadSection0, 1))
          .addPosition(new Position(roadSection0, 1))
          .build();
      fail("Should have thrown an exception.");
    } catch (ItineraryBuildingException exception) {
      assertEquals("Too few Positions specified to build an Itinerary (1).", exception.getMessage());
    }
  }

  @Test
  public void testSamePositionTwiceIgnored2() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(roadSection0, 0))
        .addPosition(new Position(roadSection1, 3))
        .addPosition(new Position(roadSection2, 0))
        .addPosition(new Position(roadSection3, 3))
        .addPosition(new Position(roadSection3, 3))
        .build();
    Iterator<DirectedPosition> iter = itinerary.iterator();
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 0, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection0, 2, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 3, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 2, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection1, 1, RoadSection.Direction.BACKWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 0, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 1, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 2, RoadSection.Direction.FORWARD)));
    assertTrue(iter.hasNext());
    assertTrue(iter.next().equals(new DirectedPosition(roadSection2, 3, RoadSection.Direction.FORWARD)));
    assertFalse(iter.hasNext());
  }

  @Test
  public void testSettingCurrentPositionWorks() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(roadSection0, 0))
        .addPosition(new Position(roadSection1, 3))
        .addPosition(new Position(roadSection1, 0))
        .build();
    Itinerary.Iterator iter = itinerary.itineraryIterator();
    try {
      itinerary.setCurrentPosition(iter);
      fail("Should have thrown an exception.");
    } catch (GameException exception) {
      assertEquals("Iterator should have called next() at least once.", exception.getMessage());
    }
    assertFalse(itinerary.isFinished());
    iter.next();
    itinerary.setCurrentPosition(iter);
    assertTrue(itinerary.getCurrentPosition().equals(
        new DirectedPosition(roadSection0, 0, RoadSection.Direction.FORWARD)));
    iter.next();
    iter.next();
    assertTrue(itinerary.getCurrentPosition().equals(
        new DirectedPosition(roadSection0, 0, RoadSection.Direction.FORWARD)));
    itinerary.setCurrentPosition(iter);
    assertTrue(itinerary.getCurrentPosition().equals(
        new DirectedPosition(roadSection0, 2, RoadSection.Direction.FORWARD)));
    iter.next();
    itinerary.setCurrentPosition(iter);
    assertTrue(itinerary.getCurrentPosition().equals(
        new DirectedPosition(roadSection1, 3, RoadSection.Direction.BACKWARD)));
    iter.next();
    assertTrue(itinerary.getCurrentPosition().equals(
        new DirectedPosition(roadSection1, 3, RoadSection.Direction.BACKWARD)));
    itinerary.setCurrentPosition(iter);
    assertTrue(itinerary.getCurrentPosition().equals(
        new DirectedPosition(roadSection1, 2, RoadSection.Direction.BACKWARD)));
    iter.next();
    iter.next();
    assertFalse(itinerary.isFinished());
    itinerary.setCurrentPosition(iter);
    assertTrue(itinerary.isFinished());
    assertTrue(itinerary.getCurrentPosition().equals(
        new DirectedPosition(roadSection1, 0, RoadSection.Direction.BACKWARD)));
    assertFalse(iter.hasNext());
  }

  @Test
  public void testSettingPreviousPositionWorks() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(roadSection0, 0))
        .addPosition(new Position(roadSection1, 3))
        .addPosition(new Position(roadSection1, 0))
        .build();
    Itinerary.Iterator iter = itinerary.itineraryIterator();
    try {
      itinerary.setPreviousPosition(iter);
      fail("Should have thrown an exception.");
    } catch (GameException exception) {
      assertEquals("Iterator should have called next() at least two times.", exception.getMessage());
    }
    iter.next();
    try {
      itinerary.setPreviousPosition(iter);
      fail("Should have thrown an exception.");
    } catch (GameException exception) {
      assertEquals("Iterator should have called next() at least two times.", exception.getMessage());
    }
    iter.next();
    itinerary.setPreviousPosition(iter);
    assertTrue(itinerary.getCurrentPosition().equals(
        new DirectedPosition(roadSection0, 0, RoadSection.Direction.FORWARD)));
    iter.next();
    iter.next();
    assertTrue(itinerary.getCurrentPosition().equals(
        new DirectedPosition(roadSection0, 0, RoadSection.Direction.FORWARD)));
    itinerary.setPreviousPosition(iter);
    assertTrue(itinerary.getCurrentPosition().equals(
        new DirectedPosition(roadSection0, 2, RoadSection.Direction.FORWARD)));
    iter.next();
    itinerary.setPreviousPosition(iter);
    assertTrue(itinerary.getCurrentPosition().equals(
        new DirectedPosition(roadSection1, 3, RoadSection.Direction.BACKWARD)));
    iter.next();
    assertTrue(itinerary.getCurrentPosition().equals(
        new DirectedPosition(roadSection1, 3, RoadSection.Direction.BACKWARD)));
    itinerary.setPreviousPosition(iter);
    assertTrue(itinerary.getCurrentPosition().equals(
        new DirectedPosition(roadSection1, 2, RoadSection.Direction.BACKWARD)));
    iter.next();
    assertFalse(iter.hasNext());
  }

}
