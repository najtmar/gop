package cz.wie.najtmar.gop.game;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import cz.wie.najtmar.gop.board.BoardException;
import cz.wie.najtmar.gop.board.Position;
import cz.wie.najtmar.gop.board.RoadSection;
import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.event.EventProcessingException;
import cz.wie.najtmar.gop.game.Battle;
import cz.wie.najtmar.gop.game.City;
import cz.wie.najtmar.gop.game.Move;
import cz.wie.najtmar.gop.game.ObjectSerializer;
import cz.wie.najtmar.gop.game.Player;
import cz.wie.najtmar.gop.game.Prawn;

import org.junit.Before;
import org.junit.Test;

import java.io.StringReader;

import javax.json.Json;

public class EventFactoryTest {

  Game mockedGame;
  ObjectSerializer mockedObjectSerializer;
  EventFactory eventFactory;

  /**
   * Method setUp().
   * @throws Exception when something goes wrong
   */
  @Before
  public void setUp() throws Exception {
    mockedGame = mock(Game.class);
    mockedObjectSerializer = mock(ObjectSerializer.class);
    when(mockedObjectSerializer.getGame()).thenReturn(mockedGame);
    when(mockedGame.getTime()).thenReturn(123456L);
    eventFactory = new EventFactory(mockedObjectSerializer);
  }

  @Test
  public void testInitializeGameEventCreationWorks() throws EventProcessingException, BoardException {
    when(mockedObjectSerializer.serializeGameWithoutPrawnsOrCities()).thenReturn(
        Json.createReader(new StringReader(
            "{ "
            + "  \"board\": { "
            + "    \"properties\": [ "
            + "      { "
            + "        \"key\": \"Board.ysize\", "
            + "        \"value\":\"10\" "
            + "      }, "
            + "      { "
            + "        \"key\": \"Board.stepSize\", "
            + "        \"value\": \"1.0\" "
            + "      }, "
            + "      { "
            + "        \"key\": \"Board.xsize\", "
            + "        \"value\": \"15\" "
            + "      } "
            + "    ], "
            + "    \"joints\": [ "
            + "      { "
            + "        \"index\": 0, "
            + "        \"name\": \"joint0\", "
            + "        \"x\": 0.0, "
            + "        \"y\": 0.0 "
            + "      }, "
            + "      { "
            + "        \"index\": 1, "
            + "        \"name\": \"joint1\", "
            + "        \"x\": 3.0, "
            + "        \"y\": 0.0 "
            + "      } "
            + "    ], "
            + "    \"roadSections\": [ "
            + "      { "
            + "        \"index\": 0, "
            + "        \"joint0\": 0, "
            + "        \"joint1\": 1 "
            + "      } "
            + "    ] "
            + "  }, "
            + "  \"players\": [ "
            + "    { "
            + "      \"index\": 0, "
            + "      \"name\":\"playerA\" "
            + "    }, "
            + "    { "
            + "      \"index\": 1, "
            + "      \"name\":\"playerB\" "
            + "    } "
            + "  ] "
            + "}")).readObject());
    final Event initializeGameEvent = eventFactory.createInitializeGameEvent();
    assertEquals(Event.Type.INITIALIZE_GAME, initializeGameEvent.getType());
    assertEquals(123456, initializeGameEvent.getTime());
    assertEquals(Json.createReader(new StringReader(
        "{\n"
        + "  \"type\": \"INITIALIZE_GAME\",\n"
        + "  \"time\": 123456,\n"
        + "  \"game\": {\n"
        + "    \"board\": {\n"
        + "      \"properties\": [\n"
        + "        { "
        + "          \"key\": \"Board.ysize\", "
        + "          \"value\":\"10\" "
        + "        }, "
        + "        { "
        + "          \"key\": \"Board.stepSize\", "
        + "          \"value\":\"1.0\" "
        + "        }, "
        + "        { "
        + "          \"key\": \"Board.xsize\", "
        + "          \"value\":\"15\" "
        + "        } "
        + "      ],\n"
        + "      \"joints\": [\n"
        + "        {\n"
        + "          \"index\": 0,\n"
        + "        \"name\": \"joint0\", "
        + "          \"x\": 0.0,\n"
        + "          \"y\": 0.0\n"
        + "        },\n"
        + "        {\n"
        + "          \"index\": 1,\n"
        + "        \"name\": \"joint1\", "
        + "          \"x\": 3.0,\n"
        + "          \"y\": 0.0\n"
        + "        }\n"
        + "      ],\n"
        + "      \"roadSections\": [\n"
        + "        {\n"
        + "          \"index\": 0,\n"
        + "          \"joint0\": 0,\n"
        + "          \"joint1\": 1\n"
        + "        }\n"
        + "      ]\n"
        + "    },\n"
        + "    \"players\": [\n"
        + "      {\n"
        + "        \"index\": 0,\n"
        + "        \"name\":\"playerA\"\n"
        + "      },\n"
        + "      {\n"
        + "        \"index\": 1,\n"
        + "        \"name\":\"playerB\"\n"
        + "      }\n"
        + "    ]\n"
        + "  }\n"
        + "}\n")).readObject(),
        initializeGameEvent.getBody());
    verify(mockedObjectSerializer, times(1)).serializeGameWithoutPrawnsOrCities();
  }

  @Test
  public void testChangePlayerStateEventCreationWorks() throws EventProcessingException, BoardException {
    final Player player = mock(Player.class);
    when(player.getState()).thenReturn(Player.State.IN_GAME);
    when(mockedObjectSerializer.serializePlayer(player)).thenReturn(2);
    final Event changePlayerStateEvent = eventFactory.createChangePlayerStateEvent(player, Player.State.LOST);
    assertEquals(Event.Type.CHANGE_PLAYER_STATE, changePlayerStateEvent.getType());
    assertEquals(123456, changePlayerStateEvent.getTime());
    assertEquals(
        Json.createReader(new StringReader(
          "{ "
          + "  \"type\": \"CHANGE_PLAYER_STATE\", "
          + "  \"time\": 123456, "
          + "  \"player\": 2, "
          + "  \"state\": \"LOST\" "
          + "}")).readObject(),
        changePlayerStateEvent.getBody());
    verify(player, times(1)).getState();
    verify(mockedObjectSerializer, times(1)).serializePlayer(player);
  }

  @Test
  public void testMovePrawnEventCreationWorks() throws EventProcessingException, BoardException {
    final Move move = mock(Move.class);
    final Prawn prawn = mock(Prawn.class);
    when(mockedObjectSerializer.serializeMove(move)).thenReturn(
        Json.createReader(new StringReader(
            "{ "
          + "  \"position\": {"
          + "    \"roadSection\": 0, "
          + "    \"index\": 1 "
          + "  }, "
          + "  \"moveTime\": 123455, "
          + "  \"stop\": true "
          + "}")).readObject());
    when(mockedObjectSerializer.serializePrawn(prawn)).thenReturn(123);
    final Event movePrawnEvent = eventFactory.createMovePrawnEvent(prawn, move);
    assertEquals(Event.Type.MOVE_PRAWN, movePrawnEvent.getType());
    assertEquals(123456, movePrawnEvent.getTime());
    assertTrue(movePrawnEvent.getBody().equals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"MOVE_PRAWN\", "
          + "  \"time\": 123456, "
          + "  \"prawn\": 123, "
          + "  \"move\": {"
          + "    \"position\": { "
          + "      \"roadSection\": 0, "
          + "      \"index\": 1 "
          + "    }, "
          + "    \"moveTime\": 123455, "
          + "    \"stop\": true "
          + "  } "
          + "}")).readObject()));
    verify(mockedObjectSerializer, times(1)).serializeMove(move);
    verify(mockedObjectSerializer, times(1)).serializePrawn(prawn);
  }

  @Test
  public void testFinishGameEventCreationWorks() throws EventProcessingException, BoardException {
    final Event finishGameEvent = eventFactory.createFinishGameEvent();
    assertEquals(Event.Type.FINISH_GAME, finishGameEvent.getType());
    assertEquals(123456, finishGameEvent.getTime());
    assertEquals(
        Json.createReader(new StringReader(
            "{\n"
          + "  \"type\": \"FINISH_GAME\",\n"
          + "  \"time\": 123456\n"
          + "}")).readObject(),
        finishGameEvent.getBody());
  }

  @Test
  public void testStayPrawnEventCreationWorks() throws EventProcessingException, BoardException {
    final Prawn prawn = mock(Prawn.class);
    when(mockedObjectSerializer.serializePrawn(prawn)).thenReturn(123);
    final Event stayPrawnEvent = eventFactory.createStayPrawnEvent(prawn, 123456);
    assertEquals(Event.Type.STAY_PRAWN, stayPrawnEvent.getType());
    assertEquals(123456, stayPrawnEvent.getTime());
    assertTrue(stayPrawnEvent.getBody().equals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"STAY_PRAWN\", "
          + "  \"time\": 123456, "
          + "  \"prawn\": 123, "
          + "  \"stayTime\": 123456 "
          + "}")).readObject()));
    verify(mockedObjectSerializer, times(1)).serializePrawn(prawn);
  }

  @Test
  public void testDecreasePrawnEnergyEventCreationWorks() throws EventProcessingException, BoardException {
    final Prawn prawn = mock(Prawn.class);
    when(mockedObjectSerializer.serializePrawn(prawn)).thenReturn(123);
    final Event decreasePrawnEnergyEvent = eventFactory.createDecreasePrawnEnergyEvent(prawn, 0.25);
    assertEquals(Event.Type.DECREASE_PRAWN_ENERGY, decreasePrawnEnergyEvent.getType());
    assertEquals(123456, decreasePrawnEnergyEvent.getTime());
    assertTrue(decreasePrawnEnergyEvent.getBody().equals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"DECREASE_PRAWN_ENERGY\", "
          + "  \"time\": 123456, "
          + "  \"prawn\": 123, "
          + "  \"delta\": 0.25 "
          + "}")).readObject()));
    verify(mockedObjectSerializer, times(1)).serializePrawn(prawn);
  }

  @Test
  public void testDestroyPrawnEventCreationWorks() throws EventProcessingException, BoardException {
    final Prawn prawn = mock(Prawn.class);
    when(mockedObjectSerializer.serializePrawn(prawn)).thenReturn(123);
    final Event destroyPrawnEvent = eventFactory.createDestroyPrawnEvent(prawn);
    assertEquals(Event.Type.DESTROY_PRAWN, destroyPrawnEvent.getType());
    assertEquals(123456, destroyPrawnEvent.getTime());
    assertTrue(destroyPrawnEvent.getBody().equals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"DESTROY_PRAWN\", "
          + "  \"time\": 123456, "
          + "  \"prawn\": 123 "
          + "}")).readObject()));
    verify(mockedObjectSerializer, times(1)).serializePrawn(prawn);
  }

  @Test
  public void testProduceWarriorEventCreationWorks() throws EventProcessingException, BoardException {
    final Position position = mock(Position.class);
    final Player player = mock(Player.class);
    when(mockedObjectSerializer.serializePosition(position)).thenReturn(Json.createReader(new StringReader(
        "{"
        + "  \"roadSection\": 0, "
        + "  \"index\": 3 "
        + "}")).readObject());

    when(mockedObjectSerializer.serializePlayer(player)).thenReturn(2);
    final Event produceWarriorEvent = eventFactory.createProduceWarriorEvent(7, position, 0.375, player);
    assertEquals(Event.Type.PRODUCE_WARRIOR, produceWarriorEvent.getType());
    assertEquals(123456, produceWarriorEvent.getTime());
    assertTrue(produceWarriorEvent.getBody().equals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"PRODUCE_WARRIOR\", "
          + "  \"time\": 123456, "
          + "  \"id\": 7, "
          + "  \"position\": { "
          + "    \"roadSection\": 0, "
          + "    \"index\": 3 "
          + "  }, "
          + "  \"direction\": 0.375, "
          + "  \"player\": 2 "
          + "}")).readObject()));
    verify(mockedObjectSerializer, times(1)).serializePosition(position);
    verify(mockedObjectSerializer, times(1)).serializePlayer(player);
  }

  @Test
  public void testProduceSettlersUnitEventCreationWorks() throws EventProcessingException, BoardException {
    final Position position = mock(Position.class);
    final Player player = mock(Player.class);
    when(mockedObjectSerializer.serializePosition(position)).thenReturn(Json.createReader(new StringReader(
        "{"
        + "  \"roadSection\": 0, "
        + "  \"index\": 3 "
        + "}")).readObject());

    when(mockedObjectSerializer.serializePlayer(player)).thenReturn(2);
    final Event produceSettlersUnitEvent = eventFactory.createProduceSettlersUnitEvent(7, position, player);
    assertEquals(Event.Type.PRODUCE_SETTLERS_UNIT, produceSettlersUnitEvent.getType());
    assertEquals(123456, produceSettlersUnitEvent.getTime());
    assertTrue(produceSettlersUnitEvent.getBody().equals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"PRODUCE_SETTLERS_UNIT\", "
          + "  \"time\": 123456, "
          + "  \"id\": 7, "
          + "  \"position\": { "
          + "    \"roadSection\": 0, "
          + "    \"index\": 3 "
          + "  }, "
          + "  \"player\": 2 "
          + "}")).readObject()));
    verify(mockedObjectSerializer, times(1)).serializePosition(position);
    verify(mockedObjectSerializer, times(1)).serializePlayer(player);
  }

  @Test
  public void testRepairPrawnEventCreationWorks() throws EventProcessingException, BoardException {
    final Prawn prawn = mock(Prawn.class);
    when(mockedObjectSerializer.serializePrawn(prawn)).thenReturn(123);
    final Event repairPrawnEvent = eventFactory.createRepairPrawnEvent(prawn, 0.25);
    assertEquals(Event.Type.REPAIR_PRAWN, repairPrawnEvent.getType());
    assertEquals(123456, repairPrawnEvent.getTime());
    assertTrue(repairPrawnEvent.getBody().equals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"REPAIR_PRAWN\", "
          + "  \"time\": 123456, "
          + "  \"prawn\": 123, "
          + "  \"delta\": 0.25 "
          + "}")).readObject()));
    verify(mockedObjectSerializer, times(1)).serializePrawn(prawn);
  }

  @Test
  public void testCreateCityEventCreationWorks() throws EventProcessingException, BoardException {
    final SettlersUnit settlersUnit = mock(SettlersUnit.class);
    when(mockedObjectSerializer.serializePrawn(settlersUnit)).thenReturn(123);

    final Event createCityEvent = eventFactory.createCreateCityEvent(settlersUnit, 321, "cityName");
    assertEquals(Event.Type.CREATE_CITY, createCityEvent.getType());
    assertEquals(123456, createCityEvent.getTime());
    assertEquals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"CREATE_CITY\", "
          + "  \"time\": 123456, "
          + "  \"settlersUnit\": 123, "
          + "  \"id\": 321, "
          + "  \"name\": \"cityName\" "
          + "}")).readObject(),
        createCityEvent.getBody());
    verify(mockedObjectSerializer, times(1)).serializePrawn(settlersUnit);
  }

  @Test
  public void testGrowCityEventCreationWorks() throws EventProcessingException, BoardException {
    final City city = mock(City.class);
    when(mockedObjectSerializer.serializeCity(city)).thenReturn(123);
    final Event growCityEvent = eventFactory.createGrowCityEvent(city);
    assertEquals(Event.Type.GROW_CITY, growCityEvent.getType());
    assertEquals(123456, growCityEvent.getTime());
    assertTrue(growCityEvent.getBody().equals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"GROW_CITY\", "
          + "  \"time\": 123456, "
          + "  \"city\": 123 "
          + "}")).readObject()));
    verify(mockedObjectSerializer, times(1)).serializeCity(city);
  }

  @Test
  public void testShrinkOrDestroyCityEventCreationWorks() throws EventProcessingException, BoardException {
    final City city = mock(City.class);
    when(mockedObjectSerializer.serializeCity(city)).thenReturn(123);
    final Event shrinkOrDestroyCityEvent = eventFactory.createShrinkOrDestroyCityEvent(city, 2);
    assertEquals(Event.Type.SHRINK_OR_DESTROY_CITY, shrinkOrDestroyCityEvent.getType());
    assertEquals(123456, shrinkOrDestroyCityEvent.getTime());
    assertTrue(shrinkOrDestroyCityEvent.getBody().equals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"SHRINK_OR_DESTROY_CITY\", "
          + "  \"time\": 123456, "
          + "  \"city\": 123, "
          + "  \"factoryIndex\": 2 "
          + "}")).readObject()));
    verify(mockedObjectSerializer, times(1)).serializeCity(city);
  }


  @Test
  public void testCaptureOrDestroyCityEventCreationWorks() throws EventProcessingException, BoardException {
    final City city = mock(City.class);
    final Player defender = mock(Player.class);
    final Player attacker = mock(Player.class);
    when(mockedObjectSerializer.serializeCity(city)).thenReturn(123);
    when(mockedObjectSerializer.serializePlayer(defender)).thenReturn(3);
    when(mockedObjectSerializer.serializePlayer(attacker)).thenReturn(2);
    final Event captureOrDestroyCityEvent = eventFactory.createCaptureOrDestroyCityEvent(city, defender, attacker);
    assertEquals(Event.Type.CAPTURE_OR_DESTROY_CITY, captureOrDestroyCityEvent.getType());
    assertEquals(123456, captureOrDestroyCityEvent.getTime());
    assertTrue(captureOrDestroyCityEvent.getBody().equals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"CAPTURE_OR_DESTROY_CITY\", "
          + "  \"time\": 123456, "
          + "  \"city\": 123, "
          + "  \"defender\": 3, "
          + "  \"attacker\": 2 "
          + "}")).readObject()));
    verify(mockedObjectSerializer, times(1)).serializeCity(city);
    verify(mockedObjectSerializer, times(1)).serializePlayer(defender);
    verify(mockedObjectSerializer, times(1)).serializePlayer(attacker);
  }

  @Test
  public void testAddBattleEventCreationWorks() throws EventProcessingException, BoardException {
    final Battle battle = mock(Battle.class);
    when(mockedObjectSerializer.serializeBattle(battle)).thenReturn(Json.createReader(new StringReader(
        "{"
        + "  \"prawn0\": 3, "
        + "  \"prawn1\": 4, "
        + "  \"normalizedPosition0\": {"
        + "    \"roadSection\": 0, "
        + "    \"index\": 1 "
        + "  }, "
        + "  \"normalizedPosition1\": {"
        + "    \"roadSection\": 0, "
        + "    \"index\": 2 "
        + "  } "
        + "}")).readObject());

    final Event addBattleEvent = eventFactory.createAddBattleEvent(battle);
    assertEquals(Event.Type.ADD_BATTLE, addBattleEvent.getType());
    assertEquals(123456, addBattleEvent.getTime());
    assertTrue(addBattleEvent.getBody().equals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"ADD_BATTLE\", "
          + "  \"time\": 123456, "
          + "  \"battle\": { "
          + "    \"prawn0\": 3, "
          + "    \"prawn1\": 4, "
          + "    \"normalizedPosition0\": {"
          + "      \"roadSection\": 0, "
          + "      \"index\": 1 "
          + "    }, "
          + "    \"normalizedPosition1\": {"
          + "      \"roadSection\": 0, "
          + "      \"index\": 2 "
          + "    } "
          + "  } "
          + "}")).readObject()));
    verify(mockedObjectSerializer, times(1)).serializeBattle(battle);
  }

  @Test
  public void testRemoveBattleEventCreationWorks() throws EventProcessingException, BoardException {
    final Battle battle = mock(Battle.class);
    when(mockedObjectSerializer.serializeBattle(battle)).thenReturn(Json.createReader(new StringReader(
        "{"
        + "  \"prawn0\": 3, "
        + "  \"prawn1\": 4, "
        + "  \"normalizedPosition0\": {"
        + "    \"roadSection\": 0, "
        + "    \"index\": 1 "
        + "  }, "
        + "  \"normalizedPosition1\": {"
        + "    \"roadSection\": 0, "
        + "    \"index\": 2 "
        + "  } "
        + "}")).readObject());

    final Event removeBattleEvent = eventFactory.createRemoveBattleEvent(battle);
    assertEquals(Event.Type.REMOVE_BATTLE, removeBattleEvent.getType());
    assertEquals(123456, removeBattleEvent.getTime());
    assertTrue(removeBattleEvent.getBody().equals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"REMOVE_BATTLE\", "
          + "  \"time\": 123456, "
          + "  \"battle\": { "
          + "    \"prawn0\": 3, "
          + "    \"prawn1\": 4, "
          + "    \"normalizedPosition0\": {"
          + "      \"roadSection\": 0, "
          + "      \"index\": 1 "
          + "    }, "
          + "    \"normalizedPosition1\": {"
          + "      \"roadSection\": 0, "
          + "      \"index\": 2 "
          + "    } "
          + "  } "
          + "}")).readObject()));
    verify(mockedObjectSerializer, times(1)).serializeBattle(battle);
  }

  @Test
  public void testIncreaseProgressByUniversalDeltaEventCreationWorks() throws EventProcessingException, BoardException {
    final CityFactory factory = mock(CityFactory.class);
    when(mockedObjectSerializer.serializeFactory(factory)).thenReturn(Json.createReader(new StringReader(
        "{"
        + "  \"city\": 123, "
        + "  \"index\": 2 "
        + "}")).readObject());
    final Event increaseProgressByUniversalDeltaEvent =
        eventFactory.createIncreaseProgressByUniversalDeltaEvent(factory, 0.25);
    assertEquals(Event.Type.INCREASE_PROGRESS_BY_UNIVERSAL_DELTA, increaseProgressByUniversalDeltaEvent.getType());
    assertEquals(123456, increaseProgressByUniversalDeltaEvent.getTime());
    assertTrue(increaseProgressByUniversalDeltaEvent.getBody().equals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"INCREASE_PROGRESS_BY_UNIVERSAL_DELTA\", "
          + "  \"time\": 123456, "
          + "  \"factory\": { "
          + "    \"city\": 123, "
          + "    \"index\": 2 "
          + "  }, "
          + "  \"universalDelta\": 0.25 "
          + "}")).readObject()));
    verify(mockedObjectSerializer, times(1)).serializeFactory(factory);
  }

  @Test
  public void testSetFactoryProgressEventCreationWorks() throws EventProcessingException, BoardException {
    final CityFactory factory = mock(CityFactory.class);
    when(mockedObjectSerializer.serializeFactory(factory)).thenReturn(Json.createReader(new StringReader(
        "{"
        + "  \"city\": 123, "
        + "  \"index\": 2 "
        + "}")).readObject());
    final Event setFactoryProgressEvent = eventFactory.createSetFactoryProgressEvent(factory, 0.25);
    assertEquals(Event.Type.SET_FACTORY_PROGRESS, setFactoryProgressEvent.getType());
    assertEquals(123456, setFactoryProgressEvent.getTime());
    assertTrue(setFactoryProgressEvent.getBody().equals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"SET_FACTORY_PROGRESS\", "
          + "  \"time\": 123456, "
          + "  \"factory\": { "
          + "    \"city\": 123, "
          + "    \"index\": 2 "
          + "  }, "
          + "  \"progress\": 0.25 "
          + "}")).readObject()));
    verify(mockedObjectSerializer, times(1)).serializeFactory(factory);
  }

  @Test
  public void testActionFinishedEventCreationWorks() throws EventProcessingException, BoardException {
    final CityFactory factory = mock(CityFactory.class);
    when(mockedObjectSerializer.serializeFactory(factory)).thenReturn(Json.createReader(new StringReader(
        "{"
        + "  \"city\": 123, "
        + "  \"index\": 2 "
        + "}")).readObject());
    final Event setActionFinishedEvent = eventFactory.createActionFinishedEvent(factory);
    assertEquals(Event.Type.ACTION_FINISHED, setActionFinishedEvent.getType());
    assertEquals(123456, setActionFinishedEvent.getTime());
    assertEquals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"ACTION_FINISHED\", "
          + "  \"time\": 123456, "
          + "  \"factory\": { "
          + "    \"city\": 123, "
          + "    \"index\": 2 "
          + "  } "
          + "}")).readObject(),
        setActionFinishedEvent.getBody());
    verify(mockedObjectSerializer, times(1)).serializeFactory(factory);
  }

  @Test
  public void testPeriodicInterruptEventCreationWorks() throws EventProcessingException, BoardException {
    final Event periodicInterruptEvent = eventFactory.createPeriodicInterruptEvent();
    assertEquals(Event.Type.PERIODIC_INTERRUPT, periodicInterruptEvent.getType());
    assertEquals(123456, periodicInterruptEvent.getTime());
    assertEquals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"PERIODIC_INTERRUPT\", "
          + "  \"time\": 123456 "
          + "}")).readObject(),
        periodicInterruptEvent.getBody());
  }

  @Test
  public void testRoadSectionHalfAppearsEventCreationWorks() throws EventProcessingException, BoardException {
    final RoadSection.Half roadSectionHalf = mock(RoadSection.Half.class);
    final Player player = mock(Player.class);
    when(mockedObjectSerializer.serializeRoadSectionHalf(roadSectionHalf)).thenReturn(Json.createObjectBuilder()
        .add("roadSection", 42)
        .add("end", "FORWARD")
        .build());
    when(mockedObjectSerializer.serializePlayer(player)).thenReturn(1);
    final Event roadSectionHalfAppearsEvent =
        eventFactory.createRoadSectionHalfAppearsEvent(roadSectionHalf, player);
    assertEquals(Event.Type.ROAD_SECTION_HALF_APPEARS, roadSectionHalfAppearsEvent.getType());
    assertEquals(123456, roadSectionHalfAppearsEvent.getTime());
    assertEquals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"ROAD_SECTION_HALF_APPEARS\", "
          + "  \"time\": 123456, "
          + "  \"roadSectionHalf\": { "
          + "    \"roadSection\": 42, "
          + "    \"end\": \"FORWARD\" "
          + "  }, "
          + "  \"player\": 1 "
          + "}")).readObject(),
        roadSectionHalfAppearsEvent.getBody());
    verify(mockedObjectSerializer, times(1)).serializeRoadSectionHalf(roadSectionHalf);
    verify(mockedObjectSerializer, times(1)).serializePlayer(player);
  }

  @Test
  public void testRoadSectionHalfDisappearsEventCreationWorks() throws EventProcessingException, BoardException {
    final RoadSection.Half roadSectionHalf = mock(RoadSection.Half.class);
    final Player player = mock(Player.class);
    when(mockedObjectSerializer.serializeRoadSectionHalf(roadSectionHalf)).thenReturn(Json.createObjectBuilder()
        .add("roadSection", 42)
        .add("end", "FORWARD")
        .build());
    when(mockedObjectSerializer.serializePlayer(player)).thenReturn(1);
    final Event roadSectionHalfDisappearsEvent =
        eventFactory.createRoadSectionHalfDisappearsEvent(roadSectionHalf, player);
    assertEquals(Event.Type.ROAD_SECTION_HALF_DISAPPEARS, roadSectionHalfDisappearsEvent.getType());
    assertEquals(123456, roadSectionHalfDisappearsEvent.getTime());
    assertEquals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"ROAD_SECTION_HALF_DISAPPEARS\", "
          + "  \"time\": 123456, "
          + "  \"roadSectionHalf\": { "
          + "    \"roadSection\": 42, "
          + "    \"end\": \"FORWARD\" "
          + "  }, "
          + "  \"player\": 1 "
          + "}")).readObject(),
        roadSectionHalfDisappearsEvent.getBody());
    verify(mockedObjectSerializer, times(1)).serializeRoadSectionHalf(roadSectionHalf);
    verify(mockedObjectSerializer, times(1)).serializePlayer(player);
  }

  @Test
  public void testRoadSectionHalfDiscoveredEventCreationWorks() throws EventProcessingException, BoardException {
    final RoadSection.Half roadSectionHalf = mock(RoadSection.Half.class);
    final Player player = mock(Player.class);
    when(mockedObjectSerializer.serializeRoadSectionHalf(roadSectionHalf)).thenReturn(Json.createObjectBuilder()
        .add("roadSection", 42)
        .add("end", "FORWARD")
        .build());
    when(mockedObjectSerializer.serializePlayer(player)).thenReturn(1);
    final Event roadSectionHalfDiscoveredEvent =
        eventFactory.createRoadSectionHalfDiscoveredEvent(roadSectionHalf, player);
    assertEquals(Event.Type.ROAD_SECTION_HALF_DISCOVERED, roadSectionHalfDiscoveredEvent.getType());
    assertEquals(123456, roadSectionHalfDiscoveredEvent.getTime());
    assertEquals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"ROAD_SECTION_HALF_DISCOVERED\", "
          + "  \"time\": 123456, "
          + "  \"roadSectionHalf\": { "
          + "    \"roadSection\": 42, "
          + "    \"end\": \"FORWARD\" "
          + "  }, "
          + "  \"player\": 1 "
          + "}")).readObject(),
        roadSectionHalfDiscoveredEvent.getBody());
    verify(mockedObjectSerializer, times(1)).serializeRoadSectionHalf(roadSectionHalf);
    verify(mockedObjectSerializer, times(1)).serializePlayer(player);
  }

  @Test
  public void testPrawnAppearsEventCreationWorks() throws EventProcessingException, BoardException {
    final Prawn prawn = mock(Prawn.class);
    final Player player = mock(Player.class);
    final Player opponent = mock(Player.class);
    when(mockedObjectSerializer.serializePrawn(prawn)).thenReturn(123);
    when(mockedObjectSerializer.serializePlayer(player)).thenReturn(1);
    when(prawn.getPlayer()).thenReturn(opponent);
    final Event prawnAppearsEvent = eventFactory.createPrawnAppearsEvent(prawn, player);
    assertEquals(Event.Type.PRAWN_APPEARS, prawnAppearsEvent.getType());
    assertEquals(123456, prawnAppearsEvent.getTime());
    assertTrue(prawnAppearsEvent.getBody().equals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"PRAWN_APPEARS\", "
          + "  \"time\": 123456, "
          + "  \"prawn\": 123, "
          + "  \"player\": 1 "
          + "}")).readObject()));
    verify(mockedObjectSerializer, times(1)).serializePrawn(prawn);
    verify(mockedObjectSerializer, times(1)).serializePlayer(player);
    verify(prawn, times(1)).getPlayer();
  }

  @Test
  public void testPrawnDisappearsEventCreationWorks() throws EventProcessingException, BoardException {
    final Prawn prawn = mock(Prawn.class);
    final Player player = mock(Player.class);
    final Player opponent = mock(Player.class);
    when(mockedObjectSerializer.serializePrawn(prawn)).thenReturn(123);
    when(mockedObjectSerializer.serializePlayer(player)).thenReturn(1);
    when(prawn.getPlayer()).thenReturn(opponent);
    final Event prawnDisappearsEvent = eventFactory.createPrawnDisappearsEvent(prawn, player);
    assertEquals(Event.Type.PRAWN_DISAPPEARS, prawnDisappearsEvent.getType());
    assertEquals(123456, prawnDisappearsEvent.getTime());
    assertTrue(prawnDisappearsEvent.getBody().equals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"PRAWN_DISAPPEARS\", "
          + "  \"time\": 123456, "
          + "  \"prawn\": 123, "
          + "  \"player\": 1 "
          + "}")).readObject()));
    verify(mockedObjectSerializer, times(1)).serializePrawn(prawn);
    verify(mockedObjectSerializer, times(1)).serializePlayer(player);
    verify(prawn, times(1)).getPlayer();
  }

  @Test
  public void testCityAppearsEventCreationWorks() throws EventProcessingException, BoardException {
    final City city = mock(City.class);
    final Player player = mock(Player.class);
    final Player opponent = mock(Player.class);
    when(mockedObjectSerializer.serializeCity(city)).thenReturn(7);
    when(mockedObjectSerializer.serializePlayer(player)).thenReturn(1);
    when(city.getPlayer()).thenReturn(opponent);
    final Event cityAppearsEvent = eventFactory.createCityAppearsEvent(city, player);
    assertEquals(Event.Type.CITY_APPEARS, cityAppearsEvent.getType());
    assertEquals(123456, cityAppearsEvent.getTime());
    assertTrue(cityAppearsEvent.getBody().equals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"CITY_APPEARS\", "
          + "  \"time\": 123456, "
          + "  \"city\": 7, "
          + "  \"player\": 1 "
          + "}")).readObject()));
    verify(mockedObjectSerializer, times(1)).serializeCity(city);
    verify(mockedObjectSerializer, times(1)).serializePlayer(player);
    verify(city, times(1)).getPlayer();
  }

  @Test
  public void testCityShownDestroyedEventCreationWorks() throws EventProcessingException, BoardException {
    final City city = mock(City.class);
    final Player player = mock(Player.class);
    final Player opponent = mock(Player.class);
    when(mockedObjectSerializer.serializeCity(city)).thenReturn(7);
    when(mockedObjectSerializer.serializePlayer(player)).thenReturn(1);
    when(city.getPlayer()).thenReturn(opponent);
    final Event cityShownDestroyedEvent = eventFactory.createCityShownDestroyedEvent(city, player);
    assertEquals(Event.Type.CITY_SHOWN_DESTROYED, cityShownDestroyedEvent.getType());
    assertEquals(123456, cityShownDestroyedEvent.getTime());
    assertTrue(cityShownDestroyedEvent.getBody().equals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"CITY_SHOWN_DESTROYED\", "
          + "  \"time\": 123456, "
          + "  \"city\": 7, "
          + "  \"player\": 1 "
          + "}")).readObject()));
    verify(mockedObjectSerializer, times(1)).serializeCity(city);
    verify(mockedObjectSerializer, times(1)).serializePlayer(player);
    verify(city, times(1)).getPlayer();
  }

}
