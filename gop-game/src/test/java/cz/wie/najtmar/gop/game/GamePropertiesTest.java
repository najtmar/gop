package cz.wie.najtmar.gop.game;

import static org.junit.Assert.assertEquals;

import cz.wie.najtmar.gop.common.Constants;

import org.junit.Before;
import org.junit.Test;

import java.util.Properties;

public class GamePropertiesTest {

  GameProperties simpleGameProperties;

  /**
   * Method setUp().
   * @throws Exception when the method execution fails
   */
  @Before
  public void setUp() throws Exception {
    final Properties simpleGamePropertiesSet = new Properties();
    simpleGamePropertiesSet.load(GamePropertiesTest.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/game/simple-game.properties"));
    simpleGameProperties = new GameProperties(simpleGamePropertiesSet);
  }

  @Test
  public void testGamePropertiesInitializedCorrectly() {
    assertEquals(4, simpleGameProperties.getMaxPlayerCount());
    assertEquals(1, simpleGameProperties.getTimeDelta());
    assertEquals(1.0, simpleGameProperties.getBasicVelocity(), 0.0);
    assertEquals(2.0, simpleGameProperties.getVelocityDirContribFactor(), 0.0);
    assertEquals(0.667, simpleGameProperties.getProduceActionProgressFactor(), Constants.DOUBLE_DELTA);
    assertEquals(0.333, simpleGameProperties.getRepairActionProgressFactor(), Constants.DOUBLE_DELTA);
    assertEquals(0.2, simpleGameProperties.getGrowActionProgressFactor(), 0.0);
    assertEquals(0.8, simpleGameProperties.getAttackSuccessProbability(), 0.0);
    assertEquals(0.3, simpleGameProperties.getMaxAttackStrength(), 0.0);
    assertEquals(0.125, simpleGameProperties.getMaxProductionDelta(), 0.0);
  }

}
