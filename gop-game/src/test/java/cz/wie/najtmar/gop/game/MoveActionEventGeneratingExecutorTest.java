package cz.wie.najtmar.gop.game;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import cz.wie.najtmar.gop.board.Board;
import cz.wie.najtmar.gop.board.BoardException;
import cz.wie.najtmar.gop.board.Joint;
import cz.wie.najtmar.gop.board.Position;
import cz.wie.najtmar.gop.board.RoadSection;
import cz.wie.najtmar.gop.common.Point2D;
import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.event.EventProcessingException;
import cz.wie.najtmar.gop.game.EventGeneratingExecutor.State;
import cz.wie.najtmar.gop.game.MoveActionEventGeneratingExecutor.PrawnInfo;
import cz.wie.najtmar.gop.game.MoveActionEventGeneratingExecutor.PrawnMove;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Properties;
import java.util.UUID;

import javax.json.Json;

public class MoveActionEventGeneratingExecutorTest {

  static final long START_TIME = 966;
  static Board BOARD;
  static Joint JOINT_0_0;
  static Joint JOINT_3_0;
  static Joint JOINT_6_0;
  static Joint JOINT_3_3;
  static RoadSection ROAD_SECTION_0;
  static RoadSection ROAD_SECTION_1;
  static RoadSection ROAD_SECTION_2;
  static Position POSITION_0;
  static Event TEST_EVENT;

  Game game;
  GameProperties gameProperties;
  Game mockedGame;
  EventFactory eventFactory;
  EventFactory mockedEventFactory;
  Player playerA;
  Player playerB;
  Warrior warriorA0;
  Warrior warriorA1;
  Warrior warriorA2;  // Not added to the game in method setUp().
  Warrior warriorB0;
  Warrior warriorB1;
  SettlersUnit settlersUnitA;
  SettlersUnit settlersUnitB;

  /**
   * setUpBeforeClass method.
   * @throws Exception when something unexpected happens
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    final Properties simpleBoardPropertiesSet = new Properties();
    simpleBoardPropertiesSet.load(MoveActionEventGeneratingExecutorTest.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/board/simple-board.properties"));
    BOARD = new Board(simpleBoardPropertiesSet);
    JOINT_0_0 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setName("JOINT_0_0")
        .setCoordinatesPair(new Point2D.Double(0.0, 0.0))
        .build();
    JOINT_3_0 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(1)
        .setName("JOINT_3_0")
        .setCoordinatesPair(new Point2D.Double(3.0, 0.0))
        .build();
    JOINT_6_0 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(2)
        .setName("JOINT_6_0")
        .setCoordinatesPair(new Point2D.Double(6.0, 0.0))
        .build();
    JOINT_3_3 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(3)
        .setName("JOINT_3_3")
        .setCoordinatesPair(new Point2D.Double(3.0, 3.0))
        .build();
    ROAD_SECTION_0 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(JOINT_0_0, JOINT_3_0)
        .build();
    ROAD_SECTION_1 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(1)
        .setJoints(JOINT_3_0, JOINT_6_0)
        .build();
    ROAD_SECTION_2 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(2)
        .setJoints(JOINT_3_3, JOINT_3_0)
        .build();
    POSITION_0 = new Position(ROAD_SECTION_0, 0);
    TEST_EVENT = new Event(Json.createObjectBuilder()
        .add("type", "TEST")
        .add("time", START_TIME)
        .add("whatever", "value")
        .build());
  }

  /**
   * Sets up the test case.
   * @throws Exception when test case cannot be set up
   */
  @Before
  public void setUp() throws Exception {
    Prawn.resetFirstAvailableId();
    final Properties simpleGamePropertiesSet = new Properties();
    simpleGamePropertiesSet.load(MoveActionEventGeneratingExecutorTest.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/game/simple-game.properties"));
    simpleGamePropertiesSet.setProperty(GameProperties.START_TIME_PROPERTY_NAME, "" + START_TIME);
    game = new Game(simpleGamePropertiesSet);
    gameProperties = game.getGameProperties();
    mockedGame = mock(Game.class);
    playerA = new Player(new User("playerA", new UUID(12345, 67890)), 0, game);
    playerB = new Player(new User("playerB", new UUID(98765, 43210)), 1, game);
    game.addPlayer(playerA);
    game.addPlayer(playerB);
    warriorA0 = spy((new Warrior.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(POSITION_0)
        .setDirection(0.0)
        .setGameProperties(gameProperties)
        .setPlayer(playerA)
        .build());
    warriorA0.setCurrentPosition(new Position(ROAD_SECTION_0, 1));
    warriorA0.setItinerary(new Itinerary.Builder()
        .addPosition(new Position(ROAD_SECTION_0, 1))
        .addPosition(new Position(ROAD_SECTION_0, 3))
        .build());
    warriorA0.setLastMoveTime(START_TIME);
    warriorA1 = spy((new Warrior.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(POSITION_0)
        .setDirection(0.0)
        .setGameProperties(gameProperties)
        .setPlayer(playerA)
        .build());
    warriorA1.setItinerary(new Itinerary.Builder()
        .addPosition(new Position(ROAD_SECTION_0, 0))
        .addPosition(new Position(ROAD_SECTION_0, 1))
        .build());
    warriorA1.setLastMoveTime(START_TIME);
    warriorA2 = spy((new Warrior.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(POSITION_0)
        .setDirection(0.0)
        .setGameProperties(gameProperties)
        .setPlayer(playerA)
        .build());
    warriorA2.setCurrentPosition(new Position(ROAD_SECTION_2, 3));
    warriorA2.setItinerary(new Itinerary.Builder()
        .addPosition(new Position(ROAD_SECTION_1, 0))
        .addPosition(new Position(ROAD_SECTION_1, 1))
        .addPosition(new Position(ROAD_SECTION_1, 0))
        .build());
    warriorA2.setLastMoveTime(START_TIME);
    warriorB0 = spy((new Warrior.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(POSITION_0)
        .setDirection(0.0)
        .setGameProperties(gameProperties)
        .setPlayer(playerB)
        .build());
    warriorB0.setCurrentPosition(new Position(ROAD_SECTION_0, 2));
    warriorB0.setItinerary(new Itinerary.Builder()
        .addPosition(new Position(ROAD_SECTION_0, 2))
        .addPosition(new Position(ROAD_SECTION_1, 0))
        .addPosition(new Position(ROAD_SECTION_1, 1))
        .build());
    warriorB0.setLastMoveTime(START_TIME);
    warriorB1 = spy((new Warrior.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(POSITION_0)
        .setDirection(0.0)
        .setGameProperties(gameProperties)
        .setPlayer(playerB)
        .build());
    warriorB1.setCurrentPosition(new Position(ROAD_SECTION_2, 2));
    warriorB1.setItinerary(new Itinerary.Builder()
        .addPosition(new Position(ROAD_SECTION_2, 2))
        .addPosition(new Position(ROAD_SECTION_2, 3))
        .addPosition(new Position(ROAD_SECTION_2, 1))
        .build());
    warriorB1.setLastMoveTime(START_TIME);
    settlersUnitA = spy((new SettlersUnit.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(JOINT_3_0.getNormalizedPosition())
        .setGameProperties(gameProperties)
        .setPlayer(playerA)
        .build());
    settlersUnitA.setCurrentPosition(new Position(ROAD_SECTION_2, 1));
    settlersUnitB = spy((new SettlersUnit.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(JOINT_3_3.getNormalizedPosition())
        .setGameProperties(gameProperties)
        .setPlayer(playerB)
        .build());
    settlersUnitB.setCurrentPosition(new Position(ROAD_SECTION_2, 2));
    game.addPrawn(warriorA0);
    game.addPrawn(warriorA1);
    game.addPrawn(warriorB0);
    game.addPrawn(warriorB1);
    game.setBoard(BOARD);
    game.initialize();
    final ObjectSerializer objectSerializer = new ObjectSerializer(game);
    eventFactory = new EventFactory(objectSerializer);
    mockedEventFactory = mock(EventFactory.class);
  }

  @Test
  public void testSimpleRoundGenerationCreationWorks() throws Exception {
    final Battle battle0 = new Battle(warriorA0, new Position(ROAD_SECTION_0, 1), new Position(ROAD_SECTION_0, 2),
        warriorB0, new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_0, 1));
    when(mockedGame.getBattles()).thenReturn(new HashSet<>(Arrays.asList(battle0)));
    MoveActionEventGeneratingExecutor executor =
        (new MoveActionEventGeneratingExecutor.Factory(mockedGame, mockedEventFactory))
        .create();
    when(mockedGame.getTime()).thenReturn(START_TIME);
    executor.prepare(new LinkedList<>());
    assertEquals(new HashSet<>(Arrays.asList(battle0)), executor.getBattles());
    assertEquals(mockedGame, executor.getGame());
    assertEquals(Arrays.asList(), executor.getPrawnMoves());
    assertEquals(new HashSet<>(), executor.getPrawnsEverOnJoint(JOINT_0_0));
    assertEquals(new HashSet<>(), executor.getPrawnsEverOnJoint(JOINT_3_0));
    assertEquals(new HashSet<>(), executor.getPrawnsEverOnJoint(JOINT_3_3));
    assertEquals(new HashSet<>(), executor.getPrawnsEverOnJoint(JOINT_6_0));
    assertEquals(new HashSet<>(), executor.getPrawnsEverOnJoint(null));
    assertEquals(new HashSet<>(), executor.getPrawnsEverOnRoadSection(ROAD_SECTION_0));
    assertEquals(new HashSet<>(), executor.getPrawnsEverOnRoadSection(ROAD_SECTION_1));
    assertEquals(new HashSet<>(), executor.getPrawnsEverOnRoadSection(ROAD_SECTION_2));
    assertEquals(new HashSet<>(Arrays.asList(battle0)), executor.getPrawnBattles(warriorA0));
    assertEquals(new HashSet<>(), executor.getPrawnBattles(warriorA1));
    assertEquals(new HashSet<>(Arrays.asList(battle0)), executor.getPrawnBattles(warriorB0));
    assertEquals(new HashSet<>(), executor.getPrawnBattles(warriorB1));
    assertEquals(MoveActionEventGeneratingExecutor.State.PREPARED, executor.getState());
    verify(mockedGame, times(1)).getBattles();
  }

  @Test
  public void testGeneratorPreparationWorks() throws GameException, BoardException, EventProcessingException {
    doAnswer(new Answer<Move[]>() {
      @Override
      public Move[] answer(InvocationOnMock invocation) throws Throwable {
        return new Move[]{
            new Move(new Position(ROAD_SECTION_0, 1), START_TIME, false),
            new Move(new Position(ROAD_SECTION_0, 2), START_TIME + 3, false),
            new Move(new Position(ROAD_SECTION_0, 3), START_TIME + 6, false),
            new Move(new Position(ROAD_SECTION_2, 2), START_TIME + 9, false),
        };
      }
    }).when(warriorA0).prepareMoveSequence(START_TIME + 10);
    doAnswer(new Answer<Move[]>() {
      @Override
      public Move[] answer(InvocationOnMock invocation) throws Throwable {
        return new Move[]{
            new Move(POSITION_0, START_TIME + 10, true),
        };
      }
    }).when(warriorA1).prepareMoveSequence(START_TIME + 10);
    doAnswer(new Answer<Move[]>() {
      @Override
      public Move[] answer(InvocationOnMock invocation) throws Throwable {
        return new Move[]{
            new Move(new Position(ROAD_SECTION_1, 3), START_TIME, false),
            new Move(new Position(ROAD_SECTION_1, 2), START_TIME + 2, false),
            new Move(new Position(ROAD_SECTION_1, 1), START_TIME + 6, false),
        };
      }
    }).when(warriorB0).prepareMoveSequence(START_TIME + 10);
    doAnswer(new Answer<Move[]>() {
      @Override
      public Move[] answer(InvocationOnMock invocation) throws Throwable {
        return new Move[]{
            new Move(new Position(ROAD_SECTION_2, 2), START_TIME, false),
            new Move(new Position(ROAD_SECTION_2, 3), START_TIME + 3, true),
        };
      }
    }).when(warriorB1).prepareMoveSequence(START_TIME + 10);
    MoveActionEventGeneratingExecutor executor =
        (new MoveActionEventGeneratingExecutor.Factory(game, eventFactory))
        .create();
    assertEquals(State.NOT_PREPARED, executor.getState());
    game.advanceTimeTo(START_TIME + 10);
    executor.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    assertEquals(State.PREPARED, executor.getState());

    assertEquals(new PrawnInfo(new Position(ROAD_SECTION_0, 1), false, START_TIME),
        executor.getPrawnInfo(warriorA0));
    assertEquals(null, executor.getPrawnInfo(warriorA0).getPlannedMove());
    assertFalse(executor.getPrawnInfo(warriorA0).isSkipRound());
    assertEquals(new PrawnInfo(POSITION_0, false, START_TIME + 10), executor.getPrawnInfo(warriorA1));
    assertEquals(new PrawnInfo(new Position(ROAD_SECTION_1, 3), false, START_TIME),
        executor.getPrawnInfo(warriorB0));
    assertEquals(new PrawnInfo(new Position(ROAD_SECTION_2, 2), false, START_TIME),
        executor.getPrawnInfo(warriorB1));

    Iterator<PrawnMove> prawnMoveIter = executor.getPrawnMoves().iterator();
    assertTrue(prawnMoveIter.hasNext());
    assertEquals(new PrawnMove(START_TIME + 2, warriorB0, new Position(ROAD_SECTION_1, 3),
        new Position(ROAD_SECTION_1, 2), false), prawnMoveIter.next());
    assertTrue(prawnMoveIter.hasNext());
    assertEquals(new PrawnMove(START_TIME + 3, warriorA0, new Position(ROAD_SECTION_0, 1),
        new Position(ROAD_SECTION_0, 2), false), prawnMoveIter.next());
    assertTrue(prawnMoveIter.hasNext());
    assertEquals(new PrawnMove(START_TIME + 3, warriorB1, new Position(ROAD_SECTION_2, 2),
        new Position(ROAD_SECTION_2, 3), true), prawnMoveIter.next());
    assertTrue(prawnMoveIter.hasNext());
    assertEquals(new PrawnMove(START_TIME + 6, warriorA0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 3), false), prawnMoveIter.next());
    assertTrue(prawnMoveIter.hasNext());
    assertEquals(new PrawnMove(START_TIME + 6, warriorB0, new Position(ROAD_SECTION_1, 2),
        new Position(ROAD_SECTION_1, 1), false), prawnMoveIter.next());
    assertTrue(prawnMoveIter.hasNext());
    assertEquals(new PrawnMove(START_TIME + 9, warriorA0, new Position(ROAD_SECTION_0, 3),
        new Position(ROAD_SECTION_2, 2), false), prawnMoveIter.next());
    assertFalse(prawnMoveIter.hasNext());

    assertEquals(new HashSet<>(Arrays.asList(warriorA0, warriorA1, warriorB1)),
        executor.getPrawnsEverOnRoadSection(ROAD_SECTION_0));
    assertEquals(new HashSet<>(Arrays.asList(warriorA0, warriorB0, warriorB1)),
        executor.getPrawnsEverOnRoadSection(ROAD_SECTION_1));
    assertEquals(new HashSet<>(Arrays.asList(warriorA0, warriorB1)),
        executor.getPrawnsEverOnRoadSection(ROAD_SECTION_2));
    assertEquals(new HashSet<>(Arrays.asList(warriorA1)), executor.getPrawnsEverOnJoint(JOINT_0_0));
    assertEquals(new HashSet<>(Arrays.asList(warriorA0, warriorB1)), executor.getPrawnsEverOnJoint(JOINT_3_0));
    assertEquals(new HashSet<>(), executor.getPrawnsEverOnJoint(JOINT_3_3));
    assertEquals(new HashSet<>(Arrays.asList(warriorB0)), executor.getPrawnsEverOnJoint(JOINT_6_0));

    verify(warriorA0, times(1)).prepareMoveSequence(START_TIME + 10);
    verify(warriorA1, times(1)).prepareMoveSequence(START_TIME + 10);
    verify(warriorB0, times(1)).prepareMoveSequence(START_TIME + 10);
    verify(warriorB1, times(1)).prepareMoveSequence(START_TIME + 10);
  }

  /**
   * Auxiliary method that generates a MoveActionEventGeneratingExecutor initialized with two adjacent Warriors
   * warriorA0 and warriorB0
   * and an ongoing Battle between them.
   * @return a MoveActionEventGeneratingExecutor initialized with two adjacent Warriors
   * @throws GameException when MoveActionEventGeneratingExecutor creation fails
   * @throws BoardException when MoveActionEventGeneratingExecutor creation fails
   * @throws EventProcessingException when MoveActionEventGeneratingExecutor creation fails
   */
  private MoveActionEventGeneratingExecutor createWarriorA0WarriorB0BattleRoundGenerator()
      throws BoardException, GameException, EventProcessingException {
    final Battle battle0 = new Battle(warriorA0, new Position(ROAD_SECTION_0, 1), new Position(ROAD_SECTION_0, 2),
        warriorB0, new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_0, 1));
    when(mockedGame.getBattles()).thenReturn(new HashSet<>(Arrays.asList(battle0)));
    final MoveActionEventGeneratingExecutor executor =
        (new MoveActionEventGeneratingExecutor.Factory(mockedGame, mockedEventFactory))
        .create();
    PrawnInfo warriorA0PrawnInfo = new PrawnInfo(new Position(ROAD_SECTION_0, 1), false, START_TIME);
    executor.putPrawnInfo(warriorA0, warriorA0PrawnInfo);
    PrawnInfo warriorB0PrawnInfo = new PrawnInfo(new Position(ROAD_SECTION_0, 2), false, START_TIME);
    executor.putPrawnInfo(warriorB0, warriorB0PrawnInfo);
    when(mockedGame.getTime()).thenReturn(START_TIME);
    executor.prepare(new LinkedList<Event>());
    return executor;
  }

  @Test
  public void testReviseBattleBothAttacking() throws Exception {
    final MoveActionEventGeneratingExecutor executor = createWarriorA0WarriorB0BattleRoundGenerator();
    PrawnInfo warriorA0PrawnInfo = executor.getPrawnInfo(warriorA0);
    PrawnInfo warriorB0PrawnInfo;
    warriorA0PrawnInfo.setPlannedMove(new PrawnMove(START_TIME + 1, warriorA0, new Position(ROAD_SECTION_0, 1),
        new Position(ROAD_SECTION_0, 2), false));
    executor.reviseOngoingBattles(new PrawnMove(START_TIME + 2, warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 1), false));

    warriorA0PrawnInfo = executor.getPrawnInfo(warriorA0);
    warriorB0PrawnInfo = executor.getPrawnInfo(warriorB0);
    // skipRound
    assertTrue(warriorA0PrawnInfo.isSkipRound());
    assertTrue(warriorB0PrawnInfo.isSkipRound());
    // battles
    final Battle expectedBattle = new Battle(warriorA0, new Position(ROAD_SECTION_0, 1),
        new Position(ROAD_SECTION_0, 2), warriorB0, new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_0, 1));
    assertEquals(new HashSet<Battle>(Arrays.asList(expectedBattle)), executor.getBattles());
    assertEquals(new HashSet<Battle>(Arrays.asList(expectedBattle)), executor.getPrawnBattles(warriorA0));
    assertEquals(new HashSet<Battle>(Arrays.asList(expectedBattle)), executor.getPrawnBattles(warriorB0));

    assertEquals(new Position(ROAD_SECTION_0, 1), warriorA0.getCurrentPosition());
    assertEquals(new Position(ROAD_SECTION_0, 2), warriorB0.getCurrentPosition());

    verify(mockedGame, times(1)).getBattles();
    verify(mockedEventFactory, times(0)).createRemoveBattleEvent(any(Battle.class));

    assertEquals(START_TIME, warriorA0.getLastMoveTime());
    assertEquals(START_TIME, warriorB0.getLastMoveTime());
    verify(mockedEventFactory, times(0)).createStayPrawnEvent(any(Warrior.class), anyLong());

    // Order skipping.
    executor.orderSkipRoundPrawnsToStay();

    assertEquals(START_TIME + 1, warriorA0.getLastMoveTime());
    assertEquals(START_TIME, warriorB0.getLastMoveTime());
    verify(mockedEventFactory, times(1)).createStayPrawnEvent(any(Warrior.class), anyLong());
    verify(mockedEventFactory, times(1)).createStayPrawnEvent(warriorA0, START_TIME + 1);
  }

  @Test
  public void testReviseBattleCurrentAttacksOpponentNotReadyYet() throws Exception {
    final MoveActionEventGeneratingExecutor executor = createWarriorA0WarriorB0BattleRoundGenerator();
    PrawnInfo warriorA0PrawnInfo = executor.getPrawnInfo(warriorA0);
    PrawnInfo warriorB0PrawnInfo = executor.getPrawnInfo(warriorB0);
    warriorB0PrawnInfo.setPlannedMove(new PrawnMove(START_TIME + 2, warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 1), false));
    executor.reviseOngoingBattles(new PrawnMove(START_TIME + 2, warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 1), false));

    warriorA0PrawnInfo = executor.getPrawnInfo(warriorA0);
    warriorB0PrawnInfo = executor.getPrawnInfo(warriorB0);
    // skipRound
    assertTrue(warriorA0PrawnInfo.isSkipRound());
    assertTrue(warriorB0PrawnInfo.isSkipRound());
    // battles
    final Battle expectedBattle = new Battle(warriorA0, new Position(ROAD_SECTION_0, 1),
        new Position(ROAD_SECTION_0, 2), warriorB0, new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_0, 1));
    assertEquals(new HashSet<Battle>(Arrays.asList(expectedBattle)), executor.getBattles());
    assertEquals(new HashSet<Battle>(Arrays.asList(expectedBattle)), executor.getPrawnBattles(warriorA0));
    assertEquals(new HashSet<Battle>(Arrays.asList(expectedBattle)), executor.getPrawnBattles(warriorB0));

    assertEquals(new Position(ROAD_SECTION_0, 1), warriorA0.getCurrentPosition());
    assertEquals(new Position(ROAD_SECTION_0, 2), warriorB0.getCurrentPosition());

    verify(mockedGame, times(1)).getBattles();
    verify(mockedEventFactory, times(0)).createRemoveBattleEvent(any(Battle.class));

    assertEquals(START_TIME, warriorA0.getLastMoveTime());
    assertEquals(START_TIME, warriorB0.getLastMoveTime());
    verify(mockedEventFactory, times(0)).createStayPrawnEvent(any(Warrior.class), anyLong());

    // Order skipping.
    executor.orderSkipRoundPrawnsToStay();

    assertEquals(START_TIME, warriorA0.getLastMoveTime());
    assertEquals(START_TIME + 2, warriorB0.getLastMoveTime());
    verify(mockedEventFactory, times(1)).createStayPrawnEvent(any(Warrior.class), anyLong());
    verify(mockedEventFactory, times(1)).createStayPrawnEvent(warriorB0, START_TIME + 2);
  }

  @Test
  public void testReviseBattleCurrentAttacksOpponentStopped() throws Exception {
    final MoveActionEventGeneratingExecutor executor = createWarriorA0WarriorB0BattleRoundGenerator();
    PrawnInfo warriorA0PrawnInfo = new PrawnInfo(new Position(ROAD_SECTION_0, 1), true, START_TIME);
    executor.putPrawnInfo(warriorA0, warriorA0PrawnInfo);
    PrawnInfo warriorB0PrawnInfo;
    executor.reviseOngoingBattles(new PrawnMove(START_TIME + 2, warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 1), false));

    warriorA0PrawnInfo = executor.getPrawnInfo(warriorA0);
    warriorB0PrawnInfo = executor.getPrawnInfo(warriorB0);
    // skipRound
    assertTrue(warriorA0PrawnInfo.isSkipRound());
    assertTrue(warriorB0PrawnInfo.isSkipRound());
    // battles
    final Battle expectedBattle = new Battle(warriorA0, new Position(ROAD_SECTION_0, 1),
        new Position(ROAD_SECTION_0, 2), warriorB0, new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_0, 1));
    assertEquals(new HashSet<Battle>(Arrays.asList(expectedBattle)), executor.getBattles());
    assertEquals(new HashSet<Battle>(Arrays.asList(expectedBattle)), executor.getPrawnBattles(warriorA0));
    assertEquals(new HashSet<Battle>(Arrays.asList(expectedBattle)), executor.getPrawnBattles(warriorB0));

    verify(mockedGame, times(1)).getBattles();
    verify(mockedEventFactory, times(0)).createRemoveBattleEvent(any(Battle.class));

    assertEquals(new Position(ROAD_SECTION_0, 1), warriorA0.getCurrentPosition());
    assertEquals(new Position(ROAD_SECTION_0, 2), warriorB0.getCurrentPosition());

    assertEquals(START_TIME, warriorA0.getLastMoveTime());
    assertEquals(START_TIME, warriorB0.getLastMoveTime());
    verify(mockedEventFactory, times(0)).createStayPrawnEvent(any(Warrior.class), anyLong());

    // Order skipping doesn't have any effect, as no PrawnMoves are planned.
    executor.orderSkipRoundPrawnsToStay();

    assertEquals(START_TIME, warriorA0.getLastMoveTime());
    assertEquals(START_TIME, warriorB0.getLastMoveTime());
    verify(mockedEventFactory, times(0)).createStayPrawnEvent(any(Warrior.class), anyLong());
  }

  @Test
  public void testReviseBattleCurrentAttacksOpponentEscapes() throws Exception {
    final MoveActionEventGeneratingExecutor executor = createWarriorA0WarriorB0BattleRoundGenerator();
    PrawnInfo warriorA0PrawnInfo = executor.getPrawnInfo(warriorA0);
    PrawnInfo warriorB0PrawnInfo;
    warriorA0PrawnInfo.setPlannedMove(new PrawnMove(START_TIME + 1, warriorA0, new Position(ROAD_SECTION_0, 1),
        new Position(ROAD_SECTION_0, 0), false));
    executor.reviseOngoingBattles(new PrawnMove(START_TIME + 2, warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 1), false));

    warriorA0PrawnInfo = executor.getPrawnInfo(warriorA0);
    warriorB0PrawnInfo = executor.getPrawnInfo(warriorB0);
    // skipRound
    assertTrue(warriorA0PrawnInfo.isSkipRound());
    assertTrue(warriorB0PrawnInfo.isSkipRound());
    // battles
    final Battle expectedBattle = new Battle(warriorA0, new Position(ROAD_SECTION_0, 1),
        new Position(ROAD_SECTION_0, 0), warriorB0, new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_0, 1));
    assertEquals(new HashSet<Battle>(Arrays.asList(expectedBattle)), executor.getBattles());
    assertEquals(new HashSet<Battle>(Arrays.asList(expectedBattle)), executor.getPrawnBattles(warriorA0));
    assertEquals(new HashSet<Battle>(Arrays.asList(expectedBattle)), executor.getPrawnBattles(warriorB0));

    assertEquals(new Position(ROAD_SECTION_0, 1), warriorA0.getCurrentPosition());
    assertEquals(new Position(ROAD_SECTION_0, 2), warriorB0.getCurrentPosition());

    verify(mockedGame, times(1)).getBattles();
    verify(mockedEventFactory, times(0)).createRemoveBattleEvent(any(Battle.class));

    assertEquals(START_TIME, warriorA0.getLastMoveTime());
    assertEquals(START_TIME, warriorB0.getLastMoveTime());
    verify(mockedEventFactory, times(0)).createStayPrawnEvent(any(Warrior.class), anyLong());

    // Order skipping.
    executor.orderSkipRoundPrawnsToStay();

    assertEquals(START_TIME + 1, warriorA0.getLastMoveTime());
    assertEquals(START_TIME, warriorB0.getLastMoveTime());
    verify(mockedEventFactory, times(1)).createStayPrawnEvent(any(Warrior.class), anyLong());
    verify(mockedEventFactory, times(1)).createStayPrawnEvent(warriorA0, START_TIME + 1);
  }

  @Test
  public void testReviseBattleCurrentEscapesOpponentAttacks() throws Exception {
    final MoveActionEventGeneratingExecutor executor = createWarriorA0WarriorB0BattleRoundGenerator();
    PrawnInfo warriorA0PrawnInfo = executor.getPrawnInfo(warriorA0);
    PrawnInfo warriorB0PrawnInfo = executor.getPrawnInfo(warriorB0);
    warriorA0PrawnInfo.setPlannedMove(new PrawnMove(START_TIME + 1, warriorA0, new Position(ROAD_SECTION_0, 1),
        new Position(ROAD_SECTION_0, 2), false));
    warriorB0PrawnInfo.setPlannedMove(new PrawnMove(START_TIME + 2, warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 3), false));
    executor.reviseOngoingBattles(new PrawnMove(START_TIME + 2, warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 3), false));

    warriorA0PrawnInfo = executor.getPrawnInfo(warriorA0);
    warriorB0PrawnInfo = executor.getPrawnInfo(warriorB0);
    // skipRound
    assertTrue(warriorA0PrawnInfo.isSkipRound());
    assertTrue(warriorB0PrawnInfo.isSkipRound());
    // battles
    final Battle expectedBattle = new Battle(warriorA0, new Position(ROAD_SECTION_0, 1),
        new Position(ROAD_SECTION_0, 2), warriorB0, new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_0, 1));
    assertEquals(new HashSet<Battle>(Arrays.asList(expectedBattle)), executor.getBattles());
    assertEquals(new HashSet<Battle>(Arrays.asList(expectedBattle)), executor.getPrawnBattles(warriorA0));
    assertEquals(new HashSet<Battle>(Arrays.asList(expectedBattle)), executor.getPrawnBattles(warriorB0));

    assertEquals(new Position(ROAD_SECTION_0, 1), warriorA0.getCurrentPosition());
    assertEquals(new Position(ROAD_SECTION_0, 2), warriorB0.getCurrentPosition());

    verify(mockedGame, times(1)).getBattles();
    verify(mockedEventFactory, times(0)).createRemoveBattleEvent(any(Battle.class));

    assertEquals(START_TIME, warriorA0.getLastMoveTime());
    assertEquals(START_TIME, warriorB0.getLastMoveTime());
    verify(mockedEventFactory, times(0)).createStayPrawnEvent(any(Warrior.class), anyLong());

    // Order skipping.
    executor.orderSkipRoundPrawnsToStay();

    assertEquals(START_TIME + 1, warriorA0.getLastMoveTime());
    assertEquals(START_TIME + 2, warriorB0.getLastMoveTime());
    verify(mockedEventFactory, times(2)).createStayPrawnEvent(any(Warrior.class), anyLong());
    verify(mockedEventFactory, times(1)).createStayPrawnEvent(warriorA0, START_TIME + 1);
    verify(mockedEventFactory, times(1)).createStayPrawnEvent(warriorB0, START_TIME + 2);
  }

  @Test
  public void testReviseBattleCurrentEscapesOpponentNotReadyYet() throws Exception {
    final MoveActionEventGeneratingExecutor executor = createWarriorA0WarriorB0BattleRoundGenerator();
    PrawnInfo warriorA0PrawnInfo = executor.getPrawnInfo(warriorA0);
    PrawnInfo warriorB0PrawnInfo;
    executor.reviseOngoingBattles(new PrawnMove(START_TIME + 2, warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 3), false));

    warriorA0PrawnInfo = executor.getPrawnInfo(warriorA0);
    warriorB0PrawnInfo = executor.getPrawnInfo(warriorB0);
    // skipRound
    assertTrue(!warriorA0PrawnInfo.isSkipRound());
    assertTrue(!warriorB0PrawnInfo.isSkipRound());
    // battles
    final Battle expectedBattle = new Battle(warriorA0, new Position(ROAD_SECTION_0, 1),
        new Position(ROAD_SECTION_0, 2), warriorB0, new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_0, 1));
    assertEquals(new HashSet<Battle>(Arrays.asList(expectedBattle)), executor.getBattles());
    assertEquals(new HashSet<Battle>(Arrays.asList(expectedBattle)), executor.getPrawnBattles(warriorA0));
    assertEquals(new HashSet<Battle>(Arrays.asList(expectedBattle)), executor.getPrawnBattles(warriorB0));

    assertEquals(new Position(ROAD_SECTION_0, 1), warriorA0.getCurrentPosition());
    assertEquals(new Position(ROAD_SECTION_0, 2), warriorB0.getCurrentPosition());
    assertEquals(START_TIME, warriorA0.getLastMoveTime());
    assertEquals(START_TIME, warriorB0.getLastMoveTime());

    verify(mockedGame, times(1)).getBattles();
    verify(mockedEventFactory, times(0)).createStayPrawnEvent(any(Prawn.class), anyLong());
    verify(mockedEventFactory, times(0)).createRemoveBattleEvent(any(Battle.class));
  }

  @Test
  public void testReviseBattleCurrentEscapesOpponentStopped() throws Exception {
    final MoveActionEventGeneratingExecutor executor = createWarriorA0WarriorB0BattleRoundGenerator();
    PrawnInfo warriorA0PrawnInfo = new PrawnInfo(new Position(ROAD_SECTION_0, 1), true, START_TIME);
    executor.putPrawnInfo(warriorA0, warriorA0PrawnInfo);
    PrawnInfo warriorB0PrawnInfo;
    executor.reviseOngoingBattles(new PrawnMove(START_TIME + 2, warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 3), false));

    warriorA0PrawnInfo = executor.getPrawnInfo(warriorA0);
    warriorB0PrawnInfo = executor.getPrawnInfo(warriorB0);
    // skipRound
    assertTrue(!warriorA0PrawnInfo.isSkipRound());
    assertTrue(!warriorB0PrawnInfo.isSkipRound());
    // battles
    assertEquals(new HashSet<Battle>(), executor.getBattles());
    assertEquals(new HashSet<Battle>(), executor.getPrawnBattles(warriorA0));
    assertEquals(new HashSet<Battle>(), executor.getPrawnBattles(warriorB0));

    assertEquals(new Position(ROAD_SECTION_0, 1), warriorA0.getCurrentPosition());
    assertEquals(new Position(ROAD_SECTION_0, 2), warriorB0.getCurrentPosition());
    assertEquals(START_TIME, warriorA0.getLastMoveTime());
    assertEquals(START_TIME, warriorB0.getLastMoveTime());

    verify(mockedGame, times(1)).getBattles();
    verify(mockedEventFactory, times(0)).createStayPrawnEvent(any(Prawn.class), anyLong());
    verify(mockedEventFactory, times(1)).createRemoveBattleEvent(new Battle(warriorA0, new Position(ROAD_SECTION_0, 1),
        new Position(ROAD_SECTION_0, 2), warriorB0, new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_0, 1)));
  }

  @Test
  public void testReviseBattleBothEscape() throws Exception {
    final MoveActionEventGeneratingExecutor executor = createWarriorA0WarriorB0BattleRoundGenerator();
    PrawnInfo warriorA0PrawnInfo = executor.getPrawnInfo(warriorA0);
    PrawnInfo warriorB0PrawnInfo;
    warriorA0PrawnInfo.setPlannedMove(new PrawnMove(START_TIME + 1, warriorA0, new Position(ROAD_SECTION_0, 1),
        new Position(ROAD_SECTION_0, 0), false));
    executor.reviseOngoingBattles(new PrawnMove(START_TIME + 2, warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 3), false));

    warriorA0PrawnInfo = executor.getPrawnInfo(warriorA0);
    warriorB0PrawnInfo = executor.getPrawnInfo(warriorB0);
    // skipRound
    assertTrue(!warriorA0PrawnInfo.isSkipRound());
    assertTrue(!warriorB0PrawnInfo.isSkipRound());
    // battles
    assertEquals(new HashSet<Battle>(), executor.getBattles());
    assertEquals(new HashSet<Battle>(), executor.getPrawnBattles(warriorA0));
    assertEquals(new HashSet<Battle>(), executor.getPrawnBattles(warriorB0));

    assertEquals(new Position(ROAD_SECTION_0, 1), warriorA0.getCurrentPosition());
    assertEquals(new Position(ROAD_SECTION_0, 2), warriorB0.getCurrentPosition());
    assertEquals(START_TIME, warriorA0.getLastMoveTime());
    assertEquals(START_TIME, warriorB0.getLastMoveTime());

    verify(mockedGame, times(1)).getBattles();
    verify(mockedEventFactory, times(0)).createStayPrawnEvent(any(Prawn.class), anyLong());
    verify(mockedEventFactory, times(1)).createRemoveBattleEvent(new Battle(warriorA0, new Position(ROAD_SECTION_0, 1),
        new Position(ROAD_SECTION_0, 2), warriorB0, new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_0, 1)));
  }

  /**
   * Auxiliary method that generates a MoveActionEventGeneratingExecutor used in tests of method startNewBattle().
   * @return a RoundGenerator
   * @throws BoardException when MoveActionEventGeneratingExecutor creation fails
   * @throws GameException when MoveActionEventGeneratingExecutor creation fails
   * @throws EventProcessingException when MoveActionEventGeneratingExecutor creation fails
   */
  private MoveActionEventGeneratingExecutor createStartNewBattleRoundGenerator()
      throws BoardException, GameException, EventProcessingException {
    game.addPrawn(warriorA2);
    final MoveActionEventGeneratingExecutor executor =
        spy((new MoveActionEventGeneratingExecutor.Factory(game, mockedEventFactory))
            .create());
    executor.putPrawnInfo(warriorA0, new PrawnInfo(new Position(ROAD_SECTION_0, 1), false, START_TIME));
    executor.putPrawnInfo(warriorA1, new PrawnInfo(new Position(ROAD_SECTION_0, 0), false, START_TIME));
    executor.putPrawnInfo(warriorA2, new PrawnInfo(new Position(ROAD_SECTION_2, 3), false, START_TIME));
    executor.putPrawnInfo(warriorB0, new PrawnInfo(new Position(ROAD_SECTION_0, 2), false, START_TIME));
    assertEquals(null, executor.getPrawnInfo(warriorB0).getPlannedMove());
    executor.putPrawnInfo(warriorB1, new PrawnInfo(new Position(ROAD_SECTION_1, 2), false, START_TIME));
    assertFalse(executor.getPrawnInfo(warriorA0).isSkipRound());
    assertFalse(executor.getPrawnInfo(warriorA1).isSkipRound());
    assertFalse(executor.getPrawnInfo(warriorA2).isSkipRound());
    assertFalse(executor.getPrawnInfo(warriorB0).isSkipRound());
    assertFalse(executor.getPrawnInfo(warriorB1).isSkipRound());
    when(executor.getPrawnsEverOnRoadSection(ROAD_SECTION_0))
        .thenReturn(new HashSet<>(Arrays.asList(warriorA0, warriorA1, warriorB0)));
    when(executor.getPrawnsEverOnJoint(JOINT_3_0))
        .thenReturn(new HashSet<>(Arrays.asList(warriorA2, warriorB0)));
    executor.prepare(new LinkedList<>());
    return executor;
  }

  @Test
  public void testWarriorMovesRightNoBattleYet() throws Exception {
    final MoveActionEventGeneratingExecutor executor = createStartNewBattleRoundGenerator();
    assertEquals(new HashSet<>(), executor.getBattles());
    final PrawnMove prawnMove = new PrawnMove(START_TIME + 1, warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 3), false);
    assertTrue(executor.isNextPositionTaken(prawnMove));
    executor.startNewBattles(prawnMove);

    final Battle expectedBattle = new Battle(warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 3), warriorA2, new Position(ROAD_SECTION_2, 3), null);
    // skipRound
    assertFalse(executor.getPrawnInfo(warriorA0).isSkipRound());
    assertFalse(executor.getPrawnInfo(warriorA1).isSkipRound());
    assertTrue(executor.getPrawnInfo(warriorA2).isSkipRound());
    assertTrue(executor.getPrawnInfo(warriorB0).isSkipRound());
    assertFalse(executor.getPrawnInfo(warriorB1).isSkipRound());
    // battles
    assertEquals(new HashSet<>(Arrays.asList(expectedBattle)), executor.getBattles());
    assertEquals(new HashSet<>(), executor.getPrawnBattles(warriorA0));
    assertEquals(new HashSet<>(), executor.getPrawnBattles(warriorA1));
    assertEquals(new HashSet<>(Arrays.asList(expectedBattle)), executor.getPrawnBattles(warriorA2));
    assertEquals(new HashSet<>(Arrays.asList(expectedBattle)), executor.getPrawnBattles(warriorB0));
    assertEquals(new HashSet<>(), executor.getPrawnBattles(warriorB1));
    // plannedMove
    assertEquals(null, executor.getPrawnInfo(warriorA0).getPlannedMove());
    assertEquals(null, executor.getPrawnInfo(warriorA1).getPlannedMove());
    assertEquals(null, executor.getPrawnInfo(warriorA2).getPlannedMove());
    assertEquals(prawnMove, executor.getPrawnInfo(warriorB0).getPlannedMove());
    assertEquals(null, executor.getPrawnInfo(warriorB1).getPlannedMove());

    verify(executor, times(3)).getPrawnsEverOnRoadSection(ROAD_SECTION_0);
    verify(executor, times(1)).getPrawnsEverOnJoint(null);
    verify(executor, times(2)).getPrawnsEverOnJoint(JOINT_3_0);
    verify(mockedEventFactory, times(1)).createAddBattleEvent(expectedBattle);
  }

  @Test
  public void testWarriorMovesRightBattleAlreadyExists() throws Exception {
    final Battle oldBattle = new Battle(warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 3), warriorA2, new Position(ROAD_SECTION_2, 3), null);
    game.addBattle(oldBattle);
    final MoveActionEventGeneratingExecutor executor = createStartNewBattleRoundGenerator();
    final Battle expectedBattle = new Battle(warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 3), warriorA2, new Position(ROAD_SECTION_2, 3), null);
    assertEquals(new HashSet<>(Arrays.asList(expectedBattle)), executor.getBattles());
    final PrawnMove prawnMove = new PrawnMove(START_TIME + 1, warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 3), false);
    executor.startNewBattles(prawnMove);

    // skipRound
    assertFalse(executor.getPrawnInfo(warriorA0).isSkipRound());
    assertFalse(executor.getPrawnInfo(warriorA1).isSkipRound());
    assertTrue(executor.getPrawnInfo(warriorA2).isSkipRound());
    assertTrue(executor.getPrawnInfo(warriorB0).isSkipRound());
    assertFalse(executor.getPrawnInfo(warriorB1).isSkipRound());
    // battles
    assertEquals(new HashSet<>(Arrays.asList(expectedBattle)), executor.getBattles());
    assertEquals(new HashSet<>(), executor.getPrawnBattles(warriorA0));
    assertEquals(new HashSet<>(), executor.getPrawnBattles(warriorA1));
    assertEquals(new HashSet<>(Arrays.asList(expectedBattle)), executor.getPrawnBattles(warriorA2));
    assertEquals(new HashSet<>(Arrays.asList(expectedBattle)), executor.getPrawnBattles(warriorB0));
    assertEquals(new HashSet<>(), executor.getPrawnBattles(warriorB1));
    // plannedMove
    assertEquals(null, executor.getPrawnInfo(warriorA0).getPlannedMove());
    assertEquals(null, executor.getPrawnInfo(warriorA1).getPlannedMove());
    assertEquals(null, executor.getPrawnInfo(warriorA2).getPlannedMove());
    assertEquals(prawnMove, executor.getPrawnInfo(warriorB0).getPlannedMove());
    assertEquals(null, executor.getPrawnInfo(warriorB1).getPlannedMove());

    verify(executor, times(2)).getPrawnsEverOnRoadSection(ROAD_SECTION_0);
    verify(executor, times(1)).getPrawnsEverOnJoint(null);
    verify(executor, times(1)).getPrawnsEverOnJoint(JOINT_3_0);
    verify(mockedEventFactory, times(0)).createAddBattleEvent(any(Battle.class));
  }

  @Test
  public void testWarriorMovesLeft() throws Exception {
    final MoveActionEventGeneratingExecutor executor = createStartNewBattleRoundGenerator();
    assertEquals(new HashSet<>(), executor.getBattles());
    final PrawnMove prawnMove = new PrawnMove(START_TIME + 1, warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 1), false);
    assertTrue(executor.isNextPositionTaken(prawnMove));
    executor.startNewBattles(prawnMove);

    final Battle expectedBattle = new Battle(warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 1), warriorA0, new Position(ROAD_SECTION_0, 1), null);
    // skipRound
    assertTrue(executor.getPrawnInfo(warriorA0).isSkipRound());
    assertFalse(executor.getPrawnInfo(warriorA1).isSkipRound());
    assertFalse(executor.getPrawnInfo(warriorA2).isSkipRound());
    assertTrue(executor.getPrawnInfo(warriorB0).isSkipRound());
    assertFalse(executor.getPrawnInfo(warriorB1).isSkipRound());
    // battles
    assertEquals(new HashSet<>(Arrays.asList(expectedBattle)), executor.getBattles());
    assertEquals(new HashSet<>(Arrays.asList(expectedBattle)), executor.getPrawnBattles(warriorA0));
    assertEquals(new HashSet<>(), executor.getPrawnBattles(warriorA1));
    assertEquals(new HashSet<>(), executor.getPrawnBattles(warriorA2));
    assertEquals(new HashSet<>(Arrays.asList(expectedBattle)), executor.getPrawnBattles(warriorB0));
    assertEquals(new HashSet<>(), executor.getPrawnBattles(warriorB1));
    // plannedMove
    assertEquals(null, executor.getPrawnInfo(warriorA0).getPlannedMove());
    assertEquals(null, executor.getPrawnInfo(warriorA1).getPlannedMove());
    assertEquals(null, executor.getPrawnInfo(warriorA2).getPlannedMove());
    assertEquals(prawnMove, executor.getPrawnInfo(warriorB0).getPlannedMove());
    assertEquals(null, executor.getPrawnInfo(warriorB1).getPlannedMove());

    verify(executor, times(3)).getPrawnsEverOnRoadSection(ROAD_SECTION_0);
    verify(executor, times(3)).getPrawnsEverOnJoint(null);
    verify(mockedEventFactory, times(1)).createAddBattleEvent(expectedBattle);
  }

  @Test
  public void testWarriorMovesLeftOpponentMovesToo() throws Exception {
    final MoveActionEventGeneratingExecutor executor = createStartNewBattleRoundGenerator();
    assertEquals(new HashSet<>(), executor.getBattles());
    final PrawnMove warriorA0PrawnMove = new PrawnMove(START_TIME + 1, warriorA0, new Position(ROAD_SECTION_0, 1),
        new Position(ROAD_SECTION_0, 2), false);
    executor.getPrawnInfo(warriorA0).setPlannedMove(warriorA0PrawnMove);
    final PrawnMove prawnMove = new PrawnMove(START_TIME + 1, warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 1), false);
    assertTrue(executor.isNextPositionTaken(warriorA0PrawnMove));
    assertTrue(executor.isNextPositionTaken(prawnMove));
    executor.startNewBattles(prawnMove);

    final Battle expectedBattle = new Battle(warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 1), warriorA0, new Position(ROAD_SECTION_0, 1), null);
    // skipRound
    assertTrue(executor.getPrawnInfo(warriorA0).isSkipRound());
    assertFalse(executor.getPrawnInfo(warriorA1).isSkipRound());
    assertFalse(executor.getPrawnInfo(warriorA2).isSkipRound());
    assertTrue(executor.getPrawnInfo(warriorB0).isSkipRound());
    assertFalse(executor.getPrawnInfo(warriorB1).isSkipRound());
    // battles
    assertEquals(new HashSet<>(Arrays.asList(expectedBattle)), executor.getBattles());
    assertEquals(new HashSet<>(Arrays.asList(expectedBattle)), executor.getPrawnBattles(warriorA0));
    assertEquals(new HashSet<>(), executor.getPrawnBattles(warriorA1));
    assertEquals(new HashSet<>(), executor.getPrawnBattles(warriorA2));
    assertEquals(new HashSet<>(Arrays.asList(expectedBattle)), executor.getPrawnBattles(warriorB0));
    assertEquals(new HashSet<>(), executor.getPrawnBattles(warriorB1));
    // plannedMove
    assertEquals(warriorA0PrawnMove, executor.getPrawnInfo(warriorA0).getPlannedMove());
    assertEquals(null, executor.getPrawnInfo(warriorA1).getPlannedMove());
    assertEquals(null, executor.getPrawnInfo(warriorA2).getPlannedMove());
    assertEquals(prawnMove, executor.getPrawnInfo(warriorB0).getPlannedMove());
    assertEquals(null, executor.getPrawnInfo(warriorB1).getPlannedMove());

    verify(executor, times(4)).getPrawnsEverOnRoadSection(ROAD_SECTION_0);
    verify(executor, times(4)).getPrawnsEverOnJoint(null);
    verify(mockedEventFactory, times(1)).createAddBattleEvent(expectedBattle);
  }

  /**
   * Auxiliary method that generates a MoveActionEventGeneratingExecutor used in tests of method tryPerform().
   * @return a RoundGenerator
   * @throws BoardException when MoveActionEventGeneratingExecutor creation fails
   * @throws GameException when MoveActionEventGeneratingExecutor creation fails
   * @throws EventProcessingException when MoveActionEventGeneratingExecutor creation fails
   */
  private MoveActionEventGeneratingExecutor createTryPerformRoundGenerator()
      throws BoardException, GameException, EventProcessingException {
    final Battle initialBattle = new Battle(warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 3), warriorA2, new Position(ROAD_SECTION_2, 3), null);
    game.addBattle(initialBattle);
    final MoveActionEventGeneratingExecutor executor =
        spy((new MoveActionEventGeneratingExecutor.Factory(game, mockedEventFactory))
            .create());
    executor.prepare(new LinkedList<>());
    assertEquals(new HashSet<>(Arrays.asList(initialBattle)), executor.getBattles());
    assertEquals(new HashSet<>(), executor.getPrawnBattles(warriorA0));
    assertEquals(new HashSet<>(Arrays.asList(initialBattle)), executor.getPrawnBattles(warriorA2));
    assertEquals(new HashSet<>(Arrays.asList(initialBattle)), executor.getPrawnBattles(warriorB0));
    executor.putPrawnInfo(warriorA0, new PrawnInfo(new Position(ROAD_SECTION_0, 1), false, START_TIME));
    executor.putPrawnInfo(warriorA2, new PrawnInfo(new Position(ROAD_SECTION_2, 3), false, START_TIME));
    executor.putPrawnInfo(warriorB0, new PrawnInfo(new Position(ROAD_SECTION_0, 2), false, START_TIME));
    return executor;
  }

  @Test
  public void testOneBattleFinishedNewBattleStarted() throws Exception {
    final MoveActionEventGeneratingExecutor executor = createTryPerformRoundGenerator();
    final PrawnMove prawnMove = new PrawnMove(START_TIME + 1, warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 1), false);
    executor.getPrawnInfo(warriorB0).setPlannedMove(prawnMove);
    final Battle initialBattle = new Battle(warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 3), warriorA2, new Position(ROAD_SECTION_2, 3), null);
    final Battle newBattle = new Battle(warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 1), warriorA0, new Position(ROAD_SECTION_0, 1), null);
    doAnswer(new Answer<Object>() {
      public Object answer(InvocationOnMock invocation) {
        MoveActionEventGeneratingExecutor executor = (MoveActionEventGeneratingExecutor) invocation.getMock();
        try {
          assertTrue(executor.getBattles().contains(initialBattle));
          executor.getBattles().remove(initialBattle);
          executor.getPrawnBattles(warriorB0).remove(initialBattle);
          executor.getPrawnBattles(warriorA2).remove(initialBattle);
        } catch (Exception exception) {
          fail("Should never happen: " + exception);
        }
        return null;
      }
    }).when(executor).reviseOngoingBattles(prawnMove);
    doAnswer(new Answer<Object>() {
      public Object answer(InvocationOnMock invocation) {
        MoveActionEventGeneratingExecutor executor = (MoveActionEventGeneratingExecutor) invocation.getMock();
        try {
          executor.getBattles().add(newBattle);
          executor.getPrawnBattles(warriorB0).add(newBattle);
          executor.getPrawnBattles(warriorA0).add(newBattle);
          executor.getPrawnInfo(warriorB0).setSkipRound(true);
          executor.getPrawnInfo(warriorA0).setSkipRound(true);
        } catch (Exception exception) {
          fail("Should never happen: " + exception);
        }
        return null;
      }
    }).when(executor).startNewBattles(prawnMove);
    assertFalse(executor.tryPerform(prawnMove));
    // battles
    assertEquals(new HashSet<>(Arrays.asList(newBattle)), executor.getBattles());
    assertEquals(new HashSet<>(Arrays.asList(newBattle)), executor.getPrawnBattles(warriorA0));
    assertEquals(new HashSet<>(), executor.getPrawnBattles(warriorA2));
    assertEquals(new HashSet<>(Arrays.asList(newBattle)), executor.getPrawnBattles(warriorB0));
    // prawnInfo
    final PrawnInfo warriorB0Info = executor.getPrawnInfo(warriorB0);
    assertEquals(new Position(ROAD_SECTION_0, 2), warriorB0Info.getCurrentPosition());
    assertEquals(prawnMove, warriorB0Info.getPlannedMove());
    assertEquals(START_TIME, warriorB0Info.getTime());
    // skipRound
    assertTrue(executor.getPrawnInfo(warriorA0).isSkipRound());
    assertFalse(executor.getPrawnInfo(warriorA2).isSkipRound());
    assertTrue(warriorB0Info.isSkipRound());

    verify(executor, times(1)).reviseOngoingBattles(prawnMove);
    verify(executor, times(1)).startNewBattles(prawnMove);
    verify(executor, times(1)).isNextPositionTaken(prawnMove);
    verify(mockedEventFactory, times(0)).createMovePrawnEvent(any(Prawn.class), any(Move.class));
    verify(executor, times(0)).tryCaptureOrDestroyCity(any(Prawn.class));
  }

  @Test
  public void testPlannedMoveNotSet() throws Exception {
    final MoveActionEventGeneratingExecutor executor = createTryPerformRoundGenerator();
    final PrawnMove prawnMove = new PrawnMove(START_TIME + 1, warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 1), false);
    final Battle initialBattle = new Battle(warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 3), warriorA2, new Position(ROAD_SECTION_2, 3), null);
    final Battle newBattle = new Battle(warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 1), warriorA0, new Position(ROAD_SECTION_0, 1), null);
    doAnswer(new Answer<Object>() {
      public Object answer(InvocationOnMock invocation) {
        MoveActionEventGeneratingExecutor executor = (MoveActionEventGeneratingExecutor) invocation.getMock();
        try {
          assertTrue(executor.getBattles().contains(initialBattle));
          executor.getBattles().remove(initialBattle);
          executor.getPrawnBattles(warriorB0).remove(initialBattle);
          executor.getPrawnBattles(warriorA2).remove(initialBattle);
        } catch (Exception exception) {
          fail("Should never happen: " + exception);
        }
        return null;
      }
    }).when(executor).reviseOngoingBattles(prawnMove);
    doAnswer(new Answer<Object>() {
      public Object answer(InvocationOnMock invocation) {
        MoveActionEventGeneratingExecutor executor = (MoveActionEventGeneratingExecutor) invocation.getMock();
        try {
          executor.getBattles().add(newBattle);
          executor.getPrawnBattles(warriorB0).add(newBattle);
          executor.getPrawnBattles(warriorA0).add(newBattle);
          executor.getPrawnInfo(warriorB0).setSkipRound(true);
          executor.getPrawnInfo(warriorA0).setSkipRound(true);
        } catch (Exception exception) {
          fail("Should never happen: " + exception);
        }
        return null;
      }
    }).when(executor).startNewBattles(prawnMove);
    assertFalse(executor.tryPerform(prawnMove));
    // battles
    assertEquals(new HashSet<>(Arrays.asList(newBattle)), executor.getBattles());
    assertEquals(new HashSet<>(Arrays.asList(newBattle)), executor.getPrawnBattles(warriorA0));
    assertEquals(new HashSet<>(), executor.getPrawnBattles(warriorA2));
    assertEquals(new HashSet<>(Arrays.asList(newBattle)), executor.getPrawnBattles(warriorB0));
    // prawnInfo
    final PrawnInfo warriorB0Info = executor.getPrawnInfo(warriorB0);
    assertEquals(new Position(ROAD_SECTION_0, 2), warriorB0Info.getCurrentPosition());
    assertEquals(null, warriorB0Info.getPlannedMove());
    assertEquals(START_TIME, warriorB0Info.getTime());
    // skipRound
    assertTrue(executor.getPrawnInfo(warriorA0).isSkipRound());
    assertFalse(executor.getPrawnInfo(warriorA2).isSkipRound());
    assertTrue(warriorB0Info.isSkipRound());

    verify(executor, times(1)).reviseOngoingBattles(prawnMove);
    verify(executor, times(1)).startNewBattles(prawnMove);
    verify(executor, times(1)).isNextPositionTaken(prawnMove);
    verify(mockedEventFactory, times(0)).createMovePrawnEvent(any(Prawn.class), any(Move.class));
    verify(executor, times(0)).tryCaptureOrDestroyCity(any(Prawn.class));
  }

  @Test
  public void testBattleStaysUnconfirmed() throws Exception {
    final MoveActionEventGeneratingExecutor executor = createTryPerformRoundGenerator();
    final PrawnMove prawnMove = new PrawnMove(START_TIME + 1, warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 1), false);
    executor.getPrawnInfo(warriorB0).setPlannedMove(prawnMove);
    doAnswer(new Answer<Object>() {
      public Object answer(InvocationOnMock invocation) {
        return null;
      }
    }).when(executor).reviseOngoingBattles(prawnMove);
    doAnswer(new Answer<Object>() {
      public Object answer(InvocationOnMock invocation) {
        return null;
      }
    }).when(executor).startNewBattles(prawnMove);
    assertFalse(executor.tryPerform(prawnMove));
    // battles
    final Battle initialBattle = new Battle(warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 3), warriorA2, new Position(ROAD_SECTION_2, 3), null);
    assertEquals(new HashSet<>(Arrays.asList(initialBattle)), executor.getBattles());
    assertEquals(new HashSet<>(), executor.getPrawnBattles(warriorA0));
    assertEquals(new HashSet<>(Arrays.asList(initialBattle)), executor.getPrawnBattles(warriorA2));
    assertEquals(new HashSet<>(Arrays.asList(initialBattle)), executor.getPrawnBattles(warriorB0));
    // prawnInfo
    final PrawnInfo warriorB0Info = executor.getPrawnInfo(warriorB0);
    assertEquals(new Position(ROAD_SECTION_0, 2), warriorB0Info.getCurrentPosition());
    assertEquals(prawnMove, warriorB0Info.getPlannedMove());
    assertEquals(START_TIME, warriorB0Info.getTime());
    // skipRound
    assertFalse(executor.getPrawnInfo(warriorA0).isSkipRound());
    assertFalse(executor.getPrawnInfo(warriorA2).isSkipRound());
    assertFalse(warriorB0Info.isSkipRound());

    verify(executor, times(1)).reviseOngoingBattles(prawnMove);
    verify(executor, times(1)).startNewBattles(prawnMove);
    verify(executor, times(1)).isNextPositionTaken(prawnMove);
    verify(mockedEventFactory, times(0)).createMovePrawnEvent(any(Prawn.class), any(Move.class));
    verify(executor, times(0)).tryCaptureOrDestroyCity(any(Prawn.class));
  }

  @Test
  public void testOneBattleConfirmed() throws Exception {
    final MoveActionEventGeneratingExecutor executor = createTryPerformRoundGenerator();
    final PrawnMove prawnMove = new PrawnMove(START_TIME + 1, warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 1), false);
    executor.getPrawnInfo(warriorB0).setPlannedMove(prawnMove);
    final Battle initialBattle = new Battle(warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 3), warriorA2, new Position(ROAD_SECTION_2, 3), null);
    doAnswer(new Answer<Object>() {
      public Object answer(InvocationOnMock invocation) {
        try {
          executor.getPrawnInfo(warriorB0).setSkipRound(true);
          executor.getPrawnInfo(warriorA2).setSkipRound(true);
        } catch (Exception exception) {
          fail("Should never happen: " + exception);
        }
        return null;
      }
    }).when(executor).reviseOngoingBattles(prawnMove);
    doAnswer(new Answer<Object>() {
      public Object answer(InvocationOnMock invocation) {
        return null;
      }
    }).when(executor).startNewBattles(prawnMove);
    assertFalse(executor.tryPerform(prawnMove));
    // battles
    assertEquals(new HashSet<>(Arrays.asList(initialBattle)), executor.getBattles());
    assertEquals(new HashSet<>(), executor.getPrawnBattles(warriorA0));
    assertEquals(new HashSet<>(Arrays.asList(initialBattle)), executor.getPrawnBattles(warriorA2));
    assertEquals(new HashSet<>(Arrays.asList(initialBattle)), executor.getPrawnBattles(warriorB0));
    // prawnInfo
    final PrawnInfo warriorB0Info = executor.getPrawnInfo(warriorB0);
    assertEquals(new Position(ROAD_SECTION_0, 2), warriorB0Info.getCurrentPosition());
    assertEquals(prawnMove, warriorB0Info.getPlannedMove());
    assertEquals(START_TIME, warriorB0Info.getTime());
    // skipRound
    assertFalse(executor.getPrawnInfo(warriorA0).isSkipRound());
    assertTrue(executor.getPrawnInfo(warriorA2).isSkipRound());
    assertTrue(warriorB0Info.isSkipRound());

    verify(executor, times(1)).reviseOngoingBattles(prawnMove);
    verify(executor, times(1)).startNewBattles(prawnMove);
    verify(executor, times(1)).isNextPositionTaken(prawnMove);
    verify(mockedEventFactory, times(0)).createMovePrawnEvent(any(Prawn.class), any(Move.class));
    verify(executor, times(0)).tryCaptureOrDestroyCity(any(Prawn.class));
  }

  @Test
  public void testBattleRemovedMovePerformed() throws Exception {
    final MoveActionEventGeneratingExecutor executor = createTryPerformRoundGenerator();
    final PrawnMove prawnMove = new PrawnMove(START_TIME + 1, warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_1, 0), false);
    executor.getPrawnInfo(warriorB0).setPlannedMove(prawnMove);
    final Battle initialBattle = new Battle(warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 3), warriorA2, new Position(ROAD_SECTION_2, 3), null);
    doAnswer(new Answer<Object>() {
      public Object answer(InvocationOnMock invocation) {
        MoveActionEventGeneratingExecutor executor = (MoveActionEventGeneratingExecutor) invocation.getMock();
        try {
          assertTrue(executor.getBattles().contains(initialBattle));
          executor.getBattles().remove(initialBattle);
          executor.getPrawnBattles(warriorB0).remove(initialBattle);
          executor.getPrawnBattles(warriorA2).remove(initialBattle);
        } catch (Exception exception) {
          fail("Should never happen: " + exception);
        }
        return null;
      }
    }).when(executor).reviseOngoingBattles(prawnMove);
    doAnswer(new Answer<Object>() {
      public Object answer(InvocationOnMock invocation) {
        return null;
      }
    }).when(executor).startNewBattles(prawnMove);
    /*
    doAnswer(new Answer<Object>() {
      public Object answer(InvocationOnMock invocation) {
        return null;
      }
    }).when(mockedEventFactory).advanceTimeTo(START_TIME + 1);
    */
    assertTrue(executor.tryPerform(prawnMove));
    // battles
    assertEquals(new HashSet<>(), executor.getBattles());
    assertEquals(new HashSet<>(), executor.getPrawnBattles(warriorA0));
    assertEquals(new HashSet<>(), executor.getPrawnBattles(warriorA2));
    assertEquals(new HashSet<>(), executor.getPrawnBattles(warriorB0));
    // prawnInfo
    final PrawnInfo warriorB0Info = executor.getPrawnInfo(warriorB0);
    assertEquals(new Position(ROAD_SECTION_1, 0), warriorB0Info.getCurrentPosition());
    assertEquals(null, warriorB0Info.getPlannedMove());
    assertEquals(START_TIME + 1, warriorB0Info.getTime());
    // skipRound
    assertFalse(executor.getPrawnInfo(warriorA0).isSkipRound());
    assertFalse(executor.getPrawnInfo(warriorA2).isSkipRound());
    assertFalse(warriorB0Info.isSkipRound());

    verify(executor, times(1)).reviseOngoingBattles(prawnMove);
    verify(executor, times(1)).startNewBattles(prawnMove);
    verify(executor, times(1)).isNextPositionTaken(prawnMove);
    verify(mockedEventFactory, times(1)).createMovePrawnEvent(warriorB0, new Move(new Position(ROAD_SECTION_1, 0),
        START_TIME + 1, false));
    verify(executor, times(1)).tryCaptureOrDestroyCity(warriorB0);
  }

  @Test
  public void testBattleConfirmed() throws Exception {
    final MoveActionEventGeneratingExecutor executor = createTryPerformRoundGenerator();
    final PrawnMove prawnMove = new PrawnMove(START_TIME + 1, warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 1), false);
    executor.getPrawnInfo(warriorB0).setPlannedMove(prawnMove);
    executor.getPrawnInfo(warriorB0).setSkipRound(true);
    final Battle initialBattle = new Battle(warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 3), warriorA2, new Position(ROAD_SECTION_2, 3), null);
    assertFalse(executor.tryPerform(prawnMove));
    // battles
    assertEquals(new HashSet<>(Arrays.asList(initialBattle)), executor.getBattles());
    assertEquals(new HashSet<>(), executor.getPrawnBattles(warriorA0));
    assertEquals(new HashSet<>(Arrays.asList(initialBattle)), executor.getPrawnBattles(warriorA2));
    assertEquals(new HashSet<>(Arrays.asList(initialBattle)), executor.getPrawnBattles(warriorB0));
    // prawnInfo
    final PrawnInfo warriorB0Info = executor.getPrawnInfo(warriorB0);
    assertEquals(new Position(ROAD_SECTION_0, 2), warriorB0Info.getCurrentPosition());
    assertEquals(prawnMove, warriorB0Info.getPlannedMove());
    assertEquals(START_TIME, warriorB0Info.getTime());
    // skipRound
    assertFalse(executor.getPrawnInfo(warriorA0).isSkipRound());
    assertFalse(executor.getPrawnInfo(warriorA2).isSkipRound());
    assertTrue(warriorB0Info.isSkipRound());

    verify(executor, times(0)).reviseOngoingBattles(prawnMove);
    verify(executor, times(0)).startNewBattles(prawnMove);
    verify(executor, times(0)).isNextPositionTaken(prawnMove);
    verify(mockedEventFactory, times(0)).createMovePrawnEvent(any(Prawn.class), any(Move.class));
    verify(mockedEventFactory, times(0)).createRemoveBattleEvent(any(Battle.class));
    verify(executor, times(0)).tryCaptureOrDestroyCity(any(Prawn.class));
  }

  /**
   * Auxiliary method that generates a MoveActionEventGeneratingExecutor with a simple scenario used in tests of method
   * generate().
   * @return a RoundGenerator
   * @throws GameException when MoveActionEventGeneratingExecutor creation fails
   * @throws EventProcessingException when MoveActionEventGeneratingExecutor creation fails
   */
  private MoveActionEventGeneratingExecutor createScenarioRoundGenerator()
      throws GameException, EventProcessingException {
    doAnswer(new Answer<Move[]>() {
      @Override
      public Move[] answer(InvocationOnMock invocation) throws Throwable {
        return new Move[]{
            new Move(new Position(ROAD_SECTION_0, 1), START_TIME, false),
            new Move(new Position(ROAD_SECTION_0, 2), START_TIME + 2, false),
            new Move(new Position(ROAD_SECTION_0, 3), START_TIME + 3, true),
        };
      }
    }).when(warriorA0).prepareMoveSequence(START_TIME + 4);
    doAnswer(new Answer<Move[]>() {
      @Override
      public Move[] answer(InvocationOnMock invocation) throws Throwable {
        return new Move[]{
            new Move(POSITION_0, START_TIME, true),
            new Move(new Position(ROAD_SECTION_0, 1), START_TIME + 1, true),
        };
      }
    }).when(warriorA1).prepareMoveSequence(START_TIME + 4);
    doAnswer(new Answer<Move[]>() {
      @Override
      public Move[] answer(InvocationOnMock invocation) throws Throwable {
        return new Move[]{
            new Move(new Position(ROAD_SECTION_0, 2), START_TIME, false),
            new Move(new Position(ROAD_SECTION_0, 3), START_TIME + 1, false),
        };
      }
    }).when(warriorB0).prepareMoveSequence(START_TIME + 4);
    game.addPrawn(warriorA2);
    doAnswer(new Answer<Move[]>() {
      @Override
      public Move[] answer(InvocationOnMock invocation) throws Throwable {
        return new Move[]{
            new Move(new Position(ROAD_SECTION_1, 0), START_TIME, false),
            new Move(new Position(ROAD_SECTION_1, 1), START_TIME + 1, false),
            new Move(new Position(ROAD_SECTION_1, 0), START_TIME + 2, false),
        };
      }
    }).when(warriorA2).prepareMoveSequence(START_TIME + 4);
    doAnswer(new Answer<Move[]>() {
      @Override
      public Move[] answer(InvocationOnMock invocation) throws Throwable {
        return new Move[]{
            new Move(new Position(ROAD_SECTION_2, 2), START_TIME, false),
            new Move(new Position(ROAD_SECTION_2, 3), START_TIME + 3, false),
            new Move(new Position(ROAD_SECTION_2, 2), START_TIME + 4, false),
        };
      }
    }).when(warriorB1).prepareMoveSequence(START_TIME + 4);
    final MoveActionEventGeneratingExecutor executor =
        spy((new MoveActionEventGeneratingExecutor.Factory(game, eventFactory))
            .create());
    assertEquals(State.NOT_PREPARED, executor.getState());
    game.advanceTimeTo(START_TIME + 4);
    executor.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    assertEquals(State.PREPARED, executor.getState());
    return executor;
  }

  /**
   * Runs the body of tests on a scenario testing method generate(). Depending on the value of mockTryPerform, either
   * calls to tryPerform() are mocked out or not.
   * @param mockTryPerform determines whether calls to tryPerform() are mocked out or not
   * @throws Exception when something unexpected happens
   */
  private void runTestGeneratedScenarioWorks(boolean mockTryPerform) throws Exception {
    final MoveActionEventGeneratingExecutor executor = createScenarioRoundGenerator();
    assertEquals(new HashSet<>(), game.getBattles());
    final PrawnMove warriorA0Move0 =
        new PrawnMove(START_TIME + 2, warriorA0, new Position(ROAD_SECTION_0, 1), new Position(ROAD_SECTION_0, 2),
            false);
    final PrawnMove warriorA0Move1 =
        new PrawnMove(START_TIME + 3, warriorA0, new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_0, 3),
            true);
    final PrawnMove warriorA1Move0 =
        new PrawnMove(START_TIME + 1, warriorA1, new Position(ROAD_SECTION_0, 0), new Position(ROAD_SECTION_0, 1),
            true);
    final PrawnMove warriorA2Move0 =
        new PrawnMove(START_TIME + 1, warriorA2, new Position(ROAD_SECTION_1, 0), new Position(ROAD_SECTION_1, 1),
            false);
    final PrawnMove warriorA2Move1 =
        new PrawnMove(START_TIME + 2, warriorA2, new Position(ROAD_SECTION_1, 1), new Position(ROAD_SECTION_1, 0),
            false);
    final PrawnMove warriorB0Move0 =
        new PrawnMove(START_TIME + 1, warriorB0, new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_0, 3),
            false);
    final PrawnMove warriorB1Move0 =
        new PrawnMove(START_TIME + 3, warriorB1, new Position(ROAD_SECTION_2, 2), new Position(ROAD_SECTION_2, 3),
            false);
    final PrawnMove warriorB1Move1 =
        new PrawnMove(START_TIME + 4, warriorB1, new Position(ROAD_SECTION_2, 3), new Position(ROAD_SECTION_2, 2),
            false);
    final Battle battleA0B0 = new Battle(warriorA0, warriorA0Move1.getInitialPosition(),
        warriorA0Move1.getNextPosition(), warriorB0, new Position(ROAD_SECTION_0, 3), null);
    final Battle battleA2B0 = new Battle(warriorA2, warriorA2Move1.getInitialPosition(),
        warriorA2Move1.getNextPosition(), warriorB0, new Position(ROAD_SECTION_0, 3), null);

    // Mocking out calls to tryPerform().
    if (mockTryPerform) {
      doAnswer(new Answer<Boolean>() {
        public Boolean answer(InvocationOnMock invocation) {
          final PrawnMove prawnMove = warriorA0Move0;
          executor.putPrawnInfo(prawnMove.getPrawn(),
              new PrawnInfo(prawnMove.getNextPosition(), prawnMove.isStop(), prawnMove.getTime()));
          return true;
        }
      }).when(executor).tryPerform(warriorA0Move0);
      doAnswer(new Answer<Boolean>() {
        public Boolean answer(InvocationOnMock invocation) {
          try {
            executor.getBattles().add(battleA0B0);
            executor.getPrawnBattles(warriorA0).add(battleA0B0);
            executor.getPrawnBattles(warriorB0).add(battleA0B0);
            executor.getPrawnInfo(warriorA0).setPlannedMove(warriorA0Move1);
            executor.getPrawnInfo(warriorA0).setSkipRound(true);
            executor.getPrawnInfo(warriorB0).setSkipRound(true);
          } catch (GameException exception) {
            fail("Should never happen: " + exception);
          }
          return false;
        }
      }).when(executor).tryPerform(warriorA0Move1);
      doAnswer(new Answer<Boolean>() {
        public Boolean answer(InvocationOnMock invocation) {
          final PrawnMove prawnMove = warriorA1Move0;
          executor.putPrawnInfo(prawnMove.getPrawn(),
              new PrawnInfo(prawnMove.getNextPosition(), prawnMove.isStop(), prawnMove.getTime()));
          return true;
        }
      }).when(executor).tryPerform(warriorA1Move0);
      doAnswer(new Answer<Boolean>() {
        public Boolean answer(InvocationOnMock invocation) {
          final PrawnMove prawnMove = warriorA2Move0;
          executor.putPrawnInfo(prawnMove.getPrawn(),
              new PrawnInfo(prawnMove.getNextPosition(), prawnMove.isStop(), prawnMove.getTime()));
          return true;
        }
      }).when(executor).tryPerform(warriorA2Move0);
      doAnswer(new Answer<Boolean>() {
        public Boolean answer(InvocationOnMock invocation) {
          try {
            executor.getBattles().add(battleA2B0);
            executor.getPrawnBattles(warriorA2).add(battleA2B0);
            executor.getPrawnBattles(warriorB0).add(battleA2B0);
            executor.getPrawnInfo(warriorA2).setPlannedMove(warriorA2Move1);
            executor.getPrawnInfo(warriorA2).setSkipRound(true);
          } catch (GameException exception) {
            fail("Should never happen: " + exception);
          }
          return false;
        }
      }).when(executor).tryPerform(warriorA2Move1);
      doAnswer(new Answer<Boolean>() {
        public Boolean answer(InvocationOnMock invocation) {
          final PrawnMove prawnMove = warriorB0Move0;
          executor.putPrawnInfo(prawnMove.getPrawn(),
              new PrawnInfo(prawnMove.getNextPosition(), prawnMove.isStop(), prawnMove.getTime()));
          return true;
        }
      }).when(executor).tryPerform(warriorB0Move0);
      doAnswer(new Answer<Boolean>() {
        public Boolean answer(InvocationOnMock invocation) {
          final PrawnMove prawnMove = warriorB1Move0;
          executor.putPrawnInfo(prawnMove.getPrawn(),
              new PrawnInfo(prawnMove.getNextPosition(), prawnMove.isStop(), prawnMove.getTime()));
          return true;
        }
      }).when(executor).tryPerform(warriorB1Move0);
      doAnswer(new Answer<Boolean>() {
        public Boolean answer(InvocationOnMock invocation) {
          final PrawnMove prawnMove = warriorB1Move1;
          executor.putPrawnInfo(prawnMove.getPrawn(),
              new PrawnInfo(prawnMove.getNextPosition(), prawnMove.isStop(), prawnMove.getTime()));
          return true;
        }
      }).when(executor).tryPerform(warriorB1Move1);
    }

    executor.executeAndGenerate();

    // battles
    assertEquals(new HashSet<>(Arrays.asList(battleA0B0, battleA2B0)), executor.getBattles());
    assertEquals(new HashSet<>(Arrays.asList(battleA0B0)), executor.getPrawnBattles(warriorA0));
    assertEquals(new HashSet<>(), executor.getPrawnBattles(warriorA1));
    assertEquals(new HashSet<>(Arrays.asList(battleA2B0)), executor.getPrawnBattles(warriorA2));
    assertEquals(new HashSet<>(Arrays.asList(battleA0B0, battleA2B0)), executor.getPrawnBattles(warriorB0));
    assertEquals(new HashSet<>(), executor.getPrawnBattles(warriorB1));
    // prawnInfo
    final PrawnInfo warriorA0Info = executor.getPrawnInfo(warriorA0);
    final PrawnInfo warriorA1Info = executor.getPrawnInfo(warriorA1);
    final PrawnInfo warriorA2Info = executor.getPrawnInfo(warriorA2);
    final PrawnInfo warriorB0Info = executor.getPrawnInfo(warriorB0);
    final PrawnInfo warriorB1Info = executor.getPrawnInfo(warriorB1);
    final PrawnInfo expectedWarriorA0Info = new PrawnInfo(warriorA0Move0.getNextPosition(), warriorA0Move0.isStop(),
        warriorA0Move0.getTime());  // NOTE: warriorA0Move0, not warriorA0Move1
    final PrawnInfo expectedWarriorA1Info = new PrawnInfo(warriorA1Move0.getNextPosition(), warriorA1Move0.isStop(),
        warriorA1Move0.getTime());
    final PrawnInfo expectedWarriorA2Info = new PrawnInfo(warriorA2Move0.getNextPosition(), warriorA2Move0.isStop(),
        warriorA2Move0.getTime());  // NOTE: warriorA2Move0, not warriorA2Move1
    final PrawnInfo expectedWarriorB0Info = new PrawnInfo(warriorB0Move0.getNextPosition(), warriorB0Move0.isStop(),
        warriorB0Move0.getTime());
    final PrawnInfo expectedWarriorB1Info = new PrawnInfo(warriorB1Move1.getNextPosition(), warriorB1Move1.isStop(),
        warriorB1Move1.getTime());
    // -> skipRound
    expectedWarriorA0Info.setSkipRound(true);
    expectedWarriorA1Info.setSkipRound(false);
    expectedWarriorA2Info.setSkipRound(true);
    expectedWarriorB0Info.setSkipRound(true);
    expectedWarriorB1Info.setSkipRound(false);
    // -> plannedMove
    expectedWarriorA0Info.setPlannedMove(warriorA0Move1);
    expectedWarriorA2Info.setPlannedMove(warriorA2Move1);
    //
    assertEquals(expectedWarriorA0Info, warriorA0Info);
    assertEquals(expectedWarriorA1Info, warriorA1Info);
    assertEquals(expectedWarriorA2Info, warriorA2Info);
    assertEquals(expectedWarriorB0Info, warriorB0Info);
    assertEquals(expectedWarriorB1Info, warriorB1Info);

    // game.battles
    assertEquals(new HashSet<>(Arrays.asList(battleA0B0, battleA2B0)), game.getBattles());

    verify(executor, times(1)).tryPerform(warriorA1Move0);
    verify(executor, times(1)).tryPerform(warriorA2Move0);
    verify(executor, times(1)).tryPerform(warriorB0Move0);
    verify(executor, times(1)).tryPerform(warriorA0Move0);
    verify(executor, times(1)).tryPerform(warriorA2Move1);
    verify(executor, times(1)).tryPerform(warriorA0Move1);
    verify(executor, times(1)).tryPerform(warriorB1Move0);
    verify(executor, times(1)).tryPerform(warriorB1Move1);

    if (!mockTryPerform) {
      assertEquals(Arrays.asList(
          TEST_EVENT,
          // warriorA1Move0
          new Event(Json.createObjectBuilder()
              .add("type", "MOVE_PRAWN")
              .add("time", START_TIME + 4)
              .add("prawn", 1)
              .add("move", Json.createObjectBuilder()
                  .add("position", Json.createObjectBuilder()
                      .add("roadSection", 0)
                      .add("index", 1)
                      .build())
                  .add("moveTime", START_TIME + 1)
                  .add("stop", true)
                  .build())
              .build()),
          // warriorA2Move0
          new Event(Json.createObjectBuilder()
              .add("type", "MOVE_PRAWN")
              .add("time", START_TIME + 4)
              .add("prawn", 2)
              .add("move", Json.createObjectBuilder()
                  .add("position", Json.createObjectBuilder()
                      .add("roadSection", 1)
                      .add("index", 1)
                      .build())
                  .add("moveTime", START_TIME + 1)
                  .add("stop", false)
                  .build())
              .build()),
          // warriorB0Move0
          new Event(Json.createObjectBuilder()
              .add("type", "MOVE_PRAWN")
              .add("time", START_TIME + 4)
              .add("prawn", 3)
              .add("move", Json.createObjectBuilder()
                  .add("position", Json.createObjectBuilder()
                      .add("roadSection", 0)
                      .add("index", 3)
                      .build())
                  .add("moveTime", START_TIME + 1)
                  .add("stop", false)
                  .build())
              .build()),
          // warriorA0Move0
          new Event(Json.createObjectBuilder()
              .add("type", "MOVE_PRAWN")
              .add("time", START_TIME + 4)
              .add("prawn", 0)
              .add("move", Json.createObjectBuilder()
                  .add("position", Json.createObjectBuilder()
                      .add("roadSection", 0)
                      .add("index", 2)
                      .build())
                  .add("moveTime", START_TIME + 2)
                  .add("stop", false)
                  .build())
              .build()),
          // Add Battle warriorA2 vs. warriorB0
          new Event(Json.createObjectBuilder()
              .add("type", "ADD_BATTLE")
              .add("time", START_TIME + 4)
              .add("battle", Json.createObjectBuilder()
                  .add("prawn0", 2)
                  .add("prawn1", 3)
                  .add("normalizedPosition0", Json.createObjectBuilder()
                      .add("roadSection", 1)
                      .add("index", 1)
                      .build())
                  .add("normalizedPosition1", Json.createObjectBuilder()
                      .add("roadSection", 1)
                      .add("index", 0)
                      .build())
                  .build())
              .build()),
          // Add Battle warriorA0 vs. warriorB0
          new Event(Json.createObjectBuilder()
              .add("type", "ADD_BATTLE")
              .add("time", START_TIME + 4)
              .add("battle", Json.createObjectBuilder()
                  .add("prawn0", 0)
                  .add("prawn1", 3)
                  .add("normalizedPosition0", Json.createObjectBuilder()
                      .add("roadSection", 0)
                      .add("index", 2)
                      .build())
                  .add("normalizedPosition1", Json.createObjectBuilder()
                      .add("roadSection", 0)
                      .add("index", 3)
                      .build())
                  .build())
              .build()),
          // warriorB1Move0
          new Event(Json.createObjectBuilder()
              .add("type", "MOVE_PRAWN")
              .add("time", START_TIME + 4)
              .add("prawn", 4)
              .add("move", Json.createObjectBuilder()
                  .add("position", Json.createObjectBuilder()
                      .add("roadSection", 2)
                      .add("index", 3)
                      .build())
                  .add("moveTime", START_TIME + 3)
                  .add("stop", false)
                  .build())
              .build()),
          // warriorB1Move1
          new Event(Json.createObjectBuilder()
              .add("type", "MOVE_PRAWN")
              .add("time", START_TIME + 4)
              .add("prawn", 4)
              .add("move", Json.createObjectBuilder()
                  .add("position", Json.createObjectBuilder()
                      .add("roadSection", 2)
                      .add("index", 2)
                      .build())
                  .add("moveTime", START_TIME + 4)
                  .add("stop", false)
                  .build())
              .build()),
          // warriorA0 stay()s
          new Event(Json.createObjectBuilder()
              .add("type", "STAY_PRAWN")
              .add("time", START_TIME + 4)
              .add("prawn", 0)
              .add("stayTime", START_TIME + 3)
              .build()),
          // warriorA2 stay()s
          new Event(Json.createObjectBuilder()
              .add("type", "STAY_PRAWN")
              .add("time", START_TIME + 4)
              .add("prawn", 2)
              .add("stayTime", START_TIME + 2)
              .build())
          ), executor.getEventsGenerated());
    }
  }

  @Test
  public void testGeneratedScenarioWorks() throws Exception {
    runTestGeneratedScenarioWorks(true);
  }

  @Test
  public void testGeneratedScenarioWorksNotMocked() throws Exception {
    runTestGeneratedScenarioWorks(false);
  }

  /**
   * Runs the body of tests on a scenario testing method generate(). Depending on the value of mockTryPerform, either
   * calls to tryPerform() are mocked out or not.
   * @param mockTryPerform determines whether calls to tryPerform() are mocked out or not
   * @throws Exception when something unexpected happens
   */
  private void runTestScenarioTwoMovesSameTimeWorks(boolean mockTryPerform) throws Exception {
    // The scenario is reduced to one Prawn only.
    game.removePrawn(warriorA1);
    game.removePrawn(warriorB0);
    game.removePrawn(warriorB1);
    doAnswer(new Answer<Move[]>() {
      @Override
      public Move[] answer(InvocationOnMock invocation) throws Throwable {
        return new Move[]{
            new Move(new Position(ROAD_SECTION_0, 1), START_TIME, false),
            new Move(new Position(ROAD_SECTION_0, 2), START_TIME + 2, false),
            new Move(new Position(ROAD_SECTION_0, 3), START_TIME + 2, true),
        };
      }
    }).when(warriorA0).prepareMoveSequence(START_TIME + 4);
    final MoveActionEventGeneratingExecutor executor =
        spy((new MoveActionEventGeneratingExecutor.Factory(game, eventFactory))
            .create());
    assertEquals(State.NOT_PREPARED, executor.getState());
    game.advanceTimeTo(START_TIME + 4);
    executor.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    assertEquals(State.PREPARED, executor.getState());
    assertEquals(new HashSet<>(Arrays.asList()), game.getBattles());

    final PrawnMove warriorA0Move0 =
        new PrawnMove(START_TIME + 2, warriorA0, new Position(ROAD_SECTION_0, 1), new Position(ROAD_SECTION_0, 2),
            false);
    final PrawnMove warriorA0Move1 =
        new PrawnMove(START_TIME + 2, warriorA0, new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_0, 3),
            true);

    // Mocking out calls to tryPerform().
    if (mockTryPerform) {
      doAnswer(new Answer<Boolean>() {
        public Boolean answer(InvocationOnMock invocation) {
          final PrawnMove prawnMove = warriorA0Move0;
          executor.putPrawnInfo(prawnMove.getPrawn(),
              new PrawnInfo(prawnMove.getNextPosition(), prawnMove.isStop(), prawnMove.getTime()));
          return true;
        }
      }).when(executor).tryPerform(warriorA0Move0);
      doAnswer(new Answer<Boolean>() {
        public Boolean answer(InvocationOnMock invocation) {
          final PrawnMove prawnMove = warriorA0Move1;
          executor.putPrawnInfo(prawnMove.getPrawn(),
              new PrawnInfo(prawnMove.getNextPosition(), prawnMove.isStop(), prawnMove.getTime()));
          return true;
        }
      }).when(executor).tryPerform(warriorA0Move1);
    }

    executor.executeAndGenerate();

    // prawnInfo
    final PrawnInfo warriorA0Info = executor.getPrawnInfo(warriorA0);
    final PrawnInfo expectedWarriorA0Info = new PrawnInfo(warriorA0Move1.getNextPosition(), warriorA0Move1.isStop(),
        warriorA0Move1.getTime());
    // -> skipRound
    expectedWarriorA0Info.setSkipRound(false);
    // No plannedMove .
    //
    assertEquals(expectedWarriorA0Info, warriorA0Info);

    // game.battles
    assertEquals(new HashSet<>(), game.getBattles());

    verify(executor, times(1)).tryPerform(warriorA0Move0);
    verify(executor, times(1)).tryPerform(warriorA0Move1);

    if (!mockTryPerform) {
      assertEquals(Arrays.asList(
          TEST_EVENT,
          // warriorA0Move0
          new Event(Json.createObjectBuilder()
              .add("type", "MOVE_PRAWN")
              .add("time", START_TIME + 4)
              .add("prawn", 0)
              .add("move", Json.createObjectBuilder()
                  .add("position", Json.createObjectBuilder()
                      .add("roadSection", 0)
                      .add("index", 2)
                      .build())
                  .add("moveTime", START_TIME + 2)
                  .add("stop", false)
                  .build())
              .build()),
          // warriorA0Move1
          new Event(Json.createObjectBuilder()
              .add("type", "MOVE_PRAWN")
              .add("time", START_TIME + 4)
              .add("prawn", 0)
              .add("move", Json.createObjectBuilder()
                  .add("position", Json.createObjectBuilder()
                      .add("roadSection", 0)
                      .add("index", 3)
                      .build())
                  .add("moveTime", START_TIME + 2)
                  .add("stop", true)
                  .build())
              .build())
          ), executor.getEventsGenerated());
    }
  }

  @Test
  public void testScenarioTwoMovesSameTimeWorks() throws Exception {
    runTestScenarioTwoMovesSameTimeWorks(true);
  }

  @Test
  public void testScenarioTwoMovesSameTimeWorksMocked() throws Exception {
    runTestScenarioTwoMovesSameTimeWorks(false);
  }

  /**
   * Runs the body of tests on scenario 1 testing method generate(). Depending on the value of mockTryPerform, either
   * calls to tryPerform() are mocked out or not.
   * @param mockTryPerform determines whether calls to tryPerform() are mocked out or not
   * @throws Exception when something unexpected happens
   */
  private void runTestGeneratedScenario1Works(boolean mockTryPerform) throws Exception {
    final MoveActionEventGeneratingExecutor executor = createScenarioRoundGenerator();
    assertEquals(new HashSet<>(), game.getBattles());

    // Extra warrorA3, which is in a pending Battle with warriorB1 and fulfills Battle conditions.
    Warrior warriorA3 = spy((new Warrior.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(POSITION_0)
        .setDirection(3 * Math.PI / 2)
        .setGameProperties(gameProperties)
        .setPlayer(playerA)
        .build());
    warriorA3.setCurrentPosition(new Position(ROAD_SECTION_2, 1));
    final PrawnInfo warriorA3PrawnInfo = new PrawnInfo(new Position(ROAD_SECTION_2, 1), false, START_TIME);
    warriorA3PrawnInfo.setPlannedMove(new PrawnMove(START_TIME + 1, warriorA3, new Position(ROAD_SECTION_2, 1),
        new Position(ROAD_SECTION_2, 2), false));
    executor.putPrawnInfo(warriorA3, warriorA3PrawnInfo);
    game.addPrawn(warriorA3);

    final PrawnMove warriorA0Move0 =
        new PrawnMove(START_TIME + 2, warriorA0, new Position(ROAD_SECTION_0, 1), new Position(ROAD_SECTION_0, 2),
            false);
    final PrawnMove warriorA0Move1 =
        new PrawnMove(START_TIME + 3, warriorA0, new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_0, 3),
            true);
    final PrawnMove warriorA1Move0 =
        new PrawnMove(START_TIME + 1, warriorA1, new Position(ROAD_SECTION_0, 0), new Position(ROAD_SECTION_0, 1),
            true);
    final PrawnMove warriorA2Move0 =
        new PrawnMove(START_TIME + 1, warriorA2, new Position(ROAD_SECTION_1, 0), new Position(ROAD_SECTION_1, 1),
            false);
    final PrawnMove warriorA2Move1 =
        new PrawnMove(START_TIME + 2, warriorA2, new Position(ROAD_SECTION_1, 1), new Position(ROAD_SECTION_1, 0),
            false);
    final PrawnMove warriorA3Move0 =
        new PrawnMove(START_TIME, warriorA3, new Position(ROAD_SECTION_2, 1), new Position(ROAD_SECTION_2, 2),
            false);
    final PrawnMove warriorB0Move0 =
        new PrawnMove(START_TIME + 1, warriorB0, new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_0, 3),
            false);
    final PrawnMove warriorB1Move0 =
        new PrawnMove(START_TIME + 3, warriorB1, new Position(ROAD_SECTION_2, 2), new Position(ROAD_SECTION_2, 3),
            false);
    final Battle battleA0B0 = new Battle(warriorA0, warriorA0Move1.getInitialPosition(),
        warriorA0Move1.getNextPosition(), warriorB0, new Position(ROAD_SECTION_0, 3), null);
    final Battle battleA2B0 = new Battle(warriorA2, warriorA2Move1.getInitialPosition(),
        warriorA2Move1.getNextPosition(), warriorB0, new Position(ROAD_SECTION_0, 3), null);

    // A Battle between warriorA3 and warriorB1 already exists.
    final Battle battleA3B1 = new Battle(warriorA3, warriorA3Move0.getInitialPosition(),
        warriorA3Move0.getNextPosition(), warriorB1, new Position(ROAD_SECTION_2, 2), null);
    executor.getBattles().add(battleA3B1);
    executor.getPrawnBattles(warriorA3).add(battleA3B1);
    executor.getPrawnBattles(warriorB1).add(battleA3B1);
    // Mocking out calls to tryPerform().
    if (mockTryPerform) {
      doAnswer(new Answer<Boolean>() {
        public Boolean answer(InvocationOnMock invocation) {
          final PrawnMove prawnMove = warriorA0Move0;
          executor.putPrawnInfo(prawnMove.getPrawn(),
              new PrawnInfo(prawnMove.getNextPosition(), prawnMove.isStop(), prawnMove.getTime()));
          return true;
        }
      }).when(executor).tryPerform(warriorA0Move0);
      doAnswer(new Answer<Boolean>() {
        public Boolean answer(InvocationOnMock invocation) {
          try {
            executor.getBattles().add(battleA0B0);
            executor.getPrawnBattles(warriorA0).add(battleA0B0);
            executor.getPrawnBattles(warriorB0).add(battleA0B0);
            executor.getPrawnInfo(warriorA0).setPlannedMove(warriorA0Move1);
            executor.getPrawnInfo(warriorA0).setSkipRound(true);
            executor.getPrawnInfo(warriorB0).setSkipRound(true);
          } catch (GameException exception) {
            fail("Should never happen: " + exception);
          }
          return false;
        }
      }).when(executor).tryPerform(warriorA0Move1);
      doAnswer(new Answer<Boolean>() {
        public Boolean answer(InvocationOnMock invocation) {
          final PrawnMove prawnMove = warriorA1Move0;
          executor.putPrawnInfo(prawnMove.getPrawn(),
              new PrawnInfo(prawnMove.getNextPosition(), prawnMove.isStop(), prawnMove.getTime()));
          return true;
        }
      }).when(executor).tryPerform(warriorA1Move0);
      doAnswer(new Answer<Boolean>() {
        public Boolean answer(InvocationOnMock invocation) {
          final PrawnMove prawnMove = warriorA2Move0;
          executor.putPrawnInfo(prawnMove.getPrawn(),
              new PrawnInfo(prawnMove.getNextPosition(), prawnMove.isStop(), prawnMove.getTime()));
          return true;
        }
      }).when(executor).tryPerform(warriorA2Move0);
      doAnswer(new Answer<Boolean>() {
        public Boolean answer(InvocationOnMock invocation) {
          try {
            executor.getBattles().add(battleA2B0);
            executor.getPrawnBattles(warriorA2).add(battleA2B0);
            executor.getPrawnBattles(warriorB0).add(battleA2B0);
            executor.getPrawnInfo(warriorA2).setPlannedMove(warriorA2Move1);
            executor.getPrawnInfo(warriorA2).setSkipRound(true);
          } catch (GameException exception) {
            fail("Should never happen: " + exception);
          }
          return false;
        }
      }).when(executor).tryPerform(warriorA2Move1);
      doAnswer(new Answer<Boolean>() {
        public Boolean answer(InvocationOnMock invocation) {
          final PrawnMove prawnMove = warriorB0Move0;
          executor.putPrawnInfo(prawnMove.getPrawn(),
              new PrawnInfo(prawnMove.getNextPosition(), prawnMove.isStop(), prawnMove.getTime()));
          return true;
        }
      }).when(executor).tryPerform(warriorB0Move0);
      doAnswer(new Answer<Boolean>() {
        public Boolean answer(InvocationOnMock invocation) {
          try {
            executor.getPrawnInfo(warriorA3).setSkipRound(true);
            executor.getPrawnInfo(warriorB1).setSkipRound(true);
          } catch (GameException exception) {
            fail("Should never happen: " + exception);
          }
          return false;
        }
      }).when(executor).tryPerform(warriorB1Move0);
    }

    executor.executeAndGenerate();

    // battles
    assertEquals(new HashSet<>(Arrays.asList(battleA0B0, battleA2B0, battleA3B1)), executor.getBattles());
    assertEquals(new HashSet<>(Arrays.asList(battleA0B0)), executor.getPrawnBattles(warriorA0));
    assertEquals(new HashSet<>(), executor.getPrawnBattles(warriorA1));
    assertEquals(new HashSet<>(Arrays.asList(battleA2B0)), executor.getPrawnBattles(warriorA2));
    assertEquals(new HashSet<>(Arrays.asList(battleA3B1)), executor.getPrawnBattles(warriorA3));
    assertEquals(new HashSet<>(Arrays.asList(battleA0B0, battleA2B0)), executor.getPrawnBattles(warriorB0));
    assertEquals(new HashSet<>(Arrays.asList(battleA3B1)), executor.getPrawnBattles(warriorB1));
    // prawnInfo
    final PrawnInfo warriorA0Info = executor.getPrawnInfo(warriorA0);
    final PrawnInfo warriorA1Info = executor.getPrawnInfo(warriorA1);
    final PrawnInfo warriorA2Info = executor.getPrawnInfo(warriorA2);
    final PrawnInfo warriorB0Info = executor.getPrawnInfo(warriorB0);
    final PrawnInfo warriorB1Info = executor.getPrawnInfo(warriorB1);
    final PrawnInfo expectedWarriorA0Info = new PrawnInfo(warriorA0Move0.getNextPosition(), warriorA0Move0.isStop(),
        warriorA0Move0.getTime());  // NOTE: warriorA0Move0, not warriorA0Move1
    final PrawnInfo expectedWarriorA1Info = new PrawnInfo(warriorA1Move0.getNextPosition(), warriorA1Move0.isStop(),
        warriorA1Move0.getTime());
    final PrawnInfo expectedWarriorA2Info = new PrawnInfo(warriorA2Move0.getNextPosition(), warriorA2Move0.isStop(),
        warriorA2Move0.getTime());  // NOTE: warriorA2Move0, not warriorA2Move1
    final PrawnInfo expectedWarriorB0Info = new PrawnInfo(warriorB0Move0.getNextPosition(), warriorB0Move0.isStop(),
        warriorB0Move0.getTime());
    final PrawnInfo expectedWarriorB1Info = new PrawnInfo(new Position(ROAD_SECTION_2, 2), false,
        START_TIME);  // NOTE: warriorB1 hasn't moved
    // -> skipRound
    expectedWarriorA0Info.setSkipRound(true);
    expectedWarriorA1Info.setSkipRound(false);
    expectedWarriorA2Info.setSkipRound(true);
    expectedWarriorB0Info.setSkipRound(true);
    expectedWarriorB1Info.setSkipRound(true);
    // -> plannedMove
    expectedWarriorA0Info.setPlannedMove(warriorA0Move1);
    expectedWarriorA2Info.setPlannedMove(warriorA2Move1);
    expectedWarriorB1Info.setPlannedMove(warriorB1Move0);
    //
    assertEquals(expectedWarriorA0Info, warriorA0Info);
    assertEquals(expectedWarriorA1Info, warriorA1Info);
    assertEquals(expectedWarriorA2Info, warriorA2Info);
    assertEquals(expectedWarriorB0Info, warriorB0Info);
    assertEquals(expectedWarriorB1Info, warriorB1Info);

    // game.battles
    assertEquals(new HashSet<>(Arrays.asList(battleA0B0, battleA2B0, battleA3B1)), game.getBattles());

    //
    if (!mockTryPerform) {
      assertEquals(new Position(ROAD_SECTION_0, 2), warriorA0.getCurrentPosition());
      assertEquals(new Position(ROAD_SECTION_0, 1), warriorA1.getCurrentPosition());
      assertEquals(new Position(ROAD_SECTION_1, 1), warriorA2.getCurrentPosition());
      assertEquals(new Position(ROAD_SECTION_2, 1), warriorA3.getCurrentPosition());
      assertEquals(new Position(ROAD_SECTION_0, 3), warriorB0.getCurrentPosition());
      assertEquals(new Position(ROAD_SECTION_2, 2), warriorB1.getCurrentPosition());
      assertEquals(START_TIME + 3, warriorA0.getLastMoveTime());
      assertEquals(START_TIME + 1, warriorA1.getLastMoveTime());
      assertEquals(START_TIME + 2, warriorA2.getLastMoveTime());
      assertEquals(START_TIME + 1, warriorA3.getLastMoveTime());  // The time of A3's pendingMove .
      assertEquals(START_TIME + 1, warriorB0.getLastMoveTime());
      assertEquals(START_TIME + 3, warriorB1.getLastMoveTime());
    }

    verify(executor, times(1)).tryPerform(warriorA1Move0);
    verify(executor, times(1)).tryPerform(warriorA2Move0);
    verify(executor, times(1)).tryPerform(warriorB0Move0);
    verify(executor, times(1)).tryPerform(warriorA0Move0);
    verify(executor, times(1)).tryPerform(warriorA2Move1);
    verify(executor, times(1)).tryPerform(warriorA0Move1);
    verify(executor, times(1)).tryPerform(warriorB1Move0);

    if (!mockTryPerform) {
      assertEquals(Arrays.asList(
          TEST_EVENT,
          // warriorA1Move0
          new Event(Json.createObjectBuilder()
              .add("type", "MOVE_PRAWN")
              .add("time", START_TIME + 4)
              .add("prawn", 1)
              .add("move", Json.createObjectBuilder()
                  .add("position", Json.createObjectBuilder()
                      .add("roadSection", 0)
                      .add("index", 1)
                      .build())
                  .add("moveTime", START_TIME + 1)
                  .add("stop", true)
                  .build())
              .build()),
          // warriorA2Move0
          new Event(Json.createObjectBuilder()
              .add("type", "MOVE_PRAWN")
              .add("time", START_TIME + 4)
              .add("prawn", 2)
              .add("move", Json.createObjectBuilder()
                  .add("position", Json.createObjectBuilder()
                      .add("roadSection", 1)
                      .add("index", 1)
                      .build())
                  .add("moveTime", START_TIME + 1)
                  .add("stop", false)
                  .build())
              .build()),
          // warriorB0Move0
          new Event(Json.createObjectBuilder()
              .add("type", "MOVE_PRAWN")
              .add("time", START_TIME + 4)
              .add("prawn", 3)
              .add("move", Json.createObjectBuilder()
                  .add("position", Json.createObjectBuilder()
                      .add("roadSection", 0)
                      .add("index", 3)
                      .build())
                  .add("moveTime", START_TIME + 1)
                  .add("stop", false)
                  .build())
              .build()),
          // warriorA0Move0
          new Event(Json.createObjectBuilder()
              .add("type", "MOVE_PRAWN")
              .add("time", START_TIME + 4)
              .add("prawn", 0)
              .add("move", Json.createObjectBuilder()
                  .add("position", Json.createObjectBuilder()
                      .add("roadSection", 0)
                      .add("index", 2)
                      .build())
                  .add("moveTime", START_TIME + 2)
                  .add("stop", false)
                  .build())
              .build()),
          // Add Battle warriorA2 vs. warriorB0
          new Event(Json.createObjectBuilder()
              .add("type", "ADD_BATTLE")
              .add("time", START_TIME + 4)
              .add("battle", Json.createObjectBuilder()
                  .add("prawn0", 2)
                  .add("prawn1", 3)
                  .add("normalizedPosition0", Json.createObjectBuilder()
                      .add("roadSection", 1)
                      .add("index", 1)
                      .build())
                  .add("normalizedPosition1", Json.createObjectBuilder()
                      .add("roadSection", 1)
                      .add("index", 0)
                      .build())
                  .build())
              .build()),
          // Add Battle warriorA0 vs. warriorB0
          new Event(Json.createObjectBuilder()
              .add("type", "ADD_BATTLE")
              .add("time", START_TIME + 4)
              .add("battle", Json.createObjectBuilder()
                  .add("prawn0", 0)
                  .add("prawn1", 3)
                  .add("normalizedPosition0", Json.createObjectBuilder()
                      .add("roadSection", 0)
                      .add("index", 2)
                      .build())
                  .add("normalizedPosition1", Json.createObjectBuilder()
                      .add("roadSection", 0)
                      .add("index", 3)
                      .build())
                  .build())
              .build()),
          // warriorA0 stays
          new Event(Json.createObjectBuilder()
              .add("type", "STAY_PRAWN")
              .add("time", START_TIME + 4)
              .add("prawn", 0)
              .add("stayTime", START_TIME + 3)
              .build()),
          // warriorA2 stays
          new Event(Json.createObjectBuilder()
              .add("type", "STAY_PRAWN")
              .add("time", START_TIME + 4)
              .add("prawn", 2)
              .add("stayTime", START_TIME + 2)
              .build()),
          // warriorB1 stays
          new Event(Json.createObjectBuilder()
              .add("type", "STAY_PRAWN")
              .add("time", START_TIME + 4)
              .add("prawn", 4)
              .add("stayTime", START_TIME + 3)
              .build()),
          // warriorA3 stays
          new Event(Json.createObjectBuilder()
              .add("type", "STAY_PRAWN")
              .add("time", START_TIME + 4)
              .add("prawn", 7)
              .add("stayTime", START_TIME + 1)
              .build())
      ), executor.getEventsGenerated());
    }
  }

  @Test
  public void testGeneratedScenario1Works() throws Exception {
    runTestGeneratedScenario1Works(true);
  }

  @Test
  public void testGeneratedScenario1WorksNotMocked() throws Exception {
    runTestGeneratedScenario1Works(false);
  }

  @Test
  public void testCityCapturing() throws Exception {
    City.resetFirstAvailableId();
    final City cityA = (new City.Builder())
        .setGame(game)
        .setJoint(JOINT_0_0)
        .setName("cityA")
        .setPlayer(playerA)
        .build();
    final City cityB = (new City.Builder())
        .setGame(game)
        .setJoint(JOINT_3_0)
        .setName("cityB")
        .setPlayer(playerB)
        .build();
    cityB.grow();
    MoveActionEventGeneratingExecutor executor =
        (new MoveActionEventGeneratingExecutor.Factory(game, eventFactory).create());
    executor.prepare(new LinkedList<Event>());

    game.advanceTimeTo(START_TIME);
    warriorA0.setCurrentPosition(new Position(ROAD_SECTION_0, 1));
    executor.tryCaptureOrDestroyCity(warriorA0);
    assertEquals(new HashSet<>(Arrays.asList(cityA)), game.getPlayerCities(playerA));
    assertEquals(new HashSet<>(Arrays.asList(cityB)), game.getPlayerCities(playerB));
    assertEquals(Arrays.asList(), executor.getInternalEventsList());

    game.advanceTimeTo(START_TIME + 10);
    warriorA0.setCurrentPosition(new Position(ROAD_SECTION_0, 0));
    executor.tryCaptureOrDestroyCity(warriorA0);
    assertEquals(new HashSet<>(Arrays.asList(cityA)), game.getPlayerCities(playerA));
    assertEquals(new HashSet<>(Arrays.asList(cityB)), game.getPlayerCities(playerB));
    assertEquals(playerA, cityA.getPlayer());
    assertEquals(1, cityA.getSize());
    assertEquals(Arrays.asList(), executor.getInternalEventsList());

    game.advanceTimeTo(START_TIME + 20);
    warriorA0.setCurrentPosition(new Position(ROAD_SECTION_2, 3));
    executor.tryCaptureOrDestroyCity(warriorA0);
    assertEquals(new HashSet<>(Arrays.asList(cityA, cityB)), game.getPlayerCities(playerA));
    assertEquals(new HashSet<>(), game.getPlayerCities(playerB));
    assertEquals(playerA, cityB.getPlayer());
    assertEquals(1, cityB.getSize());
    assertEquals(Arrays.asList(
        new Event(Json.createObjectBuilder()
            .add("type", "CAPTURE_OR_DESTROY_CITY")
            .add("time", START_TIME + 20)
            .add("city", 1)
            .add("defender", 1)
            .add("attacker", 0)
            .build())
        ), executor.getInternalEventsList());

    game.advanceTimeTo(START_TIME + 30);
    warriorA0.setCurrentPosition(new Position(ROAD_SECTION_0, 0));
    warriorB0.setCurrentPosition(new Position(ROAD_SECTION_2, 3));
    executor.tryCaptureOrDestroyCity(warriorB0);
    assertEquals(new HashSet<>(Arrays.asList(cityA)), game.getPlayerCities(playerA));
    assertEquals(new HashSet<>(), game.getPlayerCities(playerB));
    assertEquals(Arrays.asList(
        new Event(Json.createObjectBuilder()
            .add("type", "CAPTURE_OR_DESTROY_CITY")
            .add("time", START_TIME + 20)
            .add("city", 1)
            .add("defender", 1)
            .add("attacker", 0)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "CAPTURE_OR_DESTROY_CITY")
            .add("time", START_TIME + 30)
            .add("city", 1)
            .add("defender", 0)
            .add("attacker", 1)
            .build())
        ), executor.getInternalEventsList());
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testBattlesUpdatedInGame() throws Exception {
    final EventFactory mockedEventFactory = mock(EventFactory.class);
    final Battle[] mockedBattles = {mock(Battle.class), mock(Battle.class), };
    when(mockedGame.getBattles())
        .thenReturn(
            // NOTE: Providing 2 different instances is important, so that Mockito doesn't return the same Set twice.
            new HashSet<>(Arrays.asList(mockedBattles[0])), new HashSet<>(Arrays.asList(mockedBattles[0])));
    when(mockedGame.getBattle(mockedBattles[0], false)).thenReturn(mockedBattles[0]);
    when(mockedGame.getBattle(mockedBattles[1], false)).thenReturn(null);
    when(mockedBattles[0].getPrawns()).thenReturn(new Prawn[]{warriorA0, warriorB0});
    when(mockedBattles[1].getPrawns()).thenReturn(new Prawn[]{warriorA1, warriorB1});
    final MoveActionEventGeneratingExecutor executor =
        (new MoveActionEventGeneratingExecutor.Factory(mockedGame, mockedEventFactory)).create();
    when(mockedGame.getTime()).thenReturn(START_TIME);
    executor.prepare(new LinkedList<>());
    executor.getBattles().remove(mockedBattles[0]);
    executor.getBattles().add(mockedBattles[1]);
    executor.updateBattlesInGame();

    verify(mockedGame, times(1)).removeBattle(mockedBattles[0]);
    verify(mockedGame, times(1)).addBattle(mockedBattles[1]);
  }

  /**
   * Auxiliary method that generates a MoveActionEventGeneratingExecutor used in tests of method isNextPositionTaken().
   * @return a RoundGenerator
   * @throws BoardException when MoveActionEventGeneratingExecutor creation fails
   * @throws GameException when MoveActionEventGeneratingExecutor creation fails
   * @throws EventProcessingException when MoveActionEventGeneratingExecutor creation fails
   */
  private MoveActionEventGeneratingExecutor createNextPositionTakenExecutor()
      throws BoardException, GameException, EventProcessingException {
    doAnswer(new Answer<Void>() {
      public Void answer(InvocationOnMock invocation) {
        return null;
      }
    }).when(settlersUnitA).move(any(Move.class), anyLong());
    doAnswer(new Answer<Void>() {
      public Void answer(InvocationOnMock invocation) {
        return null;
      }
    }).when(settlersUnitB).move(any(Move.class), anyLong());
    game.removePrawn(warriorB1);
    game.addPrawn(settlersUnitA);
    game.addPrawn(settlersUnitB);
    final MoveActionEventGeneratingExecutor executor =
        spy((new MoveActionEventGeneratingExecutor.Factory(game, mockedEventFactory))
            .create());
    executor.putPrawnInfo(settlersUnitA, new PrawnInfo(new Position(ROAD_SECTION_2, 1), false, START_TIME));
    executor.putPrawnInfo(settlersUnitB, new PrawnInfo(new Position(ROAD_SECTION_2, 2), false, START_TIME));
    assertEquals(null, executor.getPrawnInfo(settlersUnitA).getPlannedMove());
    assertEquals(null, executor.getPrawnInfo(settlersUnitB).getPlannedMove());
    assertFalse(executor.getPrawnInfo(settlersUnitA).isSkipRound());
    assertFalse(executor.getPrawnInfo(settlersUnitB).isSkipRound());
    when(executor.getPrawnsEverOnRoadSection(ROAD_SECTION_0))
        .thenReturn(new HashSet<>(Arrays.asList(settlersUnitA, settlersUnitB)));
    executor.prepare(new LinkedList<>());
    return executor;
  }

  @Test
  public void testNextPositionBlockedMoveNotPerformed() throws Exception {
    final MoveActionEventGeneratingExecutor executor = createNextPositionTakenExecutor();

    assertEquals(new HashSet<>(), executor.getBattles());
    final PrawnMove settlersUnitAPrawnMove = new PrawnMove(START_TIME + 1, settlersUnitA,
        new Position(ROAD_SECTION_2, 1), new Position(ROAD_SECTION_2, 2), false);
    executor.getPrawnInfo(settlersUnitA).setPlannedMove(settlersUnitAPrawnMove);

    assertFalse(executor.tryPerform(settlersUnitAPrawnMove));
    // battles
    assertEquals(new HashSet<>(Arrays.asList()), executor.getBattles());
    // prawnInfo
    final PrawnInfo settlersUnitAInfo = executor.getPrawnInfo(settlersUnitA);
    assertEquals(new Position(ROAD_SECTION_2, 1), settlersUnitAInfo.getCurrentPosition());
    assertEquals(settlersUnitAPrawnMove, settlersUnitAInfo.getPlannedMove());
    assertEquals(START_TIME, settlersUnitAInfo.getTime());
    // skipRound
    assertFalse(executor.getPrawnInfo(settlersUnitA).isSkipRound());

    verify(executor, times(1)).reviseOngoingBattles(settlersUnitAPrawnMove);
    verify(executor, times(1)).startNewBattles(settlersUnitAPrawnMove);
    verify(executor, times(1)).isNextPositionTaken(settlersUnitAPrawnMove);
    verify(mockedEventFactory, times(0)).createMovePrawnEvent(any(Prawn.class), any(Move.class));
    verify(executor, times(0)).tryCaptureOrDestroyCity(any(Prawn.class));
  }

  @Test
  public void testMoveOnlyPerformedWhenNextPositionNotBlocked() throws Exception {
    final MoveActionEventGeneratingExecutor executor = createNextPositionTakenExecutor();

    assertEquals(new HashSet<>(), executor.getBattles());
    final PrawnMove settlersUnitAPrawnMove = new PrawnMove(START_TIME + 1, settlersUnitA,
        new Position(ROAD_SECTION_2, 1), new Position(ROAD_SECTION_2, 2), false);
    executor.getPrawnInfo(settlersUnitA).setPlannedMove(settlersUnitAPrawnMove);
    final PrawnMove settlersUnitBPrawnMove = new PrawnMove(START_TIME + 1, settlersUnitB,
        new Position(ROAD_SECTION_2, 2), new Position(ROAD_SECTION_2, 3), false);
    executor.getPrawnInfo(settlersUnitB).setPlannedMove(settlersUnitBPrawnMove);

    // First attempt fails.
    assertFalse(executor.tryPerform(settlersUnitAPrawnMove));
    {
      // battles
      assertEquals(new HashSet<>(Arrays.asList()), executor.getBattles());
      // settlersUnits
      assertEquals(new Position(ROAD_SECTION_2, 1), settlersUnitA.getCurrentPosition());
      assertEquals(new Position(ROAD_SECTION_2, 2), settlersUnitB.getCurrentPosition());
      // prawnInfo
      final PrawnInfo settlersUnitAInfo = executor.getPrawnInfo(settlersUnitA);
      assertEquals(new Position(ROAD_SECTION_2, 1), settlersUnitAInfo.getCurrentPosition());
      assertEquals(settlersUnitAPrawnMove, settlersUnitAInfo.getPlannedMove());
      assertEquals(START_TIME, settlersUnitAInfo.getTime());
      // skipRound
      assertFalse(executor.getPrawnInfo(settlersUnitA).isSkipRound());
      // verify
      verify(executor, times(1)).reviseOngoingBattles(settlersUnitAPrawnMove);
      verify(executor, times(1)).startNewBattles(settlersUnitAPrawnMove);
      verify(executor, times(1)).isNextPositionTaken(settlersUnitAPrawnMove);
      verify(settlersUnitA, times(0)).move(any(Move.class), anyLong());
      verify(mockedEventFactory, times(0)).createMovePrawnEvent(any(Prawn.class), any(Move.class));
      verify(executor, times(0)).tryCaptureOrDestroyCity(any(Prawn.class));
    }

    // Second attempt succeeds.
    assertTrue(executor.tryPerform(settlersUnitBPrawnMove));
    assertTrue(executor.tryPerform(settlersUnitAPrawnMove));
    {
      // battles
      assertEquals(new HashSet<>(Arrays.asList()), executor.getBattles());
      // prawnInfo
      final PrawnInfo settlersUnitAInfo = executor.getPrawnInfo(settlersUnitA);
      assertEquals(new Position(ROAD_SECTION_2, 2), settlersUnitAInfo.getCurrentPosition());
      assertEquals(START_TIME + 1, settlersUnitAInfo.getTime());
      final PrawnInfo settlersUnitBInfo = executor.getPrawnInfo(settlersUnitB);
      assertEquals(new Position(ROAD_SECTION_2, 3), settlersUnitBInfo.getCurrentPosition());
      assertEquals(START_TIME + 1, settlersUnitBInfo.getTime());
      // skipRound
      assertFalse(executor.getPrawnInfo(settlersUnitA).isSkipRound());
      assertFalse(executor.getPrawnInfo(settlersUnitB).isSkipRound());
      // verify
      verify(executor, times(2)).reviseOngoingBattles(settlersUnitAPrawnMove);
      verify(executor, times(2)).startNewBattles(settlersUnitAPrawnMove);
      verify(executor, times(2)).isNextPositionTaken(settlersUnitAPrawnMove);
      verify(executor, times(1)).reviseOngoingBattles(settlersUnitBPrawnMove);
      verify(executor, times(1)).startNewBattles(settlersUnitBPrawnMove);
      verify(executor, times(1)).isNextPositionTaken(settlersUnitBPrawnMove);
      verify(settlersUnitA, times(1)).move(
          new Move(new Position(ROAD_SECTION_2, 2), START_TIME + 1, false), START_TIME + 1);
      verify(settlersUnitB, times(1)).move(
          new Move(new Position(ROAD_SECTION_2, 3), START_TIME + 1, false), START_TIME + 1);
      verify(mockedEventFactory, times(1)).createMovePrawnEvent(
          settlersUnitA, new Move(new Position(ROAD_SECTION_2, 2), START_TIME + 1, false));
      verify(mockedEventFactory, times(1)).createMovePrawnEvent(
          settlersUnitB, new Move(new Position(ROAD_SECTION_2, 3), START_TIME + 1, false));
      verify(executor, times(1)).tryCaptureOrDestroyCity(settlersUnitA);
      verify(executor, times(1)).tryCaptureOrDestroyCity(settlersUnitB);
    }
  }

  @Test
  public void testOrderingSkipRoundPrawnsToStayWorks() throws Exception {
    MoveActionEventGeneratingExecutor executor =
        (new MoveActionEventGeneratingExecutor.Factory(mockedGame, mockedEventFactory))
        .create();
    when(mockedGame.getTime()).thenReturn(START_TIME);
    executor.prepare(new LinkedList<Event>());
    executor.putPrawnInfo(warriorA0, new PrawnInfo(new Position(ROAD_SECTION_0, 1), false, START_TIME));
    executor.getPrawnInfo(warriorA0).setPlannedMove(new PrawnMove(
        START_TIME + 1, warriorA0, new Position(ROAD_SECTION_0, 1), new Position(ROAD_SECTION_0, 0), false));
    executor.putPrawnInfo(warriorB0, new PrawnInfo(new Position(ROAD_SECTION_0, 2), false, START_TIME));
    executor.getPrawnInfo(warriorB0).setPlannedMove(new PrawnMove(
        START_TIME + 2, warriorB0, new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_0, 3), false));
    executor.getPrawnInfo(warriorB0).setSkipRound(true);
    executor.putPrawnInfo(warriorB1, new PrawnInfo(new Position(ROAD_SECTION_2, 2), false, START_TIME));
    executor.getPrawnInfo(warriorB1).setSkipRound(true);
    assertEquals(0, executor.getInternalEventsList().size());
    executor.orderSkipRoundPrawnsToStay();

    // lastMoveTime
    assertEquals(START_TIME, warriorA0.getLastMoveTime());
    assertEquals(START_TIME + 2, warriorB0.getLastMoveTime());
    // Events
    verify(mockedEventFactory, times(1)).createStayPrawnEvent(warriorB0, START_TIME + 2);
    assertEquals(1, executor.getInternalEventsList().size());
  }

}
