package cz.wie.najtmar.gop.game;

import static org.junit.Assert.assertEquals;

import cz.wie.najtmar.gop.board.Board;
import cz.wie.najtmar.gop.board.Joint;
import cz.wie.najtmar.gop.board.RoadSection;
import cz.wie.najtmar.gop.common.Point2D;
import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.game.Player.State;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Properties;
import java.util.UUID;

import javax.json.Json;

public class GameEvaluationEventGeneratingExecutorTest {

  static final long START_TIME = 966;
  static final long INTERRUPT_TIME_PERIOD = 100;
  static Event TEST_EVENT;

  Board board;
  Joint joint0;
  Joint joint1;
  Joint joint2;
  Joint joint3;
  RoadSection roadSection0;
  RoadSection roadSection1;
  RoadSection roadSection2;

  Game game;
  GameProperties gameProperties;
  EventFactory eventFactory;
  Player playerA;
  Player playerB;
  Player playerC;
  Warrior warriorA;
  Warrior warriorB;
  SettlersUnit settlersUnit;
  City city;
  GameEvaluationEventGeneratingExecutor.Factory executorFactory;
  GameEvaluationEventGeneratingExecutor executor;

  /**
   * Method setUpBeforeClass() .
   * @throws Exception when the method execution fails
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    TEST_EVENT = new Event(Json.createObjectBuilder()
        .add("type", "TEST")
        .add("time", START_TIME)
        .add("whatever", "value")
        .build());
  }

  /**
   * Method setUp().
   * @throws Exception when something unexpected happens
   */
  @Before
  public void setUp() throws Exception {
    final Properties simpleBoardPropertiesSet = new Properties();
    simpleBoardPropertiesSet.load(GameEvaluationEventGeneratingExecutorTest.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/board/simple-board.properties"));
    board = new Board(simpleBoardPropertiesSet);
    joint0 = (new Joint.Builder())
        .setBoard(board)
        .setIndex(0)
        .setName("joint0")
        .setCoordinatesPair(new Point2D.Double(1.0, 0.0))
        .build();
    joint1 = (new Joint.Builder())
        .setBoard(board)
        .setIndex(1)
        .setName("joint1")
        .setCoordinatesPair(new Point2D.Double(4.0, 0.0))
        .build();
    joint2 = (new Joint.Builder())
        .setBoard(board)
        .setIndex(2)
        .setName("joint2")
        .setCoordinatesPair(new Point2D.Double(4.0, 4.0))
        .build();
    joint3 = (new Joint.Builder())
        .setBoard(board)
        .setIndex(3)
        .setName("joint3")
        .setCoordinatesPair(new Point2D.Double(0.0, 4.0))
        .build();
    roadSection0 = (new RoadSection.Builder())
        .setBoard(board)
        .setIndex(0)
        .setJoints(joint0, joint1)
        .build();
    roadSection1 = (new RoadSection.Builder())
        .setBoard(board)
        .setIndex(1)
        .setJoints(joint1, joint2)
        .build();
    roadSection2 = (new RoadSection.Builder())
        .setBoard(board)
        .setIndex(2)
        .setJoints(joint2, joint3)
        .build();

    final Properties simpleGamePropertiesSet = new Properties();
    simpleGamePropertiesSet.load(GameEvaluationEventGeneratingExecutorTest.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/game/simple-game.properties"));
    simpleGamePropertiesSet.setProperty(GameProperties.START_TIME_PROPERTY_NAME, "" + START_TIME);
    game = new Game(simpleGamePropertiesSet);
    game.setBoard(board);
    gameProperties = game.getGameProperties();
    playerA = new Player(new User("playerA", new UUID(12345, 67890)), 0, game);
    playerB = new Player(new User("playerB", new UUID(98765, 43210)), 1, game);
    playerC = new Player(new User("playerC", new UUID(12345, 54321)), 2, game);
    game.addPlayer(playerA);
    game.addPlayer(playerB);
    game.addPlayer(playerC);
    Prawn.resetFirstAvailableId();
    warriorA = (new Warrior.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(joint0.getNormalizedPosition())
        .setDirection(0.0)
        .setGameProperties(gameProperties)
        .setPlayer(playerA)
        .build();
    warriorB = (new Warrior.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(joint1.getNormalizedPosition())
        .setDirection(Math.PI)
        .setGameProperties(gameProperties)
        .setPlayer(playerB)
        .build();
    settlersUnit = (new SettlersUnit.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(joint3.getNormalizedPosition())
        .setGameProperties(gameProperties)
        .setPlayer(playerC)
        .build();
    game.addPrawn(warriorA);
    game.addPrawn(warriorB);
    game.addPrawn(settlersUnit);
    City.resetFirstAvailableId();
    city = (new City.Builder())
        .setGame(game)
        .setName("city")
        .setPlayer(playerA)
        .setJoint(joint2)
        .build();
    game.initialize();
    game.start();

    final ObjectSerializer objectSerializer = new ObjectSerializer(game);
    eventFactory = new EventFactory(objectSerializer);
    game.advanceTimeTo(START_TIME);
    executorFactory = new GameEvaluationEventGeneratingExecutor.Factory(game, eventFactory);
    executorFactory.advanceNextPeriodicEventTimeTo(START_TIME + INTERRUPT_TIME_PERIOD);
    executor = executorFactory.create();
  }

  @Test
  public void testNothingHasHappened() throws Exception {
    executor.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    executor.executeAndGenerate();
    assertEquals(Arrays.asList(TEST_EVENT), executor.getEventsGenerated());
  }

  @Test
  public void testNoPrawnsPlayerLost() throws Exception {
    game.removePrawn(warriorB);

    executor.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    executor.executeAndGenerate();
    assertEquals(Arrays.asList(
        TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "CHANGE_PLAYER_STATE")
            .add("time", START_TIME)
            .add("player", 1)
            .add("state", "LOST")
            .build())
        ), executor.getEventsGenerated());
  }

  @Test
  public void testNoPrawnsButACityNothingHappens() throws Exception {
    game.removePrawn(warriorA);

    executor.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    executor.executeAndGenerate();
    assertEquals(Arrays.asList(TEST_EVENT), executor.getEventsGenerated());
  }

  @Test
  public void testNoPrawnsOrCitiesPlayerLost() throws Exception {
    game.removePrawn(warriorA);
    game.destroyCity(city);

    executor.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    executor.executeAndGenerate();
    assertEquals(Arrays.asList(
        TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "CHANGE_PLAYER_STATE")
            .add("time", START_TIME)
            .add("player", 0)
            .add("state", "LOST")
            .build())
        ), executor.getEventsGenerated());
  }

  @Test
  public void testTwoPlayersLostGameFinished() throws Exception {
    game.removePrawn(warriorB);
    game.removePrawn(settlersUnit);

    executor.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    executor.executeAndGenerate();
    assertEquals(Arrays.asList(
        TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "CHANGE_PLAYER_STATE")
            .add("time", START_TIME)
            .add("player", 1)
            .add("state", "LOST")
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "CHANGE_PLAYER_STATE")
            .add("time", START_TIME)
            .add("player", 2)
            .add("state", "LOST")
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "CHANGE_PLAYER_STATE")
            .add("time", START_TIME)
            .add("player", 0)
            .add("state", "WON")
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "FINISH_GAME")
            .add("time", START_TIME)
            .build())
        ), executor.getEventsGenerated());
  }

  @Test
  public void testPeriodInterruptEventsGenerated() throws Exception {
    // Period has just started.
    executor.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    executor.executeAndGenerate();
    assertEquals(Arrays.asList(TEST_EVENT), executor.getEventsGenerated());
    // Period not yet passed started.
    game.advanceTimeTo(START_TIME + INTERRUPT_TIME_PERIOD - 1);
    executor = executorFactory.create();
    executor.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    executor.executeAndGenerate();
    assertEquals(Arrays.asList(TEST_EVENT), executor.getEventsGenerated());
    // Period has just passed.
    game.advanceTimeTo(START_TIME + INTERRUPT_TIME_PERIOD);
    executor = executorFactory.create();
    executor.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    executor.executeAndGenerate();
    assertEquals(Arrays.asList(
        TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "PERIODIC_INTERRUPT")
            .add("time", START_TIME + INTERRUPT_TIME_PERIOD)
            .build())), executor.getEventsGenerated());
    // Another period not yet passed.
    executor = executorFactory.create();
    executor.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    executor.executeAndGenerate();
    assertEquals(Arrays.asList(TEST_EVENT), executor.getEventsGenerated());
    // Another period has passed some time ago.
    game.advanceTimeTo(START_TIME + 2 * INTERRUPT_TIME_PERIOD + 1);
    executor = executorFactory.create();
    executor.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    executor.executeAndGenerate();
    assertEquals(Arrays.asList(
        TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "PERIODIC_INTERRUPT")
            .add("time", START_TIME + 2 * INTERRUPT_TIME_PERIOD + 1)
            .build())), executor.getEventsGenerated());
  }

  @Test
  public void testPeriodInterruptEventsGenerated1() throws Exception {
    final Properties simpleGamePropertiesSet = new Properties();
    simpleGamePropertiesSet.load(GameEvaluationEventGeneratingExecutorTest.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/game/simple-game.properties"));
    simpleGamePropertiesSet.setProperty(GameProperties.START_TIME_PROPERTY_NAME, "" + START_TIME);
    game = new Game(simpleGamePropertiesSet);
    game.setBoard(board);
    gameProperties = game.getGameProperties();
    game.addPlayer(playerA);
    game.addPlayer(playerB);
    game.addPlayer(playerC);
    game.addPrawn(warriorA);
    game.addPrawn(warriorB);
    game.addPrawn(settlersUnit);
    /*
    City.resetFirstAvailableId();
    city = (new City.Builder())
        .setGame(game)
        .setName("city")
        .setPlayer(playerA)
        .setJoint(joint2)
        .build();
    */
    game.initialize();
    game.start();

    final ObjectSerializer objectSerializer = new ObjectSerializer(game);
    eventFactory = new EventFactory(objectSerializer);
    game.advanceTimeTo(START_TIME);
    executorFactory = new GameEvaluationEventGeneratingExecutor.Factory(game, eventFactory);
    executor = executorFactory.create();

    // 966
    executor.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    executor.executeAndGenerate();
    assertEquals(Arrays.asList(TEST_EVENT), executor.getEventsGenerated());
    // 1000 - 1
    game.advanceTimeTo(1000 - 1);
    executor = executorFactory.create();
    executor.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    executor.executeAndGenerate();
    assertEquals(Arrays.asList(TEST_EVENT), executor.getEventsGenerated());
    // 1000
    game.advanceTimeTo(1000);
    executor = executorFactory.create();
    executor.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    executor.executeAndGenerate();
    assertEquals(Arrays.asList(
        TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "PERIODIC_INTERRUPT")
            .add("time", 1000)
            .build())), executor.getEventsGenerated());
    // 1000 again
    executor = executorFactory.create();
    executor.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    executor.executeAndGenerate();
    assertEquals(Arrays.asList(TEST_EVENT), executor.getEventsGenerated());
    // 1100 + 1
    game.advanceTimeTo(1100 + 1);
    executor = executorFactory.create();
    executor.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    executor.executeAndGenerate();
    assertEquals(Arrays.asList(
        TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "PERIODIC_INTERRUPT")
            .add("time", 1100 + 1)
            .build())), executor.getEventsGenerated());
    // 1200 - 2
    game.advanceTimeTo(1200 - 2);
    executor = executorFactory.create();
    executor.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    executor.executeAndGenerate();
    assertEquals(Arrays.asList(TEST_EVENT), executor.getEventsGenerated());
    // 1200 - 1
    game.advanceTimeTo(1200 - 1);
    executor = executorFactory.create();
    executor.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    executor.executeAndGenerate();
    assertEquals(Arrays.asList(TEST_EVENT), executor.getEventsGenerated());
    // 1200
    game.advanceTimeTo(1200);
    executor = executorFactory.create();
    executor.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    executor.executeAndGenerate();
    assertEquals(Arrays.asList(
        TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "PERIODIC_INTERRUPT")
            .add("time", 1200)
            .build())), executor.getEventsGenerated());
  }

  @Test
  public void testOnePlayerAbandonedTwoInGame() throws Exception {
    playerC.setState(State.UNDECIDED);

    executor.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    executor.executeAndGenerate();
    assertEquals(Arrays.asList(
        TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "CHANGE_PLAYER_STATE")
            .add("time", START_TIME)
            .add("player", 0)
            .add("state", "UNDECIDED")
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "CHANGE_PLAYER_STATE")
            .add("time", START_TIME)
            .add("player", 1)
            .add("state", "UNDECIDED")
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "FINISH_GAME")
            .add("time", START_TIME)
            .build())
        ), executor.getEventsGenerated());
    assertEquals(Game.Result.UNDECIDED, game.calculateResult());
  }

  @Test
  public void testOnePlayerAbandonedOneInGame() throws Exception {
    game.removePrawn(settlersUnit);
    game.removePlayer(playerC);
    playerB.setState(State.ABANDONED);

    executor.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    executor.executeAndGenerate();
    assertEquals(Arrays.asList(
        TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "CHANGE_PLAYER_STATE")
            .add("time", START_TIME)
            .add("player", 0)
            .add("state", "WON")
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "FINISH_GAME")
            .add("time", START_TIME)
            .build())
        ), executor.getEventsGenerated());
  }

}
