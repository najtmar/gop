package cz.wie.najtmar.gop.common;

/**
 * Utility interface for definition of constants used in tests.
 * @author najtmar
 */
public interface TestConstants {

  /**
   * Delta used for comparison of doubles in tests.
   */
  public static final double DOUBLE_DELTA = 0.0000000001;

}
