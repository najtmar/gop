package cz.wie.najtmar.gop.game;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.wie.najtmar.gop.board.Board;
import cz.wie.najtmar.gop.board.BoardException;
import cz.wie.najtmar.gop.board.Joint;
import cz.wie.najtmar.gop.board.Position;
import cz.wie.najtmar.gop.board.RoadSection;
import cz.wie.najtmar.gop.common.Point2D;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Properties;
import java.util.UUID;

public class BattleTest {

  static final Game DUMMY_GAME = mock(Game.class);
  static final GameProperties MOCK_GAME_PROPERTIES = mock(GameProperties.class);
  static final Player PLAYER_A = new Player(new User("playerA", new UUID(12345, 67890)), 0, DUMMY_GAME);
  static final Player PLAYER_B = new Player(new User("playerB", new UUID(98765, 43210)), 1, DUMMY_GAME);

  static Board BOARD;
  static Joint JOINT_0;
  static Joint JOINT_1;
  static Joint JOINT_2;
  static RoadSection ROAD_SECTION_0;
  static RoadSection ROAD_SECTION_1;
  static RoadSection ROAD_SECTION_2;
  static Position POSITION_0;
  static final long START_MOVE_TIME = 1000;

  Warrior warriorA0;
  Warrior warriorA1;
  Warrior warriorB0;
  Warrior warriorB1;
  SettlersUnit settlersUnitA;
  SettlersUnit settlersUnitB;

  /**
   * Executed before the class is set up.
   * @throws Exception when something unexpected happens
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    final Properties simpleBoardPropertiesSet = new Properties();
    simpleBoardPropertiesSet.load(BattleTest.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/board/simple-board.properties"));
    BOARD = new Board(simpleBoardPropertiesSet);
    JOINT_0 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setName("joint0")
        .setCoordinatesPair(new Point2D.Double(1.0, 1.0))
        .build();
    JOINT_1 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(1)
        .setName("joint1")
        .setCoordinatesPair(new Point2D.Double(4.0, 1.0))
        .build();
    JOINT_2 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(2)
        .setName("joint2")
        .setCoordinatesPair(new Point2D.Double(1.0, 4.0))
        .build();
    ROAD_SECTION_0 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(JOINT_0, JOINT_1)
        .build();
    ROAD_SECTION_1 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(1)
        .setJoints(JOINT_1, JOINT_2)
        .build();
    ROAD_SECTION_2 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(2)
        .setJoints(JOINT_0, JOINT_2)
        .build();
    POSITION_0 = new Position(ROAD_SECTION_0, 0);
  }

  /**
   * Executed after the class is teared down.
   */
  @AfterClass
  public static void tearDownAfterClass() {
    verify(MOCK_GAME_PROPERTIES, times(0)).getMaxPlayerCount();
    verify(MOCK_GAME_PROPERTIES, times(0)).getBasicVelocity();
    verify(MOCK_GAME_PROPERTIES, times(0)).getVelocityDirContribFactor();
    verify(MOCK_GAME_PROPERTIES, times(0)).getProduceActionProgressFactor();
    verify(MOCK_GAME_PROPERTIES, times(0)).getRepairActionProgressFactor();
    verify(MOCK_GAME_PROPERTIES, times(0)).getGrowActionProgressFactor();
  }

  /**
   * Sets up the test case.
   * @throws Exception when something unexpected happens
   */
  @Before
  public void setUp() throws Exception {
    Prawn.resetFirstAvailableId();
    warriorA0 = (new Warrior.Builder())
        .setCreationTime(START_MOVE_TIME)
        .setCurrentPosition(POSITION_0)
        .setDirection(0.0)
        .setGameProperties(MOCK_GAME_PROPERTIES)
        .setPlayer(PLAYER_A)
        .build();
    warriorA1 = (new Warrior.Builder())
        .setCreationTime(START_MOVE_TIME)
        .setCurrentPosition(POSITION_0)
        .setDirection(0.0)
        .setGameProperties(MOCK_GAME_PROPERTIES)
        .setPlayer(PLAYER_A)
        .build();
    warriorB0 = (new Warrior.Builder())
        .setCreationTime(START_MOVE_TIME)
        .setCurrentPosition(POSITION_0)
        .setDirection(0.0)
        .setGameProperties(MOCK_GAME_PROPERTIES)
        .setPlayer(PLAYER_B)
        .build();
    warriorB1 = (new Warrior.Builder())
        .setCreationTime(START_MOVE_TIME)
        .setCurrentPosition(POSITION_0)
        .setDirection(0.0)
        .setGameProperties(MOCK_GAME_PROPERTIES)
        .setPlayer(PLAYER_B)
        .build();
    settlersUnitA = (new SettlersUnit.Builder())
        .setCreationTime(START_MOVE_TIME)
        .setCurrentPosition(POSITION_0)
        .setGameProperties(MOCK_GAME_PROPERTIES)
        .setPlayer(PLAYER_A)
        .build();
    settlersUnitB = (new SettlersUnit.Builder())
        .setCreationTime(START_MOVE_TIME)
        .setCurrentPosition(POSITION_0)
        .setGameProperties(MOCK_GAME_PROPERTIES)
        .setPlayer(PLAYER_B)
        .build();
  }

  @Test
  public void testSimpleBattleCreated() throws BoardException, GameException {
    warriorA0.setCurrentPosition(new Position(ROAD_SECTION_0, 1));
    warriorB0.setCurrentPosition(new Position(ROAD_SECTION_0, 2));
    Battle simpleBattle = new Battle(warriorA0, new Position(ROAD_SECTION_0, 1), new Position(ROAD_SECTION_0, 2),
        warriorB0, new Position(ROAD_SECTION_0, 2), null);
    assertArrayEquals(new Prawn[]{warriorA0, warriorB0}, simpleBattle.getPrawns());
    assertArrayEquals(new Position[]{new Position(ROAD_SECTION_0, 1), new Position(ROAD_SECTION_0, 2)},
        simpleBattle.getNormalizedPositions());
    assertArrayEquals(new RoadSection.Direction[]{RoadSection.Direction.FORWARD, RoadSection.Direction.BACKWARD},
        simpleBattle.getAttackDirections());
    assertEquals(Battle.State.PLANNED, simpleBattle.getState());
    try {
      simpleBattle.finish();
      fail("Should have thrown an exception.");
    } catch (GameException exception) {
      assertEquals("Battle state must be RUNNING (found PLANNED).", exception.getMessage());
    }
    simpleBattle.start();
    assertEquals(Battle.State.RUNNING, simpleBattle.getState());
    simpleBattle.finish();
    assertEquals(Battle.State.FINISHED, simpleBattle.getState());
    assertEquals(956228, simpleBattle.hashCode());  // NOTE: The same as for reverseBattle below.
    assertEquals(warriorB0, simpleBattle.getOpponent(warriorA0));
    assertEquals(warriorA0, simpleBattle.getOpponent(warriorB0));
    try {
      simpleBattle.getOpponent(warriorA1);
      fail("Should have thrown an exception.");
    } catch (GameException exception) {
      assertEquals("Prawn (WARRIOR, (playerA, 1), 0.0) not involved in Battle [prawns=[(WARRIOR, (playerA, 0), 0.0), "
          + "(WARRIOR, (playerB, 2), 0.0)], normalizedPositions=[((0, (Point2D.Double[1.0, 1.0], "
          + "Point2D.Double[4.0, 1.0]), 4, 0.0), 1), ((0, (Point2D.Double[1.0, 1.0], "
          + "Point2D.Double[4.0, 1.0]), 4, 0.0), 2)], state=FINISHED] .",
          exception.getMessage());
    }
  }

  @Test
  public void testReverseBattleCreated() throws BoardException, GameException {
    warriorA0.setCurrentPosition(new Position(ROAD_SECTION_0, 1));
    warriorB0.setCurrentPosition(new Position(ROAD_SECTION_0, 2));
    Battle reverseBattle = new Battle(warriorB0, new Position(ROAD_SECTION_0, 2), null, warriorA0,
        new Position(ROAD_SECTION_0, 1), new Position(ROAD_SECTION_0, 2));
    assertArrayEquals(new Prawn[]{warriorA0, warriorB0}, reverseBattle.getPrawns());
    assertArrayEquals(new Position[]{new Position(ROAD_SECTION_0, 1), new Position(ROAD_SECTION_0, 2)},
        reverseBattle.getNormalizedPositions());
    assertArrayEquals(new RoadSection.Direction[]{RoadSection.Direction.FORWARD, RoadSection.Direction.BACKWARD},
        reverseBattle.getAttackDirections());
    assertEquals(Battle.State.PLANNED, reverseBattle.getState());
    try {
      reverseBattle.finish();
      fail("Should have thrown an exception.");
    } catch (GameException exception) {
      assertEquals("Battle state must be RUNNING (found PLANNED).", exception.getMessage());
    }
    reverseBattle.start();
    assertEquals(Battle.State.RUNNING, reverseBattle.getState());
    reverseBattle.finish();
    assertEquals(Battle.State.FINISHED, reverseBattle.getState());
    assertEquals(956228, reverseBattle.hashCode());  // NOTE: The same as for simpleBattle above.
  }

  @Test
  public void testReverseBattleDirection() throws BoardException, GameException {
    warriorA0.setCurrentPosition(new Position(ROAD_SECTION_0, 2));
    warriorB0.setCurrentPosition(new Position(ROAD_SECTION_0, 1));
    Battle simpleBattle = new Battle(warriorA0, new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_0, 1),
        warriorB0, new Position(ROAD_SECTION_0, 1), null);
    assertArrayEquals(new Prawn[]{warriorA0, warriorB0}, simpleBattle.getPrawns());
    assertArrayEquals(new Position[]{new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_0, 1)},
        simpleBattle.getNormalizedPositions());
    assertArrayEquals(new RoadSection.Direction[]{RoadSection.Direction.BACKWARD, RoadSection.Direction.FORWARD},
        simpleBattle.getAttackDirections());
    assertEquals(Battle.State.PLANNED, simpleBattle.getState());
  }

  @Test
  public void testCurrentPrawnPositionsOnlyMatterWhenStarting() throws BoardException, GameException {
    warriorA0.setCurrentPosition(new Position(ROAD_SECTION_0, 0));
    warriorB0.setCurrentPosition(new Position(ROAD_SECTION_0, 2));
    Battle simpleBattle = new Battle(warriorA0, new Position(ROAD_SECTION_0, 1), new Position(ROAD_SECTION_0, 2),
        warriorB0, new Position(ROAD_SECTION_0, 2), null);
    assertArrayEquals(new Prawn[]{warriorA0, warriorB0}, simpleBattle.getPrawns());
    assertArrayEquals(new Position[]{new Position(ROAD_SECTION_0, 1), new Position(ROAD_SECTION_0, 2)},
        simpleBattle.getNormalizedPositions());
    assertArrayEquals(new RoadSection.Direction[]{RoadSection.Direction.FORWARD, RoadSection.Direction.BACKWARD},
        simpleBattle.getAttackDirections());
    assertEquals(Battle.State.PLANNED, simpleBattle.getState());
    try {
      simpleBattle.start();
      fail("Should have thrown an exception.");
    } catch (GameException exception) {
      assertEquals("Prawn Positions have changed. Expected: "
          + "(((0, (Point2D.Double[1.0, 1.0], Point2D.Double[4.0, 1.0]), 4, 0.0), 1), "
          + "((0, (Point2D.Double[1.0, 1.0], Point2D.Double[4.0, 1.0]), 4, 0.0), 2)), "
          + "actual: "
          + "(((0, (Point2D.Double[1.0, 1.0], Point2D.Double[4.0, 1.0]), 4, 0.0), 0), "
          + "((0, (Point2D.Double[1.0, 1.0], Point2D.Double[4.0, 1.0]), 4, 0.0), 2)).",
          exception.getMessage());
    }
    warriorA0.setCurrentPosition(new Position(ROAD_SECTION_0, 1));
    simpleBattle.start();
  }

  @Test
  public void testCurrentPrawnPositionsOnlyMatterWhenStartingReverse() throws BoardException, GameException {
    warriorA0.setCurrentPosition(new Position(ROAD_SECTION_0, 0));
    warriorB0.setCurrentPosition(new Position(ROAD_SECTION_0, 2));
    Battle reverseBattle = new Battle(warriorB0, new Position(ROAD_SECTION_0, 2), null, warriorA0,
        new Position(ROAD_SECTION_0, 1), new Position(ROAD_SECTION_0, 2));
    assertArrayEquals(new Prawn[]{warriorA0, warriorB0}, reverseBattle.getPrawns());
    assertArrayEquals(new Position[]{new Position(ROAD_SECTION_0, 1), new Position(ROAD_SECTION_0, 2)},
        reverseBattle.getNormalizedPositions());
    assertArrayEquals(new RoadSection.Direction[]{RoadSection.Direction.FORWARD, RoadSection.Direction.BACKWARD},
        reverseBattle.getAttackDirections());
    assertEquals(Battle.State.PLANNED, reverseBattle.getState());
    try {
      reverseBattle.start();
      fail("Should have thrown an exception.");
    } catch (GameException exception) {
      assertEquals("Prawn Positions have changed. Expected: "
          + "(((0, (Point2D.Double[1.0, 1.0], Point2D.Double[4.0, 1.0]), 4, 0.0), 1), "
          + "((0, (Point2D.Double[1.0, 1.0], Point2D.Double[4.0, 1.0]), 4, 0.0), 2)), "
          + "actual: "
          + "(((0, (Point2D.Double[1.0, 1.0], Point2D.Double[4.0, 1.0]), 4, 0.0), 0), "
          + "((0, (Point2D.Double[1.0, 1.0], Point2D.Double[4.0, 1.0]), 4, 0.0), 2)).",
          exception.getMessage());
    }
    warriorA0.setCurrentPosition(new Position(ROAD_SECTION_0, 1));
    reverseBattle.start();
  }

  @Test
  public void testCrossRoadSectionBattle1Created() throws BoardException, GameException {
    warriorA0.setCurrentPosition(new Position(ROAD_SECTION_0, 2));
    warriorB0.setCurrentPosition(new Position(ROAD_SECTION_1, 0));
    int battleHashCode;
    {
      Battle battle = new Battle(warriorA0, new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_0, 3), warriorB0,
          new Position(ROAD_SECTION_0, 3), new Position(ROAD_SECTION_0, 2));
      assertArrayEquals(new Prawn[]{warriorA0, warriorB0}, battle.getPrawns());
      final Position[] normalizedPositions = battle.getNormalizedPositions();
      assertArrayEquals(new Position[]{new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_1, 0)},
          normalizedPositions);
      assertArrayEquals(new RoadSection.Direction[]{RoadSection.Direction.FORWARD, RoadSection.Direction.BACKWARD},
          battle.getAttackDirections());
      assertTrue(normalizedPositions[0].getRoadSection().equals(ROAD_SECTION_0));
      assertTrue(normalizedPositions[1].getRoadSection().equals(ROAD_SECTION_0));
      battleHashCode = battle.hashCode();
      assertEquals(956195, battleHashCode);
    }
    {
      Battle battle = new Battle(warriorB0, new Position(ROAD_SECTION_0, 3), new Position(ROAD_SECTION_0, 2), warriorA0,
          new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_0, 3));
      assertArrayEquals(new Prawn[]{warriorA0, warriorB0}, battle.getPrawns());
      final Position[] normalizedPositions = battle.getNormalizedPositions();
      assertArrayEquals(new Position[]{new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_1, 0)},
          normalizedPositions);
      assertArrayEquals(new RoadSection.Direction[]{RoadSection.Direction.FORWARD, RoadSection.Direction.BACKWARD},
          battle.getAttackDirections());
      assertTrue(normalizedPositions[0].getRoadSection().equals(ROAD_SECTION_0));
      assertTrue(normalizedPositions[1].getRoadSection().equals(ROAD_SECTION_0));
      assertEquals(battleHashCode, battle.hashCode());
    }
  }

  @Test
  public void testCrossRoadSectionBattle2Created() throws BoardException, GameException {
    warriorA0.setCurrentPosition(new Position(ROAD_SECTION_0, 2));
    warriorB0.setCurrentPosition(new Position(ROAD_SECTION_0, 3));
    {
      Battle battle = new Battle(warriorA0, new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_0, 1), warriorB0,
          new Position(ROAD_SECTION_1, 0), new Position(ROAD_SECTION_0, 2));
      assertArrayEquals(new Prawn[]{warriorA0, warriorB0}, battle.getPrawns());
      final Position[] normalizedPositions = battle.getNormalizedPositions();
      assertArrayEquals(new Position[]{new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_1, 0)},
          normalizedPositions);
      assertArrayEquals(new RoadSection.Direction[]{RoadSection.Direction.FORWARD, RoadSection.Direction.BACKWARD},
          battle.getAttackDirections());
      assertTrue(normalizedPositions[0].getRoadSection().equals(ROAD_SECTION_0));
      assertTrue(normalizedPositions[1].getRoadSection().equals(ROAD_SECTION_0));
    }
    {
      Battle battle = new Battle(warriorB0, new Position(ROAD_SECTION_1, 0), new Position(ROAD_SECTION_0, 2), warriorA0,
          new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_0, 1));
      assertArrayEquals(new Prawn[]{warriorA0, warriorB0}, battle.getPrawns());
      final Position[] normalizedPositions = battle.getNormalizedPositions();
      assertArrayEquals(new Position[]{new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_1, 0)},
          normalizedPositions);
      assertArrayEquals(new RoadSection.Direction[]{RoadSection.Direction.FORWARD, RoadSection.Direction.BACKWARD},
          battle.getAttackDirections());
      assertTrue(normalizedPositions[0].getRoadSection().equals(ROAD_SECTION_0));
      assertTrue(normalizedPositions[1].getRoadSection().equals(ROAD_SECTION_0));
    }
  }

  @Test
  public void testCrossRoadSectionBattle3Created() throws BoardException, GameException {
    warriorA0.setCurrentPosition(new Position(ROAD_SECTION_0, 3));
    warriorB0.setCurrentPosition(new Position(ROAD_SECTION_1, 1));
    {
      Battle battle = new Battle(warriorA0, new Position(ROAD_SECTION_0, 3), new Position(ROAD_SECTION_1, 1), warriorB0,
          new Position(ROAD_SECTION_1, 1), new Position(ROAD_SECTION_0, 3));
      assertArrayEquals(new Prawn[]{warriorA0, warriorB0}, battle.getPrawns());
      final Position[] normalizedPositions = battle.getNormalizedPositions();
      assertArrayEquals(new Position[]{new Position(ROAD_SECTION_1, 0), new Position(ROAD_SECTION_1, 1)},
          normalizedPositions);
      assertArrayEquals(new RoadSection.Direction[]{RoadSection.Direction.FORWARD, RoadSection.Direction.BACKWARD},
          battle.getAttackDirections());
      assertTrue(normalizedPositions[0].getRoadSection().equals(ROAD_SECTION_1));
      assertTrue(normalizedPositions[1].getRoadSection().equals(ROAD_SECTION_1));
    }
    {
      Battle battle = new Battle(warriorB0, new Position(ROAD_SECTION_1, 1), new Position(ROAD_SECTION_1, 0), warriorA0,
          new Position(ROAD_SECTION_0, 3), new Position(ROAD_SECTION_1, 1));
      assertArrayEquals(new Prawn[]{warriorA0, warriorB0}, battle.getPrawns());
      final Position[] normalizedPositions = battle.getNormalizedPositions();
      assertArrayEquals(new Position[]{new Position(ROAD_SECTION_1, 0), new Position(ROAD_SECTION_1, 1)},
          normalizedPositions);
      assertArrayEquals(new RoadSection.Direction[]{RoadSection.Direction.FORWARD, RoadSection.Direction.BACKWARD},
          battle.getAttackDirections());
      assertTrue(normalizedPositions[0].getRoadSection().equals(ROAD_SECTION_1));
      assertTrue(normalizedPositions[1].getRoadSection().equals(ROAD_SECTION_1));
    }
  }

  @Test
  public void testBattleConditionsNotMetFails() throws BoardException, GameException {
    warriorA0.setCurrentPosition(new Position(ROAD_SECTION_0, 0));
    warriorB0.setCurrentPosition(new Position(ROAD_SECTION_0, 2));
    try {
      new Battle(warriorA0, new Position(ROAD_SECTION_0, 1), new Position(ROAD_SECTION_0, 0), warriorB0,
          new Position(ROAD_SECTION_0, 2), null);
      fail("Should have thrown an exception.");
    } catch (GameException exception) {
      assertEquals("Battle conditions between Prawns "
          + "(WARRIOR, (playerA, 0), 0.0) at "
          + "((0, (Point2D.Double[1.0, 1.0], Point2D.Double[4.0, 1.0]), 4, 0.0), 1)->"
          + "((0, (Point2D.Double[1.0, 1.0], Point2D.Double[4.0, 1.0]), 4, 0.0), 0) and "
          + "(WARRIOR, (playerB, 2), 0.0) at "
          + "((0, (Point2D.Double[1.0, 1.0], Point2D.Double[4.0, 1.0]), 4, 0.0), 2)->null are not met.",
          exception.getMessage());
    }
  }

  @Test
  public void testBattleConditionsMet() throws BoardException, GameException {
    assertTrue(Battle.allBattleConditionsMet(warriorA0, new Position(ROAD_SECTION_0, 1),
        new Position(ROAD_SECTION_0, 2), warriorB0, new Position(ROAD_SECTION_0, 2), null));
    assertTrue(Battle.allBattleConditionsMet(warriorB0, new Position(ROAD_SECTION_0, 2), null, warriorA0,
        new Position(ROAD_SECTION_0, 1), new Position(ROAD_SECTION_0, 2)));
  }

  @Test
  public void testSamePlayerBattleConditionsNotMet() throws BoardException, GameException {
    assertFalse(Battle.allBattleConditionsMet(warriorA0, new Position(ROAD_SECTION_0, 1),
        new Position(ROAD_SECTION_0, 2), warriorA1, new Position(ROAD_SECTION_0, 2), null));
    assertFalse(Battle.allBattleConditionsMet(warriorA1, new Position(ROAD_SECTION_0, 2), null, warriorA0,
        new Position(ROAD_SECTION_0, 1), new Position(ROAD_SECTION_0, 2)));
  }

  @Test
  public void testNotAdjacentBattleConditionsNotMet() throws BoardException, GameException {
    assertFalse(Battle.allBattleConditionsMet(warriorA0, new Position(ROAD_SECTION_0, 1),
        new Position(ROAD_SECTION_0, 2), warriorB0, new Position(ROAD_SECTION_0, 3), null));
    assertFalse(Battle.allBattleConditionsMet(warriorB0, new Position(ROAD_SECTION_0, 3), null, warriorA0,
        new Position(ROAD_SECTION_0, 1), new Position(ROAD_SECTION_0, 2)));
  }

  @Test
  public void testWarriorsDoesntAttackBattleConditionsNotMet() throws BoardException, GameException {
    assertTrue(Battle.allBattleConditionsMet(warriorA0, new Position(ROAD_SECTION_0, 1),
        new Position(ROAD_SECTION_0, 2), settlersUnitB, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 1)));
    assertTrue(Battle.allBattleConditionsMet(settlersUnitA, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 1), warriorB0, new Position(ROAD_SECTION_0, 1),
        new Position(ROAD_SECTION_0, 2)));
    assertFalse(Battle.allBattleConditionsMet(settlersUnitA, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 1), warriorB0, new Position(ROAD_SECTION_0, 1), null));
    assertFalse(Battle.allBattleConditionsMet(settlersUnitA, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 1), settlersUnitB, new Position(ROAD_SECTION_0, 1),
        new Position(ROAD_SECTION_0, 2)));
  }

  @Test
  public void testAttackEffectivenessMatters() throws BoardException, GameException {
    assertTrue(Battle.allBattleConditionsMet(settlersUnitA, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 1), warriorB0, new Position(ROAD_SECTION_0, 1),
        new Position(ROAD_SECTION_0, 2)));
    assertFalse(Battle.allBattleConditionsMet(settlersUnitA, new Position(ROAD_SECTION_0, 1),
        new Position(ROAD_SECTION_0, 2), warriorB0, new Position(ROAD_SECTION_0, 2),
        new Position(ROAD_SECTION_0, 1)));
    assertFalse(Battle.allBattleConditionsMet(settlersUnitA, new Position(ROAD_SECTION_1, 2),
        new Position(ROAD_SECTION_1, 1), warriorB0, new Position(ROAD_SECTION_1, 1),
        new Position(ROAD_SECTION_1, 2)));
    assertTrue(Battle.allBattleConditionsMet(settlersUnitA, new Position(ROAD_SECTION_1, 1),
        new Position(ROAD_SECTION_1, 2), warriorB0, new Position(ROAD_SECTION_1, 2),
        new Position(ROAD_SECTION_1, 1)));
    assertFalse(Battle.allBattleConditionsMet(settlersUnitA, new Position(ROAD_SECTION_2, 2),
        new Position(ROAD_SECTION_2, 1), warriorB0, new Position(ROAD_SECTION_2, 1),
        new Position(ROAD_SECTION_2, 2)));
    assertFalse(Battle.allBattleConditionsMet(settlersUnitA, new Position(ROAD_SECTION_2, 1),
        new Position(ROAD_SECTION_2, 2), warriorB0, new Position(ROAD_SECTION_2, 2),
        new Position(ROAD_SECTION_2, 1)));
  }

  @Test
  public void testNonCollidingDirectionsBattleConditionsNotMet() throws BoardException, GameException {
    assertTrue(Battle.allBattleConditionsMet(warriorA0, new Position(ROAD_SECTION_0, 1),
        new Position(ROAD_SECTION_0, 2), warriorB0, new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_0, 1)));
    assertTrue(Battle.allBattleConditionsMet(warriorA0, new Position(ROAD_SECTION_0, 1), null, warriorB0,
        new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_0, 1)));
    assertTrue(Battle.allBattleConditionsMet(warriorA0, new Position(ROAD_SECTION_0, 1),
        new Position(ROAD_SECTION_0, 2), warriorB0, new Position(ROAD_SECTION_0, 2), null));
    assertFalse(Battle.allBattleConditionsMet(warriorA0, new Position(ROAD_SECTION_0, 1), null, warriorB0,
        new Position(ROAD_SECTION_0, 2), null));
    assertFalse(Battle.allBattleConditionsMet(warriorA0, new Position(ROAD_SECTION_0, 1), null, warriorB0,
        new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_0, 3)));
  }

}
