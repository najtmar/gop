package cz.wie.najtmar.gop.game;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import cz.wie.najtmar.gop.board.Board;
import cz.wie.najtmar.gop.board.BoardException;
import cz.wie.najtmar.gop.board.DirectedPosition;
import cz.wie.najtmar.gop.board.Joint;
import cz.wie.najtmar.gop.board.Position;
import cz.wie.najtmar.gop.board.RoadSection;
import cz.wie.najtmar.gop.common.Constants;
import cz.wie.najtmar.gop.common.Point2D;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Properties;
import java.util.UUID;

/**
 * Tests for class Prawn.
 * @author najtmar
 */
public class PrawnTest {

  static final long START_TIME = 966;
  static final Game DUMMY_GAME = mock(Game.class);
  static final GameProperties MOCK_GAME_PROPERTIES = mock(GameProperties.class);
  static final Player PLAYER_0 = new Player(new User("playerA", new UUID(12345, 67890)), 0, DUMMY_GAME);
  static final Player PLAYER_1 = new Player(new User("playerB", new UUID(98765, 43210)), 1, DUMMY_GAME);

  static Board BOARD;
  static Joint JOINT_0_0;
  static Joint JOINT_3_0;
  static Joint JOINT_3_3;
  static RoadSection ROAD_SECTION_0;
  static RoadSection ROAD_SECTION_1;
  static Position POSITION_0;
  static Position POSITION_1;
  static Position POSITION_MOVING;
  static Position POSITION_MIDDLE;
  static final long START_MOVE_TIME = 1000;

  TestPrawn prawn0;
  TestPrawn prawn1;
  TestPrawn movingPrawnPartialMock;
  Itinerary itinerary0;

  /**
   * Executed before the class is initialized.
   * @throws Exception when tests cannot be initialized
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    final Properties simpleBoardPropertiesSet = new Properties();
    simpleBoardPropertiesSet.load(PrawnTest.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/board/simple-board.properties"));
    BOARD = new Board(simpleBoardPropertiesSet);
    JOINT_0_0 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setName("JOINT_0_0")
        .setCoordinatesPair(new Point2D.Double(0.0, 0.0))
        .build();
    JOINT_3_0 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(1)
        .setName("JOINT_3_0")
        .setCoordinatesPair(new Point2D.Double(3.0, 0.0))
        .build();
    JOINT_3_3 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(2)
        .setName("JOINT_3_3")
        .setCoordinatesPair(new Point2D.Double(3.0, 3.0))
        .build();
    ROAD_SECTION_0 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(JOINT_0_0, JOINT_3_0)
        .build();
    ROAD_SECTION_1 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(1)
        .setJoints(JOINT_3_0, JOINT_3_3)
        .build();
    POSITION_0 = new Position(ROAD_SECTION_0, 0);
    POSITION_1 = new Position(ROAD_SECTION_0, 3);
    POSITION_MOVING = new Position(ROAD_SECTION_0, 0);
    POSITION_MIDDLE = new Position(ROAD_SECTION_0, 1);
  }

  /**
   * Executed after the class is teared down.
   */
  @AfterClass
  public static void tearDownAfterClass() {
    verify(MOCK_GAME_PROPERTIES, times(0)).getMaxPlayerCount();
    verify(MOCK_GAME_PROPERTIES, times(0)).getBasicVelocity();
    verify(MOCK_GAME_PROPERTIES, times(0)).getVelocityDirContribFactor();
    verify(MOCK_GAME_PROPERTIES, times(0)).getProduceActionProgressFactor();
    verify(MOCK_GAME_PROPERTIES, times(0)).getRepairActionProgressFactor();
    verify(MOCK_GAME_PROPERTIES, times(0)).getGrowActionProgressFactor();
  }

  /**
   * @throws java.lang.Exception when test cannot be set up
   */
  @Before
  public void setUp() throws Exception {
    TestPrawn.resetFirstAvailableId();
    prawn0 = new TestPrawn(MOCK_GAME_PROPERTIES, PLAYER_0, POSITION_0, START_TIME);
    prawn1 = new TestPrawn(MOCK_GAME_PROPERTIES, PLAYER_1, POSITION_1, START_TIME);
    movingPrawnPartialMock = spy(new TestPrawn(MOCK_GAME_PROPERTIES, PLAYER_0, POSITION_0, START_TIME));
    movingPrawnPartialMock.setCurrentPosition(new Position(ROAD_SECTION_0, 0));
    movingPrawnPartialMock.setItinerary((new Itinerary.Builder())
        .addPosition(new Position(ROAD_SECTION_0, 0))
        .addPosition(new Position(ROAD_SECTION_0, 3))
        .build());
    movingPrawnPartialMock.setLastMoveTime(START_MOVE_TIME);
    itinerary0 = (new Itinerary.Builder())
        .addPosition(new Position(ROAD_SECTION_0, 0))
        .addPosition(new Position(ROAD_SECTION_0, 2))
        .build();
  }

  @Test
  public void testPrawnsGeneratedCorrectly() {
    assertEquals(Prawn.MAX_ENERGY, prawn0.getEnergy(), 0.0);
    assertEquals(0, prawn0.getId());
    assertEquals(POSITION_0, prawn0.getCurrentPosition());
    assertEquals(0, prawn0.hashCode());
    assertEquals(Prawn.MAX_ENERGY, prawn1.getEnergy(), 0.0);
    assertEquals(1, prawn1.getId());
    assertEquals(POSITION_1, prawn1.getCurrentPosition());
    assertEquals(1, prawn1.hashCode());
  }

  @Test
  public void testEnergyManipulation() {
    assertEquals(false, prawn0.decreaseEnergy(0.4));
    assertEquals(0.6, prawn0.getEnergy(), Constants.DOUBLE_DELTA);
    assertEquals(false, prawn0.decreaseEnergy(0.4));
    assertEquals(0.2, prawn0.getEnergy(), Constants.DOUBLE_DELTA);
    assertEquals(true, prawn0.decreaseEnergy(0.4));
    assertEquals(0.0, prawn0.getEnergy(), 0.0);
    assertEquals(false, prawn0.increaseEnergy(0.4));
    assertEquals(0.4, prawn0.getEnergy(), Constants.DOUBLE_DELTA);
    assertEquals(false, prawn0.increaseEnergy(0.4));
    assertEquals(0.8, prawn0.getEnergy(), Constants.DOUBLE_DELTA);
    assertEquals(true, prawn0.increaseEnergy(0.4));
    assertEquals(1.0, prawn0.getEnergy(), 0.0);
  }

  @Test
  public void testNonJointPositionFails() {
    try {
      new TestPrawn(MOCK_GAME_PROPERTIES, PLAYER_0, POSITION_MIDDLE, START_TIME);
      fail("Should have thrown an exception.");
    } catch (GameException exception) {
      assertEquals("Newly created Prawn must be located on a Joint (Position 1 found).", exception.getMessage());
    }
  }

  @Test
  public void testItinerarySetProperly() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(ROAD_SECTION_0, 0))
        .addPosition(new Position(ROAD_SECTION_1, 0))
        .addPosition(new Position(ROAD_SECTION_1, 3))
        .build();
    prawn0.setItinerary(itinerary);
    assertEquals(itinerary, prawn0.getItinerary());
  }

  @Test
  public void testItineraryFromJointSetProperlyAndPositionUpdated() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(ROAD_SECTION_1, 0))
        .addPosition(new Position(ROAD_SECTION_1, 3))
        .build();
    assertTrue(prawn1.getCurrentPosition().getRoadSection().equals(ROAD_SECTION_0));
    assertEquals(3, prawn1.getCurrentPosition().getIndex());
    prawn1.setItinerary(itinerary);
    assertEquals(itinerary, prawn1.getItinerary());
    assertTrue(prawn1.getCurrentPosition().getRoadSection().equals(ROAD_SECTION_1));
    assertEquals(0, prawn1.getCurrentPosition().getIndex());
  }

  @Test
  public void testFinishedItineraryCannotBeSet() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(ROAD_SECTION_0, 0))
        .addPosition(new Position(ROAD_SECTION_0, 3))
        .build();

    // Finish the itinerary.
    Itinerary.Iterator iterator = itinerary.itineraryIterator();
    iterator.next();
    iterator.next();
    iterator.next();
    iterator.next();
    itinerary.setCurrentPosition(iterator);

    try {
      prawn1.setItinerary(itinerary);
      fail("Should have thrown an exception.");
    } catch (GameException exception) {
      assertEquals("Cannot set finished Itinerary.", exception.getMessage());
    }
  }

  @Test
  public void testItineraryMustStartAtCurrentPosition() throws Exception {
    Itinerary itinerary = (new Itinerary.Builder())
        .addPosition(new Position(ROAD_SECTION_0, 0))
        .addPosition(new Position(ROAD_SECTION_1, 0))
        .addPosition(new Position(ROAD_SECTION_1, 3))
        .build();
    try {
      prawn1.setItinerary(itinerary);
      fail("Should have thrown an exception.");
    } catch (GameException exception) {
      assertEquals("Itinerary must start in the current Prawn position ("
          + "Prawn position: ((0, (Point2D.Double[0.0, 0.0], Point2D.Double[3.0, 0.0]), 4, 0.0), 3), "
          + "Itinerary position: ((0, (Point2D.Double[0.0, 0.0], Point2D.Double[3.0, 0.0]), 4, 0.0), 0)).",
          exception.getMessage());
    }
  }

  @Test
  public void testMoveWorks() throws BoardException, GameException {
    assertEquals(new Position(ROAD_SECTION_0, 0), movingPrawnPartialMock.getCurrentPosition());
    movingPrawnPartialMock.move(new Move(new Position(ROAD_SECTION_0, 1), START_MOVE_TIME + 10, false),
        START_MOVE_TIME + 10 + 1);
    assertEquals(new Position(ROAD_SECTION_0, 1), movingPrawnPartialMock.getCurrentPosition());
    assertEquals(START_MOVE_TIME + 10, movingPrawnPartialMock.getLastMoveTime());
    assertTrue(movingPrawnPartialMock.getItinerary() != null);
    movingPrawnPartialMock.clearItinerary();
    assertEquals(null, movingPrawnPartialMock.getItinerary());
  }

  @Test
  public void testMoveFirstWorks() throws BoardException, GameException {
    movingPrawnPartialMock.move(new Move(new Position(ROAD_SECTION_0, 1), START_MOVE_TIME, false),
        START_MOVE_TIME + 1);
    assertEquals(new Position(ROAD_SECTION_0, 1), movingPrawnPartialMock.getCurrentPosition());
    assertEquals(START_MOVE_TIME, movingPrawnPartialMock.getLastMoveTime());
  }

  @Test
  public void testMoveAndStopWorks() throws BoardException, GameException {
    movingPrawnPartialMock.move(new Move(new Position(ROAD_SECTION_0, 1), START_MOVE_TIME + 10, false),
        START_MOVE_TIME + 10 + 1);
    movingPrawnPartialMock.move(new Move(new Position(ROAD_SECTION_0, 2), START_MOVE_TIME + 20, false),
        START_MOVE_TIME + 20 + 1);
    movingPrawnPartialMock.move(new Move(new Position(ROAD_SECTION_0, 3), START_MOVE_TIME + 30, true),
        START_MOVE_TIME + 30 + 1);
    assertEquals(new Position(ROAD_SECTION_0, 3), movingPrawnPartialMock.getCurrentPosition());
    assertEquals(START_MOVE_TIME + 30, movingPrawnPartialMock.getLastMoveTime());
    assertEquals(null, movingPrawnPartialMock.getItinerary());
  }

  @Test
  public void testStoppingNotMarkedAsStoppingFails() throws BoardException, GameException {
    try {
      movingPrawnPartialMock.move(new Move(new Position(ROAD_SECTION_0, 1), START_MOVE_TIME + 10, false),
          START_MOVE_TIME + 10 + 1);
      movingPrawnPartialMock.move(new Move(new Position(ROAD_SECTION_0, 2), START_MOVE_TIME + 20, false),
          START_MOVE_TIME + 20 + 1);
      movingPrawnPartialMock.move(new Move(new Position(ROAD_SECTION_0, 3), START_MOVE_TIME + 30, false),
          START_MOVE_TIME + 30 + 1);
      fail("Should have thrown an exception.");
    } catch (GameException exception) {
      assertEquals("Move that finishes an Itinerary must be explicitly marked as stopping (found "
          + "(playerA, 2), (((0, (Point2D.Double[0.0, 0.0], Point2D.Double[3.0, 0.0]), 4, 0.0), 3), 1030, false)).",
          exception.getMessage());
    }
  }

  @Test
  public void testMoveTimeJustRight() throws BoardException, GameException {
    movingPrawnPartialMock.move(new Move(new Position(ROAD_SECTION_0, 1), START_MOVE_TIME + 10, false),
        START_MOVE_TIME + 10);
    assertEquals(new Position(ROAD_SECTION_0, 1), movingPrawnPartialMock.getCurrentPosition());
    assertEquals(START_MOVE_TIME + 10, movingPrawnPartialMock.getLastMoveTime());
    assertTrue(movingPrawnPartialMock.getItinerary() != null);
  }

  @Test
  public void testStoppingDoesNotFinishFails() throws BoardException, GameException {
    try {
      movingPrawnPartialMock.move(new Move(new Position(ROAD_SECTION_0, 1), START_MOVE_TIME + 10, false),
          START_MOVE_TIME + 10);
      movingPrawnPartialMock.move(new Move(new Position(ROAD_SECTION_0, 2), START_MOVE_TIME + 20, true),
          START_MOVE_TIME + 20);
      fail("Should have thrown an exception.");
    } catch (GameException exception) {
      assertEquals("Move marked as stopping must finish the Itinerary "
          + "((playerA, 2), (((0, (Point2D.Double[0.0, 0.0], Point2D.Double[3.0, 0.0]), 4, 0.0), 2), 1020, true)).",
          exception.getMessage());
    }
  }

  @Test
  public void testMoveTimeTooSmallFails() throws BoardException {
    try {
      movingPrawnPartialMock.move(new Move(new Position(ROAD_SECTION_0, 1), START_MOVE_TIME + 10, false),
          START_MOVE_TIME + 10 - 1);
      fail("Should have thrown an exception.");
    } catch (GameException exception) {
      assertEquals("Move time smaller than the time of the move to perform (" + (START_MOVE_TIME + 10 - 1) + " < "
          + (START_MOVE_TIME + 10) + ").", exception.getMessage());
    }
  }

  @Test
  public void testInvalidStopFails() throws BoardException {
    try {
      movingPrawnPartialMock.move(new Move(new Position(ROAD_SECTION_0, 1), START_MOVE_TIME + 10, true),
          START_MOVE_TIME + 10);
      fail("Should have thrown an exception.");
    } catch (GameException exception) {
      assertEquals("Move marked as stopping must finish the Itinerary ((playerA, 2), (((0, (Point2D.Double[0.0, 0.0], "
          + "Point2D.Double[3.0, 0.0]), 4, 0.0), 1), 1010, true)).", exception.getMessage());
    }
  }

  @Test
  public void testMoveDoesntMatchItineraryFails() throws BoardException, GameException {
    movingPrawnPartialMock.setItinerary(itinerary0);
    try {
      movingPrawnPartialMock.move(new Move(new Position(ROAD_SECTION_0, 3), START_MOVE_TIME + 30, false),
          START_MOVE_TIME + 30);
      fail("Should have thrown an exception.");
    } catch (GameException exception) {
      assertEquals("Itinerary of (playerA, 2) should be updated to Position "
          + "((0, (Point2D.Double[0.0, 0.0], Point2D.Double[3.0, 0.0]), 4, 0.0), 3) (found "
          + "((0, (Point2D.Double[0.0, 0.0], Point2D.Double[3.0, 0.0]), 4, 0.0), 1)).", exception.getMessage());
    }
  }

  @Test
  public void testMoveDoesntMatchItinerary1Fails() throws BoardException, GameException {
    movingPrawnPartialMock.setItinerary(itinerary0);
    try {
      movingPrawnPartialMock.move(new Move(new Position(ROAD_SECTION_0, 3), START_MOVE_TIME + 30, true),
          START_MOVE_TIME + 30);
      fail("Should have thrown an exception.");
    } catch (GameException exception) {
      assertEquals("Itinerary of (playerA, 2) should be updated to Position "
          + "((0, (Point2D.Double[0.0, 0.0], Point2D.Double[3.0, 0.0]), 4, 0.0), 3) (found "
          + "((0, (Point2D.Double[0.0, 0.0], Point2D.Double[3.0, 0.0]), 4, 0.0), 1)).", exception.getMessage());
    }
  }

  @Test
  public void testInvalidMoveSequenceFails() throws BoardException {
    try {
      movingPrawnPartialMock.move(new Move(new Position(ROAD_SECTION_0, 2), START_MOVE_TIME + 10, false),
          START_MOVE_TIME + 10);
      fail("Should have thrown an exception.");
    } catch (GameException exception) {
      assertEquals("Itinerary of (playerA, 2) should be updated to Position "
          + "((0, (Point2D.Double[0.0, 0.0], Point2D.Double[3.0, 0.0]), 4, 0.0), 2) (found "
          + "((0, (Point2D.Double[0.0, 0.0], Point2D.Double[3.0, 0.0]), 4, 0.0), 1)).", exception.getMessage());
    }
  }

  @Test
  public void testStayWorks() throws BoardException, GameException {
    // movingPrawnPartialMock.move(new Move(new Position(ROAD_SECTION_0, 0), START_MOVE_TIME, false), START_MOVE_TIME);
    assertEquals(START_MOVE_TIME, movingPrawnPartialMock.getLastMoveTime());
    assertEquals(new Position(ROAD_SECTION_0, 0), movingPrawnPartialMock.getCurrentPosition());
    movingPrawnPartialMock.stay(START_MOVE_TIME + 100);
    assertEquals(START_MOVE_TIME + 100, movingPrawnPartialMock.getLastMoveTime());
    assertEquals(new Position(ROAD_SECTION_0, 0), movingPrawnPartialMock.getCurrentPosition());
  }

  @Test
  public void testWholeItineraryPrepared() throws BoardException {
    when(movingPrawnPartialMock.calculateVelocity(any(DirectedPosition.class)))
        .thenReturn(1.0 / 10);
    Move[] moveSequence = movingPrawnPartialMock.prepareMoveSequence(START_MOVE_TIME + 30);
    assertArrayEquals(new Move[]{
        new Move(new Position(ROAD_SECTION_0, 0), START_MOVE_TIME, false),
        new Move(new Position(ROAD_SECTION_0, 1), START_MOVE_TIME + 10, false),
        new Move(new Position(ROAD_SECTION_0, 2), START_MOVE_TIME + 20, false),
        new Move(new Position(ROAD_SECTION_0, 3), START_MOVE_TIME + 30, true),
    }, moveSequence);
    verify(movingPrawnPartialMock, times(4)).calculateVelocity(any(DirectedPosition.class));
  }

  @Test
  public void testVaryingVelocity() throws BoardException {
    when(movingPrawnPartialMock.calculateVelocity(any(DirectedPosition.class)))
        .thenReturn(1.0 / 10)
        .thenReturn(1.0 / 20)
        .thenReturn(1.0 / 30)
        .thenReturn(1.0 / 40);
    Move[] moveSequence = movingPrawnPartialMock.prepareMoveSequence(START_MOVE_TIME + 10 + 20 + 30);
    assertArrayEquals(new Move[]{
        new Move(new Position(ROAD_SECTION_0, 0), START_MOVE_TIME, false),
        new Move(new Position(ROAD_SECTION_0, 1), START_MOVE_TIME + 10, false),
        new Move(new Position(ROAD_SECTION_0, 2), START_MOVE_TIME + 10 + 20, false),
        new Move(new Position(ROAD_SECTION_0, 3), START_MOVE_TIME + 10 + 20 + 30, true),
    }, moveSequence);
    verify(movingPrawnPartialMock, times(4)).calculateVelocity(any(DirectedPosition.class));
  }

  @Test
  public void testWholeItineraryPlusPrepared() throws BoardException {
    when(movingPrawnPartialMock.calculateVelocity(any(DirectedPosition.class)))
        .thenReturn(1.0 / 10);
    Move[] moveSequence = movingPrawnPartialMock.prepareMoveSequence(START_MOVE_TIME + 30 + 1);
    assertArrayEquals(new Move[]{
        new Move(new Position(ROAD_SECTION_0, 0), START_MOVE_TIME, false),
        new Move(new Position(ROAD_SECTION_0, 1), START_MOVE_TIME + 10, false),
        new Move(new Position(ROAD_SECTION_0, 2), START_MOVE_TIME + 20, false),
        new Move(new Position(ROAD_SECTION_0, 3), START_MOVE_TIME + 30 + 1, true),
    }, moveSequence);
    verify(movingPrawnPartialMock, times(4)).calculateVelocity(any(DirectedPosition.class));
  }

  @Test
  public void testWholeItineraryMinusPrepared() throws BoardException {
    when(movingPrawnPartialMock.calculateVelocity(any(DirectedPosition.class)))
        .thenReturn(1.0 / 10);
    Move[] moveSequence = movingPrawnPartialMock.prepareMoveSequence(START_MOVE_TIME + 30 - 1);
    assertArrayEquals(new Move[]{
        new Move(new Position(ROAD_SECTION_0, 0), START_MOVE_TIME, false),
        new Move(new Position(ROAD_SECTION_0, 1), START_MOVE_TIME + 10, false),
        new Move(new Position(ROAD_SECTION_0, 2), START_MOVE_TIME + 20, false),
    }, moveSequence);
    verify(movingPrawnPartialMock, times(3)).calculateVelocity(any(DirectedPosition.class));
  }

  @Test
  public void testOneElementItineraryPrepared() throws BoardException {
    when(movingPrawnPartialMock.calculateVelocity(any(DirectedPosition.class)))
        .thenReturn(1.0 / 10);
    Move[] moveSequence = movingPrawnPartialMock.prepareMoveSequence(START_MOVE_TIME + 1);
    assertArrayEquals(new Move[]{
        new Move(new Position(ROAD_SECTION_0, 0), START_MOVE_TIME, false),
    }, moveSequence);
    verify(movingPrawnPartialMock, times(1)).calculateVelocity(any(DirectedPosition.class));
  }

  @Test
  public void testNoItineraryStays() throws BoardException {
    movingPrawnPartialMock.clearItinerary();
    Move[] moveSequence = movingPrawnPartialMock.prepareMoveSequence(START_MOVE_TIME + 13);
    assertArrayEquals(new Move[]{
        new Move(new Position(ROAD_SECTION_0, 0), START_MOVE_TIME + 13, true),
    }, moveSequence);
  }

}
