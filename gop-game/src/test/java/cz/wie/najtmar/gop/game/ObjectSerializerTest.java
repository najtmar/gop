package cz.wie.najtmar.gop.game;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import cz.wie.najtmar.gop.board.Board;
import cz.wie.najtmar.gop.board.DirectedPosition;
import cz.wie.najtmar.gop.board.Joint;
import cz.wie.najtmar.gop.board.Position;
import cz.wie.najtmar.gop.board.RoadSection;
import cz.wie.najtmar.gop.common.Point2D;
import cz.wie.najtmar.gop.event.SerializationException;
import cz.wie.najtmar.gop.game.Battle;
import cz.wie.najtmar.gop.game.City;
import cz.wie.najtmar.gop.game.Game;
import cz.wie.najtmar.gop.game.GameProperties;
import cz.wie.najtmar.gop.game.Move;
import cz.wie.najtmar.gop.game.ObjectSerializer;
import cz.wie.najtmar.gop.game.Player;
import cz.wie.najtmar.gop.game.Warrior;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.StringReader;
import java.util.Properties;
import java.util.UUID;

import javax.json.Json;
import javax.json.JsonObject;

public class ObjectSerializerTest {

  static final long START_TIME = 966;
  static Board BOARD;
  static Joint JOINT_0_0;
  static Joint JOINT_3_0;
  static Joint JOINT_3_3;
  static RoadSection ROAD_SECTION_0;
  static RoadSection ROAD_SECTION_1;

  Game game;
  GameProperties gameProperties;
  User userA;
  User userB;
  Player playerA;
  Player playerB;
  TestPrawn prawnA;
  TestPrawn prawnB;
  ObjectSerializer objectSerializer;
  Position position0;
  Position position1;
  Move move;
  City cityA;
  City cityB;
  CityFactory cityBFactory1;
  Warrior warriorA;
  Warrior warriorB;
  Battle battle;

  /**
   * Method setUpBeforeClass().
   * @throws Exception when method execution fails
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    final Properties simpleBoardPropertiesSet = new Properties();
    simpleBoardPropertiesSet.load(ObjectSerializerTest.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/board/simple-board.properties"));
    BOARD = new Board(simpleBoardPropertiesSet);
    JOINT_0_0 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setName("JOINT_0_0")
        .setCoordinatesPair(new Point2D.Double(0.0, 0.0))
        .build();
    JOINT_3_0 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(1)
        .setName("JOINT_3_0")
        .setCoordinatesPair(new Point2D.Double(3.0, 0.0))
        .build();
    JOINT_3_3 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(2)
        .setName("JOINT_3_3")
        .setCoordinatesPair(new Point2D.Double(3.0, 3.0))
        .build();
    ROAD_SECTION_0 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(JOINT_0_0, JOINT_3_0)
        .build();
    ROAD_SECTION_1 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(1)
        .setJoints(JOINT_3_0, JOINT_3_3)
        .build();
    BOARD.addJoint(JOINT_0_0);
    BOARD.addJoint(JOINT_3_0);
    BOARD.addJoint(JOINT_3_3);
    BOARD.addRoadSection(ROAD_SECTION_0);
    BOARD.addRoadSection(ROAD_SECTION_1);
  }

  /**
   * Method setUp().
   * @throws Exception when method execution fails
   */
  @Before
  public void setUp() throws Exception {
    final Properties simpleGamePropertiesSet = new Properties();
    simpleGamePropertiesSet.load(ObjectSerializerTest.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/game/simple-game.properties"));
    simpleGamePropertiesSet.setProperty(GameProperties.START_TIME_PROPERTY_NAME, "" + START_TIME);
    game = new Game(simpleGamePropertiesSet);
    gameProperties = game.getGameProperties();
    game.setBoard(BOARD);
    userA = new User("userA", new UUID(12345, 67890));
    userB = new User("userB", new UUID(98765, 43210));
    playerA = new Player(userA, 0, game);
    playerB = new Player(userB, 1, game);
    game.addPlayer(playerA);
    game.addPlayer(playerB);
    prawnA = new TestPrawn(gameProperties, playerA, new Position(ROAD_SECTION_0, 0), START_TIME);
    prawnB = new TestPrawn(gameProperties, playerB, new Position(ROAD_SECTION_0, 3), START_TIME);
    game.addPrawn(prawnA);
    game.addPrawn(prawnB);
    position0 = new Position(ROAD_SECTION_0, 0);
    position1 = new Position(ROAD_SECTION_0, 1);
    move = new Move(position1, 123456, true);
    City.resetFirstAvailableId();
    cityA = (new City.Builder())
        .setGame(game)
        .setJoint(JOINT_0_0)
        .setName("cityA")
        .setPlayer(playerA)
        .build();
    cityB = (new City.Builder())
        .setGame(game)
        .setJoint(JOINT_3_0)
        .setName("cityB")
        .setPlayer(playerB)
        .build();
    cityB.grow();
    cityBFactory1 = cityB.getFactory(1);
    warriorA = (new Warrior.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(new Position(ROAD_SECTION_0, 0))
        .setDirection(0.0)
        .setGameProperties(gameProperties)
        .setPlayer(playerA)
        .build();
    warriorB = (new Warrior.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(new Position(ROAD_SECTION_0, 3))
        .setDirection(Math.PI)
        .setGameProperties(gameProperties)
        .setPlayer(playerB)
        .build();
    game.addPrawn(warriorA);
    game.addPrawn(warriorB);
    battle = new Battle(warriorA, new Position(ROAD_SECTION_0, 1), new Position(ROAD_SECTION_0, 2), warriorB,
        new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_0, 1));
    game.initialize();
    objectSerializer = new ObjectSerializer(game);
  }

  @Test
  public void testPositionSerializationWorks() throws SerializationException {
    final JsonObject positionSerialized = objectSerializer.serializePosition(position1);
    assertTrue(
        positionSerialized.equals(
            Json.createReader(new StringReader(
                "{"
              + "  \"roadSection\": 0, "
              + "  \"index\": 1 "
              + "}"))
              .readObject()));
    final Position positionDeserialized = objectSerializer.deserializeNewPosition(positionSerialized);
    assertEquals(position1, positionDeserialized);
  }

  @Test
  public void testMoveSerializationWorks() throws SerializationException {
    final JsonObject moveSerialized = objectSerializer.serializeMove(move);
    assertTrue(
        moveSerialized.equals(
            Json.createReader(new StringReader(
                "{"
              + "  \"position\": {"
              + "    \"roadSection\": 0, "
              + "    \"index\": 1 "
              + "  }, "
              + "  \"moveTime\": 123456, "
              + "  \"stop\": true "
              + "}"))
              .readObject()));
    final Move moveDeserialized = objectSerializer.deserializeNewMove(moveSerialized);
    assertEquals(move, moveDeserialized);
  }

  @Test
  public void testPrawnSerializationWorks() throws Exception {
    final int prawnSerialized = objectSerializer.serializePrawn(prawnB);
    assertEquals(prawnB.getId(), prawnSerialized);
    assertEquals(prawnB, objectSerializer.deserializePrawn(prawnSerialized, false));
    assertEquals(prawnB, objectSerializer.deserializePrawn(prawnSerialized, true));
    game.removePrawn(prawnB);
    assertEquals(prawnB, objectSerializer.deserializePrawn(prawnSerialized, true));
  }

  @Test
  public void testUserSerializationWorks() throws SerializationException {
    final JsonObject userSerialized = objectSerializer.serializeUser(userB);
    assertEquals(Json.createReader(new StringReader(
          "{"
        + "  \"name\": \"userB\", "
        + "  \"uuid\": \"00000000-0001-81cd-0000-00000000a8ca\" "
        + "}"))
        .readObject(), userSerialized);
    final User userDeserialized = objectSerializer.deserializeNewUser(userSerialized);
    assertEquals(userB, userDeserialized);
    assertFalse(userB == userDeserialized);
  }

  @Test
  public void testPlayerSerializationWorks() throws SerializationException {
    final int playerSerialized = objectSerializer.serializePlayer(playerB);
    assertEquals(playerB.getIndex(), playerSerialized);
    final Player playerDeserialized = objectSerializer.deserializePlayer(playerSerialized);
    assertEquals(playerB, playerDeserialized);
  }

  @Test
  public void testCitySerializationWorks() throws Exception {
    final int citySerialized = objectSerializer.serializeCity(cityB);
    assertEquals(cityB.getId(), citySerialized);
    assertEquals(cityB, objectSerializer.deserializeCity(citySerialized, false));
    assertEquals(cityB, objectSerializer.deserializeCity(citySerialized, true));
    game.destroyCity(cityB);
    assertNull(game.lookupCityById(cityB.getId(), false));
    assertEquals(cityB, objectSerializer.deserializeCity(citySerialized, true));
  }

  @Test
  public void testFactorySerializationWorks() throws Exception {
    final JsonObject factorySerialized = objectSerializer.serializeFactory(cityBFactory1);
    assertEquals(
        Json.createReader(new StringReader(
            "{"
          + "  \"city\": 1, "
          + "  \"index\": 1 "
          + "}"))
          .readObject(),
        factorySerialized);
    {
      final CityFactory factoryDeserialized = objectSerializer.deserializeFactory(factorySerialized, false);
      assertEquals(cityB, factoryDeserialized.getCity());
      assertEquals(1, factoryDeserialized.getIndex());
    }
    {
      final CityFactory factoryDeserialized = objectSerializer.deserializeFactory(factorySerialized, true);
      assertEquals(cityB, factoryDeserialized.getCity());
      assertEquals(1, factoryDeserialized.getIndex());
    }
    game.destroyCity(cityB);
    assertNull(game.lookupCityById(cityB.getId(), false));
    {
      final CityFactory factoryDeserialized = objectSerializer.deserializeFactory(factorySerialized, true);
      assertEquals(cityB, factoryDeserialized.getCity());
      assertEquals(1, factoryDeserialized.getIndex());
    }
  }

  @Test
  public void testBattleSerializationWorks() throws Exception {
    final JsonObject battleSerialized = objectSerializer.serializeBattle(battle);
    assertTrue(
        battleSerialized.equals(
            Json.createReader(new StringReader(
                "{"
              + "  \"prawn0\": " + warriorA.getId() + ", "
              + "  \"prawn1\": " + warriorB.getId() + ", "
              + "  \"normalizedPosition0\": {"
              + "    \"roadSection\": 0, "
              + "    \"index\": 1 "
              + "  }, "
              + "  \"normalizedPosition1\": {"
              + "    \"roadSection\": 0, "
              + "    \"index\": 2 "
              + "  } "
              + "}"))
              .readObject()));
    assertEquals(battle, objectSerializer.deserializeNewBattle(battleSerialized, false));
    assertEquals(battle, objectSerializer.deserializeNewBattle(battleSerialized, true));
    game.removePrawn(warriorA);
    assertEquals(battle, objectSerializer.deserializeNewBattle(battleSerialized, true));
    game.removePrawn(warriorB);
    assertEquals(battle, objectSerializer.deserializeNewBattle(battleSerialized, true));
  }

  @Test
  public void testItineraryDeserializationWorks() throws SerializationException {
    final Itinerary itineraryDeserialized = objectSerializer.deserializeNewItinerary(Json.createReader(new StringReader(
        "[\n"
        + "  {\n"
        + "    \"roadSection\": 0,\n"
        + "    \"index\": 0\n"
        + "  },\n"
        + "  {\n"
        + "    \"roadSection\": 0,\n"
        + "    \"index\": 1\n"
        + "  }\n"
        + "]"))
        .readArray());
    assertArrayEquals(
        new DirectedPosition[]{
            new DirectedPosition(position0, RoadSection.Direction.FORWARD),
            new DirectedPosition(position1, RoadSection.Direction.FORWARD)},
        itineraryDeserialized.getNodes());
    assertNull(objectSerializer.deserializeNewItinerary(Json.createArrayBuilder().build()));
  }

  @Test
  public void testGameSerializationWorks() throws Exception {
    final JsonObject gameSerialized = objectSerializer.serializeGameWithoutPrawnsOrCities();
    final UUID id = game.getId();
    final long timestamp = game.getTimestamp();
    assertEquals(
            Json.createReader(new StringReader(
                    "{ "
                            + "  \"id\": \"" + id.toString() + "\", "
                            + "  \"timestamp\": " + timestamp + ", "
                            + "  \"board\": { "
                            + "    \"properties\": [ "
                            + "      { "
                            + "        \"key\": \"Board.xsize\", "
                            + "        \"value\": \"15\" "
                            + "      }, "
                            + "      { "
                            + "        \"key\": \"Board.stepSize\", "
                            + "        \"value\": \"1.0\" "
                            + "      }, "
                            + "      { "
                            + "        \"key\":\"Board.backgroundImage\", "
                            + "        \"value\":\"/images/gop-background.jpg\" "
                            + "      }, "
                            + "      { "
                            + "        \"key\": \"Board.ysize\", "
                            + "        \"value\":\"10\" "
                            + "      } "
                            + "    ], "
                            + "    \"joints\": [ "
                            + "      { "
                            + "        \"index\": 0, "
                            + "        \"name\": \"JOINT_0_0\", "
                            + "        \"x\": 0.0, "
                            + "        \"y\": 0.0 "
                            + "      }, "
                            + "      { "
                            + "        \"index\": 1, "
                            + "        \"name\": \"JOINT_3_0\", "
                            + "        \"x\": 3.0, "
                            + "        \"y\": 0.0 "
                            + "      }, "
                            + "      { "
                            + "        \"index\": 2, "
                            + "        \"name\": \"JOINT_3_3\", "
                            + "        \"x\": 3.0, "
                            + "        \"y\": 3.0 "
                            + "      } "
                            + "    ], "
                            + "    \"roadSections\": [ "
                            + "      { "
                            + "        \"index\": 0, "
                            + "        \"joint0\": 0, "
                            + "        \"joint1\": 1 "
                            + "      }, "
                            + "      { "
                            + "        \"index\": 1, "
                            + "        \"joint0\": 1, "
                            + "        \"joint1\": 2 "
                            + "      } "
                            + "    ] "
                            + "  }, "
                            + "  \"players\": [ "
                            + "    { "
                            + "      \"index\": 0, "
                            + "      \"user\": { "
                            + "        \"name\":\"userA\", "
                            + "        \"uuid\": \"00000000-0000-3039-0000-000000010932\" "
                            + "      } "
                            + "    }, "
                            + "    { "
                            + "      \"index\": 1, "
                            + "      \"user\": { "
                            + "        \"name\":\"userB\", "
                            + "        \"uuid\": \"00000000-0001-81cd-0000-00000000a8ca\" "
                            + "      } "
                            + "    } "
                            + "  ] "
                            + "}"))
                    .readObject(),
            gameSerialized);
  }

  @Test
  public void testRoadSectionHalfSerializationWorks() throws SerializationException {
    final JsonObject roadSectionHalfSerialized =
        objectSerializer.serializeRoadSectionHalf(ROAD_SECTION_1.getHalves()[0]);
    assertEquals(
        Json.createReader(new StringReader(
            "{"
          + "  \"roadSection\": 1, "
          + "  \"end\": \"BACKWARD\" "
          + "}"))
          .readObject(), roadSectionHalfSerialized);
    final RoadSection.Half roadSectionHalfDeserialized =
        objectSerializer.deserializeRoadSectionHalf(roadSectionHalfSerialized);
    assertEquals(ROAD_SECTION_1.getHalves()[0], roadSectionHalfDeserialized);
  }

  @Test
  public void testRoadSectionHalfSerializationWorks1() throws SerializationException {
    final JsonObject roadSectionHalfSerialized =
        objectSerializer.serializeRoadSectionHalf(ROAD_SECTION_1.getHalves()[1]);
    assertEquals(
        Json.createReader(new StringReader(
            "{"
          + "  \"roadSection\": 1, "
          + "  \"end\": \"FORWARD\" "
          + "}"))
          .readObject(), roadSectionHalfSerialized);
    final RoadSection.Half roadSectionHalfDeserialized =
        objectSerializer.deserializeRoadSectionHalf(roadSectionHalfSerialized);
    assertEquals(ROAD_SECTION_1.getHalves()[1], roadSectionHalfDeserialized);
  }

}
