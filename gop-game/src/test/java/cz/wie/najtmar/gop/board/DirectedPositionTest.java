package cz.wie.najtmar.gop.board;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import cz.wie.najtmar.gop.common.Point2D;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Properties;

public class DirectedPositionTest {

  static Board BOARD;
  static Joint JOINT_0_0;
  static Joint JOINT_3_0;
  static Joint JOINT_3_3;
  static RoadSection ROAD_SECTION_0;
  static RoadSection ROAD_SECTION_1;

  /**
   * Executed before the class is initialized.
   * @throws Exception when tests cannot be initialized
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    final Properties simpleBoardPropertiesSet = new Properties();
    simpleBoardPropertiesSet.load(RoadSectionTest.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/board/simple-board.properties"));
    BOARD = new Board(simpleBoardPropertiesSet);
    JOINT_0_0 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setName("JOINT_0_0")
        .setCoordinatesPair(new Point2D.Double(0.0, 0.0))
        .build();
    JOINT_3_0 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(1)
        .setName("JOINT_3_0")
        .setCoordinatesPair(new Point2D.Double(3.0, 0.0))
        .build();
    JOINT_3_3 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(2)
        .setName("JOINT_3_3")
        .setCoordinatesPair(new Point2D.Double(3.0, 3.0))
        .build();
    ROAD_SECTION_0 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(JOINT_0_0, JOINT_3_0)
        .build();
    ROAD_SECTION_1 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(1)
        .setJoints(JOINT_3_0, JOINT_3_3)
        .build();
  }

  @Test
  public void testDirectedPositionCreationSucceeds() throws BoardException {
    DirectedPosition directedPosition = new DirectedPosition(ROAD_SECTION_0, 1, RoadSection.Direction.FORWARD);
    assertEquals(1, directedPosition.getPosition().getIndex());
    assertEquals(ROAD_SECTION_0, directedPosition.getPosition().getRoadSection());
    assertEquals(RoadSection.Direction.FORWARD, directedPosition.getDirection());
  }

  @Test
  public void testComparisonWorks() throws BoardException, BoardException {
    DirectedPosition directedPosition0 = new DirectedPosition(ROAD_SECTION_0, 0, RoadSection.Direction.FORWARD);
    DirectedPosition directedPosition01 = new DirectedPosition(ROAD_SECTION_0, 0, RoadSection.Direction.FORWARD);
    DirectedPosition directedPosition1 = new DirectedPosition(ROAD_SECTION_0, 1, RoadSection.Direction.FORWARD);
    DirectedPosition directedPosition2 = new DirectedPosition(ROAD_SECTION_0, 0, RoadSection.Direction.BACKWARD);
    assertTrue(directedPosition0.equals(directedPosition0));
    assertTrue(directedPosition0.equals(directedPosition01));
    assertFalse(directedPosition0.equals(directedPosition1));
    assertFalse(directedPosition0.equals(directedPosition2));
    assertFalse(directedPosition0.equals(JOINT_0_0));
  }

  @Test
  public void testComparisonOnJointWorks() throws BoardException, BoardException {
    DirectedPosition directedPosition0 = new DirectedPosition(ROAD_SECTION_0, 3, RoadSection.Direction.FORWARD);
    DirectedPosition directedPosition1 = new DirectedPosition(ROAD_SECTION_1, 0, RoadSection.Direction.FORWARD);
    DirectedPosition directedPosition2 = new DirectedPosition(ROAD_SECTION_1, 0, RoadSection.Direction.BACKWARD);
    assertFalse(directedPosition0.equals(directedPosition1));
    assertFalse(directedPosition0.equals(directedPosition2));
    assertFalse(directedPosition1.equals(directedPosition0));
    assertFalse(directedPosition2.equals(directedPosition0));
  }

}
