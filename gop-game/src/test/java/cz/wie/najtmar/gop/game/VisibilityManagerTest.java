package cz.wie.najtmar.gop.game;

import cz.wie.najtmar.gop.board.Board;
import cz.wie.najtmar.gop.board.Joint;
import cz.wie.najtmar.gop.board.Position;
import cz.wie.najtmar.gop.board.RoadSection;
import cz.wie.najtmar.gop.common.Point2D;
import cz.wie.najtmar.gop.event.Event;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.json.Json;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class VisibilityManagerTest {

  static final long START_TIME = 966;
  static final long INTERRUPT_TIME_PERIOD = 100;
  static Event TEST_EVENT;

  Board board;
  Joint joint0;
  Joint joint1;
  Joint joint2;
  Joint joint3;
  RoadSection roadSection0;
  RoadSection roadSection1;
  RoadSection roadSection2;

  Game game;
  GameProperties gameProperties;
  EventFactory eventFactory;
  Player playerA;
  Player playerB;
  Player playerC;
  Warrior warriorA;
  Warrior warriorB;
  Warrior warriorC;
  SettlersUnit settlersUnitA;
  SettlersUnit settlersUnitB;
  City cityA;
  City cityA1;
  City cityB;
  City cityC;
  VisibilityManager.Factory visibilityManagerAFactory;
  VisibilityManager.Factory visibilityManagerBFactory;
  VisibilityManager.Factory visibilityManagerCFactory;
  VisibilityManager visibilityManagerA;
  VisibilityManager visibilityManagerB;
  VisibilityManager visibilityManagerC;

  /**
   * Method setUpBeforeClass() .
   * @throws Exception when the method execution fails
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    TEST_EVENT = new Event(Json.createObjectBuilder()
        .add("type", "TEST")
        .add("time", START_TIME)
        .add("whatever", "value")
        .build());
  }

  /**
   * Method setUp().
   * @throws Exception when something unexpected happens
   */
  @Before
  public void setUp() throws Exception {
    final Properties simpleBoardPropertiesSet = new Properties();
    simpleBoardPropertiesSet.load(VisibilityManagerTest.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/board/simple-board.properties"));
    board = new Board(simpleBoardPropertiesSet);
    joint0 = (new Joint.Builder())
        .setBoard(board)
        .setIndex(0)
        .setName("joint0")
        .setCoordinatesPair(new Point2D.Double(1.0, 0.0))
        .build();
    joint1 = (new Joint.Builder())
        .setBoard(board)
        .setIndex(1)
        .setName("joint1")
        .setCoordinatesPair(new Point2D.Double(4.0, 0.0))
        .build();
    joint2 = (new Joint.Builder())
        .setBoard(board)
        .setIndex(2)
        .setName("joint2")
        .setCoordinatesPair(new Point2D.Double(4.0, 4.0))
        .build();
    joint3 = (new Joint.Builder())
        .setBoard(board)
        .setIndex(3)
        .setName("joint3")
        .setCoordinatesPair(new Point2D.Double(0.0, 4.0))
        .build();
    roadSection0 = (new RoadSection.Builder())
        .setBoard(board)
        .setIndex(0)
        .setJoints(joint0, joint1)
        .build();
    roadSection1 = (new RoadSection.Builder())
        .setBoard(board)
        .setIndex(1)
        .setJoints(joint1, joint2)
        .build();
    roadSection2 = (new RoadSection.Builder())
        .setBoard(board)
        .setIndex(2)
        .setJoints(joint2, joint3)
        .build();

    final Properties simpleGamePropertiesSet = new Properties();
    simpleGamePropertiesSet.load(VisibilityManagerTest.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/game/simple-game.properties"));
    simpleGamePropertiesSet.setProperty(GameProperties.START_TIME_PROPERTY_NAME, "" + START_TIME);
    game = new Game(simpleGamePropertiesSet);
    game.setBoard(board);
    gameProperties = game.getGameProperties();
    playerA = new Player(new User("playerA", new UUID(12345, 67890)), 0, game);
    playerB = new Player(new User("playerB", new UUID(98765, 43210)), 1, game);
    playerC = new Player(new User("playerC", new UUID(12345, 54321)), 2, game);
    game.addPlayer(playerA);
    game.addPlayer(playerB);
    game.addPlayer(playerC);
    Prawn.resetFirstAvailableId();
    warriorA = (new Warrior.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(joint2.getNormalizedPosition())
        .setDirection(0.0)
        .setGameProperties(gameProperties)
        .setPlayer(playerA)
        .build();
    warriorB = (new Warrior.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(joint1.getNormalizedPosition())
        .setDirection(Math.PI)
        .setGameProperties(gameProperties)
        .setPlayer(playerB)
        .build();
    warriorC = (new Warrior.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(joint0.getNormalizedPosition())
        .setDirection(3 * Math.PI / 2)
        .setGameProperties(gameProperties)
        .setPlayer(playerC)
        .build();
    settlersUnitA = (new SettlersUnit.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(joint2.getNormalizedPosition())
        .setGameProperties(gameProperties)
        .setPlayer(playerA)
        .build();
    settlersUnitB = (new SettlersUnit.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(joint1.getNormalizedPosition())
        .setGameProperties(gameProperties)
        .setPlayer(playerB)
        .build();
    game.addPrawn(warriorA);
    game.addPrawn(warriorB);
    game.addPrawn(warriorC);
    game.addPrawn(settlersUnitA);
    game.addPrawn(settlersUnitB);
    warriorB.setCurrentPosition(new Position(roadSection0, 2));
    warriorC.setCurrentPosition(new Position(roadSection0, 1));
    settlersUnitA.setCurrentPosition(new Position(roadSection2, 4));
    City.resetFirstAvailableId();
    cityA = (new City.Builder())
        .setGame(game)
        .setName("city")
        .setPlayer(playerA)
        .setJoint(joint2)
        .build();
    cityA1 = (new City.Builder())
        .setGame(game)
        .setName("City A")
        .setPlayer(playerA)
        .setJoint(joint3)
        .build();
    cityB = (new City.Builder())
        .setGame(game)
        .setName("City B")
        .setPlayer(playerB)
        .setJoint(joint1)
        .build();
    cityC = (new City.Builder())
        .setGame(game)
        .setName("City C")
        .setPlayer(playerC)
        .setJoint(joint0)
        .build();
    game.initialize();
    game.start();

    final ObjectSerializer objectSerializer = new ObjectSerializer(game);
    eventFactory = new EventFactory(objectSerializer);
    game.advanceTimeTo(START_TIME);
    visibilityManagerAFactory = new VisibilityManager.Factory(game, eventFactory, 0);
    visibilityManagerBFactory = new VisibilityManager.Factory(game, eventFactory, 1);
    visibilityManagerCFactory = new VisibilityManager.Factory(game, eventFactory, 2);
    visibilityManagerA = visibilityManagerAFactory.create();
    visibilityManagerB = visibilityManagerBFactory.create();
    visibilityManagerC = visibilityManagerCFactory.create();
  }

  @Test
  public void testVisiblePrawnsFoundCorrectly() throws Exception {
    visibilityManagerA.initializeVisibleRoadSectionHalves();
    visibilityManagerB.initializeVisibleRoadSectionHalves();
    visibilityManagerC.initializeVisibleRoadSectionHalves();

    assertNull(visibilityManagerA.getVisiblePrawns());
    visibilityManagerA.initializeVisibleAndDiscoveredPrawnsAndCities();
    assertEquals(new HashSet<>(Arrays.asList(warriorA, settlersUnitA, settlersUnitB)),
        visibilityManagerA.getVisiblePrawns());

    assertNull(visibilityManagerB.getVisiblePrawns());
    visibilityManagerB.initializeVisibleAndDiscoveredPrawnsAndCities();
    assertEquals(new HashSet<>(Arrays.asList(warriorB, settlersUnitB, warriorA, warriorC)),
        visibilityManagerB.getVisiblePrawns());

    assertNull(visibilityManagerC.getVisiblePrawns());
    visibilityManagerC.initializeVisibleAndDiscoveredPrawnsAndCities();
    assertEquals(new HashSet<>(Arrays.asList(warriorC, settlersUnitB, warriorB)),
        visibilityManagerC.getVisiblePrawns());
  }

  @Test
  public void testDiscoveredCitiesFoundCorrectly() throws Exception {
    game.removePrawn(warriorC);

    visibilityManagerA.initializeVisibleRoadSectionHalves();
    visibilityManagerB.initializeVisibleRoadSectionHalves();
    visibilityManagerC.initializeVisibleRoadSectionHalves();

    assertNull(visibilityManagerA.getDiscoveredCities());
    visibilityManagerA.initializeVisibleAndDiscoveredPrawnsAndCities();
    assertEquals(new HashSet<>(Arrays.asList(cityA, cityA1, cityB)),
        visibilityManagerA.getDiscoveredCities());

    assertNull(visibilityManagerB.getDiscoveredCities());
    visibilityManagerB.initializeVisibleAndDiscoveredPrawnsAndCities();
    assertEquals(new HashSet<>(Arrays.asList(cityB, cityA, cityC)), visibilityManagerB.getDiscoveredCities());

    assertNull(visibilityManagerC.getVisiblePrawns());
    visibilityManagerC.initializeVisibleAndDiscoveredPrawnsAndCities();
    assertEquals(new HashSet<>(Arrays.asList(cityC, cityB)), visibilityManagerC.getDiscoveredCities());
  }

  @Test
  public void testDiscoveredAndVisibleCitiesRememberedCorrectly() throws Exception {
    game.putPlayerDiscoveredCities(playerC, new HashSet<>(Arrays.asList(cityA1)));
    game.destroyCity(cityA1);

    visibilityManagerC.initializeVisibleRoadSectionHalves();

    assertNull(visibilityManagerC.getDiscoveredCities());
    assertNull(visibilityManagerC.getVisibleCities());
    visibilityManagerC.initializeVisibleAndDiscoveredPrawnsAndCities();
    assertEquals(new HashSet<>(Arrays.asList(cityC, cityB, cityA1)), visibilityManagerC.getDiscoveredCities());
    assertEquals(new HashSet<>(Arrays.asList(cityC, cityB)), visibilityManagerC.getVisibleCities());
  }

  @Test
  public void testDiscoveredAndVisibleCitiesRememberedCorrectly1() throws Exception {
    game.putPlayerDiscoveredCities(playerC, new HashSet<>(Arrays.asList(cityA1)));
    warriorC.setCurrentPosition(new Position(roadSection2, 2));

    visibilityManagerC.initializeVisibleRoadSectionHalves();

    assertNull(visibilityManagerC.getDiscoveredCities());
    assertNull(visibilityManagerC.getVisibleCities());
    visibilityManagerC.initializeVisibleAndDiscoveredPrawnsAndCities();
    assertEquals(new HashSet<>(Arrays.asList(cityC, cityA, cityB, cityA1)), visibilityManagerC.getDiscoveredCities());
    assertEquals(new HashSet<>(Arrays.asList(cityC, cityA, cityB, cityA1)), visibilityManagerC.getVisibleCities());
  }

  @Test
  public void testDiscoveredAndVisibleCitiesForgotCorrectly() throws Exception {
    game.putPlayerDiscoveredCities(playerC, new HashSet<>(Arrays.asList(cityA1)));
    game.destroyCity(cityA1);
    warriorC.setCurrentPosition(new Position(roadSection2, 2));

    visibilityManagerC.initializeVisibleRoadSectionHalves();

    assertNull(visibilityManagerC.getDiscoveredCities());
    assertNull(visibilityManagerC.getVisibleCities());
    visibilityManagerC.initializeVisibleAndDiscoveredPrawnsAndCities();
    assertEquals(new HashSet<>(Arrays.asList(cityC, cityA, cityB)), visibilityManagerC.getDiscoveredCities());
    assertEquals(new HashSet<>(Arrays.asList(cityC, cityA, cityB)), visibilityManagerC.getVisibleCities());
  }

  @Test
  public void testManagerPreparationWorks() throws Exception {
    assertEquals(EventGeneratingExecutor.State.NOT_PREPARED, visibilityManagerA.getState());
    assertEquals(EventGeneratingExecutor.State.NOT_PREPARED, visibilityManagerB.getState());
    assertEquals(EventGeneratingExecutor.State.NOT_PREPARED, visibilityManagerC.getState());

    visibilityManagerA.prepare(Arrays.asList(TEST_EVENT));
    visibilityManagerB.prepare(Arrays.asList(TEST_EVENT));
    visibilityManagerC.prepare(Arrays.asList(TEST_EVENT));

    assertEquals(EventGeneratingExecutor.State.PREPARED, visibilityManagerA.getState());
    assertEquals(EventGeneratingExecutor.State.PREPARED, visibilityManagerB.getState());
    assertEquals(EventGeneratingExecutor.State.PREPARED, visibilityManagerC.getState());

    assertEquals(Arrays.asList(TEST_EVENT), visibilityManagerA.getInternalEventsList());
    assertEquals(Arrays.asList(TEST_EVENT), visibilityManagerB.getInternalEventsList());
    assertEquals(Arrays.asList(TEST_EVENT), visibilityManagerC.getInternalEventsList());

    assertEquals(
        new HashSet<>(Arrays.asList(roadSection1.getHalves()[1], roadSection1.getHalves()[0],
            roadSection2.getHalves()[0], roadSection2.getHalves()[1])),
        visibilityManagerA.getVisibleRoadSectionHalves());
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection1.getHalves()[0], roadSection1.getHalves()[1],
            roadSection0.getHalves()[1], roadSection0.getHalves()[0])),
        visibilityManagerB.getVisibleRoadSectionHalves());
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1])),
        visibilityManagerC.getVisibleRoadSectionHalves());

    assertEquals(
        new HashSet<>(Arrays.asList(warriorA, settlersUnitA, settlersUnitB)),
        visibilityManagerA.getVisiblePrawns());
    assertEquals(
        new HashSet<>(Arrays.asList(warriorB, settlersUnitB, warriorA, warriorC)),
        visibilityManagerB.getVisiblePrawns());
    assertEquals(
        new HashSet<>(Arrays.asList(warriorC, settlersUnitB, warriorB)),
        visibilityManagerC.getVisiblePrawns());

    assertEquals(new HashSet<>(Arrays.asList(cityA, cityA1, cityB)), visibilityManagerA.getDiscoveredCities());
    assertEquals(new HashSet<>(Arrays.asList(cityB, cityA, cityC)), visibilityManagerB.getDiscoveredCities());
    assertEquals(new HashSet<>(Arrays.asList(cityC, cityB)), visibilityManagerC.getDiscoveredCities());
    assertEquals(new HashSet<>(Arrays.asList(cityA, cityA1, cityB)), visibilityManagerA.getVisibleCities());
    assertEquals(new HashSet<>(Arrays.asList(cityB, cityA, cityC)), visibilityManagerB.getVisibleCities());
    assertEquals(new HashSet<>(Arrays.asList(cityC, cityB)), visibilityManagerC.getVisibleCities());
  }

  @Test
  public void testExecutionAndEventGenerationWorks() throws Exception {
    visibilityManagerA.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    visibilityManagerB.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    visibilityManagerC.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));

    visibilityManagerA.executeAndGenerate();
    visibilityManagerB.executeAndGenerate();
    visibilityManagerC.executeAndGenerate();

    assertEquals(EventGeneratingExecutor.State.GENERATED, visibilityManagerA.getState());
    assertEquals(EventGeneratingExecutor.State.GENERATED, visibilityManagerB.getState());
    assertEquals(EventGeneratingExecutor.State.GENERATED, visibilityManagerC.getState());

    assertEquals(Arrays.asList(
        TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "ROAD_SECTION_HALF_DISCOVERED")
            .add("time", START_TIME)
            .add("roadSectionHalf", Json.createObjectBuilder()
                .add("roadSection", 1)
                .add("end", "BACKWARD")
                .build())
            .add("player", 0)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "ROAD_SECTION_HALF_DISCOVERED")
            .add("time", START_TIME)
            .add("roadSectionHalf", Json.createObjectBuilder()
                .add("roadSection", 1)
                .add("end", "FORWARD")
                .build())
            .add("player", 0)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "ROAD_SECTION_HALF_DISCOVERED")
            .add("time", START_TIME)
            .add("roadSectionHalf", Json.createObjectBuilder()
                .add("roadSection", 2)
                .add("end", "BACKWARD")
                .build())
            .add("player", 0)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "ROAD_SECTION_HALF_DISCOVERED")
            .add("time", START_TIME)
            .add("roadSectionHalf", Json.createObjectBuilder()
                .add("roadSection", 2)
                .add("end", "FORWARD")
                .build())
            .add("player", 0)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "PRAWN_APPEARS")
            .add("time", START_TIME)
            .add("prawn", settlersUnitB.getId())
            .add("player", 0)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "CITY_APPEARS")
            .add("time", START_TIME)
            .add("city", cityB.getId())
            .add("player", 0)
            .build())
        ),
        visibilityManagerA.getEventsGenerated());
    assertEquals(Arrays.asList(
        TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "ROAD_SECTION_HALF_DISCOVERED")
            .add("time", START_TIME)
            .add("roadSectionHalf", Json.createObjectBuilder()
                .add("roadSection", 0)
                .add("end", "BACKWARD")
                .build())
            .add("player", 1)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "ROAD_SECTION_HALF_DISCOVERED")
            .add("time", START_TIME)
            .add("roadSectionHalf", Json.createObjectBuilder()
                .add("roadSection", 0)
                .add("end", "FORWARD")
                .build())
            .add("player", 1)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "ROAD_SECTION_HALF_DISCOVERED")
            .add("time", START_TIME)
            .add("roadSectionHalf", Json.createObjectBuilder()
                .add("roadSection", 1)
                .add("end", "BACKWARD")
                .build())
            .add("player", 1)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "ROAD_SECTION_HALF_DISCOVERED")
            .add("time", START_TIME)
            .add("roadSectionHalf", Json.createObjectBuilder()
                .add("roadSection", 1)
                .add("end", "FORWARD")
                .build())
            .add("player", 1)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "PRAWN_APPEARS")
            .add("time", START_TIME)
            .add("prawn", warriorA.getId())
            .add("player", 1)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "PRAWN_APPEARS")
            .add("time", START_TIME)
            .add("prawn", warriorC.getId())
            .add("player", 1)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "CITY_APPEARS")
            .add("time", START_TIME)
            .add("city", cityA.getId())
            .add("player", 1)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "CITY_APPEARS")
            .add("time", START_TIME)
            .add("city", cityC.getId())
            .add("player", 1)
            .build())
        ),
        visibilityManagerB.getEventsGenerated());
    assertEquals(Arrays.asList(
        TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "ROAD_SECTION_HALF_DISCOVERED")
            .add("time", START_TIME)
            .add("roadSectionHalf", Json.createObjectBuilder()
                .add("roadSection", 0)
                .add("end", "BACKWARD")
                .build())
            .add("player", 2)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "ROAD_SECTION_HALF_DISCOVERED")
            .add("time", START_TIME)
            .add("roadSectionHalf", Json.createObjectBuilder()
                .add("roadSection", 0)
                .add("end", "FORWARD")
                .build())
            .add("player", 2)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "PRAWN_APPEARS")
            .add("time", START_TIME)
            .add("prawn", warriorB.getId())
            .add("player", 2)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "PRAWN_APPEARS")
            .add("time", START_TIME)
            .add("prawn", settlersUnitB.getId())
            .add("player", 2)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "CITY_APPEARS")
            .add("time", START_TIME)
            .add("city", cityB.getId())
            .add("player", 2)
            .build())
        ),
        visibilityManagerC.getEventsGenerated());

    assertEquals(
        new HashSet<>(Arrays.asList(roadSection1.getHalves()[0], roadSection1.getHalves()[1],
            roadSection2.getHalves()[0], roadSection2.getHalves()[1])),
        game.getPlayerVisibleRoadSectionHalves(playerA));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection1.getHalves()[0], roadSection1.getHalves()[1])),
        game.getPlayerVisibleRoadSectionHalves(playerB));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1])),
        game.getPlayerVisibleRoadSectionHalves(playerC));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection1.getHalves()[0], roadSection1.getHalves()[1],
            roadSection2.getHalves()[0], roadSection2.getHalves()[1])),
        game.getPlayerDiscoveredRoadSectionHalves(playerA));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection1.getHalves()[0], roadSection1.getHalves()[1])),
        game.getPlayerDiscoveredRoadSectionHalves(playerB));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1])),
        game.getPlayerDiscoveredRoadSectionHalves(playerC));

    assertEquals(new HashSet<>(Arrays.asList(warriorA, settlersUnitA, settlersUnitB)),
        game.getPlayerVisiblePrawns(playerA));
    assertEquals(new HashSet<>(Arrays.asList(warriorB, settlersUnitB, warriorA, warriorC)),
        game.getPlayerVisiblePrawns(playerB));
    assertEquals(new HashSet<>(Arrays.asList(warriorC, warriorB, settlersUnitB)),
        game.getPlayerVisiblePrawns(playerC));

    assertEquals(new HashSet<>(Arrays.asList(cityA, cityA1, cityB)), game.getPlayerDiscoveredCities(playerA));
    assertEquals(new HashSet<>(Arrays.asList(cityB, cityA, cityC)), game.getPlayerDiscoveredCities(playerB));
    assertEquals(new HashSet<>(Arrays.asList(cityC, cityB)), game.getPlayerDiscoveredCities(playerC));
    assertEquals(new HashSet<>(Arrays.asList(cityA, cityA1, cityB)), game.getPlayerVisibleCities(playerA));
    assertEquals(new HashSet<>(Arrays.asList(cityB, cityA, cityC)), game.getPlayerVisibleCities(playerB));
    assertEquals(new HashSet<>(Arrays.asList(cityC, cityB)), game.getPlayerVisibleCities(playerC));
  }

  @Test
  public void testMoreCitiesDiscoveredThanVisible() throws Exception {
    game.putPlayerVisibleRoadSectionHalves(playerA,
        new HashSet<>(Arrays.asList(roadSection1.getHalves()[0], roadSection1.getHalves()[1],
            roadSection2.getHalves()[0], roadSection2.getHalves()[1])));
    game.putPlayerVisibleRoadSectionHalves(playerB,
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection1.getHalves()[0], roadSection1.getHalves()[1])));
    game.putPlayerVisibleRoadSectionHalves(playerC,
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1])));
    game.addPlayerDiscoveredRoadSectionHalves(playerA,
        new HashSet<>(Arrays.asList(roadSection1.getHalves()[0], roadSection1.getHalves()[1],
            roadSection2.getHalves()[0], roadSection2.getHalves()[1])));
    game.addPlayerDiscoveredRoadSectionHalves(playerB,
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection1.getHalves()[0], roadSection1.getHalves()[1])));
    game.addPlayerDiscoveredRoadSectionHalves(playerC,
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1])));

    game.putPlayerVisiblePrawns(playerA, new HashSet<>(Arrays.asList(warriorA, settlersUnitA, settlersUnitB)));
    game.putPlayerVisiblePrawns(playerB, new HashSet<>(Arrays.asList(warriorB, settlersUnitB, warriorA, warriorC)));
    game.putPlayerVisiblePrawns(playerC, new HashSet<>(Arrays.asList(warriorC, warriorB, settlersUnitB)));

    game.putPlayerDiscoveredCities(playerA, new HashSet<>(Arrays.asList(cityA, cityA1, cityB)));
    game.putPlayerDiscoveredCities(playerB, new HashSet<>(Arrays.asList(cityB, cityA, cityC)));
    game.putPlayerDiscoveredCities(playerC, new HashSet<>(Arrays.asList(cityC, cityA1, cityB)));
    game.putPlayerVisibleCities(playerA, new HashSet<>(Arrays.asList(cityA, cityA1, cityB)));
    game.putPlayerVisibleCities(playerB, new HashSet<>(Arrays.asList(cityB, cityA, cityC)));
    game.putPlayerVisibleCities(playerC, new HashSet<>(Arrays.asList(cityC, cityB)));

    visibilityManagerA.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    visibilityManagerB.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    visibilityManagerC.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));

    visibilityManagerA.executeAndGenerate();
    visibilityManagerB.executeAndGenerate();
    visibilityManagerC.executeAndGenerate();

    assertEquals(EventGeneratingExecutor.State.GENERATED, visibilityManagerA.getState());
    assertEquals(EventGeneratingExecutor.State.GENERATED, visibilityManagerB.getState());
    assertEquals(EventGeneratingExecutor.State.GENERATED, visibilityManagerC.getState());

    assertEquals(Arrays.asList(TEST_EVENT), visibilityManagerA.getEventsGenerated());
    assertEquals(Arrays.asList(TEST_EVENT), visibilityManagerB.getEventsGenerated());
    assertEquals(Arrays.asList(TEST_EVENT), visibilityManagerC.getEventsGenerated());

    assertEquals(
        new HashSet<>(Arrays.asList(roadSection1.getHalves()[0], roadSection1.getHalves()[1],
            roadSection2.getHalves()[0], roadSection2.getHalves()[1])),
        game.getPlayerVisibleRoadSectionHalves(playerA));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection1.getHalves()[0], roadSection1.getHalves()[1])),
        game.getPlayerVisibleRoadSectionHalves(playerB));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1])),
        game.getPlayerVisibleRoadSectionHalves(playerC));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection1.getHalves()[0], roadSection1.getHalves()[1],
            roadSection2.getHalves()[0], roadSection2.getHalves()[1])),
        game.getPlayerDiscoveredRoadSectionHalves(playerA));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection1.getHalves()[0], roadSection1.getHalves()[1])),
        game.getPlayerDiscoveredRoadSectionHalves(playerB));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1])),
        game.getPlayerDiscoveredRoadSectionHalves(playerC));

    assertEquals(new HashSet<>(Arrays.asList(warriorA, settlersUnitA, settlersUnitB)),
        game.getPlayerVisiblePrawns(playerA));
    assertEquals(new HashSet<>(Arrays.asList(warriorB, settlersUnitB, warriorA, warriorC)),
        game.getPlayerVisiblePrawns(playerB));
    assertEquals(new HashSet<>(Arrays.asList(warriorC, warriorB, settlersUnitB)),
        game.getPlayerVisiblePrawns(playerC));

    assertEquals(new HashSet<>(Arrays.asList(cityA, cityA1, cityB)), game.getPlayerDiscoveredCities(playerA));
    assertEquals(new HashSet<>(Arrays.asList(cityB, cityA, cityC)), game.getPlayerDiscoveredCities(playerB));
    assertEquals(new HashSet<>(Arrays.asList(cityC, cityA1, cityB)), game.getPlayerDiscoveredCities(playerC));
    assertEquals(new HashSet<>(Arrays.asList(cityA, cityA1, cityB)), game.getPlayerVisibleCities(playerA));
    assertEquals(new HashSet<>(Arrays.asList(cityB, cityA, cityC)), game.getPlayerVisibleCities(playerB));
    assertEquals(new HashSet<>(Arrays.asList(cityC, cityB)), game.getPlayerVisibleCities(playerC));
  }

  @Test
  public void testDiscoveredNotVisibleCityNotShownDestroyed() throws Exception {
    game.putPlayerVisibleRoadSectionHalves(playerA,
        new HashSet<>(Arrays.asList(roadSection1.getHalves()[0], roadSection1.getHalves()[1],
            roadSection2.getHalves()[0], roadSection2.getHalves()[1])));
    game.putPlayerVisibleRoadSectionHalves(playerB,
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection1.getHalves()[0], roadSection1.getHalves()[1])));
    game.putPlayerVisibleRoadSectionHalves(playerC,
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1])));
    game.addPlayerDiscoveredRoadSectionHalves(playerA,
        new HashSet<>(Arrays.asList(roadSection1.getHalves()[0], roadSection1.getHalves()[1],
            roadSection2.getHalves()[0], roadSection2.getHalves()[1])));
    game.addPlayerDiscoveredRoadSectionHalves(playerB,
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection1.getHalves()[0], roadSection1.getHalves()[1])));
    game.addPlayerDiscoveredRoadSectionHalves(playerC,
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1])));

    game.putPlayerVisiblePrawns(playerA, new HashSet<>(Arrays.asList(warriorA, settlersUnitA, settlersUnitB)));
    game.putPlayerVisiblePrawns(playerB, new HashSet<>(Arrays.asList(warriorB, settlersUnitB, warriorA, warriorC)));
    game.putPlayerVisiblePrawns(playerC, new HashSet<>(Arrays.asList(warriorC, warriorB, settlersUnitB)));

    game.putPlayerDiscoveredCities(playerA, new HashSet<>(Arrays.asList(cityA, cityA1, cityB)));
    game.putPlayerDiscoveredCities(playerB, new HashSet<>(Arrays.asList(cityB, cityA, cityC)));
    game.putPlayerDiscoveredCities(playerC, new HashSet<>(Arrays.asList(cityC, cityA1, cityB)));
    game.putPlayerVisibleCities(playerA, new HashSet<>(Arrays.asList(cityA, cityA1, cityB)));
    game.putPlayerVisibleCities(playerB, new HashSet<>(Arrays.asList(cityB, cityA, cityC)));
    game.putPlayerVisibleCities(playerC, new HashSet<>(Arrays.asList(cityC, cityB)));

    game.destroyCity(cityA1);

    visibilityManagerA.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    visibilityManagerB.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    visibilityManagerC.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));

    visibilityManagerA.executeAndGenerate();
    visibilityManagerB.executeAndGenerate();
    visibilityManagerC.executeAndGenerate();

    assertEquals(EventGeneratingExecutor.State.GENERATED, visibilityManagerA.getState());
    assertEquals(EventGeneratingExecutor.State.GENERATED, visibilityManagerB.getState());
    assertEquals(EventGeneratingExecutor.State.GENERATED, visibilityManagerC.getState());

    assertEquals(Arrays.asList(TEST_EVENT), visibilityManagerA.getEventsGenerated());
    assertEquals(Arrays.asList(TEST_EVENT), visibilityManagerB.getEventsGenerated());
    assertEquals(Arrays.asList(TEST_EVENT), visibilityManagerC.getEventsGenerated());

    assertEquals(
        new HashSet<>(Arrays.asList(roadSection1.getHalves()[0], roadSection1.getHalves()[1],
            roadSection2.getHalves()[0], roadSection2.getHalves()[1])),
        game.getPlayerVisibleRoadSectionHalves(playerA));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection1.getHalves()[0], roadSection1.getHalves()[1])),
        game.getPlayerVisibleRoadSectionHalves(playerB));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1])),
        game.getPlayerVisibleRoadSectionHalves(playerC));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection1.getHalves()[0], roadSection1.getHalves()[1],
            roadSection2.getHalves()[0], roadSection2.getHalves()[1])),
        game.getPlayerDiscoveredRoadSectionHalves(playerA));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection1.getHalves()[0], roadSection1.getHalves()[1])),
        game.getPlayerDiscoveredRoadSectionHalves(playerB));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1])),
        game.getPlayerDiscoveredRoadSectionHalves(playerC));

    assertEquals(new HashSet<>(Arrays.asList(warriorA, settlersUnitA, settlersUnitB)),
        game.getPlayerVisiblePrawns(playerA));
    assertEquals(new HashSet<>(Arrays.asList(warriorB, settlersUnitB, warriorA, warriorC)),
        game.getPlayerVisiblePrawns(playerB));
    assertEquals(new HashSet<>(Arrays.asList(warriorC, warriorB, settlersUnitB)),
        game.getPlayerVisiblePrawns(playerC));

    assertEquals(new HashSet<>(Arrays.asList(cityA, cityB)), game.getPlayerDiscoveredCities(playerA));
    assertEquals(new HashSet<>(Arrays.asList(cityB, cityA, cityC)), game.getPlayerDiscoveredCities(playerB));
    assertEquals(new HashSet<>(Arrays.asList(cityC, cityA1, cityB)), game.getPlayerDiscoveredCities(playerC));
    assertEquals(new HashSet<>(Arrays.asList(cityA, cityB)), game.getPlayerVisibleCities(playerA));
    assertEquals(new HashSet<>(Arrays.asList(cityB, cityA, cityC)), game.getPlayerVisibleCities(playerB));
    assertEquals(new HashSet<>(Arrays.asList(cityC, cityB)), game.getPlayerVisibleCities(playerC));
  }

  @Test
  public void testSomePrawnsAndCitiesDiscoveredAndVisibleChangesShown() throws Exception {
    game.putPlayerVisibleRoadSectionHalves(playerA,
        new HashSet<>(Arrays.asList(roadSection1.getHalves()[0], roadSection1.getHalves()[1],
            roadSection2.getHalves()[0], roadSection2.getHalves()[1])));
    game.putPlayerVisibleRoadSectionHalves(playerB,
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection1.getHalves()[0], roadSection1.getHalves()[1])));
    game.putPlayerVisibleRoadSectionHalves(playerC,
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1])));
    game.addPlayerDiscoveredRoadSectionHalves(playerA,
        new HashSet<>(Arrays.asList(roadSection1.getHalves()[0], roadSection1.getHalves()[1],
            roadSection2.getHalves()[0], roadSection2.getHalves()[1])));
    game.addPlayerDiscoveredRoadSectionHalves(playerB,
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection1.getHalves()[0], roadSection1.getHalves()[1])));
    game.addPlayerDiscoveredRoadSectionHalves(playerC,
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1])));

    game.putPlayerVisiblePrawns(playerA, new HashSet<>(Arrays.asList(warriorA, settlersUnitA, settlersUnitB)));
    game.putPlayerVisiblePrawns(playerB, new HashSet<>(Arrays.asList(warriorB, settlersUnitB, warriorA, warriorC)));
    game.putPlayerVisiblePrawns(playerC, new HashSet<>(Arrays.asList(warriorC, warriorB, settlersUnitB)));

    game.putPlayerDiscoveredCities(playerA, new HashSet<>(Arrays.asList(cityA, cityA1, cityB)));
    game.putPlayerDiscoveredCities(playerB, new HashSet<>(Arrays.asList(cityB, cityA, cityC)));
    game.putPlayerDiscoveredCities(playerC, new HashSet<>(Arrays.asList(cityC, cityA1, cityB)));
    game.putPlayerVisibleCities(playerA, new HashSet<>(Arrays.asList(cityA, cityA1, cityB)));
    game.putPlayerVisibleCities(playerB, new HashSet<>(Arrays.asList(cityB, cityA, cityC)));
    game.putPlayerVisibleCities(playerC, new HashSet<>(Arrays.asList(cityC, cityB)));

    game.destroyCity(cityA1);
    warriorC.setCurrentPosition(new Position(roadSection2, 2));

    visibilityManagerA.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    visibilityManagerB.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    visibilityManagerC.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));

    visibilityManagerA.executeAndGenerate();
    visibilityManagerB.executeAndGenerate();
    visibilityManagerC.executeAndGenerate();

    assertEquals(EventGeneratingExecutor.State.GENERATED, visibilityManagerA.getState());
    assertEquals(EventGeneratingExecutor.State.GENERATED, visibilityManagerB.getState());
    assertEquals(EventGeneratingExecutor.State.GENERATED, visibilityManagerC.getState());

    assertEquals(Arrays.asList(
        TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "PRAWN_APPEARS")
            .add("time", START_TIME)
            .add("prawn", warriorC.getId())
            .add("player", 0)
            .build())
        ), visibilityManagerA.getEventsGenerated());
    assertEquals(Arrays.asList(TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "PRAWN_DISAPPEARS")
            .add("time", START_TIME)
            .add("prawn", warriorC.getId())
            .add("player", 1)
            .build())
        ), visibilityManagerB.getEventsGenerated());
    assertEquals(Arrays.asList(TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "ROAD_SECTION_HALF_DISCOVERED")
            .add("time", START_TIME)
            .add("roadSectionHalf", Json.createObjectBuilder()
                .add("roadSection", 1)
                .add("end", "FORWARD")
                .build())
            .add("player", 2)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "ROAD_SECTION_HALF_DISCOVERED")
            .add("time", START_TIME)
            .add("roadSectionHalf", Json.createObjectBuilder()
                .add("roadSection", 2)
                .add("end", "BACKWARD")
                .build())
            .add("player", 2)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "ROAD_SECTION_HALF_DISCOVERED")
            .add("time", START_TIME)
            .add("roadSectionHalf", Json.createObjectBuilder()
                .add("roadSection", 2)
                .add("end", "FORWARD")
                .build())
            .add("player", 2)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "PRAWN_APPEARS")
            .add("time", START_TIME)
            .add("prawn", warriorA.getId())
            .add("player", 2)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "PRAWN_APPEARS")
            .add("time", START_TIME)
            .add("prawn", settlersUnitA.getId())
            .add("player", 2)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "CITY_APPEARS")
            .add("time", START_TIME)
            .add("city", cityA.getId())
            .add("player", 2)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "CITY_SHOWN_DESTROYED")
            .add("time", START_TIME)
            .add("city", cityA1.getId())
            .add("player", 2)
            .build())
        ), visibilityManagerC.getEventsGenerated());

    assertEquals(
        new HashSet<>(Arrays.asList(roadSection1.getHalves()[0], roadSection1.getHalves()[1],
            roadSection2.getHalves()[0], roadSection2.getHalves()[1])),
        game.getPlayerVisibleRoadSectionHalves(playerA));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection1.getHalves()[0], roadSection1.getHalves()[1])),
        game.getPlayerVisibleRoadSectionHalves(playerB));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection1.getHalves()[1], roadSection2.getHalves()[0], roadSection2.getHalves()[1])),
        game.getPlayerVisibleRoadSectionHalves(playerC));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection1.getHalves()[0], roadSection1.getHalves()[1],
            roadSection2.getHalves()[0], roadSection2.getHalves()[1])),
        game.getPlayerDiscoveredRoadSectionHalves(playerA));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection1.getHalves()[0], roadSection1.getHalves()[1])),
        game.getPlayerDiscoveredRoadSectionHalves(playerB));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection1.getHalves()[1], roadSection2.getHalves()[0], roadSection2.getHalves()[1])),
        game.getPlayerDiscoveredRoadSectionHalves(playerC));

    assertEquals(new HashSet<>(Arrays.asList(warriorA, settlersUnitA, settlersUnitB, warriorC)),
        game.getPlayerVisiblePrawns(playerA));
    assertEquals(new HashSet<>(Arrays.asList(warriorB, settlersUnitB, warriorA)),
        game.getPlayerVisiblePrawns(playerB));
    assertEquals(new HashSet<>(Arrays.asList(warriorC, warriorA, settlersUnitA, warriorB, settlersUnitB)),
        game.getPlayerVisiblePrawns(playerC));

    assertEquals(new HashSet<>(Arrays.asList(cityA, cityB)), game.getPlayerDiscoveredCities(playerA));
    assertEquals(new HashSet<>(Arrays.asList(cityB, cityA, cityC)), game.getPlayerDiscoveredCities(playerB));
    assertEquals(new HashSet<>(Arrays.asList(cityC, cityA, cityB)), game.getPlayerDiscoveredCities(playerC));
    assertEquals(new HashSet<>(Arrays.asList(cityA, cityB)), game.getPlayerVisibleCities(playerA));
    assertEquals(new HashSet<>(Arrays.asList(cityB, cityA, cityC)), game.getPlayerVisibleCities(playerB));
    assertEquals(new HashSet<>(Arrays.asList(cityC, cityA, cityB)), game.getPlayerVisibleCities(playerC));
  }

  @Test
  public void testSomeRoadSectionHalvesReappear() throws Exception {
    game.putPlayerVisibleRoadSectionHalves(playerA,
        new HashSet<>(Arrays.asList(roadSection1.getHalves()[0], roadSection1.getHalves()[1],
            roadSection2.getHalves()[0], roadSection2.getHalves()[1])));
    game.putPlayerVisibleRoadSectionHalves(playerB,
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection1.getHalves()[0], roadSection1.getHalves()[1])));
    game.putPlayerVisibleRoadSectionHalves(playerC,
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1])));
    game.addPlayerDiscoveredRoadSectionHalves(playerA,
        new HashSet<>(Arrays.asList(roadSection1.getHalves()[0], roadSection1.getHalves()[1],
            roadSection2.getHalves()[0], roadSection2.getHalves()[1])));
    game.addPlayerDiscoveredRoadSectionHalves(playerB,
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection1.getHalves()[0], roadSection1.getHalves()[1])));
    game.addPlayerDiscoveredRoadSectionHalves(playerC,
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection2.getHalves()[0], roadSection2.getHalves()[1])));

    game.putPlayerVisiblePrawns(playerA, new HashSet<>(Arrays.asList(warriorA, settlersUnitA, settlersUnitB)));
    game.putPlayerVisiblePrawns(playerB, new HashSet<>(Arrays.asList(warriorB, settlersUnitB, warriorA, warriorC)));
    game.putPlayerVisiblePrawns(playerC, new HashSet<>(Arrays.asList(warriorC, warriorB, settlersUnitB)));

    game.putPlayerDiscoveredCities(playerA, new HashSet<>(Arrays.asList(cityA, cityA1, cityB)));
    game.putPlayerDiscoveredCities(playerB, new HashSet<>(Arrays.asList(cityB, cityA, cityC)));
    game.putPlayerDiscoveredCities(playerC, new HashSet<>(Arrays.asList(cityC, cityA, cityA1, cityB)));
    game.putPlayerVisibleCities(playerA, new HashSet<>(Arrays.asList(cityA, cityA1, cityB)));
    game.putPlayerVisibleCities(playerB, new HashSet<>(Arrays.asList(cityB, cityA, cityC)));
    game.putPlayerVisibleCities(playerC, new HashSet<>(Arrays.asList(cityC, cityB)));

    warriorC.setCurrentPosition(new Position(roadSection2, 2));

    visibilityManagerA.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    visibilityManagerB.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    visibilityManagerC.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));

    visibilityManagerA.executeAndGenerate();
    visibilityManagerB.executeAndGenerate();
    visibilityManagerC.executeAndGenerate();

    assertEquals(EventGeneratingExecutor.State.GENERATED, visibilityManagerA.getState());
    assertEquals(EventGeneratingExecutor.State.GENERATED, visibilityManagerB.getState());
    assertEquals(EventGeneratingExecutor.State.GENERATED, visibilityManagerC.getState());

    assertEquals(Arrays.asList(
        TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "PRAWN_APPEARS")
            .add("time", START_TIME)
            .add("prawn", warriorC.getId())
            .add("player", 0)
            .build())
        ), visibilityManagerA.getEventsGenerated());
    assertEquals(Arrays.asList(TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "PRAWN_DISAPPEARS")
            .add("time", START_TIME)
            .add("prawn", warriorC.getId())
            .add("player", 1)
            .build())
        ), visibilityManagerB.getEventsGenerated());
    assertEquals(Arrays.asList(TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "ROAD_SECTION_HALF_DISCOVERED")
            .add("time", START_TIME)
            .add("roadSectionHalf", Json.createObjectBuilder()
                .add("roadSection", 1)
                .add("end", "FORWARD")
                .build())
            .add("player", 2)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "ROAD_SECTION_HALF_APPEARS")
            .add("time", START_TIME)
            .add("roadSectionHalf", Json.createObjectBuilder()
                .add("roadSection", 2)
                .add("end", "BACKWARD")
                .build())
            .add("player", 2)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "ROAD_SECTION_HALF_APPEARS")
            .add("time", START_TIME)
            .add("roadSectionHalf", Json.createObjectBuilder()
                .add("roadSection", 2)
                .add("end", "FORWARD")
                .build())
            .add("player", 2)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "PRAWN_APPEARS")
            .add("time", START_TIME)
            .add("prawn", warriorA.getId())
            .add("player", 2)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "PRAWN_APPEARS")
            .add("time", START_TIME)
            .add("prawn", settlersUnitA.getId())
            .add("player", 2)
            .build())
        ), visibilityManagerC.getEventsGenerated());

    assertEquals(
        new HashSet<>(Arrays.asList(roadSection1.getHalves()[0], roadSection1.getHalves()[1],
            roadSection2.getHalves()[0], roadSection2.getHalves()[1])),
        game.getPlayerVisibleRoadSectionHalves(playerA));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection1.getHalves()[0], roadSection1.getHalves()[1])),
        game.getPlayerVisibleRoadSectionHalves(playerB));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection1.getHalves()[1], roadSection2.getHalves()[0], roadSection2.getHalves()[1])),
        game.getPlayerVisibleRoadSectionHalves(playerC));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection1.getHalves()[0], roadSection1.getHalves()[1],
            roadSection2.getHalves()[0], roadSection2.getHalves()[1])),
        game.getPlayerDiscoveredRoadSectionHalves(playerA));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection1.getHalves()[0], roadSection1.getHalves()[1])),
        game.getPlayerDiscoveredRoadSectionHalves(playerB));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection1.getHalves()[1], roadSection2.getHalves()[0], roadSection2.getHalves()[1])),
        game.getPlayerDiscoveredRoadSectionHalves(playerC));
  }

  @Test
  public void testVisibilityShrinks() throws Exception {
    game.putPlayerVisibleRoadSectionHalves(playerA,
        new HashSet<>(Arrays.asList(roadSection1.getHalves()[0], roadSection1.getHalves()[1],
            roadSection2.getHalves()[0], roadSection2.getHalves()[1])));
    game.putPlayerVisibleRoadSectionHalves(playerB,
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection1.getHalves()[0], roadSection1.getHalves()[1])));
    game.putPlayerVisibleRoadSectionHalves(playerC,
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1])));
    game.addPlayerDiscoveredRoadSectionHalves(playerA,
        new HashSet<>(Arrays.asList(roadSection1.getHalves()[0], roadSection1.getHalves()[1],
            roadSection2.getHalves()[0], roadSection2.getHalves()[1])));
    game.addPlayerDiscoveredRoadSectionHalves(playerB,
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection1.getHalves()[0], roadSection1.getHalves()[1])));
    game.addPlayerDiscoveredRoadSectionHalves(playerC,
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1])));

    game.putPlayerVisiblePrawns(playerA, new HashSet<>(Arrays.asList(warriorA, settlersUnitA, settlersUnitB)));
    game.putPlayerVisiblePrawns(playerB, new HashSet<>(Arrays.asList(warriorB, settlersUnitB, warriorA, warriorC)));
    game.putPlayerVisiblePrawns(playerC, new HashSet<>(Arrays.asList(warriorC, warriorB, settlersUnitB)));

    game.putPlayerDiscoveredCities(playerA, new HashSet<>(Arrays.asList(cityA, cityA1, cityB)));
    game.putPlayerDiscoveredCities(playerB, new HashSet<>(Arrays.asList(cityB, cityA, cityC)));
    game.putPlayerDiscoveredCities(playerC, new HashSet<>(Arrays.asList(cityC, cityA1, cityB)));
    game.putPlayerVisibleCities(playerA, new HashSet<>(Arrays.asList(cityA, cityA1, cityB)));
    game.putPlayerVisibleCities(playerB, new HashSet<>(Arrays.asList(cityB, cityA, cityC)));
    game.putPlayerVisibleCities(playerC, new HashSet<>(Arrays.asList(cityC, cityB)));

    game.destroyCity(cityA1);
    game.destroyCity(cityC);
    warriorC.setCurrentPosition(new Position(roadSection2, 2));

    visibilityManagerA.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    visibilityManagerB.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    visibilityManagerC.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));

    visibilityManagerA.executeAndGenerate();
    visibilityManagerB.executeAndGenerate();
    visibilityManagerC.executeAndGenerate();

    assertEquals(EventGeneratingExecutor.State.GENERATED, visibilityManagerA.getState());
    assertEquals(EventGeneratingExecutor.State.GENERATED, visibilityManagerB.getState());
    assertEquals(EventGeneratingExecutor.State.GENERATED, visibilityManagerC.getState());

    assertEquals(Arrays.asList(
        TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "PRAWN_APPEARS")
            .add("time", START_TIME)
            .add("prawn", warriorC.getId())
            .add("player", 0)
            .build())
        ), visibilityManagerA.getEventsGenerated());
    assertEquals(Arrays.asList(TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "PRAWN_DISAPPEARS")
            .add("time", START_TIME)
            .add("prawn", warriorC.getId())
            .add("player", 1)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "CITY_SHOWN_DESTROYED")
            .add("time", START_TIME)
            .add("city", cityC.getId())
            .add("player", 1)
            .build())
        ), visibilityManagerB.getEventsGenerated());
    assertEquals(Arrays.asList(TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "ROAD_SECTION_HALF_DISCOVERED")
            .add("time", START_TIME)
            .add("roadSectionHalf", Json.createObjectBuilder()
                .add("roadSection", 1)
                .add("end", "FORWARD")
                .build())
            .add("player", 2)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "ROAD_SECTION_HALF_DISCOVERED")
            .add("time", START_TIME)
            .add("roadSectionHalf", Json.createObjectBuilder()
                .add("roadSection", 2)
                .add("end", "BACKWARD")
                .build())
            .add("player", 2)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "ROAD_SECTION_HALF_DISCOVERED")
            .add("time", START_TIME)
            .add("roadSectionHalf", Json.createObjectBuilder()
                .add("roadSection", 2)
                .add("end", "FORWARD")
                .build())
            .add("player", 2)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "ROAD_SECTION_HALF_DISAPPEARS")
            .add("time", START_TIME)
            .add("roadSectionHalf", Json.createObjectBuilder()
                .add("roadSection", 0)
                .add("end", "BACKWARD")
                .build())
            .add("player", 2)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "ROAD_SECTION_HALF_DISAPPEARS")
            .add("time", START_TIME)
            .add("roadSectionHalf", Json.createObjectBuilder()
                .add("roadSection", 0)
                .add("end", "FORWARD")
                .build())
            .add("player", 2)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "PRAWN_APPEARS")
            .add("time", START_TIME)
            .add("prawn", warriorA.getId())
            .add("player", 2)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "PRAWN_APPEARS")
            .add("time", START_TIME)
            .add("prawn", settlersUnitA.getId())
            .add("player", 2)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "PRAWN_DISAPPEARS")
            .add("time", START_TIME)
            .add("prawn", warriorB.getId())
            .add("player", 2)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "PRAWN_DISAPPEARS")
            .add("time", START_TIME)
            .add("prawn", settlersUnitB.getId())
            .add("player", 2)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "CITY_APPEARS")
            .add("time", START_TIME)
            .add("city", cityA.getId())
            .add("player", 2)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "CITY_SHOWN_DESTROYED")
            .add("time", START_TIME)
            .add("city", cityA1.getId())
            .add("player", 2)
            .build())
        ), visibilityManagerC.getEventsGenerated());

    assertEquals(
        new HashSet<>(Arrays.asList(roadSection1.getHalves()[0], roadSection1.getHalves()[1],
            roadSection2.getHalves()[0], roadSection2.getHalves()[1])),
        game.getPlayerVisibleRoadSectionHalves(playerA));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection1.getHalves()[0], roadSection1.getHalves()[1])),
        game.getPlayerVisibleRoadSectionHalves(playerB));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection1.getHalves()[1], roadSection2.getHalves()[0],
            roadSection2.getHalves()[1])), game.getPlayerVisibleRoadSectionHalves(playerC));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection1.getHalves()[0], roadSection1.getHalves()[1],
            roadSection2.getHalves()[0], roadSection2.getHalves()[1])),
        game.getPlayerDiscoveredRoadSectionHalves(playerA));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection1.getHalves()[0], roadSection1.getHalves()[1])),
        game.getPlayerDiscoveredRoadSectionHalves(playerB));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection1.getHalves()[1], roadSection2.getHalves()[0], roadSection2.getHalves()[1])),
        game.getPlayerDiscoveredRoadSectionHalves(playerC));

    assertEquals(new HashSet<>(Arrays.asList(warriorA, settlersUnitA, settlersUnitB, warriorC)),
        game.getPlayerVisiblePrawns(playerA));
    assertEquals(new HashSet<>(Arrays.asList(warriorB, settlersUnitB, warriorA)), game.getPlayerVisiblePrawns(playerB));
    assertEquals(new HashSet<>(Arrays.asList(warriorC, warriorA, settlersUnitA)), game.getPlayerVisiblePrawns(playerC));

    assertEquals(new HashSet<>(Arrays.asList(cityA, cityB)), game.getPlayerDiscoveredCities(playerA));
    assertEquals(new HashSet<>(Arrays.asList(cityB, cityA)), game.getPlayerDiscoveredCities(playerB));
    assertEquals(new HashSet<>(Arrays.asList(cityA, cityB)), game.getPlayerDiscoveredCities(playerC));
    assertEquals(new HashSet<>(Arrays.asList(cityA, cityB)), game.getPlayerVisibleCities(playerA));
    assertEquals(new HashSet<>(Arrays.asList(cityB, cityA)), game.getPlayerVisibleCities(playerB));
    assertEquals(new HashSet<>(Arrays.asList(cityA)), game.getPlayerVisibleCities(playerC));
  }

  @Test
  public void testPrawnsRemoved() throws Exception {
    game.putPlayerVisibleRoadSectionHalves(playerA,
        new HashSet<>(Arrays.asList(roadSection1.getHalves()[0], roadSection1.getHalves()[1],
            roadSection2.getHalves()[0], roadSection2.getHalves()[1])));
    game.putPlayerVisibleRoadSectionHalves(playerB,
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection1.getHalves()[0], roadSection1.getHalves()[1])));
    game.putPlayerVisibleRoadSectionHalves(playerC,
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1])));
    game.addPlayerDiscoveredRoadSectionHalves(playerA,
        new HashSet<>(Arrays.asList(roadSection1.getHalves()[0], roadSection1.getHalves()[1],
            roadSection2.getHalves()[0], roadSection2.getHalves()[1])));
    game.addPlayerDiscoveredRoadSectionHalves(playerB,
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection1.getHalves()[0], roadSection1.getHalves()[1])));
    game.addPlayerDiscoveredRoadSectionHalves(playerC,
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1])));

    game.putPlayerVisiblePrawns(playerA, new HashSet<>(Arrays.asList(warriorA, settlersUnitA, settlersUnitB)));
    game.putPlayerVisiblePrawns(playerB, new HashSet<>(Arrays.asList(warriorB, settlersUnitB, warriorA, warriorC)));
    game.putPlayerVisiblePrawns(playerC, new HashSet<>(Arrays.asList(warriorC, warriorB, settlersUnitB)));

    game.putPlayerDiscoveredCities(playerA, new HashSet<>(Arrays.asList(cityA, cityA1, cityB)));
    game.putPlayerDiscoveredCities(playerB, new HashSet<>(Arrays.asList(cityB, cityA, cityC)));
    game.putPlayerDiscoveredCities(playerC, new HashSet<>(Arrays.asList(cityC, cityB)));
    game.putPlayerVisibleCities(playerA, new HashSet<>(Arrays.asList(cityA, cityA1, cityB)));
    game.putPlayerVisibleCities(playerB, new HashSet<>(Arrays.asList(cityB, cityA, cityC)));
    game.putPlayerVisibleCities(playerC, new HashSet<>(Arrays.asList(cityC, cityB)));

    game.removePrawn(warriorC);

    visibilityManagerA.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    visibilityManagerB.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));
    visibilityManagerC.prepare(new LinkedList<Event>(Arrays.asList(TEST_EVENT)));

    visibilityManagerA.executeAndGenerate();
    visibilityManagerB.executeAndGenerate();
    visibilityManagerC.executeAndGenerate();

    assertEquals(EventGeneratingExecutor.State.GENERATED, visibilityManagerA.getState());
    assertEquals(EventGeneratingExecutor.State.GENERATED, visibilityManagerB.getState());
    assertEquals(EventGeneratingExecutor.State.GENERATED, visibilityManagerC.getState());

    assertEquals(Arrays.asList(TEST_EVENT), visibilityManagerA.getEventsGenerated());
    assertEquals(Arrays.asList(
        TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "PRAWN_DISAPPEARS")
            .add("time", START_TIME)
            .add("prawn", warriorC.getId())
            .add("player", 1)
            .build())
        ), visibilityManagerB.getEventsGenerated());
    assertEquals(Arrays.asList(TEST_EVENT), visibilityManagerC.getEventsGenerated());

    assertEquals(
        new HashSet<>(Arrays.asList(roadSection1.getHalves()[0], roadSection1.getHalves()[1],
            roadSection2.getHalves()[0], roadSection2.getHalves()[1])),
        game.getPlayerVisibleRoadSectionHalves(playerA));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection1.getHalves()[0], roadSection1.getHalves()[1])),
        game.getPlayerVisibleRoadSectionHalves(playerB));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1])),
        game.getPlayerVisibleRoadSectionHalves(playerC));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection1.getHalves()[0], roadSection1.getHalves()[1],
            roadSection2.getHalves()[0], roadSection2.getHalves()[1])),
        game.getPlayerDiscoveredRoadSectionHalves(playerA));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1],
            roadSection1.getHalves()[0], roadSection1.getHalves()[1])),
        game.getPlayerDiscoveredRoadSectionHalves(playerB));
    assertEquals(
        new HashSet<>(Arrays.asList(roadSection0.getHalves()[0], roadSection0.getHalves()[1])),
        game.getPlayerDiscoveredRoadSectionHalves(playerC));

    assertEquals(new HashSet<>(Arrays.asList(warriorA, settlersUnitA, settlersUnitB)),
        game.getPlayerVisiblePrawns(playerA));
    assertEquals(new HashSet<>(Arrays.asList(warriorB, settlersUnitB, warriorA)),
        game.getPlayerVisiblePrawns(playerB));
    assertEquals(new HashSet<>(Arrays.asList(warriorB, settlersUnitB)), game.getPlayerVisiblePrawns(playerC));

    assertEquals(new HashSet<>(Arrays.asList(cityA, cityA1, cityB)), game.getPlayerDiscoveredCities(playerA));
    assertEquals(new HashSet<>(Arrays.asList(cityB, cityA, cityC)), game.getPlayerDiscoveredCities(playerB));
    assertEquals(new HashSet<>(Arrays.asList(cityC, cityB)), game.getPlayerDiscoveredCities(playerC));
    assertEquals(new HashSet<>(Arrays.asList(cityA, cityA1, cityB)), game.getPlayerVisibleCities(playerA));
    assertEquals(new HashSet<>(Arrays.asList(cityB, cityA, cityC)), game.getPlayerVisibleCities(playerB));
    assertEquals(new HashSet<>(Arrays.asList(cityC, cityB)), game.getPlayerVisibleCities(playerC));
  }

}
