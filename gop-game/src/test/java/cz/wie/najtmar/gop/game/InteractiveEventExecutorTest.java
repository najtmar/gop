package cz.wie.najtmar.gop.game;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import cz.wie.najtmar.gop.board.Board;
import cz.wie.najtmar.gop.board.DirectedPosition;
import cz.wie.najtmar.gop.board.Joint;
import cz.wie.najtmar.gop.board.Position;
import cz.wie.najtmar.gop.board.RoadSection;
import cz.wie.najtmar.gop.common.Constants;
import cz.wie.najtmar.gop.common.Point2D;
import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.game.CityFactory.ActionState;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;
import java.util.UUID;

import javax.json.Json;

public class InteractiveEventExecutorTest {

  static final long START_TIME = 966;
  static final double ENERGY_DELTA = 0.1;
  static Board BOARD;
  static Joint JOINT_0_0;
  static Joint JOINT_3_0;
  static Joint JOINT_6_0;
  static Joint JOINT_3_3;
  static RoadSection ROAD_SECTION_0;
  static RoadSection ROAD_SECTION_1;
  static RoadSection ROAD_SECTION_2;
  static Event TEST_EVENT;

  Game game;
  GameProperties gameProperties;
  EventFactory eventFactory;
  Player playerA;
  Player playerB;
  Player playerC;
  Warrior warriorA;
  Warrior warriorB;
  SettlersUnit settlersUnit;
  City cityA;
  City cityB;
  InteractiveEventExecutor executor;

  /**
   * setUpBeforeClass method.
   * @throws Exception when something unexpected happens
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    final Properties simpleBoardPropertiesSet = new Properties();
    simpleBoardPropertiesSet.load(InteractiveEventExecutorTest.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/board/simple-board.properties"));
    BOARD = new Board(simpleBoardPropertiesSet);
    JOINT_0_0 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setName("JOINT_0_0")
        .setCoordinatesPair(new Point2D.Double(0.0, 0.0))
        .build();
    JOINT_3_0 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(1)
        .setName("JOINT_3_0")
        .setCoordinatesPair(new Point2D.Double(3.0, 0.0))
        .build();
    JOINT_6_0 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(2)
        .setName("JOINT_6_0")
        .setCoordinatesPair(new Point2D.Double(6.0, 0.0))
        .build();
    JOINT_3_3 = (new Joint.Builder())
        .setBoard(BOARD)
        .setIndex(3)
        .setName("JOINT_3_3")
        .setCoordinatesPair(new Point2D.Double(3.0, 3.0))
        .build();
    BOARD.addJoint(JOINT_0_0);
    BOARD.addJoint(JOINT_3_0);
    BOARD.addJoint(JOINT_6_0);
    BOARD.addJoint(JOINT_3_3);
    ROAD_SECTION_0 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(0)
        .setJoints(JOINT_0_0, JOINT_3_0)
        .build();
    ROAD_SECTION_1 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(1)
        .setJoints(JOINT_3_0, JOINT_6_0)
        .build();
    ROAD_SECTION_2 = (new RoadSection.Builder())
        .setBoard(BOARD)
        .setIndex(2)
        .setJoints(JOINT_3_3, JOINT_3_0)
        .build();
    BOARD.addRoadSection(ROAD_SECTION_0);
    BOARD.addRoadSection(ROAD_SECTION_1);
    BOARD.addRoadSection(ROAD_SECTION_2);
    TEST_EVENT = new Event(Json.createObjectBuilder()
        .add("type", "TEST")
        .add("time", START_TIME)
        .add("whatever", "value")
        .build());
  }

  /**
   * Sets up the test case.
   * @throws Exception when test case cannot be set up
   */
  @Before
  public void setUp() throws Exception {
    Prawn.resetFirstAvailableId();
    final Properties simpleGamePropertiesSet = new Properties();
    simpleGamePropertiesSet.load(InteractiveEventExecutorTest.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/game/simple-game.properties"));
    simpleGamePropertiesSet.setProperty(GameProperties.START_TIME_PROPERTY_NAME, "" + START_TIME);
    game = new Game(simpleGamePropertiesSet);
    gameProperties = game.getGameProperties();
    playerA = new Player(new User("playerA", new UUID(12345, 67890)), 0, game);
    playerB = new Player(new User("playerB", new UUID(98765, 43210)), 1, game);
    playerC = new Player(new User("playerC", new UUID(12345, 54321)), 2, game);
    game.addPlayer(playerA);
    game.addPlayer(playerB);
    game.addPlayer(playerC);
    warriorA = spy((new Warrior.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(JOINT_0_0.getNormalizedPosition())
        .setDirection(0.0)
        .setGameProperties(gameProperties)
        .setPlayer(playerA)
        .build());
    warriorB = spy((new Warrior.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(JOINT_3_0.getNormalizedPosition())
        .setDirection(Math.PI / 2.0)
        .setGameProperties(gameProperties)
        .setPlayer(playerB)
        .build());
    settlersUnit = spy((new SettlersUnit.Builder())
        .setCreationTime(START_TIME)
        .setCurrentPosition(JOINT_3_3.getNormalizedPosition())
        .setGameProperties(gameProperties)
        .setPlayer(playerC)
        .build());
    City.resetFirstAvailableId();
    cityA = (new City.Builder())
        .setGame(game)
        .setJoint(JOINT_0_0)
        .setName("cityA")
        .setPlayer(playerA)
        .build();
    cityB = (new City.Builder())
        .setGame(game)
        .setJoint(JOINT_3_0)
        .setName("cityB")
        .setPlayer(playerB)
        .build();
    game.addPrawn(warriorA);
    game.addPrawn(warriorB);
    game.addPrawn(settlersUnit);
    game.setBoard(BOARD);
    game.initialize();
    final ObjectSerializer objectSerializer = new ObjectSerializer(game);
    eventFactory = new EventFactory(objectSerializer);
    executor = (new InteractiveEventExecutor.Factory(game, eventFactory)).create();
  }

  @Test
  public void testAbandonPlayerWorks() throws Exception {
    assertEquals(EventGeneratingExecutor.State.NOT_PREPARED, executor.getState());
    game.advanceTimeTo(START_TIME + 10);
    executor.prepare(Arrays.asList(
        TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "ABANDON_PLAYER")
            .add("time", START_TIME + 10)
            .add("player", 1)
            .build())));
    assertEquals(EventGeneratingExecutor.State.PREPARED, executor.getState());

    assertEquals(Player.State.IN_GAME, playerA.getState());
    assertEquals(Player.State.IN_GAME, playerB.getState());
    assertEquals(Player.State.IN_GAME, playerC.getState());
    assertEquals(new HashSet<>(Arrays.asList(cityB)), game.getPlayerCities(playerB));
    assertEquals(new HashSet<>(Arrays.asList(warriorB)), game.getPlayerPrawns(playerB));
    executor.executeAndGenerate();
    assertEquals(EventGeneratingExecutor.State.GENERATED, executor.getState());
    assertEquals(Arrays.asList(
        new Event(Json.createObjectBuilder()
            .add("type", "CHANGE_PLAYER_STATE")
            .add("time", START_TIME + 10)
            .add("player", 1)
            .add("state", "ABANDONED")
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "SHRINK_OR_DESTROY_CITY")
            .add("time", START_TIME + 10)
            .add("city", cityB.getId())
            .add("factoryIndex", 0)
            .build()),
        new Event(Json.createObjectBuilder()
            .add("type", "DESTROY_PRAWN")
            .add("time", START_TIME + 10)
            .add("prawn", warriorB.getId())
            .build())
        ), executor.getEventsGenerated());
    assertEquals(Player.State.IN_GAME, playerA.getState());
    assertEquals(Player.State.ABANDONED, playerB.getState());
    assertEquals(Player.State.IN_GAME, playerC.getState());
    assertEquals(new HashSet<>(), game.getPlayerCities(playerB));
    assertEquals(new HashSet<>(), game.getPlayerPrawns(playerB));
  }

  @Test
  public void testSetAndClearPrawnItineraryWorks() throws Exception {
    assertEquals(EventGeneratingExecutor.State.NOT_PREPARED, executor.getState());
    game.advanceTimeTo(START_TIME + 10);
    executor.prepare(Arrays.asList(
        TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "SET_PRAWN_ITINERARY")
            .add("time", START_TIME + 10)
            .add("prawn", 1)
            .add("nodes", Json.createArrayBuilder()
                .add(Json.createObjectBuilder()
                    .add("roadSection", 0)
                    .add("index", 3)
                    .build())
                .add(Json.createObjectBuilder()
                    .add("roadSection", 1)
                    .add("index", 2)
                    .build())
                .add(Json.createObjectBuilder()
                    .add("roadSection", 0)
                    .add("index", 3)
                    .build())
                .add(Json.createObjectBuilder()
                    .add("roadSection", 2)
                    .add("index", 2)
                    .build())
                .add(Json.createObjectBuilder()
                    .add("roadSection", 2)
                    .add("index", 1)
                    .build())
                .build())
            .build())));
    assertEquals(EventGeneratingExecutor.State.PREPARED, executor.getState());

    assertNull(warriorB.getItinerary());
    assertEquals(START_TIME, warriorB.getLastMoveTime());
    executor.executeAndGenerate();
    assertEquals(EventGeneratingExecutor.State.GENERATED, executor.getState());
    assertEquals(Arrays.asList(), executor.getEventsGenerated());
    final Itinerary itinerary = warriorB.getItinerary();
    assertNotNull(itinerary);
    final Iterator<DirectedPosition> iter = itinerary.iterator();
    assertTrue(iter.hasNext());
    assertEquals(new DirectedPosition(ROAD_SECTION_1, 0, RoadSection.Direction.FORWARD), iter.next());
    assertTrue(iter.hasNext());
    assertEquals(new DirectedPosition(ROAD_SECTION_1, 1, RoadSection.Direction.FORWARD), iter.next());
    assertTrue(iter.hasNext());
    assertEquals(new DirectedPosition(ROAD_SECTION_1, 2, RoadSection.Direction.BACKWARD), iter.next());
    assertTrue(iter.hasNext());
    assertEquals(new DirectedPosition(ROAD_SECTION_1, 1, RoadSection.Direction.BACKWARD), iter.next());
    assertTrue(iter.hasNext());
    assertEquals(new DirectedPosition(ROAD_SECTION_2, 3, RoadSection.Direction.BACKWARD), iter.next());
    assertTrue(iter.hasNext());
    assertEquals(new DirectedPosition(ROAD_SECTION_2, 2, RoadSection.Direction.BACKWARD), iter.next());
    assertTrue(iter.hasNext());
    assertEquals(new DirectedPosition(ROAD_SECTION_2, 1, RoadSection.Direction.BACKWARD), iter.next());
    assertFalse(iter.hasNext());
    assertEquals(START_TIME + 10, warriorB.getLastMoveTime());

    // Clear the Itinerary.
    executor = (new InteractiveEventExecutor.Factory(game, eventFactory)).create();
    game.advanceTimeTo(START_TIME + 20);
    executor.prepare(Arrays.asList(
        TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "CLEAR_PRAWN_ITINERARY")
            .add("time", START_TIME + 15)
            .add("prawn", 1)
            .build())));
    assertNotNull(warriorB.getItinerary());
    executor.executeAndGenerate();
    assertNull(warriorB.getItinerary());
    assertEquals(START_TIME + 15, warriorB.getLastMoveTime());
  }

  @Test
  public void testSetProduceWarriorActionWorks() throws Exception {
    assertEquals(EventGeneratingExecutor.State.NOT_PREPARED, executor.getState());
    game.advanceTimeTo(START_TIME + 10);
    executor.prepare(Arrays.asList(
        TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "SET_PRODUCE_WARRIOR_ACTION")
            .add("time", START_TIME + 10)
            .add("factory", Json.createObjectBuilder()
                .add("city", 1)
                .add("index", 0)
                .build())
            .add("direction", 0.375)
            .build())));
    assertEquals(EventGeneratingExecutor.State.PREPARED, executor.getState());

    assertNull(cityB.getFactory(0).getAction());
    executor.executeAndGenerate();
    assertNotNull(cityB.getFactory(0).getAction());
    assertTrue(cityB.getFactory(0).getAction() instanceof CityFactory.ProduceWarriorAction);
    {
      final CityFactory.ProduceWarriorAction action =
          (CityFactory.ProduceWarriorAction) cityB.getFactory(0).getAction();
      assertEquals(0.375, action.getDirection(), Constants.DOUBLE_DELTA);
      assertEquals(0.0, action.getProgress(), 0.0);
      assertEquals(ActionState.INITIALIZED, action.getState());
      action.increaseProgressByUniversalDelta(0.1, null, null);
      assertNotEquals(0.0, action.getProgress(), 0.0);
    }
    assertEquals(Arrays.asList(), executor.getEventsGenerated());

    // Overwrite the Action.
    executor = (new InteractiveEventExecutor.Factory(game, eventFactory)).create();
    game.advanceTimeTo(START_TIME + 10);
    executor.prepare(Arrays.asList(
        TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "SET_PRODUCE_WARRIOR_ACTION")
            .add("time", START_TIME + 10)
            .add("factory", Json.createObjectBuilder()
                .add("city", 1)
                .add("index", 0)
                .build())
            .add("direction", 0.5)
            .build())));
    assertEquals(EventGeneratingExecutor.State.PREPARED, executor.getState());
    executor.executeAndGenerate();
    assertNotNull(cityB.getFactory(0).getAction());
    assertTrue(cityB.getFactory(0).getAction() instanceof CityFactory.ProduceWarriorAction);
    {
      final CityFactory.ProduceWarriorAction action =
          (CityFactory.ProduceWarriorAction) cityB.getFactory(0).getAction();
      assertEquals(0.5, action.getDirection(), Constants.DOUBLE_DELTA);
      assertEquals(0.0, action.getProgress(), 0.0);
      assertEquals(ActionState.INITIALIZED, action.getState());
    }
    assertEquals(Arrays.asList(), executor.getEventsGenerated());
  }

  @Test
  public void testSetProduceSettlersUnitActionWorks() throws Exception {
    assertEquals(EventGeneratingExecutor.State.NOT_PREPARED, executor.getState());
    game.advanceTimeTo(START_TIME + 10);
    executor.prepare(Arrays.asList(
        TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "SET_PRODUCE_SETTLERS_UNIT_ACTION")
            .add("time", START_TIME + 10)
            .add("factory", Json.createObjectBuilder()
                .add("city", 1)
                .add("index", 0)
                .build())
            .build())));
    assertEquals(EventGeneratingExecutor.State.PREPARED, executor.getState());

    assertNull(cityB.getFactory(0).getAction());
    executor.executeAndGenerate();
    assertNotNull(cityB.getFactory(0).getAction());
    assertTrue(cityB.getFactory(0).getAction() instanceof CityFactory.ProduceSettlersUnitAction);
    {
      final CityFactory.ProduceSettlersUnitAction action =
          (CityFactory.ProduceSettlersUnitAction) cityB.getFactory(0).getAction();
      assertEquals(0.0, action.getProgress(), 0.0);
      assertEquals(ActionState.INITIALIZED, action.getState());
    }
    assertEquals(Arrays.asList(), executor.getEventsGenerated());
  }

  @Test
  public void testSetRepairPrawnActionWorks() throws Exception {
    warriorB.decreaseEnergy(ENERGY_DELTA);
    assertEquals(EventGeneratingExecutor.State.NOT_PREPARED, executor.getState());
    game.advanceTimeTo(START_TIME + 10);
    executor.prepare(Arrays.asList(
        TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "SET_REPAIR_PRAWN_ACTION")
            .add("time", START_TIME + 10)
            .add("factory", Json.createObjectBuilder()
                .add("city", 1)
                .add("index", 0)
                .build())
            .add("prawn", 1)
            .build())));
    assertEquals(EventGeneratingExecutor.State.PREPARED, executor.getState());

    assertNull(cityB.getFactory(0).getAction());
    executor.executeAndGenerate();
    assertNotNull(cityB.getFactory(0).getAction());
    assertTrue(cityB.getFactory(0).getAction() instanceof CityFactory.RepairPrawnAction);
    {
      final CityFactory.RepairPrawnAction action = (CityFactory.RepairPrawnAction) cityB.getFactory(0).getAction();
      assertEquals(Prawn.MAX_ENERGY - ENERGY_DELTA, action.getProgress(), Constants.DOUBLE_DELTA);
      assertEquals(ActionState.INITIALIZED, action.getState());
      assertEquals(warriorB, action.getPrawn());
    }
    assertEquals(Arrays.asList(), executor.getEventsGenerated());
  }

  @Test
  public void testSetGrowCityActionWorks() throws Exception {
    assertEquals(EventGeneratingExecutor.State.NOT_PREPARED, executor.getState());
    game.advanceTimeTo(START_TIME + 10);
    executor.prepare(Arrays.asList(
        TEST_EVENT,
        new Event(Json.createObjectBuilder()
            .add("type", "SET_GROW_CITY_ACTION")
            .add("time", START_TIME + 10)
            .add("factory", Json.createObjectBuilder()
                .add("city", 1)
                .add("index", 0)
                .build())
            .build())));
    assertEquals(EventGeneratingExecutor.State.PREPARED, executor.getState());

    assertNull(cityB.getFactory(0).getAction());
    executor.executeAndGenerate();
    assertNotNull(cityB.getFactory(0).getAction());
    assertTrue(cityB.getFactory(0).getAction() instanceof CityFactory.GrowAction);
    {
      final CityFactory.GrowAction action = (CityFactory.GrowAction) cityB.getFactory(0).getAction();
      assertEquals(0.0, action.getProgress(), 0.0);
      assertEquals(ActionState.INITIALIZED, action.getState());
    }
    assertEquals(Arrays.asList(), executor.getEventsGenerated());
  }

}
