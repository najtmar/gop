package cz.wie.najtmar.gop.game;

import com.google.gson.JsonObject;
import cz.wie.najtmar.gop.board.BoardException;
import cz.wie.najtmar.gop.board.Joint;
import cz.wie.najtmar.gop.board.Position;
import cz.wie.najtmar.gop.common.Persistable;
import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.event.EventProcessingException;
import cz.wie.najtmar.gop.event.SerializationException;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

import static cz.wie.najtmar.gop.common.Persistable.serializeNullable;
import static cz.wie.najtmar.gop.common.Persistable.typeOf;
import static io.vavr.API.*;

/**
 * Represents a CityFactory in a City.
 * @author najtmar
 */
public class CityFactory implements Persistable {

  public static final String STATE_PROPERTY = "state";
  public static final String INDEX_PROPERTY = "index";
  public static final String ACTION_PROPERTY = "action";

  /**
   * Represents the state of a CityFactory.
   * @author najtmar
   */
  public enum State {
    /**
     * The CityFactory effectively does not exist.
     */
    DISABLED,
    /**
     * The CityFactory exists and is operational.
     */
    ENABLED,
  }

  /**
   * Represents the state of a CityFactory Action.
   * @author najtmar
   */
  public enum ActionState {
    /**
     * The Action is not yet initialized.
     */
    CREATED,
    /**
     * The Action is initialized but not yet performed.
     */
    INITIALIZED,
    /**
     * The Action has already been performed and is finished.
     */
    FINISHED,
  }

  /**
   * State of the CityFactory.
   */
  private State state;

  /**
   * The City to which the CityFactory belongs.
   */
  private final City city;

  /**
   * Index of the CityFactory in the City.
   */
  private int index;

  /**
   * Represents any type of Actions performed by a CityFactory.
   * @author najtmar
   */
  public abstract class Action implements Persistable {

    public static final String STATE_PROPERTY = "state";
    public static final String PROGRESS_PROPERTY = "progress";
    public static final String BUILDER_PROPERTY = "builder";
    public static final String PROGRESS_FACTOR_PROPERTY = "progressFactor";

    /**
     * The state of the Action.
     */
    private ActionState state;

    /**
     * Number from [0.0, 1.0] denoting the progress of the Action. progress 1.0 means that the Action has been finished.
     */
    @Nonnegative private double progress;

    protected Action(JsonObject serialized) {
      this.state = ActionState.valueOf(serialized.getAsJsonPrimitive(STATE_PROPERTY).getAsString());
      this.progress = serialized.getAsJsonPrimitive(PROGRESS_PROPERTY).getAsDouble();
    }

    /**
     * Increases progress by universalDelta and caps the result to 1.0 . The argument denotes an Action type-agnostic
     * delta value--it is multiplied by the Action progress factor to obtain the actual delta. Executes onProgress().
     * Executes onFinished() if the call makes the progress reach 1.0 . If not null, eventFactory is used to generate
     * Events to be appended to list eventsGenerated .
     * @param universalDelta multiplied by Action progress factor gives the value by which the progress is increased
     * @param eventFactory the EventFactory used to generate Events
     * @param eventsGenerated populated with the Events generated with eventFactory
     * @throws GameException when increasing the progress fails
     */
    public void increaseProgressByUniversalDelta(@Nonnegative double universalDelta,
        @Nullable EventFactory eventFactory, @Nullable List<Event> eventsGenerated) throws GameException {
      if (state != ActionState.INITIALIZED) {
        throw new GameException("Only Actions in state INITIALIZED can increase their progress (found " + state + ").");
      }
      /*
      final double previousProgress = progress;
      */
      final double delta = universalDelta * getActionProgressFactor();
      progress += delta;
      if (progress > 1.0) {
        progress = 1.0;
      }
      if (eventFactory != null) {
        try {
          eventsGenerated.add(eventFactory.createSetFactoryProgressEvent(CityFactory.this, progress));
        } catch (EventProcessingException exception) {
          throw new GameException("Event creation for updating progress of " + this + " failed.", exception);
        }
      }
      onIncreaseProgress(delta, eventFactory, eventsGenerated);
      if (progress == 1.0) {
        onFinished(eventFactory, eventsGenerated);
      }
    }

    /**
     * Returns the number by which the progress number should be multiplied to obtain the actual progress number for
     * the Action.
     * @return number from interval (0, 1] denoting the progress factor
     */
    protected abstract double getActionProgressFactor();

    /**
     * Executed from increaseProgress() just after progress has been increased. If not null, eventFactory is used
     * to generate Events to be appended to list eventsGenerated .
     * @param delta the value by which the progress has increased
     * @param eventFactory the EventFactory used to generate Events
     * @param eventsGenerated populated with the Events generated with eventFactory
     * @throws GameException when increasing the progress fails
     */
    protected abstract void onIncreaseProgress(@Nonnegative double delta, @Nullable EventFactory eventFactory,
        @Nullable List<Event> eventsGenerated) throws GameException;

    /**
     * Executed from increaseProgress() just after increaseProgress(), if increasing progress makes progress reach 1.0 .
     * It's the responsibility of this method to change Action state to FINISHED. If not null, eventFactory is used
     * to generate Events to be appended to list eventsGenerated .
     * @param eventFactory the EventFactory used to generate Events
     * @param eventsGenerated populated with the Events generated with eventFactory
     * @throws GameException when finishing the Action fails
     */
    protected abstract void onFinished(@Nullable EventFactory eventFactory, @Nullable List<Event> eventsGenerated)
        throws GameException;

    /**
     * Checks whether the Action is still in progress. Changes Action state to FINISHED and generates appropriate Events
     * if the revision result is negative.
     * @param eventFactory the EventFactory used to generate Events
     * @param eventsGenerated populated with the Events generated with eventFactory
     * @return true iff the revision result is positive
     * @throws GameException when revising the Action fails
     */
    public abstract boolean revise(EventFactory eventFactory, List<Event> eventsGenerated) throws GameException;

    public double getProgress() {
      return progress;
    }

    public ActionState getState() {
      return state;
    }

    /**
     * Sets the state of the Action.
     * @param state the state to be set
     * @throws GameException if the state cannot be set
     */
    protected void setState(@Nonnull ActionState state) throws GameException {
      switch (state) {
        case CREATED:
          throw new GameException("ActionState cannot be set to CREATED.");
        case INITIALIZED:
          if (this.state != ActionState.CREATED) {
            throw new GameException("ActionState can only be set to INITIALIZED when the Action is in state CREATED "
                + "(found " + this.state + ").");
          }
          break;
        case FINISHED:
          if (this.state != ActionState.INITIALIZED) {
            throw new GameException("ActionState can only be set to FINISHED when the Action is in state INITIALIZED "
                + "(found " + this.state + ").");
          }
          break;
        default:
          throw new GameException("Unrecognized ActionState: " + state);
      }
      this.state = state;
    }

    /**
     * Sets the progress. Can only be done on Actions in state CREATED.
     * @param progress the progress to be set
     * @throws GameException when progress cannot be set
     */
    protected void setProgress(double progress) throws GameException {
      if (this.state != ActionState.CREATED) {
        throw new GameException("Progress can only be set for Actions in state CREATED (found " + state + ").");
      }
      this.progress = progress;
    }

    protected Action() throws GameException {
      this.progress = 0.0;
      this.state = ActionState.CREATED;
    }

    @Override
    public JsonObject snapshot() {
      JsonObject returnValue = new JsonObject();
      returnValue.addProperty(STATE_PROPERTY, state.name());
      returnValue.addProperty(PROGRESS_PROPERTY, progress);
      return returnValue;
    }
  }

  /**
   * Represents an Action to produce a Warrior.
   * @author najtmar
   */
  public class ProduceWarriorAction extends Action {

    public static final String DIRECTION_PROPERTY = "direction";

    /**
     * Builder to produce the Warrior.
     */
    private final Warrior.Builder builder;

    /**
     * Direction of the Warrior to be built.
     */
    private final double direction;

    /**
     * The progress factor for the ProduceWarriorAction.
     */
    private final double progressFactor;

    /**
     * Constructor.
     * @param direction the direction of the Warrior to build
     * @throws GameException when creating the Action fails
     */
    public ProduceWarriorAction(@Nonnegative double direction) throws GameException {
      super();
      this.builder = new Warrior.Builder();
      this.direction = direction;
      final Joint joint = city.getJoint();
      this.builder.setCreationTime(city.getGame().getTime());
      this.builder.setCurrentPosition(joint.getNormalizedPosition());
      this.builder.setDirection(direction);
      this.builder.setGameProperties(city.getGame().getGameProperties());
      this.builder.setPlayer(city.getPlayer());
      this.progressFactor = city.getGame().getGameProperties().getProduceActionProgressFactor();
      this.setState(ActionState.INITIALIZED);
    }

    public ProduceWarriorAction(JsonObject serialized, Player player) throws GameException {
      super(serialized);
      this.builder = new Warrior.Builder(serialized.getAsJsonObject(BUILDER_PROPERTY), player);
      this.direction = serialized.getAsJsonPrimitive(DIRECTION_PROPERTY).getAsDouble();
      this.progressFactor = serialized.getAsJsonPrimitive(PROGRESS_FACTOR_PROPERTY).getAsDouble();
    }

    public double getDirection() {
      return direction;
    }

    @Override
    protected double getActionProgressFactor() {
      return progressFactor;
    }

    @Override
    protected void onIncreaseProgress(double unusedDelta, EventFactory unusedEventFactory,
        List<Event> unusedEventsGenerated) throws GameException {
      // No extra action to be performed.
    }

    @Override
    protected void onFinished(@Nonnull EventFactory eventFactory, @Nonnull List<Event> eventsGenerated)
        throws GameException  {
      final Warrior warrior = builder.build();
      city.getGame().addPrawn(warrior);
      if (eventFactory != null) {
        try {
          eventsGenerated.add(eventFactory.createProduceWarriorEvent(warrior.getId(), warrior.getCurrentPosition(),
              warrior.getWarriorDirection(), warrior.getPlayer()));
          eventsGenerated.add(eventFactory.createActionFinishedEvent(CityFactory.this));
        } catch (EventProcessingException exception) {
          throw new GameException("Event for creation of " + warrior + " or finishing " + this + " failed.", exception);
        }
      }

      // Potentially start new Battles with warrior .
      try {
        tryStartBattlesForNewPrawn(warrior, eventFactory, eventsGenerated);
      } catch (BoardException exception) {
        throw new GameException("Event for creation of " + warrior + " or finishing " + this + " failed.", exception);
      } catch (EventProcessingException exception) {
        throw new GameException("Event for creation of " + warrior + " or finishing " + this + " failed.", exception);
      }

      setState(ActionState.FINISHED);
    }

    @Override
    public boolean revise(EventFactory eventFactory, List<Event> eventsGenerated)
        throws GameException {
      return true;
    }

    @Override
    public JsonObject snapshot() {
      JsonObject returnValue = super.snapshot();
      returnValue.addProperty(TYPE_PROPERTY, typeOf(this.getClass()));
      returnValue.add(BUILDER_PROPERTY, builder.snapshot());
      returnValue.addProperty(DIRECTION_PROPERTY, direction);
      returnValue.addProperty(PROGRESS_FACTOR_PROPERTY, progressFactor);

      return returnValue;
    }
  }

  /**
   * Represents an Action to produce a SettlersUnit.
   * @author najtmar
   */
  public class ProduceSettlersUnitAction extends Action {

    /**
     * Builder to produce the SettlersUnit.
     */
    private final SettlersUnit.Builder builder;

    /**
     * The progress factor for the ProduceSettlersUnitAction.
     */
    private final double progressFactor;

    /**
     * Constructor.
     * @throws GameException when creating the Action fails
     */
    public ProduceSettlersUnitAction() throws GameException {
      super();
      this.builder = new SettlersUnit.Builder();
      final Joint joint = city.getJoint();
      this.builder.setCreationTime(city.getGame().getTime());
      this.builder.setCurrentPosition(joint.getNormalizedPosition());
      this.builder.setGameProperties(city.getGame().getGameProperties());
      this.builder.setPlayer(city.getPlayer());
      this.progressFactor = city.getGame().getGameProperties().getProduceActionProgressFactor();
      this.setState(ActionState.INITIALIZED);
    }

    public ProduceSettlersUnitAction(JsonObject serialized, Player player) throws GameException {
      super(serialized);
      this.builder = new SettlersUnit.Builder(serialized.getAsJsonObject(BUILDER_PROPERTY), player);
      this.progressFactor = serialized.getAsJsonPrimitive(PROGRESS_FACTOR_PROPERTY).getAsDouble();
    }

    @Override
    protected double getActionProgressFactor() {
      return progressFactor;
    }

    @Override
    protected void onIncreaseProgress(double unusedDelta, EventFactory unusedEventFactory,
        List<Event> unusedEventsGenerated) throws GameException {
      // No extra action to be performed.
    }

    @Override
    protected void onFinished(EventFactory eventFactory, List<Event> eventsGenerated) throws GameException  {
      final SettlersUnit settlersUnit = builder.build();
      city.getGame().addPrawn(settlersUnit);
      final int oldIndex = index;
      city.shrinkOrDestroy(index);
      if (eventFactory != null) {
        try {
          eventsGenerated.add(eventFactory.createProduceSettlersUnitEvent(settlersUnit.getId(),
              settlersUnit.getCurrentPosition(), settlersUnit.getPlayer()));
          eventsGenerated.add(eventFactory.createShrinkOrDestroyCityEvent(city, oldIndex));
          eventsGenerated.add(eventFactory.createActionFinishedEvent(CityFactory.this));
        } catch (EventProcessingException exception) {
          throw new GameException("Events creation for creation of " + settlersUnit + " in " + city + " or finishing "
              + this + " failed.",
              exception);
        }
      }

      // Potentially start new Battles with settlersUnit .
      try {
        tryStartBattlesForNewPrawn(settlersUnit, eventFactory, eventsGenerated);
      } catch (BoardException exception) {
        throw new GameException("Event for creation of " + settlersUnit + " or finishing " + this + " failed.",
            exception);
      } catch (EventProcessingException exception) {
        throw new GameException("Event for creation of " + settlersUnit + " or finishing " + this + " failed.",
            exception);
      }

      setState(ActionState.FINISHED);
    }

    @Override
    public boolean revise(EventFactory eventFactory, List<Event> eventsGenerated)
        throws GameException {
      return true;
    }

    @Override
    public JsonObject snapshot() {
      JsonObject returnValue = super.snapshot();
      returnValue.addProperty(TYPE_PROPERTY, typeOf(this.getClass()));
      returnValue.add(BUILDER_PROPERTY, builder.snapshot());
      returnValue.addProperty("progressFactor", progressFactor);

      return returnValue;
    }
  }

  /**
   * Represents an Action to repair a Prawn.
   * @author najtmar
   */
  public class RepairPrawnAction extends Action {

    public static final String PRAWN_PROPERTY = "prawn";

    /**
     * Prawn to be repaired.
     */
    private final Prawn prawn;

    /**
     * The progress factor for the RepairPrawnAction.
     */
    private final double progressFactor;

    /**
     * Constructor.
     * @param prawn the Prawn to be repaired
     * @throws GameException when creating the Action fails
     */
    public RepairPrawnAction(@Nonnull Prawn prawn) throws GameException {
      super();
      if (!prawn.getCurrentPosition().equals(city.getJoint().getNormalizedPosition())) {
        throw new GameException("Prawn to be repaired must be located in the city. City Position: "
            + city.getJoint().getNormalizedPosition() + ", Prawn Position: " + prawn.getCurrentPosition());
      }
      this.prawn = prawn;
      this.setProgress(this.prawn.getEnergy() / Prawn.MAX_ENERGY);
      this.progressFactor = city.getGame().getGameProperties().getRepairActionProgressFactor();
      this.setState(ActionState.INITIALIZED);
    }

    public RepairPrawnAction(JsonObject serialized, Game game) {
      super(serialized);
      this.prawn = game.lookupPrawnById(serialized.getAsJsonPrimitive(PRAWN_PROPERTY).getAsInt(), false);
      this.progressFactor = serialized.getAsJsonPrimitive(PROGRESS_FACTOR_PROPERTY).getAsDouble();
    }

    Prawn getPrawn() {
      return prawn;
    }

    @Override
    protected double getActionProgressFactor() {
      return progressFactor;
    }

    @Override
    protected void onIncreaseProgress(double delta, EventFactory eventFactory, List<Event> eventsGenerated)
        throws GameException {
      prawn.increaseEnergy(delta * Prawn.MAX_ENERGY);
      if (eventFactory != null) {
        try {
          eventsGenerated.add(eventFactory.createRepairPrawnEvent(prawn, delta * Prawn.MAX_ENERGY));
        } catch (EventProcessingException exception) {
          throw new GameException("Event creation for repairing " + prawn + " failed.", exception);
        }
      }
    }

    @Override
    protected void onFinished(EventFactory eventFactory, List<Event> eventsGenerated) throws GameException  {
      prawn.increaseEnergy(Prawn.MAX_ENERGY);  // Just to make sure that the energy is fully recharged.
      setState(ActionState.FINISHED);
      if (eventFactory != null) {
        try {
          eventsGenerated.add(eventFactory.createRepairPrawnEvent(prawn, Prawn.MAX_ENERGY));
          eventsGenerated.add(eventFactory.createActionFinishedEvent(CityFactory.this));
        } catch (EventProcessingException exception) {
          throw new GameException("Event creation for repairing " + prawn + " or finishing " + this + "failed.",
              exception);
        }
      }
    }

    @Override
    public boolean revise(EventFactory eventFactory, List<Event> eventsGenerated)
        throws GameException {
      final Joint prawnJoint = prawn.getCurrentPosition().getJoint();
      if (prawnJoint == null || !city.getJoint().equals(prawnJoint)) {
        setState(ActionState.FINISHED);
        if (eventFactory != null) {
          try {
            eventsGenerated.add(eventFactory.createActionFinishedEvent(CityFactory.this));
          } catch (EventProcessingException exception) {
            throw new GameException("Event creation for finishing " + this + "failed.",
                exception);
          }
        }
        return false;
      }
      return true;
    }

    @Override
    public JsonObject snapshot() {
      JsonObject returnValue = super.snapshot();
      returnValue.addProperty(TYPE_PROPERTY, typeOf(this.getClass()));
      returnValue.addProperty(PRAWN_PROPERTY, prawn.getId());
      returnValue.addProperty(PROGRESS_FACTOR_PROPERTY, progressFactor);

      return returnValue;
    }
  }

  /**
   * Represents an Action to grow the City.
   * @author najtmar
   *
   */
  public class GrowAction extends Action {

    /**
     * The progress factor for the GrowAction.
     */
    private final double progressFactor;

    protected GrowAction() throws GameException {
      super();
      this.progressFactor = city.getGame().getGameProperties().getGrowActionProgressFactor();
      this.setState(ActionState.INITIALIZED);
    }

    public GrowAction(JsonObject serialized) {
      super(serialized);
      this.progressFactor = serialized.getAsJsonPrimitive(PROGRESS_FACTOR_PROPERTY).getAsDouble();
    }

    @Override
    protected double getActionProgressFactor() {
      return progressFactor;
    }

    @Override
    protected void onIncreaseProgress(double delta, EventFactory unusedEventFactory, List<Event> unusedEventsGenerated)
        throws GameException {
      // No extra action to be performed.
    }

    @Override
    protected void onFinished(EventFactory eventFactory, List<Event> eventsGenerated) throws GameException {
      int growActionFinishedCount = 0;
      for (int i = 0; i < city.getSize(); ++i) {
        final CityFactory.Action action = city.getFactory(i).getAction();
        if (action == null || !(action instanceof CityFactory.GrowAction)) {
          continue;
        }
        final CityFactory.GrowAction growAction = (CityFactory.GrowAction) action;
        if (growAction.getState() == ActionState.INITIALIZED && growAction.getProgress() == 1.0) {
          ++growActionFinishedCount;
        }
      }
      if (growActionFinishedCount == 0) {
        throw new GameException(
            "Method onGrowActionFinished() invoked even though no GrowAction in the city has reached progress 1.0.");
      }
      if (growActionFinishedCount == city.getSize()) {
        final int oldSize = city.getSize();
        city.grow();
        if (eventFactory != null) {
          try {
            eventsGenerated.add(eventFactory.createGrowCityEvent(city));
          } catch (EventProcessingException exception) {
            throw new GameException("Event creation for growing " + this + " failed.", exception);
          }
        }
        for (int i = 0; i < oldSize; ++i) {
          if (eventFactory != null) {
            try {
              eventsGenerated.add(eventFactory.createActionFinishedEvent(city.getFactory(i)));
            } catch (EventProcessingException exception) {
              throw new GameException("Event creation for finishing Action " + city.getFactory(i).getAction()
                  + " failed.", exception);
            }
          }
          city.getFactory(i).getAction().setState(ActionState.FINISHED);
        }
      }
    }

    @Override
    public boolean revise(EventFactory eventFactory, List<Event> eventsGenerated)
        throws GameException {
      return true;
    }

    @Override
    public JsonObject snapshot() {
      JsonObject returnValue = super.snapshot();
      returnValue.addProperty(TYPE_PROPERTY, typeOf(this.getClass()));
      returnValue.addProperty(PROGRESS_FACTOR_PROPERTY, progressFactor);

      return returnValue;
    }
  }

  /**
   * The Action being performed by the CityFactory. null if the CityFactory performs no Action.
   */
  @Nullable private Action action;

  public CityFactory(JsonObject serialized, City city) {
    this.state = State.valueOf(serialized.getAsJsonPrimitive(STATE_PROPERTY).getAsString());
    this.index = serialized.getAsJsonPrimitive(INDEX_PROPERTY).getAsInt();
    this.city = city;
    this.action = Persistable.deserializeNullable(serialized, ACTION_PROPERTY, JsonObject::getAsJsonObject)
            .map(action -> Match(Persistable.typeOf(action)).of(
                    Case($(ProduceWarriorAction.class.getSimpleName()), () -> new ProduceWarriorAction(action, city.getPlayer())),
                    Case($(ProduceSettlersUnitAction.class.getSimpleName()), () -> new ProduceSettlersUnitAction(action, city.getPlayer())),
                    Case($(RepairPrawnAction.class.getSimpleName()), () -> new RepairPrawnAction(action, city.getGame())),
                    Case($(GrowAction.class.getSimpleName()), () -> new GrowAction(action)),
                    Case($(), type -> {
                      throw new SerializationException("Unrecognized type: " + type);
                    })))
            .orElse(null);
  }

  /**
   * Constructor.
   * @param city the City to which the CityFactory belongs
   * @param index the index of the CityFactory in the City
   */
  public CityFactory(@Nonnull City city, @Nonnegative int index) {
    this.state = State.DISABLED;
    this.city = city;
    this.index = index;
  }

  public State getState() {
    return state;
  }

  City getCity() {
    return city;
  }

  int getIndex() {
    return index;
  }

  public void setIndex(int index) {
    this.index = index;
  }

  /**
   * Disables the CityFactory. Interrupts any Action being performed by the CityFactory.
   * @throws GameException when the CityFactory is not ENABLED
   */
  public void disable() throws GameException {
    if (state != State.ENABLED) {
      throw new GameException("The CityFactory is not ENABLED.");
    }
    this.action = null;
    this.state = State.DISABLED;
  }

  /**
   * Enables the CityFactory.
   * @throws GameException when the CityFactory is not DISABLED
   */
  public void enable() throws GameException {
    if (state != State.DISABLED) {
      throw new GameException("The CityFactory is not DISABLED.");
    }
    this.state = State.ENABLED;
  }

  /**
   * Returns the Action being performed by the CityFactory or null if the CityFactory doesn't perform an Action.
   * @return the Action being performed by the CityFactory or null
   * @throws GameException when the CityFactory is not ENABLED
   */
  public Action getAction() throws GameException {
    if (state != State.ENABLED) {
      throw new GameException("Actions only make sense for ENABLED Factories.");
    }
    return action;
  }

  /**
   * Sets the Action to be performed by the CityFactory.
   * @param action the Action to be set
   * @throws GameException when the CityFactory is not ENABLED
   */
  public void setAction(Action action) throws GameException {
    if (state != State.ENABLED) {
      throw new GameException("Actions only make sense for ENABLED Factories.");
    }
    this.action = action;
  }

  /**
   * Given the ongoing Battles, check whether a new Battle should be started for newPrawn .
   * @param newPrawn a new Prawn just created
   * @param eventFactory an instance of EventFactory used to create ADD_BATTLE Events
   * @param eventsGenerated list of Events to be generated
   * @throws BoardException when Battle checking process fails
   * @throws GameException when Battle checking process fails
   * @throws EventProcessingException when Event generation fails
   */
  void tryStartBattlesForNewPrawn(@Nonnull Prawn newPrawn, EventFactory eventFactory, List<Event> eventsGenerated)
      throws BoardException, GameException, EventProcessingException {
    final Game game = getCity().getGame();
    for (Battle battle : game.getBattles()) {
      // Check whether battle may potentially affect newPrawn .
      final Prawn[] participants = battle.getPrawns();
      if (!participants[0].getPlayer().equals(city.getPlayer())
          && !participants[1].getPlayer().equals(city.getPlayer())) {
        continue;
      }
      final Prawn companion;
      final Prawn opponent;
      if (participants[0].getPlayer().equals(city.getPlayer())) {
        companion = participants[0];
        opponent = participants[1];
      } else {
        companion = participants[1];
        opponent = participants[0];
      }
      if (!companion.getCurrentPosition().equals(newPrawn.getCurrentPosition())) {
        continue;
      }

      // Check if opponent attacks newPrawn's position.
      final Move[] opponentPlannedMoves = opponent.prepareMoveSequence(
          opponent.getLastMoveTime() + game.getGameProperties().getTimeDelta());
      // If opponent has no plan to move, skip checking.
      if (opponentPlannedMoves.length < 2) {
        continue;
      }
      final Position opponentCurrentPosition = opponent.getCurrentPosition();
      final Position opponentNextPosition = opponentPlannedMoves[1].getPosition();
      if (Battle.allBattleConditionsMet(newPrawn, newPrawn.getCurrentPosition(), null, opponent,
          opponentCurrentPosition, opponentNextPosition)) {
        final Battle newBattle = new Battle(newPrawn, newPrawn.getCurrentPosition(), null, opponent,
            opponentCurrentPosition, opponentNextPosition);
        if (game.getBattle(newBattle, false) == null) {
          game.addBattle(newBattle);
          if (eventFactory != null) {
            eventsGenerated.add(eventFactory.createAddBattleEvent(newBattle));
          }
        }

      }
    }
  }

  @Override
  public JsonObject snapshot() {
    JsonObject returnValue = new JsonObject();
    returnValue.addProperty(STATE_PROPERTY, state.name());
    returnValue.addProperty(INDEX_PROPERTY, index);
    returnValue.add(ACTION_PROPERTY, serializeNullable(action, Action::snapshot));
    return returnValue;
  }

}
