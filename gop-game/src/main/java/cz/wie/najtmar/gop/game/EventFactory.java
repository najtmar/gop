package cz.wie.najtmar.gop.game;

import cz.wie.najtmar.gop.board.Position;
import cz.wie.najtmar.gop.board.RoadSection;
import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.event.EventProcessingException;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import java.io.StringReader;

/**
 * Factory class to create Events in the context of a Game.
 * @author najtmar
 */
public class EventFactory {

  /**
   * ObjectSerializer instance used to create Events.
   */
  private final ObjectSerializer objectSerializer;

  /**
   * The Game in which Events are processed.
   */
  private final Game game;

  public EventFactory(@Nonnull ObjectSerializer objectSerializer) {
    this.objectSerializer = objectSerializer;
    this.game = objectSerializer.getGame();
  }

  /**
   * Creates Events of type INITIALIZE_GAME .
   * @return a new instance of an INITIALIZE_GAME Event
   * @throws EventProcessingException when Event creation fails
   */
  public Event createInitializeGameEvent()
      throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.INITIALIZE_GAME);
    final JsonObject body = bodyBuilder
        .add("game", objectSerializer.serializeGameWithoutPrawnsOrCities())
        .build();
    return new Event(body);
  }

  /**
   * Creates Events of type RESTORE_GAME .
   * @return a new instance of an RESTORE_GAME Event
   * @throws EventProcessingException when Event creation fails
   */
  public Event createRestoreGameEvent()
          throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.RESTORE_GAME);
    JsonReader jsonReader = Json.createReader(new StringReader(game.snapshot().toString()));
    final JsonObject body = bodyBuilder
            .add("game", jsonReader.readObject())
            .build();
    jsonReader.close();
    return new Event(body);
  }

  /**
   * Creates Events of type ABANDON_GAME_ATTEMPT .
   * @return a new instance of an ABANDON_GAME_ATTEMPT Event
   * @throws EventProcessingException when Event creation fails
   */
  public Event createAbandonGameAttemptEvent()
          throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.ABANDON_GAME_ATTEMPT);
    final JsonObject body = bodyBuilder.build();
    return new Event(body);
  }

  /**
   * Creates Events of type INTERRUPT_GAME .
   * @return a new instance of an INTERRUPT_GAME Event
   * @throws EventProcessingException when Event creation fails
   */
  public Event createInterruptGameEvent()
          throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.INTERRUPT_GAME);
    final JsonObject body = bodyBuilder.build();
    return new Event(body);
  }

  /**
   * Creates Events of type CHANGE_PLAYER_STATE .
   * @param player the Player whose State changes
   * @param state the State the Player changes its State to
   * @return a new instance of a CHANGE_PLAYER_STATE Event
   * @throws EventProcessingException when Event creation fails
   */
  public Event createChangePlayerStateEvent(@Nonnull Player player, @Nonnull Player.State state)
      throws EventProcessingException {
    if (player.getState() != Player.State.IN_GAME || state == Player.State.IN_GAME) {
      throw new EventProcessingException("Player can only change State from IN_GAME to not IN_GAME (found "
          + player.getState() + " -> " + state + ").");
    }
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.CHANGE_PLAYER_STATE);
    bodyBuilder.add("player", objectSerializer.serializePlayer(player));
    bodyBuilder.add("state", state.toString());
    return new Event(bodyBuilder.build());
  }

  /**
   * Creates Events of type FINISH_GAME .
   * @return a new instance of a FINISH_GAME Event
   * @throws EventProcessingException when Event creation fails
   */
  public Event createFinishGameEvent() throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.FINISH_GAME);
    return new Event(bodyBuilder.build());
  }

  /**
   * Creates Events of type MOVE_PRAWN .
   * @param prawn the Prawn to move
   * @param move the Move of the prawn
   * @return a new instance of a MOVE_PRAWN Event to move the prawn with a given move
   * @throws EventProcessingException when Event creation fails
   */
  public Event createMovePrawnEvent(@Nonnull Prawn prawn, @Nonnull Move move)
      throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.MOVE_PRAWN);
    if (game.getTime() < move.getMoveTime()) {
      throw new EventProcessingException("Move time must not be greater than the Game time (" + move.getMoveTime()
          + " >  " + game.getTime() + ").");
    }
    final JsonObject body = bodyBuilder
        .add("prawn", objectSerializer.serializePrawn(prawn))
        .add("move", objectSerializer.serializeMove(move))
        .build();
    return new Event(body);
  }

  /**
   * Creates Events of type STAY_PRAWN .
   * @param prawn the Prawn to stay
   * @param stayTime the time at which the prawn is forced to stay
   * @return a new instance of a STAY_PRAWN Event to make the prawn stay
   * @throws EventProcessingException when Event creation fails
   */
  public Event createStayPrawnEvent(@Nonnull Prawn prawn, @Nonnegative long stayTime) throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.STAY_PRAWN);
    if (game.getTime() < stayTime) {
      throw new EventProcessingException("Stay time must not be greater than the Game time (" + stayTime + " >  "
          + game.getTime() + ").");
    }
    final JsonObject body = bodyBuilder
        .add("prawn", objectSerializer.serializePrawn(prawn))
        .add("stayTime", stayTime)
        .build();
    return new Event(body);
  }

  /**
   * Creates Events of type DECREASE_PRAWN_ENERGY .
   * @param prawn the Prawn to decrease the energy
   * @param delta the number by which the Prawn energy is decreased
   * @return a new instance of a DECREASE_PRAWN_ENERGY Event to decrease the prawn energy
   * @throws EventProcessingException when Event creation fails
   */
  public Event createDecreasePrawnEnergyEvent(@Nonnull Prawn prawn, @Nonnegative double delta)
      throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.DECREASE_PRAWN_ENERGY);
    final JsonObject body = bodyBuilder
        .add("prawn", objectSerializer.serializePrawn(prawn))
        .add("delta", delta)
        .build();
    return new Event(body);
  }

  /**
   * Creates Events of type DESTROY_PRAWN .
   * @param prawn the Prawn to decrease the energy
   * @return a new instance of a DESTROY_PRAWN Event to destroy the prawn
   * @throws EventProcessingException when Event creation fails
   */
  public Event createDestroyPrawnEvent(@Nonnull Prawn prawn) throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.DESTROY_PRAWN);
    final JsonObject body = bodyBuilder
        .add("prawn", objectSerializer.serializePrawn(prawn))
        .build();
    return new Event(body);
  }

  /**
   * Creates Events of type PRODUCE_WARRIOR .
   * @param id the id of the Warrior
   * @param position the Position on which the Warrior is produced
   * @param direction the Warrior direction of the Warrior produced
   * @param player the Player whose Warrior is produced
   * @return a new instance of a PRODUCE_WARRIOR Event to produce a Warrior
   * @throws EventProcessingException when Event creation fails
   */
  public Event createProduceWarriorEvent(@Nonnegative int id, @Nonnull Position position, double direction,
      @Nonnull Player player) throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.PRODUCE_WARRIOR);
    final JsonObject body = bodyBuilder
        .add("id", id)
        .add("position", objectSerializer.serializePosition(position))
        .add("direction", direction)
        .add("player", objectSerializer.serializePlayer(player))
        .build();
    return new Event(body);
  }

  /**
   * Creates Events of type PRODUCE_SETTLERS_UNIT .
   * @param id the id of the SettlersUnit
   * @param position the Position on which the SettlersUnit is produced
   * @param player the Player whose SettlersUnit is produced
   * @return a new instance of a PRODUCE_SETTLERS_UNIT Event to produce a SettlersUnit
   * @throws EventProcessingException when Event creation fails
   */
  public Event createProduceSettlersUnitEvent(@Nonnegative int id, @Nonnull Position position, @Nonnull Player player)
      throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.PRODUCE_SETTLERS_UNIT);
    final JsonObject body = bodyBuilder
        .add("id", id)
        .add("position", objectSerializer.serializePosition(position))
        .add("player", objectSerializer.serializePlayer(player))
        .build();
    return new Event(body);
  }

  /**
   * Creates Events of type REPAIR_PRAWN .
   * @param prawn the Prawn to repair
   * @param delta the number by which the Prawn energy is increased
   * @return a new instance of a REPAIR_PRAWN Event to repair prawn
   * @throws EventProcessingException when Event creation fails
   */
  public Event createRepairPrawnEvent(@Nonnull Prawn prawn, @Nonnegative double delta)
      throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.REPAIR_PRAWN);
    final JsonObject body = bodyBuilder
        .add("prawn", objectSerializer.serializePrawn(prawn))
        .add("delta", delta)
        .build();
    return new Event(body);
  }

  /**
   * Creates Events of type CREATE_CITY .
   * @param settlersUnit the SettlersUnit that creates the City
   * @param id the id of the City
   * @param name the name of the City
   * @return a new instance of the CREATE_CITY Event
   * @throws EventProcessingException when Event creation fails
   */
  public Event createCreateCityEvent(@Nonnull SettlersUnit settlersUnit, @Nonnegative int id, @Nonnull String name)
      throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.CREATE_CITY);
    final JsonObject body = bodyBuilder
        .add("settlersUnit", objectSerializer.serializePrawn(settlersUnit))
        .add("id", id)
        .add("name", name)
        .build();
    return new Event(body);
  }

  /**
   * Creates Events of type GROW_CITY .
   * @param city the City to grow
   * @return a new instance of a GROW_CITY Event to grow city
   * @throws EventProcessingException when Event creation fails
   */
  public Event createGrowCityEvent(@Nonnull City city)
      throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.GROW_CITY);
    final JsonObject body = bodyBuilder
        .add("city", objectSerializer.serializeCity(city))
        .build();
    return new Event(body);
  }

  /**
   * Creates Events of type SHRINK_OR_DESTROY_CITY .
   * @param city the City to shrink
   * @param factoryIndex the index of the Factory that is removed
   * @return a new instance of a SHRINK_OR_DESTROY_CITY Event to shrink or destroy city
   * @throws EventProcessingException when Event creation fails
   */
  public Event createShrinkOrDestroyCityEvent(@Nonnull City city, @Nonnegative int factoryIndex)
      throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.SHRINK_OR_DESTROY_CITY);
    final JsonObject body = bodyBuilder
        .add("city", objectSerializer.serializeCity(city))
        .add("factoryIndex", factoryIndex)
        .build();
    return new Event(body);
  }

  /**
   * Creates Events of type CAPTURE_OR_DESTROY_CITY .
   * @param city the City to capture or destroy
   * @param defender the Player whose City is captured or destroyed
   * @param attacker the Player who attacks or destroys city
   * @return a new instance of a CAPTURE_OR_DESTROY_CITY Event to capture or destroy city
   * @throws EventProcessingException when Event creation fails
   */
  public Event createCaptureOrDestroyCityEvent(@Nonnull City city, @Nonnull Player defender, @Nonnull Player attacker)
      throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.CAPTURE_OR_DESTROY_CITY);
    final JsonObject body = bodyBuilder
        .add("city", objectSerializer.serializeCity(city))
        .add("defender", objectSerializer.serializePlayer(defender))
        .add("attacker", objectSerializer.serializePlayer(attacker))
        .build();
    return new Event(body);
  }

  /**
   * Creates Events of type ADD_BATTLE .
   * @param battle the Battle to create
   * @return a new instance of a ADD_BATTLE Event to add battle
   * @throws EventProcessingException when Event creation fails
   */
  public Event createAddBattleEvent(@Nonnull Battle battle)
      throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.ADD_BATTLE);
    final JsonObject body = bodyBuilder
        .add("battle", objectSerializer.serializeBattle(battle))
        .build();
    return new Event(body);
  }

  /**
   * Creates Events of type REMOVE_BATTLE .
   * @param battle the Battle to remove
   * @return a new instance of a REMOVE_BATTLE Event to remove battle
   * @throws EventProcessingException when Event creation fails
   */
  public Event createRemoveBattleEvent(@Nonnull Battle battle)
      throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.REMOVE_BATTLE);
    final JsonObject body = bodyBuilder
        .add("battle", objectSerializer.serializeBattle(battle))
        .build();
    return new Event(body);
  }

  /**
   * Creates Events of type INCREASE_PROGRESS_BY_UNIVERSAL_DELTA .
   * @param factory the CityFactory whose Action's progress is increased
   * @param universalDelta the value by which the Action's progress is increased
   * @return a new instance of an INCREASE_PROGRESS_BY_UNIVERSAL_DELTA Event
   * @throws EventProcessingException when Event creation fails
   */
  public Event createIncreaseProgressByUniversalDeltaEvent(@Nonnull CityFactory factory,
      @Nonnegative double universalDelta) throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.INCREASE_PROGRESS_BY_UNIVERSAL_DELTA);
    final JsonObject body = bodyBuilder
        .add("factory", objectSerializer.serializeFactory(factory))
        .add("universalDelta", universalDelta)
        .build();
    return new Event(body);
  }

  /**
   * Creates Events of type SET_FACTORY_PROGRESS .
   * @param factory the CityFactory whose Action's progress is set
   * @param progress the value to which the Action's progress is set
   * @return a new instance of an SET_FACTORY_PROGRESS Event
   * @throws EventProcessingException when Event creation fails
   */
  public Event createSetFactoryProgressEvent(@Nonnull CityFactory factory, @Nonnegative double progress)
      throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.SET_FACTORY_PROGRESS);
    final JsonObject body = bodyBuilder
        .add("factory", objectSerializer.serializeFactory(factory))
        .add("progress", progress)
        .build();
    return new Event(body);
  }

  /**
   * Creates Events of type ACTION_FINISHED .
   * @param factory the CityFactory whose Action is set to FINISHED
   * @return a new instance of an ACTION_FINISHED Event
   * @throws EventProcessingException when Event creation fails
   */
  public Event createActionFinishedEvent(@Nonnull CityFactory factory) throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.ACTION_FINISHED);
    final JsonObject body = bodyBuilder
        .add("factory", objectSerializer.serializeFactory(factory))
        .build();
    return new Event(body);
  }

  /**
   * Creates Events of type PERIODIC_INTERRUPT .
   * @return a new instance of a PERIODIC_INTERRUPT Event
   * @throws EventProcessingException when Event creation fails
   */
  public Event createPeriodicInterruptEvent() throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.PERIODIC_INTERRUPT);
    final JsonObject body = bodyBuilder
        .build();
    return new Event(body);
  }

  /**
   * Creates Events of type ROAD_SECTION_HALF_APPEARS .
   * @param roadSectionHalf the RoadSection.Half that appears
   * @param player the Player for whom roadSectionHalf appears
   * @return a new instance of a ROAD_SECTION_HALF_APPEARS Event
   * @throws EventProcessingException when Event creation fails
   */
  public Event createRoadSectionHalfAppearsEvent(@Nonnull RoadSection.Half roadSectionHalf, @Nonnull Player player)
      throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.ROAD_SECTION_HALF_APPEARS);
    final JsonObject body = bodyBuilder
        .add("roadSectionHalf", objectSerializer.serializeRoadSectionHalf(roadSectionHalf))
        .add("player", objectSerializer.serializePlayer(player))
        .build();
    return new Event(body);
  }

  /**
   * Creates Events of type ROAD_SECTION_HALF_DISAPPEARS .
   * @param roadSectionHalf the RoadSection.Half that appears
   * @param player the Player for whom roadSectionHalf appears
   * @return a new instance of a ROAD_SECTION_HALF_DISAPPEARS Event
   * @throws EventProcessingException when Event creation fails
   */
  public Event createRoadSectionHalfDisappearsEvent(@Nonnull RoadSection.Half roadSectionHalf, @Nonnull Player player)
      throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.ROAD_SECTION_HALF_DISAPPEARS);
    final JsonObject body = bodyBuilder
        .add("roadSectionHalf", objectSerializer.serializeRoadSectionHalf(roadSectionHalf))
        .add("player", objectSerializer.serializePlayer(player))
        .build();
    return new Event(body);
  }

  /**
   * Creates Events of type ROAD_SECTION_HALF_DISCOVERED .
   * @param roadSectionHalf the RoadSection.Half that appears
   * @param player the Player for whom roadSectionHalf appears
   * @return a new instance of a ROAD_SECTION_HALF_DISCOVERED Event
   * @throws EventProcessingException when Event creation fails
   */
  public Event createRoadSectionHalfDiscoveredEvent(@Nonnull RoadSection.Half roadSectionHalf, @Nonnull Player player)
      throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.ROAD_SECTION_HALF_DISCOVERED);
    final JsonObject body = bodyBuilder
        .add("roadSectionHalf", objectSerializer.serializeRoadSectionHalf(roadSectionHalf))
        .add("player", objectSerializer.serializePlayer(player))
        .build();
    return new Event(body);
  }

  /**
   * Creates Events of type PRAWN_APPEARS .
   * @param prawn the Prawn that appears
   * @param player the Player for whom prawn appears
   * @return a new instance of a PRAWN_APPEARS Event
   * @throws EventProcessingException when Event creation fails
   */
  public Event createPrawnAppearsEvent(@Nonnull Prawn prawn, @Nonnull Player player) throws EventProcessingException {
    if (prawn.getPlayer().equals(player)) {
      throw new EventProcessingException("Event PRAWN_APPEARS can only be generated for Prawns of a different Player ("
          + prawn + ").");
    }
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.PRAWN_APPEARS);
    final JsonObject body = bodyBuilder
        .add("prawn", objectSerializer.serializePrawn(prawn))
        .add("player", objectSerializer.serializePlayer(player))
        .build();
    return new Event(body);
  }

  /**
   * Creates Events of type PRAWN_DISAPPEARS .
   * @param prawn the Prawn that disappears
   * @param player the Player for whom prawn disappears
   * @return a new instance of a PRAWN_DISAPPEARS Event
   * @throws EventProcessingException when Event creation fails
   */
  public Event createPrawnDisappearsEvent(@Nonnull Prawn prawn, @Nonnull Player player)
      throws EventProcessingException {
    if (prawn.getPlayer().equals(player)) {
      throw new EventProcessingException("Event PRAWN_DISAPPEARS can only be generated for Prawns of a different "
          + "Player (" + prawn + ").");
    }
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.PRAWN_DISAPPEARS);
    final JsonObject body = bodyBuilder
        .add("prawn", objectSerializer.serializePrawn(prawn))
        .add("player", objectSerializer.serializePlayer(player))
        .build();
    return new Event(body);
  }

  /**
   * Creates Events of type CITY_APPEARS .
   * @param city the City that appears
   * @param player the Player for whom city appears
   * @return a new instance of a CITY_APPEARS Event
   * @throws EventProcessingException when Event creation fails
   */
  public Event createCityAppearsEvent(@Nonnull City city, @Nonnull Player player) throws EventProcessingException {
    if (city.getPlayer().equals(player)) {
      throw new EventProcessingException("Event CITY_APPEARS can only be generated for Cities of a different Player ("
          + city + ").");
    }
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.CITY_APPEARS);
    final JsonObject body = bodyBuilder
        .add("city", objectSerializer.serializeCity(city))
        .add("player", objectSerializer.serializePlayer(player))
        .build();
    return new Event(body);
  }

  /**
   * Creates Events of type CITY_SHOWN_DESTROYED .
   * @param city the City that disappears
   * @param player the Player for whom city disappears
   * @return a new instance of a CITY_SHOWN_DESTROYED Event
   * @throws EventProcessingException when Event creation fails
   */
  public Event createCityShownDestroyedEvent(@Nonnull City city, @Nonnull Player player)
      throws EventProcessingException {
    if (city.getPlayer().equals(player)) {
      throw new EventProcessingException("Event CITY_SHOWN_DESTROYED can only be generated for Cities of a different "
          + "Player (" + city + ").");
    }
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.CITY_SHOWN_DESTROYED);
    final JsonObject body = bodyBuilder
        .add("city", objectSerializer.serializeCity(city))
        .add("player", objectSerializer.serializePlayer(player))
        .build();
    return new Event(body);
  }

  /**
   * Sets fields common for all Events in builder used to create an Event object.
   * @param builder JsonObjectBuilder used to create an Event object
   * @param type the Type of the Event
   * @throws EventProcessingException when setting common fields fails
   */
  private void setCommonEventFields(@Nonnull JsonObjectBuilder builder, @Nonnull Event.Type type)
      throws EventProcessingException {
    if (game.getTime() == -1) {
      throw new EventProcessingException("time not initialized.");
    }
    builder.add("type", type.name()).add("time", game.getTime());
  }

}
