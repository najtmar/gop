package cz.wie.najtmar.gop.game;

import cz.wie.najtmar.gop.board.Position;

import javax.annotation.Nonnull;

/**
 * Represents a time stamped move to a Position.
 * @author najtmar
 */
public class Move {

  /**
   * Position where to move.
   */
  private final Position position;

  /**
   * Time at which the move occurs.
   */
  private final long moveTime;

  /**
   * Indicates whether the Move is to stop. Only the last Move in a sequence can be to stop.
   */
  private final boolean stop;

  /**
   * Constructor.
   * @param position Move position
   * @param moveTime Move time
   * @param stop whether the Move is to stop a Prawn or not
   */
  public Move(@Nonnull Position position, long moveTime, boolean stop) {
    this.position = position;
    this.moveTime = moveTime;
    this.stop = stop;
  }

  public Position getPosition() {
    return position;
  }

  public long getMoveTime() {
    return moveTime;
  }

  public boolean isStop() {
    return stop;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Move)) {
      return false;
    }
    final Move otherMove = (Move) obj;
    return this.position.equals(otherMove.position) && this.moveTime == otherMove.moveTime
        && this.stop == otherMove.stop;
  }

  @Override
  public String toString() {
    return "(" + position + ", " + moveTime + ", " + stop + ")";
  }

}
