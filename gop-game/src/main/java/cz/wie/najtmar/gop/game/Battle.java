package cz.wie.najtmar.gop.game;

import com.google.gson.JsonObject;
import cz.wie.najtmar.gop.board.BoardException;
import cz.wie.najtmar.gop.board.Position;
import cz.wie.najtmar.gop.board.RoadSection;
import cz.wie.najtmar.gop.board.RoadSection.Direction;
import cz.wie.najtmar.gop.common.Constants;
import cz.wie.najtmar.gop.common.Persistable;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.Objects;

import static cz.wie.najtmar.gop.common.Persistable.*;

/**
 * Represents a Battle between 2 Prawns belonging to 2 Players. Hashed by Prawns and their Positions.
 * Two Battles are equal if Prawns and Positions are equal.
 * @author najtmar
 */
public class Battle implements Persistable {

  public static final String PRAWNS_PROPERTY = "prawns";
  public static final String NORMALIZED_POSITIONS_PROPERTY = "normalizedPositions";
  public static final String ATTACK_DIRECTIONS_PROPERTY = "attackDirections";
  public static final String HASH_CODE_PROPERTY = "hashCode";
  public static final String STATE_PROPERTY = "state";
  /**
   * 2 Prawns taking part in a Battle.
   */
  private final Prawn[] prawns;

  /**
   * 2 Positions on which respective Prawns taking part in the Battle are located. Normalization means that both
   * Positions are on the same RoadSection.
   */
  private final Position[] normalizedPositions;

  /**
   * 2 RoadSection.Directions for respective Prawn, which determine the Direction on the given RoadSection in which
   * the opponent's Prawn is located. For each Prawn, all attacks will be launched in that direction.
   */
  private final RoadSection.Direction[] attackDirections;

  /**
   * Unique hash code of the Battle.
   */
  private final int hashCode;

  /**
   * Describes the state of a Battle.
   * @author najtmar
   */
  public enum State {
    /**
     * Battle not yet started. Prawns not yet on their Battle Positions.
     */
    PLANNED,
    /**
     * Battle taking part right now. Prawns on their respective Battle positions.
     */
    RUNNING,
    /**
     * Battle finished.
     */
    FINISHED,
  }

  /**
   * State of the Battle.
   */
  private State state;

  /**
   * Constructor.
   * @param prawn0 first Prawn taking part in the Battle
   * @param currentPosition0 current Position of prawn0
   * @param nextPosition0 next Position of prawn0
   * @param prawn1 second Prawn taking part in the Battle
   * @param currentPosition1 current Position of prawn1
   * @param nextPosition1 next Position of prawn1
   * @throws GameException when the Battle cannot be created
   * @throws BoardException when the Battle cannot be created
   */
  public Battle(@Nonnull Prawn prawn0, @Nonnull Position currentPosition0, Position nextPosition0,
      @Nonnull Prawn prawn1, @Nonnull Position currentPosition1, Position nextPosition1)
          throws BoardException, GameException {
    if (!allBattleConditionsMet(prawn0, currentPosition0, nextPosition0, prawn1, currentPosition1, nextPosition1)) {
      throw new GameException("Battle conditions between Prawns " + prawn0 + " at " + currentPosition0 + "->"
          + nextPosition0 + " and " + prawn1 + " at " + currentPosition1 + "->" + nextPosition1 + " are not met.");
    }

    // Normalize the Prawn order based on their IDs.
    if (prawn0.getId() < prawn1.getId()) {
      this.prawns = new Prawn[]{prawn0, prawn1};
      this.normalizedPositions = new Position[]{currentPosition0, currentPosition1};
    } else {
      this.prawns = new Prawn[]{prawn1, prawn0};
      this.normalizedPositions = new Position[]{currentPosition1, currentPosition0};
    }

    // Normalize the Positions based on their RoadSection indexes.
    if (!this.normalizedPositions[0].getRoadSection().equals(this.normalizedPositions[1].getRoadSection())) {
      final Position projectedPosition0 =
          this.normalizedPositions[1].getRoadSection().getProjectedPosition(this.normalizedPositions[0]);
      final Position projectedPosition1 =
          this.normalizedPositions[0].getRoadSection().getProjectedPosition(this.normalizedPositions[1]);
      if ((projectedPosition0 == null) && (projectedPosition1 == null)
          || (projectedPosition0 != null) && (projectedPosition1 != null)) {
        throw new GameException("Exactly one adjacent Position should be possible to be projected to the other "
            + "Position's RoadSection (found " + projectedPosition0 + " and " + projectedPosition1 + ").");
      }
      if (projectedPosition0 != null) {
        this.normalizedPositions[0] = projectedPosition0;
      } else {
        this.normalizedPositions[1] = projectedPosition1;
      }
    }

    // Determine attack directions.
    if (this.normalizedPositions[0].getIndex() < this.normalizedPositions[1].getIndex()) {
      this.attackDirections =
          new RoadSection.Direction[]{RoadSection.Direction.FORWARD, RoadSection.Direction.BACKWARD};
    } else {
      this.attackDirections =
          new RoadSection.Direction[]{RoadSection.Direction.BACKWARD, RoadSection.Direction.FORWARD};
    }

    this.hashCode = Objects.hash(prawns[0].hashCode(), prawns[1].hashCode(), normalizedPositions[0].hashCode(),
        normalizedPositions[1].hashCode());
    this.state = State.PLANNED;
  }

  public Battle(JsonObject serialized, Game game) {
    this.prawns = deserializeStream(serialized.getAsJsonArray(PRAWNS_PROPERTY), el -> game.lookupPrawnById(el.getAsInt(), true))
            .toArray(Prawn[]::new);
    this.normalizedPositions = deserializeStream(serialized.getAsJsonArray(NORMALIZED_POSITIONS_PROPERTY), el -> new Position(el.getAsJsonObject(), game.getBoard()))
            .toArray(Position[]::new);
    this.attackDirections = deserializeStream(serialized.getAsJsonArray(ATTACK_DIRECTIONS_PROPERTY), el -> RoadSection.Direction.valueOf(el.getAsString()))
            .toArray(RoadSection.Direction[]::new);
    this.hashCode = serialized.getAsJsonPrimitive(HASH_CODE_PROPERTY).getAsInt();
    this.state = Battle.State.valueOf(serialized.getAsJsonPrimitive(STATE_PROPERTY).getAsString());
  }

  public Prawn[] getPrawns() {
    return prawns;
  }

  public Position[] getNormalizedPositions() {
    return normalizedPositions;
  }

  public RoadSection.Direction[] getAttackDirections() {
    return attackDirections;
  }

  public State getState() {
    return state;
  }

  /**
   * Returns the opponent of prawn in the Battle. Throws a GameException is prawn is not involved in the Battle.
   * @param prawn the Prawn to look up for opponent for
   * @return the opponent of prawn in the Battle
   * @throws GameException if prawn is not involved in the Battle
   */
  public Prawn getOpponent(@Nonnull Prawn prawn) throws GameException {
    if (prawns[0].equals(prawn)) {
      return prawns[1];
    }
    if (prawns[1].equals(prawn)) {
      return prawns[0];
    }
    throw new GameException("Prawn " + prawn + " not involved in " + this + " .");
  }

  /**
   * Checks whether all Battle conditions between prawn0 and prawn1 are met:
   * 1. prawn0 and prawn1 are owned by two different Players.
   * 2. currentPosition0 and currentPosition1 are adjacent.
   * 3. At least one of the Prawns is a Warrior.
   * 4. Move direction of at least one of the Warriors (nextPositionX) is toward the other Prawn (currentPositionY).
   * 5. Warrior direction of at least one of the Warriors is toward the other Prawn (the attack is effective).
   * @param prawn0 first Prawn to examine
   * @param currentPosition0 current Position of prawn0
   * @param nextPosition0 next Position of prawn0
   * @param currentPosition1 current Position of prawn1
   * @param nextPosition1 next Position of prawn1
   * @param prawn1 second Prawn to examine
   * @return true iff all Battle conditions between prawn0 and prawn1 are met
   * @throws BoardException when examination cannot be performed
   */
  public static boolean allBattleConditionsMet(@Nonnull Prawn prawn0, @Nonnull Position currentPosition0,
      Position nextPosition0, @Nonnull Prawn prawn1, @Nonnull Position currentPosition1, Position nextPosition1)
          throws BoardException {
    // 1.
    if (prawn0.getPlayer().equals(prawn1.getPlayer())) {
      return false;
    }

    // 2.
    if (!currentPosition0.adjacentTo(currentPosition1)) {
      return false;
    }

    // 3.
    if (!(prawn0 instanceof Warrior) && !(prawn1 instanceof Warrior)) {
      return false;
    }

    // 4.
    if ((!(prawn0 instanceof Warrior) || nextPosition0 == null ||  !nextPosition0.equals(currentPosition1))
        && (!(prawn1 instanceof Warrior) || nextPosition1 == null || !nextPosition1.equals(currentPosition0))) {
      return false;
    }

    // 5.
    boolean isAttackEffective = false;
    final RoadSection roadSection = Position.findCommonRoadSection(currentPosition0, currentPosition1);
    if (roadSection != null) {
      final Position projectedCurrentPosition0 = roadSection.getProjectedPosition(currentPosition0);
      final Position projectedCurrentPosition1 = roadSection.getProjectedPosition(currentPosition1);
      final Direction prawn0Direction;
      final Direction prawn1Direction;
      if (projectedCurrentPosition0.getIndex() < projectedCurrentPosition1.getIndex()) {
        prawn0Direction = Direction.FORWARD;
        prawn1Direction = Direction.BACKWARD;
      } else {
        prawn0Direction = Direction.BACKWARD;
        prawn1Direction = Direction.FORWARD;
      }
      if (prawn0 instanceof Warrior) {
        final Warrior warrior0 = (Warrior) prawn0;
        if (roadSection.calculateDirContrib(warrior0.getWarriorDirection(), prawn0Direction) > Constants.DOUBLE_DELTA) {
          isAttackEffective = true;
        }
      }
      if (!isAttackEffective && (prawn1 instanceof Warrior)) {
        final Warrior warrior1 = (Warrior) prawn1;
        if (roadSection.calculateDirContrib(warrior1.getWarriorDirection(), prawn1Direction) > Constants.DOUBLE_DELTA) {
          isAttackEffective = true;
        }
      }
    }
    if (!isAttackEffective) {
      return false;
    }

    return true;
  }

  /**
   * Changes Battle State from PLANNED to RUNNING. Checks if Battle conditions are met for the current Prawn positions.
   * @throws GameException when the Battle cannot start
   * @throws BoardException when Battle conditions evaluation cannot be performed
   */
  public void start() throws GameException, BoardException {
    if (state != State.PLANNED) {
      throw new GameException("Battle state must be PLANNED (found " + state + ").");
    }
    if (!normalizedPositions[0].equals(prawns[0].getCurrentPosition())
        || !normalizedPositions[1].equals(prawns[1].getCurrentPosition())) {
      throw new GameException("Prawn Positions have changed. Expected: (" + normalizedPositions[0] + ", "
          + normalizedPositions[1] + "), actual: (" + prawns[0].getCurrentPosition() + ", "
          + prawns[1].getCurrentPosition() + ").");
    }
    state = State.RUNNING;
  }

  /**
   * Changes Battle State from RUNNING to FINISHED. Checks if Battle conditions are met for the current Prawn positions.
   * @throws GameException when the Battle cannot be finished
   * @throws BoardException when Battle conditions evaluation cannot be performed
   */
  public void finish() throws GameException, BoardException {
    if (state != State.RUNNING) {
      throw new GameException("Battle state must be RUNNING (found " + state + ").");
    }
    if (!normalizedPositions[0].equals(prawns[0].getCurrentPosition())
        || !normalizedPositions[1].equals(prawns[1].getCurrentPosition())) {
      throw new GameException("Prawn Positions have changed. Expected: (" + normalizedPositions[0] + ", "
          + normalizedPositions[1] + "), actual: (" + prawns[0].getCurrentPosition() + ", "
          + prawns[1].getCurrentPosition() + ").");
    }
    state = State.FINISHED;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Battle)) {
      return false;
    }
    final Battle otherBattle = (Battle) obj;
    return this.prawns[0].equals(otherBattle.prawns[0])
        && this.prawns[1].equals(otherBattle.prawns[1])
        && this.normalizedPositions[0].equals(otherBattle.normalizedPositions[0])
        && this.normalizedPositions[1].equals(otherBattle.normalizedPositions[1]);
  }

  @Override
  public int hashCode() {
    return hashCode;
  }

  @Override
  public String toString() {
    return "Battle [prawns=" + Arrays.toString(prawns) + ", normalizedPositions="
        + Arrays.toString(normalizedPositions) + ", state=" + state + "]";
  }

  @Override
  public JsonObject snapshot() {
    JsonObject returnValue = new JsonObject();
    returnValue.add(PRAWNS_PROPERTY, serializeArrayOfObjects(prawns, Prawn::getId));
    returnValue.add(NORMALIZED_POSITIONS_PROPERTY, serializeArray(normalizedPositions));
    returnValue.add(ATTACK_DIRECTIONS_PROPERTY, serializeArrayOfObjects(attackDirections, RoadSection.Direction::name));
    returnValue.addProperty(HASH_CODE_PROPERTY, hashCode);
    returnValue.addProperty(STATE_PROPERTY, serializeNullable(state, State::name));
    return returnValue;
  }

}
