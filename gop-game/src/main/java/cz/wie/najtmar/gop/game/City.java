package cz.wie.najtmar.gop.game;

import com.google.gson.JsonObject;
import cz.wie.najtmar.gop.board.BoardException;
import cz.wie.najtmar.gop.board.Joint;
import cz.wie.najtmar.gop.board.Position;
import cz.wie.najtmar.gop.board.RoadSection;
import cz.wie.najtmar.gop.board.RoadSection.Direction;
import cz.wie.najtmar.gop.common.Persistable;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;

import static cz.wie.najtmar.gop.common.Persistable.serializeArray;

/**
 * Represents a City. Hashed by unique id.
 * @author najtmar
 */
public class City implements Comparable<City>, Persistable {

  public static final String NAME_PROPERTY = "name";
  public static final String ID_PROPERTY = "id";
  public static final String JOINT_PROPERTY = "joint";
  public static final String FACTORIES_PROPERTY = "factories";
  public static final String PLAYER_PROPERTY = "player";
  public static final String SIZE_PROPERTY = "size";
  /**
   * Game in which the City takes part.
   */
  private final Game game;

  /**
   * The name of the City.
   */
  private String name;

  /**
   * Unique ID of the City.
   */
  private final int id;

  /**
   * Joint on which the City is located.
   */
  private final Joint joint;

  /**
   * The list of all Factories that the City may ever have. The number of Factories is equal to the number of
   * RoadSections adjacent to the Joint on which the City is located. The number of ENABLED Factories is equal to
   * the size of the City. All factories with indexes higher than the index of a DISABLED CityFactory are also DISABLED.
   */
  private final CityFactory[] factories;

  /**
   * Player to which the City belongs.
   */
  private Player player;

  /**
   * The size of the City.
   */
  @Nonnegative private int size;

  public City(JsonObject serialized, Game game) {
    this.game = game;
    this.name = serialized.getAsJsonPrimitive(NAME_PROPERTY).getAsString();
    this.id = serialized.getAsJsonPrimitive(ID_PROPERTY).getAsInt();
    City.firstAvailableId = Integer.max(City.firstAvailableId, this.id + 1);
    this.joint = this.game.getBoard().getJoints().get(serialized.getAsJsonPrimitive(JOINT_PROPERTY).getAsInt());
    this.factories = Persistable.deserializeStream(serialized.getAsJsonArray(FACTORIES_PROPERTY), el -> new CityFactory(el.getAsJsonObject(), this))
            .toArray(CityFactory[]::new);
    this.player = game.getPlayers().get(serialized.getAsJsonPrimitive(PLAYER_PROPERTY).getAsInt());
    this.size = serialized.getAsJsonPrimitive(SIZE_PROPERTY).getAsInt();
  }

  /**
   * First ID that is available for assignment to Cities.
   */
  static int firstAvailableId;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Game getGame() {
    return game;
  }

  public Player getPlayer() {
    return player;
  }

  public int getId() {
    return id;
  }

  public Joint getJoint() {
    return joint;
  }

  /**
   * Returns City CityFactory with a given index.
   * @param index the index of the CityFactory to return
   * @return the factory with a given index
   */
  CityFactory getFactory(@Nonnegative int index) {
    return factories[index];
  }

  public int getSize() {
    return size;
  }

  /**
   * Generates incremental unique ID and increases the internal ID count.
   * @return incremental unique ID
   */
  private static synchronized int generateUniqueIncrementalId() {
    return firstAvailableId++;
  }

  /**
   * Resets firstAvailableId to 0. Used in tests.
   */
  public static synchronized void resetFirstAvailableId() {
    firstAvailableId = 0;
  }

  /**
   * Increases the size of a City by 1.
   * @throws GameException if the City cannot grow
   */
  public void grow() throws GameException {
    if (size == 0) {
      throw new GameException("Destroyed cities (of size 0) cannot grow.");
    }
    final int cityJointAdjacencyLevel = joint.getRoadSections().size();
    if (size >= cityJointAdjacencyLevel) {
      throw new GameException("Only a City of size smaller than the number of adjacent RoadSections can further grow "
          + "(" + size + " >= " + cityJointAdjacencyLevel + ").");
    }
    factories[size].enable();
    ++size;
  }

  /**
   * Implementation of shrinking or destroying the City, used in public methods.
   * @param index the index of the Factory to remove
   * @throws GameException if the City cannot shrink or be destroyed
   */
  private void doShrinkOrDestroy(@Nonnegative int index) throws GameException {
    final CityFactory removedCityFactory = factories[index];
    removedCityFactory.disable();
    int currentIndex = index;
    while (currentIndex < size - 1) {
      factories[currentIndex] = factories[currentIndex + 1];
      factories[currentIndex].setIndex(currentIndex);
      ++currentIndex;
    }
    factories[currentIndex] = removedCityFactory;
    factories[currentIndex].setIndex(currentIndex);
    --size;
    if (size == 0) {
      game.destroyCity(this);
    }
  }

  /**
   * Decreases the size of the City by 1. Destroys the City if the result size is 0.
   * @param index the index of the Factory to remove
   * @throws GameException if the City cannot shrink or be destroyed
   */
  void shrinkOrDestroy(@Nonnegative int index) throws GameException {
    if (size == 0) {
      throw new GameException("Destroyed cities (of size 0) cannot further shrink.");
    }
    doShrinkOrDestroy(index);
  }

  /**
   * Calculates the size of the cultivable area for the City. One Road half-section counts as 0.5 .
   * @return the size of the cultivable area for the City
   * @throws GameException when the cultivable area cannot be calculated
   */
  @Nonnegative
  double calculateCultivableArea() throws GameException {
    double result = 0.0;
    roadSectionLoop:
    for (RoadSection roadSection : joint.getRoadSections()) {
      double cultivableAreaContribution = 1.0;
      final RoadSection.Direction cityDirection = roadSection.getJointDirection(joint);

      // Check the opponents' Prawns.
      for (Player opponent : game.getPlayers()) {
        if (!opponent.equals(player)) {
          opponentPrawnLoop:
          for (Prawn opponentPrawn : game.getPlayerPrawns(opponent)) {
            Position opponentPrawnPosition;
            try {
              opponentPrawnPosition = roadSection.getProjectedPosition(opponentPrawn.getCurrentPosition());
            } catch (BoardException exception) {
              throw new GameException("Cannot project " + opponentPrawn.getCurrentPosition() +  "  on " + roadSection,
                  exception);
            }
            if (opponentPrawnPosition == null) {
              continue opponentPrawnLoop;
            }
            if ((cityDirection.equals(Direction.BACKWARD)
                 && opponentPrawnPosition.getIndex() <= roadSection.getMidpointIndex())
                || (cityDirection.equals(Direction.FORWARD)
                    && roadSection.getMidpointIndex() <= opponentPrawnPosition.getIndex())) {
              // The whole RoadSection is captured.
              continue roadSectionLoop;
            } else {
              cultivableAreaContribution = 0.5;
            }
          }
        }
      }

      if (cultivableAreaContribution == 0.5) {
        // Skipping City checking, as it would not change the contribution.
        result += 0.5;
        continue roadSectionLoop;
      }

      // Check other cities.
      for (Player anyPlayer : game.getPlayers()) {
        for (City otherCity : game.getPlayerCities(anyPlayer)) {
          if (!otherCity.equals(this)) {
            if (roadSection.adjacentTo(otherCity.getJoint())) {
              // Skipping further checking, as it would not change the contribution.
              result += 0.5;
              continue roadSectionLoop;
            }
          }
        }
      }

      if (cultivableAreaContribution != 1.0) {
        throw new GameException("Unexpected cultivableAreaContribution value: " + cultivableAreaContribution);
      }
      result += 1.0;
    }

    return result;
  }

  /**
   * Make newPlayer capture the City, if the size of the City is greater than 1. Otherwise, destroy the city.
   * @param newPlayer the Player to capture the City
   * @throws GameException when the City cannot be captured by player or destroyed
   */
  public void captureOrDestroy(@Nonnull Player newPlayer) throws GameException {
    if (size == 0) {
      throw new GameException("Destroyed cities (of size 0) cannot be captured.");
    }
    final Player oldPlayer = this.player;
    if (oldPlayer.equals(newPlayer)) {
      throw new GameException("City " + this + " already owned by Player " + newPlayer + " .");
    }
    doShrinkOrDestroy(size - 1);
    this.player = newPlayer;
    if (size >= 1) {
      for (int i = 0; i < size; ++i) {
        factories[i].setAction(null);
      }
      game.captureCity(this, oldPlayer, newPlayer);
    }
  }

  @Override
  public int hashCode() {
    return id;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof City)) {
      return false;
    }
    final City otherCity = (City) obj;
    return this.id == otherCity.id;
  }

  @Override
  public int compareTo(City otherCity) {
    return Integer.compare(id,  otherCity.id);
  }

  @Override
  public String toString() {
    return "City [name=" + name + ", id=" + id + ", joint=" + joint + ", player=" + player
        + ", size=" + size + "]";
  }

  @Override
  public JsonObject snapshot() {
    JsonObject returnValue = new JsonObject();
    returnValue.addProperty(NAME_PROPERTY, name);
    returnValue.addProperty(ID_PROPERTY, id);
    returnValue.addProperty(JOINT_PROPERTY, joint.getIndex());
    returnValue.add(FACTORIES_PROPERTY, serializeArray(factories));
    returnValue.addProperty(PLAYER_PROPERTY, player.getIndex());
    returnValue.addProperty(SIZE_PROPERTY, size);
    return returnValue;
  }

  public static class Builder {
    private Game game;
    private String name;
    private Player player;
    private Joint joint;

    public Builder setGame(Game game) {
      this.game = game;
      return this;
    }

    public Builder setName(String name) {
      this.name = name;
      return this;
    }

    public Builder setPlayer(Player player) {
      this.player = player;
      return this;
    }

    public Builder setJoint(Joint joint) {
      this.joint = joint;
      return this;
    }

    /**
     * Builds an instance of City.
     * @return build instance of City
     * @throws GameException when creation of City fails
     */
    public City build() throws GameException {
      if (game == null) {
        throw new GameException("Game not set.");
      }
      if (player == null) {
        throw new GameException("Player not set.");
      }
      if (name == null) {
        throw new GameException("Name not set.");
      }
      if (joint == null) {
        throw new GameException("Joint not set.");
      }
      return new City(game, player, name, joint);
    }
  }

  private City(@Nonnull Game game, @Nonnull Player player, @Nonnull String name, @Nonnull Joint joint)
      throws GameException {
    this.game = game;
    this.player = player;
    this.name = name;
    this.joint = joint;
    this.id = generateUniqueIncrementalId();
    this.size = 1;
    final int adjacentRoadSectionNumber = joint.getRoadSections().size();
    if (adjacentRoadSectionNumber == 0) {
      throw new GameException("The number of RoadSections adjacent to a City must be greater than zero.");
    }
    this.factories = new CityFactory[adjacentRoadSectionNumber];
    for (int i = 0; i < this.factories.length; ++i) {
      this.factories[i] = new CityFactory(this, i);
    }
    this.factories[0].enable();

    this.game.addCity(this);
  }

}
