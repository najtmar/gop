package cz.wie.najtmar.gop.board;

import com.google.gson.JsonObject;
import cz.wie.najtmar.gop.common.Persistable;

import javax.annotation.Nonnull;
import java.util.*;

import static cz.wie.najtmar.gop.common.Persistable.*;
import static cz.wie.najtmar.gop.common.Persistable.serializeNullable;

/**
 * Represents a Position on a RoadSection. Hashed by the RoadSection and index. It is guaranteed that two equal
 * Positions have the same hashCode.
 * @author najtmar
 */
public class Position implements Persistable {

  public static final String ROAD_SECTION_PROPERTY = "roadSection";
  public static final String INDEX_PROPERTY = "index";
  public static final String JOINT_PROPERTY = "joint";
  public static final String HASH_CODE_PROPERTY = "hashCode";
  /**
   * RoadSection to which the position belongs.
   * NOTE: If a Position is a Joint, then it may belong to more than one RoadSection. However, a given Position is
   * defined in the context of this particular position (the index).
   */
  private final RoadSection roadSection;

  /**
   * The index of Position on roadSection.
   */
  private final int index;

  /**
   * The Joint to which the Position corresponds or null if the position does not correspond to any Joint .
   */
  private final Joint joint;

  /**
   * Unique hash code.
   */
  private final int hashCode;

  /**
   * Constructor.
   * @param roadSection RoadSection for the Position
   * @param index index of Position on roadSection
   * @throws BoardException when Position is outside roadSection
   */
  public Position(@Nonnull RoadSection roadSection, int index) throws BoardException {
    this.roadSection = roadSection;
    if (index < 0 || index >= roadSection.getPositionsCount()) {
      throw new BoardException("index must be between 0 and " + (roadSection.getPositionsCount() - 1)
          + " (found " + index + ").");
    }
    this.index = index;

    final int[] jointPositionIndexes = roadSection.getJointPositionIndexes();
    if (index == jointPositionIndexes[0]) {
      this.joint = roadSection.getJoints()[0];
    } else if (index == jointPositionIndexes[1]) {
      this.joint = roadSection.getJoints()[1];
    } else {
      this.joint = null;
    }

    if (this.joint == null) {
      this.hashCode = Objects.hash(roadSection.hashCode(), this.index);
    } else {
      this.hashCode = Objects.hash(-this.joint.hashCode() - 1, 0);
    }
  }

  public Position(JsonObject serialized, Board board) {
    this.roadSection = board.getRoadSections().get(serialized.getAsJsonPrimitive(ROAD_SECTION_PROPERTY).getAsInt());
    this.index = serialized.getAsJsonPrimitive(INDEX_PROPERTY).getAsInt();
    this.joint = deserializeNullable(serialized, JOINT_PROPERTY, JsonObject::getAsJsonPrimitive)
            .map(el -> board.getJoints().get(el.getAsInt()))
            .orElse(null);
    this.hashCode = serialized.getAsJsonPrimitive(HASH_CODE_PROPERTY).getAsInt();
  }

  public RoadSection getRoadSection() {
    return roadSection;
  }

  public int getIndex() {
    return index;
  }

  public Joint getJoint() {
    return joint;
  }

  /**
   * Finds the Set of all RoadSection.Halves to which the Position belongs.
   * @return the Set of all RoadSection.Halves to which the Position belongs
   */
  public Set<RoadSection.Half> findRoadSectionHalves() {
    final Set<RoadSection> adjacentRoadSections = new HashSet<>();
    adjacentRoadSections.add(roadSection);
    if (this.joint != null) {
      for (RoadSection adjacentRoadSection : this.joint.getRoadSections()) {
        adjacentRoadSections.add(adjacentRoadSection);
      }
    }
    final Set<RoadSection.Half> result = new HashSet<>();
    for (RoadSection adjacentRoadSection : adjacentRoadSections) {
      for (int i = 0; i < 2; ++i) {
        if (adjacentRoadSection.getHalves()[i].contains(this)) {
          result.add(adjacentRoadSection.getHalves()[i]);
        }
      }
    }
    return result;
  }

  /**
   * Finds the Set of all RoadSection.Halves that a Prawn or a City located at the given Position makes visible.
   * @return the Set of all RoadSection.Halves that a Prawn or a City located at the given Position makes visible
   */
  public Set<RoadSection.Half> findVisibleRoadSectionHalves() {
    final Set<RoadSection.Half> result = new HashSet<>();
    final Set<RoadSection.Half> roadSectionHalves = findRoadSectionHalves();
    result.addAll(roadSectionHalves);
    for (RoadSection.Half roadSectionHalf : roadSectionHalves) {
      result.addAll(roadSectionHalf.getAdjacentHalves());
    }
    return result;
  }

  /**
   * NOTE: All Positions on a Joint are considered to be the same Position, irrespective of the RoadSection to which
   * they belong.
   */
  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Position)) {
      return false;
    }
    final Position otherPosition = (Position) obj;
    if (this.joint != null) {
      return this.joint.equals(otherPosition.joint);
    }
    return roadSection.equals(otherPosition.roadSection) && index == otherPosition.index;
  }

  /**
   * Checks whether position is adjacent to the current Position.
   * @param position the Position compared with the current Position
   * @return true iff position is adjacent to the current Position
   * @throws BoardException if comparison cannot be performed
   */
  public boolean adjacentTo(@Nonnull Position position) throws BoardException {
    if (this.roadSection.getIndex() == position.roadSection.getIndex()) {
      return Math.abs(this.index - position.index) == 1;
    }

    // Inter-RoadSection comparison.
    Joint comparedJoint;
    RoadSection comparedRoadSection;
    int comparedIndex;
    if (position.getJoint() != null) {
      comparedJoint = position.getJoint();
      comparedRoadSection = this.roadSection;
      comparedIndex = this.index;
    } else if (this.getJoint() != null) {
      comparedJoint = this.getJoint();
      comparedRoadSection = position.roadSection;
      comparedIndex = position.index;
    } else {
      return false;
    }
    if (!comparedRoadSection.adjacentTo(comparedJoint)) {
      return false;
    }
    return Math.abs(comparedIndex - comparedRoadSection.getJointIndex(comparedJoint)) == 1;
  }

  /**
   * Looks up a RoadSection on which both Positions are located.
   * @param position0 the first of the Positions for which a common RoadSection is looked up
   * @param position1 the second of the Positions for which a common RoadSection is looked up
   * @return a RoadSection on which both Positions are located, or null if no such RoadSection exists
   */
  public static RoadSection findCommonRoadSection(@Nonnull Position position0, @Nonnull Position position1) {
    final Set<RoadSection> position0RoadSections;
    if (position0.getJoint() == null) {
      position0RoadSections = new HashSet<>(Arrays.asList(position0.getRoadSection()));
    } else {
      position0RoadSections = new HashSet<>(position0.getJoint().getRoadSections());
    }
    final Set<RoadSection> position1RoadSections;
    if (position1.getJoint() == null) {
      position1RoadSections = new HashSet<>(Arrays.asList(position1.getRoadSection()));
    } else {
      position1RoadSections = new HashSet<>(position1.getJoint().getRoadSections());
    }
    final Set<RoadSection> commonRoadSections = position0RoadSections;
    commonRoadSections.retainAll(position1RoadSections);
    if (commonRoadSections.isEmpty()) {
      return null;
    }
    return commonRoadSections.iterator().next();
  }

  @Override
  public int hashCode() {
    return hashCode;
  }

  @Override
  public String toString() {
    return "(" + roadSection.toString() + ", " + index + ")";
  }

  @Override
  public JsonObject snapshot() {
    JsonObject returnValue = new JsonObject();
    returnValue.addProperty(ROAD_SECTION_PROPERTY, roadSection.getIndex());
    returnValue.addProperty(INDEX_PROPERTY, index);
    returnValue.addProperty(JOINT_PROPERTY, serializeNullable(joint, Joint::getIndex));
    returnValue.addProperty(HASH_CODE_PROPERTY, hashCode);
    return returnValue;
  }
}
