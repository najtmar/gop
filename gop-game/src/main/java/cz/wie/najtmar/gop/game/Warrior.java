package cz.wie.najtmar.gop.game;

import com.google.gson.JsonObject;
import cz.wie.najtmar.gop.board.DirectedPosition;
import cz.wie.najtmar.gop.board.Position;
import cz.wie.najtmar.gop.board.RoadSection;
import cz.wie.najtmar.gop.common.Persistable;
import lombok.NoArgsConstructor;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;

import static cz.wie.najtmar.gop.common.Persistable.typeOf;

/**
 * Represents a Warrior.
 * @author najtmar
 */
public class Warrior extends Prawn {

  public static final String WARRIOR_DIRECTION_PROPERTY = "warriorDirection";
  /**
   * Angle in radians [0.0, 2.0 * Math.PI) denoting Warrior's warriorDirection.
   */
  private final double warriorDirection;

  public double getWarriorDirection() {
    return warriorDirection;
  }

  /**
   * Warrior velocity is calculated based on Warrior direction.
   */
  @Override
  protected double calculateVelocity(@Nonnull DirectedPosition directedPosition) {
    final GameProperties gameProperties = getGameProperties();
    final Position position = directedPosition.getPosition();
    final RoadSection roadSection = position.getRoadSection();
    final RoadSection.Direction direction = directedPosition.getDirection();
    return gameProperties.getBasicVelocity() + gameProperties.getVelocityDirContribFactor()
        * roadSection.calculateDirContrib(warriorDirection, direction);
  }

  @Override
  public double calculateAttackStrength(DirectedPosition directedPosition) throws GameException {
    final GameProperties gameProperties = getGameProperties();
    final Position position = directedPosition.getPosition();
    if (!position.equals(getCurrentPosition())) {
      throw new GameException("Attack strength must be calculated at the current Position of the Prawn (" + position
          + " != " + getCurrentPosition() + ").");
    }
    final RoadSection roadSection = position.getRoadSection();
    final RoadSection.Direction direction = directedPosition.getDirection();
    final double dirContrib = roadSection.calculateDirContrib(warriorDirection, direction);
    return gameProperties.getMaxAttackStrength() * dirContrib;
  }

  /**
   * Super-class implementation should be sufficient. This one only does some additional checking.
   */
  @Override
  public boolean equals(Object obj) {
    if (!super.equals(obj)) {
      return false;
    }
    if (!(obj instanceof Warrior)) {
      throw new RuntimeException("This should never happen.");
    }
    final Warrior warrior = (Warrior) obj;
    if (this.warriorDirection != warrior.warriorDirection) {
      throw new RuntimeException("This should never happen.");
    }
    return true;
  }

  @Override
  public String toString() {
    return "(WARRIOR, " + super.toString() + ", " + warriorDirection + ")";
  }

  @NoArgsConstructor
  public static class Builder implements Persistable {
    public static final String CURRENT_POSITION_PROPERTY = "currentPosition";
    public static final String DIRECTION_PROPERTY = "direction";
    public static final String CREATION_TIME_PROPERTY = "creationTime";

    private GameProperties gameProperties;
    private Player player;
    private Position currentPosition;
    private double direction = -1.0;
    private long creationTime = -1;

    public Builder(JsonObject serialized, Player player) {
      this.gameProperties = player.getGame().getGameProperties();
      this.player = player;
      this.currentPosition = new Position(serialized.getAsJsonObject(CURRENT_POSITION_PROPERTY), player.getGame().getBoard());
      this.direction = serialized.getAsJsonPrimitive(DIRECTION_PROPERTY).getAsDouble();
      this.creationTime = serialized.getAsJsonPrimitive(CREATION_TIME_PROPERTY).getAsLong();
    }

    public Builder setGameProperties(GameProperties gameProperties) {
      this.gameProperties = gameProperties;
      return this;
    }

    public Builder setPlayer(Player player) {
      this.player = player;
      return this;
    }

    public Builder setDirection(double direction) {
      this.direction = direction;
      return this;
    }

    public double getDirection() {
      return direction;
    }

    public Builder setCurrentPosition(Position currentPosition) {
      this.currentPosition = currentPosition;
      return this;
    }

    public long getCreationTime() {
      return creationTime;
    }

    public Builder setCreationTime(long creationTime) {
      this.creationTime = creationTime;
      return this;
    }

    /**
     * Builds an instance of Warrior.
     * @return build instance of Warrior
     * @throws GameException when creation of Warrior fails
     */
    public Warrior build() throws GameException {
      if (gameProperties == null) {
        throw new GameException("GameProperties not set.");
      }
      if (player == null) {
        throw new GameException("Player not set.");
      }
      if (currentPosition == null) {
        throw new GameException("currentPosition not set.");
      }
      if (direction == -1.0) {
        throw new GameException("Direction not set.");
      }
      if (creationTime == -1) {
        throw new GameException("Creation time not set.");
      }
      return new Warrior(gameProperties, player, currentPosition, direction, creationTime);
    }

    @Override
    public JsonObject snapshot() {
      JsonObject resultValue = new JsonObject();
      resultValue.add(CURRENT_POSITION_PROPERTY, currentPosition.snapshot());
      resultValue.addProperty(DIRECTION_PROPERTY, direction);
      resultValue.addProperty(CREATION_TIME_PROPERTY, creationTime);
      return resultValue;
    }
  }

  private Warrior(@Nonnull GameProperties gameProperties, @Nonnull Player player, @Nonnull Position currentPosition,
      double direction, @Nonnegative long creationTime) throws GameException {
    super(gameProperties, player, currentPosition, creationTime);
    if (direction < 0.0 || direction >= 2.0 * Math.PI) {
      throw new GameException("warriorDirection must be from interval [0, 2*pi) (found " + direction + ").");
    }
    this.warriorDirection = direction;
  }

  public Warrior(JsonObject serialized, Game game) {
    super(serialized, game);
    this.warriorDirection = serialized.getAsJsonPrimitive(WARRIOR_DIRECTION_PROPERTY).getAsDouble();
  }

  @Override
  public JsonObject snapshot() {
    JsonObject returnValue = super.snapshot();
    returnValue.addProperty(TYPE_PROPERTY, typeOf(this.getClass()));
    returnValue.addProperty(WARRIOR_DIRECTION_PROPERTY, warriorDirection);
    return returnValue;
  }
}
