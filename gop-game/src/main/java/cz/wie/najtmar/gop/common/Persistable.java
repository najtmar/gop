package cz.wie.najtmar.gop.common;

import com.google.gson.*;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public interface Persistable {

    String TYPE_PROPERTY = "type";

    Gson GSON = new GsonBuilder()
            .disableHtmlEscaping()
            .setPrettyPrinting()
            .serializeNulls()
            .registerTypeHierarchyAdapter(Properties.class, propertiesSerializer())
            .create();

    static JsonElement serializeObject(Object input) {
        return GSON.toJsonTree(input);
    }

    static JsonSerializer<Properties> propertiesSerializer() {
        return (src, typeOfSrc, context) -> GSON.toJsonTree(src.entrySet().stream()
                    .collect(
                            TreeMap::new,
                            (Map<Object, Object> map, Map.Entry<Object, Object> el) -> map.put(
                                    el.getKey(),
                                    el.getValue()),
                            Map<Object, Object>::putAll));
    }

    static <T>T deserialize(JsonElement serialized, Class<T> cls) {
        return GSON.fromJson(serialized, cls);
    }

    static <K, V, S, T> JsonElement serializeMapOfSets(
            Map<K, Set<V>> input,
            Function<K, S> serializeKey,
            Function<V, T> serializeValueElement) {
        return Optional.ofNullable(input)
                .map(inputMap -> inputMap.entrySet().stream()
                        .collect(
                                HashMap::new,
                                (Map<S, Set<T>> map, Map.Entry<K, Set<V>> el) -> map.put(serializeKey.apply(el.getKey()),
                                        el.getValue().stream()
                                                .map(serializeValueElement)
                                                .collect(Collectors.<T>toSet())),
                                Map<S, Set<T>>::putAll))
                .map(GSON::toJsonTree)
                .orElse(null);
    }

    static <K, V> HashMap<K, Set<V>> deserializeMapOfSets(
            JsonObject input,
            Function<String, K> deserializeKey,
            Function<JsonElement, V> deserializeValueElement) {
        return Optional.ofNullable(input)
                .map(inputMap -> inputMap.entrySet().stream()
                        .collect(
                                HashMap::new,
                                (HashMap<K, Set<V>> map, Map.Entry<String, JsonElement> el) -> map.put(deserializeKey.apply(el.getKey()),
                                        asStream(el.getValue().getAsJsonArray().iterator())
                                                .map(deserializeValueElement)
                                                .collect(Collectors.<V>toSet())),
                                Map<K, Set<V>>::putAll))
                .orElse(null);
    }

    static <K, V> void deserializeToMapOfSets(
            HashMap<K, Set<V>> map,
            JsonObject input,
            Function<String, K> deserializeKey,
            Function<JsonElement, V> deserializeValueElement) {
        Optional.ofNullable(input)
                .ifPresent(inputMap -> inputMap.entrySet()
                        .forEach((Map.Entry<String, JsonElement> el) -> map.put(deserializeKey.apply(el.getKey()),
                                asStream(el.getValue().getAsJsonArray().iterator())
                                        .map(deserializeValueElement)
                                        .collect(Collectors.<V>toSet()))));
    }

    static <T>void deserializeToCollection(Collection<T> collection, JsonArray serialized, Function<JsonElement, T> deserialize) {
        collection.clear();
        asStream(serialized.iterator())
            .forEach(serializedObject -> collection.add(deserialize.apply(serializedObject)));
    }

    static <K, V, S, T> JsonElement serializeMapOfObjects(
            Map<K, V> input,
            Function<K, S> serializeKey,
            Function<V, T> serializeValue) {
        return Optional.ofNullable(input)
                .map(inputMap -> inputMap.entrySet().stream()
                        .collect(
                                HashMap::new,
                                (Map<S, T> map, Map.Entry<K, V> el) -> map.put(
                                        serializeKey.apply(el.getKey()),
                                        serializeValue.apply(el.getValue())),
                                Map<S, T>::putAll))
                .map(GSON::toJsonTree)
                .orElse(null);
    }

    static <K, V> void deserializeToMap(
            HashMap<K, V> map,
            JsonObject input,
            Function<String, K> deserializeKey,
            Function<JsonElement, V> deserializeValue) {
        map.clear();
        Optional.ofNullable(input)
                .ifPresent(inputMap -> inputMap.entrySet()
                        .forEach(el -> map.put(deserializeKey.apply(el.getKey()), deserializeValue.apply(el.getValue()))));
    }

    static String typeOf(JsonElement el) {
        return el.getAsJsonObject().getAsJsonPrimitive(TYPE_PROPERTY).getAsString();
    }

    static String typeOf(Class<?> cls) {
        return cls.getSimpleName();
    }

    static <T> JsonElement serializeList(List<? extends Persistable> input) {
        return Optional.ofNullable(input)
                .map(list -> list.stream().map(Persistable::snapshot).collect(Collectors.toList()))
                .map(GSON::toJsonTree)
                .orElse(null);
    }

    static <T> JsonElement serializeSet(Set<? extends Persistable>input) {
        return Optional.ofNullable(input)
                .map(set -> set.stream().map(Persistable::snapshot).collect(Collectors.toList()))
                .map(GSON::toJsonTree)
                .orElse(null);
    }

    static <T> JsonElement serializeArray(Persistable[] input) {
        return Optional.ofNullable(input)
                .map(array -> Arrays.stream(array).map(Persistable::snapshot).collect(Collectors.toList()))
                .map(GSON::toJsonTree)
                .orElse(null);
    }

    static <T>Stream<T> deserializeStream(JsonArray serialized, Function<JsonElement, T> deserialize) {
        return asStream(serialized.iterator())
                .map(deserialize);
    }

    static <T, S> JsonElement serializeArrayOfObjects(T[] input, Function<T, S> serialize) {
        return Optional.ofNullable(input)
                .map(array -> Arrays.stream(array).map(serialize).collect(Collectors.toList()))
                .map(GSON::toJsonTree)
                .orElse(null);
    }

    static <T, S> S serializeNullable(T input, Function<T, S> serialize) {
        return Optional.ofNullable(input).map(serialize).orElse(null);
    }

    static <T> Optional<T> deserializeNullable(JsonObject input, String key, BiFunction<JsonObject, String, T> deserialize) {
        return input.has(key) && !input.get(key).isJsonNull() ? Optional.of(deserialize.apply(input, key)) : Optional.empty();
    }

    static <T> Stream<T> asStream(Iterator<T> sourceIterator) {
        return asStream(sourceIterator, false);
    }

    static <T> Stream<T> asStream(Iterator<T> sourceIterator, boolean parallel) {
        Iterable<T> iterable = () -> sourceIterator;
        return StreamSupport.stream(iterable.spliterator(), parallel);
    }

    JsonObject snapshot();
}
