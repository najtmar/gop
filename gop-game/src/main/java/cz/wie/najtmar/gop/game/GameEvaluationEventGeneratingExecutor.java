package cz.wie.najtmar.gop.game;

import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.event.EventProcessingException;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

import java.util.List;

import javax.annotation.Nonnull;

/**
 * Evaluates once the Game to potentially change its State.
 * @author najtmar
 */
public class GameEvaluationEventGeneratingExecutor implements EventGeneratingExecutor {

  /**
   * State of the Executor.
   */
  private State state;

  /**
   * The Game for which the Battles should be executed and Events generated.
   */
  private final Game game;

  /**
   * The instance of the Factory class that created this object.
   */
  private final Factory factory;

  /**
   * The factory of events used to generate events. Shared with other event generators.
   */
  private final EventFactory eventFactory;

  /**
   * List of events generated by the generator. Reuses the list provided in the prepare() method.
   */
  private List<Event> eventsGenerated;

  /**
   * The period time at which PERIODIC_INTERRUPT Events are generated.
   */
  private final long interruptTimePeriod;

  /**
   * Factory to create instances of class GameEvaluationEventGeneratingExecutor.
   * @author najtmar
   */
  public static class Factory implements EventGeneratingExecutor.Factory<GameEvaluationEventGeneratingExecutor> {

    final Game game;
    final EventFactory eventFactory;

    /**
     * At latest at this time the next PERIODIC_INTERRUPT Event should be generated.
     */
    private long nextPeriodicEventTime;

    /**
     * Constructor.
     * @param game the Game for which the Battles should be executed
     * @param eventFactory the EventFactory used by the executor
     */
    public Factory(@Nonnull Game game, @Nonnull EventFactory eventFactory) {
      this.game = game;
      this.eventFactory = eventFactory;
      final long interruptTimePeriod = game.getGameProperties().getInterruptTimePeriod();
      this.nextPeriodicEventTime = ((long) (this.game.getTime() / interruptTimePeriod)) * interruptTimePeriod;
      if (this.nextPeriodicEventTime < this.game.getTime()) {
        this.nextPeriodicEventTime += interruptTimePeriod;
      }
    }

    @Override
    public GameEvaluationEventGeneratingExecutor create() throws GameException {
      return new GameEvaluationEventGeneratingExecutor(game, eventFactory, this);
    }

    public long getNextPeriodicEventTime() {
      return nextPeriodicEventTime;
    }

    /**
     * Sets the time when the next PERIODIC_INTERRUPT Event should be generated.
     * @param nextPeriodicEventTime the time to advance to
     * @throws GameException when time cannot be advanced
     */
    void advanceNextPeriodicEventTimeTo(long nextPeriodicEventTime) throws GameException {
      if (nextPeriodicEventTime < this.nextPeriodicEventTime) {
        throw new GameException("Time cannot advance backward (" + nextPeriodicEventTime + " < "
            + this.nextPeriodicEventTime);
      }
      this.nextPeriodicEventTime = nextPeriodicEventTime;
    }

  }

  /**
   * Constructor.
   * @param game the Game for which the City productions should be performed
   * @param eventFactory the EventFactory used by the executor
   * @param factory the instance of the Factory class that creates this object
   */
  private GameEvaluationEventGeneratingExecutor(@Nonnull Game game, @Nonnull EventFactory eventFactory,
      @Nonnull Factory factory) {
    this.game = game;
    this.eventFactory = eventFactory;
    this.factory = factory;
    this.interruptTimePeriod = game.getGameProperties().getInterruptTimePeriod();
    this.state = State.NOT_PREPARED;
  }

  @Override
  public State getState() {
    return state;
  }

  @Override
  public void prepare(List<Event> inputEvents) throws GameException, EventProcessingException {
    if (state != State.NOT_PREPARED) {
      throw new GameException("prepare() can only be executed in the NOT_PREPARED State (found " + state + ").");
    }
    eventsGenerated = inputEvents;
    state = State.PREPARED;
  }

  @Override
  public void executeAndGenerate() throws GameException, EventProcessingException {
    if (state != State.PREPARED) {
      throw new GameException("generate() can only be executed if the generator is in the PREPARED state (found "
          + state + ").");
    }
    if (game.getState() != Game.State.RUNNING) {
      return;
    }

    // If needed, generate a new PERIODIC_INTERRUPT Event .
    final long nextPeriodicEventTime = factory.getNextPeriodicEventTime();
    if (nextPeriodicEventTime <= game.getTime()) {
      eventsGenerated.add(eventFactory.createPeriodicInterruptEvent());
      factory.advanceNextPeriodicEventTimeTo(
          ((long) (nextPeriodicEventTime / interruptTimePeriod)) * interruptTimePeriod + interruptTimePeriod);
    }

    // Check if any Player has State UNDECIDED .
    final int undecidedPlayersCount = StreamSupport.stream(game.getPlayers()).filter(
        (player) -> player.getState() == Player.State.UNDECIDED).collect(Collectors.counting()).intValue();
    if (undecidedPlayersCount >= 1) {
      for (Player player : game.getPlayers()) {
        if (player.getState() == Player.State.IN_GAME) {
          eventsGenerated.add(eventFactory.createChangePlayerStateEvent(player, Player.State.UNDECIDED));
          player.setState(Player.State.UNDECIDED);
        }
      }
    }

    // Check if any Player has lost.
    for (Player player : game.getPlayers()) {
      if (player.getState() == Player.State.IN_GAME) {
        if (game.getPlayerCities(player).isEmpty() && game.getPlayerPrawns(player).isEmpty()) {
          eventsGenerated.add(eventFactory.createChangePlayerStateEvent(player, Player.State.LOST));
          player.setState(Player.State.LOST);
        }
      }
    }

    // Check if there remains only one Player IN_GAME.
    final int inGamePlayersCount = StreamSupport.stream(game.getPlayers()).filter(
        (player) -> player.getState() == Player.State.IN_GAME).collect(Collectors.counting()).intValue();
    if (inGamePlayersCount == 1) {
      for (Player player : game.getPlayers()) {
        if (player.getState() == Player.State.IN_GAME) {
          eventsGenerated.add(eventFactory.createChangePlayerStateEvent(player, Player.State.WON));
          player.setState(Player.State.WON);
        }
      }
    }

    // Check if the Game has finished.
    if (game.calculateResult() != null) {
      eventsGenerated.add(eventFactory.createFinishGameEvent());
      game.finish();
    }

    state = State.GENERATED;
  }

  @Override
  public List<Event> getEventsGenerated() throws GameException {
    if (state != State.GENERATED) {
      throw new GameException("getEventsGenerated() can only be executed in the GENERATED State (found " + state
          + ").");
    }
    return eventsGenerated;
  }

}
