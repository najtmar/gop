package cz.wie.najtmar.gop.board;

/**
 * Generates Boards given Board Properties.
 * @author najtmar
 */
public interface BoardGenerator {

  /**
   * Generates a Board .
   * @return the Board generated
   * @throws BoardException when Board generation fails
   */
  public Board generate() throws BoardException;

}
