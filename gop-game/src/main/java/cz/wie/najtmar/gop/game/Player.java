package cz.wie.najtmar.gop.game;

import com.google.gson.JsonObject;
import cz.wie.najtmar.gop.common.Persistable;

/**
 * Player taking part in a Game.
 * @author najtmar
 */
public class Player implements Persistable {

  public static final String USER_PROPERTY = "user";
  public static final String STATE_PROPERTY = "state";
  public static final String INDEX_PROPERTY = "index";

  /**
   * Describes possible states a Player can be in.
   * @author najtmar
   */
  public enum State {
    /**
     * The Player is taking part in the Game.
     */
    IN_GAME,
    /**
     * The Player has lost the Game.
     */
    LOST,
    /**
     * The Player has abandoned the Game.
     */
    ABANDONED,
    /**
     * The Player has won the Game.
     */
    WON,
    /**
     * The Game is finished but the Player neither lost nor won (e.g., when a Player force quit the Game).
     */
    UNDECIDED,
  }

  /**
   * The user who acts as the Player .
   */
  private final User user;

  /**
   * The State of the Player in the Game.
   */
  private State state;

  /**
   * Index of the Player in the internal list of Players in {@link Player#game}.
   */
  private int index;

  /**
   * The Game which the Player takes part.
   */
  private final Game game;

  /**
   * Constructor.
   * @param user the User who acts as the Player
   * @param index index of the Player in the internal list of Players in game
   * @param game game in which the Player takes part
   */
  public Player(User user, int index, Game game) {
    this.user = user;
    this.index = index;
    this.game = game;
    this.state = State.IN_GAME;
  }

  public Player(JsonObject serialized, Game game) {
    this.user = new User(serialized.getAsJsonObject(USER_PROPERTY));
    this.state = Player.State.valueOf(serialized.getAsJsonPrimitive(STATE_PROPERTY).getAsString());
    this.index = serialized.getAsJsonPrimitive(INDEX_PROPERTY).getAsInt();
    this.game = game;
  }

  public User getUser() {
    return user;
  }

  public int getIndex() {
    return index;
  }

  public void setIndex(int index) {
    this.index = index;
  }

  public Game getGame() {
    return game;
  }

  public State getState() {
    return state;
  }

  public void setState(State state) {
    this.state = state;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Player)) {
      return false;
    }
    final Player player = (Player) obj;
    return this.index == player.index;
  }

  @Override
  public int hashCode() {
    return index;
  }

  @Override
  public String toString() {
    return user.getName();
  }

  @Override
  public JsonObject snapshot() {
    JsonObject returnValue = new JsonObject();
    returnValue.add(USER_PROPERTY, user.snapshot());
    returnValue.addProperty(STATE_PROPERTY, state.name());
    returnValue.addProperty(INDEX_PROPERTY, index);
    return returnValue;
  }

}
