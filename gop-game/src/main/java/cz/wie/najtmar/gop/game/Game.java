package cz.wie.najtmar.gop.game;

import com.google.gson.JsonObject;
import cz.wie.najtmar.gop.board.Board;
import cz.wie.najtmar.gop.board.Joint;
import cz.wie.najtmar.gop.board.RoadSection;
import cz.wie.najtmar.gop.common.Persistable;
import cz.wie.najtmar.gop.event.SerializationException;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.GuardedBy;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.*;

import static cz.wie.najtmar.gop.common.Persistable.*;
import static io.vavr.API.*;

/**
 * Standard Game for multiple players.
 * @author najtmar
 */
public class Game implements Persistable {

  public static final String GAME_PROPERTIES_PROPERTY = "gameProperties";
  public static final String STATE_PROPERTY = "state";
  public static final String ID_PROPERTY = "id";
  public static final String TIMESTAMP_PROPERTY = "timestamp";
  public static final String TIME_PROPERTY = "time";
  public static final String RESULT_PROPERTY = "result";
  public static final String PLAYERS_BY_STATE_PROPERTY = "playersByState";
  public static final String BOARD_PROPERTY = "board";
  public static final String PLAYERS_PROPERTY = "players";
  public static final String PRAWN_MAP_PROPERTY = "prawnMap";
  public static final String REMOVED_PRAWN_MAP_PROPERTY = "removedPrawnMap";
  public static final String PRAWNS_PROPERTY = "prawns";
  public static final String PLAYER_PRAWNS_PROPERTY = "playerPrawns";
  public static final String CITY_MAP_PROPERTY = "cityMap";
  public static final String REMOVED_CITY_MAP_PROPERTY = "removedCityMap";
  public static final String PLAYER_CITIES_PROPERTY = "playerCities";
  public static final String JOINT_TO_CITY_MAP_PROPERTY = "jointToCityMap";
  public static final String BATTLES_PROPERTY = "battles";
  public static final String REMOVED_BATTLES_PROPERTY = "removedBattles";
  public static final String PLAYER_VISIBLE_ROAD_SECTION_HALVES_PROPERTY = "playerVisibleRoadSectionHalves";
  public static final String PLAYER_DISCOVERED_ROAD_SECTION_HALVES_PROPERTY = "playerDiscoveredRoadSectionHalves";
  public static final String PLAYER_VISIBLE_PRAWNS_PROPERTY = "playerVisiblePrawns";
  public static final String PLAYER_DISCOVERED_CITIES_PROPERTY = "playerDiscoveredCities";
  public static final String PLAYER_VISIBLE_CITIES_PROPERTY = "playerVisibleCities";
  public static final String ROAD_SECTION_PROPERTY = "roadSection";
  public static final String INDEX_PROPERTY = "index";

  /**
   * State of the Game.
   * @author najtmar
   */
  public enum State {
    /**
     * The Game hasn't been initialized yet.
     */
    NOT_INITIALIZED,
    /**
     * The Game has been initialized but not started yet.
     */
    INITIALIZED,
    /**
     * The Game has not been started and never will.
     */
    INTENT_ABORTED,
    /**
     * The Game is running.
     */
    RUNNING,
    /**
     * The Game has finished.
     */
    FINISHED,
  }

  /**
   * Result of the Game.
   * @author najtmar
   */
  public enum Result {
    /**
     * There is exactly one winner of the Game.
     */
    WON,

    /**
     * The Result is undecided.
     */
    UNDECIDED,
  }

  /**
   * Game Properties.
   */
  private final GameProperties gameProperties;

  @GuardedBy("this")
  /**
   * State of the game.
   */
  private volatile State state;

  /**
   * Unique Game id.
   */
  private final UUID id;

  /**
   * Clock time timestamp used to identify the Game .
   */
  private final long timestamp;

  /**
   * The current Game time.
   */
  private long time;

  /**
   * The result of the Game or null if the Game is not yet FINISHED.
   */
  private Result result;

  /**
   * A map mapping a State to a Set of Players in this State, or null if the Game is still not finished.
   */
  private HashMap<Player.State, Set<Player>> playersByState;

  /**
   * Game's Board.
   */
  private Board board;

  /**
   * List of Players taking part in the Game.
   */
  private final ArrayList<Player> players = new ArrayList<>();

  /**
   * Container of Prawns taking part in the Game. Indexed by Prawn id.
   */
  private final HashMap<Integer, Prawn> prawnMap = new HashMap<>();

  /**
   * Equivalent of prawnMap for removed Prawns .
   */
  private final HashMap<Integer, Prawn> removedPrawnMap = new HashMap<>();

  /**
   * Container of all Prawns in the Game.
   */
  private final HashSet<Prawn> prawns = new HashSet<>();

  /**
   * Maps a Player to the set of all its Prawns that currently exist.
   */
  private final HashMap<Player, Set<Prawn>> playerPrawns = new HashMap<>();

  /**
   * Container of Cities taking part in the Game. Indexed by City id.
   */
  private final HashMap<Integer, City> cityMap = new HashMap<>();

  /**
   * Equivalent of cityMap for removed Cities .
   */
  private final HashMap<Integer, City> removedCityMap = new HashMap<>();

  /**
   * Maps a Player to the set of all its Cities that currently exist.
   */
  @GuardedBy("this")
  private final HashMap<Player, Set<City>> playerCities = new HashMap<>();

  /**
   * Maps a Joint to the a City.
   */
  private final HashMap<Joint, City> jointToCityMap = new HashMap<>();

  /**
   * Set of Battles currently going on. Values represent the actual set, keys are used for element matching.
   */
  private final HashMap<Battle, Battle> battles = new HashMap<>();

  /**
   * Equivalent of HashMap battles for removed Battles .
   */
  private final HashMap<Battle, Battle> removedBattles = new HashMap<>();

  /**
   * Maps a Player to all RoadSection.Halves that are visible to it.
   */
  private final HashMap<Player, Set<RoadSection.Half>> playerVisibleRoadSectionHalves = new HashMap<>();

  /**
   * Maps a Player to all RoadSection.Halves that have been already seen by it.
   */
  private final HashMap<Player, Set<RoadSection.Half>> playerDiscoveredRoadSectionHalves = new HashMap<>();

  /**
   * Maps a Player to all Prawns that are visible to it.
   */
  private final HashMap<Player, Set<Prawn>> playerVisiblePrawns = new HashMap<>();

  /**
   * Maps a Player to all Cities that have once seen by it but haven't been seen destroyed.
   */
  private final HashMap<Player, Set<City>> playerDiscoveredCities = new HashMap<>();

  /**
   * Maps a Player to all Cities that are visible to it.
   */
  private final HashMap<Player, Set<City>> playerVisibleCities = new HashMap<>();

  /**
   * Constructor.
   * @param gamePropertiesSet Properties for the Game
   * @throws GameException when Game cannot be created
   */
  public Game(@Nonnull Properties gamePropertiesSet) throws GameException {
    this.gameProperties = new GameProperties(gamePropertiesSet);
    this.timestamp = System.currentTimeMillis();
    this.id = UUID.randomUUID();
    this.state = State.NOT_INITIALIZED;
    this.result = null;
  }

  public Game(JsonObject serialized) {
    Prawn.firstAvailableId = 0;
    City.firstAvailableId = 0;
    this.gameProperties = deserialize(serialized.getAsJsonObject(GAME_PROPERTIES_PROPERTY), GameProperties.class);
    this.state = deserialize(serialized.getAsJsonPrimitive(STATE_PROPERTY), Game.State.class);
    this.id = deserialize(serialized.getAsJsonPrimitive(ID_PROPERTY), UUID.class);
    this.timestamp = serialized.getAsJsonPrimitive(TIMESTAMP_PROPERTY).getAsLong();
    this.time = serialized.getAsJsonPrimitive(TIME_PROPERTY).getAsLong();
    this.result = deserializeNullable(serialized, RESULT_PROPERTY, JsonObject::getAsJsonPrimitive)
            .map(el -> Game.Result.valueOf(el.getAsString()))
            .orElse(null);
    deserializeToCollection(this.players, serialized.getAsJsonArray(PLAYERS_PROPERTY), item -> new Player(item.getAsJsonObject(), this));
    this.playersByState = deserializeNullable(serialized, PLAYERS_BY_STATE_PROPERTY, JsonObject::getAsJsonObject)
            .map(object -> deserializeMapOfSets(
                    object,
                    Player.State::valueOf,
                    value -> this.players.get(deserialize(value, Integer.class))))
            .orElse(null);
    this.board = Optional.ofNullable(serialized.getAsJsonObject(BOARD_PROPERTY)).map(Board::new).orElse(null);
    Map<Integer, Prawn> allPrawnsMap = new HashMap<>();
    Prawn.resetFirstAvailableId();
    deserializeToCollection(this.prawns, serialized.getAsJsonArray(PRAWNS_PROPERTY), item -> {
      Prawn prawn = Match(typeOf(item)).of(
              Case($(type -> type.equals(Warrior.class.getSimpleName())), () -> new Warrior(item.getAsJsonObject(), this)),
              Case($(type -> type.equals(SettlersUnit.class.getSimpleName())), () -> new SettlersUnit(item.getAsJsonObject(), this)),
              Case($(), type -> {
                throw new SerializationException("Unrecognized type: " + type);
              }));
      allPrawnsMap.put(prawn.getId(), prawn);
      return prawn;
    });
    deserializeToMap(this.prawnMap, serialized.getAsJsonObject(PRAWN_MAP_PROPERTY), Integer::valueOf, value -> allPrawnsMap.get(value.getAsInt()));
    deserializeToMap(this.removedPrawnMap, serialized.getAsJsonObject(REMOVED_PRAWN_MAP_PROPERTY), Integer::valueOf, value -> allPrawnsMap.get(value.getAsInt()));
    deserializeToMapOfSets(
            this.playerPrawns,
            serialized.getAsJsonObject(PLAYER_PRAWNS_PROPERTY),
            key -> this.players.get(Integer.parseInt(key)),
            value -> this.prawnMap.get(value.getAsInt()));
    City.resetFirstAvailableId();
    deserializeToMap(
            this.cityMap,
            serialized.getAsJsonObject(CITY_MAP_PROPERTY),
            Integer::parseInt,
            value -> new City(value.getAsJsonObject(), this));
    deserializeToMap(
            this.removedCityMap,
            serialized.getAsJsonObject(REMOVED_CITY_MAP_PROPERTY),
            Integer::parseInt,
            value -> new City(value.getAsJsonObject(), this));
    deserializeToMapOfSets(
            this.playerCities,
            serialized.getAsJsonObject(PLAYER_CITIES_PROPERTY),
            key -> this.players.get(Integer.parseInt(key)),
            value -> this.cityMap.get(value.getAsInt()));
    deserializeToMap(
            this.jointToCityMap,
            serialized.getAsJsonObject(JOINT_TO_CITY_MAP_PROPERTY),
            key -> this.board.getJoints().get(Integer.parseInt(key)),
            value -> this.cityMap.get(value.getAsInt())
    );
    this.battles.clear();
    deserializeStream(
            serialized.getAsJsonArray(BATTLES_PROPERTY),
            el -> new Battle(el.getAsJsonObject(), this))
            .forEach(battle -> this.battles.put(battle, battle));
    deserializeStream(
            serialized.getAsJsonArray(REMOVED_BATTLES_PROPERTY),
            el -> new Battle(el.getAsJsonObject(), this))
            .forEach(battle -> this.removedBattles.put(battle, battle));
    deserializeToMapOfSets(
            this.playerVisibleRoadSectionHalves,
            serialized.getAsJsonObject(PLAYER_VISIBLE_ROAD_SECTION_HALVES_PROPERTY),
            key -> this.players.get(Integer.parseInt(key)),
            value -> {
              JsonObject object = value.getAsJsonObject();
              RoadSection roadSection = this.board.getRoadSections().get(object.getAsJsonPrimitive(ROAD_SECTION_PROPERTY).getAsInt());
              return roadSection.getHalves()[object.getAsJsonPrimitive(INDEX_PROPERTY).getAsInt()];
            }
    );
    deserializeToMapOfSets(
            this.playerDiscoveredRoadSectionHalves,
            serialized.getAsJsonObject(PLAYER_DISCOVERED_ROAD_SECTION_HALVES_PROPERTY),
            key -> this.players.get(Integer.parseInt(key)),
            value -> {
              JsonObject object = value.getAsJsonObject();
              RoadSection roadSection = this.board.getRoadSections().get(object.getAsJsonPrimitive(ROAD_SECTION_PROPERTY).getAsInt());
              return roadSection.getHalves()[object.getAsJsonPrimitive(INDEX_PROPERTY).getAsInt()];
            }
    );
    deserializeToMapOfSets(
            this.playerVisiblePrawns,
            serialized.getAsJsonObject(PLAYER_VISIBLE_PRAWNS_PROPERTY),
            key -> this.players.get(Integer.parseInt(key)),
            value -> this.lookupPrawnById(value.getAsInt(), true)
    );
    deserializeToMapOfSets(
            this.playerDiscoveredCities,
            serialized.getAsJsonObject(PLAYER_DISCOVERED_CITIES_PROPERTY),
            key -> this.players.get(Integer.parseInt(key)),
            value -> this.cityMap.get(value.getAsInt())
    );
    deserializeToMapOfSets(
            this.playerVisibleCities,
            serialized.getAsJsonObject(PLAYER_VISIBLE_CITIES_PROPERTY),
            key -> this.players.get(Integer.parseInt(key)),
            value -> this.cityMap.get(value.getAsInt())
    );
    if (this.state == State.RUNNING) {
      this.state = State.INITIALIZED;
    }
  }

  /**
   * Returns the State of the Game.
   * @return Game state
   */
  public synchronized State getState() {
    return state;
  }

  public UUID getId() {
    return id;
  }

  public long getTimestamp() {
    return timestamp;
  }

  public long getTime() {
    return time;
  }

  /**
   * Advances Game time to time. The time cannot go back.
   * @param time the time to advance to
   * @throws GameException when advancing time fails
   */
  public void advanceTimeTo(long time) throws GameException {
    if (time < this.time) {
      throw new GameException("Time cannot go backward (" + time + " < " + this.time + ").");
    }
    this.time = time;
  }

  /**
   * Returns the result of the Game.
   * @return the result of the Game
   * @throws GameException when the Game is not FINISHED
   */
  public synchronized Result getResult() throws GameException {
    if (state != State.FINISHED) {
      throw new GameException("Game result only makes sense for FINISHED Games (found " + state + ").");
    }
    return result;
  }

  /**
   * Returns a map between a State and corresponding Players.
   * @return a map between a State and corresponding Players
   * @throws GameException when the Game is not FINISHED
   */
  public synchronized HashMap<Player.State, Set<Player>> getPlayersByState() throws GameException {
    if (state != State.FINISHED) {
      throw new GameException("Mapping between States and Players only makes sense for FINISHED Games (found "
          + state + ").");
    }
    return playersByState;
  }

  /**
   * Sets board for the Game.
   * @param board the Board to set
   * @throws GameException when board cannot be set
   */
  public synchronized void setBoard(@Nonnull Board board) throws GameException {
    if (state != State.NOT_INITIALIZED) {
      throw new GameException("Game can only be set for NOT_INITIALIZED Games (state " + state + ").");
    }
    this.board = board;
  }

  /**
   * Returns the Board for the Game.
   * @return the Board for the Game
   */
  public Board getBoard() {
    return board;
  }

  /**
   * Returns a list of Players taking part in the Game.
   * @return a list of Players taking part in the Game
   */
  public List<Player> getPlayers() {
    return players;
  }

  /**
   * Initializes the Game.
   * @throws GameException when the Game State is incorrect
   */
  public synchronized void initialize() throws GameException {
    if (state != State.NOT_INITIALIZED) {
      throw new GameException("Game State must be NOT_INITIALIZED (found " + state + ").");
    }
    if (players.size() < 2) {
      throw new GameException("The Game must have at least 2 Players (found " + players.size() + ").");
    }
    if (board == null) {
      throw new GameException("Game Board not set.");
    }
    this.time = gameProperties.getStartTime();
    state = State.INITIALIZED;
  }

  /**
   * Aborts the intent to start the Game .
   * @throws GameException when Game State is incorrect
   */
  public synchronized void abortIntent() throws GameException {
    if (state != State.NOT_INITIALIZED) {
      throw new GameException("Game State must be NOT_INITIALIZED (found " + state + ").");
    }
    state = State.INTENT_ABORTED;
  }

  /**
   * Starts the Game.
   * @throws GameException if the Game cannot be started
   */
  public synchronized void start() throws GameException {
    if (state != State.INITIALIZED) {
      throw new GameException("Game State must be INITIALIZED (found " + state + ").");
    }
    state = State.RUNNING;
  }

  private HashMap<Player.State, Set<Player>> calculatePlayersByState() {
    final HashMap<Player.State, Set<Player>> result = new HashMap<>();
    for (Player player : players) {
      final Player.State state = player.getState();
      if (!result.containsKey(state)) {
        result.put(state, new HashSet<>());
      }
      result.get(state).add(player);
    }
    return result;
  }

  /**
   * Calculates the Result of the Game.
   * @return the Result of the Game or null if the Result cannot be calculated yet (the Game is not FINISHED).
   * @throws GameException when the Game is in an inconsistent State
   */
  public Result calculateResult() throws GameException {
    final HashMap<Player.State, Set<Player>> playersByState = calculatePlayersByState();
    final Set<Player> undecidedSet;
    if (playersByState.containsKey(Player.State.UNDECIDED)) {
      undecidedSet = playersByState.get(Player.State.UNDECIDED);
    } else {
      undecidedSet = new HashSet<>();
    }
    final Set<Player> wonSet;
    if (playersByState.containsKey(Player.State.WON)) {
      wonSet = playersByState.get(Player.State.WON);
    } else {
      wonSet = new HashSet<>();
    }
    final int wonSetCount = wonSet.size();
    if (wonSetCount > 1) {
      throw new GameException("Game cannot have more than 1 winner: " + playersByState.get(Player.State.WON));
    }
    final Set<Player> lostSet;
    if (playersByState.containsKey(Player.State.LOST)) {
      lostSet = playersByState.get(Player.State.LOST);
    } else {
      lostSet = new HashSet<>();
    }
    final Set<Player> abandonedSet;
    if (playersByState.containsKey(Player.State.ABANDONED)) {
      abandonedSet = playersByState.get(Player.State.ABANDONED);
    } else {
      abandonedSet = new HashSet<>();
    }
    final int lostOrAbandonedCount = lostSet.size() + abandonedSet.size();
    if (wonSetCount == 1 && lostOrAbandonedCount != players.size() - 1) {
      throw new GameException("If there is one Player who WON, all other must have LOST or ABANDONED (found "
          + playersByState + ").");
    }
    if (lostOrAbandonedCount == players.size() - 1 && wonSetCount != 1) {
      throw new GameException("If all Players but one have LOST or ABANDONED, the remaining one should have WON (found "
          + playersByState + ").");
    }
    if (!undecidedSet.isEmpty()) {
      return Result.UNDECIDED;
    } else if (!wonSet.isEmpty()) {
      return Result.WON;
    }
    return null;
  }

  /**
   * Finishes the Game.
   * @throws GameException when finishing the Game fails.
   */
  public synchronized void finish() throws GameException {
    if (state != State.RUNNING) {
      throw new GameException("Game State must be RUNNING (found " + state + ").");
    }
    result = calculateResult();
    if (result == null) {
      throw new GameException("Game Result could not be determined. The Game cannot finish.");
    }
    playersByState = calculatePlayersByState();
    state = State.FINISHED;
  }

  /**
   * Adds a Player to the list of Players. Fails if the index of the Player doesn't match its index in the list.
   * @param player a Player to be added
   * @throws GameException when the Player has an invalid index or the Game cannot be modified
   */
  public synchronized void addPlayer(@Nonnull Player player) throws GameException {
    if (state != State.NOT_INITIALIZED) {
      throw new GameException("Game State must be NOT_INITIALIZED (found " + state + ").");
    }
    final int maxPlayerCount = gameProperties.getMaxPlayerCount();
    if (players.size() >= maxPlayerCount) {
      throw new GameException("The Game can only have " + maxPlayerCount + " Players.");
    }
    if (player.getIndex() != players.size()) {
      throw new GameException("Player with index " + player.getIndex() + " cannot be added at position "
          + players.size());
    }
    players.add(player);
  }

  /**
   * Removes the Player from the list of Players. Changes the indexes of the remaining Players.
   * @param player the Player to be removed
   * @throws GameException when the Player cannot be removed
   */
  public void removePlayer(@Nonnull Player player) throws GameException {
    if (!players.contains(player)) {
      throw new GameException("Player " + player + " cannot be removed because it does not exist in list "
          + players + ".");
    }
    players.remove(player);
    for (int i = 0; i < players.size(); ++i) {
      players.get(i).setIndex(i);
    }
  }

  /**
   * Adds a Prawn to the Game.
   * @throws GameException when prawn cannot be added
   */
  public void addPrawn(@Nonnull Prawn prawn) throws GameException {
    if (prawnMap.containsKey(prawn.getId())) {
      throw new GameException("Prawn with id " + prawn.getId() + " already exists in the Game.");
    }
    prawnMap.put(prawn.getId(), prawn);
    prawns.add(prawn);
    final Player player = prawn.getPlayer();
    if (!players.contains(player)) {
      throw new GameException("Prawn " + prawn + " cannot be added because Player " + player
          + " does not exist in the Game.");
    }
    if (!playerPrawns.containsKey(player)) {
      playerPrawns.put(player, new HashSet<>());
    }
    playerPrawns.get(player).add(prawn);
  }

  /**
   * Removes a Prawn from the Game.
   * @throws GameException when prawn cannot be removed
   */
  public void removePrawn(@Nonnull Prawn prawn) throws GameException {
    final Player player = prawn.getPlayer();
    if (!players.contains(player)) {
      throw new GameException("Prawn " + prawn + " cannot be removed because Player " + player
          + " does not exist in the Game.");
    }
    final Set<Prawn> currentPlayerPrawns = playerPrawns.get(player);
    if (!prawnMap.containsKey(prawn.getId()) || !currentPlayerPrawns.contains(prawn)) {
      throw new GameException("Prawn " + prawn + " cannot be removed because it does not exist in the Game.");
    }
    prawnMap.remove(prawn.getId());
    removedPrawnMap.put(prawn.getId(), prawn);
    prawns.remove(prawn);
    currentPlayerPrawns.remove(prawn);
  }

  /**
   * Returns all Prawns for player.
   * @param player the Player whose Prawns should be returned
   * @return the Set of all Prawns for player
   * @throws GameException when looking for Prawns fails
   */
  public Set<Prawn> getPlayerPrawns(@Nonnull Player player) throws GameException {
    if (!players.contains(player)) {
      throw new GameException("Prawns for Player " + player
          + " cannot be looked up because the Player does not exist in the Game.");
    }
    if (playerPrawns.containsKey(player)) {
      return playerPrawns.get(player);
    } else {
      return new HashSet<>();
    }
  }

  /**
   * Looks up a Prawn with a given id.
   * @param id the id of the Prawn to look up
   * @param alsoRemoved iff true then also the removed Prawns are looked up
   * @return the Prawn with a given id or null if such Prawn does not exist in the Game
   */
  public Prawn lookupPrawnById(int id, boolean alsoRemoved) {
    final Prawn prawn = prawnMap.get(id);
    if (prawn != null || !alsoRemoved) {
      return prawn;
    }
    return removedPrawnMap.get(id);
  }

  public HashSet<Prawn> getPrawns() {
    return prawns;
  }

  /**
   * Adds a City of size 1 to the Game.
   * @throws GameException when city cannot be added
   */
  public synchronized void addCity(@Nonnull City city) throws GameException {
    if (cityMap.containsKey(city.getId())) {
      throw new GameException("City with id " + city.getId() + " already exists in the Game.");
    }
    cityMap.put(city.getId(), city);
    final Player player = city.getPlayer();
    if (!players.contains(player)) {
      throw new GameException("" + city + " cannot be added because Player " + player
          + " does not exist in the Game.");
    }
    if (!playerCities.containsKey(player)) {
      playerCities.put(player, new HashSet<>());
    }
    playerCities.get(player).add(city);
    jointToCityMap.put(city.getJoint(), city);
  }

  /**
   * Destroys a City of size 0.
   * @throws GameException when city cannot be destroyed
   */
  public synchronized void destroyCity(@Nonnull City city) throws GameException {
    final Player player = city.getPlayer();
    if (!players.contains(player)) {
      throw new GameException("" + city + " cannot be destroyed because Player " + player
          + " does not exist in the Game.");
    }
    final Set<City> cities = playerCities.get(player);
    if (!cityMap.containsKey(city.getId()) || !cities.contains(city)) {
      throw new GameException("City " + city + " cannot be removed because it does not exist in the Game.");
    }
    cityMap.remove(city.getId());
    removedCityMap.put(city.getId(), city);
    cities.remove(city);
    jointToCityMap.remove(city.getJoint());
  }

  /**
   * Looks up a City with a given id.
   * @param id the id of the City to look up
   * @param alsoRemoved iff true then also the removed Cities are looked up
   * @return the City with a given id or null if such City does not exist in the Game
   */
  public City lookupCityById(int id, boolean alsoRemoved) {
    final City city = cityMap.get(id);
    if (city != null || !alsoRemoved) {
      return city;
    }
    return removedCityMap.get(id);
  }

  /**
   * Looks up a City with a given joint.
   * @param joint the Joint to look up
   * @return the City with a given joint or null if such City does not exist in the Game
   */
  public City lookupCityByJoint(Joint joint) {
    return jointToCityMap.get(joint);
  }

  /**
   * Marks oldPlayer and newPlayer and the old and the current owner of city, respectively, to match the actual status.
   * @param city the City to be captured
   * @param newPlayer the Player that captures city
   * @throws GameException when the city capturing process fails
   */
  public synchronized void captureCity(@Nonnull City city, @Nonnull Player oldPlayer, @Nonnull Player newPlayer)
      throws GameException {
    if (!players.contains(oldPlayer)) {
      throw new GameException("" + city + " cannot be captured because Player " + oldPlayer
          + " does not exist in the Game.");
    }
    if (!players.contains(newPlayer)) {
      throw new GameException("" + city + " cannot be captured because Player " + newPlayer
          + " does not exist in the Game.");
    }
    final Set<City> oldPlayerCities = playerCities.get(oldPlayer);
    if (!oldPlayerCities.contains(city)) {
      throw new GameException("" + city + " not present in the Game.");
    }
    if (!playerCities.containsKey(newPlayer)) {
      playerCities.put(newPlayer, new HashSet<>());
    }
    final Set<City> newPlayerCities = playerCities.get(newPlayer);
    if (newPlayerCities.contains(city)) {
      throw new GameException("" + city + " still marked as owned by Player " + oldPlayer);
    }
    oldPlayerCities.remove(city);
    newPlayerCities.add(city);
  }

  /**
   * Returns all Cities for player.
   * @param player the Player whose Cities should be returned
   * @return the Set of all Cities for player
   * @throws GameException when looking for Players fails
   */
  public synchronized Set<City> getPlayerCities(@Nonnull Player player) throws GameException {
    if (!players.contains(player)) {
      throw new GameException("Cities for Player " + player
          + " cannot be looked up because the Player does not exist in the Game.");
    }
    if (playerCities.containsKey(player)) {
      return playerCities.get(player);
    } else {
      return new HashSet<>();
    }
  }

  /**
   * Returns the set of GameProperties used in the current Game.
   * @return the set of GameProperties used in the current Game
   */
  public GameProperties getGameProperties() {
    return gameProperties;
  }

  /**
   * Returns the copy of a set of Battles currently going on.
   * @return a copy of a set of Battles currently going on
   */
  public Set<Battle> getBattles() {
    return new HashSet<>(battles.values());
  }

  /**
   * Returns the Battle matching battle from the set of Battles going on, or null if no matching Battle is found.
   * @param battle the Battle to look for
   * @param alsoRemoved iff true then also the most recently removed matching Battle is looked up
   * @return the Battle matching battle, or null of no matching Battle is found
   */
  public Battle getBattle(@Nonnull Battle battle, boolean alsoRemoved) {
    final Battle lookedUpBattle = battles.get(battle);
    if (lookedUpBattle != null || !alsoRemoved) {
      return lookedUpBattle;
    }
    return removedBattles.get(battle);
  }

  /**
   * Adds battle to the set of Battles going on. Dies if battle is already going on.
   * @param battle the Battle to be added
   * @throws GameException if battle is already going on
   */
  public void addBattle(@Nonnull Battle battle) throws GameException {
    if (battles.containsKey(battle)) {
      throw new GameException("Battle " + battle + " is already going on.");
    }
    battles.put(battle, battle);
  }

  /**
   * Removes battle from the set of Battles going on. Dies if battle is not going on.
   * @param battle to be removed
   * @throws GameException if battle is not going on
   */
  public void removeBattle(@Nonnull Battle battle) throws GameException {
    if (!battles.containsKey(battle)) {
      throw new GameException("Battle " + battle + " is not going on.");
    }
    battles.remove(battle);
    removedBattles.put(battle, battle);
  }

  /**
   * Returns the Set of RoadSection.Halves visible to player .
   * @param player the Player whose Set of visible RoadSection.Halves is returned
   * @return the Set of RoadSection.Halves visible to player
   */
  public Set<RoadSection.Half> getPlayerVisibleRoadSectionHalves(@Nonnull Player player) {
    if (!playerVisibleRoadSectionHalves.containsKey(player)) {
      playerVisibleRoadSectionHalves.put(player, new HashSet<>());
    }
    return playerVisibleRoadSectionHalves.get(player);
  }

  /**
   * Puts the Set of RoadSection.Halves visible to player .
   * @param player the Player for whom the Set is put
   * @param roadSectionHalves the Set of RoadSection.Halves to put
   */
  public void putPlayerVisibleRoadSectionHalves(@Nonnull Player player,
      @Nonnull Set<RoadSection.Half> roadSectionHalves) {
    playerVisibleRoadSectionHalves.put(player, roadSectionHalves);
  }

  /**
   * Returns the Set of RoadSection.Halves discovered by player .
   * @param player the Player whose Set of discovered Prawns is returned
   * @return the Set of RoadSection.Halves discovered by player
   */
  public Set<RoadSection.Half> getPlayerDiscoveredRoadSectionHalves(@Nonnull Player player) {
    if (!playerDiscoveredRoadSectionHalves.containsKey(player)) {
      playerDiscoveredRoadSectionHalves.put(player, new HashSet<>());
    }
    return playerDiscoveredRoadSectionHalves.get(player);
  }

  /**
   * Add roadSectionHalves to the Set of RoadSection.Halves discovered by player . If needed, initialize the Set .
   * @param player the Player for whom the Set is added
   * @param roadSectionHalves a Collection of RoadSection.Halves to add
   */
  public void addPlayerDiscoveredRoadSectionHalves(@Nonnull Player player,
      @Nonnull Collection<RoadSection.Half> roadSectionHalves) {
    if (!playerDiscoveredRoadSectionHalves.containsKey(player)) {
      playerDiscoveredRoadSectionHalves.put(player, new HashSet<>());
    }
    playerDiscoveredRoadSectionHalves.get(player).addAll(roadSectionHalves);
  }

  /**
   * Returns the Set of Prawns visible to player .
   * @param player the Player whose Set of visible Prawns is returned
   * @return the Set of Prawns visible to player
   */
  public Set<Prawn> getPlayerVisiblePrawns(@Nonnull Player player) {
    if (!playerVisiblePrawns.containsKey(player)) {
      playerVisiblePrawns.put(player, new HashSet<>());
    }
    return playerVisiblePrawns.get(player);
  }

  /**
   * Puts the Set of Prawns visible to player .
   * @param player the Player for whom the Set is put
   * @param prawns the Set of Prawns to put
   */
  public void putPlayerVisiblePrawns(@Nonnull Player player, @Nonnull Set<Prawn> prawns) {
    playerVisiblePrawns.put(player, prawns);
  }

  /**
   * Returns the Set of Cities discovered by player .
   * @param player the Player whose Set of discovered Cities is returned
   * @return the Set of Cities discovered by player
   */
  public Set<City> getPlayerDiscoveredCities(@Nonnull Player player) {
    if (!playerDiscoveredCities.containsKey(player)) {
      playerDiscoveredCities.put(player, new HashSet<>());
    }
    return playerDiscoveredCities.get(player);
  }

  /**
   * Puts the Set of Cities discovered by player .
   * @param player the Player for whom the Set is put
   * @param cities the Set of Cities to put
   */
  public void putPlayerDiscoveredCities(@Nonnull Player player, @Nonnull Set<City> cities) {
    playerDiscoveredCities.put(player, cities);
  }

  /**
   * Returns the Set of Cities visible to player .
   * @param player the Player whose Set of visible Cities is returned
   * @return the Set of Cities visible to player
   */
  public Set<City> getPlayerVisibleCities(@Nonnull Player player) {
    if (!playerVisibleCities.containsKey(player)) {
      playerVisibleCities.put(player, new HashSet<>());
    }
    return playerVisibleCities.get(player);
  }

  /**
   * Puts the Set of Cities visible to player .
   * @param player the Player for whom the Set is put
   * @param cities the Set of Cities to put
   */
  public void putPlayerVisibleCities(@Nonnull Player player, @Nonnull Set<City> cities) {
    playerVisibleCities.put(player, cities);
  }

  @Override
  public JsonObject snapshot() {
    JsonObject returnValue = new JsonObject();
    returnValue.add(GAME_PROPERTIES_PROPERTY, serializeObject(gameProperties));
    returnValue.addProperty(STATE_PROPERTY, state.name());
    returnValue.addProperty(ID_PROPERTY, id.toString());
    returnValue.addProperty(TIMESTAMP_PROPERTY, timestamp);
    returnValue.addProperty(TIME_PROPERTY, time);
    returnValue.addProperty(RESULT_PROPERTY, serializeNullable(result, Result::name));
    returnValue.add(PLAYERS_BY_STATE_PROPERTY, serializeMapOfSets(playersByState, Player.State::name, Player::getIndex));
    returnValue.add(BOARD_PROPERTY, serializeNullable(board, Board::snapshot));
    returnValue.add(PLAYERS_PROPERTY, serializeList(players));
    returnValue.add(PRAWN_MAP_PROPERTY, serializeMapOfObjects(prawnMap, id -> id, Prawn::getId));
    returnValue.add(REMOVED_PRAWN_MAP_PROPERTY, serializeMapOfObjects(removedPrawnMap, id -> id, Prawn::getId));
    returnValue.add(PRAWNS_PROPERTY, serializeSet(prawns));
    returnValue.add(PLAYER_PRAWNS_PROPERTY, serializeMapOfSets(playerPrawns, Player::getIndex, Prawn::getId));
    returnValue.add(CITY_MAP_PROPERTY, serializeMapOfObjects(cityMap, id -> id, City::snapshot));
    returnValue.add(REMOVED_CITY_MAP_PROPERTY, serializeMapOfObjects(removedCityMap, id -> id, City::snapshot));
    returnValue.add(PLAYER_CITIES_PROPERTY, serializeMapOfSets(playerCities, Player::getIndex, City::getId));
    returnValue.add(JOINT_TO_CITY_MAP_PROPERTY, serializeMapOfObjects(jointToCityMap, Joint::getIndex, City::getId));
    returnValue.add(BATTLES_PROPERTY, serializeSet(battles.keySet()));
    returnValue.add(REMOVED_BATTLES_PROPERTY, serializeSet(removedBattles.keySet()));
    returnValue.add(PLAYER_VISIBLE_ROAD_SECTION_HALVES_PROPERTY,
            serializeMapOfSets(playerVisibleRoadSectionHalves, Player::getIndex, this::serializeRoadSectorHalf));
    returnValue.add(PLAYER_DISCOVERED_ROAD_SECTION_HALVES_PROPERTY,
            serializeMapOfSets(playerDiscoveredRoadSectionHalves, Player::getIndex, this::serializeRoadSectorHalf));
    returnValue.add(PLAYER_VISIBLE_PRAWNS_PROPERTY, serializeMapOfSets(playerVisiblePrawns, Player::getIndex, Prawn::getId));
    returnValue.add(PLAYER_DISCOVERED_CITIES_PROPERTY, serializeMapOfSets(playerDiscoveredCities, Player::getIndex, City::getId));
    returnValue.add(PLAYER_VISIBLE_CITIES_PROPERTY, serializeMapOfSets(playerVisibleCities, Player::getIndex, City::getId));

    return returnValue;
  }

  private JsonObject serializeRoadSectorHalf(RoadSection.Half half) {
    JsonObject serializedHalf = new JsonObject();
    serializedHalf.addProperty(ROAD_SECTION_PROPERTY, half.getRoadSection().getIndex());
    serializedHalf.addProperty(INDEX_PROPERTY, half.getEnd() == RoadSection.Direction.BACKWARD ? 0 : 1);
    return serializedHalf;
  }

}
