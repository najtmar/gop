package cz.wie.najtmar.gop.board;

import com.google.inject.Inject;

import cz.wie.najtmar.gop.common.Point2D;
import cz.wie.najtmar.gop.game.PropertiesReader;

import java.io.IOException;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;

/**
 * Simple BoardGenerator used in tests. Generates a Board that resembles a clock face.
 * @author najtmar
 */
public class ClockBoardGenerator implements BoardGenerator {

  private final PropertiesReader propertiesReader;

  @Inject
  ClockBoardGenerator(@Nonnull PropertiesReader propertiesReader) {
    this.propertiesReader = propertiesReader;
  }

  /**
   * Creates and adds to board a Joint located at a given hour on a clock face with a given center and x/y radiuses.
   * @param index Joint index
   * @param hour the hour on which the Joint is located
   * @param xcenter the x-coordinate of the center of the clock face
   * @param ycenter the y-coordinate of the center of the clock face
   * @param xradius the radius of the clock face projected on the x axis
   * @param yradius the radius of the clock face projected on the y axis
   * @return the Joint created
   * @throws BoardException when Joint creation fails
   */
  private Joint createAndAddClockJoint(@Nonnull Board board, @Nonnegative int index, @Nonnegative int hour,
      @Nonnegative int xcenter, @Nonnegative int ycenter, @Nonnegative int xradius, @Nonnegative int yradius)
          throws BoardException {
    final double angle = Math.PI / 6.0 * (3 - hour);
    final Point2D.Double coord = new Point2D.Double(
        xcenter + xradius * Math.cos(angle),
        ycenter + yradius * Math.sin(angle));
    final Joint joint = (new Joint.Builder())
        .setBoard(board)
        .setCoordinatesPair(coord)
        .setIndex(index)
        .setName("joint" + index)
        .build();
    board.addJoint(joint);
    return joint;
  }

  @Override
  public Board generate() throws BoardException {
    final Board result;
    try {
      result = new Board(propertiesReader.getBoardProperties());
    } catch (IOException exception) {
      throw new BoardException("Reading Board Properties failed.", exception);
    }
    final int xsize = result.getBoardPropertiesSet().getXsize();
    final int ysize = result.getBoardPropertiesSet().getYsize();
    final int xcenter = xsize / 2;
    final int ycenter = ysize / 2;
    int xradius = (xsize - 1) - xcenter;
    if (xradius > xcenter - 1) {
      xradius = xcenter - 1;
    }
    int yradius = (ysize - 1) - ycenter;
    if (yradius > ycenter - 1) {
      yradius = ycenter - 1;
    }

    // Joints.
    final Joint[] outerClock = new Joint[12];
    for (int i = 0; i < 12; ++i) {
      outerClock[i] = createAndAddClockJoint(result, i, i, xcenter, ycenter, xradius, yradius);
    }
    final Joint[] innerClock = new Joint[12];
    for (int i = 0; i < 12; ++i) {
      innerClock[i] = createAndAddClockJoint(result, i + 12, i, xcenter, ycenter, xradius / 2, yradius / 2);
    }
    final Joint centralJoint = (new Joint.Builder())
        .setBoard(result)
        .setCoordinatesPair(new Point2D.Double(xcenter, ycenter))
        .setIndex(24)
        .setName("joint24")
        .build();
    result.addJoint(centralJoint);

    // RoadSections.
    int roadSectionIndex = 0;
    for (int i = 0; i < 12; ++i) {
      final RoadSection roadSection = (new RoadSection.Builder())
          .setBoard(result)
          .setIndex(roadSectionIndex++)
          .setJoints(outerClock[i], innerClock[i])
          .build();
      result.addRoadSection(roadSection);
    }
    for (int i = 0; i < 12; ++i) {
      final RoadSection roadSection = (new RoadSection.Builder())
          .setBoard(result)
          .setIndex(roadSectionIndex++)
          .setJoints(outerClock[i], outerClock[(i + 1) % 12])
          .build();
      result.addRoadSection(roadSection);
    }
    for (int i = 0; i < 12; ++i) {
      final RoadSection roadSection = (new RoadSection.Builder())
          .setBoard(result)
          .setIndex(roadSectionIndex++)
          .setJoints(innerClock[i], centralJoint)
          .build();
      result.addRoadSection(roadSection);
    }
    for (int i = 0; i < 12; ++i) {
      final RoadSection roadSection = (new RoadSection.Builder())
          .setBoard(result)
          .setIndex(roadSectionIndex++)
          .setJoints(innerClock[i], innerClock[(i + 1) % 12])
          .build();
      result.addRoadSection(roadSection);
    }

    return result;
  }

}
