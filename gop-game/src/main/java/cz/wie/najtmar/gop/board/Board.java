package cz.wie.najtmar.gop.board;

import cz.wie.najtmar.gop.common.Persistable;
import cz.wie.najtmar.gop.common.Point2D;
import cz.wie.najtmar.gop.event.SerializationException;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.json.*;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.stream.Collectors;

import static cz.wie.najtmar.gop.common.Persistable.*;

/**
 * Represents a Game board.
 * @author najtmar
 */
@EqualsAndHashCode
@ToString
public class Board implements Persistable {

  public static final String STEP_SIZE_PROPERTY_NAME = "Board.stepSize";
  public static final String X_SIZE_PROPERTY_NAME = "Board.xSize";
  public static final String Y_SIZE_PROPERTY_NAME = "Board.ySize";
  public static final String BOARD_PROPERTIES_SET_PROPERTY = "boardPropertiesSet";
  public static final String JOINTS_PROPERTY = "joints";
  public static final String ROAD_SECTIONS_PROPERTY = "roadSections";

  /**
   * The Properties for this Board.
   */
  private final BoardProperties boardPropertiesSet;

  /**
   * All Joints on the Board.
   */
  private ArrayList<Joint> joints;

  /**
   * All RoadSections on the Board.
   */
  private ArrayList<RoadSection> roadSections;

  /**
   * Constructor.
   * @param boardPropertiesSet Properties for the Board
   * @throws BoardException when Board creation fails
   */
  public Board(@Nonnull Properties boardPropertiesSet) throws BoardException {
    if (boardPropertiesSet.getProperty(STEP_SIZE_PROPERTY_NAME) == null) {
      throw new BoardException("Property " + STEP_SIZE_PROPERTY_NAME + " not specified.");
    }
    this.boardPropertiesSet = new BoardProperties(boardPropertiesSet);
    this.joints = new ArrayList<Joint>();
    this.roadSections = new ArrayList<RoadSection>();
  }

  public Board(com.google.gson.JsonObject serialized) {
    this.boardPropertiesSet = Persistable.deserialize(serialized.getAsJsonObject(BOARD_PROPERTIES_SET_PROPERTY), BoardProperties.class);
    this.joints = new ArrayList<>();
    deserializeToCollection(this.joints, serialized.getAsJsonArray(JOINTS_PROPERTY), el -> new Joint(el.getAsJsonObject(), this));
    this.roadSections = Persistable.asStream(serialized.getAsJsonArray(ROAD_SECTIONS_PROPERTY).iterator())
            .map(el -> new RoadSection(el.getAsJsonObject(), this))
            .collect(Collectors.toCollection(ArrayList::new));
  }

  public ArrayList<Joint> getJoints() {
    return joints;
  }

  /**
   * Adds a Joint to the list {@link Board#joints}. Fails if the index of the Joint doesn't match its index in the list.
   * @param joint a Joint to be added
   * @throws BoardException when the Joint has an invalid index or a Joint with the same coordinates already exists
   */
  public void addJoint(Joint joint) throws BoardException {
    if (joint.getIndex() != joints.size()) {
      throw new BoardException("Joint with index " + joint.getIndex() + " cannot be added at position "
          + joints.size());
    }
    for (Joint otherJoint : joints) {
      if (otherJoint.getCoordinatesPair().equals(joint.getCoordinatesPair())) {
        throw new BoardException("Joint with coordinates " + otherJoint.getCoordinatesPair() + " already exists.");
      }
    }
    joints.add(joint);
  }

  public BoardProperties getBoardPropertiesSet() {
    return boardPropertiesSet;
  }

  public ArrayList<RoadSection> getRoadSections() {
    return roadSections;
  }

  /**
   * Adds a RoadSection to the list {@link Board#roadSections}. Fails if the index of the RoadSection doesn't match its
   * index in the list.
   * @param roadSection a RoadSection to be added
   * @throws BoardException when the RoadSection has an invalid index
   */
  public void addRoadSection(RoadSection roadSection) throws BoardException {
    if (roadSection.getIndex() != roadSections.size()) {
      throw new BoardException("RoadSection with index " + roadSection.getIndex()
          + " cannot be added at position " + roadSections.size());
    }
    roadSections.add(roadSection);
  }

  /**
   * Serializes the Board.
   * @return serialized board
   */
  public JsonObject serialize() {
    final JsonObjectBuilder builder = Json.createObjectBuilder();

    // Properties.
    final JsonArrayBuilder serializedPropertiesBuilder = Json.createArrayBuilder();
    for (Entry<Object, Object> propertyEntry : boardPropertiesSet.getPropertiesSet().entrySet()) {
      final JsonObject serializedPropertyEntry = Json.createObjectBuilder()
          .add("key", propertyEntry.getKey().toString())
          .add("value", propertyEntry.getValue().toString())
          .build();
      serializedPropertiesBuilder.add(serializedPropertyEntry);
    }
    builder.add("properties", serializedPropertiesBuilder.build());

    // Joints.
    final JsonArrayBuilder serializedJointsBuilder = Json.createArrayBuilder();
    for (Joint joint : joints) {
      final JsonObject serializedJoint = Json.createObjectBuilder()
          .add("index", joint.getIndex())
          .add("name", joint.getName())
          .add("x", joint.getCoordinatesPair().getX())
          .add("y", joint.getCoordinatesPair().getY())
          .build();
      serializedJointsBuilder.add(serializedJoint);
    }
    builder.add("joints", serializedJointsBuilder.build());

    // RoadSections.
    final JsonArrayBuilder serializedRoadSectionsBuilder = Json.createArrayBuilder();
    for (RoadSection roadSection : roadSections) {
      final JsonObject serializedRoadSection = Json.createObjectBuilder()
          .add("index", roadSection.getIndex())
          .add("joint0", roadSection.getJoints()[0].getIndex())
          .add("joint1", roadSection.getJoints()[1].getIndex())
          .build();
      serializedRoadSectionsBuilder.add(serializedRoadSection);
    }
    builder.add("roadSections", serializedRoadSectionsBuilder.build());

    return builder.build();
  }

  /**
   * Deserializes objects of type Board.
   * @param serialized serialized object of type Board
   * @return serialized deserialized to an object of type Board
   * @throws SerializationException when deserialization process fails
   */
  public static Board deserialize(@Nonnegative JsonObject serialized) throws SerializationException {
    // Board.
    Properties boardPropertiesSet = new Properties();
    for (JsonValue serializedPropertyEntryValue : serialized.getJsonArray("properties")) {
      JsonObject serializedPropertyEntry = (JsonObject) serializedPropertyEntryValue;
      boardPropertiesSet.setProperty(
          serializedPropertyEntry.getString("key"), serializedPropertyEntry.getString("value"));
    }
    Board resultBoard;
    try {
      resultBoard = new Board(boardPropertiesSet);
    } catch (BoardException exception) {
      throw new SerializationException("Board creation failed.", exception);
    }

    // Joints.
    for (JsonValue serializedJointValue : serialized.getJsonArray("joints")) {
      JsonObject serializedJoint = (JsonObject) serializedJointValue;
      try {
        resultBoard.addJoint(new Joint.Builder()
            .setBoard(resultBoard)
            .setCoordinatesPair(new Point2D.Double(
                serializedJoint.getJsonNumber("x").doubleValue(),
                serializedJoint.getJsonNumber("y").doubleValue()))
            .setIndex(serializedJoint.getInt("index"))
            .setName(serializedJoint.getString("name"))
            .build());
      } catch (BoardException exception) {
        throw new SerializationException("Joint deserialization failed.", exception);
      }
    }

    // RoadSections.
    for (JsonValue serializedRoadSectionValue : serialized.getJsonArray("roadSections")) {
      JsonObject serializedRoadSection = (JsonObject) serializedRoadSectionValue;
      try {
        resultBoard.addRoadSection(new RoadSection.Builder()
            .setBoard(resultBoard)
            .setIndex(serializedRoadSection.getInt("index"))
            .setJoints(
                resultBoard.getJoints().get(serializedRoadSection.getInt("joint0")),
                resultBoard.getJoints().get(serializedRoadSection.getInt("joint1")))
            .build());
      } catch (BoardException exception) {
        throw new SerializationException("RoadSection deserialization failed.", exception);
      }
    }

    return resultBoard;
  }

  @Override
  public com.google.gson.JsonObject snapshot() {
    com.google.gson.JsonObject returnValue = new com.google.gson.JsonObject();
    returnValue.add(BOARD_PROPERTIES_SET_PROPERTY, serializeObject(boardPropertiesSet));
    returnValue.add(JOINTS_PROPERTY, serializeList(joints));
    returnValue.add(ROAD_SECTIONS_PROPERTY, serializeList(roadSections));

    return returnValue;
  }
}
