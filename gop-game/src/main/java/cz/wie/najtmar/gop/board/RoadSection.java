package cz.wie.najtmar.gop.board;

import com.google.gson.JsonObject;
import cz.wie.najtmar.gop.common.Persistable;

import javax.annotation.Nonnull;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static cz.wie.najtmar.gop.common.Persistable.*;

/**
 * Represents a section of road. Hashed by the Board-unique index.
 * @author najtmar
 */
public class RoadSection implements Persistable {

  public static final String INDEX_PROPERTY = "index";
  public static final String JOINTS_PROPERTY = "joints";
  public static final String POSITIONS_COUNT_PROPERTY = "positionsCount";
  public static final String LENGTH_PROPERTY = "length";
  public static final String STEP_SIZE_PROPERTY = "stepSize";
  public static final String ANGLE_PROPERTY = "angle";
  public static final String HALVES_PROPERTY = "halves";

  /**
   * Direction on a RoadSection. Either from Joint 0 to Joint 1 (FORWARD) or from Joint 1 to Joint 0 (BACKWARD).
   * @author najtmar
   */
  public enum Direction {
    FORWARD,
    BACKWARD,
  }

  /**
   * Index of the RoadSection in the internal list of RoadSections in {@link RoadSection#board}.
   */
  private final int index;

  /**
   * The Board to which the ReadSection belongs.
   */
  private final Board board;

  /**
   * Contains 2 Joints on both ends of the RoadSections.
   */
  private final Joint[] joints;

  /**
   * Number of positions on the RoadSection, including both Joints and the midpoint.
   */
  private final int positionsCount;

  /**
   * Length of the RoadSection.
   */
  private final double length;

  /**
   * Length between two consecutive positions on the RoadSection.
   */
  private final double stepSize;

  /**
   * Angle in radians of vector (joint[0], joint[1]).
   */
  private final double angle;

  public RoadSection(JsonObject serialized, Board board) {
    this.index = serialized.getAsJsonPrimitive(INDEX_PROPERTY).getAsInt();
    this.board = board;
    this.joints = deserializeStream(serialized.getAsJsonArray(JOINTS_PROPERTY), el -> board.getJoints().get(el.getAsJsonPrimitive().getAsInt()))
            .toArray(Joint[]::new);
    this.positionsCount = serialized.getAsJsonPrimitive(POSITIONS_COUNT_PROPERTY).getAsInt();
    this.length = serialized.getAsJsonPrimitive(LENGTH_PROPERTY).getAsDouble();
    this.stepSize = serialized.getAsJsonPrimitive(STEP_SIZE_PROPERTY).getAsDouble();
    this.angle = serialized.getAsJsonPrimitive(ANGLE_PROPERTY).getAsDouble();
    this.halves = deserializeStream(serialized.getAsJsonArray(HALVES_PROPERTY), el -> new Half(el.getAsJsonObject()))
            .toArray(Half[]::new);
  }

  /**
   * Represents a RoadSection half as an end of the RoadSection. Hashed by the RoadSection and its end.
   * @author najtmar
   */
  public class Half implements Comparable<Half>, Persistable {

    public static final String END_PROPERTY = "end";
    public static final String HASH_CODE_PROPERTY = "hashCode";

    /**
     * The end of roadSection to which the Half is adjacent.
     */
    private final RoadSection.Direction end;

    /**
     * Unique hash code.
     */
    private final int hashCode;

    /**
     * Constructor.
     * @param end the end of roadSection to which the Half is adjacent
     */
    public Half(@Nonnull RoadSection.Direction end) {
      this.end = end;
      this.hashCode = Objects.hash(RoadSection.this.hashCode(), this.end.ordinal());
    }

    public Half(JsonObject serialized) {
      this.end = RoadSection.Direction.valueOf(serialized.getAsJsonPrimitive(END_PROPERTY).getAsString());
      this.hashCode = serialized.getAsJsonPrimitive(HASH_CODE_PROPERTY).getAsInt();
    }

    /**
     * Returns the RoadSection to which this Half belongs.
     * @return the RoadSection to which this Half belongs
     */
    public RoadSection getRoadSection() {
      return RoadSection.this;
    }

    public RoadSection.Direction getEnd() {
      return end;
    }

    /**
     * Returns the Set of RoadSection.Halves that are adjacent to the current Half .
     * @return the Set of RoadSection.Halves that are adjacent to the current Half
     */
    public Set<Half> getAdjacentHalves() {
      final Set<Half> result = new HashSet<>();
      final Joint outerJoint;
      if (end == RoadSection.Direction.BACKWARD) {
        result.add(halves[1]);
        outerJoint = joints[0];
      } else {
        result.add(halves[0]);
        outerJoint = joints[1];
      }
      for (RoadSection adjacentRoadSection : outerJoint.getRoadSections()) {
        if (!adjacentRoadSection.equals(RoadSection.this)) {
          if (adjacentRoadSection.joints[0].equals(outerJoint)) {
            result.add(adjacentRoadSection.halves[0]);
          } else {
            result.add(adjacentRoadSection.halves[1]);
          }
        }
      }
      return result;
    }

    /**
     * Returns true iff a given RoadSection.Half contains position . Positions on a Joint belong to all
     * RoadSection.Halves to which the Joint belongs.
     * @param position the Position to be checked
     * @return true iff a given RoadSection.Half contains position
     */
    public boolean contains(@Nonnull Position position) {
      final Joint joint = position.getJoint();
      if (joint == null) {
        if (!getRoadSection().equals(position.getRoadSection())) {
          return false;
        }
        if (end == RoadSection.Direction.BACKWARD) {
          return position.getIndex() <= getRoadSection().getMidpointIndex();
        } else {
          return getRoadSection().getMidpointIndex() <= position.getIndex();
        }
      } else {
        final Joint currentJoint;
        if (end == RoadSection.Direction.BACKWARD) {
          currentJoint = joints[0];
        } else {
          currentJoint = joints[1];
        }
        return currentJoint.equals(joint);
      }
    }

    @Override
    public boolean equals(Object obj) {
      if (!(obj instanceof Half)) {
        return false;
      }
      final Half otherHalf = (Half) obj;
      return (RoadSection.this.equals(otherHalf.getRoadSection()) && end.equals(otherHalf.end));
    }

    @Override
    public int hashCode() {
      return hashCode;
    }

    @Override
    public int compareTo(Half otherRoadSectionHalf) {
      final int roadSectionComparison =
          Integer.compare(RoadSection.this.index, otherRoadSectionHalf.getRoadSection().index);
      if (roadSectionComparison != 0) {
        return roadSectionComparison;
      }
      return -this.end.compareTo(otherRoadSectionHalf.end);
    }

    @Override
    public String toString() {
      return "Half [roadSection=" + RoadSection.this + ", end=" + end + "]";
    }

    @Override
    public JsonObject snapshot() {
      JsonObject returnValue = new JsonObject();
      returnValue.addProperty(END_PROPERTY, end.name());
      returnValue.addProperty(HASH_CODE_PROPERTY, hashCode);
      return returnValue;
    }
  }

  /**
   * Contains 2 Halves on both ends of the RoadSections.
   */
  private final Half[] halves;


  public int getIndex() {
    return index;
  }

  public Board getBoard() {
    return board;
  }

  public Joint[] getJoints() {
    return joints;
  }

  public Half[] getHalves() {
    return halves;
  }

  public int getPositionsCount() {
    return positionsCount;
  }

  public double getLength() {
    return length;
  }

  public double getStepSize() {
    return stepSize;
  }

  public double getAngle() {
    return angle;
  }

  /**
   * Returns 2 indexes in the array {@link RoadSection#joints} corresponding to both joints. Sorted the same way as
   * array {@link RoadSection#joints}.
   */
  public int[] getJointPositionIndexes() {
    return new int[]{0, positionsCount - 1};
  }

  /**
   * Returns 2 indexes in the array {@link RoadSection#joints} corresponding to both joint-adjacent point.
   * Sorted the same way as array {@link RoadSection#joints}.
   */
  public int[] getJointAdjacentPointIndexes() {
    return new int[]{1, positionsCount - 2};
  }

  /**
   * Checks whether the current RoadSection is adjacent to the joint.
   * @param joint the Joint to be checked
   * @return true iff the current RoadSection is adjacent to the joint
   */
  public boolean adjacentTo(@Nonnull Joint joint) {
    return joints[0].equals(joint) || joints[1].equals(joint);
  }

  /**
   * Returns the index of the joint on the current RoadSection.
   * @param joint Joint to look for the index
   * @return the index of the joint
   * @throws BoardException if joint is not adjacent to the RoadSection
   */
  public int getJointIndex(@Nonnull Joint joint) throws BoardException {
    if (joints[0].equals(joint)) {
      return 0;
    }
    if (joints[1].equals(joint)) {
      return positionsCount - 1;
    }
    throw new BoardException("Joint " + joint + " is not on " + this + " .");
  }

  /**
   * Returns the index in the array {@link RoadSection#joints} corresponding to the road section midpoint.
   */
  public int getMidpointIndex() {
    return positionsCount / 2;
  }

  /**
   * Returns the Position equal to the given position, which has the current RoadSection, or null if such position does
   * not exist.
   * @param position the Position to be projected
   * @return position projected on the current RoadSection or null
   * @throws BoardException if the projection process fails
   */
  public Position getProjectedPosition(@Nonnull Position position) throws BoardException {
    if (this.equals(position.getRoadSection())) {
      return position;
    }
    final Joint positionJoint = position.getJoint();
    if (positionJoint == null || !this.adjacentTo(positionJoint)) {
      return null;
    }
    return new Position(this, this.getJointIndex(positionJoint));
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof RoadSection)) {
      return false;
    }
    return index == ((RoadSection) obj).index;
  }

  @Override
  public int hashCode() {
    return index;
  }

  @Override
  public String toString() {
    return "(" + index + ", (" + joints[0] + ", " + joints[1] + "), " + positionsCount + ", " + angle + ")";
  }

  /**
   * Given angle and direction, calculates cosine between the angle and the RoadSection angle in the given direction.
   * If the result is negative, then 0.0 is returned. Used to calculate strength and velocity of warriors.
   * @param angle the angle to which the cosine should be calculated
   * @param direction one of 2 directions for a RoadSection
   * @return cosine between angles in a direction or 0.0 if the cosine is negative
   */
  public double calculateDirContrib(double angle, Direction direction) {
    double cos = Math.cos(angle - this.angle);
    if (direction == Direction.BACKWARD) {
      cos = -cos;
    }
    if (cos >= 0.0) {
      return cos;
    }
    return 0.0;
  }

  /**
   * Returns the Direction at which the given RoadSection is adjacent to joint.
   * @param joint the Joint whose Direction is checked in the context of the given RoadSection
   * @return BACKWARD, if joint in the RoadSection's joint with index 0, FORWARD, if it's the RoadSection's joint with
   *     index 1, null otherwise
   */
  public Direction getJointDirection(Joint joint) {
    if (joints[0].equals(joint)) {
      return Direction.BACKWARD;
    }
    if (joints[1].equals(joint)) {
      return Direction.FORWARD;
    }
    return null;
  }

  public static class Builder {
    private int index = -1;
    private Board board;
    private Joint joint0;
    private Joint joint1;

    public Builder setIndex(int index) {
      this.index = index;
      return this;
    }

    public Builder setBoard(Board board) {
      this.board = board;
      return this;
    }

    /**
     * Sets joints.
     * @param joint0 first joint
     * @param joint1 second joint
     * @return the builder itself
     */
    public Builder setJoints(Joint joint0, Joint joint1) {
      this.joint0 = joint0;
      this.joint1 = joint1;
      return this;
    }

    /**
     * Builds a RoadSection.
     * @return a RoadSection
     * @throws BoardException when RoadSection cannot be created
     */
    public RoadSection build() throws BoardException {
      if (index == -1) {
        throw new BoardException("index not set.");
      }
      if (board == null) {
        throw new BoardException("Board not set.");
      }
      if (joint0 == null || joint1 == null) {
        throw new BoardException("Joints not set.");
      }
      return new RoadSection(index, board, joint0, joint1);
    }
  }

  private RoadSection(int index, @Nonnull Board board,
      @Nonnull final Joint joint0, @Nonnull final Joint joint1) throws BoardException {
    this.index = index;
    this.board = board;
    this.joints = new Joint[]{joint0, joint1};
    this.halves = new Half[]{new Half(Direction.BACKWARD), new Half(Direction.FORWARD)};
    this.length = joint0.getCoordinatesPair().distance(joint1.getCoordinatesPair());
    if (this.length == 0.0) {
      throw new BoardException("A RoadSection must have a non-zero length.");
    }
    final double stepSize = board.getBoardPropertiesSet().getStepSize();
    final double sectionLengthByStepSize = this.length / stepSize;
    final double ceilSectionLengthByStepSize = Math.ceil(sectionLengthByStepSize);
    if (sectionLengthByStepSize != ceilSectionLengthByStepSize) {
      this.positionsCount = (int) ceilSectionLengthByStepSize;
    } else {
      this.positionsCount = (int) ceilSectionLengthByStepSize + 1;
    }
    if (this.positionsCount < 3) {
      throw new BoardException("A RoadSection must have at least 3 positions. "
          + "sectionLength == " + this.length + ", stepSize == " + stepSize);
    }
    this.stepSize = this.length / (this.positionsCount - 1);
    final double roadVectorX = joint1.getCoordinatesPair().getX() - joint0.getCoordinatesPair().getX();
    final double roadVectorY = joint1.getCoordinatesPair().getY() - joint0.getCoordinatesPair().getY();
    final double roadVectorAtan2 = Math.atan2(roadVectorY, roadVectorX);
    if (roadVectorAtan2 >= 0.0) {
      this.angle = roadVectorAtan2;
    } else {
      this.angle = roadVectorAtan2 + 2.0 * Math.PI;
    }

    joint0.addRoadSection(this);
    joint1.addRoadSection(this);
  }

  @Override
  public JsonObject snapshot() {
    JsonObject returnValue = new JsonObject();
    returnValue.addProperty(INDEX_PROPERTY, index);
    returnValue.add(JOINTS_PROPERTY, serializeArrayOfObjects(joints, Joint::getIndex));
    returnValue.addProperty(POSITIONS_COUNT_PROPERTY, positionsCount);
    returnValue.addProperty(LENGTH_PROPERTY, length);
    returnValue.addProperty(STEP_SIZE_PROPERTY, stepSize);
    returnValue.addProperty(ANGLE_PROPERTY, angle);
    returnValue.add(HALVES_PROPERTY, serializeArray(halves));
    return returnValue;
  }

}
