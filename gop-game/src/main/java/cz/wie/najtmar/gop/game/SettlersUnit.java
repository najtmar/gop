package cz.wie.najtmar.gop.game;

import com.google.gson.JsonObject;
import cz.wie.najtmar.gop.board.DirectedPosition;
import cz.wie.najtmar.gop.board.Position;
import cz.wie.najtmar.gop.common.Persistable;
import lombok.NoArgsConstructor;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;

import static cz.wie.najtmar.gop.common.Persistable.typeOf;

/**
 * Represents a SettlersUnit.
 * @author najtmar
 */
public class SettlersUnit extends Prawn {

  /**
   * Super-class implementation should be sufficient. This one only does some additional checking.
   */
  @Override
  public boolean equals(Object obj) {
    if (!super.equals(obj)) {
      return false;
    }
    if (!(obj instanceof SettlersUnit)) {
      throw new RuntimeException("This should never happen.");
    }
    return true;
  }

  @NoArgsConstructor
  public static class Builder implements Persistable {
    public static final String CURRENT_POSITION_PROPERTY = "currentPosition";
    public static final String CREATION_TIME_PROPERTY = "creationTime";

    private GameProperties gameProperties;
    private Player player;
    private Position currentPosition;
    private long creationTime = -1;

    public Builder(JsonObject serialized, Player player) {
      this.gameProperties = player.getGame().getGameProperties();
      this.player = player;
      this.currentPosition = new Position(serialized.getAsJsonObject(CURRENT_POSITION_PROPERTY), player.getGame().getBoard());
      this.creationTime = serialized.getAsJsonPrimitive(CREATION_TIME_PROPERTY).getAsLong();
    }

    public Builder setGameProperties(GameProperties gameProperties) {
      this.gameProperties = gameProperties;
      return this;
    }

    public Builder setPlayer(Player player) {
      this.player = player;
      return this;
    }

    public Builder setCurrentPosition(Position currentPosition) {
      this.currentPosition = currentPosition;
      return this;
    }

    public long getCreationTime() {
      return creationTime;
    }

    public Builder setCreationTime(long creationTime) {
      this.creationTime = creationTime;
      return this;
    }

    /**
     * Builds an instance of SettlersUnit.
     * @return build instance of SettlersUnit
     * @throws GameException when creation of SettlersUnit fails
     */
    public SettlersUnit build() throws GameException {
      if (gameProperties == null) {
        throw new GameException("GameProperties not set.");
      }
      if (player == null) {
        throw new GameException("Player not set.");
      }
      if (currentPosition == null) {
        throw new GameException("currentPosition not set.");
      }
      if (creationTime == -1) {
        throw new GameException("Creation time not set.");
      }
      return new SettlersUnit(gameProperties, player, currentPosition, creationTime);
    }

    @Override
    public JsonObject snapshot() {
      JsonObject resultValue = new JsonObject();
      resultValue.add(CURRENT_POSITION_PROPERTY, currentPosition.snapshot());
      resultValue.addProperty(CREATION_TIME_PROPERTY, creationTime);
      return resultValue;
    }
  }

  /**
   * A SettlersUnit always has basic velocity.
   */
  @Override
  protected double calculateVelocity(@Nonnull DirectedPosition unusedDirectedPosition) {
    return getGameProperties().getBasicVelocity();
  }

  @Override
  public double calculateAttackStrength(DirectedPosition directedPosition) throws GameException {
    final Position position = directedPosition.getPosition();
    if (!position.equals(getCurrentPosition())) {
      throw new GameException("Attack strength must be calculated at the current Position of the Prawn (" + position
          + " != " + getCurrentPosition() + ").");
    }
    return 0.0;
  }

  private SettlersUnit(@Nonnull GameProperties gameProperties, @Nonnull Player player,
      @Nonnull Position currentPosition, @Nonnegative long creationTime) throws GameException {
    super(gameProperties, player, currentPosition, creationTime);
  }

  public SettlersUnit(JsonObject serialized, Game game) {
    super(serialized, game);
  }

  @Override
  public String toString() {
    return "(SETTLERS_UNIT, " + super.toString() + ")";
  }

  @Override
  public JsonObject snapshot() {
    JsonObject returnValue = super.snapshot();
    returnValue.addProperty(TYPE_PROPERTY, typeOf(this.getClass()));
    return returnValue;
  }
}
