package cz.wie.najtmar.gop.game;

import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.event.EventProcessingException;

import java.util.List;

/**
 * Simple implementation of EventGeneratingExecutor, which returns the same Event List as provided as an argument
 * to the prepare() method. Used in tests.
 * @author najtmar
 */
public class ForwardingEventGeneratingExecutor implements EventGeneratingExecutor {

  private State state;

  private List<Event> eventsGenerated;

  public static class Factory implements EventGeneratingExecutor.Factory<ForwardingEventGeneratingExecutor> {
    /**
     * The last instance of class ForwardingEventGeneratingExecutor created by the Factory .
     */
    private ForwardingEventGeneratingExecutor lastInstanceCreated;

    @Override
    public ForwardingEventGeneratingExecutor create() throws GameException {
      lastInstanceCreated = new ForwardingEventGeneratingExecutor();
      return lastInstanceCreated;
    }

    public ForwardingEventGeneratingExecutor getLastInstanceCreated() {
      return lastInstanceCreated;
    }

  }

  private ForwardingEventGeneratingExecutor() {
    this.state = State.NOT_PREPARED;
  }

  @Override
  public State getState() {
    return state;
  }

  @Override
  public void prepare(List<Event> inputEvents) throws GameException, EventProcessingException {
    if (state != State.NOT_PREPARED) {
      throw new GameException("prepare() can only be executed in the NOT_PREPARED State (found " + state + ").");
    }
    this.eventsGenerated = inputEvents;
    state = State.PREPARED;
  }

  @Override
  public void executeAndGenerate() throws GameException, EventProcessingException {
    if (state != State.PREPARED) {
      throw new GameException("executeAndGenerate() can only be executed in the PREPARED State (found " + state + ").");
    }
    state = State.GENERATED;
  }

  @Override
  public List<Event> getEventsGenerated() throws GameException {
    if (state != State.GENERATED) {
      throw new GameException("getEventsGenerated() can only be executed in the GENERATED State (found " + state
          + ").");
    }
    return eventsGenerated;
  }

}
