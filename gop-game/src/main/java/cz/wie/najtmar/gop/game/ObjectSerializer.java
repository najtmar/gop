package cz.wie.najtmar.gop.game;

import cz.wie.najtmar.gop.board.Board;
import cz.wie.najtmar.gop.board.BoardException;
import cz.wie.najtmar.gop.board.Position;
import cz.wie.najtmar.gop.board.RoadSection;
import cz.wie.najtmar.gop.event.SerializationException;
import cz.wie.najtmar.gop.game.Itinerary.ItineraryBuildingException;

import java.util.Iterator;
import java.util.UUID;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

/**
 * Serializes and deserializes objects used in the Game, to be used for event processing. Depending on the ObjectType,
 * objects are either created or reused. For every supported ObjectType, provides a pair of methods:
 * * SerializationType serializeObjectType(ObjectType object);
 * * ObjectType deserializeObjectType(SerializationType serialized);
 * @author najtmar
 */
public class ObjectSerializer {

  /**
   * Game in the context of which objects are serialized and deserialized.
   */
  private final Game game;

  private final Board board;

  /**
   * Constructor.
   * @param game The Game used as a context for ObjectSerializer.
   * @throws GameException when ObjectSerializer cannot be initialized.
   */
  public ObjectSerializer(@Nonnull Game game) throws GameException {
    if (game.getState() == Game.State.NOT_INITIALIZED) {
      throw new GameException("Game must not be in NOT_INITIALIZED State.");
    }
    this.game = game;
    this.board = game.getBoard();
  }

  public Game getGame() {
    return game;
  }

  /**
   * Serializes objects of type User .
   * @param user the User to serialize
   * @return serialized user
   */
  public JsonObject serializeUser(@Nonnull User user) {
    return Json.createObjectBuilder()
        .add("name", user.getName())
        .add("uuid", user.getId().toString())
        .build();
  }

  /**
   * Deserializes new objects of type User .
   * @param serialized serialized object of type User
   * @return serialized deserialized to an object of type User
   * @throws SerializationException when deserialization process fails
   */
  public User deserializeNewUser(@Nonnull JsonObject serialized) throws SerializationException {
    final String name = serialized.getString("name");
    final UUID id;
    try {
      id = UUID.fromString(serialized.getString("uuid"));
    } catch (IllegalArgumentException exception) {
      throw new SerializationException("User deserialization failed: " + serialized, exception);
    }
    return new User(name, id);
  }

  /**
   * Serializes the Game to reconstruct it on the client side.
   * @return serialized Game
   */
  public JsonObject serializeGameWithoutPrawnsOrCities() {
    final JsonObjectBuilder builder = Json.createObjectBuilder();
    // Game.
    builder.add("id", game.getId().toString());
    builder.add("timestamp", game.getTimestamp());
    // Board.
    builder.add("board", board.serialize());
    // Players.
    final JsonArrayBuilder serializedPlayersBuilder = Json.createArrayBuilder();
    for (Player player : game.getPlayers()) {
      final JsonObject serializedPropertyEntry = Json.createObjectBuilder()
          .add("index", player.getIndex())
          .add("user", serializeUser(player.getUser()))
          .build();
      serializedPlayersBuilder.add(serializedPropertyEntry);
    }
    builder.add("players", serializedPlayersBuilder.build());
    return builder.build();
  }


  /**
   * Serializes objects of type RoadSection.
   * @param roadSection the object to serialize
   * @return serialized roadSection
   */
  public int serializeRoadSection(@Nonnull RoadSection roadSection) {
    return roadSection.getIndex();
  }

  /**
   * Deserializes objects of type RoadSection.
   * @param serialized serialized object of type RoadSection
   * @return serialized deserialized to an object of type RoadSection
   * @throws SerializationException when deserialization process fails
   */
  public RoadSection deserializeRoadSection(@Nonnegative int serialized) throws SerializationException {
    try {
      return board.getRoadSections().get(serialized);
    } catch (IndexOutOfBoundsException exception) {
      throw new SerializationException("" + serialized + " could not be deserialized as RoadSection.", exception);
    }
  }

  /**
   * Serializes objects of type Position.
   * @param position the object to serialize
   * @return serialized position
   */
  public JsonObject serializePosition(@Nonnull Position position) {
    final int serializedRoadSection = serializeRoadSection(position.getRoadSection());
    return Json.createObjectBuilder()
        .add("roadSection", serializedRoadSection)
        .add("index", position.getIndex())
        .build();
  }

  /**
   * Deserializes new objects of type Position.
   * @param serialized serialized object of type Position
   * @return serialized deserialized to an object of type Position
   * @throws SerializationException when deserialization process fails
   */
  public Position deserializeNewPosition(@Nonnull JsonObject serialized) throws SerializationException {
    final RoadSection roadSection = deserializeRoadSection(serialized.getInt("roadSection"));
    final int index = serialized.getInt("index");
    try {
      return new Position(roadSection, index);
    } catch (BoardException exception) {
      throw new SerializationException("" + serialized + " could not be deserialized as a Position.", exception);
    }
  }

  /**
   * Serializes objects of type Move.
   * @param move the object to serialize
   * @return serialized move
   */
  public JsonObject serializeMove(@Nonnull Move move) {
    final JsonObject serializedPosition = serializePosition(move.getPosition());
    return Json.createObjectBuilder()
        .add("position", serializedPosition)
        .add("moveTime", move.getMoveTime())
        .add("stop", move.isStop())
        .build();
  }

  /**
   * Deserializes new objects of type Move.
   * @param serialized serialized object of type Move
   * @return serialized deserialized to an object of type Move
   * @throws SerializationException when deserialization process fails
   */
  public Move deserializeNewMove(@Nonnull JsonObject serialized) throws SerializationException {
    final Position position = deserializeNewPosition(serialized.getJsonObject("position"));
    return new Move(position, serialized.getJsonNumber("moveTime").longValue(), serialized.getBoolean("stop"));
  }

  /**
   * Serializes objects of type Prawn.
   * @param prawn the object to serialize
   * @return serialized prawn
   */
  public int serializePrawn(@Nonnull Prawn prawn) {
    return prawn.getId();
  }

  /**
   * Deserializes objects of type Prawn.
   * @param serialized serialized object of type Prawn
   * @param alsoRemoved iff true then also the removed Prawns are looked up in the Game
   * @return serialized deserialized to an object of type Prawn
   * @throws SerializationException when deserialization process fails
   */
  public Prawn deserializePrawn(@Nonnegative int serialized, boolean alsoRemoved) throws SerializationException {
    final Prawn prawn = game.lookupPrawnById(serialized, alsoRemoved);
    if (prawn == null) {
      throw new SerializationException("Prawn with id " + serialized + " not found in the Game.");
    }
    return prawn;
  }

  /**
   * Serializes objects of type Player.
   * @param player the object to serialize
   * @return serialized player
   */
  public int serializePlayer(@Nonnull Player player) {
    return player.getIndex();
  }

  /**
   * Deserializes objects of type Player.
   * @param serialized serialized object of type Player
   * @return serialized deserialized to an object of type Player
   * @throws SerializationException when deserialization process fails
   */
  public Player deserializePlayer(@Nonnegative int serialized) throws SerializationException {
    final Player player = game.getPlayers().get(serialized);
    if (player == null) {
      throw new SerializationException("Player with index " + serialized + " not found in the Game.");
    }
    return player;
  }

  /**
   * Serializes objects of type City.
   * @param city the object to serialize
   * @return serialized city
   */
  public int serializeCity(@Nonnull City city) {
    return city.getId();
  }

  /**
   * Deserializes objects of type City.
   * @param serialized serialized object of type City
   * @param alsoRemoved iff true then also the removed Cities are looked up in the Game
   * @return serialized deserialized to an object of type City
   * @throws SerializationException when deserialization process fails
   */
  public City deserializeCity(@Nonnegative int serialized, boolean alsoRemoved) throws SerializationException {
    final City city = game.lookupCityById(serialized, alsoRemoved);
    if (city == null) {
      throw new SerializationException("City with id " + serialized + " not found in the Game.");
    }
    return city;
  }

  /**
   * Serializes objects of type CityFactory.
   * @param factory the object to serialize
   * @return serialized factory
   */
  public JsonObject serializeFactory(@Nonnull CityFactory factory) {
    return Json.createObjectBuilder()
        .add("city", serializeCity(factory.getCity()))
        .add("index", factory.getIndex())
        .build();
  }

  /**
   * Deserializes objects of type CityFactory.
   * @param serialized serialized object of type CityFactory
   * @param alsoRemoved iff true then also Factories in removed Cities are looked up in the Game
   * @return serialized deserialized to an object of type CityFactory
   * @throws SerializationException when deserialization process fails
   */
  public CityFactory deserializeFactory(@Nonnull JsonObject serialized, boolean alsoRemoved)
      throws SerializationException {
    final City city = game.lookupCityById(serialized.getInt("city"), alsoRemoved);
    if (city == null) {
      throw new SerializationException("City with id " + serialized + " not found in the Game.");
    }
    final int factoryIndex = serialized.getInt("index");
    return city.getFactory(factoryIndex);
  }

  /**
   * Serializes objects of type Battle.
   * @param battle the object to serialize
   * @return serialized battle
   */
  public JsonObject serializeBattle(@Nonnull Battle battle) {
    final int serializedPrawn0 = serializePrawn(battle.getPrawns()[0]);
    final int serializedPrawn1 = serializePrawn(battle.getPrawns()[1]);
    final JsonObject serializedNormalizedPosition0 = serializePosition(battle.getNormalizedPositions()[0]);
    final JsonObject serializedNormalizedPosition1 = serializePosition(battle.getNormalizedPositions()[1]);
    return Json.createObjectBuilder()
        .add("prawn0", serializedPrawn0)
        .add("prawn1", serializedPrawn1)
        .add("normalizedPosition0", serializedNormalizedPosition0)
        .add("normalizedPosition1", serializedNormalizedPosition1)
        .build();
  }

  /**
   * Deserializes new objects of type Battle.
   * @param serialized serialized object of type Battle
   * @param alsoRemoved iff true then also removed Prawns are looked up in the Game
   * @return serialized deserialized to an object of type Battle
   * @throws SerializationException when deserialization process fails
   */
  public Battle deserializeNewBattle(@Nonnegative JsonObject serialized, boolean alsoRemoved)
      throws SerializationException {
    final Prawn[] prawns = new Prawn[]{
        deserializePrawn(serialized.getInt("prawn0"), alsoRemoved),
        deserializePrawn(serialized.getInt("prawn1"), alsoRemoved),
    };
    final Position[] normalizedPositions = new Position[]{
        deserializeNewPosition(serialized.getJsonObject("normalizedPosition0")),
        deserializeNewPosition(serialized.getJsonObject("normalizedPosition1")),
    };
    try {
      return new Battle(prawns[0], normalizedPositions[0], normalizedPositions[1], prawns[1], normalizedPositions[1],
          normalizedPositions[0]);
    } catch (GameException | BoardException exception) {
      throw new SerializationException("Battle " + serialized + " could not be deserialized.", exception);
    }
  }

  /**
   * Deserializes new objects of type Itinerary.
   * @param serialized serialized object of type Itinerary
   * @return serialized deserialized to an object of type Itinerary
   * @throws SerializationException when deserialization process fails
   */
  public Itinerary deserializeNewItinerary(@Nonnegative JsonArray serialized) throws SerializationException {
    if (serialized.size() == 1) {
      throw new SerializationException("Serialized Itinerary must not have 1 element.");
    }
    if (serialized.isEmpty()) {
      return null;
    }
    final Iterator<JsonValue> itineraryIter = serialized.iterator();
    final Itinerary.Builder builder = new Itinerary.Builder();
    while (itineraryIter.hasNext()) {
      final Position nextNode = deserializeNewPosition((JsonObject) itineraryIter.next());
      try {
        builder.addPosition(nextNode);
      } catch (ItineraryBuildingException | GameException exception) {
        throw new SerializationException("" + nextNode + " could not be added to the Itinerary.");
      }
    }
    try {
      return builder.build();
    } catch (ItineraryBuildingException | GameException exception) {
      throw new SerializationException("" + serialized.toString() + " could not be deserialized as an Itinerary.");
    }
  }

  /**
   * Serializes objects of type RoadSection.Half .
   * @param roadSectionHalf the object to serialize
   * @return serialized roadSectionHalf
   */
  public JsonObject serializeRoadSectionHalf(@Nonnull RoadSection.Half roadSectionHalf) {
    return Json.createObjectBuilder()
        .add("roadSection", serializeRoadSection(roadSectionHalf.getRoadSection()))
        .add("end", roadSectionHalf.getEnd().name())
        .build();
  }

  /**
   * Deserializes objects of type RoadSection.Half .
   * @param serialized serialized object of type RoadSection.Half
   * @return serialized deserialized to an object of type RoadSection.Half
   * @throws SerializationException when deserialization process fails
   */
  public RoadSection.Half deserializeRoadSectionHalf(@Nonnull JsonObject serialized)
      throws SerializationException {
    final RoadSection roadSection = deserializeRoadSection(serialized.getInt("roadSection"));
    switch (RoadSection.Direction.valueOf(serialized.getString("end"))) {
      case BACKWARD:
        return roadSection.getHalves()[0];
      case FORWARD:
        return roadSection.getHalves()[1];
      default:
        throw new SerializationException("Unknown RoadSection.Half end in " + serialized.toString());
    }
  }

}
