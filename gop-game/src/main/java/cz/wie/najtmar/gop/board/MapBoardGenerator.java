package cz.wie.najtmar.gop.board;

import com.google.inject.Inject;

import cz.wie.najtmar.gop.common.Point2D;
import cz.wie.najtmar.gop.game.PropertiesReader;

import java.io.IOException;

import javax.annotation.Nonnull;

/**
 * Board generator for input maps.
 * @author najtmar
 */
public class MapBoardGenerator implements BoardGenerator {

  private final BoardGenerator fallBackBoardGenerator;
  private final BoardProperties boardProperties;

  @Inject
  public MapBoardGenerator(@Nonnull PropertiesReader propertiesReader) {
    this.fallBackBoardGenerator = new ClockBoardGenerator(propertiesReader);
    try {
      boardProperties = new BoardProperties(propertiesReader.getBoardProperties());
    } catch (IOException exception) {
      throw new BoardException("Reading Board Properties failed.", exception);
    }
  }

  @Override
  public Board generate() throws BoardException {
    if (boardProperties.getBackgroundImage().equals("/images/poland.jpg")
        || boardProperties.getBackgroundImage().equals("poland")) {
      final Board result = new Board(boardProperties.getPropertiesSet());
      final Joint szczecin = new Joint.Builder().setBoard(result).setIndex(0).setName("Szczecin")
          .setCoordinatesPair(new Point2D.Double(transformX(50 - 25), transformY(237) - 25)).build();
      result.addJoint(szczecin);
      final Joint gdansk = new Joint.Builder().setBoard(result).setIndex(1).setName("Gdańsk")
          .setCoordinatesPair(new Point2D.Double(transformX(435 - 25), transformY(87) - 25)).build();
      result.addJoint(gdansk);
      final Joint olsztyn = new Joint.Builder().setBoard(result).setIndex(2).setName("Olsztyn")
          .setCoordinatesPair(new Point2D.Double(transformX(608 - 25), transformY(177) - 25)).build();
      result.addJoint(olsztyn);
      final Joint bialystok = new Joint.Builder().setBoard(result).setIndex(3).setName("Białystok")
          .setCoordinatesPair(new Point2D.Double(transformX(861 - 25), transformY(279) - 25)).build();
      result.addJoint(bialystok);
      final Joint zielonaGora = new Joint.Builder().setBoard(result).setIndex(4).setName("Zielona Góra")
          .setCoordinatesPair(new Point2D.Double(transformX(141 - 25), transformY(464) - 25)).build();
      result.addJoint(zielonaGora);
      final Joint poznan = new Joint.Builder().setBoard(result).setIndex(5).setName("Poznań")
          .setCoordinatesPair(new Point2D.Double(transformX(272 - 25), transformY(394) - 25)).build();
      result.addJoint(poznan);
      final Joint bydgoszcz = new Joint.Builder().setBoard(result).setIndex(6).setName("Bydgoszcz")
          .setCoordinatesPair(new Point2D.Double(transformX(376 - 25), transformY(284) - 25)).build();
      result.addJoint(bydgoszcz);
      final Joint lodz = new Joint.Builder().setBoard(result).setIndex(7).setName("Łódź")
          .setCoordinatesPair(new Point2D.Double(transformX(512 - 25), transformY(490) - 25)).build();
      result.addJoint(lodz);
      final Joint warszawa = new Joint.Builder().setBoard(result).setIndex(8).setName("Warszawa")
          .setCoordinatesPair(new Point2D.Double(transformX(657 - 25), transformY(418) - 25)).build();
      result.addJoint(warszawa);
      final Joint wroclaw = new Joint.Builder().setBoard(result).setIndex(9).setName("Wrocław")
          .setCoordinatesPair(new Point2D.Double(transformX(284 - 25), transformY(590) - 25)).build();
      result.addJoint(wroclaw);
      final Joint opole = new Joint.Builder().setBoard(result).setIndex(10).setName("Opole")
          .setCoordinatesPair(new Point2D.Double(transformX(369 - 25), transformY(653) - 25)).build();
      result.addJoint(opole);
      final Joint katowice = new Joint.Builder().setBoard(result).setIndex(11).setName("Katowice")
          .setCoordinatesPair(new Point2D.Double(transformX(471 - 25), transformY(716) - 25)).build();
      result.addJoint(katowice);
      final Joint krakow = new Joint.Builder().setBoard(result).setIndex(12).setName("Kraków")
          .setCoordinatesPair(new Point2D.Double(transformX(559 - 25), transformY(745) - 25)).build();
      result.addJoint(krakow);
      final Joint kielce = new Joint.Builder().setBoard(result).setIndex(13).setName("Kielce")
          .setCoordinatesPair(new Point2D.Double(transformX(625 - 25), transformY(623) - 25)).build();
      result.addJoint(kielce);
      final Joint lublin = new Joint.Builder().setBoard(result).setIndex(14).setName("Lublin")
          .setCoordinatesPair(new Point2D.Double(transformX(805 - 25), transformY(569) - 25)).build();
      result.addJoint(lublin);
      final Joint rzeszow = new Joint.Builder().setBoard(result).setIndex(15).setName("Rzeszów")
          .setCoordinatesPair(new Point2D.Double(transformX(751 - 25), transformY(743) - 25)).build();
      result.addJoint(rzeszow);

      result.addRoadSection(
          new RoadSection.Builder().setBoard(result).setIndex(0).setJoints(szczecin, gdansk).build());
      result.addRoadSection(
          new RoadSection.Builder().setBoard(result).setIndex(1).setJoints(gdansk, bydgoszcz).build());
      result.addRoadSection(
          new RoadSection.Builder().setBoard(result).setIndex(2).setJoints(poznan, bydgoszcz).build());
      result.addRoadSection(
          new RoadSection.Builder().setBoard(result).setIndex(3).setJoints(bialystok, olsztyn).build());
      result.addRoadSection(
          new RoadSection.Builder().setBoard(result).setIndex(4).setJoints(szczecin, poznan).build());
      result.addRoadSection(
          new RoadSection.Builder().setBoard(result).setIndex(5).setJoints(bydgoszcz, olsztyn).build());
      result.addRoadSection(
          new RoadSection.Builder().setBoard(result).setIndex(6).setJoints(poznan, zielonaGora).build());
      result.addRoadSection(
          new RoadSection.Builder().setBoard(result).setIndex(7).setJoints(wroclaw, poznan).build());
      result.addRoadSection(
          new RoadSection.Builder().setBoard(result).setIndex(8).setJoints(bydgoszcz, lodz).build());
      result.addRoadSection(
          new RoadSection.Builder().setBoard(result).setIndex(9).setJoints(lodz, warszawa).build());
      result.addRoadSection(
          new RoadSection.Builder().setBoard(result).setIndex(10).setJoints(warszawa, bialystok).build());
      result.addRoadSection(
          new RoadSection.Builder().setBoard(result).setIndex(11).setJoints(wroclaw, zielonaGora).build());
      result.addRoadSection(
          new RoadSection.Builder().setBoard(result).setIndex(12).setJoints(opole, lodz).build());
      result.addRoadSection(
          new RoadSection.Builder().setBoard(result).setIndex(13).setJoints(kielce, warszawa).build());
      result.addRoadSection(
          new RoadSection.Builder().setBoard(result).setIndex(14).setJoints(warszawa, lublin).build());
      result.addRoadSection(
          new RoadSection.Builder().setBoard(result).setIndex(15).setJoints(wroclaw, opole).build());
      result.addRoadSection(
          new RoadSection.Builder().setBoard(result).setIndex(16).setJoints(opole, katowice).build());
      result.addRoadSection(
          new RoadSection.Builder().setBoard(result).setIndex(17).setJoints(katowice, kielce).build());
      result.addRoadSection(
          new RoadSection.Builder().setBoard(result).setIndex(18).setJoints(kielce, krakow).build());
      result.addRoadSection(
          new RoadSection.Builder().setBoard(result).setIndex(19).setJoints(krakow, rzeszow).build());
      result.addRoadSection(
          new RoadSection.Builder().setBoard(result).setIndex(20).setJoints(lublin, kielce).build());
      result.addRoadSection(
          new RoadSection.Builder().setBoard(result).setIndex(21).setJoints(lublin, rzeszow).build());

      return result;
    } else {
      return fallBackBoardGenerator.generate();
    }
  }

  float transformX(float x) {
    if (boardProperties.getLibGdx()) {
      return x + 25;
    }
    return x;
  }

  float transformY(float y) {
    if (boardProperties.getLibGdx()) {
      return 905 - y + 25;
    }
    return y;
  }

}
