package cz.wie.najtmar.gop.game;

import com.google.gson.JsonObject;
import cz.wie.najtmar.gop.common.Persistable;

import javax.annotation.Nonnull;
import java.util.UUID;

/**
 * User who take take part in a Game .
 * @author najtmar
 */
public class User implements Persistable {

  public static final String NAME_PROPERTY = "name";
  public static final String ID_PROPERTY = "id";
  /**
   * The name of the User .
   */
  private final String name;

  /**
   * Unique id of the User .
   */
  private final UUID id;

  public User(@Nonnull String name, @Nonnull UUID id) {
    this.name = name;
    this.id = id;
  }

  public User(JsonObject serialized) {
    this.name = serialized.getAsJsonPrimitive(NAME_PROPERTY).getAsString();
    this.id = UUID.fromString(serialized.getAsJsonPrimitive(ID_PROPERTY).getAsString());
  }

  public String getName() {
    return name;
  }

  public UUID getId() {
    return id;
  }

  @Override
  public int hashCode() {
    return id.hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof User)) {
      return false;
    }
    final User otherUser = (User) obj;
    return id.equals(otherUser.id) && name.equals(otherUser.name);
  }

  @Override
  public JsonObject snapshot() {
    JsonObject returnValue = new JsonObject();
    returnValue.addProperty(NAME_PROPERTY, name);
    returnValue.addProperty(ID_PROPERTY, id.toString());
    return returnValue;
  }
}
