package cz.wie.najtmar.gop.board;

import com.google.gson.JsonObject;
import cz.wie.najtmar.gop.common.Persistable;
import cz.wie.najtmar.gop.common.Point2D;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;

import static cz.wie.najtmar.gop.common.Persistable.serializeObject;

/**
 * Represents a Joint between RoadSections. Hashed by the Board-unique index.
 * @author najtmar
 */
public class Joint implements Persistable {

  public static final String INDEX_PROPERTY = "index";
  public static final String NAME_PROPERTY = "name";
  public static final String COORDINATES_PAIR_PROPERTY = "coordinatesPair";
  /**
   * Index of the Joint in the internal list of Joints in {@link Joint#board}.
   */
  private final int index;

  /**
   * Name of the Joint, by default assigned to the City built on the Joint .
   */
  private String name;

  /**
   * The Board to which the Joint belongs.
   */
  private final Board board;

  /**
   * (x, y) coordinates of the Joint.
   */
  private final Point2D.Double coordinatesPair;

  /**
   * All RoadSections adjacent to the Joint.
   */
  private final ArrayList<RoadSection> roadSections;

  /**
   * Position of the Joint, on the first RoadSection in roadSections. null if no RoadSection added yet.
   */
  private Position normalizedPosition;

  public int getIndex() {
    return index;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Board getBoard() {
    return board;
  }

  public Point2D.Double getCoordinatesPair() {
    return coordinatesPair;
  }

  public List<RoadSection> getRoadSections() {
    return roadSections;
  }

  public Joint(JsonObject serialized, Board board) {
    this.index = serialized.getAsJsonPrimitive(INDEX_PROPERTY).getAsInt();
    this.name = serialized.getAsJsonPrimitive(NAME_PROPERTY).getAsString();
    this.board = board;
    this.coordinatesPair = Persistable.deserialize(serialized.getAsJsonObject(COORDINATES_PAIR_PROPERTY), Point2D.Double.class);
    this.roadSections = new ArrayList<>();
  }

  /**
   * Adds roadSection to the Joint's RoadSections. Updates normalizedPosition if needed.
   * @param roadSection the RoadSection to be added
   * @throws BoardException when calculating normalizedPosition fails
   */
  public void addRoadSection(final RoadSection roadSection) throws BoardException {
    roadSections.add(roadSection);
    if (roadSections.size() == 1) {
      normalizedPosition = new Position(roadSection, roadSection.getJointIndex(this));
    }
  }

  /**
   * Returns the normalized Position of the Joint.
   * @return the normalized Position of the Joint
   */
  public Position getNormalizedPosition() {
    return normalizedPosition;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Joint)) {
      return false;
    }
    return index == ((Joint) obj).index;
  }

  @Override
  public int hashCode() {
    return index;
  }

  @Override
  public String toString() {
    return coordinatesPair.toString();
  }

  /**
   * Builder class.
   */
  public static class Builder {
    private int index = -1;
    private String name;
    private Board board;
    Point2D.Double coordinatesPair;

    public Builder setIndex(int index) {
      this.index = index;
      return this;
    }

    public Builder setName(String name) {
      this.name = name;
      return this;
    }

    public Builder setBoard(Board board) {
      this.board = board;
      return this;
    }

    public Builder setCoordinatesPair(Point2D.Double coordinatesPair) {
      this.coordinatesPair = coordinatesPair;
      return this;
    }

    /**
     * Build an instance of a Joint.
     * @return an instance of a Joint
     * @throws BoardException when Board cannot be created
     */
    public Joint build() throws BoardException {
      if (index == -1) {
        throw new BoardException("index not set.");
      }
      if (name == null) {
        throw new BoardException("name not set.");
      }
      if (board == null) {
        throw new BoardException("Board not set.");
      }
      if (coordinatesPair == null) {
        throw new BoardException("Position not set.");
      }
      return new Joint(index, name, board, coordinatesPair);
    }
  }

  private Joint(int index, @Nonnull String name, @Nonnull Board board, @Nonnull Point2D.Double coordinatesPair) {
    this.index = index;
    this.name = name;
    this.board = board;
    this.coordinatesPair = coordinatesPair;
    this.roadSections = new ArrayList<>();
  }

  @Override
  public JsonObject snapshot() {
    JsonObject returnValue = new JsonObject();
    returnValue.addProperty(INDEX_PROPERTY, index);
    returnValue.addProperty(NAME_PROPERTY, name);
    returnValue.add(COORDINATES_PAIR_PROPERTY, serializeObject(coordinatesPair));
    // roadSections set blank
    // normalizedPosition set blank
    return returnValue;
  }

}
