package cz.wie.najtmar.gop.game;

import com.google.gson.JsonObject;
import cz.wie.najtmar.gop.board.*;
import cz.wie.najtmar.gop.board.RoadSection.Direction;
import cz.wie.najtmar.gop.common.Persistable;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Arrays;

import static cz.wie.najtmar.gop.common.Persistable.serializeArray;

/**
 * Itinerary of a Prawn with the current position.
 * @author najtmar
 */
public class Itinerary implements Iterable<DirectedPosition>, Persistable {

  public static final String NODES_PROPERTY = "nodes";
  public static final String CURRENT_NODE_INDEX_PROPERTY = "currentNodeIndex";
  public static final String CURRENT_POSITION_INDEX_PROPERTY = "currentPositionIndex";

  /**
   * Iterator to iterate over DirectedPositions in the Itinerary.
   * @author najtmar
   */
  public class Iterator implements java.util.Iterator<DirectedPosition> {

    /**
     * Index in array of nodes which determines the RoadSection on which the Iterator is currently located.
     * -1 if next() hasn't been invoked yet.
     */
    private int currentNodeIndex;

    /**
     * Index of the position on the current RoadSection on which the Prawn is currently located.
     * -1 if next() hasn't been invoked yet.
     */
    private int currentPositionIndex;

    /**
     * Index in array of nodes which determines the RoadSection on which the Iterator was located before the last
     * invocation of next(). -1 if next() was invoked only less than two times.
     */
    private int previousNodeIndex;

    /**
     * Index of the position on the current RoadSection on which the Prawn was located before the last invocation of
     * next(). -1 if next() was invoked less than two times.
     */
    private int previousPositionIndex;

    @Override
    public boolean hasNext() {
      // At the beginning, there is always at least one element to return.
      if (currentNodeIndex == -1) {
        return true;
      }
      if (currentNodeIndex == nodes.length - 1
          && currentPositionIndex == nodes[nodes.length - 1].getPosition().getIndex()) {
        return false;
      }
      return true;
    }

    @Override
    public DirectedPosition next() {
      if (!hasNext()) {
        return null;
      }

      // Advance currentPositionIndex .
      if (currentNodeIndex == -1) {
        // Initialize the current indexes.
        currentNodeIndex = Itinerary.this.currentNodeIndex;
        currentPositionIndex = Itinerary.this.currentPositionIndex;
      } else {
        previousNodeIndex = currentNodeIndex;
        previousPositionIndex = currentPositionIndex;

        if (nodes[currentNodeIndex].getDirection() == RoadSection.Direction.FORWARD) {
          ++currentPositionIndex;
        } else {
          --currentPositionIndex;
        }
      }

      try {
        // If needed, advance currentNodeIndex and adjust currentPositionIndex accordingly.
        final Position currentPosition =
            new Position(nodes[currentNodeIndex].getPosition().getRoadSection(), currentPositionIndex);
        final DirectedPosition nextNode = nodes[currentNodeIndex + 1];
        if (nextNode.getPosition().equals(currentPosition)) {
          ++currentNodeIndex;
          currentPositionIndex = nodes[currentNodeIndex].getPosition().getIndex();
        }

        return new DirectedPosition(nodes[currentNodeIndex].getPosition().getRoadSection(), currentPositionIndex,
            nodes[currentNodeIndex].getDirection());
      } catch (BoardException exception) {
        throw new RuntimeException("Should never happen.", exception);
      }
    }

    /**
     * Returns true iff next() was called at least once.
     * @return true iff next() was called at least once
     */
    public boolean hasPrevious() {
      return currentNodeIndex != -1;
    }

    /**
     * Returns the DirectedPosition after the previous call to next() or null if next() was called less than once.
     * @return previous DirectedPosition or null
     */
    public DirectedPosition previous() {
      if (!hasPrevious()) {
        return null;
      }
      try {
        return new DirectedPosition(nodes[currentNodeIndex].getPosition().getRoadSection(), currentPositionIndex,
            nodes[currentNodeIndex].getDirection());
      } catch (BoardException exception) {
        throw new RuntimeException("This should never happen.", exception);
      }
    }

    private Iterator() {
      this.currentNodeIndex = -1;
      this.currentPositionIndex = -1;
      this.previousNodeIndex = -1;
      this.previousPositionIndex = -1;
    }

  }

  /**
   * Significant points on the Itinerary. The first and the last element represent the beginning and the end of
   * the Itinerary, respectively. All the other Positions are in Joints. Each two adjacent Positions are on adjacent
   * RoadSections, according to the directions.
   */
  private final DirectedPosition[] nodes;

  /**
   * Index in array of nodes which determines the RoadSection on which the Prawn is currently located.
   */
  private int currentNodeIndex;

  /**
   * Index of the position on the current RoadSection on which the Prawn is currently located.
   */
  private int currentPositionIndex;

  public Itinerary(JsonObject serialized, Board board) {
    this.nodes = Persistable.deserializeStream(serialized.getAsJsonArray(NODES_PROPERTY), el -> new DirectedPosition(el.getAsJsonObject(), board))
            .toArray(DirectedPosition[]::new);
    this.currentNodeIndex = serialized.getAsJsonPrimitive(CURRENT_NODE_INDEX_PROPERTY).getAsInt();
    this.currentPositionIndex = serialized.getAsJsonPrimitive(CURRENT_POSITION_INDEX_PROPERTY).getAsInt();
  }

  @Override
  public java.util.Iterator<DirectedPosition> iterator() {
    return new Iterator();
  }

  /**
   * Returns an instance of the dedicated Iterator.
   * @return an instance of the dedicated Iterator
   */
  public Iterator itineraryIterator() {
    return new Iterator();
  }

  /**
   * Returns the DirectedPosition at which the Itinerary currently is.
   * @return current DirectedPosition
   * @throws BoardException if the position cannot be created
   */
  public DirectedPosition getCurrentPosition() throws BoardException {
    return new DirectedPosition(nodes[currentNodeIndex].getPosition().getRoadSection(), currentPositionIndex,
        nodes[currentNodeIndex].getDirection());
  }

  public DirectedPosition[] getNodes() {
    return nodes;
  }

  /**
   * Returns true iff the Itinerary is currently at its final position.
   * @return true iff the Itinerary is currently at its final position
   */
  public boolean isFinished() {
    if (currentNodeIndex == nodes.length - 1
        && currentPositionIndex == nodes[nodes.length - 1].getPosition().getIndex()) {
      return true;
    }
    return false;
  }

  /**
   * Sets the Itinerary to the current DirectedPosition pointed to by iterator. iterator should have called next() at
   * least once.
   * @param iterator points to the current DirectedPosition to be set
   * @throws GameException if the iterator didn't call next() at least once
   */
  public void setCurrentPosition(Iterator iterator) throws GameException {
    if (iterator.currentNodeIndex == -1) {
      throw new GameException("Iterator should have called next() at least once.");
    }
    this.currentNodeIndex = iterator.currentNodeIndex;
    this.currentPositionIndex = iterator.currentPositionIndex;
  }

  /**
   * Sets the Itinerary to the previous DirectedPosition pointed to by iterator. iterator should have called next() at
   * least two times.
   * @param iterator points to the previous DirectedPosition to be set
   * @throws GameException if the iterator didn't call next() at least two times
   */
  public void setPreviousPosition(Iterator iterator) throws GameException {
    if (iterator.previousNodeIndex == -1) {
      throw new GameException("Iterator should have called next() at least two times.");
    }
    this.currentNodeIndex = iterator.previousNodeIndex;
    this.currentPositionIndex = iterator.previousPositionIndex;
  }

  /**
   * Thrown when an illegal operation is performed while building an Itinerary.
   * @author najtmar
   */
  public static class ItineraryBuildingException extends Exception {

    private static final long serialVersionUID = -3386835692092690357L;

    public ItineraryBuildingException() {
      super();
    }

    public ItineraryBuildingException(String message, Throwable cause, boolean enableSuppression,
        boolean writableStackTrace) {
      super(message, cause, enableSuppression, writableStackTrace);
    }

    public ItineraryBuildingException(String message, Throwable cause) {
      super(message, cause);
    }

    public ItineraryBuildingException(String message) {
      super(message);
    }

    public ItineraryBuildingException(Throwable cause) {
      super(cause);
    }

  }

  @Override
  public String toString() {
    return "Itinerary [nodes=" + Arrays.toString(nodes) + ", currentNodeIndex=" + currentNodeIndex
        + ", currentPositionIndex=" + currentPositionIndex + "]";
  }

  public static class Builder {

    private final ArrayList<Position> positions = new ArrayList<Position>();

    /**
     * Adds a Position to the Itinerary being built.
     * @param position the Position to be added to the Itinerary
     * @return the current instance of Builder
     * @throws ItineraryBuildingException when an illegal operation is performed
     * @throws GameException when something unexpected happens
     */
    public Builder addPosition(@Nonnull final Position position) throws ItineraryBuildingException, GameException {
      doAddPosition(position);
      return this;
    }

    /**
     * Implementation of method addNode(). Also used in method removeLastPosition().
     * @param position the Position to be added to the Itinerary
     * @return the current instance of Builder
     * @throws ItineraryBuildingException when an illegal operation is performed
     * @throws GameException when something unexpected happens
     */
    private void doAddPosition(@Nonnull final Position position) throws ItineraryBuildingException, GameException {
      if (positions.isEmpty()) {
        positions.add(position);
        return;
      }
      if (positions.get(positions.size() - 1).equals(position)) {
        return;
      }

      // Project previous and current Positions so that they are on the same RoadSection.
      Position previousPosition;
      Position currentPosition = position;
      try {
        previousPosition = position.getRoadSection().getProjectedPosition(positions.get(positions.size() - 1));
        if (previousPosition == null) {
          previousPosition = positions.get(positions.size() - 1);
          currentPosition = previousPosition.getRoadSection().getProjectedPosition(position);
        }
        if (currentPosition == null) {
          final Joint previousPositionJoint = previousPosition.getJoint();
          if (previousPositionJoint != null) {
            for (RoadSection roadSection : previousPositionJoint.getRoadSections()) {
              currentPosition = roadSection.getProjectedPosition(position);
              if (currentPosition != null) {
                previousPosition = roadSection.getProjectedPosition(previousPosition);
                break;
              }
            }
          }
        }
        if (currentPosition == null) {
          throw new ItineraryBuildingException("Invalid Position pair: (Position " + (positions.size() - 1) + ": "
              + previousPosition + ", Position " + positions.size() + ": " + position + ").");
        }
      } catch (BoardException exception) {
        throw new GameException("Something unexpected happened.", exception);
      }

      positions.set(positions.size() - 1, previousPosition);
      positions.add(currentPosition);
      return;
    }

    /**
     * Removes the last Position added with addPosition().
     * @return the current instance of Builder
     * @throws ItineraryBuildingException when the list of Positions added is empty
     * @throws GameException when something unexpected happens
     */
    public Builder removeLastPosition() throws ItineraryBuildingException, GameException {
      if (positions.isEmpty()) {
        throw new ItineraryBuildingException("Positions list is empty.");
      }
      positions.remove(positions.size() - 1);
      if (!positions.isEmpty()) {
        final Position lastPosition = positions.remove(positions.size() - 1);
        doAddPosition(lastPosition);
      }
      return this;
    }

    /**
     * Compresses the internal list positions by removing those that are not needed.
     * @throws GameException when something unexpected happens
     */
    private void removeSuperflousPositions() throws GameException {
      for (int i = 1; i < positions.size() - 1;) {
        final Position projectedPreviousPosition;
        final Position projectedNextPosition;
        try {
          projectedPreviousPosition = positions.get(i).getRoadSection().getProjectedPosition(positions.get(i - 1));
          projectedNextPosition = positions.get(i).getRoadSection().getProjectedPosition(positions.get(i + 1));
        } catch (BoardException exception) {
          throw new GameException("Something unexpected happened.", exception);
        }
        if (projectedPreviousPosition != null && projectedNextPosition != null) {
          if ((projectedPreviousPosition.getIndex() < positions.get(i).getIndex()
              && positions.get(i).getIndex() < projectedNextPosition.getIndex())
              || ((projectedNextPosition.getIndex() < positions.get(i).getIndex()
                  && positions.get(i).getIndex() < projectedPreviousPosition.getIndex()))) {
            positions.remove(i);
            continue;
          }
        }
        ++i;
      }
    }

    /**
     * Builds an Itinerary based on lists nodes and positions.
     * @return the Itinerary built
     * @throws ItineraryBuildingException when the Itinerary cannot be built
     * @throws GameException when something unexpected happens
     */
    public Itinerary build() throws ItineraryBuildingException, GameException {
      if (positions.size() < 2) {
        throw new ItineraryBuildingException("Too few Positions specified to build an Itinerary (" + positions.size()
            + ").");
      }
      final ArrayList<DirectedPosition> nodes = new ArrayList<DirectedPosition>();
      removeSuperflousPositions();
      for (int i = 1; i < positions.size(); ++i) {
        final Position previousPosition = positions.get(i - 1);
        final Position projectedPosition;
        try {
          projectedPosition = previousPosition.getRoadSection().getProjectedPosition(positions.get(i));
        } catch (BoardException exception) {
          throw new GameException("Something unexpected happened.", exception);
        }
        RoadSection.Direction direction;
        if (previousPosition.getIndex() < projectedPosition.getIndex()) {
          direction = Direction.FORWARD;
        } else {
          direction = Direction.BACKWARD;
        }
        nodes.add(new DirectedPosition(previousPosition, direction));
        if (i == positions.size() - 1) {
          nodes.add(new DirectedPosition(projectedPosition, direction));
        }
      }
      return new Itinerary(nodes.toArray(new DirectedPosition[] {}));
    }

  }

  private Itinerary(@Nonnull DirectedPosition[] nodes) throws GameException {
    if (nodes.length <= 1) {
      throw new GameException("Itinerary must have at least two nodes (" + nodes.length + " found).");
    }
    for (int i = 1; i < nodes.length - 1; ++i) {
      Joint previousNodeJoint;
      if (nodes[i - 1].getDirection() == RoadSection.Direction.FORWARD) {
        previousNodeJoint = nodes[i - 1].getPosition().getRoadSection().getJoints()[1];
      } else {
        previousNodeJoint = nodes[i - 1].getPosition().getRoadSection().getJoints()[0];
      }
      Joint nextNodeJoint;
      if (nodes[i].getDirection() == RoadSection.Direction.FORWARD) {
        nextNodeJoint = nodes[i].getPosition().getRoadSection().getJoints()[0];
      } else {
        nextNodeJoint = nodes[i].getPosition().getRoadSection().getJoints()[1];
      }
      if (!previousNodeJoint.equals(nextNodeJoint)) {
        throw new GameException("Two consecutive intermediate nodes in an Itinerary must have adjacent "
            + "RoadSections (node " + (i - 1) + ": " + nodes[i - 1] + " and node " + i + ": " + nodes[i] + ").");
      }
    }

    if ((nodes[0].getDirection() == RoadSection.Direction.FORWARD
        && nodes[0].getPosition().getIndex() == nodes[0].getPosition().getRoadSection().getJointPositionIndexes()[1])
        ||
        (nodes[0].getDirection() == RoadSection.Direction.BACKWARD
        && nodes[0].getPosition().getIndex()
           == nodes[0].getPosition().getRoadSection().getJointPositionIndexes()[0])) {
      throw new GameException("First Itinerary node must not start at the last position of a RoadSection.");
    }

    for (int i = 0; i < nodes.length - 1; ++i) {
      final Position currentNodePosition = nodes[i].getPosition();
      final Position nextNodePosition = nodes[i + 1].getPosition();
      final RoadSection currentNodeRoadSection = currentNodePosition.getRoadSection();
      Position nextNodeProjectedPosition = null;
      try {
        nextNodeProjectedPosition = currentNodeRoadSection.getProjectedPosition(nextNodePosition);
      } catch (BoardException exception) {
        throw new GameException("Error projecting " + nextNodePosition + " on " + currentNodeRoadSection + ".",
            exception);
      }
      boolean continuousItinerary;
      if (nextNodeProjectedPosition != null) {
        if (nodes[i].getDirection() == RoadSection.Direction.FORWARD) {
          continuousItinerary = currentNodePosition.getIndex() <= nextNodeProjectedPosition.getIndex();
        } else {
          continuousItinerary = nextNodeProjectedPosition.getIndex() <= currentNodePosition.getIndex();
        }
      } else {
        continuousItinerary = false;
      }
      if (!continuousItinerary) {
        throw new GameException("Non-continuous Itinerary (node " + i + ": " + nodes[i] + ", node " + (i + 1) + ": "
            + nodes[i + 1] + ").");
      }
    }

    if (!nodes[nodes.length - 1].getPosition().getRoadSection().equals(
        nodes[nodes.length - 2].getPosition().getRoadSection())) {
      throw new GameException("Last and next to last Itinerary nodes must be on the same RoadSection.");
    }
    if (nodes[nodes.length - 1].getPosition().getIndex()
        == nodes[nodes.length - 2].getPosition().getIndex()) {
      throw new GameException("Last and next to last Itinerary nodes must not be at the same Position.");
    }
    if (nodes[nodes.length - 1].getDirection() != nodes[nodes.length - 2].getDirection()) {
      throw new GameException("Last and next to last Itinerary nodes must have the same Direction.");
    }
    this.nodes = nodes;
    this.currentNodeIndex = 0;
    this.currentPositionIndex = nodes[0].getPosition().getIndex();
  }

  @Override
  public JsonObject snapshot() {
    JsonObject returnValue = new JsonObject();
    returnValue.add(NODES_PROPERTY, serializeArray(nodes));
    returnValue.addProperty(CURRENT_NODE_INDEX_PROPERTY, currentNodeIndex);
    returnValue.addProperty(CURRENT_POSITION_INDEX_PROPERTY, currentPositionIndex);
    return returnValue;
  }

}
