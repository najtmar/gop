package cz.wie.najtmar.gop.game;

import com.google.gson.JsonObject;
import cz.wie.najtmar.gop.board.BoardException;
import cz.wie.najtmar.gop.board.DirectedPosition;
import cz.wie.najtmar.gop.board.Position;
import cz.wie.najtmar.gop.common.Persistable;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import java.util.LinkedList;

import static cz.wie.najtmar.gop.common.Persistable.serializeNullable;

/**
 * Represents a Prawn (either a Warrior or a SettlersUnit). Hashed by unique id.
 * @author najtmar
 */
public abstract class Prawn implements Comparable<Prawn>, Persistable {

  /**
   * Maximum energy level for any Prawn.
   */
  public static final double MAX_ENERGY = 1.0;
  public static final String PLAYER_PROPERTY = "player";
  public static final String ID_PROPERTY = "id";
  public static final String CURRENT_POSITION_PROPERTY = "currentPosition";
  public static final String ITINERARY_PROPERTY = "itinerary";
  public static final String LAST_MOVE_TIME_PROPERTY = "lastMoveTime";
  public static final String ENERGY_PROPERTY = "energy";

  /**
   * Properties of the Game in which the Prawn takes part.
   */
  private final GameProperties gameProperties;

  /**
   * Player to which the Prawn belongs.
   */
  private final Player player;

  /**
   * Unique ID of the Prawn.
   */
  private final int id;

  /**
   * Current Position of the Prawn.
   */
  private Position currentPosition;

  /**
   * Itinerary of the Prawn. null if the itinerary is not set.
   */
  private Itinerary itinerary;

  /**
   * Last time when a Move was performed.
   */
  private long lastMoveTime;

  /**
   * Current energy level for the Prawn. Can take values [0, MAX_ENERGY].
   */
  private double energy;

  /**
   * First ID that is available for assignment to Prawns.
   */
  static int firstAvailableId;

  public GameProperties getGameProperties() {
    return gameProperties;
  }

  public Player getPlayer() {
    return player;
  }

  public int getId() {
    return id;
  }

  public double getEnergy() {
    return energy;
  }

  /**
   * Increases the Prawn energy by delta. The energy is capped to MAX_ENERGY. Returns true iff the energy reaches
   * MAX_ENERGY.
   * @param delta the value by which the energy should be increased.
   * @return true if the energy reaches MAX_ENERGY, false otherwise
   */
  public boolean increaseEnergy(@Nonnegative double delta) {
    energy += delta;
    if (energy > MAX_ENERGY) {
      energy = MAX_ENERGY;
    }
    return energy == MAX_ENERGY;
  }

  /**
   * Decreases the Prawn energy by delta. The energy is capped to 0.0. Returns true iff the energy reaches 0.0.
   * @param delta the value by which the energy should be decreased.
   * @return true if the energy reaches 0.0, false otherwise
   */
  public boolean decreaseEnergy(@Nonnegative double delta) {
    energy -= delta;
    if (energy < 0.0) {
      energy = 0.0;
    }
    return energy == 0.0;
  }

  /**
   * Generates incremental unique ID and increases the internal ID count.
   * @return incremental unique ID
   */
  private static synchronized int generateUniqueIncrementalId() {
    return firstAvailableId++;
  }

  /**
   * Resets firstAvailableId to 0. Used in tests.
   */
  public static synchronized void resetFirstAvailableId() {
    firstAvailableId = 0;
  }

  public Position getCurrentPosition() {
    return currentPosition;
  }

  /**
   * Used in tests.
   * @param currentPosition the Position to set
   */
  void setCurrentPosition(Position currentPosition) {
    this.currentPosition = currentPosition;
  }

  public Itinerary getItinerary() {
    return itinerary;
  }

  /**
   * Sets Itinerary after performing all necessary validity checks. nextPosition is set to null.
   * @param itinerary Itinerary to be set
   * @throws GameException if the itinerary to be set is invalid for the Prawn
   * @throws BoardException when a node in the itinerary is invalid; should never be thrown
   */
  public void setItinerary(Itinerary itinerary) throws GameException, BoardException {
    if (itinerary.isFinished()) {
      throw new GameException("Cannot set finished Itinerary.");
    }
    if (!currentPosition.equals(itinerary.getCurrentPosition().getPosition())) {
      throw new GameException("Itinerary must start in the current Prawn position (Prawn position: "
          + currentPosition + ", Itinerary position: " + itinerary.getCurrentPosition().getPosition()
          + ").");
    }
    this.itinerary = itinerary;
    this.currentPosition = itinerary.getCurrentPosition().getPosition();
  }

  /**
   * Clears the Itinerary of the Prawn as well as the nextPosition.
   */
  public void clearItinerary() {
    this.itinerary = null;
  }

  public long getLastMoveTime() {
    return lastMoveTime;
  }

  protected void setLastMoveTime(long lastMoveTime) {
    this.lastMoveTime = lastMoveTime;
  }

  /**
   * Given the current Itinerary, prepare a sequence of consecutive Moves that the Prawn would perform until (inclusive)
   * the specified moveTime. Irrespective of the moveTime and whether the Itinerary is set or not, the sequence always
   * contains at least one move (with index 0), which is to its current Position. Iff the last Move ends the Itinerary,
   * then it has time set to moveTime and stop set to true.
   * @param moveTime time limit (inclusive) for the Move sequence
   * @return sequence of Moves the Prawn would perform within the specified time limit
   */
  public Move[] prepareMoveSequence(long moveTime) {
    if (itinerary == null) {
      return new Move[]{new Move(currentPosition, moveTime, true)};
    }
    LinkedList<Move> moveSequence = new LinkedList<>();
    Itinerary.Iterator iter = itinerary.itineraryIterator();
    double timeDelta = 0.0;
    long previousTime = -1;
    for (; iter.hasNext() && lastMoveTime + timeDelta <= moveTime;) {
      if (iter.hasPrevious()) {
        moveSequence.add(new Move(iter.previous().getPosition(), previousTime, false));
      }
      previousTime = (long) (lastMoveTime + timeDelta);
      final DirectedPosition directedPosition = iter.next();
      final Position position = directedPosition.getPosition();
      timeDelta += position.getRoadSection().getStepSize() / calculateVelocity(directedPosition);
    }
    if (iter.hasNext()) {
      moveSequence.add(new Move(iter.previous().getPosition(), previousTime, false));
    } else {
      moveSequence.add(new Move(iter.previous().getPosition(), moveTime, true));
    }
    return moveSequence.toArray(new Move[]{});
  }

  /**
   * Makes Prawn perform the move, which must be the next move in the itinerary. Itinerary is updated accordingly and
   * the internal marker lastMoveTime is set to the value denoted by the move to perform. If the action to perform is
   * to perform the last Move in the itinerary, then the Prawn stops and its Itinerary is set to null. moveTime denotes
   * the time when the action ends and is provided for validation purposes--must not be smaller than the time of
   * the Move to perform.
   * @param move the Move to perform
   * @param moveTime the time of the action
   * @throws GameException when Move action specification is illegal
   */
  public void move(@Nonnull Move move, @Nonnegative long moveTime) throws GameException {
    if (this.itinerary == null) {
      throw new GameException("Prawn without Itinerary cannot move: " + this);
    }
    if (moveTime < move.getMoveTime()) {
      throw new GameException("Move time smaller than the time of the move to perform (" + moveTime + " < "
          + move.getMoveTime() + ").");
    }
    this.currentPosition = move.getPosition();
    this.lastMoveTime = move.getMoveTime();

    // Update the Itinerary.
    Itinerary.Iterator iter = itinerary.itineraryIterator();
    iter.next();
    if (!iter.hasNext()) {
      throw new GameException("Cannot move " + this + " because its Itinerary is finished.");
    }
    iter.next();
    itinerary.setCurrentPosition(iter);
    try {
      if (!itinerary.getCurrentPosition().getPosition().equals(move.getPosition())) {
        throw new GameException("Itinerary of " + this + " should be updated to Position " + move.getPosition()
            + " (found " + itinerary.getCurrentPosition().getPosition() + ").");
      }
    } catch (BoardException exception) {
      throw new GameException("Itinerary processing failed.", exception);
    }

    if (move.isStop()) {
      if (!itinerary.isFinished()) {
        throw new GameException("Move marked as stopping must finish the Itinerary (" + this + ", " + move + ").");
      }
      itinerary = null;
    } else {
      if (itinerary.isFinished()) {
        throw new GameException("Move that finishes an Itinerary must be explicitly marked as stopping (found "
            + this + ", " + move + ").");
      }
    }
  }

  /**
   * Orders the Prawn to stay in place. Updates lastMoveTime. Neither the current Position nor the next Position change.
   * @param currentTime time to which lastMoveTime should be updated
   */
  public void stay(long currentTime) {
    this.lastMoveTime = currentTime;
  }

  /**
   * Calculates the velocity that the Prawn has at the given directedPosition.
   * @param directedPosition the DirectedPosition at which the velocity is calculated
   * @return velocity that the Prawn has at the given DirectedPosition
   */
  protected abstract double calculateVelocity(@Nonnull DirectedPosition directedPosition);

  /**
   * Calculate the strength of an attack performed with the given directedPosition.
   * @param directedPosition specifies the Position from which at the Direction in which the attack is performed;
   *     the Position must be equal to the current Prawn position, although the RoadSection may differ
   * @return a non-negative number denoting the strength of the attack
   * @throws GameException when the argument is incorrect
   */
  @Nonnegative
  public abstract double calculateAttackStrength(@Nonnull DirectedPosition directedPosition) throws GameException;

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Prawn)) {
      return false;
    }
    final Prawn prawn = (Prawn) obj;
    return this.gameProperties.equals(prawn.getGameProperties()) && this.player.equals(prawn.getPlayer())
        && this.id == prawn.id;
  }

  @Override
  public int hashCode() {
    return id;
  }

  @Override
  public int compareTo(Prawn otherPrawn) {
    return Integer.compare(id,  otherPrawn.id);
  }

  @Override
  public String toString() {
    return "(" + player + ", " + id + ")";
  }

  protected Prawn(@Nonnull GameProperties gameProperties, @Nonnull Player player, @Nonnull Position currentPosition,
      @Nonnegative long creationTime) throws GameException {
    this.gameProperties = gameProperties;
    this.player = player;
    this.id = generateUniqueIncrementalId();
    final int directedPositionIndex = currentPosition.getIndex();
    final int[] jointIndexes = currentPosition.getRoadSection().getJointPositionIndexes();
    if (directedPositionIndex != jointIndexes[0] && directedPositionIndex != jointIndexes[1]) {
      throw new GameException("Newly created Prawn must be located on a Joint (Position " + directedPositionIndex
          + " found).");
    }
    this.currentPosition = currentPosition;
    this.energy = MAX_ENERGY;
    this.lastMoveTime = creationTime;
  }

  protected Prawn(JsonObject serialized, Game game) {
    this.gameProperties = game.getGameProperties();
    this.player = game.getPlayers().get(serialized.getAsJsonPrimitive(PLAYER_PROPERTY).getAsInt());
    this.id = serialized.getAsJsonPrimitive(ID_PROPERTY).getAsInt();
    Prawn.firstAvailableId = Integer.max(Prawn.firstAvailableId, this.id + 1);
    this.currentPosition = new Position(serialized.getAsJsonObject(CURRENT_POSITION_PROPERTY), game.getBoard());
    this.itinerary = Persistable.deserializeNullable(serialized, ITINERARY_PROPERTY, JsonObject::getAsJsonObject)
            .map(object -> new Itinerary(object, game.getBoard()))
            .orElse(null);
    this.lastMoveTime = serialized.getAsJsonPrimitive(LAST_MOVE_TIME_PROPERTY).getAsLong();
    this.energy = serialized.getAsJsonPrimitive(ENERGY_PROPERTY).getAsDouble();
  }

  @Override
  public JsonObject snapshot() {
    JsonObject returnValue = new JsonObject();
    returnValue.addProperty(PLAYER_PROPERTY, player.getIndex());
    returnValue.addProperty(ID_PROPERTY, id);
    returnValue.add(CURRENT_POSITION_PROPERTY, currentPosition.snapshot());
    returnValue.add(ITINERARY_PROPERTY, serializeNullable(itinerary, Itinerary::snapshot));
    returnValue.addProperty(LAST_MOVE_TIME_PROPERTY, lastMoveTime);
    returnValue.addProperty(ENERGY_PROPERTY, energy);
    return returnValue;
  }

}
