package cz.wie.najtmar.gop.board;

import com.google.inject.Inject;

import cz.wie.najtmar.gop.common.Point2D;
import cz.wie.najtmar.gop.game.PropertiesReader;

import java.io.IOException;
import java.util.Properties;

import javax.annotation.Nonnull;

/**
 * Generates a simple Board. Used for tests and sample executions.
 * @author najtmar
 */
public class SimpleBoardGenerator implements BoardGenerator {

  private final PropertiesReader propertiesReader;

  @Inject
  SimpleBoardGenerator(@Nonnull PropertiesReader propertiesReader) {
    this.propertiesReader = propertiesReader;
  }

  @Override
  public Board generate() throws BoardException {
    final Properties boardPropertiesSet;
    try {
      boardPropertiesSet = propertiesReader.getBoardProperties();
    } catch (IOException exception) {
      throw new BoardException("Reading Board Properties failed.", exception);
    }
    final Board board = new Board(boardPropertiesSet);
    final BoardProperties boardProperties = new BoardProperties(boardPropertiesSet);
    final int xsize = boardProperties.getXsize();
    final int ysize = boardProperties.getYsize();
    final Joint joint0 = (new Joint.Builder())
        .setBoard(board)
        .setCoordinatesPair(new Point2D.Double(xsize / 3, ysize * 2 / 10))
        .setIndex(0)
        .setName("joint0")
        .build();
    final Joint joint1 = (new Joint.Builder())
        .setBoard(board)
        .setCoordinatesPair(new Point2D.Double(xsize * 2 / 3, ysize * 2 / 10))
        .setIndex(1)
        .setName("joint1")
        .build();
    final Joint joint2 = (new Joint.Builder())
        .setBoard(board)
        .setCoordinatesPair(new Point2D.Double(xsize * 2 / 3, ysize * 8 / 10))
        .setIndex(2)
        .setName("joint2")
        .build();
    final Joint joint3 = (new Joint.Builder())
        .setBoard(board)
        .setCoordinatesPair(new Point2D.Double(xsize / 3, ysize * 8 / 10))
        .setIndex(3)
        .setName("joint3")
        .build();
    final RoadSection roadSection0 = (new RoadSection.Builder())
        .setBoard(board)
        .setIndex(0)
        .setJoints(joint0, joint1)
        .build();
    final RoadSection roadSection1 = (new RoadSection.Builder())
        .setBoard(board)
        .setIndex(1)
        .setJoints(joint2, joint1)
        .build();
    final RoadSection roadSection2 = (new RoadSection.Builder())
        .setBoard(board)
        .setIndex(2)
        .setJoints(joint3, joint2)
        .build();
    final RoadSection roadSection3 = (new RoadSection.Builder())
        .setBoard(board)
        .setIndex(3)
        .setJoints(joint3, joint0)
        .build();
    final RoadSection roadSection4 = (new RoadSection.Builder())
        .setBoard(board)
        .setIndex(4)
        .setJoints(joint2, joint0)
        .build();
    board.addJoint(joint0);
    board.addJoint(joint1);
    board.addJoint(joint2);
    board.addJoint(joint3);
    board.addRoadSection(roadSection0);
    board.addRoadSection(roadSection1);
    board.addRoadSection(roadSection2);
    board.addRoadSection(roadSection3);
    board.addRoadSection(roadSection4);
    return board;
  }

}
