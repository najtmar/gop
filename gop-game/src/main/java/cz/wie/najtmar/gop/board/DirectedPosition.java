package cz.wie.najtmar.gop.board;

import com.google.gson.JsonObject;
import cz.wie.najtmar.gop.common.Persistable;

import javax.annotation.Nonnull;

/**
 * Prawn's Position together with the move direction on a RoadSection.
 * @author najtmar
 */
public class DirectedPosition implements Persistable {

  public static final String POSITION_PROPERTY = "position";
  public static final String DIRECTION_PROPERTY = "direction";
  /**
   * Position.
   */
  private final Position position;

  /**
   * Direction on the RoadSection on which position is located.
   */
  private final RoadSection.Direction direction;

  public DirectedPosition(@Nonnull RoadSection roadSection, int index, RoadSection.Direction direction)
      throws BoardException {
    this.position = new Position(roadSection, index);
    this.direction = direction;
  }

  public DirectedPosition(@Nonnull Position position, @Nonnull RoadSection.Direction direction) {
    this.position = position;
    this.direction = direction;
  }

  public DirectedPosition(JsonObject serialized, Board board) {
    this.position = new Position(serialized.getAsJsonObject(POSITION_PROPERTY), board);
    this.direction = RoadSection.Direction.valueOf(serialized.getAsJsonPrimitive(DIRECTION_PROPERTY).getAsString());
  }

  public Position getPosition() {
    return position;
  }

  public RoadSection.Direction getDirection() {
    return direction;
  }

  /**
   * NOTE: Unlike Position, DirectedPosition requires two positions being on the same RoadSection to be equal.
   */
  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof DirectedPosition)) {
      return false;
    }
    final DirectedPosition otherDirectedPosition = (DirectedPosition) obj;
    return this.position.equals(otherDirectedPosition.position)
        && direction == otherDirectedPosition.direction
        && position.getRoadSection().equals(otherDirectedPosition.position.getRoadSection());
  }

  @Override
  public String toString() {
    return "(" + position + ", " + direction + ")";
  }

  @Override
  public JsonObject snapshot() {
    JsonObject returnValue = new JsonObject();
    returnValue.add(POSITION_PROPERTY, position.snapshot());
    returnValue.addProperty(DIRECTION_PROPERTY, direction.name());
    return returnValue;
  }
}
