# Testing

## Android

Android emulator runs may require certain properties to be manually tuned.
This is achieved by creating a file `gop-android/app/src/main/assets/test.properties`
with the following syntax:

```
GameManager.serverSocketAddresses = myhost0:1234,myhost1:5678
```

# TODOs

* Fix the issue with broken local connection.

* Fix the general issue with broken connection.
