package cz.wie.najtmar.gop.client.ui;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.name.Names;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import com.esotericsoftware.minlog.Log;

import cz.wie.najtmar.gop.board.BoardGenerator;
import cz.wie.najtmar.gop.board.SimpleBoardGenerator;
import cz.wie.najtmar.gop.client.ui.GameManager.Action;
import cz.wie.najtmar.gop.client.ui.GameManager.GameInfo;
import cz.wie.najtmar.gop.client.ui.GameManager.State;
import cz.wie.najtmar.gop.client.view.ClientViewException;
import cz.wie.najtmar.gop.client.view.PlayerView;
import cz.wie.najtmar.gop.client.view.UserView;
import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.event.EventProcessingException;
import cz.wie.najtmar.gop.game.Game;
import cz.wie.najtmar.gop.game.Move;
import cz.wie.najtmar.gop.game.Player;
import cz.wie.najtmar.gop.game.Prawn;
import cz.wie.najtmar.gop.game.PropertiesReader;
import cz.wie.najtmar.gop.game.SettlersUnit;
import cz.wie.najtmar.gop.game.SimplePropertiesReader;
import cz.wie.najtmar.gop.io.Message;
import cz.wie.najtmar.gop.server.ClientMessage;
import cz.wie.najtmar.gop.server.ConnectionManager;
import cz.wie.najtmar.gop.server.GameSession;
import cz.wie.najtmar.gop.server.InitialPrawnDeployer;
import cz.wie.najtmar.gop.server.KryoNetCommunicator;
import cz.wie.najtmar.gop.server.ServerException;
import cz.wie.najtmar.gop.server.ServerMessage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import javax.json.Json;

public class GameManagerTest {

  static final UUID SAMPLE_GAME_ID = UUID.randomUUID();
  static final int FAKE_HEARTBEAT_ID = 99999999;

  UserView testUser;
  TestGameManagerEngine testGameManagerEngine;
  GameManager testGameManager;
  Client otherClient;
  int otherClientLastMessageId;
  List<ServerMessage> otherClientInput;
  List<ClientMessage> clientMessagesReceived;
  UUID otherUserId;
  Session mockSession;
  volatile boolean runtimeExceptionThrown;
  volatile int sendOverdueMessagesInvocationCount;

  static class TestGameManagerEngine implements GameManagementEngine {

    List<State> states;

    List<Action> actionsNotFinished;

    TestGameManagerEngine() {
      actionsNotFinished = new LinkedList<>();
    }

    @Override
    public void initializeGameManagementEngine(GameManager gameManager) throws ClientViewException {
      this.states = new LinkedList<>();
    }

    @Override
    public void onGameManagerStateChange(State oldState) throws ClientViewException {
      states.add(oldState);
    }

    @Override
    public void updatePendingGamesList() {
    }

    @Override
    public void setOwnGameEnabled(boolean isEnabled, List<String> participants) {
    }

    @Override
    public void notifyActionNotFinished(Action action) {
      actionsNotFinished.add(action);
    }

    public List<State> getStates() {
      return states;
    }

  }

  private Module getTestModule() {
    return new AbstractModule() {
      private final OutputEngine mockedOutputEngine = mock(OutputEngine.class);
      private final InputEngine mockedInputEngine = mock(InputEngine.class);
      private final GameManagerManagedEngine mockedGameManagerManagedEngine = mock(GameManagerManagedEngine.class);
      private final Session mockedSession = GameManagerTest.this.mockSession;

      @Override
      protected void configure() {
        try {
          when(mockedSession.processEvents(any(Event[].class), any(PlayerView.class)))
              .thenReturn(new Event[]{});
        } catch (ClientViewException exception) {
          fail(exception.getMessage());
        }
        bind(PropertiesReader.class).to(SimplePropertiesReader.class);
        bind(BoardGenerator.class).to(SimpleBoardGenerator.class);
        bind(InitialPrawnDeployer.class).toInstance(mock(InitialPrawnDeployer.class));
        bind(ConnectionManager.Factory.class).to(KryoNetCommunicator.Factory.class);
        bind(Long.class).annotatedWith(Names.named("random seed")).toInstance(45L);
        bind(GameManagementEngine.class).toInstance(testGameManagerEngine);
        bind(KryoNetCommunicator.class);
        bind(UserView.class).toInstance(testUser);
        bind(Locale.class).toInstance(Locale.UK);
        bind(OutputEngine.class).toInstance(mockedOutputEngine);
        bind(InputEngine.class).toInstance(mockedInputEngine);
        bind(GameManagerManagedEngine.class).toInstance(mockedGameManagerManagedEngine);
        bind(Session.class).toInstance(mockedSession);
      }
    };
  }

  /**
   * Method setUp().
   * @throws Exception when something goes wrong
   */
  @Before
  public void setUp() throws Exception {
    KryoNetCommunicator.injectConnectionTimeoutMillis(5000);
    runtimeExceptionThrown = false;
    sendOverdueMessagesInvocationCount = 0;
    testUser = new UserView("testUser", new UUID(12345, 67890));
    otherUserId = new UUID(98765, 43210);
    testGameManagerEngine = new TestGameManagerEngine();
    mockSession = mock(Session.class);
    final Injector injector = Guice.createInjector(getTestModule());
    testGameManager = (GameManager) spy(injector.getInstance(GameManager.class));
    testGameManager.injectOnlyAllowLocalServers(true);
    otherClientInput = new LinkedList<>();
    clientMessagesReceived = new LinkedList<>();

    otherClient = new Client();
    otherClientLastMessageId = 0;
    otherClient.addListener(new Listener() {
      @Override
      public void received(Connection connection, Object object) {
        synchronized (GameManagerTest.this) {
          if (object instanceof ServerMessage) {
            final ServerMessage serverMessage = (ServerMessage) object;
            if (serverMessage.getId() != ServerMessage.ID_NOT_SET) {
              if (serverMessage.getId() <= otherClientLastMessageId) {
                return;
              }
              otherClientLastMessageId = serverMessage.getId();
            }
            switch (serverMessage.getType()) {
              case REQUEST_ADD_PLAYER: {
                final UUID gameId = UUID.fromString(serverMessage.getBody().getString("gameId"));
                final UUID userId = UUID.fromString(serverMessage.getBody().getString("userId"));
                final int index = serverMessage.getBody().getInt("index");
                PlayerView player = new PlayerView(new UserView("otherUser", otherUserId), index);
                try {
                  otherClient.sendTCP(ClientMessage.createSendMessage(gameId, userId,
                      new Message(new Event[]{player.createAddPlayerEvent(0)})));
                } catch (EventProcessingException exception) {
                  Log.error("Creating ADD_PLAYER Message failed for Game with id " + gameId + " and User with id "
                      + userId + " .");
                  runtimeExceptionThrown = true;
                }
                break;
              }
              default: {
                // Just ignore.
                break;
              }
            }
            otherClientInput.add(serverMessage);
          }
        }
      }
    });
    final Kryo kryo = otherClient.getKryo();
    kryo.register(ClientMessage.class);
    kryo.register(ClientMessage.Type.class);
    kryo.register(ServerMessage.class);
    kryo.register(ServerMessage.Type.class);
    otherClient.start();
  }

  @After
  public void tearDown() {
    assertFalse(runtimeExceptionThrown);
    testGameManager.close();
  }

  @Test
  public void testGameManagerInitializationWorks() throws Exception {
    assertEquals(GameManager.State.NOT_INITIALIZED, testGameManager.getState());
    assertEquals(ConnectionManager.State.NOT_INITIALIZED, testGameManager.getServer().getState());
    testGameManager.initialize();
    assertEquals(GameManager.State.INITIAL_MENU, testGameManager.getState());
    assertEquals(ConnectionManager.State.STARTED, testGameManager.getServer().getState());
    assertEquals(Arrays.asList(GameManager.State.NOT_INITIALIZED), testGameManagerEngine.getStates());
  }

  @Test
  public void testGameIntentInitiationWorks() throws Exception {
    testGameManager.initialize();
    testGameManager.initiateGameIntent();
    final GameSession gameSession = testGameManager.getGameSession();
    while (gameSession.getGame() == null) {
      Thread.sleep(1);
    }
    assertEquals(Game.State.NOT_INITIALIZED, gameSession.getGame().getState());
  }

  @Test
  public void testGameInitializationWorks() throws Exception {
    testGameManager.initialize();
    final KryoNetCommunicator testServer = testGameManager.getServer();
    otherClient.connect(KryoNetCommunicator.getConnectionTimeoutMillis(), InetAddress.getLocalHost(),
        testServer.getServerTcpPort(), testServer.getServerUdpPort());
    testGameManager.initiateGameIntent();
    final GameSession gameSession = testGameManager.getGameSession();
    while (gameSession.getGame() == null) {
      Thread.sleep(1);
    }
    final Game game = gameSession.getGame();
    while (true) {
      otherClient.sendTCP(ClientMessage.createGetPendingGames(otherUserId));
      while (otherClientInput.size() < 1) {
        Thread.sleep(1);
      }
      final ServerMessage response = otherClientInput.remove(0);
      if (response.getBody().getJsonArray("pendingGames").size() == 1) {
        break;
      }
    }
    otherClient.sendTCP(ClientMessage.createJoinGame(game.getId(), otherUserId, "otherUser"));
    while (otherClientInput.size() < 1) {
      Thread.sleep(1);
    }
    assertEquals(ServerMessage.createJoinGameResponse(game.getId(), otherUserId, true), otherClientInput.remove(0));
    testGameManager.initializeGame();
    while (otherClientInput.size() < 1) {
      Thread.sleep(1);
    }
    assertEquals(ServerMessage.createStartGame(game.getId(), otherUserId), otherClientInput.remove(0));
    while (otherClientInput.size() < 1) {
      Thread.sleep(1);
    }
    assertEquals(ServerMessage.createRequestAddPlayer(game.getId(), otherUserId, 1), otherClientInput.remove(0));
    while (otherClientInput.size() < 1) {
      Thread.sleep(1);
    }
    assertEquals(ServerMessage.Type.CLIENT_MESSAGE_RESPONSE, otherClientInput.remove(0).getType());
    while (otherClientInput.size() < 1) {
      Thread.sleep(1);
    }
    final ServerMessage initializeGameMessage = otherClientInput.remove(0);
    assertEquals(ServerMessage.Type.SEND_MESSAGE, initializeGameMessage.getType());
    while (!game.getState().equals(Game.State.RUNNING)) {
      Thread.sleep(1);
    }
  }

  @Test
  public void testLocalConnectionReestablishingWorks() throws Exception {
    testGameManager.initialize();
    final KryoNetCommunicator testServer = testGameManager.getServer();
    otherClient.connect(KryoNetCommunicator.getConnectionTimeoutMillis(), InetAddress.getLocalHost(),
        testServer.getServerTcpPort(), testServer.getServerUdpPort());
    testGameManager.initiateGameIntent();
    final GameSession gameSession = testGameManager.getGameSession();
    while (gameSession.getGame() == null) {
      Thread.sleep(1);
    }
    final Game game = gameSession.getGame();
    while (true) {
      otherClient.sendTCP(ClientMessage.createGetPendingGames(otherUserId));
      while (otherClientInput.size() < 1) {
        Thread.sleep(1);
      }
      final ServerMessage response = otherClientInput.remove(0);
      if (response.getBody().getJsonArray("pendingGames").size() == 1) {
        break;
      }
    }
    otherClient.sendTCP(ClientMessage.createJoinGame(game.getId(), otherUserId, "otherUser"));
    while (otherClientInput.size() < 1) {
      Thread.sleep(1);
    }
    assertEquals(ServerMessage.createJoinGameResponse(game.getId(), otherUserId, true), otherClientInput.remove(0));

    // Deliberately close the connection.
    testGameManager.getLocalClient().close();
    while (testGameManager.getLocalClient().isConnected()) {
      Thread.sleep(10);
    }

    testGameManager.initializeGame();
    while (otherClientInput.size() < 1) {
      Thread.sleep(1);
    }
    assertEquals(ServerMessage.createStartGame(game.getId(), otherUserId), otherClientInput.remove(0));
    while (otherClientInput.size() < 1) {
      Thread.sleep(1);
    }
    assertEquals(ServerMessage.createRequestAddPlayer(game.getId(), otherUserId, 1), otherClientInput.remove(0));
    while (otherClientInput.size() < 1) {
      Thread.sleep(1);
    }
    assertEquals(ServerMessage.Type.CLIENT_MESSAGE_RESPONSE, otherClientInput.remove(0).getType());
    while (otherClientInput.size() < 1) {
      Thread.sleep(1);
    }
    final ServerMessage initializeGameMessage = otherClientInput.remove(0);
    assertEquals(ServerMessage.Type.SEND_MESSAGE, initializeGameMessage.getType());
    while (!game.getState().equals(Game.State.RUNNING)) {
      Thread.sleep(1);
    }
  }

  @Test
  public void testGameFinishingWorks() throws Exception {
    Log.info("testGameFinishingWorks()");
    testGameManager.initialize();
    final KryoNetCommunicator testServer = testGameManager.getServer();
    otherClient.connect(KryoNetCommunicator.getConnectionTimeoutMillis(), InetAddress.getLocalHost(),
        testServer.getServerTcpPort(), testServer.getServerUdpPort());
    testGameManager.initiateGameIntent();
    final GameSession gameSession = testGameManager.getGameSession();
    while (gameSession.getGame() == null) {
      Thread.sleep(1);
    }
    final Game game = gameSession.getGame();
    while (true) {
      otherClient.sendTCP(ClientMessage.createGetPendingGames(otherUserId));
      while (otherClientInput.size() < 1) {
        Thread.sleep(1);
      }
      final ServerMessage response = otherClientInput.remove(0);
      if (response.getBody().getJsonArray("pendingGames").size() == 1) {
        break;
      }
    }
    otherClient.sendTCP(ClientMessage.createJoinGame(game.getId(), otherUserId, "otherUser"));
    while (otherClientInput.size() < 1) {
      Thread.sleep(1);
    }
    assertEquals(ServerMessage.createJoinGameResponse(game.getId(), otherUserId, true), otherClientInput.remove(0));
    testGameManager.initializeGame();
    while (otherClientInput.size() < 1) {
      Thread.sleep(1);
    }
    assertEquals(ServerMessage.createStartGame(game.getId(), otherUserId), otherClientInput.remove(0));
    while (otherClientInput.size() < 1) {
      Thread.sleep(1);
    }
    assertEquals(ServerMessage.createRequestAddPlayer(game.getId(), otherUserId, 1), otherClientInput.remove(0));
    while (otherClientInput.size() < 1) {
      Thread.sleep(1);
    }
    assertEquals(ServerMessage.Type.CLIENT_MESSAGE_RESPONSE, otherClientInput.remove(0).getType());
    while (otherClientInput.size() < 1) {
      Thread.sleep(1);
    }
    final ServerMessage initializeGameMessage = otherClientInput.remove(0);
    assertEquals(ServerMessage.Type.SEND_MESSAGE, initializeGameMessage.getType());
    while (!game.getState().equals(Game.State.RUNNING)) {
      Thread.sleep(1);
    }

    // Add dummy Prawns to make sure that Game evaluation succeeds.
    for (int i = 0; i < 2; ++i) {
      final Prawn playerPrawn = mock(SettlersUnit.class);
      when(playerPrawn.getId()).thenReturn(i);
      when(playerPrawn.getPlayer()).thenReturn(game.getPlayers().get(i));
      when(playerPrawn.prepareMoveSequence(anyLong())).thenReturn(new Move[]{});
      when(playerPrawn.getCurrentPosition()).thenReturn(game.getBoard().getJoints().get(i).getNormalizedPosition());
      game.addPrawn(playerPrawn);
    }

    testGameManager.getGameCommunicationClient().sendTCP(ClientMessage.createSendMessage(game.getId(), testUser.getId(),
        new Message(new Event[]{})));
    otherClient.sendTCP(ClientMessage.createSendMessage(game.getId(), otherUserId,
        new Message(new Event[]{new Event(Json.createObjectBuilder()
            .add("type", "ABANDON_PLAYER")
            .add("time", 12345)
            .add("player", 1)
            .build())})));
    while (!game.getState().equals(Game.State.FINISHED)) {
      Thread.sleep(1);
    }
    assertEquals(Player.State.WON, game.getPlayers().get(0).getState());
    assertEquals(Player.State.ABANDONED, game.getPlayers().get(1).getState());
  }

  @Test
  public void testGameFinishingWorks1() throws Exception {
    Log.info("testGameFinishingWorks1()");

    // Making the local client send an ABANDON_PLAYER message.
    when(mockSession.processEvents(any(Event[].class), any(PlayerView.class))).thenReturn(
        new Event[]{new Event(Json.createObjectBuilder()
            .add("type", "ABANDON_PLAYER")
            .add("time", 12345)
            .add("player", 0)
            .build())});

    testGameManager.initialize();
    final KryoNetCommunicator testServer = testGameManager.getServer();
    otherClient.connect(KryoNetCommunicator.getConnectionTimeoutMillis(), InetAddress.getLocalHost(),
        testServer.getServerTcpPort(), testServer.getServerUdpPort());
    testGameManager.initiateGameIntent();
    final GameSession gameSession = testGameManager.getGameSession();
    while (gameSession.getGame() == null) {
      Thread.sleep(1);
    }
    final Game game = gameSession.getGame();
    while (true) {
      otherClient.sendTCP(ClientMessage.createGetPendingGames(otherUserId));
      while (otherClientInput.size() < 1) {
        Thread.sleep(1);
      }
      final ServerMessage response = otherClientInput.remove(0);
      if (response.getBody().getJsonArray("pendingGames").size() == 1) {
        break;
      }
    }
    otherClient.sendTCP(ClientMessage.createJoinGame(game.getId(), otherUserId, "otherUser"));
    while (otherClientInput.size() < 1) {
      Thread.sleep(1);
    }
    assertEquals(ServerMessage.createJoinGameResponse(game.getId(), otherUserId, true), otherClientInput.remove(0));
    testGameManager.initializeGame();
    while (otherClientInput.size() < 1) {
      Thread.sleep(1);
    }
    assertEquals(ServerMessage.createStartGame(game.getId(), otherUserId), otherClientInput.remove(0));
    while (otherClientInput.size() < 1) {
      Thread.sleep(1);
    }
    assertEquals(ServerMessage.createRequestAddPlayer(game.getId(), otherUserId, 1), otherClientInput.remove(0));
    while (otherClientInput.size() < 1) {
      Thread.sleep(1);
    }
    assertEquals(ServerMessage.Type.CLIENT_MESSAGE_RESPONSE, otherClientInput.remove(0).getType());
    while (otherClientInput.size() < 1) {
      Thread.sleep(1);
    }
    final ServerMessage initializeGameMessage = otherClientInput.remove(0);
    assertEquals(ServerMessage.Type.SEND_MESSAGE, initializeGameMessage.getType());
    while (!game.getState().equals(Game.State.RUNNING)) {
      Thread.sleep(1);
    }

    // Add dummy Prawns to make sure that Game evaluation succeeds.
    for (int i = 0; i < 2; ++i) {
      final Prawn playerPrawn = mock(SettlersUnit.class);
      when(playerPrawn.getId()).thenReturn(i);
      when(playerPrawn.getPlayer()).thenReturn(game.getPlayers().get(i));
      when(playerPrawn.prepareMoveSequence(anyLong())).thenReturn(new Move[]{});
      when(playerPrawn.getCurrentPosition()).thenReturn(game.getBoard().getJoints().get(i).getNormalizedPosition());
      game.addPrawn(playerPrawn);
    }

    // Local client made send an ABANDON_PLAYER message via mockSession at the top of the method.
    otherClient.sendTCP(ClientMessage.createSendMessage(game.getId(), otherUserId, new Message(new Event[]{})));
    while (!game.getState().equals(Game.State.FINISHED)) {
      Thread.sleep(1);
    }
    assertEquals(Player.State.ABANDONED, game.getPlayers().get(0).getState());
    assertEquals(Player.State.WON, game.getPlayers().get(1).getState());
  }

  @Test
  public void testGameIntentCancelationWorks() throws Exception {
    testGameManager.initialize();
    final KryoNetCommunicator testServer = testGameManager.getServer();
    otherClient.connect(KryoNetCommunicator.getConnectionTimeoutMillis(), InetAddress.getLocalHost(),
        testServer.getServerTcpPort(), testServer.getServerUdpPort());
    testGameManager.initiateGameIntent();
    final GameSession gameSession = testGameManager.getGameSession();
    while (gameSession.getGame() == null) {
      Thread.sleep(1);
    }
    final Game game = gameSession.getGame();
    while (true) {
      otherClient.sendTCP(ClientMessage.createGetPendingGames(otherUserId));
      while (otherClientInput.size() < 1) {
        Thread.sleep(1);
      }
      final ServerMessage response = otherClientInput.remove(0);
      if (response.getBody().getJsonArray("pendingGames").size() == 1) {
        break;
      }
    }
    otherClient.sendTCP(ClientMessage.createJoinGame(game.getId(), otherUserId, "otherUser"));
    while (otherClientInput.size() < 1) {
      Thread.sleep(1);
    }
    assertEquals(ServerMessage.createJoinGameResponse(game.getId(), otherUserId, true), otherClientInput.remove(0));
    testGameManager.cancelGameIntent();
    while (otherClientInput.size() < 1) {
      Thread.sleep(1);
    }
    assertEquals(ServerMessage.createJoinGameResponse(game.getId(), otherUserId, false), otherClientInput.remove(0));
    while (!game.getState().equals(Game.State.INTENT_ABORTED)) {
      Thread.sleep(1);
    }
  }

  @Test
  public void testGettingPendingGamesWorks() throws Exception {
    testGameManager.initialize();

    KryoNetCommunicator.injectConnectionTimeoutMillis(500);
    Server server = null;
    try {
      server = new Server();

      final Kryo kryo =  server.getKryo();
      kryo.register(ClientMessage.class);
      kryo.register(ClientMessage.Type.class);
      kryo.register(ServerMessage.class);
      kryo.register(ServerMessage.Type.class);

      final Game mockGame0 = mock(Game.class);
      final GameSession gameSession0 = mock(GameSession.class);
      when(mockGame0.getId()).thenReturn(new UUID(12345, 67890));
      when(mockGame0.getTimestamp()).thenReturn(12345L);
      when(gameSession0.getGame()).thenReturn(mockGame0);
      final Game mockGame1 = mock(Game.class);
      final GameSession gameSession1 = mock(GameSession.class);
      when(mockGame1.getId()).thenReturn(new UUID(98765, 43210));
      when(mockGame1.getTimestamp()).thenReturn(54321L);
      when(gameSession1.getGame()).thenReturn(mockGame1);

      server.addListener(new Listener() {
        @Override
        public void received(Connection connection, Object object) {
          if (object instanceof ClientMessage) {
            final ClientMessage request = (ClientMessage) object;
            switch (request.getType()) {
              case GET_PENDING_GAMES: {
                Log.info("ClientMessage GET_PENDING_GAMES received from " + connection.getRemoteAddressTCP());
                connection.sendTCP(ServerMessage.createGetPendingGamesResponse(
                    new GameSession[]{gameSession0, gameSession1, }, "otherUser"));
                break;
              }
              default: {
                // Do nothing.
                break;
              }
            }
          }
        }
      });

      boolean serverBound = false;
      for (int i = 0; !serverBound && i < KryoNetCommunicator.PORT_TRIED_COUNT; ++i) {
        int serverTcpPort = KryoNetCommunicator.FIRST_TCP_PORT + i;
        int serverUdpPort = KryoNetCommunicator.FIRST_UDP_PORT + i;
        try {
          server.bind(serverTcpPort, serverUdpPort);
          serverBound = true;
        } catch (IOException exception) {
          Log.info("Local port combination (tcp=" + serverTcpPort + ", udp=" + serverUdpPort + ") already taken: "
              + exception);
        }
      }
      testGameManager.getServer().close();
      if (!serverBound) {
        throw new ServerException("Binding Server ports failed.");
      }

      server.start();

      testGameManager.getPendingGames();
      while (testGameManager.getPendingGamesList().size() < 2) {
        Thread.sleep(1);
      }
      final List<GameInfo> pendingGames = testGameManager.getPendingGamesList();
      assertEquals(new UUID(12345, 67890), pendingGames.get(0).getId());
      assertEquals(12345, pendingGames.get(0).getTimestamp());
      assertEquals(new UUID(98765, 43210), pendingGames.get(1).getId());
      assertEquals(54321, pendingGames.get(1).getTimestamp());
    } finally {
      server.close();
    }
  }

  @Test
  public void testJoiningPendingGameWorks() throws Exception {
    testGameManager.initialize();

    KryoNetCommunicator.injectConnectionTimeoutMillis(2000);
    Server server = null;
    try {
      server = new Server();

      final Kryo kryo =  server.getKryo();
      kryo.register(ClientMessage.class);
      kryo.register(ClientMessage.Type.class);
      kryo.register(ServerMessage.class);
      kryo.register(ServerMessage.Type.class);

      final Game mockGame0 = mock(Game.class);
      final GameSession gameSession0 = mock(GameSession.class);
      when(mockGame0.getId()).thenReturn(new UUID(12345, 67890));
      when(mockGame0.getTimestamp()).thenReturn(12345L);
      when(gameSession0.getGame()).thenReturn(mockGame0);
      final Game mockGame1 = mock(Game.class);
      final GameSession gameSession1 = mock(GameSession.class);
      when(mockGame1.getId()).thenReturn(new UUID(98765, 43210));
      when(mockGame1.getTimestamp()).thenReturn(54321L);
      when(gameSession1.getGame()).thenReturn(mockGame1);

      server.addListener(new Listener() {
        @Override
        public void received(Connection connection, Object object) {
          if (object instanceof ClientMessage) {
            final ClientMessage request = (ClientMessage) object;
            switch (request.getType()) {
              case GET_PENDING_GAMES: {
                Log.info("ClientMessage GET_PENDING_GAMES received from " + connection.getRemoteAddressTCP());
                connection.sendTCP(ServerMessage.createGetPendingGamesResponse(
                    new GameSession[]{gameSession0, gameSession1, }, "otherUser"));
                break;
              }
              case JOIN_GAME: {
                connection.sendTCP(ServerMessage.createJoinGameResponse(
                    UUID.fromString(request.getBody().getString("gameId")),
                    UUID.fromString(request.getBody().getString("userId")), true));
                break;
              }
              default: {
                // Do nothing.
                break;
              }
            }
          }
        }
      });

      boolean serverBound = false;
      for (int i = 0; !serverBound && i < KryoNetCommunicator.PORT_TRIED_COUNT; ++i) {
        int serverTcpPort = KryoNetCommunicator.FIRST_TCP_PORT + i;
        int serverUdpPort = KryoNetCommunicator.FIRST_UDP_PORT + i;
        try {
          server.bind(serverTcpPort, serverUdpPort);
          serverBound = true;
        } catch (IOException exception) {
          Log.info("Local port combination (tcp=" + serverTcpPort + ", udp=" + serverUdpPort + ") already taken: "
              + exception);
        }
      }
      testGameManager.getServer().close();
      if (!serverBound) {
        throw new ServerException("Binding Server ports failed.");
      }

      server.start();

      testGameManager.getPendingGames();
      while (testGameManager.getPendingGamesList().size() < 2) {
        Thread.sleep(1);
      }
      while (!testGameManagerEngine.getStates().get(testGameManagerEngine.getStates().size() - 1)
          .equals(State.INITIAL_MENU)) {
        Thread.sleep(1);
      }
      assertEquals(State.GAMES_LISTED, testGameManager.getState());
      testGameManager.joinPendingGame(1);
      while (!testGameManagerEngine.getStates().get(testGameManagerEngine.getStates().size() - 1)
          .equals(State.GAMES_LISTED)) {
        Thread.sleep(1);
      }
      assertEquals(State.GAME_JOINED, testGameManager.getState());
    } finally {
      server.close();
    }
  }

  @Test
  public void testClientMessageIdManagementWorks() throws Exception {
    Log.info("testClientMessageIdManagementWorks()");
    testGameManager.initialize();

    KryoNetCommunicator.injectConnectionTimeoutMillis(2000);
    Server server = null;
    try {
      server = new Server();

      final Kryo kryo =  server.getKryo();
      kryo.register(ClientMessage.class);
      kryo.register(ClientMessage.Type.class);
      kryo.register(ServerMessage.class);
      kryo.register(ServerMessage.Type.class);

      final Game mockGame0 = mock(Game.class);
      final GameSession gameSession0 = mock(GameSession.class);
      when(mockGame0.getId()).thenReturn(new UUID(12345, 67890));
      when(mockGame0.getTimestamp()).thenReturn(12345L);
      when(gameSession0.getGame()).thenReturn(mockGame0);
      final Game mockGame1 = mock(Game.class);
      final GameSession gameSession1 = mock(GameSession.class);
      when(mockGame1.getId()).thenReturn(new UUID(98765, 43210));
      when(mockGame1.getTimestamp()).thenReturn(54321L);
      when(gameSession1.getGame()).thenReturn(mockGame1);

      server.addListener(new Listener() {
        @Override
        public void received(Connection connection, Object object) {
          if (object instanceof ClientMessage) {
            final ClientMessage request = (ClientMessage) object;
            switch (request.getType()) {
              case GET_PENDING_GAMES: {
                Log.info("ClientMessage GET_PENDING_GAMES received from " + connection.getRemoteAddressTCP());
                connection.sendTCP(ServerMessage.createGetPendingGamesResponse(
                    new GameSession[]{gameSession0, gameSession1, }, "otherUser"));
                break;
              }
              case JOIN_GAME: {
                connection.sendTCP(ServerMessage.createJoinGameResponse(
                    UUID.fromString(request.getBody().getString("gameId")),
                    UUID.fromString(request.getBody().getString("userId")), true));
                break;
              }
              case SEND_MESSAGE: {
                clientMessagesReceived.add(request);
                break;
              }
              case HEARTBEAT: {
                if (request.getBody().getInt("id") == FAKE_HEARTBEAT_ID) {
                  // For the sake of the test, lastMessageId of the response is equal to lastMessageId, which does not
                  // correspond to a real situation.
                  connection.sendTCP(ServerMessage.createHeartbeatResponse(FAKE_HEARTBEAT_ID, true, false,
                      Arrays.asList(), request.getBody().getInt("lastMessageId")));
                  clientMessagesReceived.add(null);
                }
                break;
              }
              default: {
                // Do nothing.
                break;
              }
            }
          }
        }
      });

      boolean serverBound = false;
      for (int i = 0; !serverBound && i < KryoNetCommunicator.PORT_TRIED_COUNT; ++i) {
        int serverTcpPort = KryoNetCommunicator.FIRST_TCP_PORT + i;
        int serverUdpPort = KryoNetCommunicator.FIRST_UDP_PORT + i;
        try {
          server.bind(serverTcpPort, serverUdpPort);
          serverBound = true;
        } catch (IOException exception) {
          Log.info("Local port combination (tcp=" + serverTcpPort + ", udp=" + serverUdpPort + ") already taken: "
              + exception);
        }
      }
      testGameManager.getServer().close();
      if (!serverBound) {
        throw new ServerException("Binding Server ports failed.");
      }

      server.start();

      testGameManager.getPendingGames();
      while (testGameManager.getPendingGamesList().size() < 2) {
        Thread.sleep(1);
      }
      while (!testGameManagerEngine.getStates().get(testGameManagerEngine.getStates().size() - 1)
          .equals(State.INITIAL_MENU)) {
        Thread.sleep(1);
      }
      assertEquals(State.GAMES_LISTED, testGameManager.getState());
      testGameManager.joinPendingGame(1);
      while (!testGameManagerEngine.getStates().get(testGameManagerEngine.getStates().size() - 1)
          .equals(State.GAMES_LISTED)) {
        Thread.sleep(1);
      }
      assertEquals(State.GAME_JOINED, testGameManager.getState());

      // Sending actual ClientMessages .
      ClientMessage testMessage0 = ClientMessage.createSendMessage(new UUID(12345, 67890),
          testUser.getId(), new Message(new Event[]{new Event(Json.createObjectBuilder()
              .add("type", "TEST")
              .add("time", 12345)
              .build())}));
      testGameManager.sendGameMessage(testMessage0);
      while (clientMessagesReceived.size() < 1) {
        Thread.sleep(1);
      }
      assertEquals(Arrays.asList(testMessage0), clientMessagesReceived);
      //
      ClientMessage testMessage1 = ClientMessage.createSendMessage(new UUID(12345, 67890),
          testUser.getId(), new Message(new Event[]{new Event(Json.createObjectBuilder()
              .add("type", "TEST")
              .add("time", 12346)
              .build())}));
      testGameManager.sendGameMessage(testMessage1);
      while (clientMessagesReceived.size() < 3) {
        Thread.sleep(1);
      }
      assertEquals(Arrays.asList(testMessage0, testMessage0, testMessage1), clientMessagesReceived);
      // Clear the unconfirmed message queue.
      testGameManager.sendGameMessage(ClientMessage.createHeartbeat(FAKE_HEARTBEAT_ID, new UUID(12345, 67890),
          testUser.getId(), testMessage1.getId()));
      while (clientMessagesReceived.size() < 6) {
        Thread.sleep(1);
      }
      assertEquals(Arrays.asList(testMessage0, testMessage0, testMessage1, testMessage0, testMessage1, null),
          clientMessagesReceived);
      while (!testGameManager.getUnconfirmedMessageQueue().isEmpty()) {
        Thread.sleep(1);
      }
      //
      ClientMessage testMessage2 = ClientMessage.createSendMessage(new UUID(12345, 67890),
          testUser.getId(), new Message(new Event[]{new Event(Json.createObjectBuilder()
              .add("type", "TEST")
              .add("time", 12347)
              .build())}));
      testGameManager.sendGameMessage(testMessage2);
      while (clientMessagesReceived.size() < 7) {
        Thread.sleep(1);
      }
      assertEquals(Arrays.asList(testMessage0, testMessage0, testMessage1, testMessage0, testMessage1, null,
          testMessage2), clientMessagesReceived);
    } finally {
      server.close();
    }
  }

  @Test
  public void testReconnectingGameClientWorks() throws Exception {
    testGameManager.initialize();

    KryoNetCommunicator.injectConnectionTimeoutMillis(2000);
    Server server = null;
    try {
      server = new Server();

      final Kryo kryo =  server.getKryo();
      kryo.register(ClientMessage.class);
      kryo.register(ClientMessage.Type.class);
      kryo.register(ServerMessage.class);
      kryo.register(ServerMessage.Type.class);

      final Game mockGame0 = mock(Game.class);
      final GameSession gameSession0 = mock(GameSession.class);
      when(mockGame0.getId()).thenReturn(new UUID(12345, 67890));
      when(mockGame0.getTimestamp()).thenReturn(12345L);
      when(gameSession0.getGame()).thenReturn(mockGame0);
      final Game mockGame1 = mock(Game.class);
      final GameSession gameSession1 = mock(GameSession.class);
      when(mockGame1.getId()).thenReturn(new UUID(98765, 43210));
      when(mockGame1.getTimestamp()).thenReturn(54321L);
      when(gameSession1.getGame()).thenReturn(mockGame1);

      server.addListener(new Listener() {
        @Override
        public void received(Connection connection, Object object) {
          if (object instanceof ClientMessage) {
            final ClientMessage request = (ClientMessage) object;
            switch (request.getType()) {
              case GET_PENDING_GAMES: {
                Log.info("ClientMessage GET_PENDING_GAMES received from " + connection.getRemoteAddressTCP());
                connection.sendTCP(ServerMessage.createGetPendingGamesResponse(
                    new GameSession[]{gameSession0, gameSession1, }, "otherUser"));
                break;
              }
              case JOIN_GAME: {
                connection.sendTCP(ServerMessage.createJoinGameResponse(
                    UUID.fromString(request.getBody().getString("gameId")),
                    UUID.fromString(request.getBody().getString("userId")), true));
                break;
              }
              case RECONNECT_PLAYER: {
                connection.sendTCP(ServerMessage.createReconnectPlayerResponse(
                    UUID.fromString(request.getBody().getString("gameId")),
                    UUID.fromString(request.getBody().getString("userId")),
                    request.getBody().getString("hostString"), true));
                break;
              }
              default: {
                // Do nothing.
                break;
              }
            }
          }
        }
      });

      boolean serverBound = false;
      for (int i = 0; !serverBound && i < KryoNetCommunicator.PORT_TRIED_COUNT; ++i) {
        int serverTcpPort = KryoNetCommunicator.FIRST_TCP_PORT + i;
        int serverUdpPort = KryoNetCommunicator.FIRST_UDP_PORT + i;
        try {
          server.bind(serverTcpPort, serverUdpPort);
          serverBound = true;
        } catch (IOException exception) {
          Log.info("Local port combination (tcp=" + serverTcpPort + ", udp=" + serverUdpPort + ") already taken: "
              + exception);
        }
      }
      testGameManager.getServer().close();
      if (!serverBound) {
        throw new ServerException("Binding Server ports failed.");
      }

      server.start();

      testGameManager.getPendingGames();
      while (testGameManager.getPendingGamesList().size() < 2) {
        Thread.sleep(1);
      }
      while (!testGameManagerEngine.getStates().get(testGameManagerEngine.getStates().size() - 1)
          .equals(State.INITIAL_MENU)) {
        Thread.sleep(1);
      }
      assertEquals(State.GAMES_LISTED, testGameManager.getState());
      testGameManager.joinPendingGame(1);
      while (!testGameManagerEngine.getStates().get(testGameManagerEngine.getStates().size() - 1)
          .equals(State.GAMES_LISTED)) {
        Thread.sleep(1);
      }
      assertEquals(State.GAME_JOINED, testGameManager.getState());
      assertTrue(testGameManager.getGameCommunicationClient().isConnected());
      int oldConnectionId = testGameManager.getGameCommunicationClient().getID();

      // Make sure all sendOverdueMessages() invocations are counted.
      doAnswer(new Answer<Boolean>() {
        public Boolean answer(InvocationOnMock invocation) {
          try {
            ++sendOverdueMessagesInvocationCount;
            invocation.callRealMethod();
          } catch (Throwable throwable) {
            Log.error(throwable.getMessage());
            runtimeExceptionThrown = true;
          }
          return true;
        }
      }).when(testGameManager).sendOverdueMessages();

      // Reconnection 1.
      assertEquals(0, sendOverdueMessagesInvocationCount);
      testGameManager.reconnectGameClient();
      while (sendOverdueMessagesInvocationCount < 1) {
        Thread.sleep(1);
      }
      assertNotEquals(oldConnectionId, testGameManager.getGameCommunicationClient().getID());
      oldConnectionId = testGameManager.getGameCommunicationClient().getID();

      // Reconnection 2.
      testGameManager.getGameCommunicationClient().close();
      while (testGameManager.getGameCommunicationClient().isConnected()) {
        Thread.sleep(1);
      }
      assertEquals(1, sendOverdueMessagesInvocationCount);
      testGameManager.reconnectGameClient();
      while (sendOverdueMessagesInvocationCount < 2) {
        Thread.sleep(1);
      }
      assertNotEquals(oldConnectionId, testGameManager.getGameCommunicationClient().getID());
      oldConnectionId = testGameManager.getGameCommunicationClient().getID();
    } finally {
      server.close();
    }
  }

  @Test
  public void testGameServerRediscoveryWorks() throws Exception {
    testGameManager.initialize();

    KryoNetCommunicator.injectConnectionTimeoutMillis(2000);
    Server server = null;
    try {
      server = new Server();

      final Kryo kryo =  server.getKryo();
      kryo.register(ClientMessage.class);
      kryo.register(ClientMessage.Type.class);
      kryo.register(ServerMessage.class);
      kryo.register(ServerMessage.Type.class);

      final Game mockGame0 = mock(Game.class);
      final GameSession gameSession0 = mock(GameSession.class);
      when(mockGame0.getId()).thenReturn(new UUID(12345, 67890));
      when(mockGame0.getTimestamp()).thenReturn(12345L);
      when(gameSession0.getGame()).thenReturn(mockGame0);
      final Game mockGame1 = mock(Game.class);
      final GameSession gameSession1 = mock(GameSession.class);
      when(mockGame1.getId()).thenReturn(new UUID(98765, 43210));
      when(mockGame1.getTimestamp()).thenReturn(54321L);
      when(gameSession1.getGame()).thenReturn(mockGame1);

      server.addListener(new Listener() {
        @Override
        public void received(Connection connection, Object object) {
          if (object instanceof ClientMessage) {
            final ClientMessage request = (ClientMessage) object;
            switch (request.getType()) {
              case GET_PENDING_GAMES: {
                Log.info("ClientMessage GET_PENDING_GAMES received from " + connection.getRemoteAddressTCP());
                connection.sendTCP(ServerMessage.createGetPendingGamesResponse(
                    new GameSession[]{gameSession0, gameSession1, }, "otherUser"));
                break;
              }
              case JOIN_GAME: {
                connection.sendTCP(ServerMessage.createJoinGameResponse(
                    UUID.fromString(request.getBody().getString("gameId")),
                    UUID.fromString(request.getBody().getString("userId")), true));
                break;
              }
              case GET_MATCHING_GAMES: {
                connection.sendTCP(ServerMessage.createGetMatchingGamesResponse(new GameSession[]{gameSession1}));
                break;
              }
              case RECONNECT_PLAYER: {
                connection.sendTCP(ServerMessage.createReconnectPlayerResponse(
                    UUID.fromString(request.getBody().getString("gameId")),
                    UUID.fromString(request.getBody().getString("userId")),
                    request.getBody().getString("hostString"), true));
                break;
              }
              default: {
                // Do nothing.
                break;
              }
            }
          }
        }
      });

      boolean serverBound = false;
      for (int i = 0; !serverBound && i < KryoNetCommunicator.PORT_TRIED_COUNT; ++i) {
        int serverTcpPort = KryoNetCommunicator.FIRST_TCP_PORT + i;
        int serverUdpPort = KryoNetCommunicator.FIRST_UDP_PORT + i;
        try {
          server.bind(serverTcpPort, serverUdpPort);
          serverBound = true;
        } catch (IOException exception) {
          Log.info("Local port combination (tcp=" + serverTcpPort + ", udp=" + serverUdpPort + ") already taken: "
              + exception);
        }
      }
      testGameManager.getServer().close();
      if (!serverBound) {
        throw new ServerException("Binding Server ports failed.");
      }

      server.start();

      testGameManager.getPendingGames();
      while (testGameManager.getPendingGamesList().size() < 2) {
        Thread.sleep(1);
      }
      while (!testGameManagerEngine.getStates().get(testGameManagerEngine.getStates().size() - 1)
          .equals(State.INITIAL_MENU)) {
        Thread.sleep(1);
      }
      assertEquals(State.GAMES_LISTED, testGameManager.getState());
      testGameManager.joinPendingGame(1);
      while (!testGameManagerEngine.getStates().get(testGameManagerEngine.getStates().size() - 1)
          .equals(State.GAMES_LISTED)) {
        Thread.sleep(1);
      }
      assertEquals(State.GAME_JOINED, testGameManager.getState());
      int oldConnectionId = testGameManager.getGameCommunicationClient().getID();

      // Make sure all sendOverdueMessages() invocations are counted.
      doAnswer(new Answer<Boolean>() {
        public Boolean answer(InvocationOnMock invocation) {
          try {
            ++sendOverdueMessagesInvocationCount;
            invocation.callRealMethod();
          } catch (Throwable throwable) {
            Log.error(throwable.getMessage());
            runtimeExceptionThrown = true;
          }
          return true;
        }
      }).when(testGameManager).sendOverdueMessages();

      // Reconnection 1.
      assertEquals(0, sendOverdueMessagesInvocationCount);
      testGameManager.rediscoverAndConnectGameServer();
      while (sendOverdueMessagesInvocationCount < 1) {
        Thread.sleep(1);
      }
      assertNotEquals(oldConnectionId, testGameManager.getGameCommunicationClient().getID());
      oldConnectionId = testGameManager.getGameCommunicationClient().getID();

      // Reconnection 2.
      testGameManager.getGameCommunicationClient().close();
      while (testGameManager.getGameCommunicationClient().isConnected()) {
        Thread.sleep(1);
      }
      assertEquals(1, sendOverdueMessagesInvocationCount);
      testGameManager.injectLastGameServerRedicoveryStartTimeMillis(0);
      testGameManager.rediscoverAndConnectGameServer();
      while (sendOverdueMessagesInvocationCount < 2) {
        Thread.sleep(1);
      }
      assertNotEquals(oldConnectionId, testGameManager.getGameCommunicationClient().getID());
      oldConnectionId = testGameManager.getGameCommunicationClient().getID();
    } finally {
      server.close();
    }
  }

  @Test
  public void testRejectingJoiningPendingGameWorks() throws Exception {
    testGameManager.initialize();

    KryoNetCommunicator.injectConnectionTimeoutMillis(2000);
    Server server = null;
    try {
      server = new Server();

      final Kryo kryo =  server.getKryo();
      kryo.register(ClientMessage.class);
      kryo.register(ClientMessage.Type.class);
      kryo.register(ServerMessage.class);
      kryo.register(ServerMessage.Type.class);

      final Game mockGame0 = mock(Game.class);
      final GameSession gameSession0 = mock(GameSession.class);
      when(mockGame0.getId()).thenReturn(new UUID(12345, 67890));
      when(mockGame0.getTimestamp()).thenReturn(12345L);
      when(gameSession0.getGame()).thenReturn(mockGame0);
      final Game mockGame1 = mock(Game.class);
      final GameSession gameSession1 = mock(GameSession.class);
      when(mockGame1.getId()).thenReturn(new UUID(98765, 43210));
      when(mockGame1.getTimestamp()).thenReturn(54321L);
      when(gameSession1.getGame()).thenReturn(mockGame1);

      server.addListener(new Listener() {
        @Override
        public void received(Connection connection, Object object) {
          if (object instanceof ClientMessage) {
            final ClientMessage request = (ClientMessage) object;
            switch (request.getType()) {
              case GET_PENDING_GAMES: {
                Log.info("ClientMessage GET_PENDING_GAMES received from " + connection.getRemoteAddressTCP());
                connection.sendTCP(ServerMessage.createGetPendingGamesResponse(
                    new GameSession[]{gameSession0, gameSession1, }, "otherUser"));
                break;
              }
              case JOIN_GAME: {
                // NOTE: We ignore this message and only react to a face SEND_MESSAGE, to better test the scenario.
                break;
              }
              // Handling fake SEND_MESSAGE messages.
              case SEND_MESSAGE: {
                connection.sendTCP(ServerMessage.createJoinGameResponse(
                    UUID.fromString(request.getBody().getString("gameId")),
                    UUID.fromString(request.getBody().getString("userId")), false));
                break;
              }
              default: {
                // Do nothing.
                break;
              }
            }
          }
        }
      });

      boolean serverBound = false;
      for (int i = 0; !serverBound && i < KryoNetCommunicator.PORT_TRIED_COUNT; ++i) {
        int serverTcpPort = KryoNetCommunicator.FIRST_TCP_PORT + i;
        int serverUdpPort = KryoNetCommunicator.FIRST_UDP_PORT + i;
        try {
          server.bind(serverTcpPort, serverUdpPort);
          serverBound = true;
        } catch (IOException exception) {
          Log.info("Local port combination (tcp=" + serverTcpPort + ", udp=" + serverUdpPort + ") already taken: "
              + exception);
        }
      }
      testGameManager.getServer().close();
      if (!serverBound) {
        throw new ServerException("Binding Server ports failed.");
      }

      server.start();

      testGameManager.getPendingGames();
      while (testGameManager.getPendingGamesList().size() < 2) {
        Thread.sleep(1);
      }
      while (!testGameManagerEngine.getStates().get(testGameManagerEngine.getStates().size() - 1)
          .equals(State.INITIAL_MENU)) {
        Thread.sleep(1);
      }
      assertEquals(State.GAMES_LISTED, testGameManager.getState());
      testGameManager.joinPendingGame(1);
      assertEquals(State.GAME_JOINED, testGameManager.getState());
      testGameManager.getGameCommunicationClient().sendTCP(ClientMessage.createSendMessage(new UUID(98765, 43210),
          new UUID(12345, 67890),
          new Message(new Event[]{new Event(Json.createObjectBuilder().add("type", "TEST").add("time", 0)
              .add("purpose", "This is is fake message intended to provoke the Server to send a rejecting response")
              .build())})));
      while (!testGameManager.getState().equals(State.GAMES_LISTED)) {
        Thread.sleep(1);
      }
      assertEquals(State.GAME_JOINED,
          testGameManagerEngine.getStates().get(testGameManagerEngine.getStates().size() - 1));
      /*
      while (!testGameManagerEngine.getStates().get(testGameManagerEngine.getStates().size() - 1)
          .equals(State.GAME_JOINED)) {
        Thread.sleep(1);
      }
      assertEquals(State.GAMES_LISTED, testGameManager.getState());
      */
    } finally {
      server.close();
    }
  }

  @Test
  public void testRejectingJoiningPendingGameWorks1() throws Exception {
    Log.info("testRejectingJoiningPendingGameWorks1()");
    testGameManager.initialize();

    KryoNetCommunicator.injectConnectionTimeoutMillis(2000);
    Server server = null;
    try {
      server = new Server();

      final Kryo kryo =  server.getKryo();
      kryo.register(ClientMessage.class);
      kryo.register(ClientMessage.Type.class);
      kryo.register(ServerMessage.class);
      kryo.register(ServerMessage.Type.class);

      final Game mockGame0 = mock(Game.class);
      final GameSession gameSession0 = mock(GameSession.class);
      when(mockGame0.getId()).thenReturn(new UUID(12345, 67890));
      when(mockGame0.getTimestamp()).thenReturn(12345L);
      when(gameSession0.getGame()).thenReturn(mockGame0);
      final Game mockGame1 = mock(Game.class);
      final GameSession gameSession1 = mock(GameSession.class);
      when(mockGame1.getId()).thenReturn(new UUID(98765, 43210));
      when(mockGame1.getTimestamp()).thenReturn(54321L);
      when(gameSession1.getGame()).thenReturn(mockGame1);

      server.addListener(new Listener() {
        @Override
        public void received(Connection connection, Object object) {
          if (object instanceof ClientMessage) {
            final ClientMessage request = (ClientMessage) object;
            switch (request.getType()) {
              case GET_PENDING_GAMES: {
                Log.info("ClientMessage GET_PENDING_GAMES received from " + connection.getRemoteAddressTCP());
                connection.sendTCP(ServerMessage.createGetPendingGamesResponse(
                    new GameSession[]{gameSession0, gameSession1, }, "otherUser"));
                break;
              }
              case JOIN_GAME: {
                connection.sendTCP(ServerMessage.createJoinGameResponse(
                    UUID.fromString(request.getBody().getString("gameId")),
                    UUID.fromString(request.getBody().getString("userId")), true));
                break;
              }
              // Handling fake SEND_MESSAGE messages.
              case SEND_MESSAGE: {
                connection.sendTCP(ServerMessage.createJoinGameResponse(
                    UUID.fromString(request.getBody().getString("gameId")),
                    UUID.fromString(request.getBody().getString("userId")), false));
                break;
              }
              default: {
                // Do nothing.
                break;
              }
            }
          }
        }
      });

      boolean serverBound = false;
      for (int i = 0; !serverBound && i < KryoNetCommunicator.PORT_TRIED_COUNT; ++i) {
        int serverTcpPort = KryoNetCommunicator.FIRST_TCP_PORT + i;
        int serverUdpPort = KryoNetCommunicator.FIRST_UDP_PORT + i;
        try {
          server.bind(serverTcpPort, serverUdpPort);
          serverBound = true;
          Log.info("Connected to: tcp=" + serverTcpPort + ", udp=" + serverUdpPort);
        } catch (IOException exception) {
          Log.info("Local port combination (tcp=" + serverTcpPort + ", udp=" + serverUdpPort + ") already taken: "
              + exception);
        }
      }
      if (!serverBound) {
        throw new ServerException("Binding Server ports failed.");
      }
      testGameManager.getServer().close();

      server.start();

      testGameManager.getPendingGames();
      while (testGameManager.getPendingGamesList().size() < 2) {
        Thread.sleep(1);
      }
      while (!testGameManagerEngine.getStates().get(testGameManagerEngine.getStates().size() - 1)
          .equals(State.INITIAL_MENU)) {
        Thread.sleep(1);
      }
      assertEquals(State.GAMES_LISTED, testGameManager.getState());
      testGameManager.joinPendingGame(1);
      while (!testGameManagerEngine.getStates().get(testGameManagerEngine.getStates().size() - 1)
          .equals(State.GAMES_LISTED)) {
        Thread.sleep(1);
      }
      assertEquals(State.GAME_JOINED, testGameManager.getState());
      testGameManager.getGameCommunicationClient().sendTCP(ClientMessage.createSendMessage(new UUID(98765, 43210),
          new UUID(12345, 67890),
          new Message(new Event[]{new Event(Json.createObjectBuilder().add("type", "TEST").add("time", 0)
              .add("purpose", "This is is fake message intended to provoke the Server to send a rejecting response")
              .build())})));
      while (testGameManagerEngine.getStates().get(testGameManagerEngine.getStates().size() - 1) != State.GAME_JOINED) {
        Thread.sleep(1);
      }
      assertEquals(State.GAMES_LISTED, testGameManager.getState());
    } finally {
      server.close();
    }
  }

  @Test
  public void testLeavingJoinedGameIntentWorks() throws Exception {
    testGameManager.initialize();

    KryoNetCommunicator.injectConnectionTimeoutMillis(2000);
    Server server = null;
    try {
      server = new Server();

      final Kryo kryo =  server.getKryo();
      kryo.register(ClientMessage.class);
      kryo.register(ClientMessage.Type.class);
      kryo.register(ServerMessage.class);
      kryo.register(ServerMessage.Type.class);

      final Game mockGame0 = mock(Game.class);
      final GameSession gameSession0 = mock(GameSession.class);
      when(mockGame0.getId()).thenReturn(new UUID(12345, 67890));
      when(mockGame0.getTimestamp()).thenReturn(12345L);
      when(gameSession0.getGame()).thenReturn(mockGame0);
      final Game mockGame1 = mock(Game.class);
      final GameSession gameSession1 = mock(GameSession.class);
      when(mockGame1.getId()).thenReturn(new UUID(98765, 43210));
      when(mockGame1.getTimestamp()).thenReturn(54321L);
      when(gameSession1.getGame()).thenReturn(mockGame1);

      server.addListener(new Listener() {
        @Override
        public void received(Connection connection, Object object) {
          if (object instanceof ClientMessage) {
            final ClientMessage request = (ClientMessage) object;
            switch (request.getType()) {
              case GET_PENDING_GAMES: {
                Log.info("ClientMessage GET_PENDING_GAMES received from " + connection.getRemoteAddressTCP());
                connection.sendTCP(ServerMessage.createGetPendingGamesResponse(
                    new GameSession[]{gameSession0, gameSession1, }, "otherUser"));
                break;
              }
              case JOIN_GAME: {
                connection.sendTCP(ServerMessage.createJoinGameResponse(
                    UUID.fromString(request.getBody().getString("gameId")),
                    UUID.fromString(request.getBody().getString("userId")), true));
                break;
              }
              case LEAVE_GAME_INTENT: {
                connection.sendTCP(ServerMessage.createLeaveGameIntentResponse(
                    UUID.fromString(request.getBody().getString("gameId")),
                    UUID.fromString(request.getBody().getString("userId")), true));
                break;
              }
              default: {
                // Do nothing.
                break;
              }
            }
          }
        }
      });

      boolean serverBound = false;
      for (int i = 0; !serverBound && i < KryoNetCommunicator.PORT_TRIED_COUNT; ++i) {
        int serverTcpPort = KryoNetCommunicator.FIRST_TCP_PORT + i;
        int serverUdpPort = KryoNetCommunicator.FIRST_UDP_PORT + i;
        try {
          server.bind(serverTcpPort, serverUdpPort);
          serverBound = true;
        } catch (IOException exception) {
          Log.info("Local port combination (tcp=" + serverTcpPort + ", udp=" + serverUdpPort + ") already taken: "
              + exception);
        }
      }
      testGameManager.getServer().close();
      if (!serverBound) {
        throw new ServerException("Binding Server ports failed.");
      }

      server.start();

      testGameManager.getPendingGames();
      while (testGameManager.getPendingGamesList().size() < 2) {
        Thread.sleep(1);
      }
      while (!testGameManagerEngine.getStates().get(testGameManagerEngine.getStates().size() - 1)
          .equals(State.INITIAL_MENU)) {
        Thread.sleep(1);
      }
      assertEquals(State.GAMES_LISTED, testGameManager.getState());
      testGameManager.joinPendingGame(1);
      while (!testGameManagerEngine.getStates().get(testGameManagerEngine.getStates().size() - 1)
          .equals(State.GAMES_LISTED)) {
        Thread.sleep(1);
      }
      assertEquals(State.GAME_JOINED, testGameManager.getState());
      testGameManager.leaveGameIntent();
      while (!testGameManagerEngine.getStates().get(testGameManagerEngine.getStates().size() - 1)
          .equals(State.GAME_JOINED)) {
        Thread.sleep(1);
      }
      assertEquals(State.GAMES_LISTED, testGameManager.getState());
    } finally {
      server.close();
    }
  }

  @Test
  public void testActionFinishedCheckingWorks() throws Exception {
    final int timeoutMillis = 100;
    KryoNetCommunicator.injectConnectionTimeoutMillis(timeoutMillis);

    // CANCEL_GAME
    testGameManager.checkActionFinished(SAMPLE_GAME_ID, Action.INITIALIZE_GAME);
    while (testGameManagerEngine.actionsNotFinished.size() < 1) {
      Thread.sleep(10);
    }
    assertEquals(Arrays.asList(Action.INITIALIZE_GAME), testGameManagerEngine.actionsNotFinished);

    // JOIN_GAME
    testGameManager.checkActionFinished(SAMPLE_GAME_ID, Action.JOIN_GAME);
    while (testGameManagerEngine.actionsNotFinished.size() < 2) {
      Thread.sleep(10);
    }
    assertEquals(Arrays.asList(Action.INITIALIZE_GAME, Action.JOIN_GAME),
        testGameManagerEngine.actionsNotFinished);

    // CANCEL_GAME 2
    testGameManager.checkActionFinished(SAMPLE_GAME_ID, Action.INITIALIZE_GAME);
    while (testGameManagerEngine.actionsNotFinished.size() < 3) {
      Thread.sleep(10);
    }
    assertEquals(Arrays.asList(Action.INITIALIZE_GAME, Action.JOIN_GAME, Action.INITIALIZE_GAME),
        testGameManagerEngine.actionsNotFinished);

    // Action confirmed.
    testGameManager.checkActionFinished(SAMPLE_GAME_ID, Action.LEAVE_GAME_INTENT);
    testGameManager.confirmActionFinished(SAMPLE_GAME_ID, Action.LEAVE_GAME_INTENT);
    Thread.sleep(2 * timeoutMillis);
    // NOTE: No new not finished Actions .
    assertEquals(Arrays.asList(Action.INITIALIZE_GAME, Action.JOIN_GAME, Action.INITIALIZE_GAME),
        testGameManagerEngine.actionsNotFinished);
  }

}
