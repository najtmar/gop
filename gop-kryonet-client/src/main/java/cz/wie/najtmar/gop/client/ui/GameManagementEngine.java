package cz.wie.najtmar.gop.client.ui;

import cz.wie.najtmar.gop.client.ui.GameManager.State;
import cz.wie.najtmar.gop.client.view.ClientViewException;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Any type of graphical engine supporting GameManager .
 * @author najtmar
 */
public interface GameManagementEngine {

  /**
   * Initializes the object.
   * @param gameManager the GameManaget on which in object depends
   * @throws ClientViewException when UI initialization fails
   */
  public void initializeGameManagementEngine(@Nonnull GameManager gameManager) throws ClientViewException;

  /**
   * Invoked when a change from oldState to a new State . The new State is available in the GameManager . The UI and
   * the sensitivity to actions is adjusted accordingly.
   * @param oldState the State to change from
   * @throws ClientViewException when the operation fails
   */
  public void onGameManagerStateChange(@Nullable State oldState) throws ClientViewException;

  /**
   * Updates the list of Pending Games. Only makes sense in State GAMES_LISTED .
   */
  public void updatePendingGamesList();

  /**
   * Sets whether starting a local Game is enabled.
   * @param isEnabled whether starting a local Game is enabled
   * @param participants the list of Game participants
   */
  public void setOwnGameEnabled(boolean isEnabled, @Nonnull List<String> participants);

  /**
   * Invoked when action is not finished within the established time limit.
   * @param action the Action that has not been finished within the established time limit
   */
  public void notifyActionNotFinished(@Nonnull GameManager.Action action);

}
