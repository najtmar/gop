package cz.wie.najtmar.gop.client.ui;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.minlog.Log;

import cz.wie.najtmar.gop.client.view.ClientViewException;
import cz.wie.najtmar.gop.client.view.GameEventProcessor;
import cz.wie.najtmar.gop.client.view.GameView;
import cz.wie.najtmar.gop.client.view.PlayerView;
import cz.wie.najtmar.gop.client.view.TextManager;
import cz.wie.najtmar.gop.client.view.UserView;
import cz.wie.najtmar.gop.common.TestProperties;
import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.event.EventProcessingException;
import cz.wie.najtmar.gop.event.SerializationException;
import cz.wie.najtmar.gop.game.PropertiesReader;
import cz.wie.najtmar.gop.io.Channel;
import cz.wie.najtmar.gop.io.Message;
import cz.wie.najtmar.gop.server.ClientMessage;
import cz.wie.najtmar.gop.server.ConnectionManager;
import cz.wie.najtmar.gop.server.GameSession;
import cz.wie.najtmar.gop.server.KryoNetCommunicator;
import cz.wie.najtmar.gop.server.ServerException;
import cz.wie.najtmar.gop.server.ServerMessage;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

import org.threeten.bp.Instant;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZonedDateTime;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.format.FormatStyle;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.GuardedBy;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonString;
import javax.json.JsonValue;

/**
 * KryoNet-based class for managing Games from the client-side.
 * @author najtmar
 *
 */
@Singleton
public class GameManager {

  /**
   * How many times try to connect to a Game before giving up.
   */
  public static final int GAME_CONNECTION_RETRY_COUNT = 3;

  /**
   * Time interval between two subsequent Game server rediscovery attempts.
   */
  public static final long GAME_SERVER_REDISCOVERY_INTERVAL_MILLIS = 10000;

  /**
   * Minimum number of undelivered messages that triggers Game client reconnection.
   */
  private static final int GAME_CLIENT_RECONNECTION_OVERDUE_MESSAGE_QUEUE_TRIGGER_SIZE = 3;

  /**
   * Minimum number of undelivered messages that triggers Game server rediscovery.
   */
  private static final int GAME_SERVER_REDISCOVERY_OVERDUE_QUEUE_TRIGGER_SIZE = 5;

  /**
   * The State of a GameManager .
   * @author najtmar
   */
  public enum State {
    /**
     * GameManager not yet initialized.
     */
    NOT_INITIALIZED,
    /**
     * User can either initialize or join a Game .
     */
    INITIAL_MENU,
    /**
     * Game initialized. Waiting for participants.
     */
    GAME_INITIATED,
    /**
     * Own game initialized and started.
     */
    OWN_GAME_STARTED,
    /**
     * Games to join listed.
     */
    GAMES_LISTED,
    /**
     * A Game joined.
     */
    GAME_JOINED,
    /**
     * A Game joined and started.
     */
    JOINED_GAME_STARTED,
  }

  /**
   * The Stack of States. The top State is the current State. Empty stateStack is equivalent to the NOT_INITIALIZED
   * State .
   */
  @GuardedBy("this")
  private final Stack<State> stateStack;

  /**
   * The asynchronous action that required network communication.
   * @author najtmar
   */
  public enum Action {
    /**
     * An own Game is initialized.
     */
    INITIALIZE_GAME,
    /**
     * A Game is joined.
     */
    JOIN_GAME,
    /**
     * A Game intent is left.
     */
    LEAVE_GAME_INTENT,
  }

  /**
   * Maps the id of a Game to a map between a recent Action to be confirmed and the time when the Action was started.
   */
  @GuardedBy("this")
  private final Map<UUID, Map<Action, Long>> gameActionToConfirmMap;

  /**
   * The User running the GameManager .
   */
  private final UserView user;

  /**
   * Processes Events for the locally hosted Game .
   */
  private GameEventProcessor eventProcessor;

  /**
   * The UI Client used to run Games .
   */
  private final cz.wie.najtmar.gop.client.ui.Client gameClient;

  /**
   * Used to read Properties used.
   */
  private final PropertiesReader propertiesReader;

  /**
   * Used in test runs.
   */
  private TestProperties testProperties;

  /**
   * In States OWN_GAME_STARTED and JOINED_GAME_STARTED, determines whether a Game has been created or not.
   */
  @GuardedBy("this")
  private boolean gameCreated;

  /**
   * The UI Session managing the User view of the Game .
   */
  private final Session uiSession;

  /**
   * Provides GameSessions .
   */
  private final Provider<GameSession> gameSessionProvider;

  /**
   * KryoNet client connected to the local server.
   */
  private final Client localCommunicationClient;

  /**
   * KryoNet Client used for server discovery.
   */
  @GuardedBy("this")
  private final Client serverDiscoveryClient;

  /**
   * Mutex controlling access to serverDediscoveryClientMutex operations.
   */
  private final Semaphore serverDiscoveryClientMutex;

  /**
   * An instance of KryoNetCommunicator, used for running local Games .
   */
  private final KryoNetCommunicator communicationServer;

  /**
   * UI engine behind the GameManager .
   */
  private final GameManagementEngine gameManagerEngine;

  /**
   * The TextManager instance used.
   */
  @SuppressWarnings("unused")
  private final TextManager textManager;

  /**
   * Contains all local host addresses.
   */
  private final Set<String> siteLocalHostAddresses;

  /**
   * Iff true then only local Servers are allowed. Used for testing.
   */
  private boolean onlyAllowLocalServers;

  /**
   * The GameSession hosted (only one is supported).
   */
  @GuardedBy("this")
  private GameSession gameSession;

  /**
   * The Game in which the User participates (only one is supported).
   */
  @GuardedBy("this")
  private UUID gamePlayedId;

  /**
   * The id of the last ServerMessage received for the current Game . Initialized with 0 .
   */
  @GuardedBy("this")
  private volatile int lastMessageId;

  /**
   * Timestamp of the last confirmative HEARBEAT_RESPONSE (Game present, not undecided).
   */
  @GuardedBy("this")
  private volatile long lastConfirmativeHeartbeatResponseTimeMillis;

  /**
   * true iff the User connection is up or the heartbeat thread is not runnning.
   */
  @GuardedBy("")
  private volatile boolean isUserConnectionUp;

  /**
   * The Player playing the Game (only one is supported).
   */
  @GuardedBy("this")
  private PlayerView gamePlayer;

  /**
   * The Channel of the Game currently being played (only one is supported).
   */
  private Channel gameChannel;

  /**
   * The same as gameChannel, except that it's seen from the client side.
   */
  private Channel gameUserChannel;

  /**
   * KryoNet client connected to the server of the Game being played (only one Game supported).
   */
  @GuardedBy("this")
  private Client gameCommunicationClient;

  /**
   * Mutex controlling access to gameCommunicationClient operations.
   */
  private final Semaphore gameCommunicationClientMutex;

  /**
   * Maps the id of a Game to the server that is hosting that game.
   */
  @GuardedBy("this")
  private final Map<UUID, InetSocketAddress> gameServerAddressMap;

  /**
   * Maps an InetAddress of a server to the client that handles that connection.
   * Added for separation of clients performing multi-server communication.
   */
  @GuardedBy("this")
  private final Map<InetSocketAddress, Client> serverQueryClientMap;

  /**
   * Maps the id of a Game to the last assigned id of a ClientMessage sent to the server.
   */
  @GuardedBy("this")
  private final Map<UUID, Integer> gameLastAssignedMessageIdMap;

  /**
   * A Queue keeping overdue ClientMessages, if these could not be sent.
   */
  @GuardedBy("this")
  private Queue<ClientMessage> overdueGameMessageQueue;

  /**
   * A Queue keeping overdue ClientMessages, as long as they have not been confirmed.
   */
  @GuardedBy("this")
  private Queue<ClientMessage> unconfirmedMessageQueue;

  /**
   * Determines whether reconnection is needed due to undelivered messages.
   */
  @GuardedBy("this")
  private volatile boolean reconnectionNeeded;

  /**
   * The time when the last Game client reconnection attempt started.
   */
  @GuardedBy("this")
  private volatile long lastGameServerRedicoveryStartTimeMillis;

  /**
   * true if gameCommunicationClient reconnecting is in progress.
   */
  @GuardedBy("true")
  private volatile boolean gameClientReconnectingInProgress;

  /**
   * true if no other Game client is reported to have a broken connection.
   */
  @GuardedBy("true")
  private volatile boolean otherGameClientsResponsive;

  /**
   * Class of Threads sending periodic HEARTBEAT messages to the Game server.
   * @author najtmar
   */
  private class HeartbeatThread implements Runnable {

    /**
     * Determines whether the thread is running or not.
     */
    @GuardedBy("this")
    private volatile boolean running = true;

    @Override
    public void run() {
      while (true) {
        synchronized (this) {
          try {
            wait(KryoNetCommunicator.getHeartbeatThreadInternalMillis());
          } catch (InterruptedException exception) {
            Log.error("Heartbeat wait(timeout) interrupted.");
          }
          if (!running) {
            return;
          }
        }
        final State state;
        final UUID gameId;
        final UUID userId;
        final int heartbeatMessageId;
        synchronized (GameManager.this) {
          state = doGetState();
          if (state != State.GAME_JOINED && state != State.JOINED_GAME_STARTED
              && state != State.GAME_INITIATED && state != State.OWN_GAME_STARTED) {
            continue;
          }
          gameId = gamePlayedId;
          userId = user.getId();
          heartbeatMessageId = ++lastHeartbeatMessageSentId;
        }
        Log.debug("Sending HEARTBEAT message with id " + heartbeatMessageId + " for Game " + gameId + " by User "
            + userId + ", undelivered messages: " + overdueGameMessageQueue.size());
        sendGameMessage(ClientMessage.createHeartbeat(heartbeatMessageId, gameId, userId, lastMessageId));
      }
    }

    public synchronized void setRunning(boolean running) {
      this.running = running;
    }

  }

  /**
   * Id of the last HEARTBEAT Message sent.
   */
  @GuardedBy("this")
  private volatile int lastHeartbeatMessageSentId;

  /**
   * Id of the last HEARTBEAT Message confirmed.
   */
  @GuardedBy("this")
  private volatile int lastHeartbeatMessageConfirmedId;

  /**
   * Thread sending periodic HEARTBEAT messages to the Game server.
   */
  @GuardedBy("this")
  private HeartbeatThread heartbeatThread;

  /**
   * Describes a Game being played.
   * @author najtmar
   */
  public class GameInfo {

    /**
     * Game id.
     */
    private final UUID id;

    /**
     * Wall clock time when the the Game started.
     */
    private final long timestamp;

    /**
     * The name of the User that initiated the Game .
     */
    private final String creatorName;

    /**
     * Constructor.
     * @param id Game id
     * @param timestamp wall clock time when the the Game started
     * @param creatorName the name of the User that initiated the Game
     */
    public GameInfo(@Nonnull UUID id, @Nonnegative long timestamp, @Nonnull String creatorName) {
      this.id = id;
      this.timestamp = timestamp;
      this.creatorName = creatorName;
    }

    public UUID getId() {
      return id;
    }

    public long getTimestamp() {
      return timestamp;
    }

    public String getCreatorName() {
      return creatorName;
    }

    @Override
    public String toString() {
      final Instant instant = Instant.ofEpochSecond(timestamp / 1000);
      final ZonedDateTime atZone = instant.atZone(ZoneId.systemDefault());
      final LocalDateTime localDateTime = atZone.toLocalDateTime();
      return creatorName + " (" + localDateTime.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM))
          .replaceFirst("0(\\d\\d\\d)", "$1") + ")";
    }

    @Override
    public int hashCode() {
      return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
      if (!(obj instanceof GameInfo)) {
        return false;
      }
      final GameInfo gameInfo = (GameInfo) obj;
      return id.equals(gameInfo.id);
    }

  }

  /**
   * The List of pending Games.
   */
  @GuardedBy("this")
  private final List<GameInfo> pendingGamesList;

  @Inject
  GameManager(GameManagementEngine gameManagerEngine, cz.wie.najtmar.gop.client.ui.Client gameClient,
      GameManagerManagedEngine gameEngine, PropertiesReader propertiesReader, Session uiSession,
      KryoNetCommunicator communicationServer, UserView user, Provider<GameSession> gameSessionProvider,
      TextManager textManager) {
    this.gameManagerEngine = gameManagerEngine;
    this.textManager = textManager;
    this.gameClient = gameClient;
    this.propertiesReader = propertiesReader;
    gameEngine.setGameManager(this);
    this.uiSession = uiSession;
    this.communicationServer = communicationServer;
    this.gameCommunicationClientMutex = new Semaphore(1);
    this.user = user;
    this.gameSessionProvider = gameSessionProvider;
    this.stateStack = new Stack<>();
    this.localCommunicationClient =
        new Client(KryoNetCommunicator.KRYO_NET_BUFFER_SIZE, KryoNetCommunicator.KRYO_NET_BUFFER_SIZE);
    this.serverDiscoveryClient =
        new Client(KryoNetCommunicator.KRYO_NET_SMALL_BUFFER_SIZE, KryoNetCommunicator.KRYO_NET_SMALL_BUFFER_SIZE);
    this.serverDiscoveryClient.start();
    this.serverDiscoveryClientMutex = new Semaphore(1);
    this.gameActionToConfirmMap = new HashMap<>();
    this.gameServerAddressMap = new HashMap<>();
    this.serverQueryClientMap = new HashMap<>();
    this.gameLastAssignedMessageIdMap = new HashMap<>();
    this.pendingGamesList = new LinkedList<>();
    this.siteLocalHostAddresses = new HashSet<>();
    this.onlyAllowLocalServers = false;
    this.overdueGameMessageQueue = new LinkedList<>();
    this.unconfirmedMessageQueue = new LinkedList<>();
    this.lastHeartbeatMessageSentId = -1;
    this.lastHeartbeatMessageConfirmedId = -1;
    this.lastMessageId = 0;
    this.isUserConnectionUp = true;
  }

  /**
   * Internal not synchronized implementation of method getState().
   * @return the current State of the object; the stateStack is empty, then State INITIAL is returned
   */
  private State doGetState() {
    if (!stateStack.isEmpty()) {
      return stateStack.peek();
    }
    return State.NOT_INITIALIZED;
  }

  /**
   * Returns the current State of the object.
   * @return the current State of the object; the stateStack is empty, then State INITIAL is returned
   */
  public synchronized State getState() {
    return doGetState();
  }

  /**
   * Changes the State by putting it on top of the current State .
   * @param state the State to change to
   */
  private void pushState(@Nonnull State state) {
    stateStack.push(state);
  }

  /**
   * Changes the State by removing the current State and applying the one earlier on the Stack .
   * @return the State being removed or null in case of a failure
   */
  private State popState() {
    if (stateStack.isEmpty()) {
      Log.error("stateStack is empty.");
      return null;
    }
    return stateStack.pop();
  }

  /**
   * Depending on the size of overdueGameMessageQueue, performs or not Game client reconnection attempts.
   * @param oldOverdueGameMessageQueueSize the size of overdueGameMessageQueueSize before the last operation on it was
   *     performed
   */
  private void handleOverdueGameMessageQueue(@Nonnegative int oldOverdueGameMessageQueueSize) {
    final boolean isReconnectionNeeded;
    synchronized (this) {
      isReconnectionNeeded = reconnectionNeeded;
    }
    try {
      // If needed, perform an action.
      if (isReconnectionNeeded) {
        if (GAME_SERVER_REDISCOVERY_OVERDUE_QUEUE_TRIGGER_SIZE <= oldOverdueGameMessageQueueSize) {
          doRediscoverAndConnectGameServer();
        } else if (GAME_CLIENT_RECONNECTION_OVERDUE_MESSAGE_QUEUE_TRIGGER_SIZE <= oldOverdueGameMessageQueueSize) {
          doReconnectGameClient();
        }
      }
    } catch (ClientViewException exception) {
      throw new RuntimeException(exception);
    }
  }

  /**
   * Depending on the delay of HEARTBEAT_RESPONSE messages, potentially notifies the User .
   */
  private void handleHeartbeatResponseDelay() {
    try {
      final boolean wasUserConnectionUp;
      final boolean currentIsUserConnectionUp;
      synchronized (this) {
        wasUserConnectionUp = isUserConnectionUp;
        isUserConnectionUp =
            System.currentTimeMillis() - lastConfirmativeHeartbeatResponseTimeMillis
            <= 3 * KryoNetCommunicator.getHeartbeatThreadInternalMillis();
        currentIsUserConnectionUp = isUserConnectionUp;
      }

      if (wasUserConnectionUp != currentIsUserConnectionUp) {
        if (!currentIsUserConnectionUp) {
          gameClient.notifyOwnGameServerConnection(false);
        } else {
          gameClient.notifyOwnGameServerConnection(true);
        }
      }
    } catch (ClientViewException exception) {
      throw new RuntimeException(exception);
    }
  }

  /**
   * Checks whether action is finished within the established time limit.
   * @param gameId the id of the Game for which the check is performed
   * @param action the Action to be checked
   */
  synchronized void checkActionFinished(@Nonnull UUID gameId, @Nonnull Action action) {
    final long startTimeMillis = System.currentTimeMillis();
    if (!gameActionToConfirmMap.containsKey(gameId)) {
      gameActionToConfirmMap.put(gameId, new HashMap<>());
    }
    gameActionToConfirmMap.get(gameId).put(action, startTimeMillis);
    final long maxTimeIntervalMillis = KryoNetCommunicator.getConnectionTimeoutMillis();
    new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          Thread.sleep(maxTimeIntervalMillis);
        } catch (InterruptedException exception) {
          throw new RuntimeException("Interrupted while checking Action " + action + " .");
        }
        final long finishTimeMillis = System.currentTimeMillis();
        final long foundStartTimeMillis;
        synchronized (GameManager.this) {
          if (!gameActionToConfirmMap.get(gameId).containsKey(action)) {
            return;
          }
          foundStartTimeMillis = gameActionToConfirmMap.get(gameId).get(action);
        }
        if (foundStartTimeMillis != startTimeMillis) {
          Log.info("Another Action " + action + " started later. Action finish checking aborted.");
          return;
        }
        if (maxTimeIntervalMillis <= finishTimeMillis - startTimeMillis) {
          Log.error("Action " + action + " has not finished in " + (finishTimeMillis - startTimeMillis)
              + " milliseconds (limit " + maxTimeIntervalMillis + "). Notifying.");
          gameManagerEngine.notifyActionNotFinished(action);
        }
      }
    }).start();
  }

  /**
   * Confirms that action has been finished.
   * @param gameId the id of the Game for which action is performed
   * @param action the Action that's confirmed to be finished
   */
  synchronized void confirmActionFinished(@Nonnull UUID gameId, @Nonnull Action action) {
    if (!gameActionToConfirmMap.containsKey(gameId)) {
      Log.error("No Action checking performed for Game with id " + gameId);
      return;
    }
    if (gameActionToConfirmMap.get(gameId).containsKey(action)) {
      gameActionToConfirmMap.get(gameId).remove(action);
    }
  }

  /**
   * Given message to be sent to the server, assigns an incremental id to message . Only ClientMessages of certain Types
   * are assigned ids.
   * @param message the message to assign the id to
   * @throws ClientViewException when the id is already assigned
   */
  private synchronized void assignClientMessageId(@Nonnull ClientMessage message)
      throws ClientViewException {
    final UUID gameId;
    switch (message.getType()) {
      case SEND_MESSAGE:
        gameId = UUID.fromString(message.getBody().getString("gameId"));
        break;
      default:
        return;
    }
    if (message.getId() != ClientMessage.ID_NOT_SET) {
      throw new ClientViewException("Message id already set: " + message);
    }
    if (!gameLastAssignedMessageIdMap.containsKey(gameId)) {
      gameLastAssignedMessageIdMap.put(gameId, 0);
    }
    final int newMessageId = gameLastAssignedMessageIdMap.get(gameId) + 1;
    message.setId(newMessageId);
    gameLastAssignedMessageIdMap.put(gameId, newMessageId);
  }

  /**
   * Sends overdue ClientMessages from overdueGameMessageQueue .
   * @return true iff all overdue ClientMessages have been sent successfully
   */
  boolean sendOverdueMessages() {
    while (true) {
      // Try finishing the loop.
      final int oldOverdueGameMessageQueueSize;
      synchronized (this) {
        oldOverdueGameMessageQueueSize = overdueGameMessageQueue.size();
      }
      if (oldOverdueGameMessageQueueSize == 0) {
        return true;
      }

      // Try sending the first message from the queue.
      if (!gameCommunicationClientMutex.tryAcquire()) {
        Log.info("sendOverdueMessages(): failed to acquire gameCommunicationClientMutex");
        return false;
      }
      try {
        final ClientMessage overdueMessage;
        synchronized (this) {
          overdueMessage = overdueGameMessageQueue.peek();
        }
        int bytesSent = gameCommunicationClient.sendTCP(overdueMessage);
        if (bytesSent > 0) {
          synchronized (this) {
            overdueGameMessageQueue.remove();
            reconnectionNeeded = false;
          }
          handleOverdueGameMessageQueue(oldOverdueGameMessageQueueSize);
        } else {
          synchronized (this) {
            reconnectionNeeded = true;
          }
          Log.warn("Sending overdue message failed, overdueGameMessageQueue.size(): " + oldOverdueGameMessageQueueSize
              + ", overdueMessage: " + overdueMessage);
          return false;
        }
      } finally {
        gameCommunicationClientMutex.release();
      }
    }
  }

  /**
   * Sends all messages from overdueGameMessageQueue using gameCommunicationClient, and then message, using
   * connection . If sending any message fails, the message is queued. If ClientMessages are now being sent,
   * message is queued immediately.
   * @param message the ClientMessage to be sent or queued
   * @return true iff all overdue messages and message have been sent
   */
  boolean sendGameMessage(@Nonnull ClientMessage message) {
    handleHeartbeatResponseDelay();
    // Enqueue ClientMessage.
    try {
      assignClientMessageId(message);
    } catch (ClientViewException exception) {
      Log.error("Error assigning id to a ClientMessage: " + exception);
      return false;
    }
    final Queue<ClientMessage> currentUnconfirmedMessageQueue;
    synchronized (this) {
      currentUnconfirmedMessageQueue = new LinkedList<>(unconfirmedMessageQueue);
      if (message.getId() != ClientMessage.ID_NOT_SET) {
        unconfirmedMessageQueue.add(message);
      }
    }

    // If overdue ClientMessages are being sent, just add message to the queue.
    if (!gameCommunicationClientMutex.tryAcquire()) {
      final int oldOverdueGameMessageQueueSize;
      synchronized (this) {
        oldOverdueGameMessageQueueSize = overdueGameMessageQueue.size();
        overdueGameMessageQueue.add(message);
      }
      Log.warn("Sending messages in progress, message queued, overdueGameMessageQueue.size(): "
          + oldOverdueGameMessageQueueSize + ", message: " + message);
      handleOverdueGameMessageQueue(oldOverdueGameMessageQueueSize);
      return false;
    }

    // Send the unconfirmed Client Messages first.
    try {
      for (ClientMessage unconfirmedMessage : currentUnconfirmedMessageQueue) {
        int bytesSent = gameCommunicationClient.sendTCP(unconfirmedMessage);
        if (bytesSent == 0) {
          Log.error("Sending ClientMessage from unconfirmedMessageQueue failed: " + unconfirmedMessage);
          break;
        }
      }
    } finally {
      gameCommunicationClientMutex.release();
    }

    // Send the overdue ClientMessages second.
    if (!sendOverdueMessages()) {
      final int oldOverdueGameMessageQueueSize;
      synchronized (this) {
        oldOverdueGameMessageQueueSize = overdueGameMessageQueue.size();
        overdueGameMessageQueue.add(message);
      }
      Log.warn("Message queued, overdueGameMessageQueue.size(): " + oldOverdueGameMessageQueueSize
          + ", message: " + message);
      handleOverdueGameMessageQueue(oldOverdueGameMessageQueueSize);
      return false;
    }

    // Send or queue message .
    final int bytesSent;
    boolean gameCommunicationClientMutexAcquired = false;
    try {
      gameCommunicationClientMutex.acquire();
      gameCommunicationClientMutexAcquired = true;
      bytesSent = gameCommunicationClient.sendTCP(message);
    } catch (InterruptedException exception) {
      Log.error("Waiting for gameCommunicationClientMutex interrupted while trying to send a message: " + exception);
      return false;
    } finally {
      if (gameCommunicationClientMutexAcquired) {
        gameCommunicationClientMutex.release();
      }
    }
    if (bytesSent == 0) {
      final int oldOverdueGameMessageQueueSize;
      synchronized (this) {
        oldOverdueGameMessageQueueSize = overdueGameMessageQueue.size();
        overdueGameMessageQueue.add(message);
        reconnectionNeeded = true;
        Log.warn("Sending Message failed, overdueGameMessageQueue.size(): " + overdueGameMessageQueue.size()
            + " message queued: " + message);
      }
      handleOverdueGameMessageQueue(oldOverdueGameMessageQueueSize);
      return false;
    }

    return true;
  }

  /**
   * Connects localCommunicationClient to the local Server .
   * @throws ClientViewException when the connecting process fails
   */
  private void connectLocalCommunicationClient() throws ClientViewException {
    Log.info("Local client-server reconnection process initiated.");
    boolean connected = false;
    for (int j = 0; j < GAME_CONNECTION_RETRY_COUNT; ++j) {
      for (int i = 0; !connected && i < KryoNetCommunicator.PORT_TRIED_COUNT; ++i) {
        try {
          localCommunicationClient.connect(KryoNetCommunicator.getConnectionTimeoutMillis(), InetAddress.getLocalHost(),
              KryoNetCommunicator.FIRST_TCP_PORT + i, KryoNetCommunicator.FIRST_UDP_PORT + i);
          connected = true;
        } catch (IOException exception) {
          Log.warn("Error connecting to localhost at ports tcp=" + (KryoNetCommunicator.FIRST_TCP_PORT + i) + ", udp="
              + (KryoNetCommunicator.FIRST_UDP_PORT + i) + ": " + exception);
        } finally {
          if (!connected) {
            localCommunicationClient.close();
          }
        }
      }
    }
    if (!connected) {
      throw new ClientViewException("Connection to the local server failed.");
    }

    communicationServer.startEstablishingLocalConnection();
    localCommunicationClient.sendTCP(ClientMessage.createRegisterLocalClient(user.getId(), user.getName()));
    while (communicationServer.isEstablishingLocalConnection()) {
      try {
        Thread.sleep(1);
      } catch (InterruptedException exception) {
        throw new ClientViewException("Waiting for the local server start-up interrupted.", exception);
      }
    }

  }

  /**
   * Filter message by its id. Update lastMessageId accordingly.
   * @param message the ServerMessage to be filtered
   * @return true iff the massage should be accepted
   */
  private synchronized boolean manageServerMessageFiltering(@Nonnull ServerMessage message) {
    final int messageId = message.getId();
    if (messageId == ServerMessage.ID_NOT_SET) {
      return true;
    }
    if (messageId != lastMessageId + 1) {
      Log.warn("Message filtered by id. Expected: " + (lastMessageId + 1) + ", actual: " + messageId);
      return false;
    }
    lastMessageId = messageId;
    return true;
  }

  /**
   * Initializes the GameManager .
   * @throws ClientViewException when the initialization process fails
   */
  public void initialize() throws ClientViewException {
    synchronized (this) {
      if (!doGetState().equals(State.NOT_INITIALIZED)) {
        throw new ClientViewException("GameManager should be in State NOT_INITIALIZED (found " + doGetState() + ").");
      }
    }
    if (!communicationServer.getState().equals(ConnectionManager.State.NOT_INITIALIZED)) {
      throw new ClientViewException("Server already initialized: " + communicationServer.getState());
    }
    testProperties = new TestProperties(propertiesReader.getTestProperties());
    try {
      communicationServer.initialize();
    } catch (ServerException exception) {
      throw new ClientViewException("Initializing the local server failed.", exception);
    }

    try {
      for (NetworkInterface ni : Collections.list(NetworkInterface.getNetworkInterfaces())) {
        for (InetAddress ia : Collections.list(ni.getInetAddresses())) {
          if (ia.isSiteLocalAddress()) {
            siteLocalHostAddresses.add(ia.getHostAddress());
          }
        }
      }
    } catch (SocketException exception) {
      throw new ClientViewException("Getting local network interfaces failed.", exception);
    }

    final Kryo kryo = localCommunicationClient.getKryo();
    kryo.register(ClientMessage.class);
    kryo.register(ClientMessage.Type.class);
    kryo.register(ServerMessage.class);
    kryo.register(ServerMessage.Type.class);
    localCommunicationClient.addListener(new Listener() {
      @Override
      public void received(Connection connection, Object object) {
        if (object instanceof ServerMessage) {
          final ServerMessage serverMessage = (ServerMessage) object;
          if (!manageServerMessageFiltering(serverMessage)) {
            return;
          }
          switch (serverMessage.getType()) {
            case INITIALIZE_GAME_RESPONSE: {
              final UUID gameId = UUID.fromString(serverMessage.getBody().getString("gameId"));
              final UUID userId = UUID.fromString(serverMessage.getBody().getString("userId"));
              final boolean result = serverMessage.getBody().getBoolean("result");
              onInitializeGameResponse(gameId, userId, result);
              break;
            }
            case REQUEST_ADD_PLAYER: {
              final UUID gameId = UUID.fromString(serverMessage.getBody().getString("gameId"));
              final UUID userId = UUID.fromString(serverMessage.getBody().getString("userId"));
              final int index = serverMessage.getBody().getInt("index");
              onRequestAddPlayer(gameId, userId, index);
              break;
            }
            case SEND_MESSAGE: {
              onSendMessage(serverMessage);
              break;
            }
            case JOIN_GAME_RESPONSE: {
              final UUID gameId = UUID.fromString(serverMessage.getBody().getString("gameId"));
              final UUID userId = UUID.fromString(serverMessage.getBody().getString("userId"));
              final boolean result = serverMessage.getBody().getBoolean("result");
              onJoinGameResponse(gameId, userId, result);
              break;
            }
            case CLIENT_MESSAGE_RESPONSE: {
              // Just ignore.
              break;
            }
            case CANCEL_GAME_INTENT_RESPONSE: {
              final UUID gameId = UUID.fromString(serverMessage.getBody().getString("gameId"));
              final UUID userId = UUID.fromString(serverMessage.getBody().getString("userId"));
              final boolean result = serverMessage.getBody().getBoolean("result");
              onCancelGameResponse(gameId, userId, result);
              break;
            }
            case OWN_GAME_START_ENABLED_STATUS: {
              final boolean isEnabled = serverMessage.getBody().getBoolean("isEnabled");
              final List<String> participants =
                  StreamSupport.stream(serverMessage.getBody().getJsonArray("participants"))
                  .map(value -> ((JsonString) value).getString()).collect(Collectors.toList());
              gameManagerEngine.setOwnGameEnabled(isEnabled, participants);
              break;
            }
            case HEARTBEAT_RESPONSE:
              final int heartbeatMessageId = serverMessage.getBody().getInt("id");
              final boolean gamePresent = serverMessage.getBody().getBoolean("gamePresent");
              final boolean gameUndecided = serverMessage.getBody().getBoolean("gameUndecided");
              final List<UUID> unresponsiveParticipants =
                  StreamSupport.stream(serverMessage.getBody().getJsonArray("unresponsiveParticipants"))
                  .map(value -> UUID.fromString(((JsonString) value).getString())).collect(Collectors.toList());
              final int lastMessageId = serverMessage.getBody().getInt("lastMessageId");
              onHeartbeatResponse(heartbeatMessageId, gamePresent, gameUndecided, unresponsiveParticipants,
                  lastMessageId);
              break;
            case START_GAME:
            case RECONNECT_PLAYER_RESPONSE:
            default:
              throw new IllegalArgumentException("Unexpected ServerMessage type: " + serverMessage.getType());
          }
        }
      }
    });
    localCommunicationClient.start();

    connectLocalCommunicationClient();

    synchronized (this) {
      pushState(State.INITIAL_MENU);
    }
    gameManagerEngine.initializeGameManagementEngine(this);
    gameManagerEngine.onGameManagerStateChange(State.NOT_INITIALIZED);
  }

  /**
   * Shuts down the server and closes the GameManager .
   */
  public void close() {
    communicationServer.close();
  }

  /**
   * Initializes gameChannel and gameUserChannel used for local communication.
   * @return true iff the initialization succeeds
   */
  private boolean initializeChannels() {
    final BlockingQueue<Message> inputQueue = new LinkedBlockingQueue<>();
    final BlockingQueue<Message> outputQueue = new LinkedBlockingQueue<>();
    try {
      gameChannel = new Channel(inputQueue, outputQueue);
      gameUserChannel = new Channel(outputQueue, inputQueue);
    } catch (IOException exception) {
      Log.error("Error creating communication Channels: " + exception);
      return false;
    }
    return true;
  }

  /**
   * Sets gameCommunicationClient to client . Closes the old client, if possible.
   * @param client the client to set
   * @return client
   */
  private synchronized Client setGameCommunicationClient(@Nullable Client client) {
    if (gameCommunicationClient != null && !gameCommunicationClient.equals(localCommunicationClient)) {
      boolean gameCommunicationClientMutexAcquired = false;
      try {
        gameCommunicationClientMutex.acquire();
        gameCommunicationClientMutexAcquired = true;
        gameCommunicationClient.close();
      } catch (InterruptedException exception) {
        Log.error("Waiting for gameCommunicationClientMutex interrupted while trying to close the old client: "
            + exception);
      } finally {
        if (gameCommunicationClientMutexAcquired) {
          gameCommunicationClientMutex.release();
        }
      }
    }
    gameCommunicationClient = client;
    return client;
  }

  /**
   * Initiates a locally hosted Game .
   * @throws ClientViewException when switching the State fails
   */
  public synchronized void initiateGameIntent() throws ClientViewException {
    if (!doGetState().equals(State.INITIAL_MENU)) {
      Log.warn("GameManager should be in State INITIAL_MENU (found " + doGetState() + ").");
      return;
    }
    gameSession = gameSessionProvider.get();
    gameSession.start();
    while (gameSession.getGame() == null) {
      try {
        wait(1);
      } catch (InterruptedException exception) {
        Log.error("Interrupted when waiting for Game id: " + exception);
        return;
      }
    }
    gamePlayedId = gameSession.getGame().getId();
    lastMessageId = 0;
    lastConfirmativeHeartbeatResponseTimeMillis = System.currentTimeMillis();
    isUserConnectionUp = true;
    setGameCommunicationClient(localCommunicationClient);
    if (!initializeChannels()) {
      return;
    }
    pushState(State.GAME_INITIATED);
    otherGameClientsResponsive = true;
    gameManagerEngine.onGameManagerStateChange(State.INITIAL_MENU);
  }

  /**
   * Tries to send message using localCommunicationClient . Reestablishes local client connection and retries one if
   * message cannot be sent.
   * @param message the ClientMessage to be sent
   * @throws ClientViewException when reestablishing local connection fails
   */
  private void sendLocalMessage(@Nonnull ClientMessage message) throws ClientViewException {
    int bytesSent = localCommunicationClient.sendTCP(message);
    if (bytesSent == 0) {
      Log.error("Sending ClientMessage failed: " + message + "\nReestablishing local connection.");
      localCommunicationClient.close();
      connectLocalCommunicationClient();
      localCommunicationClient.sendTCP(message);
    }
  }

  /**
   * Initializes a locally hosted Game intent.
   * @throws ClientViewException when switching the State fails
   */
  public void initializeGame() throws ClientViewException {
    if (!doGetState().equals(State.GAME_INITIATED)) {
      Log.warn("GameManager should be in State GAME_INITIATED (found " + doGetState() + ").");
      return;
    }
    checkActionFinished(gameSession.getGame().getId(), Action.INITIALIZE_GAME);
    sendLocalMessage(ClientMessage.createInitializeGame(gameSession.getGame().getId(), user.getId()));
  }

  /**
   * Handles responses to INITIALIZE_GAME requests.
   * @param responseGameId the id of the Game to start
   * @param responseUserId the id of the User of the Game
   * @param result whether the switching process succeeded or failed
   */
  private synchronized void onInitializeGameResponse(@Nonnull UUID responseGameId, @Nonnull UUID responseUserId,
      boolean result) {
    confirmActionFinished(responseGameId, Action.INITIALIZE_GAME);
    if (!doGetState().equals(State.GAME_INITIATED)) {
      Log.warn("GameManager should be in State GAME_INITIATED (found " + doGetState() + ").");
      return;
    }
    if (!responseGameId.equals(gameSession.getGame().getId())) {
      Log.warn("Unrecognized Game id " + responseGameId + " (expected " + gameSession.getGame().getId() + " .");
      popState();
      return;
    }
    if (!responseUserId.equals(user.getId())) {
      Log.warn("Unrecognized User id " + responseGameId + " (expected " + user.getId() + " .");
      popState();
      return;
    }
    if (result) {
      pushState(State.OWN_GAME_STARTED);
      gameCreated = false;
      final int oldOverdueGameMessageQueueSize;
      synchronized (this) {
        oldOverdueGameMessageQueueSize = overdueGameMessageQueue.size();
      }
      overdueGameMessageQueue.clear();
      unconfirmedMessageQueue.clear();
      reconnectionNeeded = false;
      handleOverdueGameMessageQueue(oldOverdueGameMessageQueueSize);
      try {
        gameManagerEngine.onGameManagerStateChange(State.INITIAL_MENU);
      } catch (ClientViewException exception) {
        popState();
        throw new RuntimeException("Visualizing the State change failed.", exception);
      }
      tryStopHeartbeatThread();
      tryStartHeartbeatThread();
    } else {
      tryStopHeartbeatThread();
    }
  }

  /**
   * Handles requests JOINED_GAME_STARTED .
   * @param responseGameId the id of the Game to start
   * @param responseUserId the id of the User of the Game
   */
  private synchronized void onJoinedGameStarted(@Nonnull UUID responseGameId, @Nonnull UUID responseUserId) {
    if (!doGetState().equals(State.GAME_JOINED)) {
      Log.warn("GameManager should be in State GAME_JOINED (found " + doGetState() + ").");
      return;
    }
    if (!responseGameId.equals(gamePlayedId)) {
      Log.warn("Unrecognized Game id " + responseGameId + " (expected " + gamePlayedId + " .");
      popState();
      return;
    }
    if (!responseUserId.equals(user.getId())) {
      Log.warn("Unrecognized User id " + responseGameId + " (expected " + user.getId() + " .");
      popState();
      return;
    }
    pushState(State.JOINED_GAME_STARTED);
    gameCreated = false;
    try {
      gameManagerEngine.onGameManagerStateChange(State.GAME_JOINED);
    } catch (ClientViewException exception) {
      popState();
      throw new RuntimeException("Visualizing the State change failed.", exception);
    }
  }

  /**
   * Handles requests REQUEST_ADD_PLAYER .
   * @param gameId the id of the Game for which the ADD_PLAYER is requested
   * @param userId the id of the user for which the ADD_PLAYER is requested
   * @param index the index of the Player in the Game
   */
  private void onRequestAddPlayer(@Nonnull UUID gameId, @Nonnull UUID userId, @Nonnegative int index) {
    synchronized (this) {
      final State state = doGetState();
      if (!state.equals(State.OWN_GAME_STARTED) && !state.equals(State.JOINED_GAME_STARTED)) {
        Log.error(
            "GameManager should be either in State equal to either OWN_GAME_STARTED or JOINED_GAME_STARTED (found "
            + state + ").");
        return;
      }
      if (!gameId.equals(gamePlayedId)) {
        Log.error("Game id " + gameId + " not equal to " + gamePlayedId);
        return;
      }
      if (!userId.equals(user.getId())) {
        Log.error("User id " + userId + " not equal to " + user.getId());
        return;
      }
    }
    try {
      final ClientMessage message = ClientMessage.createSendMessage(gamePlayedId, user.getId(),
          new Message(new Event[]{(new PlayerView(user, index)).createAddPlayerEvent(0)}));
      if (!sendGameMessage(message)) {
        Log.warn("Message ADD_PLAYER failed to be sent: " + message);
        return;
      }
    } catch (EventProcessingException exception) {
      Log.error("Creating ADD_PLAYER Message failed for Game with id " + gamePlayedId + " and User with id "
          + user.getId() + " .");
      return;
    }
  }

  /**
   * Cancels a locally hosted Game intent .
   * @throws ClientViewException when switching the State fails
   */
  public void cancelGameIntent() throws ClientViewException {
    synchronized (this) {
      if (!doGetState().equals(State.GAME_INITIATED)) {
        Log.warn("GameManager should be in State GAME_INITIATED (found " + doGetState() + ").");
        return;
      }
      pushState(State.INITIAL_MENU);
    }
    sendLocalMessage(ClientMessage.createCancelGameIntent(gameSession.getGame().getId(), user.getId()));
  }

  /**
   * Handles responses to CANCEL_GAME requests.
   * @param responseGameId the id of the Game to start
   * @param responseUserId the id of the User of the Game
   * @param result whether the switching process succeeded or failed
   */
  private synchronized void onCancelGameResponse(@Nonnull UUID responseGameId, @Nonnull UUID responseUserId,
      boolean result) {
    if (!doGetState().equals(State.INITIAL_MENU)) {
      Log.warn("GameManager should be in State INITIAL_MENU (found " + doGetState() + ").");
      return;
    }
    if (gameSession == null || gameSession.getGame() == null) {
      Log.warn("GameSession not initiated.");
      return;
    }
    if (!responseGameId.equals(gameSession.getGame().getId())) {
      Log.warn("Unrecognized Game id " + responseGameId + " (expected " + gameSession.getGame().getId() + " .");
      return;
    }
    if (!responseUserId.equals(user.getId())) {
      Log.warn("Unrecognized User id " + responseGameId + " (expected " + user.getId() + " .");
      return;
    }
    if (result) {
      try {
        gameManagerEngine.onGameManagerStateChange(State.GAME_INITIATED);
      } catch (ClientViewException exception) {
        throw new RuntimeException("Visualizing the State change failed.", exception);
      }
      tryStopHeartbeatThread();
    }
  }

  /**
   * Initializes gameClient with a Game and sets gameCreated .
   * @param initializeGameEvent Event of type INITIALIZE_GAME
   * @return true iff gameClient initialization succeeds
   */
  private boolean initializeGameClient(@Nonnull Event initializeGameEvent) {
    final JsonObject gameObject = initializeGameEvent.getBody().getJsonObject("game");
    // Figure out the Player index.
    final JsonArray playersArray = gameObject.getJsonArray("players");
    int playerIndex = -1;
    for (JsonValue playerValue : playersArray) {
      final JsonObject playerObject = (JsonObject) playerValue;
      final JsonObject userObject = playerObject.getJsonObject("user");
      final UUID userId = UUID.fromString(userObject.getString("uuid"));
      if (userId.equals(user.getId())) {
        playerIndex = playerObject.getInt("index");
        break;
      }
    }
    if (playerIndex == -1) {
      Log.error("Current user with id " + user.getId() + " not found in Game " + gameObject);
      return false;
    }

    // Create the Game and initialize the Game Client and GameEventProcessor .
    final GameView game;
    try {
      game = new GameView(initializeGameEvent.getBody().getJsonObject("game"), playerIndex);
      uiSession.initialize(gameClient, game);
    } catch (ClientViewException exception) {
      Log.error("Creating Game from object " + gameObject + " failed: " + exception);
      return false;
    }
    eventProcessor = new GameEventProcessor(gameUserChannel, game);
    gamePlayer = game.getPlayers()[game.getCurrentPlayerIndex()];
    synchronized (this) {
      gameCreated = true;
    }
    return true;
  }

  /**
   * Processes events received in a SEND_MESSAGE ServerMessage .
   * @param message the Message received
   * @return true iff Event processing succeeds
   */
  private boolean processMessageReceived(@Nonnull Message message) {
    final Event[] events = message.getEvents();
    if (events.length == 0) {
      return true;
    }

    // If needed, initialize the Game .
    int firstEventIndex = 0;
    if (events[0].getType().equals(Event.Type.INITIALIZE_GAME)) {
      synchronized (this) {
        if (gameCreated) {
          Log.error("INITIALIZE_GAME received while a Game has already been created.");
          return false;
        }
      }
      if (!initializeGameClient(events[0])) {
        return false;
      }
      firstEventIndex = 1;
    } else {
      synchronized (this) {
        if (!gameCreated) {
          Log.error("Events other than INITIALIZE_GAME sent although the Game hasn't been initialized: "
              + Arrays.toString(events));
          return false;
        }
      }
    }
    final Event[] inputEvents = Arrays.copyOfRange(events, firstEventIndex, events.length);

    // Process Events.
    final Event[] eventsForTheUser;
    try {
      gameChannel.writeMessage(new Message(inputEvents));
      eventsForTheUser = eventProcessor.readAndProcessEvents();
    } catch (InterruptedException | SerializationException | ClientViewException exception) {
      Log.error("Local Event processing failed: " + exception);
      return false;
    }

    // Query User for Events.
    new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          final Event[] outputEvents = uiSession.processEvents(eventsForTheUser, gamePlayer);
          final ClientMessage message = ClientMessage.createSendMessage(gamePlayedId, user.getId(),
              new Message(outputEvents));
          if (!sendGameMessage(message)) {
            Log.warn("Sending ClientMessage failed: " + message);
            return;
          }
        } catch (ClientViewException exception) {
          Log.error("Events " + Arrays.toString(eventsForTheUser) + ": " + "failed to be processed: " + exception);
          return;
        }
      }
    }).start();

    return true;
  }

  /**
   * Handles requests SEND_MESSAGE .
   * @param serverMessage the ServerMessage of type SEND_MESSAGE
   */
  private void onSendMessage(@Nonnull ServerMessage serverMessage) {
    final State state;
    synchronized (this) {
      state = doGetState();
      if (!state.equals(State.OWN_GAME_STARTED) && !state.equals(State.JOINED_GAME_STARTED)) {
        Log.error(
            "GameManager should be either in State equal to either OWN_GAME_STARTED or JOINED_GAME_STARTED (found "
            + state + ").");
        return;
      }
    }

    final UUID gameId = UUID.fromString(serverMessage.getBody().getString("gameId"));
    final UUID userId = UUID.fromString(serverMessage.getBody().getString("userId"));
    final int hashCode = serverMessage.getBody().getInt("hashCode");
    final List<Event> eventList = new LinkedList<>();
    for (JsonValue value : serverMessage.getBody().getJsonArray("events")) {
      final JsonObject eventSerialized = (JsonObject) value;
      try {
        eventList.add(new Event(eventSerialized));
      } catch (EventProcessingException exception) {
        throw new IllegalArgumentException("Cannot parse Event " + eventSerialized + " in ServerMessage "
            + serverMessage, exception);
      }
    }
    final Message message = new Message(eventList.toArray(new Event[]{}));
    if (hashCode != message.hashCode()) {
      throw new IllegalArgumentException("hashCode " + hashCode + " doesn't match Message hashCode "
        + message.hashCode());
    }

    if (!gameId.equals(gamePlayedId)) {
      Log.warn("Invalid Game id " + gameId + " (should be " + gamePlayedId + " .");
      return;
    }
    if (!userId.equals(user.getId())) {
      Log.warn("Invalid User id " + gameId + " (should be " + user.getId() + " .");
      return;
    }
    processMessageReceived(message);
    boolean gameCommunicationClientMutexAcquired = false;
    try {
      gameCommunicationClientMutex.acquire();
      gameCommunicationClientMutexAcquired = true;
      gameCommunicationClient.sendTCP(ClientMessage.createServerMessageResponse(gameId, userId, message.hashCode()));
    } catch (InterruptedException exception) {
      Log.error("Waiting for gameCommunicationClientMutex interrupted while trying to send a SEND_MESSAGE_RESPONSE to "
          + "the server.");
      return;
    } finally {
      if (gameCommunicationClientMutexAcquired) {
        gameCommunicationClientMutex.release();
      }
    }
  }

  /**
   * Discovers local Servers running in the LAN. Ignores the local Server.
   * @return socket addresses of the TCP ports of Servers running in the LAN, excluding the local one
   * @throws ClientViewException when server discovery process fails
   */
  private List<InetSocketAddress> discoverServerSocketAddresses() throws ClientViewException {
    if (!serverDiscoveryClientMutex.tryAcquire()) {
      Log.warn("serverDiscoveryClient busy. Server discovery interrupted.");
      return Arrays.asList();
    }
    try {
      // serverAddressList can either be discovered or hardcoded in TestProperties .
      final List<?>[] serverAddressList;
      final List<InetSocketAddress> testServerSocketAddresses = testProperties.getServerSocketAddresses();
      if (testServerSocketAddresses != null) {
        return testServerSocketAddresses;
      }

      serverAddressList = new List<?>[KryoNetCommunicator.PORT_TRIED_COUNT];
      final Thread[] requestThreads = new Thread[KryoNetCommunicator.PORT_TRIED_COUNT];
      for (int i = 0; i < KryoNetCommunicator.PORT_TRIED_COUNT; ++i) {
        final int index = i;
        requestThreads[i] = new Thread(new Runnable() {
          @Override
          public void run() {
            try {
              serverAddressList[index] = serverDiscoveryClient.discoverHosts(KryoNetCommunicator.FIRST_UDP_PORT + index,
                  KryoNetCommunicator.getConnectionTimeoutMillis());
            } catch (NullPointerException exception) {
              Log.error("Error discovering hosts: " + exception);
              serverAddressList[index] = new LinkedList<>();
            }
          }
        });
        requestThreads[i].start();
      }
      for (Thread requestThread : requestThreads) {
        try {
          requestThread.join();
        } catch (InterruptedException exception) {
          throw new ClientViewException("Error while waiting for a Server address requesting thread.", exception);
        }
      }

      final List<InetSocketAddress> result = new LinkedList<>();
      for (List<?> iter : serverAddressList) {
        @SuppressWarnings("unchecked")
        final List<InetAddress> serverAddresses = (List<InetAddress>) iter;
        if (serverAddresses == null) {
          continue;
        }
        for (InetAddress serverAddress : serverAddresses) {
          boolean succeeded = false;
          for (int i = 0; !succeeded && i < KryoNetCommunicator.PORT_TRIED_COUNT; ++i) {
            final int tcpPort = KryoNetCommunicator.FIRST_TCP_PORT + i;
            // Ignore the locally hosted server.
            if (serverAddress.equals(InetAddress.getLoopbackAddress())) {
              continue;
            }
            if (siteLocalHostAddresses.contains(serverAddress.getHostAddress())
                && tcpPort == communicationServer.getServerTcpPort()) {
              continue;
            }
            if (onlyAllowLocalServers && !siteLocalHostAddresses.contains(serverAddress.getHostAddress())) {
              continue;
            }

            final int udpPort = KryoNetCommunicator.FIRST_UDP_PORT + i;
            try {
              serverDiscoveryClient.connect(KryoNetCommunicator.getConnectionTimeoutMillis() / 3,
                  serverAddress.getHostAddress(), tcpPort, udpPort);
              succeeded = true;
              result.add(new InetSocketAddress(serverAddress, tcpPort));
            } catch (IOException exception) {
              Log.info("Connection to " + serverAddress.getHostAddress() + ":" + tcpPort + " failed: " + exception);
            } finally {
              serverDiscoveryClient.close();
            }
          }
        }
      }
      return result;
    } finally {
      serverDiscoveryClientMutex.release();
    }
  }

  /**
   * Creates a simple Client with small-size buffers, intended to discover Game servers and help with reconnecting.
   * @return a simple Client with small-size buffers, intended to discover Game servers and help with reconnecting
   */
  private Client createDiscoveryClient() {
    final Client client =
        new Client(KryoNetCommunicator.KRYO_NET_SMALL_BUFFER_SIZE, KryoNetCommunicator.KRYO_NET_SMALL_BUFFER_SIZE);
    client.start();
    final Kryo kryo = client.getKryo();
    kryo.register(ClientMessage.class);
    kryo.register(ClientMessage.Type.class);
    kryo.register(ServerMessage.class);
    kryo.register(ServerMessage.Type.class);

    client.addListener(new Listener() {
      @Override
      public void received(Connection connection, Object object) {
        if (object instanceof ServerMessage) {
          final ServerMessage serverMessage = (ServerMessage) object;
          switch (serverMessage.getType()) {
            case GET_PENDING_GAMES_RESPONSE: {
              final List<GameInfo> newPendingGames = new LinkedList<>();
              for (JsonValue value : serverMessage.getBody().getJsonArray("pendingGames")) {
                JsonObject pendingGameObject = (JsonObject) value;
                final UUID gameId = UUID.fromString(pendingGameObject.getString("uuid"));
                final long timestamp = pendingGameObject.getJsonNumber("timestamp").longValue();
                final String creatorName = pendingGameObject.getString("creatorName");
                newPendingGames.add(new GameInfo(gameId, timestamp, creatorName));
              }
              if (connection.getRemoteAddressTCP() == null) {
                Log.error("null address in " + connection);
                break;
              }
              onGetPendingGamesResponse(newPendingGames, connection.getRemoteAddressTCP());
              break;
            }
            case GET_MATCHING_GAMES_RESPONSE: {
              final List<UUID> matchingGames = new LinkedList<>();
              for (JsonValue value : serverMessage.getBody().getJsonArray("games")) {
                JsonObject gameObject = (JsonObject) value;
                matchingGames.add(UUID.fromString(gameObject.getString("uuid")));
              }
              if (connection.getRemoteAddressTCP() == null) {
                Log.error("null address in " + connection);
                break;
              }
              onGetMatchingGamesResponse(matchingGames, connection.getRemoteAddressTCP());
              break;
            }
            default:
              throw new IllegalArgumentException("Unexpected ServerMessage type: " + serverMessage.getType());
          }
        }
      }
    });

    return client;
  }

  /**
   * Implementation of method refreshPendingGamesList().
   * @throws ClientViewException when server discovery fails
   */
  private void doRefreshPendingGamesList() throws ClientViewException {
    synchronized (this) {
      pendingGamesList.clear();
      gameServerAddressMap.clear();
    }
    for (InetSocketAddress serverSocketAddress : discoverServerSocketAddresses()) {
      Client client = null;
      synchronized (this) {
        if (serverQueryClientMap.containsKey(serverSocketAddress)) {
          client = serverQueryClientMap.get(serverSocketAddress);
        }
      }
      if (client == null) {
        client = createDiscoveryClient();
        synchronized (this) {
          serverQueryClientMap.put(serverSocketAddress, client);
        }
      }
      final int tcpPort = serverSocketAddress.getPort();
      final int udpPort = KryoNetCommunicator.FIRST_UDP_PORT + (tcpPort - KryoNetCommunicator.FIRST_TCP_PORT);
      try {
        client.connect(KryoNetCommunicator.getConnectionTimeoutMillis(),
            serverSocketAddress.getHostName(), tcpPort, udpPort);
        client.sendTCP(ClientMessage.createGetPendingGames(user.getId()));
      } catch (IOException exception) {
        Log.error("Connection to server " + serverSocketAddress + " failed: " + exception);
      }
    }
  }

  /**
   * Refreshes the List pending Games and of local servers (pendingGamesList and gameServerAddressMap, respectively), by
   * receiving the Game list from the local servers .
   * @throws ClientViewException when server discovery fails
   */
  public void refreshPendingGamesList() throws ClientViewException {
    doRefreshPendingGamesList();
  }

  /**
   * Implementation of method reconnectGameClient().
   * @throws ClientViewException when the reconnection process fails
   */
  private void doReconnectGameClient() throws ClientViewException {
    // If the Game is hosted locally, resort to method connectLocalCommunicationClient().
    if (gameCommunicationClient.equals(localCommunicationClient)) {
      connectLocalCommunicationClient();
      return;
    }

    if (!gameServerAddressMap.containsKey(gamePlayedId)) {
      throw new ClientViewException("No recent Game server found for game " + gamePlayedId + " .");
    }
    final InetSocketAddress serverAddress = gameServerAddressMap.get(gamePlayedId);
    if (!gameCommunicationClientMutex.tryAcquire()) {
      Log.warn("gameCommunicationClientMutex already acquired. Game client reconnection process interrupted.");
      return;
    }
    try {
      Log.info("Starting Game client reconnection process to Game server " + serverAddress);
      gameCommunicationClient.close();
      if (!connectGameCommunicationClient(serverAddress)) {
        return;
      }
      gameCommunicationClient.sendTCP(ClientMessage.createReconnectPlayer(gamePlayedId, user.getId(),
          gameCommunicationClient.getRemoteAddressTCP().getHostString()));
    } finally {
      gameCommunicationClientMutex.release();
    }
  }

  /**
   * Attempts to reconnect the Game client by closing and reopening Connection to the current
   * Game server .
   * @throws ClientViewException when the reconnection process fails
   */
  public void reconnectGameClient() throws ClientViewException {
    doReconnectGameClient();
  }

  /**
   * Implementation of method discoverAndConnectGameServer().
   * @throws ClientViewException when the server discovery or reconnection process fails
   */
  private void doRediscoverAndConnectGameServer() throws ClientViewException {
    // If the Game is hosted locally, resort to method connectLocalCommunicationClient().
    if (gameCommunicationClient.equals(localCommunicationClient)) {
      connectLocalCommunicationClient();
      return;
    }

    final long gameServerRedicoveryStartTimeMillis = System.currentTimeMillis();
    synchronized (this) {
      if (gameServerRedicoveryStartTimeMillis - lastGameServerRedicoveryStartTimeMillis
          < GAME_SERVER_REDISCOVERY_INTERVAL_MILLIS) {
        Log.info("Last Game server rediscovery attempt less than " + GAME_SERVER_REDISCOVERY_INTERVAL_MILLIS
            + " milliseconds ago. Skipping this one.");
        return;
      }
      lastGameServerRedicoveryStartTimeMillis = gameServerRedicoveryStartTimeMillis;
      gameClientReconnectingInProgress = true;
    }
    Log.info("Game server rediscovery process initiated.");
    for (InetSocketAddress serverSocketAddress : discoverServerSocketAddresses()) {
      Client client = null;
      synchronized (this) {
        if (serverQueryClientMap.containsKey(serverSocketAddress)) {
          client = serverQueryClientMap.get(serverSocketAddress);
        }
      }
      if (client == null) {
        client = createDiscoveryClient();
        synchronized (this) {
          serverQueryClientMap.put(serverSocketAddress, client);
        }
      }
      final int tcpPort = serverSocketAddress.getPort();
      final int udpPort = KryoNetCommunicator.FIRST_UDP_PORT + (tcpPort - KryoNetCommunicator.FIRST_TCP_PORT);
      try {
        client.connect(KryoNetCommunicator.getConnectionTimeoutMillis(),
            serverSocketAddress.getHostName(), tcpPort, udpPort);
        client.sendTCP(ClientMessage.createGetMatchingGames(gamePlayedId, user.getId()));
      } catch (IOException exception) {
        Log.error("Connection to server " + serverSocketAddress + " failed: " + exception);
      }
    }
  }

  /**
   * Discovers available Game servers and initializes the reconnection process.
   * @throws ClientViewException when the server discovery or reconnection process fails
   */
  public void rediscoverAndConnectGameServer() throws ClientViewException {
    doRediscoverAndConnectGameServer();
  }

  /**
   * Requests pending Games in the available servers.
   * @throws ClientViewException when switching the State fails
   */
  public void getPendingGames() throws ClientViewException {
    synchronized (this) {
      if (!doGetState().equals(State.INITIAL_MENU)) {
        Log.warn("GameManager should be in State INITIAL_MENU (found " + doGetState() + ").");
        return;
      }
      pushState(State.GAMES_LISTED);
    }
    gameManagerEngine.onGameManagerStateChange(State.INITIAL_MENU);

    final Client client = setGameCommunicationClient(
        new Client(KryoNetCommunicator.KRYO_NET_BUFFER_SIZE, KryoNetCommunicator.KRYO_NET_BUFFER_SIZE));
    client.start();
    final Kryo kryo = client.getKryo();
    kryo.register(ClientMessage.class);
    kryo.register(ClientMessage.Type.class);
    kryo.register(ServerMessage.class);
    kryo.register(ServerMessage.Type.class);
    client.addListener(new Listener() {
      @Override
      public void received(Connection connection, Object object) {
        if (object instanceof ServerMessage) {
          final ServerMessage serverMessage = (ServerMessage) object;
          if (!manageServerMessageFiltering(serverMessage)) {
            return;
          }
          switch (serverMessage.getType()) {
            case REQUEST_ADD_PLAYER: {
              final UUID gameId = UUID.fromString(serverMessage.getBody().getString("gameId"));
              final UUID userId = UUID.fromString(serverMessage.getBody().getString("userId"));
              final int index = serverMessage.getBody().getInt("index");
              onRequestAddPlayer(gameId, userId, index);
              break;
            }
            case SEND_MESSAGE: {
              onSendMessage(serverMessage);
              break;
            }
            case JOIN_GAME_RESPONSE: {
              final UUID gameId = UUID.fromString(serverMessage.getBody().getString("gameId"));
              final UUID userId = UUID.fromString(serverMessage.getBody().getString("userId"));
              final boolean result = serverMessage.getBody().getBoolean("result");
              onJoinGameResponse(gameId, userId, result);
              break;
            }
            case LEAVE_GAME_INTENT_RESPONSE: {
              final UUID gameId = UUID.fromString(serverMessage.getBody().getString("gameId"));
              final UUID userId = UUID.fromString(serverMessage.getBody().getString("userId"));
              final boolean result = serverMessage.getBody().getBoolean("result");
              onLeaveGameIntentResponse(gameId, userId, result);
              break;
            }
            case CLIENT_MESSAGE_RESPONSE: {
              // Just ignore.
              break;
            }
            case CANCEL_GAME_INTENT_RESPONSE: {
              // Just ignore.
              break;
            }
            case START_GAME: {
              final UUID gameId = UUID.fromString(serverMessage.getBody().getString("gameId"));
              final UUID userId = UUID.fromString(serverMessage.getBody().getString("userId"));
              onJoinedGameStarted(gameId, userId);
              break;
            }
            case RECONNECT_PLAYER_RESPONSE: {
              final UUID gameId = UUID.fromString(serverMessage.getBody().getString("gameId"));
              final UUID userId = UUID.fromString(serverMessage.getBody().getString("userId"));
              final String hostString = serverMessage.getBody().getString("hostString");
              final boolean result = serverMessage.getBody().getBoolean("result");
              onReconnectPlayerResponse(gameId, userId, hostString, result);
              break;
            }
            case HEARTBEAT_RESPONSE: {
              final int heartbeatMessageId = serverMessage.getBody().getInt("id");
              final boolean gamePresent = serverMessage.getBody().getBoolean("gamePresent");
              final boolean gameUndecided = serverMessage.getBody().getBoolean("gameUndecided");
              final List<UUID> unresponsiveParticipants =
                  StreamSupport.stream(serverMessage.getBody().getJsonArray("unresponsiveParticipants"))
                  .map(value -> UUID.fromString(((JsonString) value).getString())).collect(Collectors.toList());
              final int lastMessageId = serverMessage.getBody().getInt("lastMessageId");
              onHeartbeatResponse(heartbeatMessageId, gamePresent, gameUndecided, unresponsiveParticipants,
                  lastMessageId);
              break;
            }
            case INITIALIZE_GAME_RESPONSE:
            case GET_PENDING_GAMES_RESPONSE:
            default:
              throw new IllegalArgumentException("Unexpected ServerMessage type: " + serverMessage.getType());
          }
        }
      }
    });

    refreshPendingGamesList();
  }

  /**
   * Handles responses to GET_PENDING_GAMES requests.
   * @param newPendingGames new pending Games to be added to the list
   * @param serverAddress the address of the Server hosting the Games
   */
  private void onGetPendingGamesResponse(@Nonnull List<GameInfo> newPendingGames,
      @Nonnull InetSocketAddress serverAddress) {
    synchronized (this) {
      if (!doGetState().equals(State.GAMES_LISTED)) {
        Log.warn("GameManager should be in State GAMES_LISTED (found " + doGetState() + ").");
        return;
      }
      for (GameInfo pendingGame : newPendingGames) {
        gameServerAddressMap.put(pendingGame.getId(), serverAddress);
      }
      final Set<UUID> pendingGamesIds = StreamSupport.stream(pendingGamesList)
          .map(gameInfo -> gameInfo.id).collect(Collectors.toCollection(() -> new HashSet<>()));
      for (GameInfo pendingGame : newPendingGames) {
        if (!pendingGamesIds.contains(pendingGame.id)) {
          pendingGamesList.add(pendingGame);
          pendingGamesIds.add(pendingGame.id);
        }
      }
    }
    gameManagerEngine.updatePendingGamesList();
  }

  /**
   * Connects gameCommunicationClient to serverSocketAddress .
   * @param serverSocketAddress the SocketAddress of the server to connect to
   * @return true iff the action finishes correctly
   */
  private boolean connectGameCommunicationClient(@Nonnull final InetSocketAddress serverSocketAddress) {
    final int tcpPort = serverSocketAddress.getPort();
    final int udpPort = KryoNetCommunicator.FIRST_UDP_PORT + (tcpPort - KryoNetCommunicator.FIRST_TCP_PORT);
    boolean connected = false;
    for (int i = 0; !connected && i < GAME_CONNECTION_RETRY_COUNT; ++i) {
      try {
        gameCommunicationClient.connect(KryoNetCommunicator.getConnectionTimeoutMillis(),
            serverSocketAddress.getHostName(), tcpPort, udpPort);
        connected = true;
      } catch (IOException exception) {
        Log.warn("Cannot connect to the Game Server " + serverSocketAddress, exception);
      }
    }
    if (!connected) {
      Log.error("Failed " + GAME_CONNECTION_RETRY_COUNT + " times to connect to "
          + serverSocketAddress.getHostName() + ":" + tcpPort + "/" + udpPort + " . Giving up.");
      return false;
    }
    return true;
  }

  /**
   * Handles responses to GET_MACHING_GAMES requests.
   * @param matchingGames list of matching Games
   * @param serverAddress the address of the Server hosting the Games
   */
  private void onGetMatchingGamesResponse(@Nonnull List<UUID> matchingGames, @Nonnull InetSocketAddress serverAddress) {
    synchronized (this) {
      if (!gameClientReconnectingInProgress) {
        return;
      }
    }
    boolean matchingGameFound = false;
    for (UUID gameId : matchingGames) {
      if (gameId.equals(gamePlayedId)) {
        matchingGameFound = true;
        break;
      }
    }
    if (!matchingGameFound) {
      return;
    }

    if (gameCommunicationClient == null) {
      Log.warn("gameCommunicationClient not set.");
      return;
    }
    if (gameCommunicationClient.equals(localCommunicationClient)) {
      Log.warn("The case of gameCommunicationClient equal to localCommunicationClient should not be handled here.");
      return;
    }
    if (!gameCommunicationClientMutex.tryAcquire()) {
      Log.warn("gameCommunicationClientMutex already acquired. Reconnecting to a matching Game server interrupted.");
      return;
    }
    try {
      gameCommunicationClient.close();
      if (!connectGameCommunicationClient(serverAddress)) {
        return;
      }

      synchronized (this) {
        // First-Come-First-Served: looking for servers is finished.
        gameClientReconnectingInProgress = false;
      }
      gameCommunicationClient.sendTCP(ClientMessage.createReconnectPlayer(gamePlayedId, user.getId(),
          gameCommunicationClient.getRemoteAddressTCP().getHostString()));
    } finally {
      gameCommunicationClientMutex.release();
    }
  }

  /**
   * If there is no heartbeatThread, starts a new one.
   */
  private synchronized void tryStartHeartbeatThread() {
    if (heartbeatThread == null) {
      lastHeartbeatMessageSentId = -1;
      heartbeatThread = new HeartbeatThread();
      lastConfirmativeHeartbeatResponseTimeMillis = System.currentTimeMillis();
      isUserConnectionUp = true;
      (new Thread(heartbeatThread)).start();
      Log.info("Heartbeat thread started.");
    }
  }

  /**
   * If there is already a heartbeatThread, tries to stop it.
   */
  private void tryStopHeartbeatThread() {
    if (heartbeatThread != null) {
      final HeartbeatThread currentHeartbeatThread;
      synchronized (heartbeatThread) {
        heartbeatThread.setRunning(false);
        currentHeartbeatThread = heartbeatThread;
        heartbeatThread = null;
        isUserConnectionUp = true;
        Log.info("Heartbeat thread stopped.");
      }
      synchronized (currentHeartbeatThread) {
        currentHeartbeatThread.notify();
      }
    }
  }

  /**
   * Requests joining a Game from pendingGamesList .
   * @param index index in pendingGamesList of the Game to join
   * @throws ClientViewException when joining the Game fails
   */
  public void joinPendingGame(@Nonnull int index) throws ClientViewException {
    final InetSocketAddress serverSocketAddress;
    synchronized (this) {
      if (!doGetState().equals(State.GAMES_LISTED)) {
        Log.warn("GameManager should be in State GAMES_LISTED (found " + doGetState() + ").");
        return;
      }
      if (pendingGamesList.size() <= index) {
        throw new ClientViewException("Index " + index + " out of bound (" + pendingGamesList.size()
            + " Games available).");
      }
      final GameInfo gameInfo = pendingGamesList.get(index);
      gamePlayedId = gameInfo.getId();
      lastMessageId = 0;
      lastConfirmativeHeartbeatResponseTimeMillis = System.currentTimeMillis();
      isUserConnectionUp = true;
      serverSocketAddress = gameServerAddressMap.get(gamePlayedId);
    }
    boolean gameCommunicationClientMutexAcquired = false;
    try {
      gameCommunicationClientMutex.acquire();
      gameCommunicationClientMutexAcquired = true;
      if (!connectGameCommunicationClient(serverSocketAddress)) {
        return;
      }
      if (!initializeChannels()) {
        return;
      }

      synchronized (this) {
        otherGameClientsResponsive = true;
        pushState(State.GAME_JOINED);
      }
      checkActionFinished(gamePlayedId, Action.JOIN_GAME);
      gameCommunicationClient.sendTCP(ClientMessage.createJoinGame(gamePlayedId, user.getId(), user.getName()));
    } catch (InterruptedException exception) {
      Log.error("Waiting for gameCommunicationClientMutex interrupted while trying to joing a Game .");
      return;
    } finally {
      if (gameCommunicationClientMutexAcquired) {
        gameCommunicationClientMutex.release();
      }
    }
  }

  /**
   * Handles responses to JOIN_GAME requests.
   * @param gameId the id of the Game to join
   * @param userId the id of the User to join the Game
   * @param result whether the joining process succeeded or failed
   */
  private void onJoinGameResponse(@Nonnull UUID gameId, @Nonnull UUID userId, boolean result) {
    confirmActionFinished(gameId, Action.JOIN_GAME);
    synchronized (this) {
      if (!doGetState().equals(State.GAME_JOINED)) {
        Log.warn("GameManager should be in State GAME_JOINED (found " + doGetState() + ").");
        return;
      }
      if (!gameId.equals(gamePlayedId)) {
        Log.warn("Unrecognized Game id " + gameId + " (expected " + gamePlayedId + " .");
        popState();
        return;
      }
      if (!userId.equals(user.getId())) {
        Log.warn("Unrecognized User id " + userId + " (expected " + user.getId() + " .");
        popState();
        return;
      }
    }
    if (result) {
      try {
        gameManagerEngine.onGameManagerStateChange(State.GAMES_LISTED);
      } catch (ClientViewException exception) {
        synchronized (this) {
          popState();
        }
        throw new RuntimeException("Visualizing the State change failed.", exception);
      }
      tryStopHeartbeatThread();
      tryStartHeartbeatThread();
    } else {
      final State oldState;
      synchronized (this) {
        oldState = popState();
      }
      tryStopHeartbeatThread();
      try {
        // NOTE: The message received has two possible meanings: either the joining process fails, or the Game once
        // joined has been cancelled. gameManagerEngine must handle both cases.
        gameManagerEngine.onGameManagerStateChange(oldState);
      } catch (ClientViewException exception) {
        throw new RuntimeException("Visualizing the State change failed.", exception);
      }
      synchronized (this) {
        final int oldOverdueGameMessageQueueSize;
        synchronized (this) {
          oldOverdueGameMessageQueueSize = overdueGameMessageQueue.size();
        }
        overdueGameMessageQueue.clear();
        unconfirmedMessageQueue.clear();
        reconnectionNeeded = false;
        handleOverdueGameMessageQueue(oldOverdueGameMessageQueueSize);
        pendingGamesList.clear();
      }
      gameManagerEngine.updatePendingGamesList();
    }
  }

  /**
   * Handles responses to RECONNECT_PLAYER requests.
   * @param gameId the Game the User tries to reconnect to
   * @param userId the User trying to reconnect to the Game
   * @param hostString the hostString of the server the User tries to connect to
   * @param result the result of the reconnection process
   */
  private void onReconnectPlayerResponse(@Nonnull UUID gameId, @Nonnull UUID userId, @Nonnull String hostString,
      boolean result) {
    if (result) {
      synchronized (this) {
        gameServerAddressMap.put(gameId, gameCommunicationClient.getRemoteAddressTCP());
      }
      if (!sendOverdueMessages()) {
        return;
      }
      // TODO(najtmar): Leave the recovery mode.
    }
  }

  /**
   * Handles responses to HEARTBEAT messages.
   * @param heartbeatMessageId the id of the HEARTBEAT messages responded to
   * @param gamePresent whether the Game queried is present on the Game server
   * @param gameUndecided whether the Game is finished and its State is UNDECIDED
   * @param unresponsiveParticipants list of ids of participants of the Game queried that may be unresponsive
   * @param lastClientMessageId the last ClientMessage confirmed to be received by the server
   */
  private void onHeartbeatResponse(int heartbeatMessageId, boolean gamePresent, boolean gameUndecided,
      @Nonnull List<UUID> unresponsiveParticipants, @Nonnegative int lastClientMessageId) {
    if (heartbeatMessageId < lastHeartbeatMessageSentId) {
      Log.info("Heartbeat message id " + heartbeatMessageId + " smaller than the last heartbeat message sent id: "
          + lastHeartbeatMessageSentId + " . Ignoring.");
      return;
    }
    if (gamePresent && !gameUndecided) {
      synchronized (this) {
        lastConfirmativeHeartbeatResponseTimeMillis = System.currentTimeMillis();
      }
      handleHeartbeatResponseDelay();
    }
    if (gameUndecided) {
      Log.info("Game in State UNDECIDED . Notifying the Client and quitting.");
      tryStopHeartbeatThread();
      try {
        gameClient.notifyGameUndecided();
      } catch (ClientViewException exception) {
        throw new RuntimeException(exception);
      }
      return;
    }
    final boolean wasOtherGameClientsResponsive;
    final boolean isOtherGameClientsResponsive;
    synchronized (this) {
      lastHeartbeatMessageConfirmedId = heartbeatMessageId;
      Log.debug("HEARTBEAT message confirmed. heartbeatMessageId: " + heartbeatMessageId + ", gamePresent: "
          + gamePresent + ", unresponsiveParticipants: " + unresponsiveParticipants);
      wasOtherGameClientsResponsive = otherGameClientsResponsive;
      otherGameClientsResponsive = unresponsiveParticipants.isEmpty();
      isOtherGameClientsResponsive = otherGameClientsResponsive;
    }
    if (wasOtherGameClientsResponsive != isOtherGameClientsResponsive) {
      try {
        gameClient.notifyOthersGameServerConnection(isOtherGameClientsResponsive);
      } catch (ClientViewException exception) {
        throw new RuntimeException(exception);
      }
    }

    // Flush and clear unconfirmedMessageQueue .
    final Queue<ClientMessage> currentUnconfirmedMessageQueue;
    synchronized (this) {
      while (!unconfirmedMessageQueue.isEmpty() && unconfirmedMessageQueue.peek().getId() <= lastClientMessageId) {
        unconfirmedMessageQueue.remove();
      }
      currentUnconfirmedMessageQueue = new LinkedList<>(unconfirmedMessageQueue);
    }
    if (gameCommunicationClientMutex.tryAcquire()) {
      try {
        for (ClientMessage unconfirmedMessage : currentUnconfirmedMessageQueue) {
          int bytesSent = gameCommunicationClient.sendTCP(unconfirmedMessage);
          if (bytesSent == 0) {
            Log.warn("Sending ClientMessage from unconfirmedMessageQueue failed: " + unconfirmedMessage);
            return;
          }
        }
      } finally {
        gameCommunicationClientMutex.release();
      }
    }
  }

  /**
   * Requests leaving a Game intent already joined .
   * @throws ClientViewException when leaving the Game intent fails
   */
  public void leaveGameIntent() throws ClientViewException {
    synchronized (this) {
      if (!doGetState().equals(State.GAME_JOINED)) {
        Log.warn("GameManager should be in State GAME_JOINED (found " + doGetState() + ").");
        return;
      }
    }
    boolean gameCommunicationClientMutexAcquired = false;
    try {
      gameCommunicationClientMutex.acquire();
      gameCommunicationClientMutexAcquired = true;
      checkActionFinished(gamePlayedId, Action.LEAVE_GAME_INTENT);
      gameCommunicationClient.sendTCP(ClientMessage.createLeaveGameIntent(gamePlayedId, user.getId()));
    } catch (InterruptedException exception) {
      Log.error("Waiting for gameCommunicationClientMutex interrupted while trying to leave a Game intent.");
    } finally {
      if (gameCommunicationClientMutexAcquired) {
        gameCommunicationClientMutex.release();
      }
    }
  }

  /**
   * Leaves State GAMES_LISTED .
   * @throws ClientViewException when changing the State fails
   */
  public void leaveGamesListed() throws ClientViewException {
    synchronized (this) {
      if (!doGetState().equals(State.GAMES_LISTED)) {
        Log.warn("GameManager should be in State GAMES_LISTED (found " + doGetState() + ").");
        return;
      }
      popState();
    }
    gameManagerEngine.onGameManagerStateChange(State.GAMES_LISTED);
  }

  /**
   * Handles responses to LEAVE_GAME_INTENT requests.
   * @param gameId the id of the Game intent to leave
   * @param userId the id of the User to leave the Game intent
   * @param result whether the leaving process succeeded or failed
   */
  private void onLeaveGameIntentResponse(@Nonnull UUID gameId, @Nonnull UUID userId, boolean result) {
    confirmActionFinished(gameId, Action.LEAVE_GAME_INTENT);
    synchronized (this) {
      if (!doGetState().equals(State.GAME_JOINED)) {
        Log.warn("GameManager should be in State GAME_JOINED (found " + doGetState() + ").");
        return;
      }
      if (!gameId.equals(gamePlayedId)) {
        Log.warn("Unrecognized Game id " + gameId + " (expected " + gamePlayedId + " .");
        return;
      }
      if (!userId.equals(user.getId())) {
        Log.warn("Unrecognized User id " + userId + " (expected " + user.getId() + " .");
        return;
      }
    }
    if (result) {
      try {
        synchronized (this) {
          popState();
        }
        gameManagerEngine.onGameManagerStateChange(State.GAME_JOINED);
        gameManagerEngine.updatePendingGamesList();
        tryStopHeartbeatThread();
      } catch (ClientViewException exception) {
        synchronized (this) {
          popState();
        }
        throw new RuntimeException("Visualizing the State change failed.", exception);
      }
    } else {
      Log.warn("Leaving Game " + gameId + " rejected.");
    }
  }

  /**
   * Notifies the Game Server, that the Game has been force quit and should be finished. Works in a best-effort way:
   * if the notification is not delivered, it's not repeated.
   */
  public void notifyForceQuit() {
    synchronized (this) {
      if (!doGetState().equals(State.OWN_GAME_STARTED) && !doGetState().equals(State.JOINED_GAME_STARTED)) {
        Log.warn("GameManager should be in States OWN_GAME_STARTED or JOINED_GAME_STARTED (found " + doGetState()
            + ").");
        return;
      }
    }
    gameCommunicationClient.sendTCP(ClientMessage.createForceQuitGame(gamePlayedId, user.getId()));
  }

  /**
   * Resets the GameManager .
   * @param resetClient whether gameClient should also be reset
   * @throws ClientViewException when resetting the GameManager fails
   */
  public void reset(boolean resetClient) throws ClientViewException {
    final State oldState;
    final State newState;
    synchronized (this) {
      oldState = doGetState();
      if (doGetState() == State.NOT_INITIALIZED) {
        Log.warn("GameManager should not be in State NOT_INITIALIZED .");
        return;
      }
      while (!doGetState().equals(State.INITIAL_MENU)) {
        popState();
      }
      newState = doGetState();
    }
    gameManagerEngine.initializeGameManagementEngine(this);
    if (oldState != newState) {
      gameManagerEngine.onGameManagerStateChange(oldState);
    }
    if (gameClient != null && resetClient) {
      gameClient.reviseState();
    }
    tryStopHeartbeatThread();
  }

  public UserView getUser() {
    return user;
  }

  public synchronized List<GameInfo> getPendingGamesList() {
    return pendingGamesList;
  }

  KryoNetCommunicator getServer() {
    return communicationServer;
  }

  Client getLocalClient() {
    return localCommunicationClient;
  }

  Client getGameCommunicationClient() {
    return gameCommunicationClient;
  }

  Map<UUID, Map<Action, Long>> getGameActionToConfirmMap() {
    return gameActionToConfirmMap;
  }

  public cz.wie.najtmar.gop.client.ui.Client getGameClient() {
    return gameClient;
  }

  GameManagementEngine getGameManagerEngine() {
    return gameManagerEngine;
  }

  GameSession getGameSession() {
    return gameSession;
  }

  int getLastMessageId() {
    return lastMessageId;
  }

  Map<UUID, Integer> getGameLastAssignedMessageIdMap() {
    return gameLastAssignedMessageIdMap;
  }

  Queue<ClientMessage> getUnconfirmedMessageQueue() {
    return unconfirmedMessageQueue;
  }

  void injectOnlyAllowLocalServers(boolean onlyAllowLocalServers) {
    this.onlyAllowLocalServers = onlyAllowLocalServers;
  }

  void injectLastGameServerRedicoveryStartTimeMillis(long lastGameServerRedicoveryStartTimeMillis) {
    this.lastGameServerRedicoveryStartTimeMillis = lastGameServerRedicoveryStartTimeMillis;
  }

}
