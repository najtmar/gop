package cz.wie.najtmar.gop.client.ui;

import javax.annotation.Nonnull;

/**
 * A platform-dependent engine that is created by GameManager .
 * @author najtmar
 */
public interface GameManagerManagedEngine {

  /**
   * Sets the GameManager instance.
   * @param gameManager the GameManager instance to be set
   */
  public void setGameManager(@Nonnull GameManager gameManager);

}
