package cz.wie.najtmar.gop.gdx;

import android.os.Bundle;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import cz.wie.najtmar.gop.DefaultPropertiesReader;
import cz.wie.najtmar.gop.game.PropertiesReader;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		PropertiesReader propertiesReader = new DefaultPropertiesReader();

		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		initialize(new GopGdx(propertiesReader), config);
	}
}
