package cz.wie.najtmar.gop.gdx.actors;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import cz.wie.najtmar.gop.board.DirectedPosition;
import cz.wie.najtmar.gop.board.Position;
import cz.wie.najtmar.gop.board.RoadSection;
import cz.wie.najtmar.gop.game.Itinerary;
import cz.wie.najtmar.gop.gdx.Location;

public class ItinerarySectionActor extends Image {

    private final float x0;
    private final float y0;
    private final float x1;
    private final float y1;

    public ItinerarySectionActor(Itinerary itinerary, int index) {
        super(Util.createTexture("itinerary.png"));

        DirectedPosition[] nodes = itinerary.getNodes();
        RoadSection roadSection = nodes[index].getPosition().getRoadSection();
        RoadSection.Direction direction = nodes[index].getDirection();
        Position startPosition;
        Position endPosition;
        if (index == 0) {
            startPosition = itinerary.getCurrentPosition().getPosition();
        } else {
            if (direction == RoadSection.Direction.FORWARD) {
                startPosition = new Position(roadSection, 0);
            } else {
                startPosition = new Position(roadSection, roadSection.getPositionsCount() - 1);
            }
        }
        if (index == nodes.length - 2) {
            endPosition = nodes[nodes.length - 1].getPosition();
        } else {
            if (direction == RoadSection.Direction.FORWARD) {
                endPosition = new Position(roadSection, roadSection.getPositionsCount() - 1);
            } else {
                endPosition = new Position(roadSection, 0);
            }
        }

        this.x0 = Location.getX(startPosition);
        this.y0 = Location.getY(startPosition);
        this.x1 = Location.getX(endPosition);
        this.y1 = Location.getY(endPosition);

        float deltaX = x1 - x0;
        float deltaY = y1 - y0;
        float angleRad = (float) Math.atan2(deltaY, deltaX);
        float angle = angleRad * MathUtils.radiansToDegrees;

        this.setOriginY(this.getPrefHeight() / 2);
        this.setRotation(angle);
        this.setScaleX((float) (Math.sqrt(deltaX * deltaX + deltaY * deltaY) / this.getPrefWidth()));
        this.setX(x0);
        this.setY(y0, Align.center);

        // this.addListener(new ItinerarySectionListener());
    }

}
