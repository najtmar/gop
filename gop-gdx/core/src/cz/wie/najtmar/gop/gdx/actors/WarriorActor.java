package cz.wie.najtmar.gop.gdx.actors;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import cz.wie.najtmar.gop.board.Position;
import cz.wie.najtmar.gop.game.Warrior;
import cz.wie.najtmar.gop.gdx.Location;

public class WarriorActor extends Group {

    private static final float SCALE = 0.3f;

    private final Image image;

    private final float x, xImageDelta;
    private final float y, yImageDelta;

    public WarriorActor(Warrior warrior) {
        super();

        Position position = warrior.getCurrentPosition();
        double angleRad = warrior.getWarriorDirection();
        float angle = (float) (angleRad * MathUtils.radiansToDegrees);

        // NOTE: Image height is the actual image size.
        this.image = new Image(Util.createTexture("warrior-white.png"));

        this.x = Location.getX(position);
        this.xImageDelta = this.image.getHeight() / 2.0f;
        this.y = Location.getY(position);
        this.yImageDelta = this.image.getHeight() / 2.0f;

        this.image.setOrigin(image.getPrefHeight() / 2.0f, image.getPrefHeight() / 2.0f);
        this.image.setRotation(angle);

        this.image.setX(x - xImageDelta, Align.left);
        this.image.setY(y - yImageDelta, Align.bottom);

        this.image.setScale(SCALE);

        this.addActor(this.image);
    }

}
