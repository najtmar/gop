package cz.wie.najtmar.gop.gdx;

import com.badlogic.gdx.graphics.Texture;
import cz.wie.najtmar.gop.board.Joint;
import cz.wie.najtmar.gop.board.Position;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Location {

    public static float getX(Position position) {
        Joint[] joints = position.getRoadSection().getJoints();
        float beginning = (float) joints[0].getCoordinatesPair().getX();
        float end = (float) joints[1].getCoordinatesPair().getX();
        int index = position.getIndex();
        int size = position.getRoadSection().getPositionsCount();
        return beginning + ((float) index / size) * (end - beginning);
    }

    public static float getY(Position position) {
        Joint[] joints = position.getRoadSection().getJoints();
        float beginning = (float) joints[0].getCoordinatesPair().getY();
        float end = (float) joints[1].getCoordinatesPair().getY();
        int index = position.getIndex();
        int size = position.getRoadSection().getPositionsCount();
        return beginning + ((float) index / size) * (end - beginning);
    }
}
