package cz.wie.najtmar.gop.gdx.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import cz.wie.najtmar.gop.board.*;
import cz.wie.najtmar.gop.game.*;
import cz.wie.najtmar.gop.gdx.GdxGameOrchestrator;
import cz.wie.najtmar.gop.gdx.GameStateHolder;
import cz.wie.najtmar.gop.gdx.GopGdx;
import cz.wie.najtmar.gop.gdx.listeners.GameListener;
import cz.wie.najtmar.gop.gdx.stages.CityStage;
import cz.wie.najtmar.gop.gdx.stages.MapStage;
import cz.wie.najtmar.gop.gdx.stages.PrawnStage;
import cz.wie.najtmar.gop.gdx.stages.RoadStage;
import lombok.Getter;
import lombok.SneakyThrows;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class GameScreen implements Screen, GameStateHolder {

    @Getter
    private volatile GameStateHolder.GameState state = GameStateHolder.GameState.DEFAULT;
    Image map;
    final private Stage mapStage, roadStage, cityStage, prawnStage;
    final private GdxGameOrchestrator gameOrchestrator;
    private final List<OrthographicCamera> cameras = new LinkedList<>();
    private InputMultiplexer multiplexer;

    private final cz.wie.najtmar.gop.game.Game game;
    private final Board board;

    @SneakyThrows
    public GameScreen(Game gdxGame, cz.wie.najtmar.gop.game.Game game/*PropertiesReader propertiesReader*/) {
        mapStage = new MapStage(new ScreenViewport(), this);
        roadStage = new RoadStage(new ScreenViewport(), this);
        cityStage = new CityStage(new ScreenViewport(), this);
        prawnStage = new PrawnStage(new ScreenViewport(), this);
        gameOrchestrator = new GdxGameOrchestrator(roadStage, cityStage, prawnStage);

        multiplexer = new InputMultiplexer();

        ///
        this.game = game;  // testGame(propertiesReader);
        this.board = this.game.getBoard();
        //
        map = new Image(new Texture("poland.jpg"));

        List.of(prawnStage, cityStage, roadStage, mapStage)
                .forEach(stage -> {
                    multiplexer.addProcessor(stage);

                    OrthographicCamera camera = (OrthographicCamera) stage.getViewport().getCamera();
                    camera.translate(map.getImageWidth() / 2, map.getImageHeight() / 2);
                    camera.zoom = GopGdx.MAX_SCALE;
                    cameras.add(camera);
                });

        map.addListener(new GameListener(cameras));

        mapStage.addActor(map);
        for (RoadSection roadSection : this.board.getRoadSections()) {
            gameOrchestrator.addRoadSection(roadSection);
        }
        for (Joint joint : this.board.getJoints()) {
            gameOrchestrator.addCity(new City.Builder()
                    .setGame(game)
                    .setJoint(joint)
                    .setName(joint.getName())
                    .setPlayer(game.getPlayers().get(0))
                    .build());
        }
        for (Prawn prawn : this.game.getPrawns()) {
            gameOrchestrator.updateItinerary(prawn);
            if (prawn instanceof Warrior)
                gameOrchestrator.addPrawn(prawn);
        }
    }

    @Override
    public void show() {
        Gdx.app.log("GameScreen", "show");
        Gdx.input.setInputProcessor(multiplexer);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        List.of(mapStage, roadStage, cityStage, prawnStage)
                .forEach(stage -> {
                    stage.act();
                    stage.draw();
                });
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
    }

    ///
    @SneakyThrows
    private cz.wie.najtmar.gop.game.Game testGame(PropertiesReader propertiesReader) throws IOException {
        final long CREATION_TIME = 476;

        final cz.wie.najtmar.gop.game.Game newGame = new cz.wie.najtmar.gop.game.Game(propertiesReader.getGameProperties());
        final Player PLAYER_0 = new Player(new User("playerA", new UUID(12345, 67890)), 0, newGame);
        final Player PLAYER_1 = new Player(new User("playerB", new UUID(98765, 43210)), 1, newGame);
        newGame.addPlayer(PLAYER_0);
        newGame.addPlayer(PLAYER_1);

        Board newBoard;
        Warrior warrior0, warrior1, warrior2;

        MapBoardGenerator mapBoardGenerator = new MapBoardGenerator(propertiesReader);
        newBoard = mapBoardGenerator.generate();
        newGame.setBoard(newBoard);

        warrior0 = (new Warrior.Builder())
                .setCreationTime(CREATION_TIME)
                .setCurrentPosition(new Position(newBoard.getRoadSections().get(0), 0))
                .setDirection(0.0)
                .setGameProperties(new GameProperties(propertiesReader.getGameProperties()))
                .setPlayer(PLAYER_0)
                .build();
        warrior0.setItinerary((new Itinerary.Builder())
                .addPosition(new Position(newBoard.getRoadSections().get(0), 0))
                .addPosition(new Position(newBoard.getRoadSections().get(0), newBoard.getRoadSections().get(0).getPositionsCount() / 2))
                .build());
        move(warrior0, newBoard, 0);
        warrior1 = (new Warrior.Builder())
                .setCreationTime(CREATION_TIME)
                .setCurrentPosition(new Position(newBoard.getRoadSections().get(1), newBoard.getRoadSections().get(1).getPositionsCount() - 1))
                .setDirection(2.0 * Math.PI - 0.001)
                .setGameProperties(new GameProperties(propertiesReader.getGameProperties()))
                .setPlayer(PLAYER_1)
                .build();
        warrior1.setItinerary((new Itinerary.Builder())
                .addPosition(new Position(newBoard.getRoadSections().get(1), newBoard.getRoadSections().get(1).getPositionsCount() - 1))
                .addPosition(new Position(newBoard.getRoadSections().get(1), newBoard.getRoadSections().get(1).getPositionsCount() / 2))
                .build());
        moveBack(warrior1, newBoard, 1);
        warrior2 = (new Warrior.Builder())
                .setCreationTime(CREATION_TIME)
                .setCurrentPosition(new Position(newBoard.getRoadSections().get(2), 0))
                .setDirection(3.0 * Math.PI / 4.0)
                .setGameProperties(new GameProperties(propertiesReader.getGameProperties()))
                .setPlayer(PLAYER_0)
                .build();
        warrior2.setItinerary((new Itinerary.Builder())
                .addPosition(new Position(newBoard.getRoadSections().get(2), 0))
                .addPosition(new Position(newBoard.getRoadSections().get(2), newBoard.getRoadSections().get(2).getPositionsCount() / 2))
                .build());
        move(warrior2, newBoard, 2);

        newGame.addPrawn(warrior0);
        newGame.addPrawn(warrior1);
        newGame.addPrawn(warrior2);

        return newGame;
    }

    private void move(Warrior warrior, Board board, int index) {
        final long MOVE_TIME = 525;
        for (int pos = 1; pos <= board.getRoadSections().get(index).getPositionsCount() / 4; ++pos) {
            warrior.move(new Move(new Position(board.getRoadSections().get(index), pos), MOVE_TIME, pos == board.getRoadSections().get(index).getPositionsCount() / 2), MOVE_TIME);
        }
    }

    private void moveBack(Warrior warrior, Board board, int index) {
        final long MOVE_TIME = 525;
        for (int pos = board.getRoadSections().get(index).getPositionsCount() - 2; pos >= 3 * board.getRoadSections().get(index).getPositionsCount() / 4; --pos) {
            warrior.move(new Move(new Position(board.getRoadSections().get(index), pos), MOVE_TIME, pos == board.getRoadSections().get(index).getPositionsCount() / 2), MOVE_TIME);
        }
    }
    //

}
