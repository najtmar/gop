package cz.wie.najtmar.gop.gdx.actors;

import com.badlogic.gdx.graphics.Texture;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Util {

    static Texture createTexture(String fileName) {
        Texture texture = new Texture(fileName);
        texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        return texture;
    }
}
