package cz.wie.najtmar.gop.gdx.listeners;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import cz.wie.najtmar.gop.gdx.GopGdx;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class GameListener extends ActorGestureListener {

    private final List<OrthographicCamera> cameras;

    @Override
    public void pan(InputEvent event, float x, float y, float deltaX, float deltaY) {
        super.pan(event, x, y, deltaX, deltaY);
        cameras.forEach(camera -> {
            camera.position.x -= (deltaX * Gdx.graphics.getDensity());
            camera.position.y -= (deltaY * Gdx.graphics.getDensity());
        });
    }

    @Override
    public void tap (InputEvent event, float x, float y, int count, int button) {
        super.tap(event, x, y, count, button);
        cameras.forEach(camera -> {
            if (count == 2) {
                if (camera.zoom == GopGdx.MAX_SCALE) {
                    camera.zoom = GopGdx.MIN_SCALE;
                } else {
                    camera.zoom = GopGdx.MAX_SCALE;
                }
                camera.position.x = x;
                camera.position.y = y;
            }
        });
    }
}
