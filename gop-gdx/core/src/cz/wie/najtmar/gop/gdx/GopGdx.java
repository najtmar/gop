package cz.wie.najtmar.gop.gdx;

import com.badlogic.gdx.Game;
import com.google.common.base.Preconditions;
import cz.wie.najtmar.gop.game.PropertiesReader;
import cz.wie.najtmar.gop.gdx.screens.GameScreen;
import cz.wie.najtmar.gop.gdx.screens.InitScreen;
import cz.wie.najtmar.gop.gdx.screens.JoinScreen;
import cz.wie.najtmar.gop.gdx.screens.StartScreen;
import cz.wie.najtmar.gop.server.Client;
import cz.wie.najtmar.gop.server.Server;
import cz.wie.najtmar.gop.server.gop.GoPGameSession;
import cz.wie.najtmar.gop.server.jm.JmNetworkServer;
import io.vertx.core.Vertx;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

import java.util.Random;
import java.util.UUID;

@RequiredArgsConstructor
public class GopGdx extends Game {
    public static float MAX_SCALE = 1.0f;
    public static float MIN_SCALE = 0.5f;

    private final long randomSeed = new Random().nextLong();

    private final PropertiesReader propertiesReader;

    private final Vertx vertx = Vertx.vertx();
    private Server server;

    private StartScreen startScreen;
    private InitScreen initScreen;
    private JoinScreen joinScreen;
    private GameScreen gameScreen;

    public void setInitScreen() {
        server.initialize();
        server.createGame();
        this.setScreen(initScreen);
    }

    public void unsetInitScreen() {
        stopServer();
        this.setScreen(startScreen);
    }

    public void setJoinScreen() {
        this.setScreen(joinScreen);
    }

    public void unsetJoinScreen() {
        this.setScreen(startScreen);
    }

    public void setGameScreen() {
        ///
        Client client1 = new Client("Client 1", UUID.randomUUID());
        Client client2 = new Client("Client 2", UUID.randomUUID());
        server.startUserRegistration();
        server.register(client1);
        server.register(client2);
        //

        gameScreen = new GameScreen(this, Preconditions.checkNotNull(server.tryStartGame()));
        this.setScreen(gameScreen);
    }

    private void stopServer() {
        while (server != null && server.getState() != Server.State.STOPPED) {
            switch (server.getState()) {
                case GAME_ACTIVE:
                case GAME_FINISHED:
                case GAME_STARTED:
                case GAME_PAUSED:
                    server.preemptGame();
                    break;
                case GAME_CREATED:
                case GAME_INITIALIZING:
                    server.abandonGame();
                    break;
                case INITIALIZED:
                    server.stop();
                    break;
            }
        }
    }

    @SneakyThrows
    @Override
    public void create () {
        server = new Server(new JmNetworkServer(), new GoPGameSession(randomSeed), vertx.eventBus());
        startScreen = new StartScreen(this);
        initScreen = new InitScreen(this);
        joinScreen = new JoinScreen(this);
        this.setScreen(startScreen);
    }

    @Override
    public void render () {
        super.render();
    }
}
