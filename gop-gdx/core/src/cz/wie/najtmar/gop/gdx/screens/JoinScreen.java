package cz.wie.najtmar.gop.gdx.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import cz.wie.najtmar.gop.client.NetworkClient;
import cz.wie.najtmar.gop.client.jm.JmNetworkClient;
import cz.wie.najtmar.gop.gdx.GopGdx;
import cz.wie.najtmar.gop.client.Client;

import java.util.UUID;

public class JoinScreen implements Screen {

    private final Image background;

    private final Stage stage;
    private final GopGdx game;

    private final NetworkClient networkClient;
    private final Client client;

    public JoinScreen(GopGdx game) {
        this.stage = new Stage(new ScreenViewport());
        this.game = game;

        this.networkClient = new JmNetworkClient();
        this.client = new Client(networkClient);
        this.client.initialize();

        Positioner positioner = new Positioner(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), -0.1f);

        background = new Image(new Texture("gop_background.jpg"));
        stage.addActor(background);

        Skin skin = new Skin(Gdx.files.internal("skin/glassy-ui.json"));

        Button refreshButton = new TextButton("Refresh", skin,"default");
        Positioner.Position startButtonPosition = positioner.getCenteredPosition(1);
        refreshButton.setSize(startButtonPosition.getWidth(), startButtonPosition.getHeight());
        refreshButton.setPosition(startButtonPosition.getX(),startButtonPosition.getY());
        refreshButton.addListener(new InputListener() {

            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                // TODO: Implement this.
            }

            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

        });

        Button cancelButton = new TextButton("Cancel", skin,"default");
        Positioner.Position cancelButtonPosition = positioner.getCenteredPosition(2);
        cancelButton.setSize(cancelButtonPosition.getWidth(), cancelButtonPosition.getHeight());
        cancelButton.setPosition(cancelButtonPosition.getX(), cancelButtonPosition.getY());
        cancelButton.addListener(new InputListener() {

            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                JoinScreen.this.game.unsetJoinScreen();
                client.stopSearching();
            }

            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

        });

        stage.addActor(refreshButton);
        stage.addActor(cancelButton);

        Positioner.Position serverListPosition = positioner.getBottomPosition(-3, 4.5f);


        ///
        List<String> list = new List<>(skin, "big");
        list.setItems("Dupa", "Jasio", "pierdzi", "Stasio", "Dupa", "Jasio", "pierdzi", "Stasio");
        ScrollPane scrollPane = new ScrollPane(list);
        scrollPane.setPosition(serverListPosition.getX(), serverListPosition.getY());
        scrollPane.setSize(serverListPosition.getWidth(), serverListPosition.getHeight());
        stage.addActor(scrollPane);
        //


    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
        client.startSearching();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
