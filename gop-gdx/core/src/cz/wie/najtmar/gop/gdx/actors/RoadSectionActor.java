package cz.wie.najtmar.gop.gdx.actors;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import cz.wie.najtmar.gop.board.RoadSection;
import cz.wie.najtmar.gop.common.Point2D;
import cz.wie.najtmar.gop.gdx.listeners.RoadSectionListener;

public class RoadSectionActor extends Image {

    private final float x0;
    private final float y0;
    private final float x1;
    private final float y1;

    public RoadSectionActor(RoadSection roadSection) {
        super(Util.createTexture("road-section.png"));

        Point2D.Double joint0Position = roadSection.getJoints()[0].getCoordinatesPair();
        Point2D.Double joint1Position = roadSection.getJoints()[1].getCoordinatesPair();
        this.x0 = (float) joint0Position.getX();
        this.y0 = (float) joint0Position.getY();
        this.x1 = (float) joint1Position.getX();
        this.y1 = (float) joint1Position.getY();

        float deltaX = x1 - x0;
        float deltaY = y1 - y0;
        float angleRad = (float) Math.atan2(deltaY, deltaX);
        float angle = angleRad * MathUtils.radiansToDegrees;

        this.setOriginY(this.getPrefHeight() / 2);
        this.setRotation(angle);
        this.setScaleX((float) (Math.sqrt(deltaX * deltaX + deltaY * deltaY) / this.getPrefWidth()));
        this.setX(x0);
        this.setY(y0, Align.center);

        this.addListener(new RoadSectionListener());
    }

}
