package cz.wie.najtmar.gop.gdx.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import cz.wie.najtmar.gop.gdx.GopGdx;

public class StartScreen implements Screen {

    private final Image background;

    private final GopGdx game;
    private final Stage stage;

    public StartScreen(GopGdx game) {
        this.game = game;
        this.stage = new Stage(new ScreenViewport());

        int rowHeight = Gdx.graphics.getWidth() / 12;
        int colWidth = Gdx.graphics.getWidth() / 12;

        Positioner positioner = new Positioner(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), -0.1f);

        background = new Image(new Texture("gop_background.jpg"));
        background.setFillParent(true);
        stage.addActor(background);

        Skin skin = new Skin(Gdx.files.internal("skin/glassy-ui.json"));

        Button initButton = new TextButton("Init game", skin,"default");
        Positioner.Position initButtonPosition = positioner.getCenteredPosition(0);
        initButton.setSize(initButtonPosition.getWidth(), initButtonPosition.getHeight());
        initButton.setPosition(initButtonPosition.getX(), initButtonPosition.getY());
        initButton.addListener(new InputListener() {

            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                StartScreen.this.game.setInitScreen();
            }

            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

        });

        Button joinButton = new TextButton("Join game", skin,"default");
        Positioner.Position joinButtonPosition = positioner.getCenteredPosition(1);
        joinButton.setPosition(joinButtonPosition.getX(), joinButtonPosition.getY());
        joinButton.setSize(joinButtonPosition.getWidth(), joinButtonPosition.getHeight());
        joinButton.addListener(new InputListener() {

            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                StartScreen.this.game.setJoinScreen();
            }

            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

        });

        Button exitButton = new TextButton("Exit", skin,"default");
        Positioner.Position exitButtonPosition = positioner.getCenteredPosition(2);
        exitButton.setPosition(exitButtonPosition.getX(), exitButtonPosition.getY());
        exitButton.setSize(exitButtonPosition.getWidth(), exitButtonPosition.getHeight());
        exitButton.addListener(new InputListener() {

            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                Gdx.app.exit();
            }

            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

        });

        stage.addActor(createLabel("Game of Prawns", positioner, -4, 1, 1, skin, Color.WHITE));
        stage.addActor(createLabel("Game of Prawns", positioner, -4, -2, -2, skin, Color.BLACK));
        stage.addActor(createLabel("Game of Prawns", positioner, -4, 0, 0, skin, Color.CYAN));
        stage.addActor(initButton);
        stage.addActor(joinButton);
        stage.addActor(exitButton);

    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    private Label createLabel(String text, Positioner positioner, int number, float deltaX, float deltaY, Skin skin, Color color) {
        Label label = new Label(text, skin, "big");
        label.setColor(color);
        label.setFontScale(1.5f);
        Positioner.Position labelPosition = positioner.getCenteredPosition(number).translate(deltaX, deltaY);
        label.setSize(labelPosition.getWidth(), labelPosition.getHeight());
        label.setPosition(labelPosition.getX(), labelPosition.getY());
        label.setAlignment(Align.center);
        return label;
    }
}
