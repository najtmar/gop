package cz.wie.najtmar.gop.gdx.fonts;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Setter
@Accessors(fluent = true)
@NoArgsConstructor(staticName = "builder")
public class FontBuilder {

    public static final Font DEFAULT_TRUENO_LT = Font.TRUENO_LT;
    public static final float DEFAULT_SIZE = 100.0f;
    public static final int DEFAULT_BORDER_WIDTH = 1;
    public static final Color DEFAULT_COLOR = Color.BLACK;
    public static final float DEFAULT_OFFSET_X = 8.33f;
    public static final float DEFAULT_OFFSET_Y = 8.33f;
    public static final Color DEFAULT_SHADOW_COLOR = new Color(0.6f, 0.6f, 0.6f, 0.75f);
    public static final String DEFAULT_CHARACTERS = "AĄBCĆDEĘDGHIJKLŁMNŃOÓPQRSŚTUVWXYZŹŻażbcćdeęfghijklłmnńoópqrsśtuvwxyzźż01234567890 ";

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    enum Font {
        // Font converted from with .OTF https://anyconv.com/es/convertidor-de-otf-a-ttf/
        TRUENO_LT("truetypefont/TruenoLt.ttf");

        private final String file;
    }

    private Font font = DEFAULT_TRUENO_LT;
    private float size = DEFAULT_SIZE;
    private int borderWidth = DEFAULT_BORDER_WIDTH;
    private Color color = DEFAULT_COLOR;
    private Color shadowColor = DEFAULT_SHADOW_COLOR;
    private float shadowOffsetX = DEFAULT_OFFSET_X;
    private float shadowOffsetY = DEFAULT_OFFSET_Y;
    private String characters = DEFAULT_CHARACTERS;

    public BitmapFont build() {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal(font.file));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = (int) size;
        parameter.borderWidth = borderWidth;
        parameter.color = color;
        parameter.shadowOffsetX = (int) shadowOffsetX;
        parameter.shadowOffsetY = (int) shadowOffsetY;
        parameter.shadowColor = shadowColor;
        parameter.characters = characters;
        BitmapFont result = generator.generateFont(parameter);
        generator.dispose();
        return result;
    }
}
