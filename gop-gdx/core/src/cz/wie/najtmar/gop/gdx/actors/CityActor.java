package cz.wie.najtmar.gop.gdx.actors;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import cz.wie.najtmar.gop.board.Joint;
import cz.wie.najtmar.gop.common.Point2D;
import cz.wie.najtmar.gop.game.City;
import cz.wie.najtmar.gop.gdx.fonts.FontBuilder;

public class CityActor extends Group {

    private static final float SCALE = 0.3f;

    private final Image image;

    private final float x, xImageDelta;
    private final float y, yImageDelta;

    public CityActor(City city) {
        super();

        Joint joint = city.getJoint();
        Point2D.Double position = joint.getCoordinatesPair();

        // image
        this.image = new Image(Util.createTexture("city-white.png"));

        this.image.setScale(SCALE);

        this.x = (float) position.getX();
        this.xImageDelta = this.image.getWidth() * SCALE / 2.0f;
        this.y = (float) position.getY();
        this.yImageDelta = this.image.getHeight() * SCALE / 2.0f;

        this.image.setX(x - xImageDelta, Align.left);
        this.image.setY(y - yImageDelta, Align.bottom);

        this.addActor(this.image);

        Label.LabelStyle sizeLabelStyle = new Label.LabelStyle();
        sizeLabelStyle.font = FontBuilder.builder()
                .size(this.image.getHeight())
                .shadowOffsetX(this.image.getHeight() / 12)
                .shadowOffsetY(this.image.getHeight() / 12)
                .build();

        Label sizeLabel = new Label("4", sizeLabelStyle);
        sizeLabel.setAlignment(Align.center);

        sizeLabel.setX(x, Align.center);
        sizeLabel.setY(y, Align.center);
        sizeLabel.setFontScale(SCALE * 0.8f);

        this.addActor(sizeLabel);

        // City label
        Label cityLabel = new Label(joint.getName(), sizeLabelStyle);
        cityLabel.setAlignment(Align.center);

        cityLabel.setX(x + xImageDelta, Align.center);
        cityLabel.setY(y - 2 * yImageDelta, Align.center);
        cityLabel.setFontScale(SCALE * 0.8f);
        this.addActor(cityLabel);
    }

}
