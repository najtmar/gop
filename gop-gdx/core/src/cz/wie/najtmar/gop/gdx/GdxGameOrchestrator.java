package cz.wie.najtmar.gop.gdx;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.google.common.base.Preconditions;
import cz.wie.najtmar.gop.board.RoadSection;
import cz.wie.najtmar.gop.game.*;
import cz.wie.najtmar.gop.gdx.actors.CityActor;
import cz.wie.najtmar.gop.gdx.actors.ItinerarySectionActor;
import cz.wie.najtmar.gop.gdx.actors.RoadSectionActor;
import cz.wie.najtmar.gop.gdx.actors.WarriorActor;
import lombok.RequiredArgsConstructor;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
public class GdxGameOrchestrator {

    private final Stage roadStage, cityStage, prawnStage;

    private final Map<RoadSection, RoadSectionActor> roadSectionMap = new HashMap<>();

    private final Map<City, CityActor> cityMap = new HashMap<>();
    private final Map<Warrior, WarriorActor> warriorMap = new HashMap<>();
    private final Map<Prawn, List<ItinerarySectionActor>> itineraryMap = new HashMap<>();

    public void addRoadSection(RoadSection roadSection) {
        RoadSectionActor roadSectionActor = new RoadSectionActor(roadSection);
        this.roadStage.addActor(roadSectionActor);
        roadSectionMap.put(roadSection, roadSectionActor);
    }

    public void addCity(City city) {
        if (cityMap.containsKey(city)) {
            cityMap.get(city).remove();
        }
        CityActor cityActor = new CityActor(city);
        cityStage.addActor(cityActor);
        cityMap.put(city, cityActor);
    }

    public void updateItinerary(Prawn prawn) {
        if (itineraryMap.containsKey(prawn)) {
            for (ItinerarySectionActor actor : itineraryMap.get(prawn)) {
                actor.remove();
            }
        }
        Itinerary itinerary = prawn.getItinerary();
        if (itinerary != null) {
            // TODO: Only draw the current itinerary status.
            List<ItinerarySectionActor> actors = new LinkedList<>();
            for (int i = 0; i < itinerary.getNodes().length - 1; ++i) {
                ItinerarySectionActor actor = new ItinerarySectionActor(itinerary, i);
                prawnStage.addActor(actor);
                actors.add(actor);
            }
            itineraryMap.put(prawn, actors);
        }
    }

    public void addPrawn(Prawn prawn) {
        if (prawn instanceof Warrior) {
            Warrior warrior = (Warrior) prawn;
            if (warriorMap.containsKey(warrior)) {
                warriorMap.get(warrior).remove();
            }
            WarriorActor warriorActor = new WarriorActor(warrior);
            prawnStage.addActor(warriorActor);
            warriorMap.put(warrior, warriorActor);
        } else if (prawn instanceof SettlersUnit) {
            throw new IllegalArgumentException("Not implemented yet");
        } else {
            throw new IllegalArgumentException("Unknown Prawn type: " + prawn.getClass().getSimpleName());
        }
    }

}
