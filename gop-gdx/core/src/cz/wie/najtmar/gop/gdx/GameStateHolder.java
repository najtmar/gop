package cz.wie.najtmar.gop.gdx;

public interface GameStateHolder {

    enum GameState {
        DEFAULT,
        PRAWN_SELECTED,
    }

    GameState getState();
}
