package cz.wie.najtmar.gop.gdx.stages;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;
import cz.wie.najtmar.gop.gdx.GameStateHolder;

public class CityStage extends Stage {
    private final GameStateHolder gameStateHolder;

    public CityStage(Viewport viewport, GameStateHolder gameStateHolder) {
        super(viewport);
        this.gameStateHolder = gameStateHolder;
    }
}
