package cz.wie.najtmar.gop.gdx.screens;

import com.google.common.base.Preconditions;
import lombok.Value;

class Positioner {

    private final float buttonWidth;
    private final float buttonHeight;
    private final float rowHeight;
    private final float zeroWidth;
    private final float zeroHeight;

    Positioner(float width, float height, float heightScale) {
        Preconditions.checkArgument(-1.0f <= heightScale && heightScale <= 1.0f);
        float smaller = Math.min(width, height);
        float larger = Math.max(width, height);
        this.buttonWidth = smaller * 2 / 3;
        this.rowHeight = this.buttonWidth / 6;
        this.buttonHeight = this.rowHeight * 19 / 20;
        this.zeroWidth = width / 2 - buttonWidth / 2;
        this.zeroHeight = height / 2 - rowHeight + height * heightScale / 2;
    }

    public Position getCenteredPosition(int number) {
        return new Position(zeroWidth, zeroHeight - number * rowHeight, buttonWidth, buttonHeight);
    }

    public Position getBottomPosition(int number, float size) {
        return new Position(zeroWidth, zeroHeight - (number + size - 2) * rowHeight, buttonWidth, buttonHeight * (size - 1));
    }

    @Value
    class Position {
        float x;
        float y;
        float width;
        float height;

        public Position translate(float x, float y) {
            return new Position(this.x + x, this.y + y, width, height);
        }
    }

}
