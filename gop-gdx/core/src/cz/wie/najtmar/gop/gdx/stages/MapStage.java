package cz.wie.najtmar.gop.gdx.stages;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;
import cz.wie.najtmar.gop.gdx.GameStateHolder;

public class MapStage extends Stage {
    private final GameStateHolder gameStateHolder;

    public MapStage(Viewport viewport, GameStateHolder gameStateHolder) {
        super(viewport);
        this.gameStateHolder = gameStateHolder;
    }
}
