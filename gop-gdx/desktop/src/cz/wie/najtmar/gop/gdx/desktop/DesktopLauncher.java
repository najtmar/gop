package cz.wie.najtmar.gop.gdx.desktop;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import cz.wie.najtmar.gop.board.BoardProperties;
import cz.wie.najtmar.gop.game.PropertiesReader;
import cz.wie.najtmar.gop.DefaultPropertiesReader;
import cz.wie.najtmar.gop.gdx.GopGdx;

import java.io.IOException;

public class DesktopLauncher {
	public static void main (String[] arg) throws IOException {
		PropertiesReader propertiesReader = new DefaultPropertiesReader();
		BoardProperties boardProperties = new BoardProperties(propertiesReader.getBoardProperties());

		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		config.setResizable(false);
		config.setWindowedMode((int) (boardProperties.getXsize() / GopGdx.MAX_SCALE) + 25, (int) (boardProperties.getYsize() / GopGdx.MAX_SCALE) + 25);

		new Lwjgl3Application(new GopGdx(propertiesReader), config);
	}
}
