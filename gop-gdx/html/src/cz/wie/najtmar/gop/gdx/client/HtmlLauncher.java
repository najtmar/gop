package cz.wie.najtmar.gop.gdx.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;
import cz.wie.najtmar.gop.game.PropertiesReader;
import cz.wie.najtmar.gop.DefaultPropertiesReader;
import cz.wie.najtmar.gop.gdx.GopGdx;

public class HtmlLauncher extends GwtApplication {

        @Override
        public GwtApplicationConfiguration getConfig () {
                // Resizable application, uses available space in browser
                //return new GwtApplicationConfiguration(true);
                // Fixed size application:
                return new GwtApplicationConfiguration(480, 320);
        }

        @Override
        public ApplicationListener createApplicationListener () {
                PropertiesReader propertiesReader = new DefaultPropertiesReader();

                return new GopGdx(propertiesReader);
        }
}