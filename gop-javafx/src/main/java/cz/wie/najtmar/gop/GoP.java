package cz.wie.najtmar.gop;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provides;
import com.google.inject.name.Named;

import cz.wie.najtmar.gop.board.BoardGenerator;
import cz.wie.najtmar.gop.board.MapBoardGenerator;
import cz.wie.najtmar.gop.client.ui.GameManagementEngine;
import cz.wie.najtmar.gop.client.ui.GameManager;
import cz.wie.najtmar.gop.client.ui.GameManagerManagedEngine;
import cz.wie.najtmar.gop.client.ui.InputEngine;
import cz.wie.najtmar.gop.client.ui.JavaFxEngine;
import cz.wie.najtmar.gop.client.ui.JavaFxGameManagementEngine;
import cz.wie.najtmar.gop.client.ui.JavaFxSession;
import cz.wie.najtmar.gop.client.ui.OutputEngine;
import cz.wie.najtmar.gop.client.ui.Session;
import cz.wie.najtmar.gop.client.view.UserView;
import cz.wie.najtmar.gop.game.PropertiesReader;
import cz.wie.najtmar.gop.game.StandardPropertiesReader;
import cz.wie.najtmar.gop.server.ConnectionManager;
import cz.wie.najtmar.gop.server.CrossBoardPrawnDeployer;
import cz.wie.najtmar.gop.server.InitialPrawnDeployer;
import cz.wie.najtmar.gop.server.KryoNetCommunicator;
import javafx.application.Application;
import javafx.stage.Stage;

import java.net.InetAddress;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;

public class GoP extends Application {

  /**
   * The user acting as the primary Player .
   */
  private static UserView primaryUser;

  /**
   * Random number generator.
   */
  private static final Random random = new Random();

  /**
   * JavaFx primary Stage .
   */
  private static Stage primaryStage;

  /**
   * Manages Games .
   */
  private static GameManager gameManager;

  /**
   * Guice injector.
   */
  private Injector injector;

  private static AbstractModule createGopModule() {
    return new AbstractModule() {
      @Override
      protected void configure() {
        bind(PropertiesReader.class).to(StandardPropertiesReader.class);
        bind(BoardGenerator.class).to(MapBoardGenerator.class);
        bind(InitialPrawnDeployer.class).to(CrossBoardPrawnDeployer.class);  // TODO(najtmar): Change this.
        bind(ConnectionManager.Factory.class).to(KryoNetCommunicator.Factory.class);
        bind(GameManagementEngine.class).to(JavaFxGameManagementEngine.class);
        bind(InputEngine.class).to(JavaFxEngine.class);
        bind(OutputEngine.class).to(JavaFxEngine.class);
        bind(GameManagerManagedEngine.class).to(JavaFxEngine.class);
        bind(Session.class).to(JavaFxSession.class);
        bind(UserView.class).toInstance(primaryUser);
        bind(Stage.class).toInstance(primaryStage);
        bind(Locale.class).toInstance(Locale.UK);
      }

      @Provides
      @Named("random seed")
      Long provideRandomSeed() {
        return random.nextLong();
      }
    };
  }

  public GoP() {
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    // This block must come first.
    primaryUser = new UserView(InetAddress.getLocalHost().getHostName(), UUID.randomUUID());

    // This block must come second.
    GoP.primaryStage = primaryStage;
    injector = Guice.createInjector(createGopModule());

    // This makes all stages close and the app exit when the main stage is closed.
    primaryStage.setOnCloseRequest(e -> System.exit(0));

    gameManager = injector.getInstance(GameManager.class);
    gameManager.initialize();
    primaryStage.show();
  }

  public static void main(String[] args) {
    launch(args);
  }

}
