package cz.wie.najtmar.gop.client.ui;

import cz.wie.najtmar.gop.client.view.ClientViewException;
import cz.wie.najtmar.gop.client.view.GameView;
import cz.wie.najtmar.gop.client.view.PlayerView;
import cz.wie.najtmar.gop.event.Event;
import javafx.application.Platform;

/**
 * UI Session to run Games using JavaFX .
 * @author najtmar
 */
public class JavaFxSession implements Session {

  /**
   * The UI Client managing the User view of the Game .
   */
  private Client gameClient;

  public JavaFxSession() {}

  @Override
  public void initialize(Client gameClient, GameView game) throws ClientViewException {
    this.gameClient = gameClient;
    this.gameClient.initialize(game);
  }

  @Override
  public Event[] processEvents(Event[] inputEvents, PlayerView player) throws ClientViewException {
    final Event[] userEvents;
    if (inputEvents.length > 0) {
      gameClient.snapshot(inputEvents);
      // Using this to avoid "java.lang.IllegalStateException: Not on FX application thread;" errors.
      Platform.runLater(new Runnable() {
        @Override
        public void run() {
          try {
            gameClient.paint();
          } catch (ClientViewException exception) {
            throw new RuntimeException("Error while painting.");
          }
        }
      });
      userEvents = gameClient.interact(inputEvents);
    } else {
      userEvents = new Event[]{};
    }

    return userEvents;
  }

}
