package cz.wie.najtmar.gop.client.ui;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import cz.wie.najtmar.gop.board.BoardException;
import cz.wie.najtmar.gop.board.BoardProperties;
import cz.wie.najtmar.gop.client.ui.InputManager.State;
import cz.wie.najtmar.gop.client.view.BattleView;
import cz.wie.najtmar.gop.client.view.CityView;
import cz.wie.najtmar.gop.client.view.ClientViewException;
import cz.wie.najtmar.gop.client.view.ClientViewProperties;
import cz.wie.najtmar.gop.client.view.FactoryView;
import cz.wie.najtmar.gop.client.view.GameView;
import cz.wie.najtmar.gop.client.view.JointView;
import cz.wie.najtmar.gop.client.view.PlayerView;
import cz.wie.najtmar.gop.client.view.PositionView;
import cz.wie.najtmar.gop.client.view.PrawnView;
import cz.wie.najtmar.gop.client.view.RoadSectionView;
import cz.wie.najtmar.gop.client.view.RoadSectionView.Direction;
import cz.wie.najtmar.gop.client.view.TextManager;
import cz.wie.najtmar.gop.common.Point2D;
import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.game.PropertiesReader;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.TextField;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.SVGPath;
import javafx.scene.shape.Shape;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Transform;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.concurrent.Semaphore;
import java.util.stream.Collectors;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;

@Singleton
public class JavaFxEngine implements InputEngine, OutputEngine, GameManagerManagedEngine {

  /**
   * Padding used for the canvas Group .
   */
  static final double CANVAS_PADDING = 25;

  public static final Insets BUTTON_MARGINS = new Insets(2.0,  2.0,  2.0, 2.0);

  static final double STATUS_BAR_HEIGHT = 120;
  static final double CONTROL_PANEL_HEIGHT = 80;

  static final double BOARD_VISIBLE_ROAD_WIDTH = 2.0;
  static final double BOARD_DISCOVERED_ROAD_WIDTH = 1.0;
  static final double BOARD_HIGHLIGHTED_LINE_WIDTH = 4.0;
  static final double BOARD_PRAWN_RADIUS = 15.0;
  static final double BOARD_CITY_RADIUS = 25.0;
  static final double ITINERARY_JOINT_RADIUS = 3.0;

  // Colors.
  static final Paint BOX_COLOR = Color.color(0.1, 0.1, 0.1, 0.9);
  static final Paint[] PLAYER_COLORS = {
      // RadialGradient.valueOf("center 0px 0px, radius 40px, white  0%, black 100%"),
      LinearGradient.valueOf("from -34px 34px to 34px -34px, black 0% , white 50%, black 100%"),
      // RadialGradient.valueOf("center 0px 0px, radius 40px, black  0%, white 100%"),
      LinearGradient.valueOf("from -34px 34px to 34px -34px, white 0% , black 50%, white 100%"),
      Color.RED, Color.GREEN, Color.BLUE};
  static final Paint BATTLE_COLOR = Color.valueOf("ff5100");
  static final Color[] COMPLEMENTARY_PLAYER_COLORS = {Color.BLACK, Color.WHITE, Color.GREEN, Color.RED, Color.ORANGE};
  static final Paint HIGHLIGHT_COLOR = Color.color(1.0, 1.0, 1.0, 0.5);
  static final Paint ITINERARY_COLOR = Color.color(1.0, 1.0, 1.0, 1.0);
  static final Paint TEXT_COLOR = Color.color(0.8, 0.8, 0.8);

  // Styles.
  public static String SMALL_BUTTON_STYLE =
      "#bevel-grey { "
      + "-fx-background-color: "
      + "  linear-gradient(#f2f2f2, #d6d6d6), "
      + "  linear-gradient(#fcfcfc 0%, #d9d9d9 20%, #d6d6d6 100%), "
      + "  linear-gradient(#dddddd 0%, #f6f6f6 50%); "
      + "-fx-background-radius: 8,7,6; "
      + "-fx-background-insets: 0,1,2; "
      + "-fx-font-family: \"Helvetica\"; "
      + "-fx-text-fill: black; "
      + "-fx-effect: dropshadow( three-pass-box , rgba(0,0,0,0.6) , 5, 0.0 , 0 , 1 ); "
      + "} ";

  /**
   * The GameManager managing Games .
   */
  private GameManager gameManager;

  /**
   * Responsible for handing user interaction.
   */
  private InputManager inputManager;

  /**
   * Whether own connection to the Game server is up and running.
   */
  private boolean gameServerConnected;

  /**
   * Whether all the connections between the other Game client and the Game server is up and running.
   */
  private boolean otherGameClientsConnected;

  /**
   * JavaFX primary Stage .
   */
  private final Stage primaryStage;

  /**
   * Reads Properties .
   */
  private final PropertiesReader propertiesReader;

  private BoardProperties boardProperties;
  private ClientViewProperties clientViewProperties;

  /**
   * Manages text localization.
   */
  private final TextManager textManager;

  /**
   * The Player for the given Game .
   */
  private PlayerView currentPlayer;

  // Images.
  private Image connectionBrokenImage;
  private Image connectionProblemImage;
  private Image warningImage;

  // Scrolled board.
  private Group scrolledBoard;
  private ScrollPane scrollPane;
  private boolean scrollPaneInitialized;

  // Status bar.
  private TextFlow statusBar;
  private Group statusBarGroup;
  private ScrollPane statusBarScrollPane;
  private Text stateLabel;
  private Text timeBox;
  private Text relevantEventsLabel;

  // Control panel.
  private FlowPane controlPanel;

  // Layers.
  private Group backgroundLayer;
  private Group boardLayer;
  private Group prawnLayer;
  private Group cityLayer;
  private Group battleLayer;
  private Group magnifyLayer;
  private Group highlightLayer;
  private Group prawnSelectionLayer;
  private Group cityViewLayer;
  private Group floatingHighlightLayer;
  private Group warriorDirectionSelectionLayer;
  private Group promptLayer;
  private Group allLayers;

  // Board parameters.
  private int xsize;
  private int ysize;
  private int viewportWidth;
  private int viewportHeight;

  // Magnification parameters.
  private int magnificationMinX;
  private int magnificationMinY;
  private int magnificationWidth;
  private int magnificationHeight;
  private double magnificationTranslatedMinX;
  private double magnificationTranslatedMinY;
  private double magnificationRatio;
  private double magnifiedViewClickDistance;

  // City box parameters.
  private int cityBoxSide;
  private int cityBoxMinX;
  private int cityBoxMinY;
  private int cityBoxCenterX;
  private int cityBoxCenterY;
  private int cityDescriptionBoxMinX;
  private int cityDescriptionBoxMinY;
  private int cityDescriptionBoxWidth;
  private int cityDescriptionBoxHeight;

  // City view PrawnSelection parameters.
  private int cityViewPrawnSelectionMinX;
  private int cityViewPrawnSelectionMinY;
  private int cityViewPrawnSelectionWidth;
  private int cityViewPrawnSelectionHeight;
  private int cityViewPrawnSelectionBoxWidth;
  private int cityViewPrawnSelectionBoxHeight;
  private int cityViewPrawnBoxSide;

  // City view factory parameters.
  private int cityViewFactoriesMinX;
  private int cityViewFactoriesMinY;
  private int cityViewFactoryBoxWidth;
  private int cityViewFactoryBoxHeight;
  private int cityViewFactoryBoxMaxCount;
  private int cityViewFactoryItemMinX;
  private int cityViewFactoryStatusBarMinX;

  // City creation parameters.
  private int cityCreationBoxMinX;
  private int cityCreationBoxMinY;
  private int cityCreationBoxWidth;
  private int cityCreationBoxHeight;

  // Warrior direction parameters.
  private int warriorDirectionSelectionBoxMinX;
  private int warriorDirectionSelectionBoxMinY;
  private int warriorDirectionSelectionBoxSide;

  // Prompt box parameters.
  private int promptBoxMinX;
  private int promptBoxMinY;
  private int promptBoxWidth;
  private int promptBoxHeight;

  // Prawn selection parameters.
  private int prawnSelectionMinX;
  private int prawnSelectionMinY;
  private int prawnSelectionWidth;
  private int prawnSelectionHeight;
  private int prawnSelectionBoxWidth;
  private int prawnSelectionBoxHeight;
  private int prawnBoxSide;

  // Prawn repair parameters.
  private int prawnRepairPromptMinX;
  private int prawnRepairPromptMinY;
  private int prawnRepairPromptWidth;
  private int prawnRepairPromptHeight;

  // UI elements.
  private Map<RoadSectionView, Shape[]> roadSectionToShapes;
  private Map<PrawnView, Node> prawnToNodes;
  private Map<CityView, Node> cityToImage;
  private Map<BattleView, Shape> battleToShape;
  private Polygon warriorToBeBuilt;
  private double warriorToBeBuiltDirection;

  // Controls.
  // * State BOARD
  private Button boardPlayButton;
  private Semaphore boardNextRoundSemaphore;
  private Button boardAbandonGameButton;
  // * State PRAWN
  private Button prawnClearItineraryButton;
  private Button prawnBuildCityButton;
  private Button prawnClosePrawnButton;
  // * State ITINERARY_BUILT
  private Button itineraryBuiltConfirmButton;
  private Button itineraryBuiltAbandonButton;
  // * State MAGNIFICATION
  private Button magnificationCloseButton;
  // * State CITY_CREATION
  private Rectangle cityCreationBox;
  private TextField cityCreationNameField;
  private Label cityCreationNameLabel;
  private Button cityCreationConfirmButton;
  private Button cityCreationAbandonButton;
  // * State CITY
  private Button cityViewCloseButton;
  // * State FACTORY
  private Button factorySetProduceSettlersUnitButton;
  private Button factorySetProduceWarriorButton;
  private Button factorySetRepairPrawnButton;
  private Button factorySetGrowCityButton;
  private Button factoryCloseCityButton;
  // * State WARRIOR_DIRECTION
  private Button warriorDirectionConfirmButton;
  private Button warriorDirectionAbandonButton;
  // * State GAME_ABANDONING
  private Button gameAbandoningConfirmButton;
  private Button gameAbandoningCancelButton;
  // * State PRAWN_SELECTION
  private Button prawnSelectionCloseButton;
  // * State PRAWN_REPAIR
  private Rectangle prawnRepairPromptBox;
  private Button prawnRepairConfirmButton;
  private Button prawnRepairAbandonButton;
  // * State GAME_FINISHED
  private Button gameFinishedCloseButton;

  /**
   * Constructor.
   * @param primaryStage the JavaFX Stage where the Game is visualized
   * @param propertiesReader used to read Properties used in a Game
   * @param textManager used for text localization
   */
  @Inject
  public JavaFxEngine(Stage primaryStage, PropertiesReader propertiesReader, TextManager textManager) {
    this.primaryStage = primaryStage;
    this.propertiesReader = propertiesReader;
    this.textManager = textManager;
  }

  private void initializeElementContainers() {
    roadSectionToShapes = new HashMap<>();
    prawnToNodes = new HashMap<>();
    cityToImage = new HashMap<>();
    battleToShape = new HashMap<>();
  }

  private void initializeBoardParameters() {
    xsize = boardProperties.getXsize();
    ysize = boardProperties.getYsize();

    viewportWidth = clientViewProperties.getMainViewWidth();
    viewportHeight = clientViewProperties.getMainViewHeight();
  }

  private void initializeLayers() throws ClientViewException {
    backgroundLayer = new Group();
    final ImageView backgroundImage;
    try {
      backgroundImage = new ImageView(new Image(boardProperties.getBackgroundImage(),
          xsize + 2 * CANVAS_PADDING, ysize + 2 * CANVAS_PADDING, false, true));
    } catch (IllegalArgumentException exception) {
      throw new ClientViewException("Cannot load background file \"" + boardProperties.getBackgroundImage() + "\".",
          exception);
    }
    backgroundImage.setTranslateX(-CANVAS_PADDING);
    backgroundImage.setTranslateY(-CANVAS_PADDING);
    backgroundLayer.getChildren().add(backgroundImage);
    prawnLayer = new Group();
    magnifyLayer = new Group();
    cityLayer = new Group();
    highlightLayer = new Group();
    battleLayer = new Group();
    floatingHighlightLayer = new Group();
    cityViewLayer = new Group();
    warriorDirectionSelectionLayer = new Group();
    promptLayer = new Group();
    prawnSelectionLayer = new Group();
  }

  /*
  private Point2D.Double calculateScrolledBoardBarInitialSetup() {
    final GameView game = inputManager.getGame();
    double xcoord = 0.0;
    double ycoord = 0.0;
    int prawnCount = 0;
    for (PrawnView ownPrawn : game.getPlayerPrawns(game.getPlayers()[game.getCurrentPlayerIndex()])) {
      final Point2D.Double ownPrawnCoordinates = ownPrawn.getCurrentPosition().getCoordinatesPair();
      xcoord += ownPrawnCoordinates.getX();
      ycoord += ownPrawnCoordinates.getY();
      ++prawnCount;
    }
    if (0.0 < prawnCount) {
      xcoord /= prawnCount;
      ycoord /= prawnCount;
    }
    double hvalue;
    if (xsize == viewportWidth) {
      hvalue = 0.0;
    } else {
      hvalue = (xcoord - viewportWidth / 2) / (xsize - viewportWidth);
      if (hvalue < 0.0) {
        hvalue = 0.0;
      } else if (1.0 < hvalue) {
        hvalue = 1.0;
      }
    }
    double vvalue;
    if (ysize == viewportHeight) {
      vvalue = 0.0;
    } else {
      vvalue = (ycoord - viewportHeight / 2) / (ysize - viewportHeight);
      if (vvalue < 0.0) {
        vvalue = 0.0;
      } else if (1.0 < vvalue) {
        vvalue = 1.0;
      }
    }
    return new Point2D.Double(hvalue, vvalue);
  }
  */

  private void initializeScrolledBoard(@Nonnull GameView game) {
    boardLayer = new Group();
    scrolledBoard = new Group();
    scrollPane = new ScrollPane();
    scrollPaneInitialized = false;
    final Rectangle boardRectangle =
        new Rectangle(-CANVAS_PADDING, -CANVAS_PADDING, xsize + 2 * CANVAS_PADDING, ysize + 2 * CANVAS_PADDING);
    boardRectangle.setFill(Color.TRANSPARENT);

    scrolledBoard.getChildren().addAll(boardRectangle, backgroundLayer, boardLayer,
        cityLayer, highlightLayer, battleLayer, prawnLayer);
    scrollPane.setContent(this.scrolledBoard);

    scrollPane.setPrefSize(viewportWidth, viewportHeight);
    scrollPane.setPannable(true);
    scrollPane.setHbarPolicy(ScrollBarPolicy.NEVER);
    scrollPane.setVbarPolicy(ScrollBarPolicy.NEVER);

    // CARGO CULT: This prevents the ScrollPane from getting blurry.
    this.scrollPane.setStyle(
        "-fx-background-color: -fx-outer-border, -fx-inner-border, -fx-body-color; -fx-background-insets: 0, 1, 2; "
        + "-fx-background-radius: 5, 4, 3;");
  }

  private void initializeStatusBar() {
    statusBar = new TextFlow();
    statusBarGroup = new Group();
    statusBarScrollPane = new ScrollPane();

    stateLabel = new Text();
    stateLabel.setFont(new Font("Arial Bold", 30));
    stateLabel.setFill(Color.WHITE);

    timeBox = new Text();
    timeBox.setFont(new Font("Arial Italic", 15));
    timeBox.setFill(Color.WHITE);

    relevantEventsLabel = new Text();
    relevantEventsLabel.setFont(new Font("Arial", 15));
    relevantEventsLabel.setFill(Color.WHITE);

    statusBar.setPrefSize(viewportWidth, STATUS_BAR_HEIGHT);
    statusBar.setBackground(new Background(new BackgroundFill(BOX_COLOR, new CornerRadii(0.0),
        new Insets(0, 0, 0, 0))));
    statusBar.setPadding(new Insets(10.0, 10.0, 10.0, 10.0));
    statusBar.getChildren().addAll(stateLabel, timeBox, relevantEventsLabel);

    statusBarGroup.getChildren().setAll(statusBar);
    statusBarGroup.autosize();

    statusBarScrollPane.setPannable(true);
    statusBarScrollPane.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
    statusBarScrollPane.setHbarPolicy(ScrollBarPolicy.NEVER);
    statusBarScrollPane.setContent(statusBarGroup);
  }

  private void initializeControlPanel() {
    controlPanel = new FlowPane();

    controlPanel.setPrefSize(viewportWidth, CONTROL_PANEL_HEIGHT);
    controlPanel.setBackground(new Background(new BackgroundFill(BOX_COLOR, new CornerRadii(0.0),
        new Insets(0, 0, 0, 0))));
    controlPanel.setPadding(new Insets(10, 10, 10, 10));
    controlPanel.setOrientation(Orientation.HORIZONTAL);
    controlPanel.setLayoutY(viewportHeight);
    controlPanel.setAlignment(Pos.BASELINE_LEFT);
  }

  private void initializeImages() {
    connectionBrokenImage = new Image("/images/connection_broken.png", 299, 266, true, true);
    connectionProblemImage = new Image("/images/connection_problem.png", 283, 282, true, true);
    warningImage = new Image("/images/74-emoji_android_warning_sign.png", 266, 266, true, true);
  }

  private void initializePrimaryStage() {
    allLayers = new Group();
    allLayers.getChildren().addAll(scrollPane, magnifyLayer, cityViewLayer, floatingHighlightLayer,
        warriorDirectionSelectionLayer, promptLayer, prawnSelectionLayer);

    final FlowPane primaryPane = new FlowPane();
    primaryPane.setOrientation(Orientation.HORIZONTAL);
    primaryPane.setAlignment(Pos.TOP_LEFT);
    primaryPane.getChildren().addAll(statusBarScrollPane, allLayers, controlPanel);

    Scene rootScene = new Scene(primaryPane);
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        primaryStage.setScene(rootScene);
      }
    });
  }

  /**
   * Creates a small Button .
   * @param nameNotLocalized the name of the Button, to be localized using textManager
   * @return a small Button with a given name localized
   */
  private Button createSmallButton(@Nonnull String nameNotLocalized) {
    final Button result = new Button(textManager.getText(nameNotLocalized));
    result.setStyle(SMALL_BUTTON_STYLE);
    result.setPrefWidth(viewportWidth / 3.0 - 20);
    return result;
  }


  @Override
  public void initializeOutput(GameView game) throws ClientViewException {
    if (gameManager == null) {
      throw new ClientViewException("GameManager not set.");
    }
    currentPlayer = game.getPlayers()[game.getCurrentPlayerIndex()];
    try {
      boardProperties = new BoardProperties(propertiesReader.getBoardProperties());
    } catch (IOException | BoardException exception) {
      throw new ClientViewException("BoardProperties could not be read.", exception);
    }
    try {
      clientViewProperties = new ClientViewProperties(propertiesReader.getClientViewProperties());
    } catch (IOException exception) {
      throw new ClientViewException("BoardProperties could not be read.", exception);
    }

    initializeElementContainers();
    initializeBoardParameters();
    initializeLayers();
    initializeScrolledBoard(game);
    initializeStatusBar();
    initializeControlPanel();
    initializeImages();

    initializePrimaryStage();

    gameServerConnected = true;
    otherGameClientsConnected = true;
  }

  private void initializeBoardStateInput() {
    boardNextRoundSemaphore = new Semaphore(0);
    boardPlayButton = createSmallButton("Play");
    boardPlayButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        boardNextRoundSemaphore.release();
      }
    });
    boardAbandonGameButton = createSmallButton("Abandon game");
    boardAbandonGameButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onGameAbandoningPromptShown();
      }
    });
  }

  private void initializePrawnStateInput() {
    prawnClearItineraryButton = createSmallButton("Clear itinerary");
    prawnClearItineraryButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onPrawnItineraryCleared();
      }
    });
    prawnBuildCityButton = createSmallButton("Build city");
    prawnBuildCityButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onCityCreationStarted();
      }
    });
    prawnClosePrawnButton = createSmallButton("Close");
    prawnClosePrawnButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onPrawnViewClosed();
      }
    });
  }

  private void initializeItineraryBuiltStateInput() {
    itineraryBuiltConfirmButton = createSmallButton("Confirm itinerary");
    itineraryBuiltConfirmButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onPrawnItineraryBuildConfirmed();
      }
    });
    itineraryBuiltAbandonButton = createSmallButton("Abandon itinerary");
    itineraryBuiltAbandonButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onPrawnItineraryBuildAbandoned();
      }
    });
  }

  private void initializeMagnificationStateInput() {
    magnificationCloseButton = createSmallButton("Close");
    magnificationCloseButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onMagnificationOff();
      }
    });
  }

  private void initializeCityCreationStateInput() {
    cityCreationBox = new Rectangle(cityCreationBoxMinX, cityCreationBoxMinY, cityCreationBoxWidth,
        cityCreationBoxHeight);
    cityCreationBox.setFill(BOX_COLOR);
    cityCreationNameField = new TextField();
    cityCreationNameField.setPrefColumnCount(10);
    cityCreationNameField.setAlignment(Pos.CENTER_LEFT);
    cityCreationNameField.setTranslateX(cityCreationBoxMinX + 20);
    cityCreationNameField.setTranslateY(cityCreationBoxMinY + 40);
    cityCreationNameLabel = new Label(textManager.getText("Enter city name:"));
    cityCreationNameLabel.setFont(new Font(cityViewFactoryBoxHeight / 3));
    cityCreationNameLabel.setTextFill(TEXT_COLOR);
    cityCreationNameLabel.setTranslateX(cityCreationBoxMinX + 20);
    cityCreationNameLabel.setTranslateY(cityCreationBoxMinY + 40 - cityViewFactoryBoxHeight / 3 - 5);
    cityCreationConfirmButton = createSmallButton("Create city");
    cityCreationConfirmButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onCityCreationConfirmed(cityCreationNameField.getText());
      }
    });
    cityCreationAbandonButton = createSmallButton("Cancel");
    cityCreationAbandonButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onCityCreationAbandoned();
      }
    });
  }

  private void initializeCityViewStateInput() {
    cityViewCloseButton = createSmallButton("Close");
    cityViewCloseButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onCityViewOff();
      }
    });
  }

  private void initializeCityFactoryStateInput() {
    factorySetProduceSettlersUnitButton = createSmallButton("Produce settlers");
    factorySetProduceSettlersUnitButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onSetProduceSettlersUnit();
      }
    });
    factorySetProduceWarriorButton = createSmallButton("Produce warrior");
    factorySetProduceWarriorButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onWarriorDirectionSelectionShown();
      }
    });
    factorySetRepairPrawnButton = createSmallButton("Repair prawn");
    factorySetRepairPrawnButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onPrawnRepairSelectionShown();
      }
    });
    factorySetGrowCityButton = createSmallButton("Grow city");
    factorySetGrowCityButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onSetGrowCity();
      }
    });
    factoryCloseCityButton = createSmallButton("Close");
    factoryCloseCityButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onCityFactoryViewOff();
      }
    });
  }

  private void initializeWarriorDirectionStateInput() {
    warriorDirectionConfirmButton = createSmallButton("Confirm warrior");
    warriorDirectionConfirmButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onSetProduceWarrior(warriorToBeBuiltDirection);
      }
    });
    warriorDirectionAbandonButton = createSmallButton("Abandon warrior");
    warriorDirectionAbandonButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onWarriorDirectionSelectionOff();
      }
    });
  }

  private void initializeGameAbandoningStateInput() {
    gameAbandoningConfirmButton = createSmallButton("OK");
    gameAbandoningConfirmButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onGameAbandoned();
      }
    });
    gameAbandoningCancelButton = createSmallButton("Cancel");
    gameAbandoningCancelButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onGameAbandoningPromptOff();
      }
    });
  }

  private void initializePrawnSelectionStateInput() {
    prawnSelectionCloseButton = createSmallButton("Close");
    prawnSelectionCloseButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onPrawnSelectionOff();
      }
    });
  }

  private void initializePrawnRepairStateInput() {
    prawnRepairPromptBox = new Rectangle(prawnRepairPromptMinX, prawnRepairPromptMinY,
        prawnRepairPromptWidth, prawnRepairPromptHeight);
    prawnRepairConfirmButton = createSmallButton("Confirm");
    prawnRepairConfirmButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onSetRepairPrawn();
      }
    });
    prawnRepairAbandonButton = createSmallButton("Cancel");
    prawnRepairAbandonButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onPrawnRepairSelectionOff();
      }
    });
  }

  private void initializeGameFinishedStateInput() {
    gameFinishedCloseButton = createSmallButton("Close");
    gameFinishedCloseButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        try {
          inputManager.reset();
          gameManager.reset(false);
        } catch (ClientViewException exception) {
          throw new RuntimeException(exception);
        }
      }
    });
  }

  private void initializeMagnificationParameters() {
    magnificationRatio = clientViewProperties.getMagnificationRatio();
    magnificationWidth = clientViewProperties.getMagnifiedViewWidth();
    magnificationHeight = clientViewProperties.getMagnifiedViewHeight();
    magnificationMinX = (viewportWidth - magnificationWidth) / 2;
    magnificationMinY = (viewportHeight - magnificationHeight) / 2;
    magnifiedViewClickDistance = clientViewProperties.getDistinctivePositionRadius() * magnificationRatio;
  }

  private void initializeCityCreationParameters() {
    cityCreationBoxWidth = clientViewProperties.getCityCreationBoxWidth();
    cityCreationBoxHeight = clientViewProperties.getCityCreationBoxHeight();
    cityCreationBoxMinX = (viewportWidth - cityCreationBoxWidth) / 2;
    cityCreationBoxMinY = (viewportHeight - cityCreationBoxHeight) / 2;
  }

  private void initializeCityViewParameters() {
    cityBoxSide = clientViewProperties.getCityBoxSide();
    cityBoxMinX = viewportWidth - (viewportWidth - clientViewProperties.getCityFactoryBoxWidth()) / 2 - cityBoxSide;
    cityBoxMinY = 100;
    cityBoxCenterX = cityBoxMinX + cityBoxSide / 2;
    cityBoxCenterY = cityBoxMinY + cityBoxSide / 2;

    cityDescriptionBoxMinX = (viewportWidth - clientViewProperties.getCityFactoryBoxWidth()) / 2;
    cityDescriptionBoxMinY = cityBoxMinY;
    cityDescriptionBoxWidth = clientViewProperties.getCityFactoryBoxWidth() - cityBoxSide;
    cityDescriptionBoxHeight = cityBoxSide;

    cityViewPrawnBoxSide = clientViewProperties.getCityPrawnBoxSide();
    cityViewPrawnSelectionWidth = clientViewProperties.getCityPrawnSelectionWidth();
    cityViewPrawnSelectionHeight = clientViewProperties.getCityPrawnSelectionHeight();
    cityViewPrawnSelectionBoxWidth = cityViewPrawnSelectionWidth * cityViewPrawnBoxSide;
    cityViewPrawnSelectionBoxHeight = cityViewPrawnSelectionHeight * cityViewPrawnBoxSide;
    cityViewPrawnSelectionMinX = (viewportWidth - cityViewPrawnSelectionBoxWidth) / 2;
    cityViewPrawnSelectionMinY = cityBoxMinY + cityBoxSide;

    cityViewFactoryBoxWidth = clientViewProperties.getCityFactoryBoxWidth();
    cityViewFactoryBoxHeight = clientViewProperties.getCityFactoryBoxHeight();
    cityViewFactoriesMinX = (viewportWidth - cityViewFactoryBoxWidth) / 2;
    cityViewFactoriesMinY = cityViewPrawnSelectionMinY + 4 + cityViewPrawnSelectionBoxHeight;
    cityViewFactoryBoxMaxCount = clientViewProperties.getCityFactoryBoxMaxCount();
    cityViewFactoryItemMinX = cityViewFactoriesMinX + 2 * cityViewFactoryBoxHeight;
    cityViewFactoryStatusBarMinX = cityViewFactoryItemMinX + cityViewFactoryBoxHeight;
  }

  private void initializeWarriorDirectionParameters() {
    warriorDirectionSelectionBoxSide = clientViewProperties.getWarriorDirectionSelectionBoxSide();
    warriorDirectionSelectionBoxMinX = (viewportWidth - warriorDirectionSelectionBoxSide) / 2;
    warriorDirectionSelectionBoxMinY = (viewportHeight - warriorDirectionSelectionBoxSide) / 2;
  }

  private void initializePromptBoxParameters() {
    promptBoxWidth = clientViewProperties.getPromptBoxWidth();
    promptBoxHeight = clientViewProperties.getPromptBoxHeight();
    promptBoxMinX = (viewportWidth - promptBoxWidth) / 2;
    promptBoxMinY = (viewportHeight - promptBoxHeight) / 2 - 100;
  }

  private void initializePrawnSelectionParameters() {
    prawnBoxSide = clientViewProperties.getPrawnBoxSide();
    prawnSelectionWidth = clientViewProperties.getPrawnSelectionWidth();
    prawnSelectionHeight = clientViewProperties.getPrawnSelectionHeight();
    prawnSelectionBoxWidth = prawnSelectionWidth * prawnBoxSide;
    prawnSelectionBoxHeight = prawnSelectionHeight * prawnBoxSide;
    prawnSelectionMinX = (viewportWidth - prawnSelectionBoxWidth) / 2;
    prawnSelectionMinY = (viewportHeight - prawnSelectionBoxHeight) / 2;
  }

  private void initializePrawnRepairParameters() {
    prawnRepairPromptWidth = clientViewProperties.getPrawnRepairPromptWidth();
    prawnRepairPromptHeight = clientViewProperties.getPrawnRepairPromptHeight();
    prawnRepairPromptMinX = (viewportWidth - prawnRepairPromptWidth) / 2;
    prawnRepairPromptMinY = cityViewPrawnSelectionMinY - prawnRepairPromptHeight - 10;
  }

  @Override
  public void initializeInput(GameView unusedGame, InputManager inputManager) throws ClientViewException {
    if (gameManager == null) {
      throw new ClientViewException("GameManager not set.");
    }
    this.inputManager = inputManager;
    try {
      clientViewProperties = new ClientViewProperties(propertiesReader.getClientViewProperties());
    } catch (IOException exception) {
      throw new ClientViewException("BoardProperties could not be read.", exception);
    }

    initializeMagnificationParameters();
    initializeCityCreationParameters();
    initializeCityViewParameters();
    initializeWarriorDirectionStateInput();
    initializeGameAbandoningStateInput();
    initializePrawnSelectionStateInput();
    initializePrawnRepairStateInput();
    initializeGameFinishedStateInput();

    initializeBoardStateInput();
    initializePrawnStateInput();
    initializeItineraryBuiltStateInput();
    initializeMagnificationStateInput();
    initializeCityCreationStateInput();
    initializeCityViewStateInput();
    initializeCityFactoryStateInput();
    initializeWarriorDirectionParameters();
    initializePromptBoxParameters();
    initializePrawnSelectionParameters();
    initializePrawnRepairParameters();
    // TODO(najtmar): Finish this implementation.
  }

  @Override
  public void onTimeUpdated(long time) {
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        timeBox.setText(textManager.pseudoTimestampToDateString(time) + "\n");
        statusBar.autosize();
      }
    });
  }

  @Override
  public void onPeriodicInfoUpdated(NavigableMap<String, Integer> relevantEvents) throws ClientViewException {
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        final StringBuffer relevantEventsStringBuffer;
        if (!relevantEvents.isEmpty()) {
          relevantEventsStringBuffer = new StringBuffer(textManager.getText("Relevant events:"));
          for (NavigableMap.Entry<String, Integer> entry : relevantEvents.entrySet()) {
            relevantEventsStringBuffer.append("\n ");
            if (entry.getValue() > 1) {
              relevantEventsStringBuffer.append("" + entry.getValue() + " x ");
            }
            relevantEventsStringBuffer.append(textManager.getText(entry.getKey()));
          }
        } else {
          relevantEventsStringBuffer =
              new StringBuffer(
                  textManager.getText("NO RELEVANT EVENTS\n(other player's events caused the interruption)"));
        }
        relevantEventsLabel.setText(relevantEventsStringBuffer.toString());
        statusBar.autosize();
      }
    });
  }

  private Line initializeRoadSectionHalfView(RoadSectionView roadSection, Direction end) {
    roadSectionToShapes.putIfAbsent(roadSection, new Shape[2]);
    final int index;
    if (end == RoadSectionView.Direction.BACKWARD) {
      index = 0;
    } else {
      index = 1;
    }
    Line line = (Line) roadSectionToShapes.get(roadSection)[index];
    if (line == null) {
      final JointView[] joints = roadSection.getJoints();
      final PositionView midpoint = roadSection.getPositions()[roadSection.getMidpointIndex()];
      final Point2D.Double startPoint;
      final Point2D.Double endPoint;
      if (end == RoadSectionView.Direction.BACKWARD) {
        startPoint = joints[0].getCoordinatesPair();
        endPoint = midpoint.getCoordinatesPair();
      } else {
        startPoint = midpoint.getCoordinatesPair();
        endPoint = joints[1].getCoordinatesPair();
      }
      line = new Line(startPoint.getX(), startPoint.getY(), endPoint.getX(), endPoint.getY());
      line.setStroke(Color.BLACK);
      line.setSmooth(true);
      roadSectionToShapes.get(roadSection)[index] = line;
      boardLayer.getChildren().add(line);
    }
    return line;
  }

  @Override
  public void onRoadSectionHalfVisible(RoadSectionView roadSection, Direction end) {
    final Line view = initializeRoadSectionHalfView(roadSection, end);
    view.setStrokeWidth(BOARD_VISIBLE_ROAD_WIDTH);
    view.getStrokeDashArray().clear();
    view.setVisible(true);
  }

  @Override
  public void onRoadSectionHalfDiscovered(RoadSectionView roadSection, Direction end) {
    final Shape view = initializeRoadSectionHalfView(roadSection, end);
    view.setStrokeWidth(BOARD_DISCOVERED_ROAD_WIDTH);
    view.getStrokeDashArray().setAll(1.0, 2.0);
    view.setVisible(true);
  }

  @Override
  public void onRoadSectionHalfInvisible(RoadSectionView roadSection, Direction end) {
    final Shape view = initializeRoadSectionHalfView(roadSection, end);
    view.setVisible(false);
  }

  private Polygon createWarriorShape() {
    final Polygon warrior = new Polygon(
        -5.0, 0.0,
        -Math.sqrt(2) / 2 * 5, Math.sqrt(2) / 2 * 5,
        0.0, 5.0,
        7.5, 0.0,
        0.0, -5.0,
        -Math.sqrt(2) / 2 * 5, -Math.sqrt(2) / 2 * 5);
    return warrior;
  }

  private Node createPrawnShape(@Nonnull PrawnView prawn, @Nonnegative double size) {
    final Group result = new Group();

    // Shape.
    final Shape shape;
    if (prawn.getType() == PrawnView.Type.WARRIOR) {
      shape = createWarriorShape();
      shape.getTransforms().add(new Rotate(prawn.getWarriorDirection() * 180 / Math.PI, 0.0, 0.0));
      shape.setScaleX(size / 5);
      shape.setScaleY(size / 5);
    } else {
      shape = new Circle(size);
    }
    shape.setFill(PLAYER_COLORS[prawn.getPlayer().getIndex()]);
    shape.setStroke(COMPLEMENTARY_PLAYER_COLORS[prawn.getPlayer().getIndex()]);
    shape.setStrokeWidth(0.2);
    result.getChildren().add(shape);

    // Energy label for own Prawns.
    if (prawn.getPlayer().equals(currentPlayer)) {
      Label energyLabel = new Label(Integer.toString((int) Math.ceil(10 * prawn.getEnergy())));
      energyLabel.setFont(new Font(size));
      energyLabel.setTextFill(COMPLEMENTARY_PLAYER_COLORS[prawn.getPlayer().getIndex()]);
      energyLabel.setTranslateX(-size / 3 * 2);
      energyLabel.setTranslateY(-size / 3 * 2);
      result.getChildren().add(energyLabel);
    }

    return result;
  }

  private Node getCachedPrawn(@Nonnull PrawnView prawn) {
    final Node prawnView = prawnToNodes.get(prawn);
    if (prawnView == null) {
      return null;
    }
    if (!(prawnView instanceof Group)) {
      throw new IllegalArgumentException("prawnView should be of type Group: " + prawnView);
    }
    final Group group = (Group) prawnView;
    for (Node node : group.getChildren()) {
      if (node instanceof Label) {
        final Label energyLabel = (Label) node;
        energyLabel.setText(Integer.toString((int) Math.ceil(10 * prawn.getEnergy())));
      }
    }
    return prawnView;
  }

  private Node initializePrawnView(PrawnView prawn) {
    Node node = getCachedPrawn(prawn);
    if (node == null) {
      node = createPrawnShape(prawn, BOARD_PRAWN_RADIUS);
      prawnToNodes.put(prawn, node);
      prawnLayer.getChildren().add(node);
    } else {
      prawnLayer.getChildren().remove(node);
      prawnLayer.getChildren().add(node);
    }
    return node;
  }

  @Override
  public void onPrawnVisible(PrawnView prawn) {
    final Node view = initializePrawnView(prawn);
    final Point2D.Double coordinates = prawn.getCurrentPosition().getCoordinatesPair();
    view.setTranslateX(coordinates.getX());
    view.setTranslateY(coordinates.getY());
    view.setVisible(true);
  }

  @Override
  public void onPrawnInvisible(PrawnView prawn) {
    final Node view = initializePrawnView(prawn);
    view.setVisible(false);
  }

  private void markPrawnShapeJustRemoved(@Nonnull Node prawnView) {
    if (!(prawnView instanceof Group)) {
      throw new IllegalArgumentException("prawnView should be of type Group: " + prawnView);
    }
    final Group group = (Group) prawnView;
    boolean marked = false;
    for (Node node : group.getChildren()) {
      if (node instanceof Shape) {
        final Shape shape = (Shape) node;
        shape.setStroke(Color.RED);
        shape.setStrokeWidth(2.0);
        marked = true;
      }
    }
    if (!marked) {
      throw new IllegalArgumentException("prawnView should contain a Prawn Shape: " + prawnView);
    }
  }

  @Override
  public void onPrawnJustRemoved(PrawnView prawn) {
    final Node view = initializePrawnView(prawn);
    markPrawnShapeJustRemoved(view);
    view.setVisible(true);
  }

  private Node createCityShape(@Nonnull CityView city, @Nonnegative double size) {
    final Group result = new Group();

    // Shape.
    final Shape shape = new Circle(size);
    shape.setFill(PLAYER_COLORS[city.getPlayer().getIndex()]);
    shape.setStroke(COMPLEMENTARY_PLAYER_COLORS[city.getPlayer().getIndex()]);
    shape.setStrokeWidth(0.2);
    result.getChildren().add(shape);

    // City label.
    {
      final Text shadowCityLabel = new Text(city.getName());
      shadowCityLabel.setFont(new Font(size / 3 * 2));
      shadowCityLabel.setFill(Color.BLACK);
      shadowCityLabel.setTranslateX(BOARD_CITY_RADIUS / Math.sqrt(2.0) + 1);
      shadowCityLabel.setTranslateY(-BOARD_CITY_RADIUS / Math.sqrt(2.0) - 1);
      result.getChildren().add(shadowCityLabel);
    }
    {
      final Text cityLabel = new Text(city.getName());
      cityLabel.setFont(new Font(size / 3 * 2));
      cityLabel.setFill(Color.WHITE);
      cityLabel.setTranslateX(BOARD_CITY_RADIUS / Math.sqrt(2.0) + 2);
      cityLabel.setTranslateY(-BOARD_CITY_RADIUS / Math.sqrt(2.0) - 2);
      result.getChildren().add(cityLabel);
    }

    // Size label for own Prawns.
    final Label sizeLabel = new Label();
    sizeLabel.setAlignment(Pos.CENTER);
    sizeLabel.setPrefWidth(2 * size);
    sizeLabel.setPrefHeight(2 * size);
    sizeLabel.setFont(new Font(size));
    sizeLabel.setTextFill(COMPLEMENTARY_PLAYER_COLORS[city.getPlayer().getIndex()]);
    if (city.getPlayer().equals(currentPlayer)) {
      sizeLabel.setText(Integer.toString(city.getFactories().size()));
    } else {
      sizeLabel.setText("");
    }
    sizeLabel.setTranslateX(-size);
    sizeLabel.setTranslateY(-size);
    result.getChildren().add(sizeLabel);

    return result;
  }

  private Node getCachedCity(@Nonnull CityView city) {
    final Node cityView = cityToImage.get(city);
    if (cityView == null) {
      return null;
    }
    if (!(cityView instanceof Group)) {
      throw new IllegalArgumentException("cityView should be of type Group: " + cityView);
    }
    final Group group = (Group) cityView;
    boolean circleFound = false;
    for (Node node : group.getChildren()) {
      if (node instanceof Label) {
        final Label sizeLabel = (Label) node;
        if (city.getPlayer().equals(currentPlayer)) {
          sizeLabel.setText(Integer.toString(city.getFactories().size()));
        } else {
          sizeLabel.setText("");
        }
      }
      if (node instanceof Circle) {
        final Circle circle = (Circle) node;
        circle.setFill(PLAYER_COLORS[city.getPlayer().getIndex()]);
        circleFound = true;
      }
    }
    if (!circleFound) {
      throw new IllegalArgumentException("circleView should contain a City Circle: " + cityView);
    }
    return cityView;
  }

  private Node initializeCityView(CityView city) {
    final Point2D.Double coordinates = city.getJoint().getCoordinatesPair();
    Node cityView = getCachedCity(city);
    if (cityView == null) {
      cityView = createCityShape(city, BOARD_CITY_RADIUS);
      cityToImage.put(city, cityView);
      cityLayer.getChildren().add(cityView);
    }
    cityView.setTranslateX(coordinates.getX());
    cityView.setTranslateY(coordinates.getY());
    return cityView;
  }

  @Override
  public void onCityDiscovered(CityView city) {
    final Node view = initializeCityView(city);
    view.setVisible(true);
  }

  @Override
  public void onCityInvisible(CityView city) {
    final Node view = initializeCityView(city);
    view.setVisible(false);
  }

  @Override
  public void onCityDestroyed(CityView city) {
    final Node view = initializeCityView(city);
    view.setVisible(false);
  }

  private Shape initializeBattleView(BattleView battle) {
    final Point2D.Double coordinates = battle.getPoint();
    Shape shape = battleToShape.get(battle);
    if (!battleToShape.containsKey(battle)) {
      SVGPath svgPath = new SVGPath();
      svgPath.setContent("M 2.9999999,4.3622046 0.03893056,2.6927056 -3.011745,4.1922174 "
          + "-2.3389778,0.86016953 -4.7078089,-1.5778207 -1.3309466,-1.9676405 0.25571084,-4.9739132 "
          + "1.6699594,-1.8827872 5.0193987,-1.3027757 2.51659,0.99746511 Z");
      shape = svgPath;
      shape.setScaleX(5.0);
      shape.setScaleY(5.0);
      shape.setTranslateX(coordinates.getX());
      shape.setTranslateY(coordinates.getY());
      battleToShape.put(battle, shape);
      shape.setFill(BATTLE_COLOR);
      shape.setStroke(Color.DARKRED);
      shape.setStrokeWidth(0.2);
      battleLayer.getChildren().add(shape);
    }
    return shape;
  }

  @Override
  public void onBattleVisible(BattleView battle) {
    final Shape view = initializeBattleView(battle);
    view.setVisible(true);
  }

  @Override
  public void onBattleInvisible(BattleView battle) {
    final Shape view = initializeBattleView(battle);
    view.setVisible(false);
  }

  @Override
  public void onMagnifiedCityDrawn(CityView city, Point2D.Double point) {
    final Point2D.Double translatedCoordinates = translateToMagnifiedCoordinates(point);
    if (inMagnificationBox(translatedCoordinates)) {
      final int translatedX = (int) translatedCoordinates.getX();
      final int translatedY = (int) translatedCoordinates.getY();
      final Node shape = createCityShape(city, magnifiedViewClickDistance);
      shape.setTranslateX(translatedX);
      shape.setTranslateY(translatedY);
      magnifyLayer.getChildren().add(shape);
      drawMagnifiedDistinctivePositionClickCircle(translatedX, translatedY);
    }
  }

  private Point2D.Double translateToMagnifiedCoordinates(@Nonnull Point2D.Double coordinates) {
    final double translatedX =
        (coordinates.getX() + CANVAS_PADDING) * magnificationRatio - magnificationTranslatedMinX + magnificationMinX;
    final double translatedY =
        (coordinates.getY() + CANVAS_PADDING) * magnificationRatio - magnificationTranslatedMinY + magnificationMinY;
    return new Point2D.Double(translatedX, translatedY);
  }

  private void drawMagnifiedDistinctivePositionClickCircle(int translatedX, int translatedY) {
    Circle circle = new Circle(magnifiedViewClickDistance);
    circle.setTranslateX(translatedX);
    circle.setTranslateY(translatedY);
    circle.setStroke(Color.BLACK);
    circle.setFill(Color.TRANSPARENT);
    circle.setStrokeWidth(BOARD_HIGHLIGHTED_LINE_WIDTH);
    magnifyLayer.getChildren().add(circle);
  }

  @Override
  public void onMagnifiedPrawnDrawn(PrawnView prawn, Point2D.Double point, int index) {
    final Point2D.Double translatedCoordinates = translateToMagnifiedCoordinates(point);
    if (inMagnificationBox(translatedCoordinates)) {
      final int translatedX = (int) translatedCoordinates.getX() + 2 * index;
      final int translatedY = (int) translatedCoordinates.getY() + 2 * index;

      final Node node = createPrawnShape(prawn, magnifiedViewClickDistance);
      node.setTranslateX(translatedX);
      node.setTranslateY(translatedY);
      magnifyLayer.getChildren().add(node);
      if (index == 0) {
        drawMagnifiedDistinctivePositionClickCircle(translatedX, translatedY);
      }
    }
  }

  @Override
  public void onMagnifiedPositionDrawn(PositionView position, Point2D.Double point) {
    final Point2D.Double translatedCoordinates = translateToMagnifiedCoordinates(point);
    if (inMagnificationBox(translatedCoordinates)) {
      final int translatedX = (int) translatedCoordinates.getX();
      final int translatedY = (int) translatedCoordinates.getY();
      drawMagnifiedDistinctivePositionClickCircle(translatedX, translatedY);
    }
  }

  @Override
  public void onCityCreationHighlighted(PositionView position, String cityName) {
    final Point2D.Double point = position.getCoordinatesPair();
    final Circle circle = new Circle(point.getX(), point.getY(), BOARD_CITY_RADIUS);
    circle.setStrokeWidth(BOARD_HIGHLIGHTED_LINE_WIDTH);
    circle.setStroke(HIGHLIGHT_COLOR);
    circle.setFill(Color.TRANSPARENT);
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        highlightLayer.getChildren().add(circle);
      }
    });
  }

  @Override
  public void onSelectableLineSectionHighlighted(PositionView beginning, PositionView end) {
    final Point2D.Double beginningPoint = beginning.getCoordinatesPair();
    final Point2D.Double endPoint = end.getCoordinatesPair();
    final Line line =
        new Line(beginningPoint.getX(), beginningPoint.getY(), endPoint.getX(), endPoint.getY());
    line.setStrokeWidth(BOARD_VISIBLE_ROAD_WIDTH);
    line.setStroke(HIGHLIGHT_COLOR);
    line.setStrokeLineCap(StrokeLineCap.BUTT);
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        highlightLayer.getChildren().add(line);
      }
    });
  }

  @Override
  public void onLineSectionHighlighted(PositionView beginning, PositionView end) {
    final Point2D.Double beginningPoint = beginning.getCoordinatesPair();
    final Point2D.Double endPoint = end.getCoordinatesPair();
    final Line line =
        new Line(beginningPoint.getX(), beginningPoint.getY(), endPoint.getX(), endPoint.getY());
    line.setStrokeWidth(BOARD_HIGHLIGHTED_LINE_WIDTH);
    line.setStroke(ITINERARY_COLOR);
    line.setStrokeLineCap(StrokeLineCap.BUTT);
    final Circle lineEnd = new Circle(endPoint.getX(), endPoint.getY(), ITINERARY_JOINT_RADIUS);
    lineEnd.setFill(ITINERARY_COLOR);
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        if (itineraryBuiltConfirmButton.isDisable()) {
          itineraryBuiltConfirmButton.setDisable(false);
        }
        highlightLayer.getChildren().addAll(line, lineEnd);
      }
    });
  }

  @Override
  public void onHighlightingRemoved() {
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        highlightLayer.getChildren().clear();
        floatingHighlightLayer.getChildren().clear();
      }
    });
  }

  @Override
  public void onMagnificationShown(Point2D.Double point) {
    final WritableImage wi = new WritableImage(magnificationWidth, magnificationHeight);
    final SnapshotParameters parameters = new SnapshotParameters();
    parameters.setFill(Color.WHITE);
    magnificationTranslatedMinX = magnificationRatio * (point.getX() + CANVAS_PADDING) - magnificationWidth / 2;
    magnificationTranslatedMinY = magnificationRatio * (point.getY() + CANVAS_PADDING) - magnificationHeight / 2;
    parameters.setViewport(new Rectangle2D(
        magnificationTranslatedMinX, magnificationTranslatedMinY,
        magnificationWidth * magnificationRatio, magnificationHeight * magnificationRatio));

    parameters.setTransform(Transform.scale(magnificationRatio, magnificationRatio));
    prawnLayer.setVisible(false);
    cityLayer.setVisible(false);
    battleLayer.setVisible(false);
    magnifyLayer.setVisible(false);
    scrolledBoard.snapshot(parameters, wi);
    prawnLayer.setVisible(true);
    cityLayer.setVisible(true);
    battleLayer.setVisible(true);
    magnifyLayer.setVisible(true);
    final ImageView wiView = new ImageView(wi);
    wiView.setTranslateX(magnificationMinX);
    wiView.setTranslateY(magnificationMinY);
    magnifyLayer.getChildren().add(wiView);
  }

  @Override
  public void onMagnificationHidden() {
    // Deliberately left empty. The magnification layer is closed on a State change.
  }

  @Override
  public void onPrawnSelectionShown() {
    final List<Rectangle> prawnSelectionBoxes = new LinkedList<>();
    for (int j = 0; j < prawnSelectionHeight; ++j) {
      for (int i = 0; i < prawnSelectionWidth; ++i) {
        final Rectangle prawnSelectionBox = new Rectangle(prawnSelectionMinX + i * prawnBoxSide,
            prawnSelectionMinY + j * prawnBoxSide, prawnBoxSide - 2, prawnBoxSide - 2);
        prawnSelectionBox.setFill(BOX_COLOR);
        prawnSelectionBoxes.add(prawnSelectionBox);
      }
    }
    prawnSelectionLayer.getChildren().setAll(prawnSelectionBoxes);
  }

  @Override
  public void onPrawnSelectionHidden() {
    prawnSelectionLayer.getChildren().clear();
  }

  @Override
  public void onPrawnSelectionPrawnDrawn(PrawnView prawn, Point2D.Double point) {
    final int translatedX = (int) point.getX() + prawnSelectionMinX;
    final int translatedY = (int) point.getY() + prawnSelectionMinY;

    final Node node = createPrawnShape(prawn, prawnBoxSide / 2.0 / 2.0);
    node.setTranslateX(translatedX);
    node.setTranslateY(translatedY);
    prawnSelectionLayer.getChildren().add(node);
  }

  @Override
  public void onPrawnSelectionLeftSlider(Point2D.Double point, int previousPrawnsCount) {
    // TODO Auto-generated method stub

  }

  @Override
  public void onPrawnSelectionRightSlider(Point2D.Double point, int nextPrawnsCount) {
    // TODO Auto-generated method stub

  }

  @Override
  public void onCityViewShown() {
    cityViewLayer.getChildren().clear();
  }

  @Override
  public void onCityViewHidden() {
    cityViewLayer.getChildren().clear();
  }

  private void drawCityFactoryBox(@Nonnegative int positionIndex) {
    final Rectangle cityFactoryViewBox = new Rectangle(cityViewFactoriesMinX,
        cityViewFactoriesMinY + positionIndex * cityViewFactoryBoxHeight, cityViewFactoryBoxWidth,
        cityViewFactoryBoxHeight - 2);
    cityFactoryViewBox.setFill(BOX_COLOR);
    cityViewLayer.getChildren().add(cityFactoryViewBox);
  }

  private void drawCityFactoryIconAndLabel(@Nonnull FactoryView factory, @Nonnegative int positionIndex) {
    final int labelX = cityViewFactoriesMinX + 10;
    final int labelY = cityViewFactoriesMinY + positionIndex * cityViewFactoryBoxHeight
        + cityViewFactoryBoxHeight - cityViewFactoryBoxHeight / 2 / 5 * 4;

    final Text label;
    if (factory.getState().equals(FactoryView.State.IN_ACTION)) {
      final int iconX = cityViewFactoryItemMinX + cityViewFactoryBoxHeight / 2;
      final int iconY =
          cityViewFactoriesMinY + positionIndex * cityViewFactoryBoxHeight
          + cityViewFactoryBoxHeight / 2;
      switch (factory.getActionType()) {
        case PRODUCE_WARRIOR: {
          label = new Text(textManager.getText("BUILD"));
          final Polygon warrior = createWarriorShape();
          try {
            warrior.getTransforms().add(new Rotate(factory.getWarriorDirection() * 180 / Math.PI, 0.0, 0.0));
          } catch (ClientViewException exception) {
            throw new RuntimeException(exception);
          }
          warrior.setScaleX(cityViewFactoryBoxHeight / 2.0 / 2.0 / 5.0);
          warrior.setScaleY(cityViewFactoryBoxHeight / 2.0 / 2.0 / 5.0);
          warrior.setFill(PLAYER_COLORS[factory.getCity().getPlayer().getIndex()]);
          warrior.setStroke(COMPLEMENTARY_PLAYER_COLORS[factory.getCity().getPlayer().getIndex()]);
          warrior.setStrokeWidth(0.2);
          warrior.setTranslateX(iconX);
          warrior.setTranslateY(iconY);
          cityViewLayer.getChildren().add(warrior);
          break;
        }
        case PRODUCE_SETTLERS_UNIT: {
          label = new Text(textManager.getText("BUILD"));
          Circle shape = new Circle(cityViewFactoryBoxHeight / 2.0 / 2.0);
          shape.setFill(PLAYER_COLORS[factory.getCity().getPlayer().getIndex()]);
          shape.setStroke(COMPLEMENTARY_PLAYER_COLORS[factory.getCity().getPlayer().getIndex()]);
          shape.setStrokeWidth(0.2);
          shape.setTranslateX(iconX);
          shape.setTranslateY(iconY);
          cityViewLayer.getChildren().add(shape);
          break;
        }
        case GROW: {
          label = new Text(textManager.getText("GROW"));
          break;
        }
        case REPAIR_PRAWN: {
          label = new Text(textManager.getText("REPAIR"));
          final PrawnView prawnRepaired;
          try {
            prawnRepaired = factory.getPrawnRepaired();
          } catch (ClientViewException exception) {
            throw new RuntimeException("Should never happen.", exception);
          }
          if (prawnRepaired.getType().equals(PrawnView.Type.WARRIOR)) {
            final Polygon warrior = createWarriorShape();
            warrior.getTransforms().add(new Rotate(prawnRepaired.getWarriorDirection() * 180 / Math.PI, 0.0, 0.0));
            warrior.setScaleX(cityViewFactoryBoxHeight / 2.0 / 2.0 / 5.0);
            warrior.setScaleY(cityViewFactoryBoxHeight / 2.0 / 2.0 / 5.0);
            warrior.setFill(PLAYER_COLORS[factory.getCity().getPlayer().getIndex()]);
            warrior.setStroke(COMPLEMENTARY_PLAYER_COLORS[factory.getCity().getPlayer().getIndex()]);
            warrior.setStrokeWidth(0.2);
            warrior.setTranslateX(iconX);
            warrior.setTranslateY(iconY);
            cityViewLayer.getChildren().add(warrior);
          } else {
            Circle shape = new Circle(cityViewFactoryBoxHeight / 2.0 / 2.0);
            shape.setFill(PLAYER_COLORS[factory.getCity().getPlayer().getIndex()]);
            shape.setStroke(COMPLEMENTARY_PLAYER_COLORS[factory.getCity().getPlayer().getIndex()]);
            shape.setStrokeWidth(0.2);
            shape.setTranslateX(iconX);
            shape.setTranslateY(iconY);
            cityViewLayer.getChildren().add(shape);
          }
          break;
        }
        default: {
          throw new RuntimeException("Unknown ActionType: " + factory.getActionType());
        }
      }
    } else {
      final ImageView warningImageView = new ImageView(warningImage);
      label = new Text(textManager.getText("IDLE FACTORY"));
      warningImageView.setFitWidth(cityViewFactoryBoxHeight - 5 - 5);
      warningImageView.setFitHeight(cityViewFactoryBoxHeight - 5 - 5);
      warningImageView.setTranslateX(
          cityViewFactoriesMinX + cityViewFactoryBoxWidth - (cityViewFactoryBoxHeight - 10) - 10);
      warningImageView.setTranslateY(cityViewFactoriesMinY + positionIndex * cityViewFactoryBoxHeight + 5 - 2);
      cityViewLayer.getChildren().add(warningImageView);
    }
    label.setFont(new Font(cityViewFactoryBoxHeight / 3));
    label.setFill(TEXT_COLOR);
    label.setTranslateX(labelX);
    label.setTranslateY(labelY);
    cityViewLayer.getChildren().add(label);
  }

  private void drawCityFactoryProgressBar(@Nonnull FactoryView factory, @Nonnegative int positionIndex) {
    final int progressBarMinX = cityViewFactoryStatusBarMinX + 3;
    final int progressBarY = cityViewFactoriesMinY + positionIndex * cityViewFactoryBoxHeight
        + cityViewFactoryBoxHeight / 2;
    final int progressBarWidth = cityViewFactoriesMinX + cityViewFactoryBoxWidth - progressBarMinX - 6;
    final double factoryProgress;
    try {
      factoryProgress = factory.getProgress();
    } catch (ClientViewException exception) {
      throw new RuntimeException(exception);
    }
    final Line progressElapsed = new Line(progressBarMinX, progressBarY,
        progressBarMinX + progressBarWidth * factoryProgress, progressBarY);
    progressElapsed.setStrokeWidth(4.0);
    progressElapsed.setStroke(BATTLE_COLOR);
    final Line progressRemaining = new Line(progressBarMinX + progressBarWidth * factoryProgress, progressBarY,
        progressBarMinX + progressBarWidth, progressBarY);
    progressRemaining.setStrokeWidth(4.0);
    progressRemaining.setStroke(Color.BLACK);
    if (progressElapsed.getEndX() - progressElapsed.getStartX() > 0) {
      cityViewLayer.getChildren().add(progressElapsed);
    }
    if (progressRemaining.getEndX() - progressRemaining.getStartX() > 0) {
      cityViewLayer.getChildren().add(progressRemaining);
    }
  }

  @Override
  public void onCityFactoryShown(FactoryView factory, int positionIndex) {
    drawCityFactoryBox(positionIndex);
    drawCityFactoryIconAndLabel(factory, positionIndex);
    if (factory.getState() == FactoryView.State.IN_ACTION) {
      drawCityFactoryProgressBar(factory, positionIndex);
    }
  }

  @Override
  public void onCityFactoryUpperSliderShown(int positionIndex) {
    // TODO Auto-generated method stub

  }

  @Override
  public void onCityFactoryLowerSliderShown(int positionIndex) {
    // TODO Auto-generated method stub

  }

  @Override
  public void onCityFactoryHighlighted(int positionIndex) {
    final Polyline polyline = new Polyline(
        cityViewFactoriesMinX, cityViewFactoriesMinY + positionIndex * cityViewFactoryBoxHeight,
        cityViewFactoriesMinX + cityViewFactoryBoxWidth,
          cityViewFactoriesMinY + positionIndex * cityViewFactoryBoxHeight,
        cityViewFactoriesMinX + cityViewFactoryBoxWidth,
          cityViewFactoriesMinY + (positionIndex + 1) * cityViewFactoryBoxHeight,
        cityViewFactoriesMinX, cityViewFactoriesMinY + (positionIndex + 1) * cityViewFactoryBoxHeight,
        cityViewFactoriesMinX, cityViewFactoriesMinY + positionIndex * cityViewFactoryBoxHeight);
    polyline.setStrokeWidth(BOARD_HIGHLIGHTED_LINE_WIDTH);
    polyline.setStroke(HIGHLIGHT_COLOR);
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        floatingHighlightLayer.getChildren().add(polyline);
      }
    });
  }

  @Override
  public void onCityPrawnSelectionShown() {
    final List<Rectangle> prawnSelectionBoxes = new LinkedList<>();
    for (int j = 0; j < cityViewPrawnSelectionHeight; ++j) {
      for (int i = 0; i < cityViewPrawnSelectionWidth; ++i) {
        final Rectangle prawnSelectionBox = new Rectangle(cityViewPrawnSelectionMinX + i * cityViewPrawnBoxSide,
            cityViewPrawnSelectionMinY + j * cityViewPrawnBoxSide, cityViewPrawnBoxSide - 2, cityViewPrawnBoxSide - 2);
        prawnSelectionBox.setFill(BOX_COLOR);
        prawnSelectionBoxes.add(prawnSelectionBox);
      }
    }
    cityViewLayer.getChildren().addAll(prawnSelectionBoxes);
  }

  @Override
  public void onCityPrawnSelectionPrawnDrawn(PrawnView prawn, Point2D.Double point) {
    final int translatedX = (int) point.getX() + cityViewPrawnSelectionMinX;
    final int translatedY = (int) point.getY() + cityViewPrawnSelectionMinY;

    final Node shape = createPrawnShape(prawn, cityViewPrawnBoxSide / 2.0 / 2.0);
    shape.setTranslateX(translatedX);
    shape.setTranslateY(translatedY);
    cityViewLayer.getChildren().add(shape);
  }

  @Override
  public void onCityPrawnSelectionLeftSlider(Point2D.Double point, int previousPrawnsCount) {
    // TODO Auto-generated method stub

  }

  @Override
  public void onCityPrawnSelectionRightSlider(Point2D.Double point, int nextPrawnsCount) {
    // TODO Auto-generated method stub

  }

  private void drawCityDescriptionBox() {
    final Rectangle cityDescriptionBox =
        new Rectangle(cityDescriptionBoxMinX, cityDescriptionBoxMinY, cityDescriptionBoxWidth - 2,
            cityDescriptionBoxHeight - 2);
    cityDescriptionBox.setFill(BOX_COLOR);
    cityViewLayer.getChildren().add(cityDescriptionBox);
  }

  private void drawCityModelBox(@Nonnull CityView city, @Nonnull int[] capturedHalves) {
    final Rectangle cityBox = new Rectangle(cityBoxMinX, cityBoxMinY, cityBoxSide - 2, cityBoxSide - 2);
    cityBox.setFill(BOX_COLOR);
    cityViewLayer.getChildren().add(cityBox);

    final double cityRadius = cityBoxSide / 2.0 / 2.0 / 2;
    final double lineLength = cityBoxSide / 2.0 - cityRadius - 10.0;
    for (int i = 0; i < city.getRoadSectionAngles().length; ++i) {
      final double roadSectionAngle = city.getRoadSectionAngles()[i];
      final double cos = Math.cos(roadSectionAngle);
      final double sin = Math.sin(roadSectionAngle);
      final Line line0 = new Line(cityBoxCenterX + cos * cityRadius, cityBoxCenterY + sin * cityRadius,
          cityBoxCenterX + cos * (cityRadius + lineLength / 2), cityBoxCenterY + sin * (cityRadius + lineLength / 2));
      final Line line1 = new Line(cityBoxCenterX + cos * (cityRadius + lineLength / 2),
          cityBoxCenterY + sin * (cityRadius + lineLength / 2), cityBoxCenterX + cos * (cityRadius + lineLength),
          cityBoxCenterY + sin * (cityRadius + lineLength));
      switch (capturedHalves[i]) {
        case 0:
          line0.setStrokeWidth(6.0);
          line1.setStrokeWidth(6.0);
          break;
        case 1:
          line0.setStrokeWidth(6.0);
          line1.setStrokeWidth(1.0);
          break;
        case 2:
          line0.setStrokeWidth(1.0);
          line1.setStrokeWidth(1.0);
          break;
        default:
          line0.setStrokeWidth(0.0);
          line1.setStrokeWidth(0.0);
          break;
      }
      cityViewLayer.getChildren().add(line0);
      cityViewLayer.getChildren().add(line1);
    }

    final Circle circle = new Circle(cityRadius);
    circle.setFill(PLAYER_COLORS[city.getPlayer().getIndex()]);
    circle.setStroke(COMPLEMENTARY_PLAYER_COLORS[city.getPlayer().getIndex()]);
    circle.setStrokeWidth(0.2);
    circle.setTranslateX(cityBoxCenterX);
    circle.setTranslateY(cityBoxCenterY);
    cityViewLayer.getChildren().add(circle);
  }

  private void drawCityDesctiptionText(@Nonnull CityView city, @Nonnegative double productivity) {
    final Text cityDescriptionText = new Text(city.getName() + "\n" + textManager.getText("Size") + ": "
        + city.getFactories().size() + "\n(" + textManager.getText("Max size") + ": "
        + city.getRoadSectionAngles().length + ")");
    cityDescriptionText.setFill(TEXT_COLOR);
    cityDescriptionText.setTranslateX(cityDescriptionBoxMinX + 10);
    cityDescriptionText.setTranslateY(cityDescriptionBoxMinY + 20);
    cityViewLayer.getChildren().add(cityDescriptionText);

    final Text productivityText = new Text(textManager.getText("Productivity") + ": "
        + (int) (productivity * 100) + "%");
    productivityText.setFill(TEXT_COLOR);
    productivityText.setTranslateX(cityBoxMinX + 10);
    productivityText.setTranslateY(cityBoxMinY + cityBoxSide - 10);
    cityViewLayer.getChildren().add(productivityText);
  }

  @Override
  public void onCityBoxDrawn(CityView city, int[] capturedHalves, double productivity) {
    drawCityDescriptionBox();
    drawCityModelBox(city, capturedHalves);
    drawCityDesctiptionText(city, productivity);
  }

  @Override
  public void onWarriorDirectionSelectionShown() {
    final Rectangle selectionBox = new Rectangle(warriorDirectionSelectionBoxMinX, warriorDirectionSelectionBoxMinY,
        warriorDirectionSelectionBoxSide, warriorDirectionSelectionBoxSide);
    selectionBox.setFill(BOX_COLOR);
    final Circle selectionCircle = new Circle(warriorDirectionSelectionBoxMinX + warriorDirectionSelectionBoxSide / 2,
        warriorDirectionSelectionBoxMinY + warriorDirectionSelectionBoxSide / 2, warriorDirectionSelectionBoxSide / 2);
    selectionCircle.setStroke(Color.color(0.9, 0.9, 0.9));
    selectionCircle.setStrokeWidth(0.7);
    selectionCircle.setFill(Color.TRANSPARENT);

    warriorToBeBuiltDirection = 0.0;
    warriorToBeBuilt = createWarriorShape();
    warriorToBeBuilt.getTransforms().add(new Rotate(warriorToBeBuiltDirection, 0.0, 0.0));
    warriorToBeBuilt.setScaleX(warriorDirectionSelectionBoxSide / 4.0 / 2.0 / 5.0);
    warriorToBeBuilt.setScaleY(warriorDirectionSelectionBoxSide / 4.0 / 2.0 / 5.0);
    warriorToBeBuilt.setFill(PLAYER_COLORS[inputManager.getCitySelected().getPlayer().getIndex()]);
    warriorToBeBuilt.setStroke(COMPLEMENTARY_PLAYER_COLORS[inputManager.getCitySelected().getPlayer().getIndex()]);
    warriorToBeBuilt.setStrokeWidth(0.2);
    warriorToBeBuilt.setTranslateX(warriorDirectionSelectionBoxMinX + warriorDirectionSelectionBoxSide / 2);
    warriorToBeBuilt.setTranslateY(warriorDirectionSelectionBoxMinY + warriorDirectionSelectionBoxSide / 2);

    warriorDirectionSelectionLayer.getChildren().addAll(selectionBox, selectionCircle, warriorToBeBuilt);
  }

  @Override
  public void onWarriorDirectionSelectionHidden() {
    warriorToBeBuilt = null;
    warriorDirectionSelectionLayer.getChildren().clear();
  }

  @Override
  public void onGameAbandoningPromptShown() {
    final Rectangle gameAbandoningPromptBox =
        new Rectangle(promptBoxMinX, promptBoxMinY, promptBoxWidth, promptBoxHeight);
    gameAbandoningPromptBox.setFill(BOX_COLOR);

    final Label promptText = new Label(textManager.getText("Are you sure you want to abandon the game?"));
    promptText.setTextFill(TEXT_COLOR);
    promptText.setFont(new Font("Helvetica", 15));
    promptText.setPrefWidth(promptBoxWidth);
    promptText.setPrefHeight(promptBoxHeight);
    promptText.setTranslateX(promptBoxMinX);
    promptText.setTranslateY(promptBoxMinY);
    promptText.setWrapText(true);
    promptText.setTextAlignment(TextAlignment.CENTER);

    promptLayer.getChildren().addAll(gameAbandoningPromptBox, promptText);
  }

  @Override
  public void onGameAbandoningPromptHidden() {
    promptLayer.getChildren().clear();
  }

  @Override
  public void onGameAbandoned() {
    promptLayer.getChildren().clear();
    boardNextRoundSemaphore.release();
  }

  @Override
  public void onGameForceQuit() {
    // NOTE: Not implemented.
  }

  @Override
  public void onPrawnRepairSelectionShown() {
    cityViewLayer.getChildren().addAll(prawnRepairPromptBox);
  }

  @Override
  public void onPrawnRepairSelectionHighlighted(int indexX, int indexY) {
    final Rectangle prawnSelectionHighlight = new Rectangle(cityViewPrawnSelectionMinX + indexX * cityViewPrawnBoxSide,
        cityViewPrawnSelectionMinY + indexY * cityViewPrawnBoxSide, cityViewPrawnBoxSide - 2, cityViewPrawnBoxSide - 2);
    prawnSelectionHighlight.setStrokeWidth(BOARD_HIGHLIGHTED_LINE_WIDTH);
    prawnSelectionHighlight.setStroke(HIGHLIGHT_COLOR);
    prawnSelectionHighlight.setFill(Color.TRANSPARENT);
    prawnSelectionHighlight.setBlendMode(BlendMode.DIFFERENCE);
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        floatingHighlightLayer.getChildren().add(prawnSelectionHighlight);
      }
    });
  }

  @Override
  public void onPrawnRepairSelectionHidden() {
    final int nodeCount = cityViewLayer.getChildren().size();
    cityViewLayer.getChildren().remove(nodeCount - 1);
  }

  private boolean inMagnificationBox(@Nonnull Point2D.Double coordinates) {
    if (coordinates.getX() + magnifiedViewClickDistance <= magnificationMinX) {
      return false;
    }
    if (coordinates.getX() - magnifiedViewClickDistance > magnificationMinX + magnificationWidth) {
      return false;
    }
    if (coordinates.getY() + magnifiedViewClickDistance <= magnificationMinY) {
      return false;
    }
    if (coordinates.getY() - magnifiedViewClickDistance > magnificationMinY + magnificationHeight) {
      return false;
    }
    return true;
  }

  private Point2D.Double translateFromMagnifiedCoordinates(@Nonnull Point2D.Double magnifiedCoordinates) {
    if (!inMagnificationBox(magnifiedCoordinates)) {
      return null;
    }
    final double pointX =
        (magnifiedCoordinates.getX() - magnificationMinX + magnificationTranslatedMinX) / magnificationRatio
            - CANVAS_PADDING;
    final double pointY =
        (magnifiedCoordinates.getY() - magnificationMinY + magnificationTranslatedMinY) / magnificationRatio
            - CANVAS_PADDING;
    return new Point2D.Double(pointX, pointY);
  }

  private void updateOwnCityWarning(@Nonnull CityView city) {
    final Node cityNode = getCachedCity(city);
    if (cityNode == null) {
      return;
    }
    final boolean anyFactoryIdle =
        city.getFactories().stream().anyMatch(a -> a.getState().equals(FactoryView.State.IDLE));
    final Group cityNodeGroup = (Group) cityNode;
    if (anyFactoryIdle) {
      if (cityNodeGroup.getChildren().stream().noneMatch(a -> a instanceof ImageView)) {
        final ImageView cityWarningImageView = new ImageView(warningImage);
        cityWarningImageView.setFitWidth(20);
        cityWarningImageView.setFitHeight(20);
        cityWarningImageView.setTranslateX(BOARD_CITY_RADIUS / Math.sqrt(2.0) + 2);
        cityWarningImageView.setTranslateY(BOARD_CITY_RADIUS / Math.sqrt(2.0) + 2);
        cityNodeGroup.getChildren().add(cityWarningImageView);
      }
    } else {
      cityNodeGroup.getChildren().removeAll(
          cityNodeGroup.getChildren().stream().filter(a -> a instanceof ImageView).collect(Collectors.toSet()));
    }
  }

  private void updateGlobalWarning() {
    final boolean anyFactoryIdle;
    if (inputManager != null && inputManager.getGame() != null) {
      anyFactoryIdle = inputManager.getGame().getPlayerCities(currentPlayer).stream().anyMatch(
          a -> a.getFactories().stream().anyMatch(b -> b.getState().equals(FactoryView.State.IDLE)));
    } else {
      anyFactoryIdle = false;
    }
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        statusBarGroup.getChildren().setAll(statusBar);
        if (!gameServerConnected) {
          final ImageView connectionBrokenImageView = new ImageView(connectionBrokenImage);
          connectionBrokenImageView.setFitWidth(STATUS_BAR_HEIGHT - 20);
          connectionBrokenImageView.setFitHeight(STATUS_BAR_HEIGHT - 20);
          connectionBrokenImageView.setTranslateX(viewportWidth - (STATUS_BAR_HEIGHT - 20) - 20);
          connectionBrokenImageView.setTranslateY(20);
          statusBarGroup.getChildren().add(connectionBrokenImageView);
        }
        if (!otherGameClientsConnected) {
          final ImageView connectionProblemImageView = new ImageView(connectionProblemImage);
          connectionProblemImageView.setFitWidth(STATUS_BAR_HEIGHT - 20);
          connectionProblemImageView.setFitHeight(STATUS_BAR_HEIGHT - 20);
          double translateX = viewportWidth - (STATUS_BAR_HEIGHT - 20) - 20;;
          if (!gameServerConnected) {
            translateX -= STATUS_BAR_HEIGHT;
          }
          connectionProblemImageView.setTranslateX(translateX);
          connectionProblemImageView.setTranslateY(20);
          statusBarGroup.getChildren().add(connectionProblemImageView);
        }
        if (anyFactoryIdle) {
          final ImageView warningImageView = new ImageView(warningImage);
          warningImageView.setFitWidth(STATUS_BAR_HEIGHT - 20);
          warningImageView.setFitHeight(STATUS_BAR_HEIGHT - 20);
          double translateX = viewportWidth - (STATUS_BAR_HEIGHT - 20) - 20;;
          if (!gameServerConnected) {
            translateX -= STATUS_BAR_HEIGHT;
          }
          if (!otherGameClientsConnected) {
            translateX -= STATUS_BAR_HEIGHT;
          }
          warningImageView.setTranslateX(translateX);
          warningImageView.setTranslateY(20);
          statusBarGroup.getChildren().add(warningImageView);
        }
        for (CityView city : inputManager.getGame().getPlayerCities(currentPlayer)) {
          updateOwnCityWarning(city);
        }
      }
    });
  }

  @Override
  public void onStateChange(State oldState) throws ClientViewException {
    switch (oldState) {
      case BOARD:
      case ITINERARY_BUILT: {
        scrolledBoard.setOnMouseClicked(null);
        break;
      }
      case PRAWN: {
        scrolledBoard.setOnMouseClicked(null);
        break;
      }
      case CITY: {
        cityViewLayer.setOnMouseClicked(null);
        break;
      }
      case FACTORY: {
        updateGlobalWarning();
        break;
      }
      case PRAWN_SELECTION: {
        prawnSelectionLayer.setOnMouseClicked(null);
        break;
      }
      case CITY_CREATION: {
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            cityViewLayer.getChildren().clear();
          }
        });
        break;
      }
      case MAGNIFIED: {
        magnifyLayer.setOnMouseClicked(null);
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            magnifyLayer.getChildren().clear();
          }
        });
        break;
      }
      case PRAWN_REPAIR: {
        cityViewLayer.setOnMouseClicked(null);
        break;
      }
      default: {
        // Just ignore.
        break;
      }
    }

    final State newState = inputManager.getState();
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        final String stateLabelText;
        switch (newState) {
          case PRAWN: {
            stateLabelText = textManager.getText(inputManager.getPrawnSelected().getType().name());
            break;
          }
          case CITY: {
            stateLabelText = textManager.getText(newState.name()) + " " + inputManager.getCitySelected().getName();
            break;
          }
          case GAME_FINISHED: {
            switch (currentPlayer.getState()) {
              case ABANDONED:
              case LOST: {
                stateLabelText = textManager.getText("Game lost");
                break;
              }
              case WON: {
                stateLabelText = textManager.getText("Game won");
                break;
              }
              case UNDECIDED: {
                stateLabelText = textManager.getText("Game finished with a draw");
                break;
              }
              default: {
                throw new IllegalStateException("Game finished. Player in illegal state: " + currentPlayer.getState());
              }
            }
            break;
          }
          default: {
            stateLabelText = textManager.getText(newState.name());
            break;
          }
        }
        stateLabel.setText(stateLabelText + "\n");
        statusBarScrollPane.setVvalue(0.0);
      }
    });
    switch (newState) {
      case INITIALIZED: {
        // Visualize controls.
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            controlPanel.getChildren().clear();
          }
        });
        break;
      }
      case BOARD: {
        // Visualize controls.
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            controlPanel.getChildren().setAll(boardPlayButton, boardAbandonGameButton);
            for (Node child : controlPanel.getChildren()) {
              FlowPane.setMargin(child, BUTTON_MARGINS);
            }
          }
        });
        updateGlobalWarning();

        // Set event handlers.
        scrolledBoard.setOnMouseClicked(new EventHandler<MouseEvent>() {
          @Override
          public void handle(MouseEvent event) {
            inputManager.onBoardPointSelected(new Point2D.Double(event.getX(), event.getY()));
          }
        });

        // Initialize scrollPane
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            if (!scrollPaneInitialized) {
              final Point2D.Double scrolledBoardBarInitialSetup = gameManager.getGameClient().getGame()
                  .calculateScrolledBoardBarInitialSetup(viewportWidth, viewportHeight);
              scrollPane.setHvalue(scrolledBoardBarInitialSetup.getX());
              scrollPane.setVvalue(scrolledBoardBarInitialSetup.getY());
              scrollPaneInitialized = true;
            }
          }
        });

        // Wait for the user.
        if (oldState == InputManager.State.INITIALIZED) {
          try {
            boardNextRoundSemaphore.acquire();
          } catch (InterruptedException exception) {
            throw new ClientViewException("Acquiring boardNextRoundSemaphore failed.", exception);
          }
        }
        break;
      }
      case PRAWN: {
        // Visualize controls.
        final PrawnView prawn = inputManager.getPrawnSelected();
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            final List<Node> prawnControls = new LinkedList<>(Arrays.asList(prawnClosePrawnButton,
                prawnClearItineraryButton, prawnBuildCityButton));
            if (!prawn.getType().equals(PrawnView.Type.SETTLERS_UNIT)) {
              prawnControls.remove(prawnBuildCityButton);
            } else {
              prawnBuildCityButton.setDisable(prawn.getCurrentPosition().getJoint() == null);
            }
            controlPanel.getChildren().setAll(prawnControls);
            for (Node child : controlPanel.getChildren()) {
              FlowPane.setMargin(child, BUTTON_MARGINS);
            }
          }
        });

        // Set event handlers.
        scrolledBoard.setOnMouseClicked(new EventHandler<MouseEvent>() {
          @Override
          public void handle(MouseEvent event) {
            inputManager.onPrawnItineraryBuildProgress(new Point2D.Double(event.getX(), event.getY()));
          }
        });

        inputManager.onPrawnSelected();
        break;
      }
      case ITINERARY_BUILT: {
        // Visualize controls.
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            itineraryBuiltConfirmButton.setDisable(true);
            controlPanel.getChildren().setAll(itineraryBuiltConfirmButton, itineraryBuiltAbandonButton);
            for (Node child : controlPanel.getChildren()) {
              FlowPane.setMargin(child, BUTTON_MARGINS);
            }
          }
        });

        // Set event handlers.
        scrolledBoard.setOnMouseClicked(new EventHandler<MouseEvent>() {
          @Override
          public void handle(MouseEvent event) {
            inputManager.onPrawnItineraryBuildProgress(new Point2D.Double(event.getX(), event.getY()));
          }
        });

        // Close the magnification view.
        if (oldState == InputManager.State.MAGNIFIED) {
          inputManager.onPrawnItineraryBuildResumed();
        }
        break;
      }
      case MAGNIFIED: {
        // Visualize controls.
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            controlPanel.getChildren().setAll(magnificationCloseButton);
            for (Node child : controlPanel.getChildren()) {
              FlowPane.setMargin(child, BUTTON_MARGINS);
            }
          }
        });

        // Set event handlers.
        magnifyLayer.setOnMouseClicked(new EventHandler<MouseEvent>() {
          @Override
          public void handle(MouseEvent event) {
            final Point2D.Double pointSelected =
                translateFromMagnifiedCoordinates(new Point2D.Double(event.getX(), event.getY()));
            if (pointSelected != null) {
              inputManager.onMagnifiedPointSelected(pointSelected);
            }
          }
        });
        break;
      }
      case CITY_CREATION: {
        // Visualize controls.
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            cityCreationNameField.setText(inputManager.getPrawnSelected().getCurrentPosition().getJoint().getName());
            cityViewLayer.getChildren().addAll(cityCreationBox, cityCreationNameLabel, cityCreationNameField);
            controlPanel.getChildren().setAll(cityCreationConfirmButton, cityCreationAbandonButton);
            for (Node child : controlPanel.getChildren()) {
              FlowPane.setMargin(child, BUTTON_MARGINS);
            }
            cityCreationNameField.requestFocus();
          }
        });
        break;
      }
      case CITY: {
        // Visualize controls.
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            controlPanel.getChildren().setAll(cityViewCloseButton);
            for (Node child : controlPanel.getChildren()) {
              FlowPane.setMargin(child, BUTTON_MARGINS);
            }
          }
        });

        // Set event handlers.
        cityViewLayer.setOnMouseClicked(new EventHandler<MouseEvent>() {
          @Override
          public void handle(MouseEvent event) {
            Point2D.Double pointSelected = new Point2D.Double(event.getX() - cityViewPrawnSelectionMinX,
                event.getY() - cityViewPrawnSelectionMinY);
            if (0 <= pointSelected.getX() && pointSelected.getX() < cityViewPrawnSelectionBoxWidth
                && 0 <= pointSelected.getY() && pointSelected.getY() < cityViewPrawnSelectionBoxHeight) {
              inputManager.onCityPrawnSelectionPointSelected(pointSelected);
            } else {
              pointSelected =
                  new Point2D.Double(event.getX() - cityViewFactoriesMinX, event.getY() - cityViewFactoriesMinY);
              if (0 <= pointSelected.getX() && pointSelected.getX() < cityViewFactoryBoxWidth
                  && 0 <= pointSelected.getY()
                  && pointSelected.getY() < cityViewFactoryBoxHeight * cityViewFactoryBoxMaxCount) {
                inputManager.onCityFactoryPointSelected(pointSelected);
              }
            }
          }
        });
        break;
      }
      case FACTORY: {
        // Set event handlers.
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            controlPanel.getChildren().setAll(factoryCloseCityButton, factorySetProduceWarriorButton,
                factorySetProduceSettlersUnitButton, factorySetRepairPrawnButton, factorySetGrowCityButton);
            for (Node child : controlPanel.getChildren()) {
              FlowPane.setMargin(child, BUTTON_MARGINS);
            }
            final CityView citySelected = inputManager.getCitySelected();
            factorySetGrowCityButton.setDisable(
                citySelected.getJoint().getRoadSections().size() <= citySelected.getFactories().size());
            factorySetRepairPrawnButton.setDisable(
                citySelected.getJoint().getNormalizedPosition().getPrawns().size() == 0);
          }
        });
        break;
      }
      case WARRIOR_DIRECTION: {
        // Visualize controls.
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            warriorDirectionConfirmButton.setDisable(true);
            controlPanel.getChildren().setAll(warriorDirectionConfirmButton, warriorDirectionAbandonButton);
            for (Node child : controlPanel.getChildren()) {
              FlowPane.setMargin(child, BUTTON_MARGINS);
            }
          }
        });

        // Set event handlers.
        warriorDirectionSelectionLayer.setOnMouseClicked(new EventHandler<MouseEvent>() {
          @Override
          public void handle(MouseEvent event) {
            if (warriorToBeBuilt != null) {
              final Point2D.Double pointSelected = new Point2D.Double(event.getX() - warriorDirectionSelectionBoxMinX,
                  event.getY() - warriorDirectionSelectionBoxMinY);
              final Point2D.Double warriorSelectionCenter =
                  new Point2D.Double(warriorDirectionSelectionBoxSide / 2, warriorDirectionSelectionBoxSide / 2);
              if (warriorSelectionCenter.distance(pointSelected) < warriorDirectionSelectionBoxSide / 2
                  && warriorSelectionCenter.distance(pointSelected) > 2) {
                final double vectorAtan2 = Math.atan2(pointSelected.getY() - warriorSelectionCenter.getY(),
                    pointSelected.getX() - warriorSelectionCenter.getX());
                if (vectorAtan2 >= 0.0) {
                  warriorToBeBuiltDirection = vectorAtan2;
                } else {
                  warriorToBeBuiltDirection = vectorAtan2 + 2.0 * Math.PI;
                }
                warriorToBeBuilt.getTransforms().setAll(
                    new Rotate(warriorToBeBuiltDirection * 180 / Math.PI, 0.0, 0.0));
                warriorDirectionConfirmButton.setDisable(false);
              }
            }
          }
        });
        break;
      }
      case PRAWN_SELECTION: {
        // Visualize controls.
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            controlPanel.getChildren().setAll(prawnSelectionCloseButton);
            for (Node child : controlPanel.getChildren()) {
              FlowPane.setMargin(child, BUTTON_MARGINS);
            }
          }
        });

        // Set event handlers.
        prawnSelectionLayer.setOnMouseClicked(new EventHandler<MouseEvent>() {
          @Override
          public void handle(MouseEvent event) {
            final Point2D.Double pointSelected =
                new Point2D.Double(event.getX() - prawnSelectionMinX, event.getY() - prawnSelectionMinY);
            if (pointSelected.getX() < prawnSelectionBoxWidth && pointSelected.getY() < prawnSelectionMinY) {
              inputManager.onPrawnSelectionPointSelected(pointSelected);
            }
          }
        });
        break;
      }
      case PRAWN_REPAIR: {
        // Visualize controls.
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            controlPanel.getChildren().setAll(prawnRepairConfirmButton, prawnRepairAbandonButton);
            for (Node child : controlPanel.getChildren()) {
              FlowPane.setMargin(child, BUTTON_MARGINS);
            }
          }
        });

        // Set event handler.
        cityViewLayer.setOnMouseClicked(new EventHandler<MouseEvent>() {
          @Override
          public void handle(MouseEvent event) {
            Point2D.Double pointSelected = new Point2D.Double(event.getX() - cityViewPrawnSelectionMinX,
                event.getY() - cityViewPrawnSelectionMinY);
            if (0 <= pointSelected.getX() && pointSelected.getX() < cityViewPrawnSelectionBoxWidth
                && 0 <= pointSelected.getY() && pointSelected.getY() < cityViewPrawnSelectionBoxHeight) {
              inputManager.onPrawnRepairSelectionPointSelected(pointSelected);
            }
          }
        });
        break;
      }
      case GAME_ABANDONING: {
        // Visualize controls.
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            controlPanel.getChildren().setAll(gameAbandoningConfirmButton, gameAbandoningCancelButton);
            for (Node child : controlPanel.getChildren()) {
              FlowPane.setMargin(child, BUTTON_MARGINS);
            }
          }
        });
        break;
      }
      case GAME_FINISHED: {
        // Visualize controls.
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            controlPanel.getChildren().setAll(gameFinishedCloseButton);
            for (Node child : controlPanel.getChildren()) {
              FlowPane.setMargin(child, BUTTON_MARGINS);
            }
          }
        });
        break;
      }

      default:
        // TODO(najtmar): Finish the implementation.
        System.err.println("Unsupported transition: " + oldState + " -> " + newState);
        break;
    }

    // TODO(najtmar): Finish the implementation.
  }

  @Override
  public void setGameManager(GameManager gameManager) {
    this.gameManager = gameManager;
  }

  @Override
  public void notifyOwnGameServerConnection(boolean connected) throws ClientViewException {
    this.gameServerConnected = connected;
    updateGlobalWarning();
  }

  @Override
  public void notifyOthersGameServerConnection(boolean connected) throws ClientViewException {
    this.otherGameClientsConnected = connected;
    updateGlobalWarning();
  }

  @Override
  public void notifyGameUndecided() throws ClientViewException {
    // NOTE: Not implemented.
  }

  @Deprecated
  @Override
  public Event[] interact(Event[] inputEvents) throws ClientViewException {
    return null;
  }

}
