package cz.wie.najtmar.gop.client.ui;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import com.esotericsoftware.minlog.Log;

import cz.wie.najtmar.gop.client.ui.GameManager.Action;
import cz.wie.najtmar.gop.client.ui.GameManager.GameInfo;
import cz.wie.najtmar.gop.client.ui.GameManager.State;
import cz.wie.najtmar.gop.client.view.ClientViewException;
import cz.wie.najtmar.gop.client.view.ClientViewProperties;
import cz.wie.najtmar.gop.client.view.TextManager;
import cz.wie.najtmar.gop.game.PropertiesReader;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Nonnull;

@Singleton
public class JavaFxGameManagementEngine implements GameManagementEngine {

  // Styles.
  public static String LARGE_BUTTON_STYLE =
      "#ipad-grey { "
      + "-fx-background-color: "
      + "   linear-gradient(#686868 0%, #232723 25%, #373837 75%, #757575 100%), "
      + "   linear-gradient(#020b02, #3a3a3a), "
      + "   linear-gradient(#b9b9b9 0%, #c2c2c2 20%, #afafaf 80%, #c8c8c8 100%), "
      + "   linear-gradient(#f5f5f5 0%, #dbdbdb 50%, #cacaca 51%, #d7d7d7 100%); "
      + "-fx-background-insets: 0,1,4,5; "
      + "-fx-background-radius: 9,8,5,4; "
      + "-fx-padding: 15 30 15 30; "
      + "-fx-font-family: \"Helvetica\"; "
      + "-fx-font-size: 18px; "
      + "-fx-font-weight: bold; "
      + "-fx-text-fill: #333333; "
      + "-fx-effect: dropshadow( three-pass-box , rgba(255,255,255,0.2) , 1, 0.0 , 0 , 1); "
      + "} ";

  public static String DARK_TEXT_STYLE =
      "#dark-grey-text { "
      + "-fx-font-family: \"Helvetica\"; "
      + "-fx-font-size: 16px; "
      + "-fx-text-fill: #101010; "
      + "} ";

  public static Insets BUTTON_MARGINS = new Insets(2.0,  2.0,  2.0, 2.0);

  /**
   * The GameManager used.
   */
  private GameManager gameManager;

  /**
   * The main JavaFX Stage of the UI.
   */
  private Stage primaryStage;

  /**
   * The main scene for the application.
   */
  private Scene primaryScene;

  /**
   * The main Pane for the application.
   */
  private FlowPane primaryPane;

  /**
   * Background image for the primaryPane .
   */
  private ImageView backgroundImage;

  /**
   * The name of the game (GoP).
   */
  private Text gopLabel;
  private FlowPane gopLabelPane;

  /**
   * Used to read Properties .
   */
  private final PropertiesReader propertiesReader;

  private ClientViewProperties clientViewProperties;

  /**
   * Manages text localization.
   */
  private final TextManager textManager;

  // State data.
  private int selectedGameListed;

  // Layers.
  private VBox gameManagementLayer;

  // Controls.
  private Button initialMenuInitGame;
  private Button initialMenuJoinGame;
  private Button initialMenuExit;
  //
  private Label gameInitiatedParticipantsLabel;
  private ListView<String> gameInitiatedPlayerList;
  private ObservableList<String> gameInitiatedObservablePlayerList;
  private Button gameInitiatedStart;
  private Button gameInitiatedCancel;
  private HBox gameInitiatedButtonPanel;
  //
  private Label gamesListedLabel;
  private ListView<GameInfo> gamesListedGameList;
  private ObservableList<GameInfo> gamesListedObservableGameList;
  private Button gamesListedJoin;
  private Button gamesListedRefresh;
  private Button gamesListedCancel;
  private VBox gamesListedButtonPanel;
  //
  private Label gameJoinedLabel;
  private VBox gameJoinedInfoBox;
  private Button gameJoinedCancel;

  /**
   * Creates a large Button .
   * @param nameNotLocalized the name of the Button, to be localized using textManager
   * @return a large Button with a given name localized
   */
  private Button createLargeButton(@Nonnull String nameNotLocalized) {
    final Button result = new Button(textManager.getText(nameNotLocalized));
    result.setStyle(LARGE_BUTTON_STYLE);
    return result;
  }

  /**
   * Constructor.
   * @param primaryStage the main JavaFX Stage of the UI
   * @param propertiesReader used to read Properties
   * @param textManager manages text localization
   */
  @Inject
  JavaFxGameManagementEngine(Stage primaryStage, PropertiesReader propertiesReader, TextManager textManager) {
    this.primaryStage = primaryStage;
    this.propertiesReader = propertiesReader;
    this.textManager = textManager;
  }

  /**
   * Sets EventHandlers for UI elements (Buttons, etc.).
   */
  private void setEventHandlers() {
    initialMenuInitGame.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        try {
          gameManager.initiateGameIntent();
        } catch (ClientViewException exception) {
          throw new RuntimeException(exception);
        }
      }
    });
    initialMenuJoinGame.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        try {
          gameManager.getPendingGames();
        } catch (ClientViewException exception) {
          throw new RuntimeException(exception);
        }
      }
    });
    initialMenuExit.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        System.exit(0);
      }
    });
    gameInitiatedStart.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        try {
          gameManager.initializeGame();
        } catch (ClientViewException exception) {
          throw new RuntimeException(exception);
        }
      }
    });
    gameInitiatedCancel.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        try {
          gameManager.cancelGameIntent();
        } catch (ClientViewException exception) {
          throw new RuntimeException(exception);
        }
      }
    });
    gamesListedGameList.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
      @Override
      public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
        selectedGameListed = newValue.intValue();
        gamesListedJoin.setDisable(false);
      }
    });
    gamesListedJoin.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        try {
          gameManager.joinPendingGame(selectedGameListed);
        } catch (ClientViewException exception) {
          throw new RuntimeException(exception);
        }
      }
    });
    gamesListedRefresh.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            gamesListedObservableGameList.clear();
            gamesListedGameList.setItems(gamesListedObservableGameList);
          }
        });
        try {
          gameManager.refreshPendingGamesList();
        } catch (ClientViewException exception) {
          throw new RuntimeException(exception);
        }
      }
    });
    gamesListedCancel.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        try {
          gameManager.leaveGamesListed();
        } catch (ClientViewException exception) {
          throw new RuntimeException(exception);
        }
      }
    });
    gameJoinedCancel.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        try {
          gameManager.leaveGameIntent();
          gamesListedJoin.setDisable(true);
        } catch (ClientViewException exception) {
          throw new RuntimeException(exception);
        }
      }
    });
  }

  @Override
  public void initializeGameManagementEngine(GameManager gameManager) throws ClientViewException {
    this.gameManager = gameManager;
    try {
      clientViewProperties = new ClientViewProperties(propertiesReader.getClientViewProperties());
    } catch (IOException exception) {
      throw new ClientViewException("ClientViewProperties could not be read.", exception);
    }

    primaryPane = new FlowPane();
    gameManagementLayer = new VBox();

    final ImageView backgroundImageFull = new ImageView(new Image("/images/gop-background.jpg"));
    final WritableImage backgroundImageWritable =
        new WritableImage(clientViewProperties.getMainViewWidth(),
            (int) (clientViewProperties.getMainViewHeight()
                + JavaFxEngine.STATUS_BAR_HEIGHT + JavaFxEngine.CONTROL_PANEL_HEIGHT));
    final SnapshotParameters backgroundImageParameters = new SnapshotParameters();
    backgroundImageParameters.setViewport(new Rectangle2D(100, 100, clientViewProperties.getMainViewWidth(),
        clientViewProperties.getMainViewHeight() + JavaFxEngine.STATUS_BAR_HEIGHT + JavaFxEngine.CONTROL_PANEL_HEIGHT));
    backgroundImageFull.snapshot(backgroundImageParameters, backgroundImageWritable);
    backgroundImage = new ImageView(backgroundImageWritable);
    backgroundImage.setEffect(new GaussianBlur());

    gopLabel = new Text("Game\nof\nPrawns");
    gopLabel.setFont(Font.font("Helvetica", FontWeight.BOLD, 55));
    gopLabel.setTextAlignment(TextAlignment.LEFT);
    gopLabel.setFill(JavaFxEngine.TEXT_COLOR);
    gopLabel.setStrokeWidth(1.0);
    gopLabel.setStroke(JavaFxEngine.BOX_COLOR);
    gopLabel.setEffect(new GaussianBlur(3.0));
    gopLabelPane = new FlowPane(gopLabel);
    gopLabelPane.setPrefWidth(clientViewProperties.getMainViewWidth());
    gopLabelPane.setPrefHeight(clientViewProperties.getMainViewHeight() + JavaFxEngine.STATUS_BAR_HEIGHT
        + JavaFxEngine.CONTROL_PANEL_HEIGHT);
    FlowPane.setMargin(gopLabel, new Insets(clientViewProperties.getMainViewWidth() / 10.0,
        clientViewProperties.getMainViewWidth() / 6.0, 0.0, clientViewProperties.getMainViewWidth() / 6.0));

    initialMenuInitGame = createLargeButton("Init game");
    initialMenuJoinGame = createLargeButton("Join game");
    initialMenuExit = createLargeButton("Exit");

    gameInitiatedStart = createLargeButton("Start");
    gameInitiatedCancel = createLargeButton("Cancel");
    gameInitiatedButtonPanel = new HBox(gameInitiatedStart, gameInitiatedCancel);
    for (Node child : gameInitiatedButtonPanel.getChildren()) {
      HBox.setMargin(child, BUTTON_MARGINS);
    }
    gameInitiatedParticipantsLabel = new Label("Participants");
    gameInitiatedParticipantsLabel.setStyle(DARK_TEXT_STYLE);

    gamesListedObservableGameList = FXCollections.observableArrayList();
    gamesListedGameList = new ListView<>(gamesListedObservableGameList);
    gamesListedGameList.setStyle(DARK_TEXT_STYLE);
    gamesListedGameList.setPrefHeight(clientViewProperties.getMainViewHeight() / 4.0);
    gamesListedJoin = createLargeButton("Join");
    gamesListedRefresh = createLargeButton("Refresh");
    gamesListedCancel = createLargeButton("Cancel");
    gamesListedButtonPanel = new VBox(gamesListedJoin, gamesListedRefresh, gamesListedCancel);
    for (Node child : gamesListedButtonPanel.getChildren()) {
      VBox.setMargin(child, BUTTON_MARGINS);
    }
    gamesListedLabel = new Label("Available games");
    gamesListedLabel.setStyle(DARK_TEXT_STYLE);

    gameInitiatedObservablePlayerList = FXCollections.observableArrayList();
    gameInitiatedPlayerList = new ListView<>(gameInitiatedObservablePlayerList);
    gameInitiatedPlayerList.setStyle(DARK_TEXT_STYLE);
    gameInitiatedPlayerList.setPrefHeight(clientViewProperties.getMainViewHeight() / 4.0);
    gameJoinedLabel = new Label("Waiting for the game to start ...");
    gameJoinedLabel.setStyle(DARK_TEXT_STYLE);
    gameJoinedInfoBox = new VBox(gameJoinedLabel);
    gameJoinedCancel = createLargeButton("Cancel");

    primaryScene = new Scene(new Group(backgroundImage, gopLabelPane, primaryPane));

    primaryStage.setTitle(textManager.getText("Game of Prawns"));
    primaryStage.setScene(primaryScene);

    primaryPane.setPrefWidth(clientViewProperties.getMainViewWidth());
    primaryPane.setPrefHeight(clientViewProperties.getMainViewHeight() + JavaFxEngine.STATUS_BAR_HEIGHT
        + JavaFxEngine.CONTROL_PANEL_HEIGHT);
    primaryPane.setMinHeight(primaryPane.getPrefHeight());
    primaryPane.getChildren().setAll(gameManagementLayer);
    primaryPane.setAlignment(Pos.BASELINE_CENTER);

    gameInitiatedButtonPanel.setAlignment(Pos.CENTER);
    gamesListedButtonPanel.setAlignment(Pos.CENTER);

    gameManagementLayer.setAlignment(Pos.BOTTOM_CENTER);
    gameManagementLayer.setPrefWidth(clientViewProperties.getMainViewWidth() / 3.0 * 2);
    gameManagementLayer.setPrefHeight(clientViewProperties.getMainViewHeight());
    gameManagementLayer.setMinHeight(gameManagementLayer.getPrefHeight());

    primaryStage.sizeToScene();

    VBox.setMargin(gamesListedGameList, new Insets(100, 0, 0, 0));

    gameJoinedInfoBox.setPrefHeight(clientViewProperties.getMainViewHeight() / 2.0);
    gameJoinedInfoBox.setAlignment(Pos.CENTER);

    setEventHandlers();
  }

  /**
   * Initializes the INITIAL_MENU view.
   */
  private void initializeInitialMenu() {
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        gameManagementLayer.getChildren().setAll(initialMenuInitGame, initialMenuJoinGame, initialMenuExit);
        for (Node child : gameManagementLayer.getChildren()) {
          VBox.setMargin(child, BUTTON_MARGINS);
        }
      }
    });
  }

  /**
   * Initializes the GAME_INITIATED view.
   */
  private void initializeGameInitiated() {
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        gameInitiatedObservablePlayerList.setAll(Arrays.asList(gameManager.getUser().getName()));
        gameInitiatedPlayerList.setItems(gameInitiatedObservablePlayerList);
        gameInitiatedStart.setDisable(true);
        gameManagementLayer.getChildren().setAll(gameInitiatedParticipantsLabel, gameInitiatedPlayerList,
            gameInitiatedButtonPanel);
        for (Node child : gameManagementLayer.getChildren()) {
          VBox.setMargin(child, BUTTON_MARGINS);
        }
      }
    });
  }

  /**
   * Initializes the GAMES_LISTED view.
   */
  private void initializeGamesListed() {
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        selectedGameListed = -1;
        gamesListedJoin.setDisable(true);
        gameManagementLayer.getChildren().setAll(gamesListedLabel, gamesListedGameList, gamesListedButtonPanel);
        for (Node child : gameManagementLayer.getChildren()) {
          if (!child.equals(gamesListedButtonPanel)) {
            VBox.setMargin(child, BUTTON_MARGINS);
          }
        }
      }
    });
  }

  /**
   * Initializes the GAME_JOINED view.
   */
  private void initializeGameJoined() {
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        gameManagementLayer.getChildren().setAll(gameJoinedInfoBox, gameJoinedCancel);
        for (Node child : gameManagementLayer.getChildren()) {
          VBox.setMargin(child, BUTTON_MARGINS);
        }
      }
    });
  }

  /**
   * Initializes the Game view.
   */
  private void initializeGameStarted() {
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        gameManagementLayer.getChildren().setAll();
      }
    });
  }

  @Override
  public void onGameManagerStateChange(State oldState) throws ClientViewException {
    switch (gameManager.getState()) {
      case INITIAL_MENU: {
        initializeInitialMenu();
        break;
      }
      case GAME_INITIATED: {
        initializeGameInitiated();
        break;
      }
      case GAMES_LISTED: {
        initializeGamesListed();
        break;
      }
      case GAME_JOINED: {
        initializeGameJoined();
        break;
      }
      case OWN_GAME_STARTED:
      case JOINED_GAME_STARTED: {
        initializeGameStarted();
        break;
      }
      default: {
        Log.warn("Unhandled State transition " + oldState + " -> " + gameManager.getState());
        break;
      }
    }
  }

  @Override
  public void updatePendingGamesList() {
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        gamesListedObservableGameList.setAll(gameManager.getPendingGamesList());
        gamesListedJoin.setDisable(true);
      }
    });
  }

  @Override
  public void setOwnGameEnabled(boolean isEnabled, List<String> participants) {
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        gameInitiatedObservablePlayerList.setAll(participants);
        gameInitiatedPlayerList.setItems(gameInitiatedObservablePlayerList);
        gameInitiatedStart.setDisable(!isEnabled);
      }
    });
  }

  @Override
  public void notifyActionNotFinished(Action action) {
    try {
      gameManager.reset(false);
    } catch (ClientViewException exception) {
      throw new RuntimeException(exception);
    }
  }

}
