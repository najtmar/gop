package cz.wie.najtmar.gop.server.gop;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.gson.JsonObject;
import cz.wie.najtmar.gop.DefaultPropertiesReader;
import cz.wie.najtmar.gop.board.BoardException;
import cz.wie.najtmar.gop.board.BoardGenerator;
import cz.wie.najtmar.gop.board.MapBoardGenerator;
import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.event.EventProcessingException;
import cz.wie.najtmar.gop.game.*;
import cz.wie.najtmar.gop.server.Client;
import cz.wie.najtmar.gop.server.GameSession;
import cz.wie.najtmar.gop.server.InitialPrawnDeployer;
import lombok.Getter;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * A Session of the Game, started in a Server.
 * @author najtmar
 */
public class GoPGameSession implements GameSession {

    /**
     * The delta by which the Game time is increased in each round.
     */
    private long gameTimeDelta;

    /**
     * PropertiesReader instance used to read server-side Properties.
     */
    private final PropertiesReader propertiesReader;

    /**
     * Random seed.
     */
    private final long seed;

    /**
     * The Game of the Session.
     */
    private Game game;

    /**
     * Clients.
     */
    @Getter
    private Iterable<Client> clients;

    private EventFactory eventFactory;

    private EventGeneratingExecutor.Factory<?>[][] executorFactories;

    @Getter
    private State state = State.NOT_INITIALIZED;

    public GoPGameSession(long seed) {
        this.propertiesReader = new DefaultPropertiesReader();
        this.seed = seed;
    }

    @Override
    public Game getGame() {
        Preconditions.checkArgument(state != State.NOT_INITIALIZED);
        return game;
    }

    @Override
    public EventFactory getEventFactory() {
        Preconditions.checkArgument(state != State.NOT_INITIALIZED);
        return eventFactory;
    }

    @Override
    public void abandonGameAttempt() {
        // TODO: Create an Abandon event to send it to Clients
    }

    // TODO: test this
    @Override
    public void initialize(Iterable<Client> clients) {
        Preconditions.checkArgument(state == State.NOT_INITIALIZED);
        try {
            game = new Game(propertiesReader.getGameProperties());
            gameTimeDelta = game.getGameProperties().getTimeDelta();
        } catch (GameException | IOException exception) {
            throw new RuntimeException("Game could not be created.", exception);
        }

        // Board.
        BoardGenerator boardGenerator = new MapBoardGenerator(propertiesReader);
        try {
            game.setBoard(boardGenerator.generate());
        } catch (GameException | BoardException exception) {
            throw new RuntimeException("Failed to initialize a Game with a Board.", exception);
        }

        // Players
        this.clients = Lists.newArrayList(clients);
        int i = 0;
        for (Client client : this.clients) {
            game.addPlayer(new Player(new User(client.getName(), client.getId()), i++, game));
        }

        // Initialize Game
        game.initialize();

        // Deploy Prawns
        InitialPrawnDeployer prawnDeployer = new CrossBoardPrawnDeployer();
        prawnDeployer.deployPrawns(game);

        // Event factory
        eventFactory = new EventFactory(new ObjectSerializer(game));

        // Executor factories
        initializeExecutorFactories();

        state = State.ITERATION_FINISHED;
    }

    @Override
    public void interruptGame() {
        // TODO: Create an Interrupt Event and send it to Clients
    }

    @Override
    public void restore(JsonObject gameSnapshot) {
        Preconditions.checkArgument(state == State.NOT_INITIALIZED);

        this.game = new Game(gameSnapshot);
        this.clients = game.getPlayers()
                .stream()
                .filter(player -> player.getState() != Player.State.ABANDONED)
                .map(player -> new Client(player.getUser().getName(), player.getUser().getId()))
                .collect(Collectors.toList());
        if (this.game.getState() == Game.State.INITIALIZED) {
            eventFactory = new EventFactory(new ObjectSerializer(game));
            initializeExecutorFactories();
            this.game.start();
        }

        state = State.ITERATION_FINISHED;
    }

    // TODO: Test this.
    @Override
    public List<Event> startGame() {
        Preconditions.checkArgument(state == State.ITERATION_FINISHED);
        Preconditions.checkArgument(game.getState() == Game.State.INITIALIZED);

        final Event[] events = new Event[1 + game.getPrawns().size()];
        try {
            events[0] = eventFactory.createInitializeGameEvent();
            int eventIndex = 1;
            for (Prawn prawn : game.getPrawns()) {
                if (prawn instanceof SettlersUnit) {
                    events[eventIndex++] =
                            eventFactory.createProduceSettlersUnitEvent(prawn.getId(), prawn.getCurrentPosition(),
                                    prawn.getPlayer());
                } else if (prawn instanceof Warrior) {
                    final Warrior warrior = (Warrior) prawn;
                    events[eventIndex++] = eventFactory.createProduceWarriorEvent(warrior.getId(), warrior.getCurrentPosition(),
                            warrior.getWarriorDirection(), warrior.getPlayer());
                } else {
                    throw new IllegalStateException("Unknown Prawn type: " + prawn);
                }
            }
        } catch (EventProcessingException exception) {
            throw new IllegalStateException("Initial Event creation failed.", exception);
        }

        game.start();

        return Arrays.asList(events);
    }

    // TODO: Test playing a complete iteration.
    @Override
    public List<Event> playIteration(List<Event> events) {
        this.state = State.ITERATION_PENDING;

        // Executor creation.
        final EventGeneratingExecutor[][] executorWorkflow =
                Arrays.stream(executorFactories).map(
                                factoryArray -> Arrays.stream(factoryArray).map(
                                        factory -> {
                                            try {
                                                return factory.create();
                                            } catch (GameException exception) {
                                                throw new RuntimeException(exception);
                                            }
                                        }).toArray(EventGeneratingExecutor[]::new))
                        .toArray(EventGeneratingExecutor[][]::new);

        List<Event> lastEvents = List.copyOf(events);
        for (EventGeneratingExecutor[] parallelExecutors : executorWorkflow) {
            // Event processing preparation.
            for (EventGeneratingExecutor executor : parallelExecutors) {
                try {
                    executor.prepare(lastEvents);
                } catch (GameException | EventProcessingException exception) {
                    throw new RuntimeException("Event processing preparation failed.", exception);
                }
            }
            // Event generation.
            final List<Event> currentEvents = new LinkedList<>();
            for (EventGeneratingExecutor executor : parallelExecutors) {
                try {
                    executor.executeAndGenerate();
                    currentEvents.addAll(executor.getEventsGenerated());
                } catch (GameException | EventProcessingException exception) {
                    throw new RuntimeException("Event processing preparation failed.", exception);
                }
            }
            lastEvents = currentEvents;
        }
        try {
            game.advanceTimeTo(game.getTime() + gameTimeDelta);
        } catch (GameException exception) {
            throw new RuntimeException("Advancing time failed.", exception);
        }

        return lastEvents;
    }

    @Override
    public JsonObject snapshot() {
        return null;
    }

//
//  /**
//   * PropertiesReader instance used to read server-side Properties.
//   */
//  private final PropertiesReader propertiesReader;
//
//  /**
//   * BoardGenerator used in the Games started by the Server.
//   */
//  private final BoardGenerator boardGenerator;
//
//  /**
//   * Used to deploy Prawns on the Board at the beginning of the Game.
//   */
//  private final InitialPrawnDeployer prawnDeployer;
//
//  /**
//   * The Random seed used in the Server.
//   */
//  private final long seed;
//
//  /**
//   * The Game of the Session.
//   */
//  private Game game;
//
//  /**
//   * User for object serialization.
//   */
//  private ObjectSerializer objectSerializer;
//
//  /**
//   * Used for Event creation.
//   */
//  private EventFactory eventFactory;
//
//  /**
//   * Factories for a workflow of executors used in a Game.
//   */
//  private EventGeneratingExecutor.Factory<?>[][] executorFactories;
//
//  /**
//   * The most recent Events to be sent to the next Executors .
//   */
//  private List<Event> lastEvents;
//
//  /**
//   * Constructor.
//   * @param propertiesReader used to read Properties used in the Game
//   * @param boardGenerator used to generate Boards
//   * @param prawnDeployer used to deploy first Prawns on the Board at the Game
//   * @param seed the Random seed used in the Server
//   */
//  @SuppressWarnings("rawtypes")  // Because of Guice bindings.
//  @Inject
//  GoPGameSession(PropertiesReader propertiesReader, BoardGenerator boardGenerator,
//                 InitialPrawnDeployer prawnDeployer,
//                 @Named("random seed") long seed) {
//    this.propertiesReader = propertiesReader;
//    this.boardGenerator = boardGenerator;
//    this.prawnDeployer = prawnDeployer;
//    this.seed = seed;
//  }
//
//  @Override
//  public void run() {
//    if (!initializeGame()) {
//      return;
//    }
//    createClientInitializationEvents();
//    initializeExecutorFactories();
//
//    try {
//      game.start();
//    } catch (GameException exception) {
//      throw new RuntimeException("The Game failed to start.", exception);
//    }
//
//    // Initial client communication.
//    {
//      final List<Event> currentEvents = new LinkedList<>();
//      final ClientCommunicator[] initialClientCommunicators =
//              new ClientCommunicator[clientCommunicatorFactories.length];
//      for (int i = 0; i < clientCommunicatorFactories.length; ++i) {
//        try {
//          initialClientCommunicators[i] = clientCommunicatorFactories[i].create();
//        } catch (GameException exception) {
//          throw new RuntimeException("ClientCommunicator " + i + " preparation failed.", exception);
//        }
//      }
//      for (int i = 0; i < initialClientCommunicators.length; ++i) {
//        try {
//          initialClientCommunicators[i].prepare(lastEvents);
//        } catch (GameException | EventProcessingException exception) {
//          throw new RuntimeException("Event processing preparation failed.", exception);
//        }
//      }
//      for (int i = 0; i < initialClientCommunicators.length; ++i) {
//        try {
//          initialClientCommunicators[i].executeAndGenerate();
//          currentEvents.addAll(initialClientCommunicators[i].getEventsGenerated());
//        } catch (GameException exception) {
//          throw new RuntimeException("Event processing preparation failed.", exception);
//        }
//      }
//      lastEvents = currentEvents;
//    }
//
//
//    while (game.getState() != Game.State.FINISHED) {
//      // Executor creation.
//      final EventGeneratingExecutor[][] executorWorkflow = StreamSupport.stream(
//          Arrays.asList(executorFactories)).map(
//              factoryArray -> StreamSupport.stream(Arrays.asList(factoryArray)).map(
//                  factory -> {
//                    try {
//                      return factory.create();
//                    } catch (GameException exception) {
//                      throw new RuntimeException(exception);
//                    }
//                  }).toArray(EventGeneratingExecutor[]::new))
//          .toArray(EventGeneratingExecutor[][]::new);
//
//      for (EventGeneratingExecutor[] parallelExecutors : executorWorkflow) {
//        // Event processing preparation.
//        for (EventGeneratingExecutor executor : parallelExecutors) {
//          try {
//            executor.prepare(lastEvents);
//          } catch (GameException | EventProcessingException exception) {
//            throw new RuntimeException("Event processing preparation failed.", exception);
//          }
//        }
//        // Event generation.
//        final List<Event> currentEvents = new LinkedList<>();
//        for (EventGeneratingExecutor executor : parallelExecutors) {
//          try {
//            executor.executeAndGenerate();
//            currentEvents.addAll(executor.getEventsGenerated());
//          } catch (GameException | EventProcessingException exception) {
//            throw new RuntimeException("Event processing preparation failed.", exception);
//          }
//        }
//        lastEvents = currentEvents;
//      }
//      notifyIterationFinished();
//      try {
//        game.advanceTimeTo(game.getTime() + gameTimeDelta);
//      } catch (GameException exception) {
//        throw new RuntimeException("Advancing time failed.", exception);
//      }
//    }
//  }
//
//  /**
//   * Creates and initializes the Game, or changes its State to INTENT_ABORTED .
//   * @return true iff the Game was successfully initialized
//   * @throws RuntimeException when Game initialization fails
//   */
//  boolean initializeGame() {
//    try {
//      game = new Game(propertiesReader.getGameProperties());
//      gameTimeDelta = game.getGameProperties().getTimeDelta();
//    } catch (GameException | IOException exception) {
//      throw new RuntimeException("Game could not be created.", exception);
//    }
//
//    // Board.
//    try {
//      game.setBoard(boardGenerator.generate());
//    } catch (GameException | BoardException exception) {
//      throw new RuntimeException("Failed to initialize a Game with a Board.", exception);
//    }
//
//    // Connections and Players.
//    final ConnectionManager connectionManager = connectionManagerFactory.create();
//    channels = connectionManager.establishConnectionChannels(this);
//    if (channels == null) {
//      try {
//        game.abortIntent();
//      } catch (GameException exception) {
//        throw new RuntimeException("Failed to abort the Game intent.", exception);
//      }
//      return false;
//    }
//    for (int i = 0; i < channels.length; ++i) {
//      final Event[] events;
//      try {
//        events = channels[i].readMessage().getEvents();
//      } catch (InterruptedException exception) {
//        throw new RuntimeException("Communication with client " + i + " failed.", exception);
//      }
//      if (events.length != 1 || events[0].getType() != Event.Type.ADD_PLAYER) {
//        throw new RuntimeException("Exactly one Event of type ADD_PLAYER expected (found " + events + ").");
//      }
//      final Player player;
//      try {
//        player = deserializeNewPlayer(events[0].getBody().getJsonObject("player"));
//      } catch (SerializationException exception) {
//        throw new RuntimeException("Failed to deserialize Player from Event " + events[0], exception);
//      }
//      try {
//        game.addPlayer(player);
//      } catch (GameException exception) {
//        throw new RuntimeException("Adding " + player + " to the Game failed.", exception);
//      }
//    }
//
//    // Game initialization.
//    try {
//      game.initialize();
//    } catch (GameException exception) {
//      throw new RuntimeException("" + game + "could not be initialized.", exception);
//    }
//    prawnDeployer.deployPrawns(game);
//
//    try {
//      objectSerializer = new ObjectSerializer(game);
//    } catch (GameException exception) {
//      throw new RuntimeException("ObjectSerializer could not be initialized.", exception);
//    }
//    eventFactory = new EventFactory(objectSerializer);
//    return true;
//  }
//
//  /**
//   * Given an INITIALIZED Game, create and store in lastEvents initial Events to be sent to all clients.
//   * @throws RuntimeException when creating initialization Events fails
//   */
//  void createClientInitializationEvents() {
//    if (game == null) {
//      throw new RuntimeException("Game hasn't been created.");
//    }
//    if (game.getState() != Game.State.INITIALIZED) {
//      throw new RuntimeException("Game should be in state INITIALIZED ( found " + game.getState() + ").");
//    }
//    final Event[] events = new Event[1 + game.getPrawns().size()];
//    try {
//      events[0] = eventFactory.createInitializeGameEvent();
//      int eventIndex = 1;
//      for (Prawn prawn : game.getPrawns()) {
//        if (prawn instanceof SettlersUnit) {
//          events[eventIndex++] =
//              eventFactory.createProduceSettlersUnitEvent(prawn.getId(), prawn.getCurrentPosition(),
//                  prawn.getPlayer());
//        } else if (prawn instanceof Warrior) {
//          final Warrior warrior = (Warrior) prawn;
//          events[eventIndex++] = eventFactory.createProduceWarriorEvent(warrior.getId(), warrior.getCurrentPosition(),
//              warrior.getWarriorDirection(), warrior.getPlayer());
//        } else {
//          throw new RuntimeException("Unknown Prawn type: " + prawn);
//        }
//      }
//    } catch (EventProcessingException exception) {
//      throw new RuntimeException("Initial Event creation failed.", exception);
//    }
//    lastEvents = Arrays.asList(events);
//  }
//
//  /**
//   * Deserializes new objects of type Player.
//   * @param serialized serialized object of type Player
//   * @return serialized deserialized to an object of type Player
//   * @throws SerializationException when deserialization process fails
//   */
//  Player deserializeNewPlayer(@Nonnull JsonObject serialized) throws SerializationException {
//    final JsonObject serializedUser = serialized.getJsonObject("user");
//    return new Player(new User(serializedUser.getString("name"), UUID.fromString(serializedUser.getString("uuid"))),
//        serialized.getInt("index"), game);
//  }
//
//  long getGameTimeDelta() {
//    return gameTimeDelta;
//  }
//
//  public Game getGame() {
//    return game;
//  }
//
//  ObjectSerializer getObjectSerializer() {
//    return objectSerializer;
//  }
//
//  List<Event> getLastEvents() {
//    return lastEvents;
//  }
//
  /**
   * Given an INITIALIZED Game, initializes all EventGeneratingExecutorFactories used in the GameSession.
   * @throws RuntimeException when client initialization fails
   */
  void initializeExecutorFactories() {
    if (game.getState() != Game.State.INITIALIZED) {
      throw new RuntimeException("Game should be in state INITIALIZED ( found " + game.getState() + ").");
    }
    executorFactories = new EventGeneratingExecutor.Factory<?>[][]{
      new EventGeneratingExecutor.Factory<?>[] {
        new InteractiveEventExecutor.Factory(game, eventFactory),
      },
      new EventGeneratingExecutor.Factory<?>[] {
        new MoveActionEventGeneratingExecutor.Factory(game, eventFactory),
      },
      new EventGeneratingExecutor.Factory<?>[] {
        new BattleEventGeneratingExecutor.Factory(game, eventFactory, new Random(seed)),
      },
      new EventGeneratingExecutor.Factory<?>[] {
        new CityProductionEventGeneratingExecutor.Factory(game, eventFactory),
      },
      new EventGeneratingExecutor.Factory<?>[] {
        new GameEvaluationEventGeneratingExecutor.Factory(game, eventFactory),
      },
    };
  }
//
//  /**
//   * Invoked after each iteration of a GameSession. Used for synchronization purposes in tests.
//   */
//  public void notifyIterationFinished() {
//  }

}
