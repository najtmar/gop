package cz.wie.najtmar.gop.server.gop;

import cz.wie.najtmar.gop.board.Board;
import cz.wie.najtmar.gop.board.Joint;
import cz.wie.najtmar.gop.common.Point2D;
import cz.wie.najtmar.gop.game.Game;
import cz.wie.najtmar.gop.game.GameException;
import cz.wie.najtmar.gop.game.Player;
import cz.wie.najtmar.gop.game.SettlersUnit;
import cz.wie.najtmar.gop.server.InitialPrawnDeployer;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * An implementation of interface InitialPrawnDeployer, which deploys Prawns near Board corners.
 * @author najtmar
 */
public class CrossBoardPrawnDeployer implements InitialPrawnDeployer {

  @Override
  public void deployPrawns(Game game) {
    if (game.getState() != Game.State.INITIALIZED) {
      throw new IllegalStateException("Initial Prawn deployment can only been performed in an INITIALIZED Game (found "
          + game.getState() + ").");
    }
    if (!game.getPrawns().isEmpty()) {
      throw new IllegalStateException("Initial Prawn deployment can only be performed when there are no Prawns yet (found "
          + game.getPrawns() + ").");
    }
    final Board board = game.getBoard();
    if (board.getJoints().size() < game.getPlayers().size()) {
      throw new IllegalStateException("Fewer Joints (" + board.getJoints().size() + ") than Players ("
          + game.getPlayers().size() + ") in the Game.");
    }
    final int boardXsize = game.getBoard().getBoardPropertiesSet().getXsize();
    final int boardYsize = game.getBoard().getBoardPropertiesSet().getYsize();
    final Point2D.Double[] boardCorners = new Point2D.Double[]{
        new Point2D.Double(boardXsize, boardYsize),
        new Point2D.Double(0.0, 0.0),
        new Point2D.Double(boardXsize, 0.0),
        new Point2D.Double(0.0, boardYsize),
        new Point2D.Double(boardXsize / 2.0, 0.0),
        new Point2D.Double(boardXsize / 2.0, boardYsize),
        new Point2D.Double(0.0, boardYsize / 2.0),
        new Point2D.Double(boardXsize, boardYsize / 2.0),
    };
    final Joint[] jointArray = board.getJoints().toArray(new Joint[]{});
    final List<Player> players = game.getPlayers();
    final HashMap<Joint, SettlersUnit> jointSettlersUnitMap = new HashMap<>();
    for (int i = 0; i < players.size(); ++i) {
      if (i < boardCorners.length) {
        // Sort Joints based on their distance from a respective corner.
        final int boardCornerIndex = i;
        Arrays.sort(jointArray,
            (joint0, joint1) -> Double.compare(
                boardCorners[boardCornerIndex].distance(joint0.getCoordinatesPair()),
                boardCorners[boardCornerIndex].distance(joint1.getCoordinatesPair())));
      } else {
        // Arbitrary Joint order (by index).
        Arrays.sort(jointArray, (joint0, joint1) -> Double.compare(joint0.getIndex(), joint1.getIndex()));
      }

      // Assign new SettlersUnits to best matching Joints .
      int bestJointIndex;
      for (bestJointIndex = 0; bestJointIndex < jointArray.length; ++bestJointIndex) {
        if (!jointSettlersUnitMap.containsKey(jointArray[bestJointIndex])) {
          break;
        }
      }
      if (bestJointIndex == jointArray.length) {
        throw new IllegalStateException("Failed to deploy the SettlersUnit for " + players.get(i) + " on the Board.");
      }
      final SettlersUnit settlersUnit;
      try {
        settlersUnit = (new SettlersUnit.Builder())
            .setCreationTime(game.getTime())
            .setPlayer(players.get(i))
            .setGameProperties(game.getGameProperties())
            .setCurrentPosition(jointArray[bestJointIndex].getNormalizedPosition())
            .build();
        game.addPrawn(settlersUnit);
      } catch (GameException exception) {
        throw new IllegalStateException("Failed to create SettlersUnit for " + players.get(i) + " on "
            + jointArray[bestJointIndex], exception);
      }
      jointSettlersUnitMap.put(jointArray[bestJointIndex], settlersUnit);
    }
  }

}
