package cz.wie.najtmar.gop.server;

import cz.wie.najtmar.gop.event.Event;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

import javax.json.Json;
import javax.json.JsonReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.StreamSupport;

@RequiredArgsConstructor
public class GameVerticle extends AbstractVerticle {

    public static final String START_GAME_EVENT = "initialize-game";
    public static final String RESTORE_GAME_EVENT = "restore-game";
    public static final String BROADCAST_EVENTS_EVENT = "broadcast-events";
    public static final String FINISH_GAME_EVENT = "finish-game";

    private static final String CLIENT_ID_KEY = "clientId";
    private static final String EVENTS_KEY = "events";

    private final NetworkServer networkServer;

    public void start() {
        vertx.eventBus().consumer(START_GAME_EVENT, this::startGame);
        vertx.eventBus().consumer(RESTORE_GAME_EVENT, this::restoreGame);
        vertx.eventBus().consumer(BROADCAST_EVENTS_EVENT, this::broadcastEvents);
    }

    public static JsonArray eventsToJsonArray(Iterable<Event> events) {
        JsonArray jsonArray = new JsonArray();
        for (Event event : events) {
            jsonArray.add(new JsonObject(event.getBody().toString()));
        }
        return jsonArray;
    }

    public static JsonObject clientAndEventsToJsonObject(UUID clientId, Iterable<Event> events) {
        JsonArray jsonArray = new JsonArray();
        for (Event event : events) {
            jsonArray.add(new JsonObject(event.getBody().toString()));
        }
        JsonObject jsonObject = new JsonObject();
        jsonObject.put(CLIENT_ID_KEY, clientId.toString());
        jsonObject.put(EVENTS_KEY, jsonArray);
        return jsonObject;
    }

    @SneakyThrows
    private static List<Event> jsonArrayToEvents(JsonArray jsonArray) {
        List<Event> eventList = new LinkedList<>();
        for (Iterator<Object> it = jsonArray.stream().iterator(); it.hasNext(); ) {
            Object object = it.next();
            JsonReader jsonReader = Json.createReader(new StringReader(((JsonObject) object).encode()));
            javax.json.JsonObject jsonObject = jsonReader.readObject();
            jsonReader.close();
            eventList.add(new Event(jsonObject));
        }
        return eventList;
    }

    @SneakyThrows
    private static Tuple2<UUID, List<Event>> jsonObjectToEvents(JsonObject clientAndEventsObject) {
        return Tuple.of(
                UUID.fromString(clientAndEventsObject.getString(CLIENT_ID_KEY)),
                jsonArrayToEvents(clientAndEventsObject.getJsonArray(EVENTS_KEY)));
    }

    // TODO: Broadcast to all clients
    // TODO: Test it
    private void startGame(Message<JsonObject> clientIdAndEventsMessage) {
        vertx.executeBlocking(
                promise -> {
                    try {
                        Tuple2<UUID, List<Event>> clientIdAndEvents = jsonObjectToEvents(clientIdAndEventsMessage.body());
                        networkServer.sendEvents(clientIdAndEvents._1, clientIdAndEvents._2);
                        promise.complete();
                    } catch (IOException exception) {
                        promise.fail(new RuntimeException("Sending events to client " + clientIdAndEventsMessage.body().encodePrettily() + " failed", exception));
                    } catch (IllegalStateException exception) {
                        promise.fail(new RuntimeException("Creating game initialization event failed", exception));
                    }
                }
        );
    }

    private void restoreGame(Message<JsonObject> clientIdAndEventsMessage) {
        vertx.executeBlocking(
                promise -> {
                    try {
                        Tuple2<UUID, List<Event>> clientIdAndEvents = jsonObjectToEvents(clientIdAndEventsMessage.body());
                        List<Event> events = clientIdAndEvents._2;
                        if (events.size() == 1 && events.get(0).getType() == Event.Type.RESTORE_GAME) {
                            networkServer.sendEvents(clientIdAndEvents._1, events);
                            promise.complete();
                        } else {
                            promise.fail(new RuntimeException("Not a RESTORE_GAME event: " + events));
                        }
                    } catch (IOException exception) {
                        promise.fail(new RuntimeException("Sending events to client " + clientIdAndEventsMessage.body().encodePrettily() + " failed", exception));
                    }
                }
        );
    }

    private void broadcastEvents(Message<JsonArray> events) {
        vertx.executeBlocking(
                promise -> {
                    try {
                        networkServer.broadcastEvents(jsonArrayToEvents(events.body()));
                        promise.complete();
                    } catch (IOException exception) {
                        promise.fail(new RuntimeException("Broadcasting events to clients failed", exception));
                    }
                });
    }

    private static boolean shouldFinishGame(Iterable<Event> events) {
        return StreamSupport.stream(events.spliterator(), false).anyMatch(event -> event.getType() == Event.Type.FINISH_GAME);
    }

}
