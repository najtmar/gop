package cz.wie.najtmar.gop.server;

import cz.wie.najtmar.gop.event.Klonable;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.util.UUID;

@Value
@Builder(toBuilder = true)
public class RegistrationPayload implements Klonable<RegistrationPayload> {

    @NonNull
    String traceId;
    @NonNull
    String clientName;
    @NonNull
    UUID clientId;

    @Override
    public RegistrationPayload klone() {
        return this.toBuilder().build();
    }
}
