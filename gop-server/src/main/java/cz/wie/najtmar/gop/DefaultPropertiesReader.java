package cz.wie.najtmar.gop;

import cz.wie.najtmar.gop.game.PropertiesReader;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class DefaultPropertiesReader implements PropertiesReader {
    @Override
    public Properties getBoardProperties() throws IOException {
        final Properties properties = new Properties();
        properties.load(getClass().getClassLoader().getResourceAsStream("board.properties"));
        return properties;
    }

    @Override
    public Properties getGameProperties() throws IOException {
        final Properties properties = new Properties();
        properties.load(getClass().getClassLoader().getResourceAsStream("game.properties"));
        return properties;
    }

    @Override
    public Properties getClientViewProperties() throws IOException {
        return null;
    }

    @Override
    public Properties getTestProperties() {
        return null;
    }
}
