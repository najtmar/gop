package cz.wie.najtmar.gop.server;

import cz.wie.najtmar.gop.game.Game;

import javax.annotation.Nonnull;

/**
 * Interface used to deploy Prawns for Game Players at the beginning of the Game.
 * @author najtmar
 */
public interface InitialPrawnDeployer {

  /**
   * Given a Game with n Players, deploys first Prawns on the Board .
   * @param game an INITIALIZED Game for which Prawns should be deployed
   */
  void deployPrawns(@Nonnull Game game);

}
