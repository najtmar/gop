package cz.wie.najtmar.gop.server;

import com.google.gson.JsonObject;
import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.game.EventFactory;
import cz.wie.najtmar.gop.game.Game;

import java.util.List;

public interface GameSession {

    enum State {
        NOT_INITIALIZED,
        ITERATION_PENDING,
        ITERATION_FINISHED,
    }

    State getState();

    Game getGame();

    Iterable<Client> getClients();

    /**
     * Get EventFactory for the current game.
     * @return EventFactory for the current game
     */
    EventFactory getEventFactory();

    /**
     * Abandon a Game before it has started.
     */
    void abandonGameAttempt();

    /**
     * Initialize a Game with given Clients .
     * @param clients Clients for which the Game is initialized
     */
    void initialize(Iterable<Client> clients);

    /**
     * Interrupt a Game that has started.
     */
    void interruptGame();

    /**
     * Restore a Game from a snapshot .
     * @param gameSnapshot a snapshot from which the game should be initialized
     */
    void restore(JsonObject gameSnapshot);

    /**
     * Get events generated for the game initialization
     * @return output Events
     */
    List<Event> startGame();

    /**
     * Play a Game iteration and get events generated
     * @param events input Events
     * @return output Events
     */
    List<Event> playIteration(List<Event> events);

    /**
     * Create a Game snapshot
     * @return snapshot created from the current Game
     */
    JsonObject snapshot();

}
