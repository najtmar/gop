package cz.wie.najtmar.gop.server;

import com.google.common.base.Preconditions;
import com.google.gson.JsonObject;
import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.game.Game;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import lombok.Getter;
import lombok.NonNull;
import lombok.SneakyThrows;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Logger;
import java.util.stream.StreamSupport;

import static cz.wie.najtmar.gop.server.NetworkServer.Result.*;

public class Server implements NetworkServer.EventHandler, NetworkServer.RegistrationHandler {

    private static final Logger LOGGER = Logger.getLogger(Server.class.getName());

    private static final Integer MIN_CLIENT_NUMBER = 2;
    private static final Integer MAX_CLIENT_NUMBER = 4;

    public enum State {
        STOPPED,
        INITIALIZED,
        GAME_CREATED,
        GAME_INITIALIZING,
        GAME_STARTED,
        GAME_ACTIVE,
        GAME_PAUSED,
        GAME_FINISHED,
    }

    @Getter
    private State state;

    private final NetworkServer networkServer;

    private final GameSession gameSession;

    private GameVerticle gameVerticle;
    private final EventBus eventBus;

    private Map<UUID, List<Event>> sessionEvents;

    private Map<UUID, Client> clients = new TreeMap<>();

    private Set<UUID> reconnectedClients = new HashSet<>();

    public Server(NetworkServer networkServer, GameSession gameSession, EventBus eventBus) {
        this.networkServer = networkServer;
        this.gameSession = gameSession;
        this.eventBus = eventBus;  // Vertx.vertx().eventBus()
        this.state = State.STOPPED;
    }

    @Override
    public NetworkServer.Result handle(Client client, List<Event> events) {
        if (state != State.GAME_ACTIVE && state != State.GAME_STARTED && state != State.GAME_PAUSED) {
            LOGGER.warning(String.format("event from client %s rejected in state %s", client, state));
            return CONFLICT;
        }
        if (events.stream().anyMatch(event -> !event.getType().isInteractive())) {
            LOGGER.warning(String.format("non-interactive event rejected from client %s", client));
            return CONFLICT;
        }
        if (gameSession.getState() != GameSession.State.ITERATION_PENDING) {
            LOGGER.warning(String.format("game session in state %s", gameSession.getState()));
            return CONFLICT;
        }
        if (sessionEvents.containsKey(client.getId())) {
            LOGGER.warning(String.format("client %s has already performed its move", client));
            return CONFLICT;
        }
        sessionEvents.put(client.getId(), events);
        if (sessionEvents.size() == clients.size()) {
            List<Event> clientEvents = sessionEvents.values().stream().collect(LinkedList::new, List::addAll, List::addAll);
            sessionEvents.clear();
            List<Event> generatedEvents = gameSession.playIteration(clientEvents);
            if (!shouldFinishGame(generatedEvents)) {
                eventBus.send(GameVerticle.BROADCAST_EVENTS_EVENT, GameVerticle.eventsToJsonArray(generatedEvents));
            } else {
                finishGame();
            }
        }
        return ACCEPTED;
    }

    private static boolean shouldFinishGame(Iterable<Event> events) {
        return StreamSupport.stream(events.spliterator(), false).anyMatch(event -> event.getType() == Event.Type.FINISH_GAME);
    }

    @Override
    public NetworkServer.Result register(Client client) {
        if (state != State.GAME_INITIALIZING) {
            LOGGER.warning(String.format("client %s cannot be registered in state %s", client, state));
            return CONFLICT;
        }
        if (clients.size() >= MAX_CLIENT_NUMBER) {
            LOGGER.warning(String.format("client number reached maximum number %d, client %s failed to be registered", MAX_CLIENT_NUMBER, client));
            return REJECTED;
        }
        if (!clients.containsKey(client.getId())) {
            connectClient(client);
        } else {
            LOGGER.info(String.format("client %s already registered", client));
            return CONFLICT;
        }
        return ACCEPTED;
    }

    @Override
    public NetworkServer.Result unregister(UUID clientId) {
        if (state != State.GAME_INITIALIZING) {
            LOGGER.warning(String.format("client %s cannot be unregistered in state %s", clientId, state));
            return CONFLICT;
        }
        if (clients.containsKey(clientId)) {
            removeClient(clientId);
        } else {
            LOGGER.info(String.format("client with id %s not registered", clientId));
            return CONFLICT;
        }
        return ACCEPTED;
    }

    @Override
    public NetworkServer.Result disconnect(UUID clientId) {
        if (clients.containsKey(clientId)) {
            if (clients.get(clientId).isConnected()) {
                disconnectClient(clientId);
            } else {
                LOGGER.warning(String.format("Client %s already disconnected", clients.get(clientId)));
                return CONFLICT;
            }
        } else {
            LOGGER.warning(String.format("Client with id %s does not exist", clientId));
            return CONFLICT;
        }
        return ACCEPTED;
    }

    @Override
    public NetworkServer.Result reconnect(UUID clientId) {
        if (state != State.GAME_STARTED && state != State.GAME_PAUSED) {
            LOGGER.warning(String.format("client %s cannot be reconnected in state %s", clientId, state));
            return CONFLICT;
        }
        if (clients.containsKey(clientId)) {
            if (!clients.get(clientId).isConnected()) {
                reconnectClient(clientId);
            } else {
                LOGGER.warning(String.format("Client %s already connected", clients.get(clientId)));
                return CONFLICT;
            }
        } else {
            LOGGER.info(String.format("client with id %s not registered", clientId));
            return CONFLICT;
        }
        return ACCEPTED;
    }

    @SneakyThrows
    public void initialize() {
        Preconditions.checkArgument(state == State.STOPPED);
        this.networkServer.registerService(this, this);
        this.state = State.INITIALIZED;
    }

    @SneakyThrows
    public void stop() {
        Preconditions.checkArgument(state == State.INITIALIZED);
        this.networkServer.unregisterService();
        this.state = State.STOPPED;
    }

    public void createGame() {
        Preconditions.checkArgument(state == State.INITIALIZED);
        state = State.GAME_CREATED;
    }

    public void startUserRegistration() {
        Preconditions.checkArgument(state == State.GAME_CREATED);
        this.state = State.GAME_INITIALIZING;
    }

    public void abandonGame() {
        Preconditions.checkArgument(state == State.GAME_CREATED || state == State.GAME_INITIALIZING);
        this.gameSession.abandonGameAttempt();
        this.state = State.INITIALIZED;
    }

    // TODO: Fix the tests and make sure that they test all that's needed.
    @SneakyThrows
    public Game tryStartGame() {
        Preconditions.checkArgument(state == State.GAME_INITIALIZING);
        Preconditions.checkArgument(gameSession.getState() == GameSession.State.NOT_INITIALIZED);
        if (checkCanStartGame()) {
            this.gameSession.initialize(clients.values());
            this.createGameVerticle();
            this.state = State.GAME_STARTED;
            this.sessionEvents = new HashMap<>();
            if (checkCanActivateGame()) {
                this.state = State.GAME_ACTIVE;
                List<Event> initialEvents = this.gameSession.startGame();
                for (UUID clientId : clients.keySet()) {
                    eventBus.send(GameVerticle.START_GAME_EVENT, GameVerticle.clientAndEventsToJsonObject(clientId, initialEvents));
                }
                return gameSession.getGame();
            }
        }
        return null;
    }

    @SneakyThrows
    public void restoreGame(JsonObject gameSnapshot) {
        Preconditions.checkArgument(state == State.INITIALIZED);
        Preconditions.checkArgument(gameSession.getState() == GameSession.State.NOT_INITIALIZED);
        this.gameSession.restore(gameSnapshot);
        this.clients = new TreeMap<>();
        for (Client client : this.gameSession.getClients()) {
            this.clients.put(client.getId(), client);
        }
        this.createGameVerticle();
        this.state = State.GAME_PAUSED;
    }

    @SneakyThrows
    public void preemptGame() {
        Preconditions.checkArgument(state == State.GAME_STARTED || state == State.GAME_PAUSED|| state == State.GAME_ACTIVE || state == State.GAME_FINISHED);
        Preconditions.checkArgument(gameSession.getState() == GameSession.State.ITERATION_PENDING ||
                gameSession.getState() == GameSession.State.ITERATION_FINISHED);
        this.deleteGameVerticle();
        this.gameSession.interruptGame();
        this.clients.clear();
        this.state = State.INITIALIZED;
    }

    @SneakyThrows
    private void finishGame() {
        Preconditions.checkArgument(state == State.GAME_STARTED || state == State.GAME_PAUSED || state == State.GAME_ACTIVE);
        Preconditions.checkArgument(gameSession.getState() == GameSession.State.ITERATION_PENDING);
        this.state = State.GAME_FINISHED;
        eventBus.send(GameVerticle.BROADCAST_EVENTS_EVENT, GameVerticle.eventsToJsonArray(List.of(gameSession.getEventFactory().createFinishGameEvent())));
    }

    private void connectClient(@NonNull Client client) {
        Preconditions.checkArgument(state == State.GAME_INITIALIZING);
        Preconditions.checkArgument(!clients.containsKey(client.getId()));
        Preconditions.checkArgument(clients.size() < MAX_CLIENT_NUMBER);
        client.setConnected(true);
        clients.put(client.getId(), client);
    }

    @SneakyThrows
    private void reconnectClient(@NonNull UUID clientId) {
        Preconditions.checkArgument(state == State.GAME_STARTED || state == State.GAME_PAUSED || state == State.GAME_ACTIVE);
        Client client = clients.get(clientId);
        Preconditions.checkNotNull(client);
        Preconditions.checkArgument(!client.isConnected());
        client.setConnected(true);
        reconnectedClients.add(clientId);
        if (state == State.GAME_PAUSED && clients.entrySet().stream().allMatch(entry -> entry.getValue().isConnected())) {
            this.state = State.GAME_ACTIVE;
            Event gameRestoreEvent = gameSession.getEventFactory().createRestoreGameEvent();
            for (UUID reconnectedClientId : reconnectedClients) {
                eventBus.send(GameVerticle.RESTORE_GAME_EVENT, GameVerticle.clientAndEventsToJsonObject(reconnectedClientId, List.of(gameRestoreEvent)));
            }
            reconnectedClients.clear();
        }
    }

    private void removeClient(@NonNull UUID clientId) {
        Preconditions.checkArgument(state == State.GAME_INITIALIZING);
        Preconditions.checkArgument(clients.containsKey(clientId));
        clients.remove(clientId);
    }

    private void disconnectClient(@NonNull UUID clientId) {
        Preconditions.checkArgument(state == State.GAME_INITIALIZING || state == State.GAME_STARTED || state == State.GAME_PAUSED || state == State.GAME_ACTIVE);
        Preconditions.checkArgument(clients.containsKey(clientId));
        Preconditions.checkArgument(clients.get(clientId).isConnected());
        clients.get(clientId).setConnected(false);
        if (state == State.GAME_ACTIVE) {
            this.state = State.GAME_PAUSED;
        }
    }

    private void createGameVerticle() throws InterruptedException {
        gameVerticle = new GameVerticle(networkServer);
        CountDownLatch verticleDeployed = new CountDownLatch(1);
        Vertx.vertx().deployVerticle(gameVerticle, result -> {
            verticleDeployed.countDown();
        });
        verticleDeployed.await();
    }

    private void deleteGameVerticle() throws InterruptedException {
        CountDownLatch verticleUndeployed = new CountDownLatch(1);
        Vertx.vertx().undeploy(gameVerticle.deploymentID(), result -> {
            verticleUndeployed.countDown();
        });
        verticleUndeployed.await();
    }

    private boolean checkCanStartGame() {
        return clients.size() >= MIN_CLIENT_NUMBER;
    }

    private boolean checkCanActivateGame() {
        return clients.values().stream().allMatch(Client::isConnected);
    }

}
