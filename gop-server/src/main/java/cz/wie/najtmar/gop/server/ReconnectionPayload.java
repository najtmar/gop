package cz.wie.najtmar.gop.server;

import cz.wie.najtmar.gop.event.Klonable;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.util.UUID;

@Value
@Builder(toBuilder = true)
public class ReconnectionPayload implements Klonable<ReconnectionPayload> {

    @NonNull
    String traceId;
    @NonNull
    String clientName;
    @NonNull
    UUID clientId;

    @Override
    public ReconnectionPayload klone() {
        return this.toBuilder().build();
    }
}
