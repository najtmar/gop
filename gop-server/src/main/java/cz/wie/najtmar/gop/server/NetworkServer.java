package cz.wie.najtmar.gop.server;

import cz.wie.najtmar.gop.event.Event;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

public interface NetworkServer {

    enum Result {
        ACCEPTED,
        REJECTED,
        CONFLICT
    }

    interface EventHandler {

        Result handle(Client client, List<Event> event);

    }

    interface RegistrationHandler {

        Result register(Client client);

        Result unregister(UUID clientId);

        Result disconnect(UUID clientId);

        Result reconnect(UUID clientId);
    }

    void registerService(EventHandler eventHandler, RegistrationHandler registrationHandler) throws IOException;

    void unregisterService() throws IOException;

    /**
     * Send events.
     * @param clientId id of the client to send events to.
     * @param events Events to be delivered.
     * @return true iff all events could be successfully delivered
     * @throws IOException
     */
    boolean sendEvents(UUID clientId, Iterable<Event> events) throws IOException;

    /**
     * Broadcasts events.
     * @param events Events to be delivered.
     * @return true iff all events could be successfully delivered
     * @throws IOException
     */
    boolean broadcastEvents(Iterable<Event> events) throws IOException;

}
