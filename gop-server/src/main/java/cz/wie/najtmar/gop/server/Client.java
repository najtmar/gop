package cz.wie.najtmar.gop.server;

import lombok.*;

import java.util.UUID;

@RequiredArgsConstructor
@Getter
@EqualsAndHashCode
@ToString
public class Client {

    private final String name;
    private final UUID id;

    @Setter
    private boolean connected = false;

}
