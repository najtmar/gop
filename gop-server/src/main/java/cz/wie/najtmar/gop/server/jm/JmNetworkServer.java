package cz.wie.najtmar.gop.server.jm;

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.gson.*;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.event.EventProcessingException;
import cz.wie.najtmar.gop.event.Klonable;
import cz.wie.najtmar.gop.server.*;
import cz.wie.najtmar.gop.server.ReconnectionPayload;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import javax.jmdns.JmDNS;
import javax.jmdns.ServiceInfo;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonValue;
import javax.net.ssl.SSLSession;
import java.io.*;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpHeaders;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.*;
import java.util.function.BiFunction;
import java.util.logging.Logger;


public class JmNetworkServer implements NetworkServer {

    public static final String SERVICE_TYPE = "_gop._tcp.local.";
    public static final String SERVICE_NAME = "Game of Prawns server";
    public static final String SERVICE_DESCRIPTION = "Game of Prawns game server";
    public static final int SERVER_PORT = 1979;
    public static final int CLIENT_PORT = 1980;

    private static final long DEFAULT_CLIENT_REQUEST_TIMEOUT_MILLIS = 5000;

    private static final String CLIENT_ENDPOINT_TEMPLATE = "http://%s:%d/%s";

    private boolean serviceRegistered = false;
    private long clientRequestTimeoutMillis = DEFAULT_CLIENT_REQUEST_TIMEOUT_MILLIS;
    private JmDNS jmdns;
    private HttpServer httpServer;
    private HttpClient httpClient;

    private InetAddress inetAddress;

    private EventHandler eventHandler;
    private RegistrationHandler registrationHandler;

    private final Map<Address, Client> addressClientMap = new HashMap<>();
    private final Map<UUID, Address> clientAddressMap = new HashMap<>();

    private static final Gson GSON = new GsonBuilder()
            .registerTypeAdapter(javax.json.JsonObject.class, (JsonDeserializer<javax.json.JsonObject>) (json, typeOfT, context) -> {
                JsonObject jObject = json.getAsJsonObject();
                return Json.createReader(new StringReader(jObject.toString())).readObject();
            })
            .registerTypeAdapter(javax.json.JsonArray.class, (JsonDeserializer<javax.json.JsonArray>) (jsonArray, typeOfT, context) -> {
                JsonArray jArray = jsonArray.getAsJsonArray();
                return Json.createReader(new StringReader(jArray.toString())).readArray();
            })
            .create();
    private static final Logger LOGGER = Logger.getLogger(JmNetworkServer.class.getName());

    @Override
    public void registerService(EventHandler eventHandler, RegistrationHandler registrationHandler) throws IOException {
        Preconditions.checkArgument(!serviceRegistered);
        this.serviceRegistered = true;

        this.eventHandler = eventHandler;
        this.registrationHandler = registrationHandler;

        // Create a JmDNS instance
        this.jmdns = JmDNS.create();

        // Register a service
        ServiceInfo serviceInfo = ServiceInfo.create(SERVICE_TYPE, SERVICE_NAME, SERVER_PORT, SERVICE_DESCRIPTION);
        jmdns.registerService(serviceInfo);

        // Create HTTP server.
        this.inetAddress = jmdns.getInetAddress();
        this.httpServer = HttpServer.create(new InetSocketAddress( inetAddress, SERVER_PORT), 0);
        httpServer.createContext("/event", this::handleEvent);
        httpServer.createContext("/register", this::handleRegistration);
        httpServer.createContext("/unregister", this::handleUnregistration);
        httpServer.createContext("/reconnect", this::handleReconnection);
        httpServer.createContext("/_ping", httpExchange -> respond200(httpExchange, null));
        httpServer.start();

        // Create HTTP client.
        httpClient = HttpClient.newHttpClient();
    }

    @Override
    public void unregisterService() {
        Preconditions.checkArgument(serviceRegistered);
        this.serviceRegistered = false;

        // Unregister all services
        jmdns.unregisterAllServices();

        httpServer.stop(0);
    }

    @Override
    public boolean broadcastEvents(Iterable<Event> events) {
        Preconditions.checkArgument(serviceRegistered);
        boolean result = true;
        for (final var client : addressClientMap.values()) {
            HttpResponse<String> httpResponse = sendRequest(client.getId(), "events",
                    ((builder, body) -> builder.POST(HttpRequest.BodyPublishers.ofString(body))),
                    serializeEvents(events));
            if (httpResponse.statusCode() != 200) {
                registrationHandler.disconnect(client.getId());
                result = false;
            }
        }
        return result;
    }

    @Override
    public boolean sendEvents(UUID clientId, Iterable<Event> events) throws IOException {
        Preconditions.checkArgument(serviceRegistered);
        boolean result = true;
        HttpResponse<String> httpResponse = sendRequest(clientId, "events",
                ((builder, body) -> builder.POST(HttpRequest.BodyPublishers.ofString(body))),
                serializeEvents(events));
        if (httpResponse.statusCode() != 200) {
            registrationHandler.disconnect(clientId);
            result = false;
        }
        return result;
    }

    private String serializeEvents(Iterable<Event> events) {
        JsonArrayBuilder builder = Json.createArrayBuilder();
        for (Event event : events) {
            builder.add(event.getBody());
        }
        return builder.build().toString();
    }

    private HttpResponse<String> sendRequest(UUID clientId, String endpoint, BiFunction<HttpRequest.Builder, String, HttpRequest.Builder> method, String body) {
        Address address = clientAddressMap.get(clientId);
        Preconditions.checkNotNull(address, String.format("Address for client %s not found.", clientId));
        HttpRequest.Builder httpRequestBuilder = HttpRequest.newBuilder(
                URI.create(String.format(CLIENT_ENDPOINT_TEMPLATE, address.getFormattedAddress(), CLIENT_PORT, endpoint)))
                .timeout(Duration.ofMillis(clientRequestTimeoutMillis));
        HttpRequest httpRequest = method.apply(httpRequestBuilder, body).build();

        HttpResponse<String> httpResponse;
        try {
            httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            LOGGER.warning(String.format("Communication with client %s failed with error %s", clientId, e.getMessage()));
            return createResponse500(httpRequest, e.getMessage());
        }
        if (httpResponse.statusCode() != 200) {
            String message = String.format("Client responded with %d", httpResponse.statusCode());
            LOGGER.warning(message);
            return createResponse500(httpRequest, message);
        }
        return httpResponse;
    }

    HttpResponse<String> createResponse500(HttpRequest httpRequest, String message) {
        return new HttpResponse<>() {
            @Override
            public int statusCode() {
                return 500;
            }

            @Override
            public HttpRequest request() {
                return httpRequest;
            }

            @Override
            public Optional<HttpResponse<String>> previousResponse() {
                return Optional.empty();
            }

            @Override
            public HttpHeaders headers() {
                return null;
            }

            @Override
            public String body() {
                return message;
            }

            @Override
            public Optional<SSLSession> sslSession() {
                return Optional.empty();
            }

            @Override
            public URI uri() {
                return httpRequest.uri();
            }

            @Override
            public HttpClient.Version version() {
                return httpClient.version();
            }
        };
    }

    private void handleEvent(HttpExchange httpExchange) throws IOException {
        Preconditions.checkArgument(serviceRegistered);
        if (!"POST".equals(httpExchange.getRequestMethod())) {
            respond400(httpExchange, "Invalid request method " + httpExchange, null);
            return;
        }
        EventsPayload eventsPayload;
        List<Event> events;
        try {
            eventsPayload = parseJson(httpExchange.getRequestBody(), EventsPayload.class);
            events = new LinkedList<>();
            for (JsonValue json : eventsPayload.getEventsBody()) {
                events.add(new Event((javax.json.JsonObject) json));
            }
        } catch (JsonIOException | JsonSyntaxException | EventProcessingException | IllegalArgumentException exception) {
            respond400(httpExchange, "invalid data " + exception, null);
            return;
        } catch (Exception exception) {
            respond500(httpExchange, exception, null);
            return;
        }
        String traceId = eventsPayload.getTraceId();

        Address clientAddress = Address.of(httpExchange.getRemoteAddress());
        Client client = addressClientMap.get(clientAddress);
        if (client == null) {
            respond401(httpExchange, String.format("address %s is not registered", clientAddress), traceId);
            return;
        }
        if (!client.getId().equals(eventsPayload.getClientId())) {
            respond401(httpExchange, String.format("registered client %s does not match client %s", client.getId(), eventsPayload.getClientId()), traceId);
            return;
        }

        switch (eventHandler.handle(client, events)) {
            case ACCEPTED:
                respond200(httpExchange, traceId);
                break;
            case REJECTED:
                respond403(httpExchange, eventsPayload.getClientId(), traceId);
                break;
            case CONFLICT:
                respond409(httpExchange, traceId);
                break;
        }
    }

    private void handleRegistration(HttpExchange httpExchange) throws IOException {
        Preconditions.checkArgument(serviceRegistered);
        if (!"POST".equals(httpExchange.getRequestMethod())) {
            respond400(httpExchange, "invalid request method " + httpExchange, null);
            return;
        }

        RegistrationPayload registrationPayload;
        try {
            registrationPayload = parseJson(httpExchange.getRequestBody(), RegistrationPayload.class);
        } catch (JsonIOException | JsonSyntaxException | IllegalArgumentException exception) {
            respond400(httpExchange, "invalid data " + exception, null);
            return;
        } catch (Exception exception) {
            respond500(httpExchange, exception, null);
            return;
        }
        String traceId = registrationPayload.getTraceId();
        Client client = new Client(registrationPayload.getClientName(), registrationPayload.getClientId());
        Address address = Address.of(httpExchange.getRemoteAddress());

        Client oldClient = addressClientMap.get(address);
        if (oldClient != null) {
            if (!oldClient.getId().equals(client.getId())) {
                respond401(httpExchange, String.format("address %s already registered for client %s", address, oldClient), traceId);
            } else {
                respond400(httpExchange, String.format("client %s already registered", oldClient), traceId);
            }
            return;
        }

        switch (registrationHandler.register(client)) {
            case ACCEPTED:
                respond200(httpExchange, traceId);
                addressClientMap.put(address, client);
                clientAddressMap.put(client.getId(), address);
                break;
            case REJECTED:
                respond403(httpExchange, client.getId(), traceId);
                break;
            case CONFLICT:
                respond409(httpExchange, traceId);
                break;
        }
    }

    private void handleReconnection(HttpExchange httpExchange) throws IOException {
        Preconditions.checkArgument(serviceRegistered);
        if (!"POST".equals(httpExchange.getRequestMethod())) {
            respond400(httpExchange, "invalid request method " + httpExchange, null);
            return;
        }

        ReconnectionPayload reconnectionPayload;
        try {
            reconnectionPayload = parseJson(httpExchange.getRequestBody(), ReconnectionPayload.class);
        } catch (JsonIOException | JsonSyntaxException | IllegalArgumentException exception) {
            respond400(httpExchange, "invalid data " + exception, null);
            return;
        } catch (Exception exception) {
            respond500(httpExchange, exception, null);
            return;
        }
        String traceId = reconnectionPayload.getTraceId();
        Client client = new Client(reconnectionPayload.getClientName(), reconnectionPayload.getClientId());
        Address address = Address.of(httpExchange.getRemoteAddress());

        Client oldClient = addressClientMap.get(address);
        if (oldClient != null) {
            if (!oldClient.getId().equals(client.getId())) {
                LOGGER.info(String.format("client %s already connected", client));
                respond200(httpExchange, traceId);
                return;
            }
        } else {
            respond400(httpExchange, String.format("client %s not registered", oldClient), traceId);
            return;
        }

        switch (registrationHandler.reconnect(client.getId())) {
            case ACCEPTED:
                respond200(httpExchange, traceId);
                addressClientMap.put(address, client);
                clientAddressMap.put(client.getId(), address);
                break;
            case REJECTED:
                respond403(httpExchange, client.getId(), traceId);
                break;
            case CONFLICT:
                respond409(httpExchange, traceId);
                break;
        }
    }

    private void handleUnregistration(HttpExchange httpExchange) throws IOException {
        Preconditions.checkArgument(serviceRegistered);
        if (!"POST".equals(httpExchange.getRequestMethod())) {
            respond400(httpExchange, "invalid request method " + httpExchange, null);
            return;
        }

        UnregistrationPayload unregistrationPayload;
        try {
            unregistrationPayload = parseJson(httpExchange.getRequestBody(), UnregistrationPayload.class);
        } catch (JsonIOException | JsonSyntaxException | IllegalArgumentException exception) {
            respond400(httpExchange, "invalid data " + exception, null);
            return;
        } catch (Exception exception) {
            respond500(httpExchange, exception, null);
            return;
        }
        String traceId = unregistrationPayload.getTraceId();
        UUID clientId = unregistrationPayload.getClientId();
        Address address = Address.of(httpExchange.getRemoteAddress());

        if (!clientAddressMap.containsKey(clientId)) {
            LOGGER.info(String.format("client %s not registered", clientId));
            respond200(httpExchange, traceId);
            return;
        }
        if (!address.equals(clientAddressMap.get(clientId))) {
            respond401(httpExchange, String.format("client %s not registered for address %s", clientId, address), traceId);
            return;
        }

        switch (registrationHandler.unregister(clientId)) {
            case ACCEPTED:
                respond200(httpExchange, traceId);
                addressClientMap.remove(address);
                clientAddressMap.remove(clientId);
                break;
            case REJECTED:
                respond403(httpExchange, clientId, traceId);
                break;
            case CONFLICT:
                respond409(httpExchange, traceId);
                break;
        }
    }

    private void respond200(HttpExchange httpExchange, String traceId) throws IOException {
        LOGGER.info(String.format("%s : %s", traceId, " : 200 OK: "));
        httpExchange.sendResponseHeaders(200, 0);
        httpExchange.close();
    }


    private void respond400(HttpExchange httpExchange, String message, String traceId) throws IOException {
        LOGGER.warning(String.format("%s : %s", traceId, message));
        httpExchange.sendResponseHeaders(400, 0);
        new OutputStreamWriter(httpExchange.getResponseBody()).append(message);
        httpExchange.close();
    }

    private void respond401(HttpExchange httpExchange, String message, String traceId) throws IOException {
        LOGGER.warning(String.format("%s : authentication failed : %s", traceId, message));
        httpExchange.sendResponseHeaders(401, 0);
        httpExchange.close();
    }

    private void respond403(HttpExchange httpExchange, UUID clientId, String traceId) throws IOException {
        LOGGER.warning(String.format("%s : client %s forbidden", traceId, clientId.toString()));
        httpExchange.sendResponseHeaders(403, 0);
        httpExchange.close();
    }

    private void respond409(HttpExchange httpExchange, String traceId) throws IOException {
        LOGGER.warning(String.format("%s : ignored", traceId));
        httpExchange.sendResponseHeaders(409, 0);
        httpExchange.close();
    }

    private void respond500(HttpExchange httpExchange, Throwable exception, String traceId) throws IOException {
        LOGGER.warning(String.format("%s : %s : %s", traceId, " : Oops, something went wrong: ", exception));
        httpExchange.sendResponseHeaders(500, 0);
        new OutputStreamWriter(httpExchange.getResponseBody()).append(exception.getLocalizedMessage());
        httpExchange.close();
    }

    private static <T extends Klonable<T>> T parseJson(InputStream inputStream, Class<T> classOfT) {
        try {
            JsonObject json = JsonParser.parseReader(new InputStreamReader(
                    inputStream, Charsets.UTF_8)).getAsJsonObject();
            return GSON.fromJson(json, classOfT).klone();
        } catch (NullPointerException exception) {
            throw new JsonSyntaxException(exception);
        }
    }

    @Value
    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    private static class Address {

        static Address of(InetSocketAddress inetSocketAddress) {
            return new Address(inetSocketAddress.getAddress().getHostAddress(), inetSocketAddress.getAddress() instanceof Inet6Address);
        }

        String address;
        boolean isInet6Address;

        public String getFormattedAddress() {
            return isInet6Address ? String.format("[%s]", address) : address;
        }

    }

}
