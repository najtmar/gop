package cz.wie.najtmar.gop.server;

import cz.wie.najtmar.gop.event.Klonable;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.util.UUID;

@Value
@Builder(toBuilder = true)
public class UnregistrationPayload implements Klonable<UnregistrationPayload> {

    @NonNull
    String traceId;
    @NonNull
    UUID clientId;

    @Override
    public UnregistrationPayload klone() {
        return this.toBuilder().build();
    }
}
