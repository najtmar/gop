package cz.wie.najtmar.gop.server.gop

import cz.wie.najtmar.gop.event.Event
import cz.wie.najtmar.gop.game.Game
import cz.wie.najtmar.gop.game.ObjectSerializer
import cz.wie.najtmar.gop.game.Prawn
import cz.wie.najtmar.gop.game.SettlersUnit
import cz.wie.najtmar.gop.server.Client
import cz.wie.najtmar.gop.server.GameSession
import cz.wie.najtmar.gop.server.GameSnapshotFactory
import spock.lang.Specification

import javax.json.Json
import javax.json.JsonArray

import static org.junit.Assert.assertEquals
import static org.mockito.Mockito.times
import static org.mockito.Mockito.verify

class GoPGameSessionSpec extends Specification {

  private static long RANDOM_SEED = 12345

  Client clientA
  Client clientB

  Event addPlayerEvent0;
  Event addPlayerEvent1;

  private GoPGameSession objectUnderTest

  def setup() {
    clientA = new Client('userA', UUID.fromString('00000000-0000-3039-0000-000000010932'))
    clientB = new Client('userB', UUID.fromString('00000000-0001-81cd-0000-00000000a8ca'))

    addPlayerEvent0 = new Event(Json.createObjectBuilder()
            .add("type", "ADD_PLAYER")
            .add("time", 10)
            .add("player", Json.createObjectBuilder()
                    .add("user", Json.createObjectBuilder()
                            .add("name", "userA")
                            .add("uuid", "00000000-0000-3039-0000-000000010932")
                            .build())
                    .add("index", 0)
                    .build())
            .build());
    addPlayerEvent1 = new Event(Json.createObjectBuilder()
            .add("type", "ADD_PLAYER")
            .add("time", 10)
            .add("player", Json.createObjectBuilder()
                    .add("user", Json.createObjectBuilder()
                            .add("name", "userB")
                            .add("uuid", "00000000-0001-81cd-0000-00000000a8ca")
                            .build())
                    .add("index", 1)
                    .build())
            .build());
    Prawn.resetFirstAvailableId();

    objectUnderTest = new GoPGameSession(RANDOM_SEED)
  }

  def "should properly restore game session from snapshot"() {
    given: 'a game snapshot'
    def uuid = UUID.randomUUID()
    def timestamp = System.currentTimeMillis()
    def gameSnapshot = GameSnapshotFactory.createStandardGameSnapshot(uuid, timestamp)

    when: 'restoring game session from snapshot'
    objectUnderTest.restore(gameSnapshot)

    then: 'game session is restored properly'
    objectUnderTest.clients == [
            new Client('playerA', UUID.fromString('00000000-0000-3039-0000-000000010932')),
            new Client('playerB', UUID.fromString('00000000-0001-81cd-0000-00000000a8ca')),
            new Client('playerC', UUID.fromString('00000000-0000-3039-0000-00000000d431')),
            // playerD has abandoned the game
    ]
    objectUnderTest.state == GameSession.State.ITERATION_FINISHED
  }

  def "test game session creation works"() {
    when:
    objectUnderTest.initialize([clientA, clientB]);
    def game = objectUnderTest.game
    def id = game.getId()
    def timestamp = game.getTimestamp()
    def serializedGame = new ObjectSerializer(game).serializeGameWithoutPrawnsOrCities()

    then:
    game.time == 352328
    serializedGame.getString("id") == id.toString()
    serializedGame.getJsonNumber("timestamp").longValue() == timestamp
    def board = serializedGame.getJsonObject("board")
    board.getJsonArray("joints").size() == 16
    board.getJsonArray("roadSections").size() == 22
    serializedGame.getJsonArray("players") == Json.createReader(new StringReader("""[
  {
      "index": 0,
      "user": {
        "name":"userA",
        "uuid": "00000000-0000-3039-0000-000000010932"
      }
    },
    {
      "index": 1,
      "user": {
        "name": "userB",
        "uuid": "00000000-0001-81cd-0000-00000000a8ca"
      }
    }
  ]
""")).readArray()
    // TODO: Make sure that the game got properly initialized (including settlers unit events)
//    verify(channels[0], times(1)).readMessage();
//    verify(channels[1], times(1)).readMessage();
  }

  private void deployPrawns(Game game) {
    SettlersUnit settlersUnitA = (new SettlersUnit.Builder())
            .setCreationTime(game.getTime())
            .setCurrentPosition(game.getBoard().getJoints().get(0).getNormalizedPosition())
            .setGameProperties(game.getGameProperties())
            .setPlayer(game.getPlayers().get(0))
            .build();
    SettlersUnit settlersUnitB = (new SettlersUnit.Builder())
            .setCreationTime(game.getTime())
            .setCurrentPosition(game.getBoard().getJoints().get(1).getNormalizedPosition())
            .setGameProperties(game.getGameProperties())
            .setPlayer(game.getPlayers().get(1))
            .build();
    game.addPrawn(settlersUnitA);
    game.addPrawn(settlersUnitB);
  }

}
