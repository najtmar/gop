package cz.wie.najtmar.gop.server

import cz.wie.najtmar.gop.event.Event
import cz.wie.najtmar.gop.game.EventFactory
import cz.wie.najtmar.gop.game.Game
import cz.wie.najtmar.gop.server.gop.GoPGameSession
import io.vertx.core.eventbus.EventBus
import spock.lang.Specification

import javax.json.Json

import static cz.wie.najtmar.gop.server.Server.State.*

class ServerSpec extends Specification {

    NetworkServer networkServer
    GameSession gameSession
    Game game
    EventBus eventBus
    EventFactory eventFactory
    Client client1, client2, client3, client4
    Event initializeGameEvent
    Event interactiveEvent
    Event interactiveEvent2
    Event generatedEvent
    Event generatedEvent2
    Event restoreGameEvent
    Event finishGameEvent;

    Server server

    def setup() {
        networkServer = Mock(NetworkServer)
        gameSession = Mock(GameSession)
        game = Mock(Game)
        eventBus = Mock(EventBus)
        eventFactory = Mock(EventFactory)
        server = new Server(networkServer, gameSession, eventBus)
        client1 = new Client("Client 1", UUID.randomUUID())
        client2 = new Client("Client 2", UUID.randomUUID())
        client3 = new Client("Client 3", UUID.randomUUID())
        client4 = new Client("Client 4", UUID.randomUUID())
        initializeGameEvent = new Event(Json.createObjectBuilder()
                .add("type", "INITIALIZE_GAME")
                .add("time", 123456)
                .add("key0", "value0")
                .add("key1", 11)
                .build())
        interactiveEvent = new Event(Json.createObjectBuilder()
                .add("type", "TEST_INTERACTIVE")
                .add("time", 123456)
                .add("key0", "value0")
                .add("key1", 12)
                .build())
        interactiveEvent2 = new Event(Json.createObjectBuilder()
                .add("type", "TEST_INTERACTIVE")
                .add("time", 123456)
                .add("key0", "value0")
                .add("key1", 13)
                .build())
        generatedEvent = new Event(Json.createObjectBuilder()
                .add("type", "TEST")
                .add("time", 123456)
                .add("key0", "value0")
                .add("key1", 14)
                .build())
        generatedEvent2 = new Event(Json.createObjectBuilder()
                .add("type", "TEST")
                .add("time", 123456)
                .add("key0", "value0")
                .add("key1", 15)
                .build())
        restoreGameEvent = new Event(Json.createObjectBuilder()
                .add("type", "RESTORE_GAME")
                .add("time", 123456)
                .add("key0", "value0")
                .add("key1", 16)
                .build())
        finishGameEvent = new Event(Json.createObjectBuilder()
                .add("type", "FINISH_GAME")
                .add("time", 123456)
                .add("key0", "value0")
                .add("key1", 17)
                .build())
    }

    def "should properly manage state of the server"() {
        given: "server in state STOPPED"
        assert server.getState() == STOPPED
        gameSession.getState() >>> [GameSession.State.NOT_INITIALIZED, GameSession.State.NOT_INITIALIZED, GameSession.State.ITERATION_PENDING]

        when: "initializing the server"
        server.initialize()

        then: "the server is INITIALIZED and registered"
        server.getState() == INITIALIZED
        networkServer.registerService(server, server)

        when: "stopping the server"
        server.stop()

        then: "the server is STOPPED and unregistered"
        server.getState() == STOPPED
        networkServer.unregisterService()

        when: "initializing the server again"
        server.initialize()

        then: "the server is INITIALIZED and registered again"
        server.getState() == INITIALIZED
        networkServer.registerService(server, server)

        when: "creating game"
        server.createGame()

        then: "the server in state GAME_CREATED"
        server.getState() == GAME_CREATED

        when: "starting user registration"
        server.startUserRegistration()

        then: "the server is in state GAME_INITIALIZING"
        server.getState() == GAME_INITIALIZING

        when: "registering not enough clients"
        server.register(client1)
        server.register(client2)
        server.unregister(client2.getId())

        then: "starting the game fails"
        server.tryStartGame() == null
        server.getState() == GAME_INITIALIZING

        when: "having sufficient number of clients registered"
        server.register(client2)
        server.register(client3)
        server.register(client4)

        and: "game initialization invoked"
        1 * gameSession.initialize({ it as Set == [client1, client2, client3, client4] as Set })
        1 * gameSession.startGame() >> [initializeGameEvent, interactiveEvent]
        for (def client : [client1, client2, client3, client4]) {
            1 * eventBus.send(GameVerticle.START_GAME_EVENT, GameVerticle.clientAndEventsToJsonObject(client.id, [initializeGameEvent, interactiveEvent]))
        }
        1 * gameSession.getGame() >> game

        and: "game restore invoked"
        1 * gameSession.getEventFactory() >> eventFactory
        1 * eventFactory.createRestoreGameEvent() >> restoreGameEvent
        for (def client : [client3]) {
            1 * eventBus.send(GameVerticle.RESTORE_GAME_EVENT, GameVerticle.clientAndEventsToJsonObject(client.getId(), [restoreGameEvent]))
        }

        then: "the game starts successfully"
        server.tryStartGame() == game
        server.getState() == GAME_ACTIVE

        when: "some clients disconnected"
        server.disconnect(client3.getId())

        then: "the server is in state GAME_PAUSED"
        server.getState() == GAME_PAUSED

        and: "the client is not connected"
        !client3.connected

        when: "clients reconnected"
        server.reconnect(client3.id)

        then: "the server is in state GAME_ACTIVE"
        server.getState() == GAME_ACTIVE

        and: "the client is connected"
        client3.connected

        when: "finishing the game"
        1 * gameSession.getEventFactory() >> eventFactory
        1 * eventFactory.createFinishGameEvent() >> finishGameEvent
        1 * gameSession.playIteration({ List it -> it.size() == 5 && it as Set == [interactiveEvent, interactiveEvent2] as Set }) >> [finishGameEvent]
        server.handle(client1, List.of(interactiveEvent))
        server.handle(client2, List.of(interactiveEvent, interactiveEvent2))
        server.handle(client3, List.of(interactiveEvent))
        server.handle(client4, List.of(interactiveEvent))

        then: "the game is finished"
        server.getState() == GAME_FINISHED
    }

    def "should properly manage state of the server in game reconnection"() {
        given: "server with GoPGameSession in state STOPPED"
        def randomSeed = 12345
        server = new Server(networkServer, new GoPGameSession(randomSeed), eventBus)
        assert server.getState() == STOPPED

        and: "game parameters"
        def gameUuid = UUID.randomUUID()
        def timestamp = System.currentTimeMillis()

        and: "client parameters"
        def player1Uuid = UUID.fromString('00000000-0000-3039-0000-000000010932')
        def player2Uuid = UUID.fromString('00000000-0001-81cd-0000-00000000a8ca')
        def player3Uuid = UUID.fromString('00000000-0000-3039-0000-00000000d431')

        when: "initializing the server"
        server.initialize()

        then: "the server is INITIALIZED and registered"
        server.getState() == INITIALIZED
        networkServer.registerService(server, server)

        when: "restoring a game"
        server.restoreGame(GameSnapshotFactory.createStandardGameSnapshot(gameUuid, timestamp))

        then: "the game starts successfully with clients"
        def clientMap = [:]
        [
                new Client('playerA', player1Uuid),
                new Client('playerB', player2Uuid),
                new Client('playerC', player3Uuid)
        ].forEach { clientMap.put(it.getId(), it) }
        server.clients == clientMap
        server.getState() == GAME_PAUSED

        when: "not all clients reconnected"
        server.reconnect(player1Uuid)
        server.reconnect(player3Uuid)

        client1 = server.clients.get(player1Uuid)
        client2 = server.clients.get(player2Uuid)
        client3 = server.clients.get(player3Uuid)

        then: "the server still not in state GAME_ACTIVE"
        server.getState() == GAME_PAUSED
        !client2.connected

        when: "all clients reconnected"
        server.reconnect(client2.id)

        then: "server is in state GAME_ACTIVE"
        server.getState() == GAME_ACTIVE

        and: "the clients are connected"
        client1.connected
        client2.connected
        client3.connected

        when: "finishing the game"
        server.gameSession.playIteration([])
        server.finishGame()

        then: "the game is finished"
        server.getState() == GAME_FINISHED
    }

    def "created game should be properly abandoned"() {
        when: "game is created"
        server.initialize()
        server.createGame()
        assert server.getState() == GAME_CREATED

        and: "game is abandoned"
        server.abandonGame()
        gameSession.abandonGameAttempt()

        then: "the server is in state INITIALIZED"
        server.getState() == INITIALIZED
    }

    def "initializing game should be properly abandoned"() {
        when: "game is initializing"
        server.initialize()
        server.createGame()
        server.startUserRegistration()
        server.register(client1)
        server.register(client2)
        assert server.getState() == GAME_INITIALIZING

        and: "game is abandoned"
        server.abandonGame()
        gameSession.abandonGameAttempt()

        then: "the server is in state INITIALIZED"
        server.getState() == INITIALIZED
    }

    def "active game should be properly preempted"() {
        when: "game is started"
        server.initialize()
        server.createGame()
        server.startUserRegistration()
        server.register(client1)
        server.register(client2)
        gameSession.initialize(_ as Iterable<Client>)
        gameSession.getState() >>> [GameSession.State.NOT_INITIALIZED, GameSession.State.ITERATION_PENDING]
        1 * gameSession.startGame() >> [initializeGameEvent, interactiveEvent]
        for (def client : [client1, client2,]) {
            1 * eventBus.send(GameVerticle.START_GAME_EVENT, GameVerticle.clientAndEventsToJsonObject(client.id, [initializeGameEvent, interactiveEvent]))
        }
        1 * gameSession.getGame() >> game
        assert server.tryStartGame() == game
        assert server.getState() == GAME_ACTIVE
        assert server.clients.size() == 2

        and: "game is preempted"
        server.preemptGame()
        gameSession.interruptGame()

        then: "the server is in state INITIALIZED"
        server.getState() == INITIALIZED
        server.clients.isEmpty()
    }

    def "disconnected clients should be properly connected"() {
        given: "started game"
        server.initialize()
        server.createGame()
        server.startUserRegistration()
        server.register(client1)
        server.register(client2)
        server.register(client3)
        gameSession.initialize(_ as Iterable<Client>)
        gameSession.getState() >>> [GameSession.State.NOT_INITIALIZED, GameSession.State.ITERATION_PENDING]
        1 * gameSession.startGame() >> [initializeGameEvent, interactiveEvent]
        for (def client : [client1, client2, client3,]) {
            1 * eventBus.send(GameVerticle.START_GAME_EVENT, GameVerticle.clientAndEventsToJsonObject(client.id, [initializeGameEvent, interactiveEvent]))
        }
        1 * gameSession.getGame() >> game
        assert server.tryStartGame() == game
        assert server.getState() == GAME_ACTIVE

        and: "two disconnected clients"
        server.disconnect(client3.getId())
        server.disconnect(client1.getId())
        assert server.getState() == GAME_PAUSED

        when: "reconnecting one client"
        server.reconnect(client1.getId())

        then: "no reconnection happens yet"
        server.reconnectedClients == [client1.getId()] as Set
        server.getState() == GAME_PAUSED

        when: "reconnecting both clients"
        server.reconnect(client3.getId())

        then: "clients are reconnected and the game is active"
        1 * gameSession.getEventFactory() >> eventFactory
        1 * eventFactory.createRestoreGameEvent() >> restoreGameEvent
        for (def client : [client1, client3]) {
            1 * eventBus.send(GameVerticle.RESTORE_GAME_EVENT, GameVerticle.clientAndEventsToJsonObject(client.getId(), [restoreGameEvent]))
        }
        server.getState() == GAME_ACTIVE
    }

    def "paused game should be properly preempted"() {
        when: "game is paused"
        server.initialize()
        server.createGame()
        server.startUserRegistration()
        server.register(client1)
        server.register(client2)
        gameSession.initialize(_ as Iterable<Client>)
        gameSession.getState() >>> [GameSession.State.NOT_INITIALIZED, GameSession.State.ITERATION_PENDING]
        1 * gameSession.startGame() >> [initializeGameEvent, interactiveEvent]
        for (def client : [client1, client2, ]) {
            1 * eventBus.send(GameVerticle.START_GAME_EVENT, GameVerticle.clientAndEventsToJsonObject(client.id, [initializeGameEvent, interactiveEvent]))
        }
        1 * gameSession.getGame() >> game
        assert server.tryStartGame() == game
        server.disconnect(client2.getId())
        assert server.getState() == GAME_PAUSED
        assert server.clients.size() == 2

        and: "game is preempted"
        server.preemptGame()
        gameSession.interruptGame()

        then: "the server is in state INITIALIZED"
        server.getState() == INITIALIZED
        server.clients.isEmpty()
    }

    def "started game should be properly preempted"() {
        when: "game is started"
        server.initialize()
        server.createGame()
        server.startUserRegistration()
        server.register(client1)
        server.register(client2)
        gameSession.initialize(_ as Iterable<Client>)
        gameSession.getState() >>> [GameSession.State.NOT_INITIALIZED, GameSession.State.ITERATION_PENDING]
        server.disconnect(client2.getId())
        assert server.tryStartGame() == null
        assert server.getState() == GAME_STARTED
        assert server.clients.size() == 2

        and: "game is preempted"
        server.preemptGame()
        gameSession.interruptGame()

        then: "the server is in state INITIALIZED"
        server.getState() == INITIALIZED
        server.clients.isEmpty()
    }

    def "paused game should be properly finished"() {
        when: "game is paused"
        server.initialize()
        server.createGame()
        server.startUserRegistration()
        server.register(client1)
        server.register(client2)
        gameSession.initialize(_ as Iterable<Client>)
        gameSession.getState() >>> [GameSession.State.NOT_INITIALIZED, GameSession.State.ITERATION_PENDING]
        1 * gameSession.startGame() >> [initializeGameEvent, interactiveEvent]
        for (def client : [client1, client2, ]) {
            1 * eventBus.send(GameVerticle.START_GAME_EVENT, GameVerticle.clientAndEventsToJsonObject(client.id, [initializeGameEvent, interactiveEvent]))
        }
        1 * gameSession.getGame() >> game
        assert server.tryStartGame() == game
        server.disconnect(client2.getId())
        assert server.getState() == GAME_PAUSED

        and: "game is finished"
        1 * gameSession.getEventFactory() >> eventFactory
        1 * eventFactory.createFinishGameEvent() >> finishGameEvent
        1 * gameSession.playIteration({ List it -> it.size() == 3 && it as Set == [interactiveEvent, interactiveEvent2] as Set }) >> [finishGameEvent]
        server.handle(client1, List.of(interactiveEvent))
        server.handle(client2, List.of(interactiveEvent, interactiveEvent2))

        then: "the server is in state GAME_FINISHED"
        server.getState() == GAME_FINISHED
    }

    def "started game should be properly finished"() {
        when: "game is started"
        server.initialize()
        server.createGame()
        server.startUserRegistration()
        server.register(client1)
        server.register(client2)
        gameSession.initialize(_ as Iterable<Client>)
        gameSession.getState() >>> [GameSession.State.NOT_INITIALIZED, GameSession.State.ITERATION_PENDING]
        server.disconnect(client2.getId())
        assert server.tryStartGame() == null
        assert server.getState() == GAME_STARTED
        assert server.clients.size() == 2

        and: "game is finished"
        1 * gameSession.getEventFactory() >> eventFactory
        1 * eventFactory.createFinishGameEvent() >> finishGameEvent
        1 * gameSession.playIteration({ List it -> it.size() == 3 && it as Set == [interactiveEvent, interactiveEvent2] as Set }) >> [finishGameEvent]
        server.handle(client1, List.of(interactiveEvent))
        server.handle(client2, List.of(interactiveEvent, interactiveEvent2))

        then: "the server is in state GAME_FINISHED"
        server.getState() == GAME_FINISHED
    }

    def "finished game should be properly preempted"() {
        when: "game is started"
        server.initialize()
        server.createGame()
        server.startUserRegistration()
        server.register(client1)
        server.register(client2)
        gameSession.initialize(_ as Iterable<Client>)
        gameSession.getState() >>> [GameSession.State.NOT_INITIALIZED, GameSession.State.ITERATION_PENDING]
        1 * gameSession.startGame() >> [initializeGameEvent, interactiveEvent]
        for (def client : [client1, client2, ]) {
            1 * eventBus.send(GameVerticle.START_GAME_EVENT, GameVerticle.clientAndEventsToJsonObject(client.id, [initializeGameEvent, interactiveEvent]))
        }
        1 * gameSession.getGame() >> game
        assert server.tryStartGame() == game

        and: "game is finished"
        1 * gameSession.getEventFactory() >> eventFactory
        1 * eventFactory.createFinishGameEvent() >> finishGameEvent
        1 * gameSession.playIteration({ List it -> it.size() == 3 && it as Set == [interactiveEvent, interactiveEvent2] as Set }) >> [finishGameEvent]
        server.handle(client1, List.of(interactiveEvent))
        server.handle(client2, List.of(interactiveEvent, interactiveEvent2))
        assert server.getState() == GAME_FINISHED
        assert server.clients.size() == 2

        and: "game is preempted"
        server.preemptGame()
        gameSession.interruptGame()

        then: "the server is in state INITIALIZED"
        server.getState() == INITIALIZED
        server.clients.isEmpty()
    }

    def "should fail with CONFLICT when game not in state ITERATION_PENDING"() {
        given: "game session not in state ITERATION_PENDING"
        gameSession.getState() >>> [GameSession.State.NOT_INITIALIZED, GameSession.State.ITERATION_FINISHED]

        and: "started game"
        startGame()

        when: "handling an event"
        def result = server.handle(client1, [interactiveEvent])

        then: "a CONFLICT occurs"
        result == NetworkServer.Result.CONFLICT
    }

    def "should fail with CONFLICT when client has already submitted events in this round"() {
        given: "game session in state ITERATION_PENDING"
        gameSession.getState() >>> [GameSession.State.NOT_INITIALIZED, GameSession.State.ITERATION_PENDING]

        and: "started game"
        startGame()

        when: "client has already submitted events"
        server.sessionEvents.put(client1.getId(), [interactiveEvent])

        and: "handling an event"
        def result = server.handle(client1, [interactiveEvent])

        then: "a CONFLICT occurs"
        result == NetworkServer.Result.CONFLICT
    }

    def "should accept user events"() {
        given: "game session in state ITERATION_PENDING"
        gameSession.getState() >>> [GameSession.State.NOT_INITIALIZED, GameSession.State.ITERATION_PENDING]

        and: "iteration is played"
        1 * gameSession.playIteration({it as Set == [interactiveEvent, interactiveEvent2] as Set}) >> [generatedEvent, generatedEvent2]

        and: "started game"
        startGame()

        when: "handling an event from client1"
        def result = server.handle(client1, [interactiveEvent])

        then: "events are accepted"
        result == NetworkServer.Result.ACCEPTED
        server.sessionEvents.size() == 1
        server.sessionEvents.get(client1.getId()) == [interactiveEvent]

        when: "handling an event from client2"
        def result1 = server.handle(client2, [interactiveEvent2])

        then: "events are accepted"
        result1 == NetworkServer.Result.ACCEPTED

        and: "iteration is played"
        1 * eventBus.send(GameVerticle.BROADCAST_EVENTS_EVENT, { GameVerticle.jsonArrayToEvents(it) as Set == [generatedEvent, generatedEvent2] as Set })
        server.sessionEvents.isEmpty()
    }

    private def startGame() {
        server.initialize()
        server.createGame()
        server.startUserRegistration()
        server.register(client1)
        server.register(client2)
        client1.setConnected(true)
        client2.setConnected(true)
        gameSession.initialize(_ as Iterable<Client>)
        1 * gameSession.startGame() >> [initializeGameEvent, interactiveEvent]
        for (def client : [client1, client2, ]) {
            1 * eventBus.send(GameVerticle.START_GAME_EVENT, GameVerticle.clientAndEventsToJsonObject(client.id, [initializeGameEvent, interactiveEvent]))
        }
        1 * gameSession.getGame() >> game
        assert server.tryStartGame() == game
    }
}
