package cz.wie.najtmar.gop.server

import cz.wie.najtmar.gop.event.Event
import cz.wie.najtmar.gop.game.EventFactory
import io.vertx.core.Vertx
import io.vertx.core.eventbus.EventBus
import spock.lang.Shared
import spock.lang.Specification
import spock.util.concurrent.BlockingVariable

import javax.json.Json

class GameVerticleSpec extends Specification {

    @Shared
    Vertx vertx
    EventBus eventBus
    BlockingVariable<String> deploymentId
    NetworkServer networkServer
    GameSession gameSession
    GameVerticle objectUnderTest
    Event event
    Event finishGameEvent
    Event initializeGameEvent
    Event produceSettlersUnitEvent1
    Event produceSettlersUnitEvent2
    Event produceWarriorEvent1
    Event produceWarriorEvent2
    Event restoreGameEvent
    UUID clientId
    EventFactory eventFactory

    void setupSpec() {
        vertx = Vertx.vertx()
    }

    void setup() {
        eventBus = vertx.eventBus()
        networkServer = Mock(NetworkServer)
        event = new Event(Json.createObjectBuilder()
                .add("type", "TEST_INTERACTIVE")
                .add("time", 123456)
                .add("key0", "event")
                .build())
        initializeGameEvent = new Event(Json.createObjectBuilder()
                .add("type", "INITIALIZE_GAME")
                .add("time", 123457)
                .add("key0", "initializeGameEvent")
                .build())
        produceSettlersUnitEvent1 = new Event(Json.createObjectBuilder()
                .add("type", "PRODUCE_SETTLERS_UNIT")
                .add("id", 1)
                .add("time", 966)
                .add("position", Json.createObjectBuilder()
                        .add("roadSection", 0)
                        .add("index", 3)
                        .build())
                .add("player", 0)
                .build())
        produceSettlersUnitEvent2 = new Event(Json.createObjectBuilder()
                .add("type", "PRODUCE_SETTLERS_UNIT")
                .add("id", 2)
                .add("time", 966)
                .add("position", Json.createObjectBuilder()
                        .add("roadSection", 1)
                        .add("index", 2)
                        .build())
                .add("player", 0)
                .build())
        produceWarriorEvent1 = new Event(Json.createObjectBuilder()
                .add("type", "PRODUCE_WARRIOR")
                .add("id", 2)
                .add("time", 966)
                .add("position", Json.createObjectBuilder()
                        .add("roadSection", 2)
                        .add("index", 1)
                        .build())
                .add("player", 0)
                .build())
        produceWarriorEvent2 = new Event(Json.createObjectBuilder()
                .add("type", "PRODUCE_WARRIOR")
                .add("id", 3)
                .add("time", 966)
                .add("position", Json.createObjectBuilder()
                        .add("roadSection", 3)
                        .add("index", 0)
                        .build())
                .add("player", 1)
                .build())

        restoreGameEvent = new Event(Json.createObjectBuilder()
                .add("type", "RESTORE_GAME")
                .add("time", 123457)
                .add("key0", "restoreGameEvent")
                .build())
        finishGameEvent = new Event(Json.createObjectBuilder()
                .add("type", "FINISH_GAME")
                .add("time", 123457)
                .add("key0", "finishGameEvent")
                .build())
        objectUnderTest = new GameVerticle(networkServer)
        deploymentId = new BlockingVariable<>()
        eventFactory = Mock(EventFactory)
        vertx.deployVerticle(objectUnderTest, {deploymentId.set(it.result())})
        deploymentId.get()
        clientId = UUID.randomUUID()
    }

    void cleanup() {
        BlockingVariable<Boolean> finished = new BlockingVariable<>()
        vertx.undeploy(deploymentId.get(), {finished.set(true)})
        assert finished.get()
    }

    def "should initialize game on a game initialization event"() {
        when: "expecting event factory and network server invoked"
        BlockingVariable<Boolean> result = new BlockingVariable<>()
        1 * networkServer.sendEvents(clientId, [
                initializeGameEvent,
                produceSettlersUnitEvent1,
                produceWarriorEvent1,
                produceSettlersUnitEvent2,
                produceWarriorEvent2]) >> { result.set(true) }

        and: "initializing the game"
        eventBus.send(
                GameVerticle.START_GAME_EVENT,
                GameVerticle.clientAndEventsToJsonObject(clientId, [
                        initializeGameEvent,
                        produceSettlersUnitEvent1,
                        produceWarriorEvent1,
                        produceSettlersUnitEvent2,
                        produceWarriorEvent2]))
        result.get()

        then: "the expectations are fulfilled"
        result.get()
    }

    def "should restore game on a game restore event"() {
        when: "expecting event factory and network server invoked"
        BlockingVariable<Boolean> result = new BlockingVariable<>()
        1 * networkServer.sendEvents(clientId, [restoreGameEvent]) >> {result.set(true)}

        and: "restoring the game"
        eventBus.send(GameVerticle.RESTORE_GAME_EVENT, GameVerticle.clientAndEventsToJsonObject(clientId, [restoreGameEvent]))
        result.get()

        then: "the expectations are fulfilled"
        result.get()
    }

    // TODO: Add test for RESTORE_GAME handling

    def "should broadcast events"() {
        when: "expecting network server invoked"
        BlockingVariable<Boolean> result = new BlockingVariable<>()
        1 * networkServer.broadcastEvents([event]) >> {result.set(true)}

        and: "playing an iteration"
        eventBus.send(GameVerticle.BROADCAST_EVENTS_EVENT, GameVerticle.eventsToJsonArray([event]))
        result.get()

        then: "the expectations are fulfilled"
        result.get()
    }
}
