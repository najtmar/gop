package cz.wie.najtmar.gop.server.gop

import com.google.inject.Inject
import cz.wie.najtmar.gop.board.Board
import cz.wie.najtmar.gop.board.BoardException
import cz.wie.najtmar.gop.board.BoardGenerator
import cz.wie.najtmar.gop.board.Joint
import cz.wie.najtmar.gop.board.RoadSection
import cz.wie.najtmar.gop.common.Point2D
import cz.wie.najtmar.gop.game.PropertiesReader

import javax.annotation.Nonnull

class TestBoardGenerator implements BoardGenerator {
    private final PropertiesReader propertiesReader;

    @Inject
    TestBoardGenerator(@Nonnull PropertiesReader propertiesReader) {
        this.propertiesReader = propertiesReader;
    }

    @Override
    Board generate() throws BoardException {
        final Board board;
        try {
            board = new Board(propertiesReader.getBoardProperties());
        } catch (IOException exception) {
            throw new BoardException("Reading Board Properties failed.", exception);
        }
        Joint joint0 = (new Joint.Builder())
                .setBoard(board)
                .setIndex(0)
                .setName("joint0")
                .setCoordinatesPair(new Point2D.Double(0.0, 0.0))
                .build();
        Joint joint1 = (new Joint.Builder())
                .setBoard(board)
                .setIndex(1)
                .setName("joint1")
                .setCoordinatesPair(new Point2D.Double(3.0, 0.0))
                .build();
        Joint joint2 = (new Joint.Builder())
                .setBoard(board)
                .setIndex(2)
                .setName("joint2")
                .setCoordinatesPair(new Point2D.Double(0.0, 3.0))
                .build();
        RoadSection roadSection0 = (new RoadSection.Builder())
                .setBoard(board)
                .setIndex(0)
                .setJoints(joint0, joint1)
                .build();
        RoadSection roadSection1 = (new RoadSection.Builder())
                .setBoard(board)
                .setIndex(1)
                .setJoints(joint0, joint2)
                .build();
        board.addJoint(joint0);
        board.addJoint(joint1);
        board.addJoint(joint2);
        board.addRoadSection(roadSection0);
        board.addRoadSection(roadSection1);
        return board;
    }
}
