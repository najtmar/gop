package cz.wie.najtmar.gop.server.jm

import com.sun.net.httpserver.HttpServer
import cz.wie.najtmar.gop.event.Event
import cz.wie.najtmar.gop.server.Client
import cz.wie.najtmar.gop.server.NetworkServer
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll
import spock.util.concurrent.BlockingVariable

import javax.jmdns.JmDNS
import javax.jmdns.ServiceEvent
import javax.jmdns.ServiceListener
import javax.json.Json
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import java.util.function.Supplier

import static cz.wie.najtmar.gop.server.NetworkServer.Result.*

class JmNetworkServerTest extends Specification {

    def static INITIALIZATION_TIMEOUT_SECS = 10.0

    def static CLIENT_NAME = "My client name"
    def static CLIENT_ID = UUID.randomUUID()

    NetworkServer.EventHandler eventHandler
    NetworkServer.RegistrationHandler registrationHandler

    BlockingVariable<ServiceEvent> serviceResolved
    ServiceListener serviceListener

    HttpClient httpClient
    HttpRequest.Builder httpRegisterRequestBuilder
    HttpRequest.Builder httpUnregisterRequestBuilder
    HttpRequest.Builder httpReconnectRequestBuilder
    HttpRequest.Builder httpEventRequestBuilder

    private Supplier<Integer> serverResponse
    private HttpServer httpServer

    @Shared
    Event testEvent

    JmNetworkServer objectUnderTest

    void setup() {
        eventHandler = Mock(NetworkServer.EventHandler)
        registrationHandler = Mock(NetworkServer.RegistrationHandler)
        serviceListener = Mock(ServiceListener)
        serviceResolved = new BlockingVariable<ServiceEvent>(INITIALIZATION_TIMEOUT_SECS)

        objectUnderTest = new JmNetworkServer()
        objectUnderTest.registerService(eventHandler, registrationHandler)

        serviceListener.serviceResolved(_ as ServiceEvent) >> {
            ServiceEvent serviceEvent -> serviceResolved.set(serviceEvent)
        }

        httpClient = HttpClient.newHttpClient()
        httpRegisterRequestBuilder = HttpRequest
                .newBuilder(URI.create("http://[${objectUnderTest.inetAddress.hostAddress}]:${JmNetworkServer.SERVER_PORT}/register"))
        httpUnregisterRequestBuilder = HttpRequest
                .newBuilder(URI.create("http://[${objectUnderTest.inetAddress.hostAddress}]:${JmNetworkServer.SERVER_PORT}/unregister"))
        httpReconnectRequestBuilder = HttpRequest
                .newBuilder(URI.create("http://[${objectUnderTest.inetAddress.hostAddress}]:${JmNetworkServer.SERVER_PORT}/reconnect"))
        httpEventRequestBuilder = HttpRequest
                .newBuilder(URI.create("http://[${objectUnderTest.inetAddress.hostAddress}]:${JmNetworkServer.SERVER_PORT}/event"))

        testEvent = new Event(Json.createObjectBuilder()
                .add("type", "TEST")
                .add("time", 123456)
                .add("key0", "value0")
                .add("key1", 12)
                .build())

        // Create HTTP server.
        httpServer = HttpServer.create(new InetSocketAddress( objectUnderTest.inetAddress, JmNetworkServer.CLIENT_PORT), 0)
        serverResponse = Stub(Supplier)
        httpServer.createContext("/event", {it ->
            it.sendResponseHeaders(serverResponse.get(), 0)
            it.close()
        })
        httpServer.start()
    }

    void cleanup() {
        objectUnderTest.unregisterService()

        httpServer.stop(0)
    }

    def "should connect to registered service"() {
        given: "blocking variables"
        def serviceAdded = new BlockingVariable<ServiceEvent>(INITIALIZATION_TIMEOUT_SECS)
        1 * serviceListener.serviceAdded(_) >> {
            ServiceEvent serviceEvent -> serviceAdded.set(serviceEvent)
        }

        when: "connecting to the service"
        final JmDNS jmdns = JmDNS.create()
        jmdns.addServiceListener(JmNetworkServer.SERVICE_TYPE, serviceListener)

        then: "service added"
        serviceAdded.get() != null

        and: "service resolved"
        def serviceEvent = serviceResolved.get()
        serviceEvent != null
        serviceEvent.getInfo().getInetAddresses().length >= 1

        when: "pinging HTTP server"
        def address = serviceEvent.getInfo().getHostAddresses()[0]
        def port = serviceEvent.getInfo().getPort()
        def httpClient = HttpClient.newHttpClient()
        def httpRequest = HttpRequest
                .newBuilder(URI.create("http://${address}:${port}/_ping"))
                .GET()
                .build()
        def httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString())

        then: "response is 200 OK"
        httpResponse.statusCode() == 200
    }

    @Unroll
    def "invalid registration request should result in 400 BAD REQUEST"() {
        when: "trying to register with invalid request"
        def httpRequest = httpRegisterRequestBuilder
                .POST(HttpRequest.BodyPublishers.ofString(invalidRegistrationRequest))
                .build()
        def httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString())

        then: "response is 400 BAD REQUEST"
        httpResponse.statusCode() == 400

        where:
        invalidRegistrationRequest << [
                """
{
    "traceIt" : "my-trace-id",
    "clientName" : "${CLIENT_NAME}",
    "clientId": "${CLIENT_ID}"
}
""",
                """
{
    "traceId" : "my-trace-id",
    "clientName" : "${CLIENT_NAME}",
    "clientId": "${CLIENT_ID}"
"""
        ]
    }

    @Unroll
    def "client registration result #expectedResult if the registration handler responds with #responseCode"() {
        given: "that client handler accepts"
        1 * registrationHandler.register({ it.id == CLIENT_ID }) >> ACCEPTED

        when: "trying to register the client"
        def httpRequest = httpRegisterRequestBuilder
                .POST(HttpRequest.BodyPublishers.ofString(
                        """
{
    "traceId" : "my-trace-id",
    "clientName" : "${CLIENT_NAME}",
    "clientId": "${CLIENT_ID}"
}
"""))
                .build()
        def httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString())

        then: "response 200 OK"
        httpResponse.statusCode() == 200

        and: "the client is registered"
        def clientRegistrations = objectUnderTest.addressClientMap.collect { it }
        clientRegistrations.size() == 1
        !clientRegistrations[0].key.address.isBlank()
        clientRegistrations[0].value.id == CLIENT_ID
        clientRegistrations[0].value.name == CLIENT_NAME
        objectUnderTest.clientAddressMap.get(CLIENT_ID) == clientRegistrations[0].key

        when: "broadcasting events"
        serverResponse.get() >> responseCode
        def result = objectUnderTest.broadcastEvents([testEvent])

        then:
        result == expectedResult
        (expectedResult ? 0 :1) * registrationHandler.disconnect(CLIENT_ID)

        where:
        responseCode | expectedResult
        200          | true
        400          | false
    }

    def "repeated client registration result succeed if the registration handler accepts"() {
        given: "client"
        def client = new Client(CLIENT_NAME, CLIENT_ID)

        and: "client handler accepts"
        1 * registrationHandler.register({ it.id == CLIENT_ID }) >> ACCEPTED

        and: "client is already registered"
        def oldAddress = JmNetworkServer.Address.of(new InetSocketAddress("11.12.13.14", 0))
        objectUnderTest.addressClientMap.put(oldAddress, client)
        objectUnderTest.clientAddressMap.put(CLIENT_ID, oldAddress)

        when: "trying to register the client"
        def httpRequest = httpRegisterRequestBuilder
                .POST(HttpRequest.BodyPublishers.ofString(
                        """
{
    "traceId" : "my-trace-id",
    "clientName" : "${CLIENT_NAME}",
    "clientId": "${CLIENT_ID}"
}
"""))
                .build()
        def httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString())

        then: "response 200 OK"
        httpResponse.statusCode() == 200

        and: "the client is registered again"
        objectUnderTest.addressClientMap.size() == 2
        def clientRegistrations = objectUnderTest.addressClientMap.findAll { it.key != oldAddress }.collect { it }
        clientRegistrations.size() == 1
        !clientRegistrations[0].key.address.isBlank()
        clientRegistrations[0].value.id == CLIENT_ID
        clientRegistrations[0].value.name == CLIENT_NAME
        objectUnderTest.clientAddressMap.get(CLIENT_ID) == clientRegistrations[0].key
    }

    def "client registration should fail if the address is already registered for another client"() {
        given: "client registered"
        registerClient()

        and: "another client"
        def anotherClient = new Client("Another client", UUID.randomUUID())

        when: "registering another client with the same address"
        def httpRequest = httpRegisterRequestBuilder
                .POST(HttpRequest.BodyPublishers.ofString(
                        """
{
    "traceId" : "my-trace-id",
    "clientName" : "${anotherClient.getName()}",
    "clientId": "${anotherClient.getId()}"
}
"""))
                .build()
        def httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString())

        then: "response 401 NOT AUTHORIZED"
        httpResponse.statusCode() == 401
    }

    @Unroll
    def "client should be disconnected or not depending on the result of sending events"() {
        given: "client registered"
        registerClient()

        when: "sending events"
        serverResponse.get() >> responseCode
        def result = objectUnderTest.sendEvents(CLIENT_ID, [testEvent])

        then:
        result == expectedResult
        (expectedResult ? 0 :1) * registrationHandler.disconnect(CLIENT_ID)

        where:
        responseCode | expectedResult
        200          | true
        400          | false
    }

    @Unroll
    def "client registration should fail with #statusCode when the registration handler answers with #answer"() {
        given: "that client registration is #statusCode"
        1 * registrationHandler.register({ it.id == CLIENT_ID }) >> answer

        when: "trying to register the client"
        def httpRequest = httpRegisterRequestBuilder
                .POST(HttpRequest.BodyPublishers.ofString(
                        """
{
    "traceId" : "my-trace-id",
    "clientName" : "${CLIENT_NAME}",
    "clientId": "${CLIENT_ID}"
}
"""))
                .build()
        def httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString())

        then: "response is #statusCode"
        httpResponse.statusCode() == statusCode

        and: "client registration has not succeeded"
        objectUnderTest.addressClientMap.isEmpty()
        objectUnderTest.clientAddressMap.isEmpty()

        where:
        answer   | statusCode
        REJECTED | 403
        CONFLICT | 409
    }

     @Unroll
    def "invalid unregistration request should result in 400 BAD REQUEST"() {
        given: "registered client"
        registerClient()

        when: "trying to unregister with invalid request"
        def httpRequest = httpUnregisterRequestBuilder
                .POST(HttpRequest.BodyPublishers.ofString(invalidUnregistrationRequest))
                .build()
        def httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString())

        then: "response is 400 BAD REQUEST"
        httpResponse.statusCode() == 400

        where:
        invalidUnregistrationRequest << [
                """
{
    "traceIt" : "my-trace-id",
    "clientId": "${CLIENT_ID}"
}
""",
                """
{
    "traceId" : "my-trace-id",
    "clientId": "${CLIENT_ID}"
"""
        ]
    }

    def "client unregistration succeeds if the registration handler accepts"() {
        given: "registered client"
        registerClient()

        and: "that client handler accepts"
        1 * registrationHandler.unregister(CLIENT_ID) >> ACCEPTED

        when: "trying to unregister the client"
        def httpRequest = httpUnregisterRequestBuilder
                .POST(HttpRequest.BodyPublishers.ofString(
                        """
{
    "traceId" : "my-trace-id",
    "clientId": "${CLIENT_ID}"
}
"""))
                .build()
        def httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString())

        then: "response 200 OK"
        httpResponse.statusCode() == 200

        and: "the client is unregistered"
        def clientRegistrations = objectUnderTest.addressClientMap.collect { it }
        clientRegistrations.isEmpty()
        !objectUnderTest.clientAddressMap.containsKey(CLIENT_ID)
    }

    def "client unregistration succeeds if the client is not registered"() {
        when: "trying to unregister the client that hasn't been registered previously"
        def httpRequest = httpUnregisterRequestBuilder
                .POST(HttpRequest.BodyPublishers.ofString(
                        """
{
    "traceId" : "my-trace-id",
    "clientId": "${CLIENT_ID}"
}
"""))
                .build()
        def httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString())

        then: "unregistration handler is never called"
        0 * registrationHandler.unregister(_)

        then: "response 200 OK"
        httpResponse.statusCode() == 200

        and: "the client remains unregistered"
        def clientRegistrations = objectUnderTest.addressClientMap.collect { it }
        clientRegistrations.isEmpty()
        !objectUnderTest.clientAddressMap.containsKey(CLIENT_ID)
    }

    def "repeated client unregistration succeeds"() {
        given: "registered client"
        registerClient()

        and: "handler accepts unregistration"
        1 * registrationHandler.unregister(CLIENT_ID) >> ACCEPTED

        when: "trying to unregister the client twice"
        def httpRequest = httpUnregisterRequestBuilder
                .POST(HttpRequest.BodyPublishers.ofString(
                        """
{
    "traceId" : "my-trace-id",
    "clientName" : "${CLIENT_NAME}",
    "clientId": "${CLIENT_ID}"
}
"""))
                .build()
        httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString())
        def httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString())

        then: "response 200 OK"
        httpResponse.statusCode() == 200

        and: "the client is unregistered"
        objectUnderTest.addressClientMap.isEmpty()
        !objectUnderTest.clientAddressMap.containsKey(CLIENT_ID)
    }

    def "client unregistration should fail if the client is registered from another address"() {
        given: "client registered with another address"
        objectUnderTest.addressClientMap.put(JmNetworkServer.Address.of(new InetSocketAddress("11.12.13.14", 0)), new Client(CLIENT_NAME, CLIENT_ID))
        objectUnderTest.clientAddressMap.put(CLIENT_ID, JmNetworkServer.Address.of(new InetSocketAddress("11.12.13.14", 0)))

        when: "trying to unregister the client"
       def httpRequest = httpUnregisterRequestBuilder
                .POST(HttpRequest.BodyPublishers.ofString(
                        """
{
    "traceId" : "my-trace-id",
    "clientId": "${CLIENT_ID}"
}
"""))
                .build()
        def httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString())

        then: "response 401 NOT AUTHORIZED"
        httpResponse.statusCode() == 401
    }

    @Unroll
    def "client unregistration should fail with #statusCode when the registration handler answers with #answer"() {
        given: "registered client"
        registerClient()

        and: "registration handler answers with #statusCode"
        1 * registrationHandler.unregister(CLIENT_ID) >> answer

        when: "trying to unregister the client"
        def httpRequest = httpUnregisterRequestBuilder
                .POST(HttpRequest.BodyPublishers.ofString(
                        """
{
    "traceId" : "my-trace-id",
    "clientId": "${CLIENT_ID}"
}
"""))
                .build()
        def httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString())

        then: "response is #statusCode"
        httpResponse.statusCode() == statusCode

        and: "client unregistration has not succeeded"
        objectUnderTest.clientAddressMap.containsKey(CLIENT_ID)
        objectUnderTest.addressClientMap.get(objectUnderTest.clientAddressMap.get(CLIENT_ID)).getId() == CLIENT_ID

        where:
        answer   | statusCode
        REJECTED | 403
        CONFLICT | 409
    }

    @Unroll
    def "client reconnection should succeed if the client is registered and connected == #connected"() {
        given: "client registered"
        registerClient()
        objectUnderTest.addressClientMap.find {it.value.id == CLIENT_ID }.value.connected = connected

        and: "registration handler accepts reconnection"
        1 * registrationHandler.reconnect(CLIENT_ID) >> ACCEPTED

        when: "reconnecting the client"
        def httpRequest = httpReconnectRequestBuilder
                .POST(HttpRequest.BodyPublishers.ofString(
                        """
{
    "traceId" : "my-trace-id",
    "clientName" : "${CLIENT_NAME}",
    "clientId": "${CLIENT_ID}"
}
"""))
                .build()
        def httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString())

        then: "response 200 OK"
        httpResponse.statusCode() == 200

        where:
        connected << [true, false]
    }

    @Unroll
    def "invalid reconnection request should result in 400 BAD REQUEST"() {
        given: "registered client"
        registerClient()

        when: "trying to reconnect with invalid request"
        def httpRequest = httpUnregisterRequestBuilder
                .POST(HttpRequest.BodyPublishers.ofString(invalidUnregistrationRequest))
                .build()
        def httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString())

        then: "response is 400 BAD REQUEST"
        httpResponse.statusCode() == 400

        where:
        invalidUnregistrationRequest << [
                """
{
    "traceIt" : "my-trace-id",
    "clientId": "${CLIENT_ID}",
    "clientName": "${CLIENT_NAME}"
}
""",
                """
{
    "traceIt" : "my-trace-id",
    "clientId": "${CLIENT_NAME}",
    "clientName": "${CLIENT_ID}"
}
""",
                """
{
    "traceId" : "my-trace-id",
    "clientId": "${CLIENT_ID}",
    "clientName": "${CLIENT_NAME}"
""",
                """
{
    "traceId" : "my-trace-id",
    "clientName" : "${CLIENT_NAME}"
}
"""
        ]
    }

    @Unroll
    def "client reconnection should fail with #statusCode when the registration handler answers with #answer"() {
        given: "registered client"
        registerClient()

        and: "registration handler answers with #statusCode"
        1 * registrationHandler.reconnect(CLIENT_ID) >> answer

        when: "trying to reconnect the client"
        def httpRequest = httpReconnectRequestBuilder
                .POST(HttpRequest.BodyPublishers.ofString(
                        """
{
    "traceId" : "my-trace-id",
    "clientId": "${CLIENT_ID}",
    "clientName": "${CLIENT_NAME}"
}
"""))
                .build()
        def httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString())

        then: "response is #statusCode"
        httpResponse.statusCode() == statusCode

        where:
        answer   | statusCode
        REJECTED | 403
        CONFLICT | 409
    }

    @Unroll
    def "invalid event should result in 400 BAD REQUEST"() {
        given: "registered client"
        registerClient()

        when: "trying to send an invalid request"
        def httpRequest = httpEventRequestBuilder
                .POST(HttpRequest.BodyPublishers.ofString(invalidEventRequest))
                .build()
        def httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString())

        then: "response is 400 BAD REQUEST"
        httpResponse.statusCode() == 400

        where:
        invalidEventRequest << [
                """
{
    "traceIt" : "my-trace-id",
    "clientId": "${CLIENT_ID}",
    "eventsBody": [
        ${testEvent.getBody().toString()}
    ]
}
""",
                """
{
    "traceId" : "my-trace-id",
    "clientId": "${CLIENT_ID}",
    "eventsBody": [
        ${testEvent.getBody().toString()}
    ]
"""
        ]
    }

    def "event request from not registered address should result in 401 UNAUTHORIZED"() {
        given: "a different address being registered"
        def differentAddress = JmNetworkServer.Address.of(new InetSocketAddress("11.12.13.14", 0))
        def client = new Client(CLIENT_NAME, CLIENT_ID)
        objectUnderTest.addressClientMap.put(differentAddress, client)
        objectUnderTest.clientAddressMap.put(CLIENT_ID, differentAddress)

        when: "trying to send a request from not registered address"
        def httpRequest = httpEventRequestBuilder
                .POST(HttpRequest.BodyPublishers.ofString("""
{
    "traceId" : "my-trace-id",
    "clientId": "${CLIENT_ID}",
    "eventsBody": [
        ${testEvent.getBody().toString()}
    ]
}
"""))
                .build()
        def httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString())

        then: "response is 401 UNAUTHORIZED"
        httpResponse.statusCode() == 401
    }

    def "event request from not matching client should result in 401 UNAUTHORIZED"() {
        given: "registered client"
        registerClient()

        and: "not matching client id"
        def notMatchingClientId = UUID.randomUUID()

        when: "trying to send a request from not matching client"
        def httpRequest = httpEventRequestBuilder
                .POST(HttpRequest.BodyPublishers.ofString("""
{
    "traceId" : "my-trace-id",
    "clientId": "${notMatchingClientId}",
    "eventsBody": [
        ${testEvent.getBody().toString()}
    ]
}
"""))
                .build()
        def httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString())

        then: "response is 401 UNAUTHORIZED"
        httpResponse.statusCode() == 401
    }

    def "valid events should be processed correctly"() {
        given: "registered client"
        registerClient()

        and: "a valid request"
        def validEventRequest = """
{
    "traceId" : "my-trace-id",
    "clientId": "${CLIENT_ID}",
    "eventsBody": [
        ${testEvent.getBody().toString()},
        ${testEvent.getBody().toString()}
    ]
}
"""

        and: "event handler accepting the event"
        1 * eventHandler.handle({ it.id == CLIENT_ID }, [testEvent, testEvent]) >> ACCEPTED

        when: "trying to send a valid request"
        def httpRequest = httpEventRequestBuilder
                .POST(HttpRequest.BodyPublishers.ofString(validEventRequest))
                .build()
        def httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString())

        then: "response is 200 OK"
        httpResponse.statusCode() == 200
    }

    @Unroll
    def "event processing should fail with #statusCode when the registration handler answers with #answer"() {
        given: "registered client"
        registerClient()

        and: "a valid request"
        def validEventRequest = """
{
    "traceId" : "my-trace-id",
    "clientId": "${CLIENT_ID}",
    "eventsBody": [
        ${testEvent.getBody().toString()}
    ]
}
"""

        and: "event handler rejecting the event"
        1 * eventHandler.handle({ it.id == CLIENT_ID }, [testEvent]) >> answer

        when: "trying to send a valid request"
        def httpRequest = httpEventRequestBuilder
                .POST(HttpRequest.BodyPublishers.ofString(validEventRequest))
                .build()
        def httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString())

        then: "response is #statusCode"
        httpResponse.statusCode() == statusCode

        where:
        answer   | statusCode
        REJECTED | 403
        CONFLICT | 409
    }

    def registerClient() {
        1 * registrationHandler.register({ it.id == CLIENT_ID }) >> ACCEPTED
        def httpRequest = httpRegisterRequestBuilder
                .POST(HttpRequest.BodyPublishers.ofString(
                        """
{
    "traceId" : "my-trace-id",
    "clientName" : "${CLIENT_NAME}",
    "clientId": "${CLIENT_ID}"
}
"""))
                .build()
        assert httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString()).statusCode() == 200
    }

}
