package cz.wie.najtmar.gop.server

import com.google.gson.JsonObject
import com.google.gson.JsonParser

class GameSnapshotFactory {

    static JsonObject createStandardGameSnapshot(UUID id, long timestamp) {
        new JsonParser().parseString(
                """
{
  "gameProperties": {
    "gamePropertiesSet": {
      "City.growActionProgressFactor": "0.2",
      "City.produceActionProgressFactor": "0.667",
      "City.repairActionProgressFactor": "0.333",
      "Factory.maxProductionDelta": "0.125",
      "Game.interruptTimePeriod": "100",
      "Game.maxPlayerCount": "4",
      "Game.startTime": "123456",
      "Game.timeDelta": "1",
      "Prawn.attackSuccessProbability": "0.8",
      "Prawn.basicVelocity": "1.0",
      "Prawn.velocityDirContribFactor": "2.0",
      "Warrior.maxAttackStrength": "0.3"
    },
    "maxPlayerCount": 4,
    "startTime": 123456,
    "timeDelta": 1,
    "interruptTimePeriod": 100,
    "basicVelocity": 1.0,
    "velocityDirContribFactor": 2.0,
    "produceActionProgressFactor": 0.667,
    "repairActionProgressFactor": 0.333,
    "growActionProgressFactor": 0.2,
    "attackSuccessProbability": 0.8,
    "maxAttackStrength": 0.3,
    "maxProductionDelta": 0.125
  },
  "state": "RUNNING",
  "id": "${id.toString()}",
  "timestamp": ${timestamp},
  "time": 123456,
  "result": null,
  "playersByState": null,
  "board": {
    "boardPropertiesSet": {
      "propertiesSet": {
        "Board.backgroundImage": "/images/gop-background.jpg",
        "Board.stepSize": "1.0",
        "Board.xsize": "15",
        "Board.ysize": "10"
      },
      "stepSize": 1.0,
      "backgroundImage": "/images/gop-background.jpg",
      "xsize": 15,
      "ysize": 10,
      "libGdx": false
    },
    "joints": [
      {
        "index": 0,
        "name": "joint0",
        "coordinatesPair": {
          "xcoord": 0.0,
          "ycoord": 0.0
        }
      },
      {
        "index": 1,
        "name": "joint1",
        "coordinatesPair": {
          "xcoord": 3.0,
          "ycoord": 0.0
        }
      },
      {
        "index": 2,
        "name": "joint2",
        "coordinatesPair": {
          "xcoord": 3.0,
          "ycoord": 3.0
        }
      }
    ],
    "roadSections": [
      {
        "index": 0,
        "joints": [
          0,
          1
        ],
        "positionsCount": 4,
        "length": 3.0,
        "stepSize": 1.0,
        "angle": 0.0,
        "halves": [
          {
            "end": "BACKWARD",
            "hashCode": 962
          },
          {
            "end": "FORWARD",
            "hashCode": 961
          }
        ]
      },
      {
        "index": 1,
        "joints": [
          1,
          2
        ],
        "positionsCount": 4,
        "length": 3.0,
        "stepSize": 1.0,
        "angle": 1.5707963267948966,
        "halves": [
          {
            "end": "BACKWARD",
            "hashCode": 993
          },
          {
            "end": "FORWARD",
            "hashCode": 992
          }
        ]
      }
    ]
  },
  "players": [
    {
      "user": {
        "name": "playerA",
        "id": "00000000-0000-3039-0000-000000010932"
      },
      "state": "IN_GAME",
      "index": 0
    },
    {
      "user": {
        "name": "playerB",
        "id": "00000000-0001-81cd-0000-00000000a8ca"
      },
      "state": "IN_GAME",
      "index": 1
    },
    {
      "user": {
        "name": "playerC",
        "id": "00000000-0000-3039-0000-00000000d431"
      },
      "state": "IN_GAME",
      "index": 2
    },
    {
      "user": {
        "name": "playerD",
        "id": "00000000-0000-81cd-0000-000000010932"
      },
      "state": "ABANDONED",
      "index": 3
    }
  ],
  "prawnMap": {
    "0": 0,
    "1": 1
  },
  "removedPrawnMap": {},
  "prawns": [
    {
      "player": 0,
      "id": 0,
      "currentPosition": {
        "roadSection": 0,
        "index": 1,
        "joint": null,
        "hashCode": 962
      },
      "itinerary": null,
      "lastMoveTime": 123456,
      "energy": 1.0,
      "type": "Warrior",
      "warriorDirection": 0.0
    },
    {
      "player": 1,
      "id": 1,
      "currentPosition": {
        "roadSection": 0,
        "index": 2,
        "joint": null,
        "hashCode": 963
      },
      "itinerary": null,
      "lastMoveTime": 123456,
      "energy": 1.0,
      "type": "Warrior",
      "warriorDirection": 0.0
    }
  ],
  "playerPrawns": {
    "0": [
      0
    ],
    "1": [
      1
    ]
  },
  "cityMap": {
    "0": {
      "name": "cityA",
      "id": 0,
      "joint": 0,
      "factories": [
        {
          "state": "ENABLED",
          "index": 0,
          "action": null
        }
      ],
      "player": 0,
      "size": 1
    },
    "1": {
      "name": "cityB",
      "id": 1,
      "joint": 1,
      "factories": [
        {
          "state": "ENABLED",
          "index": 0,
          "action": null
        },
        {
          "state": "DISABLED",
          "index": 1,
          "action": null
        }
      ],
      "player": 1,
      "size": 1
    }
  },
  "removedCityMap": {},
  "playerCities": {
    "0": [
      0
    ],
    "1": [
      1
    ]
  },
  "jointToCityMap": {
    "0": 0,
    "1": 1
  },
  "battles": [
    {
      "prawns": [
        0,
        1
      ],
      "normalizedPositions": [
        {
          "roadSection": 0,
          "index": 1,
          "joint": null,
          "hashCode": 962
        },
        {
          "roadSection": 0,
          "index": 2,
          "joint": null,
          "hashCode": 963
        }
      ],
      "attackDirections": [
        "FORWARD",
        "BACKWARD"
      ],
      "hashCode": 955267,
      "state": "PLANNED"
    }
  ],
  "removedBattles": [],
  "playerVisibleRoadSectionHalves": {},
  "playerDiscoveredRoadSectionHalves": {},
  "playerVisiblePrawns": {},
  "playerDiscoveredCities": {},
  "playerVisibleCities": {}
  }
"""
        ).getAsJsonObject()
    }

}
