package cz.wie.najtmar.gop.server

import cz.wie.najtmar.gop.event.Event
import cz.wie.najtmar.gop.server.jm.JmNetworkServer

class Handler implements NetworkServer.RegistrationHandler, NetworkServer.EventHandler {

    @Override
    NetworkServer.Result handle(Client client, List<Event> event) {
        println "handling events ${event.join(',')}"
        return NetworkServer.Result.ACCEPTED
    }

    @Override
    NetworkServer.Result register(Client client) {
        println "registering client ${client.name}/${client.id}"
        return NetworkServer.Result.ACCEPTED
    }

    @Override
    NetworkServer.Result unregister(UUID clientId) {
        println "unregistering client ${clientId}"
        return NetworkServer.Result.ACCEPTED
    }

    @Override
    NetworkServer.Result disconnect(UUID clientId) {
        println "client ${clientId} disconnected"
        return NetworkServer.Result.ACCEPTED
    }

    @Override
    NetworkServer.Result reconnect(UUID clientId) {
        println "client ${clientId} reconnected"
        return NetworkServer.Result.ACCEPTED
    }
}


class ServerTestRun {

    Handler handler
    JmNetworkServer jmNetworkServer

    def init() {
        handler = new Handler()
        jmNetworkServer = new JmNetworkServer()
        jmNetworkServer.registerService(handler, handler)
    }

    def close() {
        jmNetworkServer.unregisterService()
        println "Done"
        System.exit(0)
    }

    static void main(String[] args) {
        ServerTestRun serverTestRun = new ServerTestRun()
        serverTestRun.init()
        print "Press <Enter> to finish"
        System.in.newReader().readLine()
        serverTestRun.close()
    }


}
