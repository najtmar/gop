package cz.wie.najtmar.gop.client

import cz.wie.najtmar.gop.client.jm.JmNetworkClient
import spock.lang.Specification

class ClientSpec extends Specification {

    Client objectUnderTest

    def setup() {
        objectUnderTest = new Client()
    }

    def "should register and unregister servers"() {
        when: "registering some servers"
        objectUnderTest.registerServer("12.10.120.76.local.", "12.10.120.76", "Server")
        objectUnderTest.registerServer("10.12.112.83.local.", "10.12.112.83", "Another server")
        objectUnderTest.registerServer("192.168.13.125.local.", "192.168.13.125", "Yet another server")

        then: "servers registered"
        objectUnderTest.servers == [
                Server.of("12.10.120.76.local.", "Server", InetSocketAddress.createUnresolved("12.10.120.76", JmNetworkClient.SERVER_PORT)),
                Server.of("10.12.112.83.local.", "Another server", InetSocketAddress.createUnresolved("10.12.112.83", JmNetworkClient.SERVER_PORT)),
                Server.of("192.168.13.125.local.", "Yet another server", InetSocketAddress.createUnresolved("192.168.13.125", JmNetworkClient.SERVER_PORT)),
        ]

        when: "unregistering a server"
        objectUnderTest.unregisterServer("10.12.112.83.local.")

        then: "server successfully unregistered"
        objectUnderTest.servers == [
                Server.of("12.10.120.76.local.", "Server", InetSocketAddress.createUnresolved("12.10.120.76", JmNetworkClient.SERVER_PORT)),
                Server.of("192.168.13.125.local.", "Yet another server", InetSocketAddress.createUnresolved("192.168.13.125", JmNetworkClient.SERVER_PORT)),
        ]

        when: "unregistering not registered server"
        objectUnderTest.unregisterServer("72.18.122.103.local.")

        then: "nothing changes"
        objectUnderTest.servers == [
                Server.of("12.10.120.76.local.","Server", InetSocketAddress.createUnresolved("12.10.120.76", JmNetworkClient.SERVER_PORT)),
                Server.of("192.168.13.125.local.", "Yet another server", InetSocketAddress.createUnresolved("192.168.13.125", JmNetworkClient.SERVER_PORT)),
        ]

        when: "registering another server"
        objectUnderTest.registerServer("103.48.13.152.local.", "103.48.13.152", "New server")

        then: "new server is registered"
        objectUnderTest.servers == [
                Server.of("12.10.120.76.local.", "Server", InetSocketAddress.createUnresolved("12.10.120.76", JmNetworkClient.SERVER_PORT)),
                Server.of("192.168.13.125.local.", "Yet another server", InetSocketAddress.createUnresolved("192.168.13.125", JmNetworkClient.SERVER_PORT)),
                Server.of("103.48.13.152.local.", "New server", InetSocketAddress.createUnresolved("103.48.13.152", JmNetworkClient.SERVER_PORT)),
        ]

        when: "unregistering all servers"
        objectUnderTest.unregisterServer("103.48.13.152.local.")
        objectUnderTest.unregisterServer("12.10.120.76.local.")
        objectUnderTest.unregisterServer("192.168.13.125.local.")

        then: "all servers are unregistered"

        objectUnderTest.servers == []
    }
}
