package cz.wie.najtmar.gop.client.jm

import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpServer
import cz.wie.najtmar.gop.client.NetworkClient
import cz.wie.najtmar.gop.event.Event
import io.vertx.core.Vertx
import spock.lang.Specification
import spock.util.concurrent.BlockingVariable

import javax.jmdns.JmDNS
import javax.jmdns.ServiceInfo
import javax.json.Json
import javax.json.JsonObject

class JmNetworkClientTest extends Specification {

    private static final INITIALIZATION_TIMEOUT_SECS = 10.0
    private static final String SERVICE_TYPE = "_gop._tcp.local."
    private static final String SERVICE_NAME = "Game of Prawns server"
    private static final String SERVICE_DESCRIPTION = "Game of Prawns game server"
    private static final int SERVER_PORT = 1979

    def static final CLIENT_NAME = "My client name"
    def static final CLIENT_ID = UUID.randomUUID()

    static Vertx vertx

    JmDNS jmdns
    ServiceInfo serviceInfo

    static HttpServer httpServer
    static int expectedStatusCode
    static URI lastRequestUri
    static String lastRequestMethod
    static JsonObject lastRequestBody

    NetworkClient.RegistrationHandler registrationHandler
    NetworkClient.EventHandler eventHandler
    JmNetworkClient objectUnderTest

    Event testEvent1, testEvent2, testEvent3

    def setupSpec() {
        vertx = Vertx.vertx()
        httpServer = HttpServer.create(new InetSocketAddress( "localhost", JmNetworkClient.SERVER_PORT), 0)
        def handler = {HttpExchange it ->
            assert it.getRequestMethod() == "POST"
            def requestBody = it.getRequestBody().readLines() as List<String>
            assert requestBody.size() == 1
            lastRequestUri = it.getRequestURI()
            lastRequestMethod = it.getRequestMethod()
            lastRequestBody = Json.createReader(new StringReader(requestBody[0])).readObject()
            it.sendResponseHeaders(expectedStatusCode, 0)
            it.getResponseBody().write("".bytes)
            it.close()
        }
        httpServer.createContext("/register", handler)
        httpServer.createContext("/unregister", handler)
        httpServer.createContext("/reconnect", handler)
        httpServer.createContext("/event", handler)
        httpServer.start()
    }

    def setup() {
        jmdns = JmDNS.create()
        serviceInfo = ServiceInfo.create(SERVICE_TYPE, SERVICE_NAME, SERVER_PORT, SERVICE_DESCRIPTION)
        jmdns.registerService(serviceInfo)

        registrationHandler = Mock(NetworkClient.RegistrationHandler)
        eventHandler = Mock(NetworkClient.EventHandler)
        expectedStatusCode = 200
        objectUnderTest = new JmNetworkClient()

        testEvent1 = new Event(Json.createObjectBuilder()
                .add("type", "TEST")
                .add("time", 123456)
                .add("key0", "value0")
                .add("key1", 12)
                .build())
        testEvent2 = new Event(Json.createObjectBuilder()
                .add("type", "TEST")
                .add("time", 123457)
                .add("key0", "value1")
                .add("key1", 13)
                .build())
        testEvent3 = new Event(Json.createObjectBuilder()
                .add("type", "TEST")
                .add("time", 123458)
                .add("key0", "value2")
                .add("key1", 14)
                .build())
    }

    def cleanup() {
        jmdns.unregisterService(serviceInfo)
        if (objectUnderTest.serverAddress != null) {
            objectUnderTest.disconnect()
        }
        if (objectUnderTest.initialized) {
            objectUnderTest.stop()
        }
    }

    def cleanupSpec() {
        vertx.close().result()
        httpServer.stop(0)
    }

    def "should discover an already registered server" () {
        given: "blocking variables"
        def keyVariable = new BlockingVariable(INITIALIZATION_TIMEOUT_SECS)
        def hostAddressVariable = new BlockingVariable<String>(INITIALIZATION_TIMEOUT_SECS)
        def nameVariable = new BlockingVariable<String>(INITIALIZATION_TIMEOUT_SECS)
        registrationHandler.registerServer(_ as String, _ as String, _ as String) >> {
            String key, String hostAddress, String name ->
                keyVariable.set(key)
                hostAddressVariable.set(hostAddress)
                nameVariable.set(name)
        }

        when: "initializing a client"
        objectUnderTest.initialize(registrationHandler, eventHandler, CLIENT_NAME, CLIENT_ID)

        then: "service registered"
        def key = keyVariable.get()
        key != null
        def hostAddress = hostAddressVariable.get()
        hostAddress != null
        def name = nameVariable.get()
        name == SERVICE_NAME
    }

    def "should correctly connect to a server"() {
        given: "initialized client"
        initializeClient()

        and: "server address"
        def serverAddress = new InetSocketAddress("localhost", JmNetworkClient.SERVER_PORT)

        when: "connecting to a server"
        def result = objectUnderTest.connect(serverAddress)

        then: "request is properly sent"
        lastRequestMethod == "POST"
        lastRequestUri.getPath() == "/register"
        lastRequestBody.getString("traceId") != null
        lastRequestBody.getString("clientId") != null
        lastRequestBody.getString("clientName") != null

        and: "client is correctly connected to the server"
        result

        when: "disconnecting the client"
        result = objectUnderTest.disconnect()

        then: "disconnection succeeds"
        result
        lastRequestMethod == "POST"
        lastRequestUri.getPath() == "/unregister"
        lastRequestBody.getString("traceId") != null
        lastRequestBody.getString("clientId") != null

        when: "stopping the client"
        assert objectUnderTest.stop()
        lastRequestBody = null

        and: "reconnecting the client"
        initializeClient()
        result = objectUnderTest.connect(serverAddress)

        then: "request is properly sent"
        lastRequestMethod == "POST"
        lastRequestUri.getPath() == "/register"
        lastRequestBody.getString("traceId") != null
        lastRequestBody.getString("clientId") != null
        lastRequestBody.getString("clientName") != null

        and: "client is correctly connected to the server"
        result
    }

    def "should fail connecting to a server if connection fails"() {
        given: "initialized client"
        initializeClient()

        and: "connection is expected to fail"
        expectedStatusCode = 400

        and: "server address"
        def serverAddress = new InetSocketAddress("localhost", JmNetworkClient.SERVER_PORT)

        when: "connecting to a server"
        def result = objectUnderTest.connect(serverAddress)

        then: "client fails to be connected to the server"
        !result
    }

    def "should correctly reconnect to a server"() {
        given: "initialized client"
        initializeClient()

        and: "server address"
        def serverAddress = new InetSocketAddress("localhost", JmNetworkClient.SERVER_PORT)

        when: "connecting to a server"
        assert objectUnderTest.connect(serverAddress)

        and: "reconnecting to a server"
        def result = objectUnderTest.reconnect()

        then: "request is properly sent"
        lastRequestMethod == "POST"
        lastRequestUri.getPath() == "/reconnect"
        lastRequestBody.getString("traceId") != null
        lastRequestBody.getString("clientId") != null
        lastRequestBody.getString("clientName") != null

        and: "client is correctly reconnected to the server"
        result
    }

    def "should fail reconnecting to a server if connection fails"() {
        given: "initialized client"
        initializeClient()

        and: "server address"
        def serverAddress = new InetSocketAddress("localhost", JmNetworkClient.SERVER_PORT)

        and: "connection to a server"
        assert objectUnderTest.connect(serverAddress)

        and: "reconnection is expected to fail"
        expectedStatusCode = 400

        when: "reconnecting to a server"
        def result = objectUnderTest.reconnect()

        then: "client fails to be reconnected to the server"
        !result
    }

    def "should disconnect in the client is connected"() {
        given: "initialized client"
        initializeClient()

        and: "server address"
        def serverAddress = new InetSocketAddress("localhost", JmNetworkClient.SERVER_PORT)

        and: "connected to a server"
        assert objectUnderTest.connect(serverAddress)

        when: "disconnecting from the server"
        def result = objectUnderTest.disconnect()

        then: "client is correctly disconnected from the server"
        result
    }

    def "should correctly send events to the server"() {
        given: "initialized and connected client"
        initializeClient()
        connectClient()

        when: "sending events to the server"
        def result = objectUnderTest.sendEvents(
                new InetSocketAddress("localhost", JmNetworkClient.SERVER_PORT),
                [testEvent1, testEvent2, testEvent3])

        then: "the events were correctly sent"
        result
        lastRequestMethod == "POST"
        lastRequestUri.getPath() == "/event"
        lastRequestBody.getString("traceId") != null
        lastRequestBody.getString("clientId") == CLIENT_ID.toString()
        def eventsBody = lastRequestBody.getJsonArray("eventsBody")
        eventsBody.size() == 3
        eventsBody.getJsonObject(0) == testEvent1.getBody()
        eventsBody.getJsonObject(1) == testEvent2.getBody()
        eventsBody.getJsonObject(2) == testEvent3.getBody()
    }

    def initializeClient() {
        assert objectUnderTest.initialize(registrationHandler, eventHandler, CLIENT_NAME, CLIENT_ID)
    }

    def connectClient() {
        assert objectUnderTest.connect(new InetSocketAddress("localhost", JmNetworkClient.SERVER_PORT))
    }

}
