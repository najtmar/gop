package cz.wie.najtmar.gop.client

import cz.wie.najtmar.gop.client.jm.JmNetworkClient
import cz.wie.najtmar.gop.event.Event

class Handler implements NetworkClient.RegistrationHandler, NetworkClient.EventHandler {

    private static final int SERVER_PORT = 1979;

    JmNetworkClient jmNetworkClient
    String address

    Handler(JmNetworkClient jmNetworkClient) {
        this.jmNetworkClient = jmNetworkClient
    }

    @Override
    void registerServer(String key, String hostAddress, String name) {
        println "registerServer(key: ${key}, hostAddress: ${hostAddress}, name: ${name})"
        address =  hostAddress
        jmNetworkClient.sendEvents(InetSocketAddress.createUnresolved(address, SERVER_PORT), [])
        println "events sent to ${hostAddress}:${name}"
    }

    @Override
    void unregisterServer(String key) {
        println "unregisterServer(hostAddress: ${key})"
    }

    @Override
    NetworkClient.Result handle(List<Event> events) {
        println "handle(events: ${events})"
        return NetworkClient.Result.ACCEPTED
    }
}

class ClientTestRun {

    static final String NAME = "Client"
    static final UUID ID = UUID.randomUUID()

    Handler handler
    JmNetworkClient jmNetworkClient

    def init() {
        jmNetworkClient = new JmNetworkClient()
        handler = new Handler(jmNetworkClient)
        jmNetworkClient.initialize(handler, handler, NAME, ID)
    }

    static void main(String[] args) {
        ClientTestRun clientTestRun = new ClientTestRun()
        clientTestRun.init()
    }

}
