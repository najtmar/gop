package cz.wie.najtmar.gop.client;

import lombok.RequiredArgsConstructor;
import lombok.Value;

import java.net.InetSocketAddress;

@Value
@RequiredArgsConstructor(staticName = "of")
class Server {

    String key;
    String name;
    InetSocketAddress address;
}
