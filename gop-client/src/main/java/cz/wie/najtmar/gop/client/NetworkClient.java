package cz.wie.najtmar.gop.client;

import cz.wie.najtmar.gop.event.Event;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.UUID;

public interface NetworkClient {

    enum Result {
        ACCEPTED,
        REJECTED,
        CONFLICT
    }

    interface RegistrationHandler {

        void registerServer(String key, String hostAddress, String name);

        void unregisterServer(String key);

    }

    interface EventHandler {

        Result handle(List<Event> events);

    }

    boolean initialize(RegistrationHandler registrationHandler, EventHandler eventHandler, String name, UUID id);

    boolean stop();

    boolean connect(InetSocketAddress serverAddress);

    boolean reconnect();

    boolean sendEvents(InetSocketAddress serverAddress, List<Event> events);

    boolean disconnect();

}
