package cz.wie.najtmar.gop.client;

import cz.wie.najtmar.gop.event.Klonable;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import javax.json.JsonArray;

@Value
@Builder(toBuilder = true)
public class EventsPayload implements Klonable<EventsPayload> {

    @NonNull
    String traceId;
    @NonNull
    JsonArray eventsBody;

    @Override
    public EventsPayload klone() {
        return this.toBuilder().build();
    }
}
