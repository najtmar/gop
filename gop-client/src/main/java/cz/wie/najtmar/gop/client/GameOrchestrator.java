package cz.wie.najtmar.gop.client;

import cz.wie.najtmar.gop.board.RoadSection;
import cz.wie.najtmar.gop.game.*;

public interface GameOrchestrator {

    void initialize();

    void addCity(City city);

    void addPrawn(Prawn prawn);

    void finishGame();

    void abandonGameAttempt();

    void interruptGame();

    void addPlayer(Player player);

    void abandonPlayer(Player player);

    void changePlayerState(Player player, Player.State state);

    void movePrawn(Prawn prawn, Move move);

    void stayPrawn(Prawn prawn, long stayTime);  // ?

    void setPrawnEnergy(Prawn prawn);

    void destroyPrawn(Prawn prawn);

    void setProduceWarriorAction(CityFactory cityFactory, double direction);

    void setProduceSettlersUnitAction(CityFactory cityFactory);

    void setRepairPrawnAction(CityFactory cityFactory, Prawn prawn);

    void setGrowCityAction(City city);

    void orderCityCreation(SettlersUnit settlersUnit, City city);

    void finishAction(CityFactory cityFactory);

    void changeCitySize(City city);

    void captureCity(City city);

    void destroyCity(City city);

    void addBattle(Battle battle);

    void removeBattle(Battle battle);

    void setFactoryProgress(CityFactory factoryProgress);

    void setPrawnItinerary(Prawn prawn, Itinerary itinerary);

    void clearPrawnItinerary(Prawn prawn);

    void showPrawn(Prawn prawn);

    void hidePrawn(Prawn prawn);

    void showCity(City city);

    void showCityDestroyed(City city);

    void discoverRoadSectionHalf(RoadSection.Half roadSectionHalf);

}
