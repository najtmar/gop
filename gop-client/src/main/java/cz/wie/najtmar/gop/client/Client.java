package cz.wie.najtmar.gop.client;

import com.google.common.base.Preconditions;
import cz.wie.najtmar.gop.client.jm.JmNetworkClient;
import cz.wie.najtmar.gop.event.Event;
import lombok.Getter;

import java.net.InetSocketAddress;
import java.util.*;
import java.util.logging.Logger;

public class Client implements NetworkClient.EventHandler, NetworkClient.RegistrationHandler {

    private static final Logger LOGGER = Logger.getLogger(Client.class.getName());

    // TODO: Create unique names.
    private static final String TEMP_NAME = "Sample name";

    // TODO: Create unique UUID.
    private static final UUID TEMP_UUID = UUID.randomUUID();

    enum State {
        STOPPED,
        INITIALIZED,
        SEARCHING,
        CONNECTED,
        DISCONNECTED,
        RESTORING,
    }
    @Getter
    private State state;

    private final NetworkClient networkClient;

    private final List<Server> servers = new LinkedList<>();

    private Server selectedServer;

    public Client(NetworkClient networkClient) {
        this.networkClient = networkClient;
        this.state = State.STOPPED;
    }

    @Override
    public void registerServer(String key, String hostAddress, String name) {
        Server server = Server.of(key, name, InetSocketAddress.createUnresolved(hostAddress, JmNetworkClient.SERVER_PORT));
        if (!servers.contains(server)) {
            servers.add(server);
        }
    }

    @Override
    public void unregisterServer(String key) {
        for (Iterator<Server> iter = servers.iterator(); iter.hasNext();) {
            Server server = iter.next();
            if (server.getKey().equals(key)) {
                iter.remove();
                break;
            }
        }
    }

    @Override
    public NetworkClient.Result handle(List<Event> events) {
        // TODO: Implement it.
        return null;
    }

    public void initialize() {
        Preconditions.checkArgument(state == State.STOPPED);
        this.state = State.INITIALIZED;
    }

    public void stop() {
        Preconditions.checkArgument(state == State.INITIALIZED);
        this.state = State.STOPPED;
    }

    public void startSearching() {
        Preconditions.checkArgument(state == State.INITIALIZED);
        Preconditions.checkArgument(this.networkClient.initialize(this, this, TEMP_NAME, TEMP_UUID));
        this.state = State.SEARCHING;
    }

    public void stopSearching() {
        Preconditions.checkArgument(state == State.SEARCHING);
        Preconditions.checkArgument(this.networkClient.stop());
        this.servers.clear();
        this.state = State.INITIALIZED;
    }

    public boolean selectServer(int serverIndex, String name) {
        Preconditions.checkArgument(state == State.SEARCHING);
        Server selectedServer = servers.get(serverIndex);
        if (networkClient.connect(this.selectedServer.getAddress())) {
            this.selectedServer = selectedServer;
            state = State.CONNECTED;
            LOGGER.info(String.format("Client connected to server %s", this.selectedServer));
            return true;
        }
        LOGGER.warning(String.format("Connection to server %s failed", this.selectedServer));
        return false;
    }

    // TODO: Handle reconnection and states DISCONNECTED and RESTORING.
}
