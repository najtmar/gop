package cz.wie.najtmar.gop.client.jm;

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.gson.*;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import cz.wie.najtmar.gop.client.EventsPayload;
import cz.wie.najtmar.gop.client.NetworkClient;
import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.event.EventProcessingException;
import cz.wie.najtmar.gop.event.Klonable;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import javax.jmdns.JmDNS;
import javax.jmdns.ServiceEvent;
import javax.jmdns.ServiceListener;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonValue;
import javax.net.ssl.SSLSession;
import java.io.*;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpHeaders;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.BiFunction;
import java.util.logging.Logger;

public class JmNetworkClient implements NetworkClient {

    private static final Logger LOGGER = Logger.getLogger(JmNetworkClient.class.getName());

    public static final String SERVICE_TYPE = "_gop._tcp.local.";
    public static final int SERVER_PORT = 1979;
    public static final int CLIENT_PORT = 1980;

    private static final long DEFAULT_SERVER_REQUEST_TIMEOUT_MILLIS = 5000;

    private static final String SERVER_ENDPOINT_TEMPLATE = "http://%s:%d/%s";

    private static final Gson GSON = new GsonBuilder()
            .registerTypeAdapter(javax.json.JsonObject.class, (JsonDeserializer<javax.json.JsonObject>) (json, typeOfT, context) -> {
                JsonObject jObject = json.getAsJsonObject();
                return Json.createReader(new StringReader(jObject.toString())).readObject();
            })
            .registerTypeAdapter(javax.json.JsonArray.class, (JsonDeserializer<javax.json.JsonArray>) (jsonArray, typeOfT, context) -> {
                com.google.gson.JsonArray jArray = jsonArray.getAsJsonArray();
                return Json.createReader(new StringReader(jArray.toString())).readArray();
            })
            .create();

    private long serverRequestTimeoutMillis = DEFAULT_SERVER_REQUEST_TIMEOUT_MILLIS;
    private JmDNS jmdns;
    private HttpServer httpServer;
    private HttpClient httpClient;

    private String name;
    private UUID id;
    private InetAddress inetAddress;
    private InetSocketAddress serverAddress;

    private RegistrationHandler registrationHandler;
    private ServiceListener serviceListener;
    private EventHandler eventHandler;

    boolean initialized = false;

    @RequiredArgsConstructor
    private static class JmServiceListener implements ServiceListener {

        private final RegistrationHandler registrationHandler;

        @Override
        public void serviceAdded(ServiceEvent event) {
            // Already done in serviceResolved()
        }

        @Override
        public void serviceRemoved(ServiceEvent event) {
            String key = ((JmDNS)event.getSource()).getHostName();
            registrationHandler.unregisterServer(key);
        }

        @Override
        public void serviceResolved(ServiceEvent event) {
            String key = ((JmDNS)event.getSource()).getHostName();
            String name = event.getName();
            String[] hostAddresses = event.getInfo().getHostAddresses();
            if (hostAddresses == null || hostAddresses.length == 0) {
                LOGGER.warning(String.format("No host address detected for registration: %s", event.getInfo()));
                return;
            }
            String hostAddress = hostAddresses[0];
            registrationHandler.registerServer(key, hostAddress, name);
        }
    }

    @Override
    public boolean initialize(RegistrationHandler registrationHandler, EventHandler eventHandler, String name, UUID id) {
        Preconditions.checkArgument(!initialized);

        this.registrationHandler = registrationHandler;
        this.eventHandler = eventHandler;
        this.name = name;
        this.id = id;

        try {
            jmdns = JmDNS.create();
            // Create HTTP server.
            this.inetAddress = jmdns.getInetAddress();
            this.httpServer = HttpServer.create(new InetSocketAddress( inetAddress, CLIENT_PORT), 0);
            httpServer.createContext("/event", this::handleEvent);
            httpServer.start();

            // Create HTTP client.
            httpClient = HttpClient.newHttpClient();
        } catch (IOException e) {
            LOGGER.warning(String.format("Oops, something went wrong: %s", e.getMessage()));
            return false;
        }

        if (!doInitialize()) {
            return false;
        }
        initialized = true;
        return true;
    }

    private boolean doInitialize() {
        this.serviceListener = new JmServiceListener(registrationHandler);
        jmdns.addServiceListener(SERVICE_TYPE, serviceListener);
        return true;
    }

    @Override
    public boolean stop() {
        Preconditions.checkArgument(initialized);
        Preconditions.checkArgument(serverAddress == null);
        httpServer.stop(0);
        jmdns.removeServiceListener(SERVICE_TYPE, serviceListener);
        initialized = false;
        return true;
    }

    @Override
    public boolean connect(InetSocketAddress serverAddress) {
        Preconditions.checkArgument(this.serverAddress == null);
        boolean connectionSucceeded = sendConnectionRequest(serverAddress, "register");
        if (connectionSucceeded) {
            this.serverAddress = serverAddress;
        }
        return connectionSucceeded;
    }

    @Override
    public boolean reconnect() {
        Preconditions.checkArgument(this.serverAddress != null);
        return sendConnectionRequest(serverAddress, "reconnect");
    }

    @Override
    public boolean sendEvents(InetSocketAddress serverAddress, List<Event> events) {
        Preconditions.checkArgument(initialized);
        HttpResponse<String> eventResponse = sendRequest(Address.of(serverAddress), "event",
                ((builder, body) -> builder.POST(HttpRequest.BodyPublishers.ofString(body))),
                eventRequest(events));
        return eventResponse.statusCode() == 200;
    }

    @Override
    public boolean disconnect() {
        Preconditions.checkArgument(initialized);
        Preconditions.checkArgument(serverAddress != null);
        // Ignoring the result.
        sendDisconnectionRequest(this.serverAddress);
        this.serverAddress = null;
        return true;
    }

    private boolean sendConnectionRequest(InetSocketAddress serverAddress, String endpoint) {
        Preconditions.checkArgument(initialized);
        HttpResponse<String> registrationResponse = sendRequest(Address.of(serverAddress), endpoint,
                ((builder, body) -> builder.POST(HttpRequest.BodyPublishers.ofString(body))),
                registrationRequest());
        return registrationResponse.statusCode() == 200;
    }

    private boolean sendDisconnectionRequest(InetSocketAddress serverAddress) {
        Preconditions.checkArgument(initialized);
        HttpResponse<String> unregistrationResponse = sendRequest(Address.of(serverAddress), "unregister",
                ((builder, body) -> builder.POST(HttpRequest.BodyPublishers.ofString(body))),
                unregistrationRequest());
        return unregistrationResponse.statusCode() == 200;
    }

    private HttpResponse<String> sendRequest(Address address, String endpoint, BiFunction<HttpRequest.Builder, String, HttpRequest.Builder> method, String body) {
        HttpRequest.Builder httpRequestBuilder = HttpRequest.newBuilder(
                URI.create(String.format(SERVER_ENDPOINT_TEMPLATE, address.getFormattedAddress(), SERVER_PORT, endpoint)))
                .timeout(Duration.ofMillis(serverRequestTimeoutMillis));
        HttpRequest httpRequest = method.apply(httpRequestBuilder, body).build();

        HttpResponse<String> httpResponse;
        try {
            httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            LOGGER.warning(String.format("Communication with server on address %s failed with error %s", address.getFormattedAddress(), e.getMessage()));
            return createResponse500(httpRequest, e.getMessage());
        }
        if (httpResponse.statusCode() != 200) {
            String message = String.format("Server responded with %d", httpResponse.statusCode());
            LOGGER.warning(message);
            return createResponse500(httpRequest, message);
        }
        return httpResponse;
    }

    HttpResponse<String> createResponse500(HttpRequest httpRequest, String message) {
        return new HttpResponse<>() {
            @Override
            public int statusCode() {
                return 500;
            }

            @Override
            public HttpRequest request() {
                return httpRequest;
            }

            @Override
            public Optional<HttpResponse<String>> previousResponse() {
                return Optional.empty();
            }

            @Override
            public HttpHeaders headers() {
                return null;
            }

            @Override
            public String body() {
                return message;
            }

            @Override
            public Optional<SSLSession> sslSession() {
                return Optional.empty();
            }

            @Override
            public URI uri() {
                return httpRequest.uri();
            }

            @Override
            public HttpClient.Version version() {
                return httpClient.version();
            }
        };
    }

    private String registrationRequest() {
        return Json.createObjectBuilder()
                .add("traceId", UUID.randomUUID().toString())
                .add("clientName",name)
                .add("clientId", id.toString())
                .build()
                .toString();
    }

    private String unregistrationRequest() {
        return Json.createObjectBuilder()
                .add("traceId", UUID.randomUUID().toString())
                .add("clientId", id.toString())
                .build()
                .toString();
    }

    private String eventRequest(List<Event> events) {
        JsonArray eventsBody = events.stream()
                .collect(Json::createArrayBuilder, (array, event) -> array.add(event.getBody()), JsonArrayBuilder::add)
                .build();
        return Json.createObjectBuilder()
                .add("traceId", UUID.randomUUID().toString())
                .add("clientId", id.toString())
                .add("eventsBody", eventsBody)
                .build()
                .toString();
    }

    private void handleEvent(HttpExchange httpExchange) throws IOException {
        Preconditions.checkArgument(initialized);
        if (serverAddress == null) {
            respond400(httpExchange, "No server not registered", null);
            return;
        }
        if (!"POST".equals(httpExchange.getRequestMethod())) {
            respond400(httpExchange, "Invalid request method " + httpExchange, null);
            return;
        }
        EventsPayload eventsPayload;
        List<Event> events;
        try {
            eventsPayload = parseJson(httpExchange.getRequestBody(), EventsPayload.class);
            events = new LinkedList<>();
            for (JsonValue json : eventsPayload.getEventsBody()) {
                events.add(new Event((javax.json.JsonObject) json));
            }
        } catch (JsonIOException | JsonSyntaxException | EventProcessingException | IllegalArgumentException exception) {
            respond400(httpExchange, "invalid data " + exception, null);
            return;
        } catch (Exception exception) {
            respond500(httpExchange, exception, null);
            return;
        }
        String traceId = eventsPayload.getTraceId();

        Address serverAddress = Address.of(httpExchange.getRemoteAddress());
        if (!serverAddress.equals(this.serverAddress)) {
            respond401(httpExchange, String.format("registered server %s does not match server %s", this.serverAddress.getAddress(), serverAddress.getAddress()), traceId);
            return;
        }

        switch (eventHandler.handle(events)) {
            case ACCEPTED:
                respond200(httpExchange, traceId);
                break;
            case REJECTED:
                respond403(httpExchange, serverAddress.getAddress(), traceId);
                break;
            case CONFLICT:
                respond409(httpExchange, traceId);
                break;
        }
    }

    private void respond200(HttpExchange httpExchange, String traceId) throws IOException {
        LOGGER.info(String.format("%s : %s", traceId, " : 200 OK: "));
        httpExchange.sendResponseHeaders(200, 0);
        httpExchange.close();
    }

    private void respond400(HttpExchange httpExchange, String message, String traceId) throws IOException {
        LOGGER.warning(String.format("%s : %s", traceId, message));
        httpExchange.sendResponseHeaders(400, 0);
        new OutputStreamWriter(httpExchange.getResponseBody()).append(message);
        httpExchange.close();
    }

    private void respond401(HttpExchange httpExchange, String message, String traceId) throws IOException {
        LOGGER.warning(String.format("%s : authentication failed : %s", traceId, message));
        httpExchange.sendResponseHeaders(401, 0);
        httpExchange.close();
    }

    private void respond403(HttpExchange httpExchange, String address, String traceId) throws IOException {
        LOGGER.warning(String.format("%s : client with address %s forbidden", traceId, address));
        httpExchange.sendResponseHeaders(403, 0);
        httpExchange.close();
    }

    private void respond409(HttpExchange httpExchange, String traceId) throws IOException {
        LOGGER.warning(String.format("%s : ignored", traceId));
        httpExchange.sendResponseHeaders(409, 0);
        httpExchange.close();
    }

    private void respond500(HttpExchange httpExchange, Throwable exception, String traceId) throws IOException {
        LOGGER.warning(String.format("%s : %s : %s", traceId, " : Oops, something went wrong: ", exception));
        httpExchange.sendResponseHeaders(500, 0);
        new OutputStreamWriter(httpExchange.getResponseBody()).append(exception.getLocalizedMessage());
        httpExchange.close();
    }

    private static <T extends Klonable<T>> T parseJson(InputStream inputStream, Class<T> classOfT) {
        try {
            JsonObject json = JsonParser.parseReader(new InputStreamReader(
                    inputStream, Charsets.UTF_8)).getAsJsonObject();
            return GSON.fromJson(json, classOfT).klone();
        } catch (NullPointerException exception) {
            throw new JsonSyntaxException(exception);
        }
    }

    @Value
    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    private static class Address {

        static Address of(InetSocketAddress inetSocketAddress) {
            return new Address(inetSocketAddress.getHostName(), inetSocketAddress.getAddress() instanceof Inet6Address);
        }

        String address;
        boolean isInet6Address;

        public String getFormattedAddress() {
            return isInet6Address ? String.format("[%s]", address) : address;
        }

    }

}
