package cz.wie.najtmar.gop.client.view;

import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.event.EventProcessingException;

import javax.annotation.Nonnull;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

/**
 * Factory class to create Events in the context of the client view of a Game.
 * @author najtmar
 */
public class EventFactory {

  /**
   * ObjectSerializer instance used to create Events.
   */
  private final ObjectSerializer objectSerializer;

  /**
   * The Game in which Events are processed.
   */
  private final GameView game;

  public EventFactory(@Nonnull ObjectSerializer objectSerializer) {
    this.objectSerializer = objectSerializer;
    this.game = objectSerializer.getGame();
  }

  /**
   * Creates Events of type SET_PRAWN_ITINERARY .
   * @param prawn the Prawn for which itinerary is set
   * @param itinerary the Itinerary
   * @return a new instance of a SET_PRAWN_ITINERARY Event
   * @throws EventProcessingException when Event creation fails
   */
  public Event createSetPrawnItineraryEvent(@Nonnull PrawnView prawn, @Nonnull ItineraryView itinerary)
      throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.SET_PRAWN_ITINERARY);
    final JsonArrayBuilder nodesBuilder = Json.createArrayBuilder();
    for (PositionView node : itinerary.getNodes()) {
      nodesBuilder.add(objectSerializer.serializePosition(node));
    }
    final JsonObject body = bodyBuilder
        .add("prawn", objectSerializer.serializePrawn(prawn))
        .add("nodes", nodesBuilder.build())
        .build();
    return new Event(body);
  }

  /**
   * Creates Events of type CLEAR_PRAWN_ITINERARY .
   * @param prawn the Prawn for which itinerary is cleared
   * @return a new instance of a CLEAR_PRAWN_ITINERARY Event
   * @throws EventProcessingException when Event creation fails
   */
  public Event createClearPrawnItineraryEvent(@Nonnull PrawnView prawn) throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.CLEAR_PRAWN_ITINERARY);
    final JsonObject body = bodyBuilder
        .add("prawn", objectSerializer.serializePrawn(prawn))
        .build();
    return new Event(body);
  }

  /**
   * Creates Events of type ABANDON_PLAYER .
   * @param player the Player to abandon the Game
   * @return a new instance of a ABANDON_PLAYER Event
   * @throws EventProcessingException when Event creation fails
   */
  public Event createAbandonPlayerEvent(@Nonnull PlayerView player)
      throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.ABANDON_PLAYER);
    bodyBuilder.add("player", objectSerializer.serializePlayer(player));
    return new Event(bodyBuilder.build());
  }

  /**
   * Creates Events of type SET_PRODUCE_WARRIOR_ACTION .
   * @param factory the Factory to be set to produce a Warrior
   * @param direction the Warrior direction of the Warrior produced
   * @return a new instance of a SET_PRODUCE_WARRIOR_ACTION Event
   * @throws EventProcessingException when Event creation fails
   */
  public Event createSetProduceWarriorActionEvent(@Nonnull FactoryView factory, double direction)
      throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.SET_PRODUCE_WARRIOR_ACTION);
    final JsonObject body = bodyBuilder
        .add("factory", objectSerializer.serializeFactory(factory))
        .add("direction", direction)
        .build();
    return new Event(body);
  }

  /**
   * Creates Events of type SET_PRODUCE_SETTLERS_UNIT_ACTION .
   * @param factory the Factory to be set to produce a Warrior
   * @return a new instance of a SET_PRODUCE_SETTLERS_UNIT_ACTION Event to produce a SettlersUnit
   * @throws EventProcessingException when Event creation fails
   */
  public Event createSetProduceSettlersUnitActionEvent(@Nonnull FactoryView factory) throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.SET_PRODUCE_SETTLERS_UNIT_ACTION);
    final JsonObject body = bodyBuilder
        .add("factory", objectSerializer.serializeFactory(factory))
        .build();
    return new Event(body);
  }

  /**
   * Creates Events of type SET_REPAIR_PRAWN_ACTION .
   * @param factory the Factory to be set to produce a Warrior
   * @param prawn the Prawn to repair
   * @return a new instance of a SET_REPAIR_PRAWN_ACTION Event to repair prawn
   * @throws EventProcessingException when Event creation fails
   */
  public Event createSetRepairPrawnActionEvent(@Nonnull FactoryView factory, @Nonnull PrawnView prawn)
      throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.SET_REPAIR_PRAWN_ACTION);
    final JsonObject body = bodyBuilder
        .add("factory", objectSerializer.serializeFactory(factory))
        .add("prawn", objectSerializer.serializePrawn(prawn))
        .build();
    return new Event(body);
  }

  /**
   * Creates Events of type SET_GROW_CITY_ACTION .
   * @param factory the Factory to be set to grow the City
   * @return a new instance of the SET_GROW_CITY_ACTION Event to grow city
   * @throws EventProcessingException when Event creation fails
   */
  public Event createSetGrowCityActionEvent(@Nonnull FactoryView factory) throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.SET_GROW_CITY_ACTION);
    final JsonObject body = bodyBuilder
        .add("factory", objectSerializer.serializeFactory(factory))
        .build();
    return new Event(body);
  }

  /**
   * Creates Events of type ORDER_CITY_CREATION .
   * @param settlersUnit the SettlersUnit that creates the City
   * @param name the name of the City
   * @return a new instance of the ORDER_CITY_CREATION Event
   * @throws EventProcessingException when Event creation fails
   */
  public Event createOrderCityCreationEvent(@Nonnull PrawnView settlersUnit, @Nonnull String name)
      throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.ORDER_CITY_CREATION);
    final JsonObject body = bodyBuilder
        .add("settlersUnit", objectSerializer.serializePrawn(settlersUnit))
        .add("name", name)
        .build();
    return new Event(body);
  }

  /**
   * Creates Events of type CLIENT_FAILURE .
   * @param player the Player whose client has failed
   * @param message the message added to the Event
   * @return a new instance of the CLIENT_FAILURE Event
   * @throws EventProcessingException when Event creation fails
   */
  public Event createClientFailureEvent(@Nonnull PlayerView player, @Nonnull String message)
      throws EventProcessingException {
    final JsonObjectBuilder bodyBuilder = Json.createObjectBuilder();
    setCommonEventFields(bodyBuilder, Event.Type.CLIENT_FAILURE);
    final JsonObject body = bodyBuilder
        .add("player", objectSerializer.serializePlayer(player))
        .add("message", message)
        .build();
    return new Event(body);
  }

  /**
   * Sets fields common for all Events in builder used to create an Event object.
   * @param builder JsonObjectBuilder used to create an Event object
   * @param type the Type of the Event
   * @throws EventProcessingException when setting common fields fails
   */
  private void setCommonEventFields(@Nonnull JsonObjectBuilder builder, @Nonnull Event.Type type)
      throws EventProcessingException {
    if (game.getTime() == -1) {
      throw new EventProcessingException("time not initialized.");
    }
    builder.add("type", type.name()).add("time", game.getTime());
  }

}
