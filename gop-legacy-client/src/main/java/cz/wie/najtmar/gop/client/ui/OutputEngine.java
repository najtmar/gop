package cz.wie.najtmar.gop.client.ui;

import cz.wie.najtmar.gop.client.view.BattleView;
import cz.wie.najtmar.gop.client.view.CityView;
import cz.wie.najtmar.gop.client.view.ClientViewException;
import cz.wie.najtmar.gop.client.view.FactoryView;
import cz.wie.najtmar.gop.client.view.GameView;
import cz.wie.najtmar.gop.client.view.PositionView;
import cz.wie.najtmar.gop.client.view.PrawnView;
import cz.wie.najtmar.gop.client.view.RoadSectionView;
import cz.wie.najtmar.gop.common.Point2D;
import cz.wie.najtmar.gop.event.Event;

import java.util.NavigableMap;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;

/**
 * Any type of graphical engine used to visualize Client UI .
 * @author najtmar
 */
public interface OutputEngine {

  /**
   * Initializes the OutputEngine .
   * @param game the Game based on which the OutputEngine is initialized
   * @throws ClientViewException when OutputEngine initialization fails
   */
  public void initializeOutput(@Nonnull GameView game) throws ClientViewException;

  /**
   * Visualize the current Game time.
   * @param time the current Game time
   */
  public void onTimeUpdated(@Nonnegative long time);

  /**
   * Updates periodic Game info.
   * @param relevantEvents relevant Events sorted by relevance with their counts
   * @throws ClientViewException when updating the periodic info fails
   */
  public void onPeriodicInfoUpdated(@Nonnull NavigableMap<String, Integer> relevantEvents) throws ClientViewException;

  /**
   * Visualizes a RoadSection.Half with visibility VISIBLE .
   * @param roadSection the RoadSection
   * @param end determines the Half of the RoadSection
   */
  public void onRoadSectionHalfVisible(
      @Nonnull RoadSectionView roadSection, @Nonnull RoadSectionView.Direction end);

  /**
   * Visualizes a RoadSection.Half with visibility DISCOVERED .
   * @param roadSection the RoadSection
   * @param end determines the Half of the RoadSection
   */
  public void onRoadSectionHalfDiscovered(
      @Nonnull RoadSectionView roadSection, @Nonnull RoadSectionView.Direction end);

  /**
   * Visualizes a RoadSection.Half with visibility INVISIBLE .
   * @param roadSection the RoadSection
   * @param end determines the Half of the RoadSection
   */
  public void onRoadSectionHalfInvisible(
      @Nonnull RoadSectionView roadSection, @Nonnull RoadSectionView.Direction end);

  /**
   * Visualizes a Prawn with visibility VISIBLE .
   * @param prawn the Prawn
   */
  public void onPrawnVisible(@Nonnull PrawnView prawn);

  /**
   * Visualizes a Prawn with visibility INVISIBLE .
   * @param prawn the Prawn
   */
  public void onPrawnInvisible(@Nonnull PrawnView prawn);

  /**
   * Visualizes a Prawn that has just been removed.
   * @param prawn the Prawn
   */
  public void onPrawnJustRemoved(@Nonnull PrawnView prawn);

  /**
   * Visualizes a City with visibility DISCOVERED .
   * @param city the City
   */
  public void onCityDiscovered(@Nonnull CityView city);

  /**
   * Visualizes a City with visibility INVISIBLE .
   * @param city the City
   */
  public void onCityInvisible(@Nonnull CityView city);

  /**
   * Visualizes a City with visibility DESTROYED .
   * @param city the City
   */
  public void onCityDestroyed(@Nonnull CityView city);

  /**
   * Visualizes a visible Battle .
   * @param battle the Battle
   */
  public void onBattleVisible(@Nonnull BattleView battle);

  /**
   * Visualizes an invisible Battle .
   * @param battle the Battle
   */
  public void onBattleInvisible(@Nonnull BattleView battle);

  /**
   * Visualizes a City in the magnified view.
   * @param city the City to be visualized
   * @param point the Point where the City should be drawn
   */
  public void onMagnifiedCityDrawn(@Nonnull CityView city, @Nonnull Point2D.Double point);

  /**
   * Visualizes a Prawn in the magnified view.
   * @param prawn the Prawn to be visualized
   * @param point the Point where the Prawn should be drawn
   * @param index the index of the Prawn on the current Position; index 0 means the top Prawn
   */
  public void onMagnifiedPrawnDrawn(@Nonnull PrawnView prawn, @Nonnull Point2D.Double point, @Nonnegative int index);

  /**
   * Visualizes a distinguished Position on the Board .
   * @param position the Position to be drawn
   * @param point the Point where the Position should be drawn
   */
  public void onMagnifiedPositionDrawn(@Nonnull PositionView position, @Nonnull Point2D.Double point);

  /**
   * Highlights a SettlersUnit that is ordered to create a City .
   * @param position the Position where the City will be created
   * @param cityName the name of the City to be created
   */
  public void onCityCreationHighlighted(@Nonnull PositionView position, @Nonnull String cityName);

  /**
   * Highlights a line section that can be selected in the next click.
   * @param beginning the beginning of the line section to highlight
   * @param end the end of the line section to highlight
   */
  public void onSelectableLineSectionHighlighted(@Nonnull PositionView beginning, @Nonnull PositionView end);

  /**
   * Highlights a line section.
   * @param beginning the beginning of the line section to highlight
   * @param end the end of the line section to highlight
   */
  public void onLineSectionHighlighted(@Nonnull PositionView beginning, @Nonnull PositionView end);

  /**
   * Removes all the highlighting shown.
   */
  public void onHighlightingRemoved();

  /**
   * Shows the magnified view.
   * @param point the center of the magnification
   */
  public void onMagnificationShown(@Nonnull Point2D.Double point);

  /**
   * Hides the magnified view.
   */
  public void onMagnificationHidden();

  /**
   * Shows the PrawnSelection view.
   */
  public void onPrawnSelectionShown();

  /**
   * Hides the PrawnSelection view.
   */
  public void onPrawnSelectionHidden();

  /**
   * Visualizes a Prawn in a PrawnSelection .
   * @param prawn the Prawn to be drawn
   * @param point the Point where the Prawn should be drawn
   */
  public void onPrawnSelectionPrawnDrawn(@Nonnull PrawnView prawn, @Nonnull Point2D.Double point);

  /**
   * Visualizes a left slider in a PrawnSelection .
   * @param point the Point where the left slider should be drawn
   * @param previousPrawnsCount the number of Prawn before the first one shown
   */
  public void onPrawnSelectionLeftSlider(@Nonnull Point2D.Double point, @Nonnegative int previousPrawnsCount);

  /**
   * Visualizes a right slider in a PrawnSelection .
   * @param point the Point where the right slider should be drawn
   * @param nextPrawnsCount the number of Prawn after the last one shown
   */
  public void onPrawnSelectionRightSlider(@Nonnull Point2D.Double point, @Nonnegative int nextPrawnsCount);

  /**
   * Visualizes the City view.
   */
  public void onCityViewShown();

  /**
   * Hides the City view.
   */
  public void onCityViewHidden();

  /**
   * Visualizes a CityFactory in the City view.
   * @param factory the CityFactory to visualize
   * @param positionIndex the index at which the CityFactory is visualized
   */
  public void onCityFactoryShown(@Nonnull FactoryView factory, int positionIndex);

  /**
   * Visualizes an upper slider for CityFactories in the City view.
   * @param positionIndex the index at which the CityFactory upper slider is visualized
   */
  public void onCityFactoryUpperSliderShown(@Nonnegative int positionIndex);

  /**
   * Visualizes an lower slider for CityFactories in the City view.
   * @param positionIndex the index at which the CityFactory lower slider is visualized
   */
  public void onCityFactoryLowerSliderShown(@Nonnegative int positionIndex);

  /**
   * Highlights CityFactory box with a given index.
   * @param positionIndex the index at which the CityFactory is located.
   */
  public void onCityFactoryHighlighted(@Nonnegative int positionIndex);

  /**
   * Shows the PrawnSelection in the City view.
   */
  public void onCityPrawnSelectionShown();

  /**
   * Visualizes a Prawn in a PrawnSelection in the City view.
   * @param prawn the Prawn to be drawn
   * @param point the Point where the Prawn should be drawn
   */
  public void onCityPrawnSelectionPrawnDrawn(@Nonnull PrawnView prawn, @Nonnull Point2D.Double point);

  /**
   * Visualizes a left slider in a PrawnSelection in the City view.
   * @param point the Point where the left slider should be drawn
   * @param previousPrawnsCount the number of Prawn before the first one shown
   */
  public void onCityPrawnSelectionLeftSlider(@Nonnull Point2D.Double point, @Nonnegative int previousPrawnsCount);

  /**
   * Visualizes a right slider in a PrawnSelection in the City view.
   * @param point the Point where the right slider should be drawn
   * @param nextPrawnsCount the number of Prawn after the last one shown
   */
  public void onCityPrawnSelectionRightSlider(@Nonnull Point2D.Double point, @Nonnegative int nextPrawnsCount);

  /**
   * Visualizes a City box in the City view.
   * @param city the City to be visualized
   * @param capturedHalves for each RoadSection adjacent to the City, the number of RoadSection.Halves that are captured
   *     on that RoadSection
   * @param productivity the number from interval [0.0, 1.0] denoting the productivity of the City
   */
  public void onCityBoxDrawn(@Nonnull CityView city, @Nonnull int[] capturedHalves, @Nonnegative double productivity);

  /**
   * Visualizes a box to select Warrior direction.
   */
  public void onWarriorDirectionSelectionShown();

  /**
   * Hides the box to select Warrior direction.
   */
  public void onWarriorDirectionSelectionHidden();

  /**
   * Visualizes a prompt to abandon the Game.
   */
  public void onGameAbandoningPromptShown();

  /**
   * Hides the prompt to abandon the Game.
   */
  public void onGameAbandoningPromptHidden();

  /**
   * Visualized Game abandonment.
   */
  public void onGameAbandoned();

  /**
   * Visualized Game force quitting.
   */
  public void onGameForceQuit();

  /**
   * Visualizes a prompt to select a Prawn to repair.
   */
  public void onPrawnRepairSelectionShown();

  /**
   * Highlights a Prawn with given coordinates selected for repair.
   * @param indexX the x-coordinate of the Prawn to repair
   * @param indexY the y-coordinate of the Prawn to repair
   */
  public void onPrawnRepairSelectionHighlighted(@Nonnegative int indexX, @Nonnegative int indexY);

  /**
   * Hides the box to select a Prawn to repair.
   */
  public void onPrawnRepairSelectionHidden();

  /**
   * Notifies the User that own connection to the Game server has changed.
   * @param connected whether the Connection is established and working
   * @throws ClientViewException if the notification process fails
   */
  public void notifyOwnGameServerConnection(boolean connected) throws ClientViewException;

  /**
   * Notifies the User that other Game clients' connections to the Game server have changed.
   * @param connected whether the Connection of all other Game clients is established and working
   * @throws ClientViewException if the notification process fails
   */
  public void notifyOthersGameServerConnection(boolean connected) throws ClientViewException;

  /**
   * Notifies the Client that the Game has been finished with State UNDECIDED .
   * @throws ClientViewException when the notification process fails
   */
  public void notifyGameUndecided() throws ClientViewException;

  /**
   * User interaction which results with providing a list of Events to be executed.
   * @param inputEvents Game Events from the current round
   * @return Events provided by the user
   * @throws ClientViewException when user interaction fails
   * @deprecated replaces by the interact() method in class InputManager
   */
  @Deprecated
  public Event[] interact(@Nonnull Event[] inputEvents) throws ClientViewException;

}
