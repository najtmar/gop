package cz.wie.najtmar.gop.client.view;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Client-side representation of class Factory .
 * @author najtmar
 */
public class FactoryView {

  /**
   * State of the Factory.
   * @author najtmar
   */
  public enum State {
    /**
     * The Factory has no Action scheduled.
     */
    IDLE,

    /**
     * Factory is doing an Action.
     */
    IN_ACTION,
  }

  /**
   * State of the Factory.
   */
  @Nonnull private State state;

  /**
   * The City to which the Factory belongs.
   */
  private final CityView city;

  /**
   * Index of the Factory in the City.
   */
  private int index;

  /**
   * Type of the Action performed by the Factory.
   * @author najtmar
   */
  public enum ActionType {
    /**
     * Action to produce a Warrior .
     */
    PRODUCE_WARRIOR,

    /**
     * Action to produce a SettlersUnit .
     */
    PRODUCE_SETTLERS_UNIT,

    /**
     * Action to repair a Prawn .
     */
    REPAIR_PRAWN,

    /**
     * Action to grow the City .
     */
    GROW,
  }

  /**
   * Type of the Action currently performed by the City. null if the Factory is in the IDLE State.
   */
  @Nullable private ActionType actionType;

  /**
   * If ActionType is REPAIR_PRAWN, then it contains the Prawn being repaired.
   */
  @Nullable private PrawnView prawnRepaired;

  /**
   * If ActionType is PRODUCE_WARRIOR, then it contains the Warrior direction.
   */
  @Nonnegative private double warriorDirection;

  /**
   * Number from [0.0, 1.0] denoting the progress of the Action. progress 1.0 means that the Action has been finished.
   */
  @Nonnegative private double progress;

  /**
   * Constructor.
   * @param city the City to which the Factory belongs
   * @param index the index of the Factory in city
   */
  public FactoryView(@Nonnull CityView city, @Nonnegative int index) {
    this.city = city;
    this.index = index;
    this.state = State.IDLE;
  }

  public State getState() {
    return state;
  }

  public CityView getCity() {
    return city;
  }

  public int getIndex() {
    return index;
  }

  public void setIndex(int index) {
    this.index = index;
  }

  public ActionType getActionType() {
    return actionType;
  }

  /**
   * Returns the value of field warriorDirection .
   * @return the value of field warriorDirection
   * @throws ClientViewException if actionType is not PRODUCE_WARRIOR
   */
  public double getWarriorDirection() throws ClientViewException {
    if (actionType != ActionType.PRODUCE_WARRIOR) {
      throw new ClientViewException("warriorDirection only makes sense for ActionType PRODUCE_WARRIOR (found "
          + actionType + ").");
    }
    return warriorDirection;
  }

  /**
   * Sets an Action for the Factory .
   * @param actionType the Type of the Action
   * @param prawnRepaired the Prawn being repaired; only set for ActionType REPAIR_PRAWN
   * @param warriorDirection the Warrior direction; only set for ActionType PRODUCE_WARRIOR, otherwise should be -1.0
   * @throws ClientViewException if the arguments are invalid
   */
  public void setAction(@Nonnull ActionType actionType, @Nullable PrawnView prawnRepaired,
      double warriorDirection) throws ClientViewException {
    this.state = State.IN_ACTION;
    this.actionType = actionType;
    if (actionType != ActionType.REPAIR_PRAWN) {
      if (prawnRepaired != null) {
        throw new ClientViewException("prawnRepaired can only be set for ActionType REPAIR_PRAWN (actionType: "
            + actionType + ", prawnRepaired: " + prawnRepaired + ").");
      }
      this.progress = 0.0;
    } else {
      if (prawnRepaired == null) {
        throw new ClientViewException("prawnRepaired not set.");
      }
      this.prawnRepaired = prawnRepaired;
      this.progress = prawnRepaired.getEnergy() / PrawnView.MAX_ENERGY;
    }
    if (actionType != ActionType.PRODUCE_WARRIOR) {
      if (warriorDirection != -1.0) {
        throw new ClientViewException("warriorDirection can only be set for ActionType PRODUCE_WARRIOR (actionType: "
            + actionType + ", warriorDirection: " + warriorDirection + ").");
      }
    } else {
      if (warriorDirection < 0.0 || warriorDirection >= Math.PI * 2) {
        throw new ClientViewException("warriorDirection out of the bound: " + warriorDirection);
      }
      this.warriorDirection = warriorDirection;
    }
  }

  /**
   * Unsets the Action and sets the Factory in state IDLE .
   */
  public void finishAction() {
    this.state = State.IDLE;
    this.actionType = null;
    this.progress = 0.0;
    this.prawnRepaired = null;
  }

  /**
   * Returns the Prawn being repaired.
   * @return the Prawn being repaired
   * @throws ClientViewException when ActionType is not REPAIR_PRAWN
   */
  public PrawnView getPrawnRepaired() throws ClientViewException {
    if (state == State.IDLE || actionType != ActionType.REPAIR_PRAWN) {
      throw new ClientViewException("Prawn repaired is only set for ActionType REPAIR_PRAWN (state: " + state
          + ", actionType: " + actionType + ").");
    }
    return prawnRepaired;
  }

  /**
   * Returns the progress of the current Action .
   * @return the progress of the current Action
   * @throws ClientViewException when the State is not IN_ACTION
   */
  public double getProgress() throws ClientViewException {
    if (state != State.IN_ACTION) {
      throw new ClientViewException("Progress is only meaningful for State IN_ACTION (state: " + state + ").");
    }
    return progress;
  }

  /**
   * Sets the progress for the current Action .
   * @param progress the progress to be set
   * @throws ClientViewException when the State is not IN_ACTION
   */
  public void setProgress(double progress) throws ClientViewException {
    if (state != State.IN_ACTION) {
      throw new ClientViewException("Progress is only meaningful for State IN_ACTION (state: " + state + ").");
    }
    this.progress = progress;
  }

}
