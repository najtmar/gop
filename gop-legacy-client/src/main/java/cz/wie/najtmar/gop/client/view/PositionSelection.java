package cz.wie.najtmar.gop.client.view;

import cz.wie.najtmar.gop.common.Point2D;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;

/**
 * Represents a collection of Positions, both regular and distinctive, created by a ItemSelector and ordered properly.
 * @author najtmar
 */
public class PositionSelection {

  /**
   * State of a PositionSelection .
   */
  public enum State {
    /**
     * PositionSelection already created by distinctive Positions not yet adjusted.
     */
    CREATED,
    /**
     * PositionSelection created and distinctive Positions adjusted.
     */
    ADJUSTED,
  }

  /**
   * State of the PositionSelection .
   */
  private State state;

  /**
   * The Player for which the PositionSelection is made.
   */
  private final PlayerView player;

  /**
   * The Point based on which Selection is made.
   */
  private final Point2D.Double selectionPoint;

  /**
   * Set of all Positions, both non-distinctive and distinctive, belonging to the PositionSelection .
   */
  private final Set<PositionView> allPositions = new HashSet<>();

  /**
   * List of DistinctivePositions, ordered by their decreasing priorities, belonging to the PositionSelection .
   */
  private final DistinctivePositionView[] distinctivePositions;

  /**
   * X-Position of the upper-left corner of the ADJUSTED PositionSelection .
   */
  private double xpos;

  /**
   * Y-Position of the upper-left corner of the ADJUSTED PositionSelection .
   */
  private double ypos;

  /**
   * Width of the ADJUSTED PositionSection .
   */
  private double width;

  /**
   * Height of the ADJUSTED PositionSelection .
   */
  private double height;

  /**
   * Constructor.
   * @param player the Player for which the PositionSelection is made
   * @param selectionPoint the Point based on which the selection was made
   * @param regularPositions the set of regular Positions that belong to the PositionSelection; it's allowed for Set
   *     regularPositions to overlap with Set distinctivePositions
   * @param distinctivePositions the set of DistinctivePositions that belong to the PositionSelection
   */
  public PositionSelection(@Nonnull PlayerView player, @Nonnull Point2D.Double selectionPoint,
      @Nonnull Set<PositionView> regularPositions, @Nonnull Set<DistinctivePositionView> distinctivePositions) {
    this.player = player;
    this.selectionPoint = selectionPoint;
    this.allPositions.addAll(regularPositions);
    this.allPositions.addAll(StreamSupport.stream(
        distinctivePositions).map((el) -> el.getPosition()).collect(Collectors.toList()));
    this.distinctivePositions = StreamSupport.stream(distinctivePositions).sorted()
        .toArray(size -> new DistinctivePositionView[size]);
    this.state = State.CREATED;
  }

  public Point2D.Double getSelectionPoint() {
    return selectionPoint;
  }

  public Set<PositionView> getAllPositions() {
    return allPositions;
  }

  public DistinctivePositionView[] getDistinctivePositions() {
    return distinctivePositions;
  }

  /**
   * Sets the magnification frame for a PositionSelection to be adjusted.
   * @param xpos the xpos parameter
   * @param ypos the ypos parameter
   * @param width the width parameter
   * @param height the height parameter
   * @throws ClientViewException when the magnification frame cannot be set
   */
  public void setMagnificationFrame(double xpos, double ypos, @Nonnegative double width, @Nonnegative double height)
      throws ClientViewException {
    if (state != State.CREATED) {
      throw new ClientViewException("Magnification frame can only be set in state CREATED .");
    }
    this.xpos = xpos;
    this.ypos = ypos;
    this.width = width;
    this.height = height;
  }

  public State getState() {
    return state;
  }

  public PlayerView getPlayer() {
    return player;
  }

  /**
   * Marks the PositionSelection as ADJUSTED .
   * @throws ClientViewException if the State is not CREATED
   */
  public void markAdjusted() throws ClientViewException {
    if (state != State.CREATED) {
      throw new ClientViewException("Only PositionSelections in state CREATED can be marked ADJUSTED .");
    }
    state = State.ADJUSTED;
  }

  /**
   * Getter for field xpos .
   * @return xpos
   * @throws ClientViewException when xpos is not defined
   */
  public double getXpos() throws ClientViewException {
    if (state != State.ADJUSTED) {
      throw new ClientViewException("xpos is only defined for ADJUSTED PositionSelections .");
    }
    return xpos;
  }

  /**
   * Getter for field ypos .
   * @return ypos
   * @throws ClientViewException when ypos is not defined
   */
  public double getYpos() throws ClientViewException {
    if (state != State.ADJUSTED) {
      throw new ClientViewException("ypos is only defined for ADJUSTED PositionSelections .");
    }
    return ypos;
  }

  /**
   * Getter for field width .
   * @return width
   * @throws ClientViewException when width is not defined
   */
  public double getWidth() throws ClientViewException {
    if (state != State.ADJUSTED) {
      throw new ClientViewException("width is only defined for ADJUSTED PositionSelections .");
    }
    return width;
  }

  /**
   * Getter for field height .
   * @return height
   * @throws ClientViewException when height is not defined
   */
  public double getHeight() throws ClientViewException {
    if (state != State.ADJUSTED) {
      throw new ClientViewException("height is only defined for ADJUSTED PositionSelections .");
    }
    return height;
  }

  @Override
  public String toString() {
    return "PositionSelection [allPositions=" + allPositions + ", distinctivePositions="
        + Arrays.toString(distinctivePositions) + "]";
  }

}
