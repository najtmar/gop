package cz.wie.najtmar.gop.client.ui;

import cz.wie.najtmar.gop.client.view.ClientViewException;
import cz.wie.najtmar.gop.client.view.GameView;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Any type of engine used to get Client actions.
 * @author najtmar
 */
public interface InputEngine {

  /**
   * Initializes the InputEngine .
   * @param game the Game based on which the OutputEngine is initialized
   * @param inputManager the InputManager invoked on relevant actions
   * @throws ClientViewException when initialization of the InputEngine fails
   */
  public void initializeInput(@Nonnull GameView game, @Nonnull InputManager inputManager)
      throws ClientViewException;

  /**
   * Invoked when a change from oldState to a new State . The new State is available in the InputManager . The UI and
   * the sensitivity to actions is adjusted accordingly.
   * Standard client interaction is triggered by changing from State.INITIALIZED to State BOARD .
   * @param oldState the State to change from
   * @throws ClientViewException when the operation fails
   */
  public void onStateChange(@Nullable InputManager.State oldState) throws ClientViewException;

}
