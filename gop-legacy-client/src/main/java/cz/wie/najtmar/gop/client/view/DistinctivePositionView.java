package cz.wie.najtmar.gop.client.view;

import cz.wie.najtmar.gop.common.Point2D;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Represents a Position that is some way is distinctive among other Positions (e.g., is a Joint or a Prawn is located
 * at it). The order of the DistinctivePositions is by decreasing priorities of their DistinctionPositions (high
 * priority Positions first).
 * @author najtmar
 *
 */
public class DistinctivePositionView implements Comparable<DistinctivePositionView> {

  /**
   * Defines relevant ways in which a Position may be distinctive among different Positions . Ordered by decreasing
   * priority.
   * @author najtmar
   */
  public enum DistinctionProperty {
    /**
     * There is own City located at the Position.
     */
    OWN_CITY(0) {
      @Override
      boolean appliesToAndVisible(PositionView position, @Nonnull PlayerView player) {
        final JointView joint = position.getJoint();
        if (joint == null) {
          return false;
        }
        final CityView city = joint.getCity();
        if (city == null) {
          return false;
        }
        return city.getPlayer().equals(player) && city.getVisibility() == CityView.Visibility.DISCOVERED;
      }
    },

    /**
     * There is an opponent's City located at the Position.
     */
    OPPONENTS_CITY(0) {
      @Override
      boolean appliesToAndVisible(PositionView position, @Nonnull PlayerView player) {
        final JointView joint = position.getJoint();
        if (joint == null) {
          return false;
        }
        final CityView city = joint.getCity();
        if (city == null) {
          return false;
        }
        return !city.getPlayer().equals(player) && city.getVisibility() == CityView.Visibility.DISCOVERED;
      }
    },

    /**
     * The Position is a Joint.
     */
    JOINT(0) {
      @Override
      boolean appliesToAndVisible(PositionView position, @Nullable PlayerView unusedPlayer) {
        return position.getJoint() != null && position.getVisibility() != RoadSectionView.Visibility.INVISIBLE;
      }
    },

    /**
     * The Position is a RoadSection midpoint.
     */
    MIDPOINT(0) {
      @Override
      boolean appliesToAndVisible(PositionView position, @Nullable PlayerView unusedPlayer) {
        return position.getIndex() == position.getRoadSection().getMidpointIndex()
            && position.getVisibility() != RoadSectionView.Visibility.INVISIBLE;
      }
    },

    /**
     * The Position is adjacent to a Joint.
     */
    JOINT_ADJACENT(1) {
      @Override
      boolean appliesToAndVisible(PositionView position, @Nullable PlayerView unusedPlayer) {
        final int[] jointAdjacentIndexes = position.getRoadSection().getJointAdjacentPointIndexes();
        return (position.getIndex() == jointAdjacentIndexes[0] || position.getIndex() == jointAdjacentIndexes[1])
            && position.getVisibility() != RoadSectionView.Visibility.INVISIBLE;
      }
    },

    /**
     * The Position is adjacent to a RoadSection midpoint.
     */
    MIDPOINT_ADJACENT(1) {
      @Override
      boolean appliesToAndVisible(PositionView position, @Nullable PlayerView unusedPlayer) {
        final int midpointIndex = position.getRoadSection().getMidpointIndex();
        return (position.getIndex() == midpointIndex - 1 || position.getIndex() == midpointIndex + 1)
            && position.getVisibility() != RoadSectionView.Visibility.INVISIBLE;
      }
    },

    /**
     * The Position is adjacent to a Position at which a City belonging to own Player is located.
     */
    OWN_CITY_ADJACENT(1) {
      @Override
      boolean appliesToAndVisible(PositionView position, @Nonnull PlayerView player) {
        if (position.getJoint() != null) {
          return false;
        }
        for (PositionView adjacentPosition : position.getAdjacentPositions()) {
          final JointView joint = adjacentPosition.getJoint();
          if (joint == null) {
            continue;
          }
          final CityView city = joint.getCity();
          if (city == null) {
            continue;
          }
          if (city.getPlayer().equals(player) && position.getVisibility() != RoadSectionView.Visibility.INVISIBLE) {
            return true;
          }
        }
        return false;
      }
    },

    /**
     * The Position is adjacent to a Position at which a City belonging to an opponent Player is located.
     */
    OPPONENTS_CITY_ADJACENT(1) {
      @Override
      boolean appliesToAndVisible(PositionView position, @Nonnull PlayerView player) {
        if (position.getJoint() != null) {
          return false;
        }
        for (PositionView adjacentPosition : position.getAdjacentPositions()) {
          final JointView joint = adjacentPosition.getJoint();
          if (joint == null) {
            continue;
          }
          final CityView city = joint.getCity();
          if (city == null) {
            continue;
          }
          if (!city.getPlayer().equals(player) && position.getVisibility() != RoadSectionView.Visibility.INVISIBLE) {
            return true;
          }
        }
        return false;
      }
    },

    /**
     * There are Prawns belonging to own Player location at the Position.
     */
    OWN_PRAWN(2) {
      @Override
      boolean appliesToAndVisible(PositionView position, @Nonnull PlayerView player) {
        final Set<PrawnView> prawns = position.getPrawns();
        if (prawns.isEmpty()) {
          return false;
        }
        for (PrawnView prawn : prawns) {
          if (prawn.getPlayer().equals(player) && prawn.getVisibility() == PrawnView.Visibility.VISIBLE) {
            return true;
          }
        }
        return false;
      }
    },

    /**
     * There are Prawns belonging to an opponent Player location at the Position.
     */
    OPPONENTS_PRAWN(2) {
      @Override
      boolean appliesToAndVisible(PositionView position, @Nonnull PlayerView player) {
        final Set<PrawnView> prawns = position.getPrawns();
        if (prawns.isEmpty()) {
          return false;
        }
        for (PrawnView prawn : prawns) {
          if (!prawn.getPlayer().equals(player) && prawn.getVisibility() == PrawnView.Visibility.VISIBLE) {
            return true;
          }
        }
        return false;
      }
    },

    /**
     * The Position is adjacent to a Position at which Prawns belonging to own Player are located.
     */
    OWN_PRAWN_ADJACENT(3) {
      @Override
      boolean appliesToAndVisible(PositionView position, @Nonnull PlayerView player) {
        for (PositionView adjacentPosition : position.getAdjacentPositions()) {
          for (PrawnView prawn : adjacentPosition.getPrawns()) {
            if (prawn.getPlayer().equals(player) && position.getVisibility() != RoadSectionView.Visibility.INVISIBLE) {
              return true;
            }
          }
        }
        return false;
      }
    },

    /**
     * The Position is adjacent to a Position at which Prawns belonging to an opponent's Player are located.
     */
    OPPONENTS_PRAWN_ADJACENT(3) {
      @Override
      boolean appliesToAndVisible(PositionView position, @Nonnull PlayerView player) {
        for (PositionView adjacentPosition : position.getAdjacentPositions()) {
          for (PrawnView prawn : adjacentPosition.getPrawns()) {
            if (!prawn.getPlayer().equals(player) && position.getVisibility() != RoadSectionView.Visibility.INVISIBLE) {
              return true;
            }
          }
        }
        return false;
      }
    };

    /**
     * Priority by which DistinctivePositions can be ordered. Lower numbers mean higher priorities.
     */
    private final int priority;

    private DistinctionProperty(int priority) {
      this.priority = priority;
    }

    /**
     * Checks whether a given DistinctionProperty applies to position in the context of player, and it is visible.
     * @param position the Position whose distinctiveness is checked
     * @param player the Player in whose context position distinctiveness is checked
     * @return true iff a given DistinctionProperty applies to position and is visible
     */
    abstract boolean appliesToAndVisible(@Nonnull PositionView position, PlayerView player);

    public int getPriority() {
      return priority;
    }

  }

  /**
   * The Position that is distinctive.
   */
  private final PositionView position;

  /**
   * Coordinates pair of the DistinctivePosition . Only determined in the ADJUSTED State .
   */
  private Point2D.Double coordinatesPair;

  /**
   * Contains the DistinctionProperties in which a given Position is distinctive in the current context.
   * The lower the index in the array, the higher the priority of the DistinctionProperty.
   */
  private final DistinctionProperty[] distinctionProperties;

  public PositionView getPosition() {
    return position;
  }

  public DistinctionProperty[] getDistinctionProperties() {
    return distinctionProperties;
  }

  /**
   * State of a DistinctivePosition .
   * @author najtmar
   */
  public enum State {
    /**
     * DistinctivePosition already created but not yet adjusted.
     */
    CREATED,
    /**
     * DistinctivePosition created and adjusted.
     */
    ADJUSTED,
  }

  /**
   * State of the DistinctivePosition .
   */
  private State state;

  @Override
  public int compareTo(DistinctivePositionView otherPosition) {
    final int propertyComparison =
        Integer.compare(distinctionProperties[0].ordinal(), otherPosition.distinctionProperties[0].ordinal());
    if (propertyComparison != 0) {
      return propertyComparison;
    }
    final int roadSectionComparison =
        Integer.compare(position.getRoadSection().getIndex(), otherPosition.getPosition().getRoadSection().getIndex());
    if (roadSectionComparison != 0) {
      return roadSectionComparison;
    }
    return Integer.compare(position.getIndex(), otherPosition.getPosition().getIndex());
  }

  public State getState() {
    return state;
  }

  /**
   * Getter for field coordinatesPair .
   * @return coordinatesPair
   * @throws ClientViewException when coordinatesPair is not defined
   */
  public Point2D.Double getCoordinatesPair() throws ClientViewException {
    if (state != State.ADJUSTED) {
      throw new ClientViewException("coordinatesPair is only defined for ADJUSTED DistinctivePositions .");
    }
    return coordinatesPair;
  }

  /**
   * Setter for field coordinatesPair . Changes object State to ADJUSTED .
   * @param coordinatesPair the coordinates pair to set
   * @throws ClientViewException when coordinatesPair cannot be set
   */
  public void setCoordinatesPair(Point2D.Double coordinatesPair) throws ClientViewException {
    if (state != State.CREATED) {
      throw new ClientViewException("coordinatesPair can only be set for ADJUSTED DistinctivePositions .");
    }
    this.coordinatesPair = coordinatesPair;
    this.state = State.ADJUSTED;
  }

  @Override
  public String toString() {
    return "DistinctivePositionView [position=" + position + ", distinctionProperties="
        + Arrays.toString(distinctionProperties) + "]";
  }

  /**
   * Static constructor for the class. Determines whether position is distinctive for player in the context of
   * distinctionProperties, and it it's true then creates an instance of DistinctivePositionView.
   * @param position the Position to be checked for distinction
   * @param player the Player for which distinction is checked
   * @param distinctionProperties a set of DistinctionProperties in the context of which position distinction is
   *     determined
   * @return an instance of DistinctivePositionView for position or null if position is not distinctive
   */
  public static DistinctivePositionView determinePositionDistinction(@Nonnull PositionView position,
      @Nonnull PlayerView player, @Nonnull Set<DistinctionProperty> distinctionProperties) {
    final Builder builder = new Builder(position);
    for (DistinctionProperty property : distinctionProperties) {
      if (property.appliesToAndVisible(position, player)) {
        builder.addProperty(property);
      }
    }
    return builder.build();
  }

  /**
   * The Builder for the class.
   * @author najtmar
   */
  private static class Builder {

    private final PositionView position;
    private final TreeSet<DistinctionProperty> distinctionProperties = new TreeSet<DistinctionProperty>();

    public Builder(@Nonnull PositionView position) {
      this.position = position;
    }

    public Builder addProperty(@Nonnull DistinctionProperty property) {
      distinctionProperties.add(property);
      return this;
    }

    /**
     *  Returns an instance of {@link DistinctivePositionView} if there is at least one DistinctiveProperty added, or
     *  null otherwise.
     * @return an instance of {@link DistinctivePositionView} if there is at least one DistinctiveProperty added, or
     *     null otherwise
     */
    public DistinctivePositionView build() {
      if (!distinctionProperties.isEmpty()) {
        return new DistinctivePositionView(position, distinctionProperties.toArray(new DistinctionProperty[]{}));
      }
      return null;
    }

  }

  private DistinctivePositionView(@Nonnull PositionView position,
      @Nonnull DistinctionProperty[] distinctionProperties) {
    this.position = position;
    this.distinctionProperties = distinctionProperties;
    this.state = State.CREATED;
  }

}
