package cz.wie.najtmar.gop.client.view;

import cz.wie.najtmar.gop.event.Event;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Client-side representation of class Prawn . Comparable class ordered by id. Takes responsibility for Prawn-Position
 * binding.
 * @author najtmar
 */
public class PrawnView implements Comparable<PrawnView> {

  @SuppressWarnings("unused")
  private static final Logger LOGGER = Logger.getLogger(PrawnView.class.getName());

  /**
   * Maximum energy level for any Prawn.
   */
  public static final double MAX_ENERGY = 1.0;

  /**
   * Type of a Prawn .
   * @author najtmar
   */
  public enum Type {
    WARRIOR,
    SETTLERS_UNIT,
  }

  /**
   * Type of the Prawn .
   */
  private final Type type;

  /**
   * State of a Prawn .
   * @author najtmar
   */
  public enum State {
    EXISTING,
    REMOVED,
  }

  /**
   * State of the Prawn .
   * @author najtmar
   */
  private State state;

  /**
   * Visibility of a Prawn .
   * @author najtmar
   */
  public enum Visibility {
    INVISIBLE,
    VISIBLE,
  }

  /**
   * Visibility of the Prawn .
   */
  private Visibility visibility;

  /**
   * If the Prawn is a Warrior, then this number represents the Warrior's direction.
   */
  private final double warriorDirection;

  /**
   * Player to which the Prawn belongs.
   */
  private final PlayerView player;

  /**
   * Unique ID of the Prawn.
   */
  private final int id;

  /**
   * Current Position of the Prawn.
   */
  private PositionView currentPosition;

  /**
   * Itinerary of the Prawn. null if the itinerary is not set.
   */
  private ItineraryView itinerary;

  /**
   * Current energy level for the Prawn. Can take values [0, MAX_ENERGY].
   */
  private double energy;

  /**
   * If a SettlersUnit is scheduled to create a City, this field is set to this Event. null otherwise.
   */
  private Event cityCreationEvent;

  /**
   * Going on Battles in which the Prawn currently participates.
   */
  private final Set<BattleView> battles;

  /**
   * Constructor.
   * @param player the Player to which the Prawn belongs
   * @param id the unique id of the Prawn
   * @param currentPosition the Position on which the Prawn is located at the beginning
   * @throws ClientViewException when some values are incorrect
   */
  public PrawnView(@Nonnull Type type, @Nonnegative double warriorDirection, @Nonnull PlayerView player,
      @Nonnegative int id, @Nonnull PositionView currentPosition) throws ClientViewException {
    this.type = type;
    this.state = State.EXISTING;
    this.visibility = Visibility.INVISIBLE;
    if (warriorDirection < 0.0 || 2.0 * Math.PI < warriorDirection) {
      throw new ClientViewException("warriorDirection should be between 0.0 and " + (2.0 * Math.PI) + " (found "
          + warriorDirection + ").");
    }
    if (type == Type.SETTLERS_UNIT && warriorDirection != 0.0) {
      throw new ClientViewException("warriorDirection of a SettlersUnit is not meaningful and should be set to 0.0 "
          + "(found " + warriorDirection + ").");
    }
    this.warriorDirection = warriorDirection;
    this.player = player;
    this.id = id;
    if (currentPosition.getJoint() == null) {
      throw new ClientViewException("Prawns can only be created on Joints (found " + currentPosition + ").");
    }
    this.currentPosition = currentPosition;
    this.energy = MAX_ENERGY;

    this.currentPosition.addPrawn(this);
    this.battles = new HashSet<>();
  }

  public Type getType() {
    return type;
  }

  public State getState() {
    return state;
  }

  public Visibility getVisibility() {
    return visibility;
  }

  public void setVisibility(Visibility visibility) {
    this.visibility = visibility;
  }

  public double getWarriorDirection() {
    return warriorDirection;
  }

  public PositionView getCurrentPosition() {
    return currentPosition;
  }

  /**
   * Sets the current Position of the Prawn. Updates the Position-Prawn binding accordingly.
   * @param newPosition the new Position of the Prawn
   * @throws ClientViewException when updating the Position-Prawn binding fails
   */
  public void setCurrentPosition(PositionView newPosition) throws ClientViewException {
    this.currentPosition.removePrawn(this);
    this.currentPosition = newPosition;
    this.currentPosition.addPrawn(this);
  }

  public double getEnergy() {
    return energy;
  }

  // TODO(najtmar): Implement method move(), which moves the Prawn and clears the Itinerary when finished.

  /**
   * Updates the energy of the Prawn .
   * @param energy the value to which the energy should be updated
   * @throws ClientViewException when the energy is out of bound
   */
  public void setEnergy(@Nonnegative double energy) throws ClientViewException {
    if (energy < 0.0 || MAX_ENERGY < energy) {
      throw new ClientViewException("energy should be between 0.0 and " + MAX_ENERGY + " (found " + energy + ").");
    }
    this.energy = energy;
  }

  public PlayerView getPlayer() {
    return player;
  }

  public int getId() {
    return id;
  }

  public ItineraryView getItinerary() {
    return itinerary;
  }

  public void setItinerary(@Nonnull ItineraryView itinerary) {
    this.itinerary = itinerary;
  }

  public void clearItinerary() {
    this.itinerary = null;
  }

  /**
   * Returns the Event to create the City to be executed for this SettlersUnit .
   * @return the Event to create the City to be executed for this SettlersUnit .
   * @throws ClientViewException when the Prawn is not a SettlersUnit
   */
  public Event getCityCreationEvent() throws ClientViewException {
    if (type != PrawnView.Type.SETTLERS_UNIT) {
      throw new ClientViewException("City building Event only makes sense for a Settlers unit.");
    }
    return cityCreationEvent;
  }

  /**
   * Set or unsets the Event to create the City to be executed for this SettlersUnit  .
   * @param cityCreationEvent the Event to be set or null to unset the Event
   * @throws ClientViewException when the operation fails
   */
  public void setCityCreationEvent(@Nullable Event cityCreationEvent) throws ClientViewException {
    if (type != PrawnView.Type.SETTLERS_UNIT) {
      throw new ClientViewException("City building Event only makes sense for a Settlers unit.");
    }
    if (cityCreationEvent == null && this.cityCreationEvent == null) {
      throw new ClientViewException("cityCreationEvent already cleared.");
    }
    if (currentPosition.getJoint() == null) {
      throw new ClientViewException("City can only be created on a Joint (found " + currentPosition + ").");
    }
    if (cityCreationEvent != null) {
      if (cityCreationEvent.getType() != Event.Type.ORDER_CITY_CREATION) {
        throw new ClientViewException("Event Type should be ORDER_CITY_CREATION: " + cityCreationEvent);
      }
      if (cityCreationEvent.getBody().getInt("settlersUnit") != id) {
        throw new ClientViewException("The Event should refer to Prawn id " + id + ": " + cityCreationEvent);
      }
    }
    this.cityCreationEvent = cityCreationEvent;
  }

  /**
   * Removes the Prawn from the Game.
   * @throws ClientViewException when removing the Prawn fails
   */
  public void remove() throws ClientViewException {
    if (this.state == State.REMOVED) {
      throw new ClientViewException("The Prawn has already been removed: " + this);
    }
    this.state = State.REMOVED;
    this.visibility = Visibility.INVISIBLE;
    this.currentPosition.removePrawn(this);
  }

  public Set<BattleView> getBattles() {
    return battles;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof PrawnView)) {
      return false;
    }
    final PrawnView otherPrawn = (PrawnView) obj;
    return id == otherPrawn.id;
  }

  @Override
  public int hashCode() {
    return id;
  }

  @Override
  public int compareTo(PrawnView otherPrawn) {
    return Integer.compare(id, otherPrawn.id);
  }

  @Override
  public String toString() {
    return "PrawnView [type=" + type + ", warriorDirection=" + warriorDirection + ", player="
        + player + ", id=" + id + "]";
  }

}
