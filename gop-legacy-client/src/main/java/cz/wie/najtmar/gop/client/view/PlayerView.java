package cz.wie.najtmar.gop.client.view;

import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.event.EventProcessingException;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.json.Json;

/**
 * Client-side representation of class Player .
 * @author najtmar
 *
 */
public class PlayerView {

  /**
   * Describes possible states a Player can be in.
   * @author najtmar
   */
  public enum State {
    /**
     * The Player is taking part in the Game.
     */
    IN_GAME,
    /**
     * The Player has lost the Game.
     */
    LOST,
    /**
     * The Player has abandoned the Game.
     */
    ABANDONED,
    /**
     * The Player has won the Game.
     */
    WON,
    /**
     * The Game is finished but the Player neither lost nor won (e.g., when a Player abandoned the Game but there are
     * still at least two participants).
     */
    UNDECIDED,
  }

  /**
   * The user who acts as the Player .
   */
  private final UserView user;

  /**
   * The State of the Player in the Game.
   */
  private State state;

  /**
   * Index of the Player in the internal list of Players in {@link Player#mockedGame}.
   */
  private final int index;

  /**
   * Constructor.
   * @param user the user who acts as the Player
   * @param index index of the Player in the internal list of Players in game
   */
  public PlayerView(UserView user, int index) {
    this.user = user;
    this.index = index;
    this.state = State.IN_GAME;
  }

  public int getIndex() {
    return index;
  }

  public UserView getUser() {
    return user;
  }

  public State getState() {
    return state;
  }

  public void setState(State state) {
    this.state = state;
  }

  /**
   * Creates an event of type ADD_PLAYER, to add a given Player to a Game.
   * @param time the time for which the Event is generated
   * @return an instance of the Event
   * @throws EventProcessingException when Event creation fails
   */
  public Event createAddPlayerEvent(@Nonnegative long time) throws EventProcessingException {
    return new Event(Json.createObjectBuilder()
        .add("type", "ADD_PLAYER")
        .add("time", time)
        .add("player", Json.createObjectBuilder()
            .add("user", Json.createObjectBuilder()
              .add("name", user.getName())
              .add("uuid", user.getId().toString())
              .build())
            .add("index", index)
            .build())
        .build());
  }

  /**
   * Creates Events of type CLIENT_FAILURE for the given Player .
   * @param time the time for which the Event is generated
   * @param message the message added to the Event
   * @return a new instance of the CLIENT_FAILURE Event
   * @throws EventProcessingException when Event creation fails
   */
  public Event createClientFailureEvent(@Nonnegative long time, @Nonnull String message)
      throws EventProcessingException {
    return new Event(Json.createObjectBuilder()
        .add("type", "CLIENT_FAILURE")
        .add("time", time)
        .add("player", index)
        .add("message", message)
        .build());
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof PlayerView)) {
      return false;
    }
    final PlayerView otherPlayer = (PlayerView) obj;
    return user.equals(otherPlayer.user) && index == otherPlayer.index;
  }

  @Override
  public int hashCode() {
    return index;
  }

  @Override
  public String toString() {
    return "PlayerView [user=" + user + ", index=" + index + "]";
  }

}
