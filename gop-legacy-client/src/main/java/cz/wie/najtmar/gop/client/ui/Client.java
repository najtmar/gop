package cz.wie.najtmar.gop.client.ui;

import com.google.inject.Inject;

import cz.wie.najtmar.gop.client.view.BattleView;
import cz.wie.najtmar.gop.client.view.CityView;
import cz.wie.najtmar.gop.client.view.ClientViewException;
import cz.wie.najtmar.gop.client.view.GameView;
import cz.wie.najtmar.gop.client.view.JointView;
import cz.wie.najtmar.gop.client.view.ObjectSerializer;
import cz.wie.najtmar.gop.client.view.PrawnView;
import cz.wie.najtmar.gop.client.view.RoadSectionView;
import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.event.SerializationException;
import cz.wie.najtmar.gop.game.PropertiesReader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.GuardedBy;

/**
 * Visualizes Client UI, using a specific Engine .
   * Sample use:
   *
   * <p>client.initialize();
   * while (condition) {
   *   ...
   *   client.snapshot();
   *   ...
   *   client.paint();
   *   ...
   *   Event[] outputEvents = interact(inputEvent);
   *   ...
   * }
   * </p>
 * @author najtmar
 */
public class Client {

  /**
   * The number of the next round to be visualized.
   */
  @GuardedBy("this")
  private int round;

  /**
   * Client view of the Game to be visualized.
   */
  private GameView game;

  /**
   * ObjectSerializer used to deserialize Game object.
   */
  private ObjectSerializer objectSerializer;

  /**
   * The graphical engine used to visualize the Client UI.
   */
  private final OutputEngine outputEngine;

  /**
   * Used to interact with the Game.
   */
  private final InputManager inputManager;

  /**
   * The State of a Client .
   * @author najtmar
   */
  public enum State {
    /**
     * The Client still needs to be initialize()d.
     */
    NOT_INITIALIZED,
    /**
     * The Client is ready to snapshot() the state of the Game .
     */
    READY,
    /**
     * The Client is ready to paint().
     */
    SNAPSHOTTED,
    /**
     * The Client is ready for user interact()ion.
     */
    PAINTED,
  }

  /**
   * The State of the Game.
   */
  @GuardedBy("this")
  private State state;

  /**
   * The current snapshot of RoadSections to paint().
   */
  private RoadSectionView[] roadSectionsSnapshot;

  /**
   * The current snapshot of Prawns to paint(), ordered by their affiliation (own Prawns last).
   */
  private List<PrawnView> orderedPrawnsSnapshot;

  /**
   * The current snapshot of Cities to paint().
   */
  private Set<CityView> citiesSnapshot;

  /**
   * The current snapshot of Battles to paint().
   */
  private List<BattleView> battlesSnapshot;

  /**
   * The Set of Prawns removed in the current round, which should be visible to the user.
   */
  private Set<PrawnView> removedVisiblePrawns;

  /**
   * The Set of Prawns that participate in a removed Battle visible by the Player .
   * This participation makes the Prawn visible even if it wouldn't be otherwise visible.
   */
  private Set<PrawnView> visibleByBattlePrawns;

  /**
   * Constructor.
   * @param propertiesReader used to read Properties used
   * @param outputEngine the graphical engine used to visualize the Client UI
   * @param inputEngine the graphical engine used to get user actions
   */
  @Inject
  Client(@Nonnull PropertiesReader propertiesReader, @Nonnull OutputEngine outputEngine,
      @Nonnull InputEngine inputEngine) {
    this.round = 0;
    this.outputEngine = outputEngine;
    this.inputManager = new InputManager(propertiesReader, inputEngine, outputEngine);
    this.state = State.NOT_INITIALIZED;
  }

  /**
   * Initializes the Client .
   * @param game the client view of the Game to be visualized
   * @throws ClientViewException when initialization fails
   */
  public synchronized void initialize(@Nonnull GameView game) throws ClientViewException {
    if (state != State.NOT_INITIALIZED) {
      throw new ClientViewException("The Client has already been initialized: " + state);
    }
    this.game = game;
    this.objectSerializer = new ObjectSerializer(game);
    outputEngine.initializeOutput(game);
    inputManager.initialize(game);
    state = State.READY;
  }

  /**
   * Makes a snapshot of the current state of game and stores it in the internal data structures.
   * @param inputEvents Events sent to the user
   * @throws ClientViewException when making the snapshot fails
   */
  public synchronized void snapshot(@Nonnull Event[] inputEvents) throws ClientViewException {
    if (state == State.NOT_INITIALIZED) {
      throw new ClientViewException("The Client hasn't been initialized.");
    }
    while (state != State.READY) {
      try {
        wait();
      } catch (InterruptedException exception) {
        notify();
        throw new ClientViewException("Unexpected interrupt.", exception);
      }
    }

    // TODO(najtmar): Create an immutable thread-safe snapshot.
    roadSectionsSnapshot = Arrays.copyOf(game.getRoadSections(), game.getRoadSections().length);
    orderedPrawnsSnapshot = new ArrayList<>(game.getPrawnMap().values());
    Collections.sort(orderedPrawnsSnapshot, (first, second) -> {
      final boolean firstPrawnOwn = first.getPlayer().getIndex() == game.getCurrentPlayerIndex();
      final boolean secondPrawnOwn = second.getPlayer().getIndex() == game.getCurrentPlayerIndex();
      if (firstPrawnOwn == secondPrawnOwn) {
        return 0;
      }
      if (firstPrawnOwn) {
        return 1;
      }
      return -1;
    });
    citiesSnapshot = game.getCities();
    battlesSnapshot = new LinkedList<>(game.getBattles());

    removedVisiblePrawns = new HashSet<>();
    visibleByBattlePrawns = new HashSet<>();
    final Set<PrawnView> removedPrawns = new HashSet<>();
    for (Event event : inputEvents) {
      if (event.getType() == Event.Type.DESTROY_PRAWN) {
        try {
          removedPrawns.add(objectSerializer.deserializePrawn(event.getBody().getInt("prawn")));
        } catch (SerializationException exception) {
          throw new ClientViewException("Event deserialization failed: " + event, exception);
        }
      }
    }
    for (Event event : inputEvents) {
      if (event.getType() == Event.Type.REMOVE_BATTLE) {
        final PrawnView prawn0;
        final PrawnView prawn1;
        try {
          prawn0 = objectSerializer.deserializePrawn(event.getBody().getJsonObject("battle").getInt("prawn0"));
          prawn1 = objectSerializer.deserializePrawn(event.getBody().getJsonObject("battle").getInt("prawn1"));
        } catch (SerializationException exception) {
          throw new ClientViewException("Event deserialization failed: " + event, exception);
        }
        if (prawn0.getPlayer().getIndex() == game.getCurrentPlayerIndex()
            || prawn1.getPlayer().getIndex() == game.getCurrentPlayerIndex()) {
          if (removedPrawns.contains(prawn0)) {
            removedVisiblePrawns.add(prawn0);
            visibleByBattlePrawns.add(prawn1);
          }
          if (removedPrawns.contains(prawn1)) {
            removedVisiblePrawns.add(prawn1);
            visibleByBattlePrawns.add(prawn0);
          }
        }
      }
    }

    state = State.SNAPSHOTTED;
    notify();
  }

  /**
   * Visualizes the current round of game .
   * @throws ClientViewException when visualization fails
   */
  public synchronized void paint() throws ClientViewException {
    if (state == State.NOT_INITIALIZED) {
      throw new ClientViewException("The Client hasn't been initialized.");
    }
    while (state != State.SNAPSHOTTED) {
      try {
        wait();
      } catch (InterruptedException exception) {
        notify();
        throw new ClientViewException("Unexpected interrupt.", exception);
      }
    }

    // Clean up.
    outputEngine.onHighlightingRemoved();

    // RoadSection.Halves .
    for (RoadSectionView roadSection : roadSectionsSnapshot) {
      for (int i = 0; i < 2; ++i) {
        final RoadSectionView.Direction end = RoadSectionView.HALF_ENDS[i];
        switch (roadSection.getHalfVisibilities()[i]) {
          case VISIBLE:
            outputEngine.onRoadSectionHalfVisible(roadSection, end);
            break;
          case DISCOVERED:
            outputEngine.onRoadSectionHalfDiscovered(roadSection, end);
            break;
          case INVISIBLE:
            outputEngine.onRoadSectionHalfInvisible(roadSection, end);
            break;
          default:
            throw new ClientViewException("Unknown RoadSection.Half visibility: "
                + roadSection.getHalfVisibilities()[i]);
        }
      }
    }

    // Prawns.
    for (PrawnView prawn : orderedPrawnsSnapshot) {
      boolean isVisible;
      switch (prawn.getVisibility()) {
        case VISIBLE: {
          final JointView joint = prawn.getCurrentPosition().getJoint();
          if (joint == null || joint.getCity() == null) {
            isVisible = true;
          } else {
            if (!prawn.getBattles().isEmpty()) {
              isVisible = true;
            } else {
              isVisible = false;
            }
          }
          break;
        }
        case INVISIBLE: {
          isVisible = false;
          break;
        }
        default: {
          throw new ClientViewException("Unknown Prawn visibility: " + prawn.getVisibility());
        }
      }
      if (isVisible) {
        outputEngine.onPrawnVisible(prawn);
      } else {
        if (!removedVisiblePrawns.contains(prawn)) {
          if (!visibleByBattlePrawns.contains(prawn)) {
            outputEngine.onPrawnInvisible(prawn);
          } else {
            outputEngine.onPrawnVisible(prawn);
          }
        } else {
          outputEngine.onPrawnJustRemoved(prawn);
        }
      }
    }

    // Cities.
    for (CityView city : citiesSnapshot) {
      switch (city.getVisibility()) {
        case DISCOVERED:
          outputEngine.onCityDiscovered(city);
          break;
        case INVISIBLE:
          outputEngine.onCityInvisible(city);
          break;
        case DESTROYED:
          outputEngine.onCityDestroyed(city);
          break;
        default:
          throw new ClientViewException("Unknown City visibility: " + city.getVisibility());
      }
    }

    // Battles.
    for (BattleView battle : battlesSnapshot) {
      final boolean isVisible;
      if (battle.getState() == BattleView.State.EXISTING) {
        if (battle.getParticipants()[0].getVisibility() == PrawnView.Visibility.VISIBLE
            || battle.getParticipants()[1].getVisibility() == PrawnView.Visibility.VISIBLE) {
          isVisible = true;
        } else {
          isVisible = false;
        }
      } else {
        isVisible = false;
      }
      if (isVisible) {
        outputEngine.onBattleVisible(battle);
      } else {
        outputEngine.onBattleInvisible(battle);
      }
    }

    state = State.PAINTED;
    notify();
  }

  /**
   * Depending on the state of inputManager, sets the state of the Client .
   */
  public synchronized void reviseState() {
    doReviseState();
  }

  /**
   * Implementation of method reviseState() . No synchronized.
   */
  private void doReviseState() {
    if (inputManager.getState() != InputManager.State.GAME_FINISHED
        && inputManager.getState() != InputManager.State.NO_GAME) {
      state = State.READY;
    } else {
      state = State.NOT_INITIALIZED;
    }
  }

  /**
   * Interacts with the user by providing Events and getting a Events produced by the user.
   * @param inputEvents Events sent to the user
   * @return Events produced by the user
   * @throws ClientViewException when user interaction fails
   */
  public synchronized Event[] interact(@Nonnull Event[] inputEvents) throws ClientViewException {
    if (state == State.NOT_INITIALIZED) {
      throw new ClientViewException("The Client hasn't been initialized.");
    }
    while (state != State.PAINTED) {
      try {
        wait();
      } catch (InterruptedException exception) {
        notify();
        throw new ClientViewException("Unexpected interrupt.", exception);
      }
    }

    final Event[] userEvents = inputManager.interact(inputEvents);

    doReviseState();
    notify();
    return userEvents;
  }

  /**
   * Notifies the Client that the Game has been finished with State UNDECIDED .
   * @throws ClientViewException when the notification process fails
   */
  public void notifyGameUndecided() throws ClientViewException {
    inputManager.notifyGameUndecided();
  }

  /**
   * Notifies the User that own connection to the Game server has changed.
   * @param connected whether the Connection is established and working
   * @throws ClientViewException when the notification process fails
   */
  public void notifyOwnGameServerConnection(boolean connected) throws ClientViewException {
    inputManager.notifyOwnGameServerConnection(connected);
  }

  /**
   * Notifies the User that other Game clients' connections to the Game server have changed.
   * @param connected whether the Connection of all other Game clients is established and working
   * @throws ClientViewException when the notification process fails
   */
  public void notifyOthersGameServerConnection(boolean connected) throws ClientViewException {
    inputManager.notifyOthersGameServerConnection(connected);
  }

  public GameView getGame() {
    return game;
  }

}
