package cz.wie.najtmar.gop.client.view;

import cz.wie.najtmar.gop.common.Point2D;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;

/**
 * Client-side representation of class Position .
 * @author najtmar
 */
public class PositionView {

  /**
   * RoadSection to which the Position belongs.
   */
  private final RoadSectionView roadSection;

  /**
   * The index of the Position on roadSection .
   */
  private final int index;

  /**
   * The value returned by method hashCode() (unless the Position is a Joint).
   */
  private final int hashCode;

  /**
   * The coordinates of the Position.
   */
  private final Point2D.Double coordinatesPair;

  /**
   * The Joint to which the Position corresponds or null if the position does not correspond to any Joint .
   */
  private final JointView joint;

  /**
   * Going on Battles in involved the current Position.
   */
  private final Set<BattleView> battles;

  /**
   * The distance in Positions to the closest of the two Joints on roadSection .
   */
  private final int distanceToJoint;

  /**
   * Prawns currently located on the given Position .
   */
  private final Set<PrawnView> prawns = new TreeSet<PrawnView>() {
    private static final long serialVersionUID = 1450940367761331335L;
  };

  /**
   * Constructor.
   * @param roadSection the RoadSection on which the Position is located
   * @param index the index of the Position on the RoadSection
   * @param coordinatesPair the coordinated of the Position on the Board
   * @throws ClientViewException when the object could not be created
   */
  public PositionView(@Nonnull RoadSectionView roadSection, @Nonnegative int index,
      @Nonnull Point2D.Double coordinatesPair) throws ClientViewException {
    this.roadSection = roadSection;
    if (index < 0 || index >= roadSection.getPositions().length) {
      throw new ClientViewException("index must be between 0 and " + (roadSection.getPositions().length - 1)
          + " (found " + index + ").");
    }
    this.index = index;
    this.hashCode = calculateHashCode(this.roadSection, this.index);
    this.coordinatesPair = coordinatesPair;

    final int[] jointPositionIndexes = roadSection.getJointPositionIndexes();
    if (index == jointPositionIndexes[0]) {
      this.joint = roadSection.getJoints()[0];
    } else if (index == jointPositionIndexes[1]) {
      this.joint = roadSection.getJoints()[1];
    } else {
      this.joint = null;
    }
    final int distanceToJoint0 = this.index - this.roadSection.getJointPositionIndexes()[0];
    final int distanceToJoint1 = this.roadSection.getJointPositionIndexes()[1] - this.index;
    if (distanceToJoint0 <= distanceToJoint1) {
      this.distanceToJoint = distanceToJoint0;
    } else {
      this.distanceToJoint = distanceToJoint1;
    }
    this.battles = new HashSet<>();
  }

  public RoadSectionView getRoadSection() {
    return roadSection;
  }

  public int getIndex() {
    return index;
  }

  public Point2D.Double getCoordinatesPair() {
    return coordinatesPair;
  }

  public JointView getJoint() {
    return joint;
  }

  /**
   * Get all Prawns located at the Position. Positions on Joints are handled properly.
   * @return the Prawns located at the given Position
   */
  public Set<PrawnView> getPrawns() {
    if (joint != null) {
      return joint.getNormalizedPosition().prawns;
    }
    return prawns;
  }

  /**
   * Returns the Set of EXISTING Battles in which the current Position is involved.
   * @return the Set of EXISTING Battles in which the current Position is involved
   */
  public Set<BattleView> getBattles() {
    if (joint != null) {
      return joint.getNormalizedPosition().battles;
    }
    return battles;
  }

  /**
   * Adds prawn to the set of Prawns located at the given Position . Positions on Joints are handled properly.
   * @param prawn the Prawn to be added to the set
   * @throws ClientViewException if the Prawn has already been added
   */
  public void addPrawn(@Nonnull PrawnView prawn) throws ClientViewException {
    Set<PrawnView> prawnsToUpdate;
    if (joint == null) {
      prawnsToUpdate = prawns;
    } else {
      prawnsToUpdate = joint.getNormalizedPosition().prawns;
    }
    if (prawnsToUpdate.contains(prawn)) {
      throw new ClientViewException("" + prawn + " already located at " + this + " .");
    }
    prawnsToUpdate.add(prawn);
  }

  /**
   * Removes prawn from the set of Prawns located at the given Position . Positions on Joints are handled properly.
   * @param prawn the Prawn to be removed from the set
   * @throws ClientViewException if the Prawn is not located at the given Position
   */
  public void removePrawn(@Nonnull PrawnView prawn) throws ClientViewException {
    Set<PrawnView> prawnsToUpdate;
    if (joint == null) {
      prawnsToUpdate = prawns;
    } else {
      prawnsToUpdate = joint.getNormalizedPosition().prawns;
    }
    if (!prawnsToUpdate.contains(prawn)) {
      throw new ClientViewException("" + prawn + " not present at " + this + " .");
    }
    prawnsToUpdate.remove(prawn);
  }

  /**
   * Calculates the distance between point and the current Position.
   * @param point the Point from which the distance is measured
   * @return the distance between point and the current Position
   */
  public double calculateDistance(@Nonnull Point2D.Double point) {
    return point.distance(coordinatesPair);
  }

  /**
   * Finds all Positions adjacent to the given Position .
   * @return all Positions adjacent to the given Position
   */
  public Set<PositionView> getAdjacentPositions() {
    if (joint == null) {
      return new HashSet<PositionView>(
          Arrays.asList(roadSection.getPositions()[index - 1], roadSection.getPositions()[index + 1]));
    }
    final Set<PositionView> result = new HashSet<>();
    List<RoadSectionView> roadSections = joint.getRoadSections();
    for (RoadSectionView currentRoadSection : roadSections) {
      final JointView[] currentJoints = currentRoadSection.getJoints();
      final int[] jointAdjacentIndexes = currentRoadSection.getJointAdjacentPointIndexes();
      if (joint.equals(currentJoints[0])) {
        result.add(currentRoadSection.getPositions()[jointAdjacentIndexes[0]]);
      } else {
        result.add(currentRoadSection.getPositions()[jointAdjacentIndexes[1]]);
      }
    }
    return result;
  }

  /**
   * Looks up a RoadSection on which both Positions are located.
   * @param position0 the first of the Positions for which a common RoadSection is looked up
   * @param position1 the second of the Positions for which a common RoadSection is looked up
   * @return a RoadSection on which both Positions are located, or null if no such RoadSection exists
   */
  public static RoadSectionView findCommonRoadSection(@Nonnull PositionView position0,
      @Nonnull PositionView position1) {
    final Set<RoadSectionView> position0RoadSections;
    if (position0.getJoint() == null) {
      position0RoadSections = new HashSet<>(Arrays.asList(position0.getRoadSection()));
    } else {
      position0RoadSections = new HashSet<>(position0.getJoint().getRoadSections());
    }
    final Set<RoadSectionView> position1RoadSections;
    if (position1.getJoint() == null) {
      position1RoadSections = new HashSet<>(Arrays.asList(position1.getRoadSection()));
    } else {
      position1RoadSections = new HashSet<>(position1.getJoint().getRoadSections());
    }
    final Set<RoadSectionView> commonRoadSections = position0RoadSections;
    commonRoadSections.retainAll(position1RoadSections);
    if (commonRoadSections.isEmpty()) {
      return null;
    }
    return commonRoadSections.iterator().next();
  }

  /**
   * Returns all RoadSections to which the Position belongs.
   * @return all RoadSections to which the Position belongs
   */
  public Set<RoadSectionView> getRoadSections() {
    final Set<RoadSectionView> result = new HashSet<>();
    if (joint == null) {
      result.add(roadSection);
    } else {
      result.addAll(joint.getRoadSections());
    }
    return result;
  }

  public int getDistanceToJoint() {
    return distanceToJoint;
  }

  /**
   * Returns the visibility for the given Position .
   * @return the visibility for the given Position
   */
  public RoadSectionView.Visibility getVisibility() {
    final Set<RoadSectionView> roadSections = getRoadSections();
    RoadSectionView.Visibility result = RoadSectionView.Visibility.INVISIBLE;
    for (RoadSectionView currentRoadSection : roadSections) {
      PositionView projectedPosition;
      try {
        projectedPosition = currentRoadSection.getProjectedPosition(this);
      } catch (ClientViewException exception) {
        throw new RuntimeException("This should never happen.", exception);
      }
      final Set<Integer> halfIndexes = new HashSet<>();
      if (projectedPosition.getIndex() <= currentRoadSection.getMidpointIndex()) {
        halfIndexes.add(0);
      }
      if (currentRoadSection.getMidpointIndex() <= projectedPosition.getIndex()) {
        halfIndexes.add(1);
      }
      for (int halfIndex : halfIndexes) {
        if (result.ordinal() < currentRoadSection.getHalfVisibilities()[halfIndex].ordinal()) {
          result = currentRoadSection.getHalfVisibilities()[halfIndex];
        }
      }
    }
    return result;
  }

  /**
   * NOTE: All Positions on a Joint are considered to be the same Position, irrespective of the RoadSection to which
   * they belong.
   */
  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof PositionView)) {
      return false;
    }
    final PositionView otherPosition = (PositionView) obj;
    if (this.joint != null) {
      return this.joint.equals(otherPosition.joint);
    }
    return super.equals(otherPosition);
  }

  private static int calculateHashCode(@Nonnull RoadSectionView roadSection, @Nonnegative int index) {
    return 31 * roadSection.getIndex() + index;
  }

  @Override
  public int hashCode() {
    if (this.joint == null) {
      return hashCode;
    }
    return this.joint.getNormalizedPosition().hashCode;
  }

  @Override
  public String toString() {
    return "PositionView [roadSection=" + roadSection + ", index=" + index + ", coordinatesPair="
        + coordinatesPair + "]";
  }

}
