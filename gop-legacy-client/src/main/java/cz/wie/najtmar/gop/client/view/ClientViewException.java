package cz.wie.najtmar.gop.client.view;

/**
 * Class of Exceptions generated on the client side.
 * @author najtmar
 */
public class ClientViewException extends Exception {

  private static final long serialVersionUID = 6970402248300088028L;

  public ClientViewException() {
  }

  public ClientViewException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

  public ClientViewException(String message, Throwable cause) {
    super(message, cause);
  }

  public ClientViewException(String message) {
    super(message);
  }

  public ClientViewException(Throwable cause) {
    super(cause);
  }

}
