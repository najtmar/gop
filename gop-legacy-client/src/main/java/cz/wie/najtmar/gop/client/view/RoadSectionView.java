package cz.wie.najtmar.gop.client.view;

import cz.wie.najtmar.gop.common.Point2D;

import java.util.Arrays;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;

/**
 * Client-side representation of class RoadSection .
 * @author najtmar
 */
public class RoadSectionView {

  /**
   * Direction on a RoadSection. Either from Joint 0 to Joint 1 (FORWARD) or from Joint 1 to Joint 0 (BACKWARD).
   * @author najtmar
   */
  public enum Direction {
    FORWARD,
    BACKWARD,
  }

  /**
   * Converts the index of a RoadSection.Half to the Direction that represents it.
   */
  public static final Direction[] HALF_ENDS = {Direction.BACKWARD, Direction.FORWARD};

  /**
   * Index of the RoadSection in the internal list of RoadSections in {@link RoadSection#board}.
   */
  private final int index;

  /**
   * Visibility of a RoadSection.Half .
   * @author najtmar
   */
  public enum Visibility {
    /**
     * The RoadSection.Half is invisible to the Player .
     */
    INVISIBLE,
    /**
     * The RoadSection.Half was once discovered by the Player .
     */
    DISCOVERED,
    /**
     * The RoadSection.Half is visible to the Player .
     */
    VISIBLE,
  }

  /**
   * Visibilities of the BACKWARD RoadSection.Half and the FORWARD RoadSection.Half , respectively.
   */
  private final Visibility[] halfVisibilities;

  /**
   * Contains 2 Joints on both ends of the RoadSections.
   */
  private final JointView[] joints;

  /**
   * All Positions on the RoadSection, including both Joints and the midpoint.
   */
  private final PositionView[] positions;

  /**
   * Length of the RoadSection.
   */
  private final double length;

  /**
   * Length of the BACKWARD RoadSection.Half and the FORWARD RoadSection.Half, respectively.
   */
  private final double[] halfLengths;

  /**
   * The step size adjusted to the RoadSection length, so that the length is a multiple of this value.
   */
  private final double realStepSize;

  /**
   * The vector derived from the current RoadSection: (joint_1.x - joint_0.x, joint_1.y - joint_0.y) .
   */
  private final Point2D.Double vector;

  /**
   * Vector for the BACKWARD RoadSection.Half and the FORWARD RoadSection.Half, respectively:
   * (midpoint.x - joint_0.x, midpoint.y - joint_0.y), (joint_1.x - midpoint.x, joint_1.y - midpoint.y) .
   */
  private final Point2D.Double[] halfVectors;

  /**
   * Constructor.
   * @param index the index of the RoadSection
   * @param joint0 the initial Joint of the RoadSection
   * @param joint1 the final Joint of the RoadSection
   * @param stepSize length between two consecutive positions on the RoadSection
   * @throws ClientViewException when the object could not be created
   */
  public RoadSectionView(int index, @Nonnull final JointView joint0, @Nonnull final JointView joint1,
      @Nonnegative double stepSize) throws ClientViewException {
    this.index = index;
    this.joints = new JointView[]{joint0, joint1};
    this.vector = new Point2D.Double(
        joints[1].getCoordinatesPair().getX() - joints[0].getCoordinatesPair().getX(),
        joints[1].getCoordinatesPair().getY() - joints[0].getCoordinatesPair().getY());
    this.length = joint0.getCoordinatesPair().distance(joint1.getCoordinatesPair());
    if (this.length == 0.0) {
      throw new ClientViewException("A RoadSection must have a non-zero length.");
    }
    final double sectionLengthByStepSize = this.length / stepSize;
    final double ceilSectionLengthByStepSize = Math.ceil(sectionLengthByStepSize);
    int positionsCount;
    if (sectionLengthByStepSize != ceilSectionLengthByStepSize) {
      positionsCount = (int) ceilSectionLengthByStepSize;
    } else {
      positionsCount = (int) ceilSectionLengthByStepSize + 1;
    }
    if (positionsCount < 3) {
      throw new ClientViewException("A RoadSection must have at least 3 positions."
          + "sectionLength == " + this.length + ", stepSize == " + stepSize);
    }
    this.realStepSize = this.length / (positionsCount - 1);
    this.positions = new PositionView[positionsCount];
    for (int i = 0; i < positionsCount - 1; ++i) {
      this.positions[i] = new PositionView(this, i, internalCalculateCoordinatesPair(i));
    }
    this.positions[positionsCount - 1] = new PositionView(this, positionsCount - 1,
        new Point2D.Double(joint1.getCoordinatesPair().getX(), joint1.getCoordinatesPair().getY()));
    final int midpointIndex = positions.length / 2;
    this.halfVectors = new Point2D.Double[]{
        new Point2D.Double(
            positions[midpointIndex].getCoordinatesPair().getX() - joints[0].getCoordinatesPair().getX(),
            positions[midpointIndex].getCoordinatesPair().getY() - joints[0].getCoordinatesPair().getY()),
        new Point2D.Double(
            joints[1].getCoordinatesPair().getX() - positions[midpointIndex].getCoordinatesPair().getX(),
            joints[1].getCoordinatesPair().getY() - positions[midpointIndex].getCoordinatesPair().getY()),
    };
    this.halfLengths = new double[]{
        joint0.getCoordinatesPair().distance(positions[midpointIndex].getCoordinatesPair()),
        positions[midpointIndex].getCoordinatesPair().distance(joint1.getCoordinatesPair()),
    };
    this.halfVisibilities = new Visibility[]{Visibility.INVISIBLE, Visibility.INVISIBLE};

    joint0.addRoadSection(this);
    joint1.addRoadSection(this);
  }

  public int getIndex() {
    return index;
  }

  public Visibility[] getHalfVisibilities() {
    return halfVisibilities;
  }

  /**
   * Calculates the coordinates pair corresponding to the Position with the given index on the current RoadSection .
   * @param index the index on the RoadSection; can exceed the size of the RoadSection
   * @return the coordinates pair of the Position with a given index
   */
  public Point2D.Double calculateCoordinatesPair(int index) {
    return internalCalculateCoordinatesPair(index);
  }

  /**
   * Internal implementation of method calculateCoordinatesPair().
   * @param index the index on the RoadSection; can exceed the size of the RoadSection
   * @return the coordinates pair of the Position with a given index
   */
  private Point2D.Double internalCalculateCoordinatesPair(int index) {
    final double roadVectorX = joints[1].getCoordinatesPair().getX() - joints[0].getCoordinatesPair().getX();
    final double roadVectorY = joints[1].getCoordinatesPair().getY() - joints[0].getCoordinatesPair().getY();
    final int positionsCount = positions.length;
    final double x = joints[0].getCoordinatesPair().getX() + roadVectorX * index / (positionsCount - 1);
    final double y = joints[0].getCoordinatesPair().getY() + roadVectorY * index / (positionsCount - 1);
    return new Point2D.Double(x, y);
  }

  /**
   * Sets the visibility of one of the two ends of the RoadSection .
   * @param end the end of the RoadSection whose Visibility should be set
   * @param visibility the Visibility to be set
   */
  public void setHalfVisibility(@Nonnull Direction end, @Nonnull Visibility visibility) {
    if (end == Direction.BACKWARD) {
      halfVisibilities[0] = visibility;
    } else {
      halfVisibilities[1] = visibility;
    }
  }

  public JointView[] getJoints() {
    return joints;
  }

  public PositionView[] getPositions() {
    return positions;
  }

  public double getLength() {
    return length;
  }

  public double[] getHalfLengths() {
    return halfLengths;
  }

  /**
   * Returns 2 indexes in the array {@link RoadSection#positions} corresponding to both joints. Sorted the same way as
   * array {@link RoadSection#joints}.
   */
  public int[] getJointPositionIndexes() {
    return new int[]{0, positions.length - 1};
  }

  /**
   * Returns 2 indexes in the array {@link RoadSection#positions} corresponding to both joint-adjacent point.
   * Sorted the same way as array {@link RoadSection#joints}.
   */
  public int[] getJointAdjacentPointIndexes() {
    return new int[]{1, positions.length - 2};
  }

  /**
   * Checks whether the current RoadSection is adjacent to the joint.
   * @param joint the Joint to be checked
   * @return true iff the current RoadSection is adjacent to the joint
   */
  public boolean adjacentTo(@Nonnull JointView joint) {
    return joints[0].equals(joint) || joints[1].equals(joint);
  }

  /**
   * Returns the index of the joint on the current RoadSection.
   * @param joint Joint to look for the index
   * @return the index of the joint
   * @throws ClientViewException if joint is not adjacent to the RoadSection
   */
  public int getJointIndex(@Nonnull JointView joint) throws ClientViewException {
    if (joints[0].equals(joint)) {
      return 0;
    }
    if (joints[1].equals(joint)) {
      return positions.length - 1;
    }
    throw new ClientViewException("Joint " + joint + " is not on " + this + " .");
  }

  /**
   * Returns the index in the array {@link RoadSection#positions} corresponding to the road section midpoint.
   */
  public int getMidpointIndex() {
    return positions.length / 2;
  }

  double getRealStepSize() {
    return realStepSize;
  }

  Point2D.Double getVector() {
    return vector;
  }

  public Point2D.Double[] getHalfVectors() {
    return halfVectors;
  }

  /**
   * Returns the Position equal to the given position, which has the current RoadSection, or null if such position does
   * not exist.
   * @param position the Position to be projected
   * @return position projected on the current RoadSection or null
   * @throws ClientViewException if the projection process fails
   */
  public PositionView getProjectedPosition(@Nonnull PositionView position) throws ClientViewException {
    if (this.equals(position.getRoadSection())) {
      return position;
    }
    final JointView positionJoint = position.getJoint();
    if (positionJoint == null || !this.adjacentTo(positionJoint)) {
      return null;
    }
    return positions[this.getJointIndex(positionJoint)];
  }

  /**
   * Returns the Position on the current RoadView that is closest to the given point .
   * @param point the Point to which the closes Position is looked for
   * @param sectionVector the vector of the section to measure the distance to
   * @param sectionLength the length of the section to measure the distance to
   * @param sectionStartIndex the index of the first Position in the section
   * @param sectionPositionCount the number of Positions in the section
   * @return the Position on the current RoadView that is closest to the given point
   */
  private PositionView getClosestPositionToSection(@Nonnull Point2D.Double point,
      @Nonnull Point2D.Double sectionVector, @Nonnegative double sectionLength,
      @Nonnegative int sectionStartIndex, @Nonnegative int sectionPositionCount) {
    final Point2D.Double startPoint = positions[sectionStartIndex].getCoordinatesPair();
    final Point2D.Double vector1 =
        new Point2D.Double(point.getX() - startPoint.getX(), point.getY() - startPoint.getY());
    final double distanceVectorLength =
        (sectionVector.getX() * vector1.getX() + sectionVector.getY() * vector1.getY()) / sectionLength;
    if (distanceVectorLength <= 0.0) {
      return positions[sectionStartIndex];
    }
    if (distanceVectorLength >= sectionLength) {
      return positions[sectionStartIndex + sectionPositionCount - 1];
    }
    final int index = (int) Math.round(distanceVectorLength / realStepSize);
    return positions[sectionStartIndex + index];
  }

  /**
   * Returns the Position on the current RoadView that is closest to the given point.
   * @param point the Point to which the closes Position is looked for
   * @return the visible Position on the current RoadView that is closest to the given point, or null if no position is
   *     visible
   */
  public PositionView getClosestVisiblePosition(@Nonnull Point2D.Double point) {
    if (halfVisibilities[0].equals(RoadSectionView.Visibility.INVISIBLE)) {
      if (halfVisibilities[1].equals(RoadSectionView.Visibility.INVISIBLE)) {
        return null;
      } else {
        return getClosestPositionToSection(point, halfVectors[1], halfLengths[1], getMidpointIndex(),
            getJointPositionIndexes()[1] - getMidpointIndex() + 1);
      }
    } else {
      if (halfVisibilities[1].equals(RoadSectionView.Visibility.INVISIBLE)) {
        return getClosestPositionToSection(point, halfVectors[0], halfLengths[0], 0,
            getMidpointIndex() - getJointPositionIndexes()[0] + 1);
      } else {
        return getClosestPositionToSection(point, vector, length, 0, positions.length);
      }
    }
  }

  @Override
  public String toString() {
    return "RoadSectionView [index=" + index + ", joints=" + Arrays.toString(joints) + "]";
  }

}
