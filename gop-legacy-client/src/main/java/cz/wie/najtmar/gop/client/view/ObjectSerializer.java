package cz.wie.najtmar.gop.client.view;

import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.event.SerializationException;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonValue;

/**
 * Serializes and deserializes objects used in the client view of the Game, to be used for Event processing.
 * Depending on the ObjectType, objects are either created or reused. For every supported ObjectType, provides a pair of
 * methods:
 * * SerializationType serializeObjectType(ObjectType object);
 * * ObjectType deserializeObjectType(SerializationType serialized);
 * @author najtmar
 */
public class ObjectSerializer {

  /**
   * Game in the context of which objects are serialized and deserialized.
   */
  private final GameView game;

  public ObjectSerializer(@Nonnull GameView game) {
    this.game = game;
  }

  public GameView getGame() {
    return game;
  }

  /**
   * Serializes objects of type RoadSection.
   * @param roadSection the object to serialize
   * @return serialized roadSection
   */
  public int serializeRoadSection(@Nonnull RoadSectionView roadSection) {
    return roadSection.getIndex();
  }

  /**
   * Deserializes objects of type RoadSection.
   * @param serialized serialized object of type RoadSection
   * @return serialized deserialized to an object of type RoadSection
   * @throws SerializationException when deserialization process fails
   */
  public RoadSectionView deserializeRoadSection(@Nonnegative int serialized) throws SerializationException {
    try {
      return game.getRoadSections()[serialized];
    } catch (IndexOutOfBoundsException exception) {
      throw new SerializationException("" + serialized + " could not be deserialized as RoadSection.", exception);
    }
  }

  /**
   * Serializes objects of type Position.
   * @param position the object to serialize
   * @return serialized position
   */
  public JsonObject serializePosition(@Nonnull PositionView position) {
    final int serializedRoadSection = serializeRoadSection(position.getRoadSection());
    return Json.createObjectBuilder()
        .add("roadSection", serializedRoadSection)
        .add("index", position.getIndex())
        .build();
  }

  /**
   * Deserializes objects of type Position.
   * @param serialized serialized object of type Position
   * @return serialized deserialized to an object of type Position
   * @throws SerializationException when deserialization process fails
   */
  public PositionView deserializePosition(@Nonnull JsonObject serialized) throws SerializationException {
    final RoadSectionView roadSection = deserializeRoadSection(serialized.getInt("roadSection"));
    final int index = serialized.getInt("index");
    return roadSection.getPositions()[index];
  }

  /**
   * Serializes objects of type Itinerary.
   * @param itinerary the object to serialize
   * @return serialized itinerary
   */
  public JsonArray serializeItinerary(@Nullable ItineraryView itinerary) {
    final JsonArrayBuilder builder = Json.createArrayBuilder();
    if (itinerary != null) {
      for (PositionView position : itinerary.getNodes()) {
        builder.add(serializePosition(position));
      }
    }
    return builder.build();
  }

  /**
   * Deserializes new objects of type Itinerary.
   * @param serialized serialized object of type Itinerary; an empty JsonArray means null Itinerary
   * @return serialized deserialized to an object of type Itinerary
   * @throws SerializationException when deserialization process fails
   */
  public ItineraryView deserializeNewItinerary(@Nonnull JsonArray serialized) throws SerializationException {
    if (serialized.size() == 1) {
      throw new SerializationException("Serialized Itinerary must not have 1 element.");
    }
    if (serialized.isEmpty()) {
      return null;
    }
    final Iterator<JsonValue> itineraryIter = serialized.iterator();
    final ItineraryView.Builder builder =
        new ItineraryView.Builder(deserializePosition((JsonObject) itineraryIter.next()));
    while (itineraryIter.hasNext()) {
      final PositionView nextNode = deserializePosition((JsonObject) itineraryIter.next());
      try {
        builder.addNode(nextNode);
      } catch (ClientViewException exception) {
        throw new SerializationException("" + nextNode + " could not be added to the Itinerary.");
      }
    }
    return builder.build();
  }

  /**
   * Serializes objects of type Prawn.
   * @param prawn the object to serialize
   * @return serialized prawn
   */
  public int serializePrawn(@Nonnull PrawnView prawn) {
    return prawn.getId();
  }

  /**
   * Deserializes objects of type Prawn.
   * @param serialized serialized object of type Prawn
   * @return serialized deserialized to an object of type Prawn
   * @throws SerializationException when deserialization process fails
   */
  public PrawnView deserializePrawn(@Nonnegative int serialized) throws SerializationException {
    final PrawnView prawn = game.lookupPrawnById(serialized);
    if (prawn == null) {
      throw new SerializationException("Prawn with id " + serialized + " not found in the Game.");
    }
    return prawn;
  }

  /**
   * Serializes objects of type Player.
   * @param player the object to serialize
   * @return serialized player
   */
  public int serializePlayer(@Nonnull PlayerView player) {
    return player.getIndex();
  }

  /**
   * Deserializes objects of type Player.
   * @param serialized serialized object of type Player
   * @return serialized deserialized to an object of type Player
   * @throws SerializationException when deserialization process fails
   */
  public PlayerView deserializePlayer(@Nonnegative int serialized) throws SerializationException {
    final PlayerView player = game.getPlayers()[serialized];
    if (player == null) {
      throw new SerializationException("Player with index " + serialized + " not found in the Game.");
    }
    return player;
  }

  /**
   * Serializes objects of type City.
   * @param city the object to serialize
   * @return serialized city
   */
  public int serializeCity(@Nonnull CityView city) {
    return city.getId();
  }

  /**
   * Deserializes objects of type City.
   * @param serialized serialized object of type City
   * @return serialized deserialized to an object of type City
   * @throws SerializationException when deserialization process fails
   */
  public CityView deserializeCity(@Nonnegative int serialized) throws SerializationException {
    final CityView city = game.lookupCityById(serialized);
    if (city == null) {
      throw new SerializationException("City with id " + serialized + " not found in the Game.");
    }
    return city;
  }

  /**
   * Serializes objects of type Factory.
   * @param factory the object to serialize
   * @return serialized factory
   */
  public JsonObject serializeFactory(@Nonnull FactoryView factory) {
    return Json.createObjectBuilder()
        .add("city", serializeCity(factory.getCity()))
        .add("index", factory.getIndex())
        .build();
  }

  /**
   * Deserializes objects of type Factory. If the Factory no longer exists, then returns null .
   * @param serialized serialized object of type Factory
   * @return serialized deserialized to an object of type Factory
   * @throws SerializationException when deserialization process fails
   */
  public FactoryView deserializeFactoryOrNull(@Nonnull JsonObject serialized) throws SerializationException {
    final CityView city = game.lookupCityById(serialized.getInt("city"));
    if (city == null) {
      throw new SerializationException("City with id " + serialized + " not found in the Game.");
    }
    final int factoryIndex = serialized.getInt("index");
    if (factoryIndex >= city.getFactories().size()) {
      return null;
    }
    return city.getFactories().get(factoryIndex);
  }

  /**
   * Deserializes new objects of type Move.
   * @param serialized serialized object of type Move
   * @return serialized deserialized to an object of type Move
   * @throws SerializationException when deserialization process fails
   */
  public MoveView deserializeNewMove(@Nonnull JsonObject serialized) throws SerializationException {
    final PositionView position = deserializePosition(serialized.getJsonObject("position"));
    return new MoveView(position, serialized.getJsonNumber("moveTime").longValue(), serialized.getBoolean("stop"));
  }

  /**
   * Deserializes objects of type Battle, which are in State EXISTING .
   * @param serialized serialized object of type Battle
   * @return serialized deserialized to an object of type Battle in State EXISTING
   * @throws SerializationException when deserialization process fails
   */
  public BattleView deserializeExistingBattle(@Nonnegative JsonObject serialized) throws SerializationException {
    final PrawnView participant0 = deserializePrawn(serialized.getInt("prawn0"));
    final PrawnView participant1 = deserializePrawn(serialized.getInt("prawn1"));
    final PositionView normalizedPosition0 = deserializePosition(serialized.getJsonObject("normalizedPosition0"));
    final PositionView normalizedPosition1 = deserializePosition(serialized.getJsonObject("normalizedPosition1"));
    final Set<BattleView> existingBattles =
        StreamSupport.stream(game.getMatchingBattles(participant0, participant1, normalizedPosition0,
            normalizedPosition1)).filter(battle -> battle.getState() == BattleView.State.EXISTING)
        .collect(Collectors.toSet());
    if (existingBattles.size() != 1) {
      throw new SerializationException("There must be exactly one matching Battle (found " + existingBattles + ").");
    }
    return existingBattles.iterator().next();
  }

  /**
   * For a given Event, returns a set of Players that do not process event.
   * @param event the Event to evaluate
   * @return returns a set of Players that do not process event
   * @throws ClientViewException when Event processing logic is broken
   * @throws SerializationException when Event deserailization fails
   */
  public Set<PlayerView> getIgnoringPlayers(@Nonnull Event event)
      throws ClientViewException, SerializationException {
    final Set<PlayerView> result = new HashSet<>();
    switch (event.getType()) {
      case ACTION_FINISHED:
      case SET_FACTORY_PROGRESS: {
        final CityView city = deserializeCity(event.getBody().getJsonObject("factory").getInt("city"));
        result.addAll(Arrays.asList(game.getPlayers()));
        result.remove(city.getPlayer());
        break;
      }
      default:
        break;
    }
    return result;
  }

  /**
   * For a given Event, returns a set of Players for whom event is an interrupting Event.
   * @param event the Event to evaluate
   * @return returns a set of Players for whom event is an interrupting Event
   * @throws ClientViewException when Event processing logic is broken
   * @throws SerializationException when Event deserialization fails
   */
  public Set<PlayerView> getInterruptedPlayers(@Nonnull Event event)
      throws ClientViewException, SerializationException {
    final Set<PlayerView> result = new HashSet<>();
    if (event.isInterrupt()) {
      switch (event.getType()) {
        case ACTION_FINISHED: {
          final CityView city = deserializeCity(event.getBody().getJsonObject("factory").getInt("city"));
          result.add(city.getPlayer());
          break;
        }
        case ADD_BATTLE: {
          final PrawnView participant0 = deserializePrawn(event.getBody().getJsonObject("battle").getInt("prawn0"));
          final PrawnView participant1 = deserializePrawn(event.getBody().getJsonObject("battle").getInt("prawn1"));
          result.add(participant0.getPlayer());
          result.add(participant1.getPlayer());
          break;
        }
        case CAPTURE_OR_DESTROY_CITY: {
          final PlayerView defender = deserializePlayer(event.getBody().getInt("defender"));
          final PlayerView attacker = deserializePlayer(event.getBody().getInt("attacker"));
          result.add(defender);
          result.add(attacker);
          break;
        }
        case CHANGE_PLAYER_STATE: {
          result.addAll(Arrays.asList(game.getPlayers()));
          break;
        }
        case CREATE_CITY: {
          result.add(deserializePrawn(event.getBody().getInt("settlersUnit")).getPlayer());
          break;
        }
        case DESTROY_PRAWN: {
          final PrawnView prawn = deserializePrawn(event.getBody().getInt("prawn"));
          result.add(prawn.getPlayer());
          break;
        }
        case FINISH_GAME: {
          result.addAll(Arrays.asList(game.getPlayers()));
          break;
        }
        case GROW_CITY: {
          final CityView city = deserializeCity(event.getBody().getInt("city"));
          result.add(city.getPlayer());
          break;
        }
        case INITIALIZE_GAME: {
          result.addAll(Arrays.asList(game.getPlayers()));
          break;
        }
        case MOVE_PRAWN: {
          final PrawnView prawn = deserializePrawn(event.getBody().getInt("prawn"));
          result.add(prawn.getPlayer());
          break;
        }
        case PERIODIC_INTERRUPT: {
          result.addAll(Arrays.asList(game.getPlayers()));
          break;
        }
        case PRODUCE_SETTLERS_UNIT: {
          result.add(deserializePlayer(event.getBody().getInt("player")));
          break;
        }
        case PRODUCE_WARRIOR: {
          result.add(deserializePlayer(event.getBody().getInt("player")));
          break;
        }
        case REMOVE_BATTLE: {
          final PrawnView participant0 = deserializePrawn(event.getBody().getJsonObject("battle").getInt("prawn0"));
          final PrawnView participant1 = deserializePrawn(event.getBody().getJsonObject("battle").getInt("prawn1"));
          result.add(participant0.getPlayer());
          result.add(participant1.getPlayer());
          break;
        }
        case SHRINK_OR_DESTROY_CITY: {
          final CityView city = deserializeCity(event.getBody().getInt("city"));
          result.add(city.getPlayer());
          break;
        }

        case ABANDON_PLAYER:
        case ADD_PLAYER:
        case CITY_APPEARS:
        case CITY_SHOWN_DESTROYED:
        case CLEAR_PRAWN_ITINERARY:
        case CLIENT_FAILURE:
        case DECREASE_PRAWN_ENERGY:
        case INCREASE_PROGRESS_BY_UNIVERSAL_DELTA:
        case ORDER_CITY_CREATION:
        case PRAWN_APPEARS:
        case PRAWN_DISAPPEARS:
        case REPAIR_PRAWN:
        case ROAD_SECTION_HALF_APPEARS:
        case ROAD_SECTION_HALF_DISAPPEARS:
        case ROAD_SECTION_HALF_DISCOVERED:
        case SET_FACTORY_PROGRESS:
        case SET_GROW_CITY_ACTION:
        case SET_PRAWN_ITINERARY:
        case SET_PRODUCE_SETTLERS_UNIT_ACTION:
        case SET_PRODUCE_WARRIOR_ACTION:
        case SET_REPAIR_PRAWN_ACTION:
        case STAY_PRAWN:
        case TEST:
        default:
          throw new ClientViewException("Event not expected to be interruptive: " + event);
      }
    } else {
      switch (event.getType()) {
        case ABANDON_PLAYER:
        case ADD_PLAYER:
        case CITY_APPEARS:
        case CITY_SHOWN_DESTROYED:
        case CLEAR_PRAWN_ITINERARY:
        case CLIENT_FAILURE:
        case CREATE_CITY:
        case DECREASE_PRAWN_ENERGY:
        case INCREASE_PROGRESS_BY_UNIVERSAL_DELTA:
        case MOVE_PRAWN:
        case ORDER_CITY_CREATION:
        case PRAWN_APPEARS:
        case PRAWN_DISAPPEARS:
        case REPAIR_PRAWN:
        case ROAD_SECTION_HALF_APPEARS:
        case ROAD_SECTION_HALF_DISAPPEARS:
        case ROAD_SECTION_HALF_DISCOVERED:
        case SET_FACTORY_PROGRESS:
        case SET_GROW_CITY_ACTION:
        case SET_PRAWN_ITINERARY:
        case SET_PRODUCE_SETTLERS_UNIT_ACTION:
        case SET_PRODUCE_WARRIOR_ACTION:
        case SET_REPAIR_PRAWN_ACTION:
        case STAY_PRAWN:
        case TEST:
          break;

        case ACTION_FINISHED:
        case ADD_BATTLE:
        case CAPTURE_OR_DESTROY_CITY:
        case CHANGE_PLAYER_STATE:
        case DESTROY_PRAWN:
        case FINISH_GAME:
        case GROW_CITY:
        case INITIALIZE_GAME:
        case PERIODIC_INTERRUPT:
        case PRODUCE_SETTLERS_UNIT:
        case PRODUCE_WARRIOR:
        case REMOVE_BATTLE:
        case SHRINK_OR_DESTROY_CITY:
        default:
          throw new ClientViewException("Event not expected to be non interruptive: " + event);
      }
    }
    return result;
  }

}
