package cz.wie.najtmar.gop.client.view;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;

/**
 * Client-side representation of class City . Takes responsibility for City-Joint binding.
 * @author najtmar
 */
public class CityView {

  /**
   * The name of the City.
   */
  private final String name;

  /**
   * Unique ID of the City. Used as hash code.
   */
  private final int id;

  /**
   * State of the City .
   * @author najtmar
   */
  public enum State {
    EXISTING,
    REMOVED,
  }

  /**
   * State of the City .
   */
  private State state;

  /**
   * Visibility of a City .
   * @author najtmar
   */
  public enum Visibility {
    /**
     * Not yet seen by the Player .
     */
    INVISIBLE,
    /**
     * Already seen by the Player . Destruction not confirmed.
     */
    DISCOVERED,
    /**
     * Confirmed that the City has been destroyed.
     */
    DESTROYED,
  }

  /**
   * Visibility of the City .
   */
  private Visibility visibility;

  /**
   * Joint on which the City is located.
   */
  private final JointView joint;

  /**
   * The list of all Factories that the City have. The number of Factories is equal to the size of the City.
   */
  private final List<FactoryView> factories;

  /**
   * The index of the first factory to be visualized.
   */
  private int firstFactoryShownIndex;

  /**
   * Player to which the City belongs.
   */
  private PlayerView player;

  /**
   * The angles for RoadSections adjacent to the City, as observed from the City.
   */
  private final double[] roadSectionAngles;

  /**
   * Creates a City of the initial size 1.
   * @param name the name of the City
   * @param id the id of the City
   * @param joint the Joint on which the City is located
   * @param player the Player to which the City belongs
   * @throws ClientViewException when the City cannot be initialized
   */
  public CityView(@Nonnull String name, @Nonnegative int id, @Nonnull JointView joint, @Nonnull PlayerView player)
      throws ClientViewException {
    this.name = name;
    this.id = id;
    this.state = State.EXISTING;
    this.visibility = Visibility.INVISIBLE;
    this.joint = joint;
    this.factories = new LinkedList<>(Arrays.asList(new FactoryView(this, 0)));
    this.firstFactoryShownIndex = 0;
    this.player = player;
    final List<RoadSectionView> roadSections = this.joint.getRoadSections();
    this.roadSectionAngles = new double[roadSections.size()];
    for (int i = 0; i < this.roadSectionAngles.length; ++i) {
      final RoadSectionView roadSection = roadSections.get(i);
      if (roadSection.getJoints()[0].equals(this.joint)) {
        roadSectionAngles[i] = Math.atan2(roadSection.getVector().getY(), roadSection.getVector().getX());
      } else {
        roadSectionAngles[i] = Math.atan2(-roadSection.getVector().getY(), -roadSection.getVector().getX());
      }
      if (roadSectionAngles[i] < 0) {
        roadSectionAngles[i] += 2 * Math.PI;
      }
    }

    joint.setCity(this);
  }

  public String getName() {
    return name;
  }

  public int getId() {
    return id;
  }

  public State getState() {
    return state;
  }

  public Visibility getVisibility() {
    return visibility;
  }

  public void setVisibility(Visibility visibility) {
    this.visibility = visibility;
  }

  public JointView getJoint() {
    return joint;
  }

  public List<FactoryView> getFactories() {
    return factories;
  }

  public int getFirstFactoryShownIndex() {
    return firstFactoryShownIndex;
  }

  /**
   * Increases firstFactoryShownIndex by 1, if still not all Factories are shown.
   * @param cityFactoryBoxMaxCount the maximum number of CityFactory boxes shown in the City view
   * @return true iff firstFactoryShownIndex has been increased
   */
  public boolean tryIncrementFirstFactoryShownIndex(@Nonnegative int cityFactoryBoxMaxCount) {
    if (firstFactoryShownIndex + cityFactoryBoxMaxCount - Integer.signum(firstFactoryShownIndex) < factories.size()) {
      ++firstFactoryShownIndex;
      return true;
    }
    return false;
  }

  /**
   * Decreases firstFactoryShownIndex by 1, if still not all Factories are shown.
   * @return true iff firstFactoryShownIndex has been decreased
   */
  public boolean tryDecrementFirstFactoryShownIndex() {
    if (firstFactoryShownIndex > 0) {
      --firstFactoryShownIndex;
      return true;
    }
    return false;
  }

  public void resetFirstFactoryShownIndex() {
    firstFactoryShownIndex = 0;
  }

  public PlayerView getPlayer() {
    return player;
  }

  public double[] getRoadSectionAngles() {
    return roadSectionAngles;
  }

  /**
   * Increases the City size by 1.
   */
  public void grow() {
    factories.add(new FactoryView(this, factories.size()));
  }

  /**
   * Implementation of method shrinkOrDestroy().
   * @param index the index of the CityFactory to be removed
   * @throws ClientViewException when the operation fails
   */
  private void doShrinkOrDestroy(@Nonnegative int index) throws ClientViewException {
    if (state == State.REMOVED || factories.size() == 0) {
      throw new ClientViewException("The City is already destroyed: " + this);
    }
    factories.remove(index);
    for (int i = index; i < factories.size(); ++i) {
      factories.get(i).setIndex(i);
    }
    if (factories.size() == 0) {
      this.joint.removeCity(this);
      this.state = State.REMOVED;
    }
  }

  /**
   * Decreases the City size by 1. Reaching size 0 means City destruction.
   * @param index the index of the CityFactory to be removed
   * @throws ClientViewException when the operation could not be performed
   */
  public void shrinkOrDestroy(@Nonnegative int index) throws ClientViewException {
    doShrinkOrDestroy(index);
  }

  /**
   * Transfers the ownership of the City to player and decreases the City size by 1. Reaching size 0 means City
   * destruction.
   * @param newOwner the Player that captures the City
   * @throws ClientViewException when the operation could not be performed
   */
  public void captureOrDestroy(@Nonnull PlayerView newOwner) throws ClientViewException {
    if (newOwner.equals(player)) {
      throw new ClientViewException("" + newOwner + " already owns the City.");
    }
    player = newOwner;
    doShrinkOrDestroy(factories.size() - 1);
    for (FactoryView factory : factories) {
      factory.finishAction();
    }
  }

  @Override
  public int hashCode() {
    return id;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof CityView)) {
      return false;
    }
    final CityView otherCity = (CityView) obj;
    return id == otherCity.id;
  }

  @Override
  public String toString() {
    return "CityView [name=" + name + ", id=" + id + ", joint=" + joint + ", factories=" + factories
        + ", player=" + player + "]";
  }

}
