package cz.wie.najtmar.gop.client.view;

import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.event.EventProcessingException;
import cz.wie.najtmar.gop.event.SerializationException;
import cz.wie.najtmar.gop.io.Channel;
import cz.wie.najtmar.gop.io.Message;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.json.JsonObject;

/**
 * Processes incoming Events, replays a Game, and sends client-side events, on behalf of a Player.
 * @author najtmar
 */
public class GameEventProcessor {

  private static final Logger LOGGER = Logger.getLogger(GameEventProcessor.class.getName());

  /**
   * The User on whose Player's behalf the Game is played.
   */
  @Deprecated
  private final UserView user;

  /**
   * The index of the main Player in the Game.
   */
  @Deprecated
  private final int playerIndex;

  /**
   * The Player on behalf of whom the Game is played.
   */
  private PlayerView player;

  /**
   * The Channel used for communication with the Server.
   */
  private final Channel channel;

  /**
   * Whether the Processor has been initialized or not.
   */
  private boolean initialized;

  /**
   * Client-side representation of a Game.
   */
  private GameView game;

  /**
   * ObjectSerializer used to serialize/deserialize objects.
   */
  private ObjectSerializer objectSerializer;

  /**
   * EventFactory used to create Events.
   */
  private EventFactory eventFactory;

  /**
   * Constructor.
   * @param channel the Channel used for communication with the Server
   * @param game the Game for which Events are processed
   */
  public GameEventProcessor(@Nonnull Channel channel, @Nonnull GameView game) {
    this.channel = channel;
    this.game = game;
    this.player = game.getPlayers()[game.getCurrentPlayerIndex()];
    this.objectSerializer = new ObjectSerializer(game);
    this.initialized = true;

    // Deprecated fields.
    this.user = null;
    this.playerIndex = -1;
  }

  /**
   * Constructor.
   * @param user the User on behalf of whose Player the Game is replayed
   * @param playerIndex the index of the main Player in the Game
   * @param channel the Channel used for communication with the Server
   * @deprecated the constructor that leaves the object initialized should be used
   */
  @Deprecated
  public GameEventProcessor(@Nonnull UserView user, @Nonnegative int playerIndex, @Nonnull Channel channel) {
    this.user = user;
    this.playerIndex = playerIndex;
    this.channel = channel;
    this.initialized = false;
  }

  /**
   * Initializes the object.
   * @throws ClientViewException when the object could not be initialized
   * @deprecated the constructor that leaves the object initialized should be used
   */
  @Deprecated
  public void initialize() throws ClientViewException {
    if (initialized) {
      throw new ClientViewException("Already initialized.");
    }

    // Write ADD_PLAYER Message .
    try {
      channel.writeMessage(new Message(new Event[]{(new PlayerView(user, playerIndex)).createAddPlayerEvent(0)}));
    } catch (EventProcessingException | InterruptedException exception) {
      throw reportErrorAndCreateException("ADD_PLAYER message creation failed.", exception);
    }

    // Initialize the Game .
    final Message initializationMessage;
    try {
      initializationMessage = channel.readMessage();
    } catch (InterruptedException exception) {
      throw reportErrorAndCreateException("Reading initialization message failed.", exception);
    }
    final Event[] initializationEvents = initializationMessage.getEvents();
    if (initializationEvents.length == 0 || initializationEvents[0].getType() != Event.Type.INITIALIZE_GAME) {
      throw reportErrorAndCreateException("Initialization message should start with an INITIALIZE_GAME Event (found "
          + initializationMessage + ").", null);
    }
    try {
      this.game = new GameView(initializationEvents[0].getBody().getJsonObject("game"), playerIndex);
    } catch (ClientViewException exception) {
      throw reportErrorAndCreateException("Unable to initialize Game from Event " + initializationEvents[0], exception);
    }
    this.player = this.game.getPlayers()[playerIndex];
    this.objectSerializer = new ObjectSerializer(game);
    this.eventFactory = new EventFactory(objectSerializer);
    this.game.advanceTimeTo(0);
    for (int i = 1; i < initializationEvents.length; ++i) {
      final Event event = initializationEvents[i];
      game.advanceTimeTo(event.getTime());
      switch (event.getType()) {
        case PRODUCE_SETTLERS_UNIT:
          try {
            game.addPrawn(new PrawnView(PrawnView.Type.SETTLERS_UNIT, 0.0,
                objectSerializer.deserializePlayer(event.getBody().getInt("player")), event.getBody().getInt("id"),
                objectSerializer.deserializePosition(event.getBody().getJsonObject("position"))));
          } catch (ClientViewException | SerializationException exception) {
            throw reportErrorAndCreateException("Unable to reproduce a SettlersUnit from " + event, exception);
          }
          break;

        case PRODUCE_WARRIOR:
          try {
            game.addPrawn(new PrawnView(PrawnView.Type.WARRIOR,
                event.getBody().getJsonNumber("direction").doubleValue(),
                objectSerializer.deserializePlayer(event.getBody().getInt("player")), event.getBody().getInt("id"),
                objectSerializer.deserializePosition(event.getBody().getJsonObject("position"))));
          } catch (ClientViewException | SerializationException exception) {
            throw reportErrorAndCreateException("Unable to reproduce a Warrior from " + event, exception);
          }
          break;

        case ROAD_SECTION_HALF_DISCOVERED: {
          try {
            final RoadSectionView roadSection = objectSerializer.deserializeRoadSection(
                event.getBody().getJsonObject("roadSectionHalf").getInt("roadSection"));
            final RoadSectionView.Direction end =
                RoadSectionView.Direction.valueOf(event.getBody().getJsonObject("roadSectionHalf").getString("end"));
            roadSection.setHalfVisibility(end, RoadSectionView.Visibility.VISIBLE);
          } catch (SerializationException exception) {
            throw reportErrorAndCreateException("Unable to extract RoadSection.Half from " + event, exception);
          }
          break;
        }

        default: {
          throw reportErrorAndCreateException("Unexpected Event Type: " + event.getType(), null);
        }
      }
    }
    this.initialized = true;
  }

  public boolean isInitialized() {
    return initialized;
  }

  /**
   * Returns the Game being processed.
   * @return the Game being processed
   * @throws ClientViewException is the Game hasn't been initialized
   */
  public GameView getGame() throws ClientViewException {
    if (!initialized) {
      throw new ClientViewException("Game not initialized.");
    }
    return game;
  }

  public EventFactory getEventFactory() {
    return eventFactory;
  }

  public ObjectSerializer getObjectSerializer() {
    return objectSerializer;
  }

  void injectObjectSerializer(@Nonnull ObjectSerializer objectSerializer) {
    this.objectSerializer = objectSerializer;
  }

  /**
   * Updates game based on event. For some type of Events, only the Events that apply to the current Player are
   * processed.
   * @param event the Event based on which game is updated
   * @throws ClientViewException when Event processing fails
   * @throws SerializationException when Event deserialization fails
   */
  void processEvent(@Nonnull Event event) throws ClientViewException, SerializationException {
    if (LOGGER.isLoggable(Level.INFO)) {
      LOGGER.info("Processing Event " + event.getBody() + " by Player " + player.getIndex() + " .");
    }
    if (objectSerializer.getIgnoringPlayers(event).contains(player)) {
      return;
    }
    switch (event.getType()) {
      case ACTION_FINISHED: {
        final FactoryView factory = objectSerializer.deserializeFactoryOrNull(event.getBody().getJsonObject("factory"));
        if (factory != null) {
          factory.finishAction();
        }
        break;
      }
      case ADD_BATTLE: {
        final JsonObject serializedBattle = event.getBody().getJsonObject("battle");
        final PrawnView participant0 = objectSerializer.deserializePrawn(serializedBattle.getInt("prawn0"));
        final PrawnView participant1 = objectSerializer.deserializePrawn(serializedBattle.getInt("prawn1"));
        final BattleView newBattle = new BattleView(participant0, participant1, event.getTime());
        game.addBattle(newBattle);
        break;
      }
      case CAPTURE_OR_DESTROY_CITY: {
        final CityView city = objectSerializer.deserializeCity(event.getBody().getInt("city"));
        final PlayerView defender = city.getPlayer();
        final PlayerView attacker = objectSerializer.deserializePlayer(event.getBody().getInt("attacker"));
        city.captureOrDestroy(attacker);
        game.captureCity(city, defender, attacker);
        if (city.getState() == CityView.State.REMOVED) {
          game.removeCity(city);
        }
        break;
      }
      case CHANGE_PLAYER_STATE: {
        final PlayerView affectedPlayer = objectSerializer.deserializePlayer(event.getBody().getInt("player"));
        final PlayerView.State affectedPlayerState = PlayerView.State.valueOf(event.getBody().getString("state"));
        affectedPlayer.setState(affectedPlayerState);
        break;
      }
      case CITY_APPEARS : {
        final CityView city = objectSerializer.deserializeCity(event.getBody().getInt("city"));
        final PlayerView affectedPlayer = objectSerializer.deserializePlayer(event.getBody().getInt("player"));
        if (!affectedPlayer.equals(player)) {
          throw new ClientViewException("" + event + " was intended for " + affectedPlayer + ", not for "
              + player);
        }
        city.setVisibility(CityView.Visibility.DISCOVERED);
        break;
      }
      case CITY_SHOWN_DESTROYED : {
        final CityView city = objectSerializer.deserializeCity(event.getBody().getInt("city"));
        final PlayerView affectedPlayer = objectSerializer.deserializePlayer(event.getBody().getInt("player"));
        if (!affectedPlayer.equals(player)) {
          throw new ClientViewException("" + event + " was intended for " + affectedPlayer + ", not for "
              + player);
        }
        if (city.getState() != CityView.State.REMOVED) {
          throw new ClientViewException("" + city + " is not in state REMOVED .");
        }
        city.setVisibility(CityView.Visibility.DESTROYED);
        break;
      }
      case CREATE_CITY: {
        final PrawnView settlersUnit = objectSerializer.deserializePrawn(event.getBody().getInt("settlersUnit"));
        game.addCity(new CityView(event.getBody().getString("name"), event.getBody().getInt("id"),
            settlersUnit.getCurrentPosition().getJoint(), settlersUnit.getPlayer()));
        settlersUnit.remove();
        break;
      }
      case DECREASE_PRAWN_ENERGY: {
        final PrawnView prawn = objectSerializer.deserializePrawn(event.getBody().getInt("prawn"));
        final double delta = event.getBody().getJsonNumber("delta").doubleValue();
        if (LOGGER.isLoggable(Level.INFO)) {
          LOGGER.info("Prawn " + prawn.getId() + " decreases its energy of " + prawn.getEnergy() + " by " + delta
              + " .");
        }
        double newEnergy = prawn.getEnergy() - delta;
        if (newEnergy < 0.0) {
          newEnergy = 0.0;
        }
        prawn.setEnergy(newEnergy);
        break;
      }
      case DESTROY_PRAWN: {
        final PrawnView prawn = objectSerializer.deserializePrawn(event.getBody().getInt("prawn"));
        if (LOGGER.isLoggable(Level.INFO)) {
          LOGGER.info("Prawn " + prawn.getId() + " destroyed.");
        }
        prawn.remove();
        break;
      }
      case MOVE_PRAWN: {
        final PrawnView prawn = objectSerializer.deserializePrawn(event.getBody().getInt("prawn"));
        final MoveView move = objectSerializer.deserializeNewMove(event.getBody().getJsonObject("move"));
        prawn.setCurrentPosition(move.getPosition());
        if (prawn.getPlayer().equals(player)) {
          if (move.isStop()) {
            prawn.clearItinerary();
          } else {
            final ItineraryView itinerary = prawn.getItinerary();
            if (itinerary != null) {
              try {
                itinerary.updateItinerary(move.getPosition());
              } catch (ClientViewException exception) {
                throw new ClientViewException("Error moving " + prawn + " from " + prawn.getCurrentPosition() + ".",
                    exception);
              }
            }
          }
        }
        break;
      }
      case PRAWN_APPEARS : {
        final PrawnView prawn = objectSerializer.deserializePrawn(event.getBody().getInt("prawn"));
        final PlayerView affectedPlayer = objectSerializer.deserializePlayer(event.getBody().getInt("player"));
        if (!affectedPlayer.equals(player)) {
          throw new ClientViewException("" + event + " was intended for " + affectedPlayer + ", not for "
              + player);
        }
        prawn.setVisibility(PrawnView.Visibility.VISIBLE);
        break;
      }
      case PRAWN_DISAPPEARS : {
        final PrawnView prawn = objectSerializer.deserializePrawn(event.getBody().getInt("prawn"));
        final PlayerView affectedPlayer = objectSerializer.deserializePlayer(event.getBody().getInt("player"));
        if (!affectedPlayer.equals(player)) {
          throw new ClientViewException("" + event + " was intended for " + affectedPlayer + ", not for "
              + player);
        }
        prawn.setVisibility(PrawnView.Visibility.INVISIBLE);
        break;
      }
      case PRODUCE_SETTLERS_UNIT: {
        game.addPrawn(new PrawnView(PrawnView.Type.SETTLERS_UNIT, 0.0,
            objectSerializer.deserializePlayer(event.getBody().getInt("player")), event.getBody().getInt("id"),
            objectSerializer.deserializePosition(event.getBody().getJsonObject("position"))));
        break;
      }
      case PRODUCE_WARRIOR: {
        final PrawnView newWarrior = new PrawnView(PrawnView.Type.WARRIOR,
            event.getBody().getJsonNumber("direction").doubleValue(),
            objectSerializer.deserializePlayer(event.getBody().getInt("player")), event.getBody().getInt("id"),
            objectSerializer.deserializePosition(event.getBody().getJsonObject("position")));
        game.addPrawn(newWarrior);
        if (LOGGER.isLoggable(Level.INFO)) {
          LOGGER.info("Warrior " + newWarrior.getId() + " created in "
              + newWarrior.getCurrentPosition().getJoint().getCity().getName() + " of Player "
              + newWarrior.getPlayer().getUser().getName());
        }
        break;
      }
      case REMOVE_BATTLE: {
        final BattleView battle = objectSerializer.deserializeExistingBattle(event.getBody().getJsonObject("battle"));
        battle.finish(event.getTime());
        break;
      }
      case REPAIR_PRAWN: {
        final PrawnView prawn = objectSerializer.deserializePrawn(event.getBody().getInt("prawn"));
        final double delta = event.getBody().getJsonNumber("delta").doubleValue();
        double newEnergy = prawn.getEnergy() + delta;
        if (newEnergy > PrawnView.MAX_ENERGY) {
          newEnergy = PrawnView.MAX_ENERGY;
        }
        prawn.setEnergy(newEnergy);
        break;
      }
      case ROAD_SECTION_HALF_APPEARS : {
        final RoadSectionView roadSection = objectSerializer.deserializeRoadSection(
            event.getBody().getJsonObject("roadSectionHalf").getInt("roadSection"));
        final RoadSectionView.Direction end =
            RoadSectionView.Direction.valueOf(event.getBody().getJsonObject("roadSectionHalf").getString("end"));
        roadSection.setHalfVisibility(end, RoadSectionView.Visibility.VISIBLE);
        break;
      }
      case ROAD_SECTION_HALF_DISAPPEARS : {
        final RoadSectionView roadSection = objectSerializer.deserializeRoadSection(
            event.getBody().getJsonObject("roadSectionHalf").getInt("roadSection"));
        final RoadSectionView.Direction end =
            RoadSectionView.Direction.valueOf(event.getBody().getJsonObject("roadSectionHalf").getString("end"));
        roadSection.setHalfVisibility(end, RoadSectionView.Visibility.DISCOVERED);
        break;
      }
      case ROAD_SECTION_HALF_DISCOVERED : {
        final RoadSectionView roadSection = objectSerializer.deserializeRoadSection(
            event.getBody().getJsonObject("roadSectionHalf").getInt("roadSection"));
        final RoadSectionView.Direction end =
            RoadSectionView.Direction.valueOf(event.getBody().getJsonObject("roadSectionHalf").getString("end"));
        roadSection.setHalfVisibility(end, RoadSectionView.Visibility.VISIBLE);
        break;
      }
      case SET_FACTORY_PROGRESS: {
        final FactoryView factory = objectSerializer.deserializeFactoryOrNull(event.getBody().getJsonObject("factory"));
        final double progress = event.getBody().getJsonNumber("progress").doubleValue();
        factory.setProgress(progress);
        break;
      }
      case SHRINK_OR_DESTROY_CITY: {
        final CityView city = objectSerializer.deserializeCity(event.getBody().getInt("city"));
        final int index = event.getBody().getInt("factoryIndex");
        city.shrinkOrDestroy(index);
        if (LOGGER.isLoggable(Level.INFO)) {
          LOGGER.info("City '" + city.getName() + "' shrinked to size " + city.getFactories().size() + " .");
        }
        if (city.getState() == CityView.State.REMOVED) {
          game.removeCity(city);
        }
        break;
      }
      case GROW_CITY: {
        final CityView city = objectSerializer.deserializeCity(event.getBody().getInt("city"));
        city.grow();
        if (LOGGER.isLoggable(Level.INFO)) {
          LOGGER.info("City '" + city.getName() + "' grew to size " + city.getFactories().size() + " .");
        }
        break;
      }

      case STAY_PRAWN:
      case ABANDON_PLAYER:
      case CLEAR_PRAWN_ITINERARY:
      case CLIENT_FAILURE:
      case FINISH_GAME:
      case INCREASE_PROGRESS_BY_UNIVERSAL_DELTA:
      case ORDER_CITY_CREATION:
      case PERIODIC_INTERRUPT:
      case SET_GROW_CITY_ACTION:
      case SET_PRAWN_ITINERARY:
      case SET_PRODUCE_SETTLERS_UNIT_ACTION:
      case SET_PRODUCE_WARRIOR_ACTION:
      case SET_REPAIR_PRAWN_ACTION:
      case TEST:
        // Ignored.
        break;
      case ADD_PLAYER:
      case INITIALIZE_GAME:
        throw new ClientViewException("Unexpected Event Type: " + event);
      default:
        throw new ClientViewException("Unrecognized Event Type: " + event);
    }
  }

  /**
   * Read Events from the input Channel. Updates game and the current time based on the Events read.
   * @return the Events read from the input Channel
   * @throws ClientViewException when the Events cannot be read
   * @throws SerializationException when Event deserialization fails
   */
  public Event[] readAndProcessEvents() throws ClientViewException, SerializationException {
    final Message message;
    try {
      message = channel.readMessage();
    } catch (InterruptedException exception) {
      throw reportErrorAndCreateException("Reading input message failed.", exception);
    }
    final Event[] events = message.getEvents();
    for (Event event : events) {
      game.advanceTimeTo(event.getTime());
      processEvent(event);
    }
    return events;
  }

  /**
   * Write Events to the output Channel. Events must not advance EventFactory time.
   * @param events the Events to write
   * @throws ClientViewException when the Events are incorrect or could not be written
   */
  public void writeEvents(@Nonnull Event[] events) throws ClientViewException {
    for (Event event : events) {
      try {
        if (event.getTime() != game.getTime()) {
          throw reportErrorAndCreateException("Event time must be equal to the current EventFactory time ("
              + event.getTime() + " != " + game.getTime() + ").", null);
        }
      } catch (NullPointerException exception) {
        throw new ClientViewException("Events: " + Arrays.toString(events), exception);
      }
    }
    try {
      channel.writeMessage(new Message(events));
    } catch (InterruptedException exception) {
      throw reportErrorAndCreateException("Events could not be written.", exception);
    }
  }

  /**
   * Used to report errors using the communication Channel.
   * @param errorMessage the message to be reported
   * @param is not null, then contains the exception that caused the error
   * @return a ClientViewException containing the error message
   */
  private ClientViewException reportErrorAndCreateException(@Nonnull String errorMessage,
      @Nullable Exception originalException) {
    final long time;
    if (initialized) {
      time = game.getTime();
    } else {
      time = 0;
    }
    final ClientViewException result;
    if (originalException != null) {
      result = new ClientViewException(errorMessage, originalException);
    } else {
      result = new ClientViewException(errorMessage);
    }
    result.printStackTrace(System.err);
    try {
      channel.writeMessage(new Message(new Event[]{player.createClientFailureEvent(time, errorMessage)}));
    } catch (EventProcessingException | InterruptedException exception) {
      System.err.println(new ClientViewException("Failed to send a Client failure message.", exception));
    }
    return result;
  }

}
