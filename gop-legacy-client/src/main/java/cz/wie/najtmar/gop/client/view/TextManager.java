package cz.wie.najtmar.gop.client.view;

import com.google.inject.Inject;

import cz.wie.najtmar.gop.client.ui.InputManager;
import java8.util.stream.Collectors;
import java8.util.stream.RefStreams;

import org.threeten.bp.LocalDate;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.format.FormatStyle;

import java.util.AbstractMap;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;

/**
 * Manages localized text.
 * @author najtmar
 *
 */
public class TextManager {

  /**
   * The Locale used for text provisioning.
   */
  private final Locale locale;

  /**
   * Keeps text for different locale.
   */
  @SuppressWarnings("unchecked")
  private static final Map<Locale, Map<String, String>> LOCALIZED_TEXT_MAP = Collections.unmodifiableMap(
      RefStreams.of(
          new AbstractMap.SimpleEntry<>(new Locale("pl"),
              (Map<String, String>) Collections.unmodifiableMap(RefStreams.of(
                  new AbstractMap.SimpleEntry<>("Game of Prawns", "Krewetki"),

                  new AbstractMap.SimpleEntry<>("Init game", "Zainicjalizuj grę"),
                  new AbstractMap.SimpleEntry<>("Join game", "Dołącz do gry"),
                  new AbstractMap.SimpleEntry<>("Exit", "Wyjdź"),
                  new AbstractMap.SimpleEntry<>("Start", "Start"),
                  new AbstractMap.SimpleEntry<>("Join", "Dołącz"),
                  new AbstractMap.SimpleEntry<>("Cancel", "Anuluj"),
                  new AbstractMap.SimpleEntry<>("Refresh", "Odśwież"),
                  new AbstractMap.SimpleEntry<>("OK", "OK"),

                  new AbstractMap.SimpleEntry<>("Play", "Graj"),
                  new AbstractMap.SimpleEntry<>("Abandon game", "Poddaj grę"),
                  new AbstractMap.SimpleEntry<>("Change itinerary", "Zmień trasę"),
                  new AbstractMap.SimpleEntry<>("Clear itinerary", "Usuń trasę"),
                  new AbstractMap.SimpleEntry<>("Build city", "Zbuduj miasto"),
                  new AbstractMap.SimpleEntry<>("Close", "Zamknij"),
                  new AbstractMap.SimpleEntry<>("Confirm itinerary", "Potwierdź trasę"),
                  new AbstractMap.SimpleEntry<>("Abandon itinerary", "Porzuć trasę"),
                  new AbstractMap.SimpleEntry<>("Create city", "Utwórz miasto"),
                  new AbstractMap.SimpleEntry<>("Produce settlers", "Produkuj osadników"),
                  new AbstractMap.SimpleEntry<>("Produce warrior", "Produkuj wojownika"),
                  new AbstractMap.SimpleEntry<>("Repair prawn", "Napraw krewetkę"),
                  new AbstractMap.SimpleEntry<>("Grow city", "Powiększ miasto"),
                  new AbstractMap.SimpleEntry<>("Confirm warrior", "Potwierdź wojownika"),
                  new AbstractMap.SimpleEntry<>("Abandon warrior", "Porzuć wojownika"),
                  new AbstractMap.SimpleEntry<>("Confirm", "Potwierdź"),

                  new AbstractMap.SimpleEntry<>("BUILD", "BUDUJ"),
                  new AbstractMap.SimpleEntry<>("REPAIR", "NAPRAW"),
                  new AbstractMap.SimpleEntry<>("GROW", "ROŚNIJ"),
                  new AbstractMap.SimpleEntry<>("IDLE", "BRAK"),
                  new AbstractMap.SimpleEntry<>("Productivity", "Produktywność"),
                  new AbstractMap.SimpleEntry<>("Size", "Rozmiar"),
                  new AbstractMap.SimpleEntry<>("Max Size", "Max rozmiar"),
                  new AbstractMap.SimpleEntry<>("Enter city name:", "Wprowadź nazwę miasta:"),
                  new AbstractMap.SimpleEntry<>("NO_GAME", ""),
                  new AbstractMap.SimpleEntry<>("INITIALIZED", "Czekaj ..."),
                  new AbstractMap.SimpleEntry<>("BOARD", "Wybierz element"),
                  new AbstractMap.SimpleEntry<>("MAGNIFIED", "Wybierz element"),
                  new AbstractMap.SimpleEntry<>("PRAWN", "Krewetka"),
                  new AbstractMap.SimpleEntry<>("WARRIOR", "Wojownik"),
                  new AbstractMap.SimpleEntry<>("SETTLERS_UNIT", "Osadnicy"),
                  new AbstractMap.SimpleEntry<>("PRAWN_SELECTION", "Wybierz krewetkę"),
                  new AbstractMap.SimpleEntry<>("ITINERARY_BUILT", "Buduj trasę"),
                  new AbstractMap.SimpleEntry<>("CITY", "Miasto"),
                  new AbstractMap.SimpleEntry<>("CITY_CREATION", "Buduj miasto"),
                  new AbstractMap.SimpleEntry<>("FACTORY", "Ustaw produkcję"),
                  new AbstractMap.SimpleEntry<>("PRAWN_REPAIR", "Wybierz krewetkę do naprawy"),
                  new AbstractMap.SimpleEntry<>("WARRIOR_DIRECTION", "Wybierz kierunek wojownika"),
                  new AbstractMap.SimpleEntry<>("GAME_ABANDONING", "Poddawanie gry"),
                  new AbstractMap.SimpleEntry<>("INVALID", "Błąd!"),
                  new AbstractMap.SimpleEntry<>(
                      InputManager.RelevantEventLabel.GAME_STARTED.name(), "gra rozpoczęta"),
                  new AbstractMap.SimpleEntry<>(
                      InputManager.RelevantEventLabel.GAME_FINISHED.name(), "gra zakończona"),
                  new AbstractMap.SimpleEntry<>(
                      InputManager.RelevantEventLabel.GAME_WON.name(), "wynik: zwyciętwo"),
                  new AbstractMap.SimpleEntry<>(
                      InputManager.RelevantEventLabel.GAME_LOST.name(), "wynik: przegrana"),
                  new AbstractMap.SimpleEntry<>(
                      InputManager.RelevantEventLabel.GAME_ABANDONED.name(), "wynik: walkower"),
                  new AbstractMap.SimpleEntry<>(
                      InputManager.RelevantEventLabel.GAME_DRAWN.name(), "wynik: remis"),
                  new AbstractMap.SimpleEntry<>(
                      InputManager.RelevantEventLabel.OPPONENT_ABANDONED.name(), "jeden z przeciwników się poddał"),
                  new AbstractMap.SimpleEntry<>(
                      InputManager.RelevantEventLabel.OWN_CITY_DESTROYED.name(), "własne miasto zniszczone"),
                  new AbstractMap.SimpleEntry<>(
                      InputManager.RelevantEventLabel.OWN_CITY_CAPTURED.name(), "własne miasto zajęte"),
                  new AbstractMap.SimpleEntry<>(
                      InputManager.RelevantEventLabel.OPPONENTS_CITY_CAPTURED.name(), "zajęcie miasta przeciwnika"),
                  new AbstractMap.SimpleEntry<>(
                      InputManager.RelevantEventLabel.OPPONENTS_CITY_DESTROYED.name(),
                      "zniszczenie miasta przeciwnika"),
                  new AbstractMap.SimpleEntry<>(
                      InputManager.RelevantEventLabel.BATTLE_STARTED.name(), "walka rozpoczęta"),
                  new AbstractMap.SimpleEntry<>(
                      InputManager.RelevantEventLabel.OWN_CITY_GROWN.name(), "własne miasto urosło"),
                  new AbstractMap.SimpleEntry<>(
                      InputManager.RelevantEventLabel.OWN_CITY_SHRINKED.name(), "własne miasto zmniejszyło się"),
                  new AbstractMap.SimpleEntry<>(
                      InputManager.RelevantEventLabel.SETTLERS_UNIT_LOST.name(), "utrata osadników"),
                  new AbstractMap.SimpleEntry<>(
                      InputManager.RelevantEventLabel.WARRIOR_LOST.name(), "utrata wojowników"),
                  new AbstractMap.SimpleEntry<>(
                      InputManager.RelevantEventLabel.OWN_CITY_CREATED.name(), "własne miasto stworzone"),
                  new AbstractMap.SimpleEntry<>(
                      InputManager.RelevantEventLabel.SETTLERS_UNIT_PRODUCED.name(), "produkcja osadników"),
                  new AbstractMap.SimpleEntry<>(
                      InputManager.RelevantEventLabel.WARRIOR_PRODUCED.name(), "produkcja wojowników"),
                  new AbstractMap.SimpleEntry<>(
                      InputManager.RelevantEventLabel.FACTORY_ACTION_FINISHED.name(), "produkcja w fabryce zakończona"),
                  new AbstractMap.SimpleEntry<>(
                      InputManager.RelevantEventLabel.BATTLE_FINISHED.name(), "walka zakończona"),
                  new AbstractMap.SimpleEntry<>(
                      InputManager.RelevantEventLabel.PRAWN_ITINERARY_FINISHED.name(), "krewetka zakończyła trasę"),
                  new AbstractMap.SimpleEntry<>(
                      InputManager.RelevantEventLabel.PERIODIC_INTERRUPT.name(), "cykliczne przerwanie"),
                  new AbstractMap.SimpleEntry<>("Relevant events:", "Istotne wydarzenia:"),
                  new AbstractMap.SimpleEntry<>("NO RELEVANT EVENTS\n(other player's events caused the interruption)",
                      "BRAK ISTOTNYCH WYDARZEŃ\n(wydarzenia istotne dla innego gracza spowodowały przerwanie)"),
                  new AbstractMap.SimpleEntry<>("IDLE FACTORY", "BEZCZYNNOŚĆ"),
                  new AbstractMap.SimpleEntry<>("Waiting for the game to start ...",
                      "Oczekiwanie na rozpoczęcie gry ..."),

                  new AbstractMap.SimpleEntry<>("Game won", "Zwycięstwo"),
                  new AbstractMap.SimpleEntry<>("Game lost", "Porażka"),
                  new AbstractMap.SimpleEntry<>("Game finished with a draw", "Gra zakończona remisem"),

                  new AbstractMap.SimpleEntry<>("Are you sure you want to abandon the game?",
                      "Czy jesteś pewien że chcesz poddać grę?")
                  )
                  .collect(Collectors.toMap((item) -> item.getKey(), item1 -> item1.getValue())))))
      .collect(Collectors.toMap((item2) -> item2.getKey(), item3 -> item3.getValue()
      )));

  /**
   * Text Map for the default Locale.
   */
  @SuppressWarnings("unchecked")
  private static final Map<String, String> DEFAULT_TEXT_MAP = Collections.unmodifiableMap(RefStreams.of(
      new AbstractMap.SimpleEntry<>("Game of Prawns", "Game of Prawns"),

      new AbstractMap.SimpleEntry<>("Init game", "Init game"),
      new AbstractMap.SimpleEntry<>("Join game", "Join game"),
      new AbstractMap.SimpleEntry<>("Exit", "Exit"),
      new AbstractMap.SimpleEntry<>("Start", "Start"),
      new AbstractMap.SimpleEntry<>("Join", "Join"),
      new AbstractMap.SimpleEntry<>("Cancel", "Cancel"),
      new AbstractMap.SimpleEntry<>("Refresh", "Refresh"),
      new AbstractMap.SimpleEntry<>("OK", "OK"),

      new AbstractMap.SimpleEntry<>("Play", "Play"),
      new AbstractMap.SimpleEntry<>("Abandon game", "Abandon game"),
      new AbstractMap.SimpleEntry<>("Change itinerary", "Change itinerary"),
      new AbstractMap.SimpleEntry<>("Clear itinerary", "Clear itinerary"),
      new AbstractMap.SimpleEntry<>("Build city", "Build city"),
      new AbstractMap.SimpleEntry<>("Close", "Close"),
      new AbstractMap.SimpleEntry<>("Confirm itinerary", "Confirm itinerary"),
      new AbstractMap.SimpleEntry<>("Abandon itinerary", "Abandon itinerary"),
      new AbstractMap.SimpleEntry<>("Create city", "Create city"),
      new AbstractMap.SimpleEntry<>("Produce settlers", "Produce settlers"),
      new AbstractMap.SimpleEntry<>("Produce warrior", "Produce warrior"),
      new AbstractMap.SimpleEntry<>("Repair prawn", "Repair prawn"),
      new AbstractMap.SimpleEntry<>("Grow city", "Grow city"),
      new AbstractMap.SimpleEntry<>("Confirm warrior", "Confirm warrior"),
      new AbstractMap.SimpleEntry<>("Abandon warrior", "Abandon warrior"),
      new AbstractMap.SimpleEntry<>("Confirm", "Confirm"),

      new AbstractMap.SimpleEntry<>("BUILD", "BUILD"),
      new AbstractMap.SimpleEntry<>("REPAIR", "REPAIR"),
      new AbstractMap.SimpleEntry<>("GROW", "GROW"),
      new AbstractMap.SimpleEntry<>("IDLE", "IDLE"),
      new AbstractMap.SimpleEntry<>("Productivity", "Productivity"),
      new AbstractMap.SimpleEntry<>("Size", "Size"),
      new AbstractMap.SimpleEntry<>("Max size", "Max size"),
      new AbstractMap.SimpleEntry<>("Enter city name:", "Enter city name:"),
      new AbstractMap.SimpleEntry<>("NO_GAME", ""),
      new AbstractMap.SimpleEntry<>("INITIALIZED", "Wait ..."),
      new AbstractMap.SimpleEntry<>("BOARD", "Select item"),
      new AbstractMap.SimpleEntry<>("MAGNIFIED", "Select item"),
      new AbstractMap.SimpleEntry<>("PRAWN", "Prawn"),
      new AbstractMap.SimpleEntry<>("WARRIOR", "Warrior"),
      new AbstractMap.SimpleEntry<>("SETTLERS_UNIT", "Settlers"),
      new AbstractMap.SimpleEntry<>("PRAWN_SELECTION", "Select prawn"),
      new AbstractMap.SimpleEntry<>("ITINERARY_BUILT", "Building itinerary"),
      new AbstractMap.SimpleEntry<>("CITY", "City"),
      new AbstractMap.SimpleEntry<>("CITY_CREATION", "Create city"),
      new AbstractMap.SimpleEntry<>("FACTORY", "Set factory action"),
      new AbstractMap.SimpleEntry<>("PRAWN_REPAIR", "Select prawn to repair"),
      new AbstractMap.SimpleEntry<>("WARRIOR_DIRECTION", "Select warrior direction"),
      new AbstractMap.SimpleEntry<>("GAME_ABANDONING", "Abandoning game"),
      new AbstractMap.SimpleEntry<>("INVALID", "Invalid!"),
      new AbstractMap.SimpleEntry<>(
          InputManager.RelevantEventLabel.GAME_STARTED.name(), "game started"),
      new AbstractMap.SimpleEntry<>(
          InputManager.RelevantEventLabel.GAME_FINISHED.name(), "game finished"),
      new AbstractMap.SimpleEntry<>(
          InputManager.RelevantEventLabel.GAME_WON.name(), "result: won"),
      new AbstractMap.SimpleEntry<>(
          InputManager.RelevantEventLabel.GAME_LOST.name(), "result: lost"),
      new AbstractMap.SimpleEntry<>(
          InputManager.RelevantEventLabel.GAME_ABANDONED.name(), "result: abandoned (lost)"),
      new AbstractMap.SimpleEntry<>(
          InputManager.RelevantEventLabel.GAME_DRAWN.name(), "result: draw"),
      new AbstractMap.SimpleEntry<>(
          InputManager.RelevantEventLabel.OPPONENT_ABANDONED.name(), "an opponent abandoned the game"),
      new AbstractMap.SimpleEntry<>(
          InputManager.RelevantEventLabel.OWN_CITY_CAPTURED.name(), "own city captured"),
      new AbstractMap.SimpleEntry<>(
          InputManager.RelevantEventLabel.OWN_CITY_DESTROYED.name(), "own city destroyed"),
      new AbstractMap.SimpleEntry<>(
          InputManager.RelevantEventLabel.OPPONENTS_CITY_CAPTURED.name(), "opponent's city captured"),
      new AbstractMap.SimpleEntry<>(
          InputManager.RelevantEventLabel.OPPONENTS_CITY_DESTROYED.name(), "opponent's city destroyed"),
      new AbstractMap.SimpleEntry<>(
          InputManager.RelevantEventLabel.BATTLE_STARTED.name(), "battle started"),
      new AbstractMap.SimpleEntry<>(
          InputManager.RelevantEventLabel.OWN_CITY_GROWN.name(), "own city grown"),
      new AbstractMap.SimpleEntry<>(
          InputManager.RelevantEventLabel.OWN_CITY_SHRINKED.name(), "own city shrunk"),
      new AbstractMap.SimpleEntry<>(
          InputManager.RelevantEventLabel.SETTLERS_UNIT_LOST.name(), "settlers lost"),
      new AbstractMap.SimpleEntry<>(
          InputManager.RelevantEventLabel.WARRIOR_LOST.name(), "warrior lost"),
      new AbstractMap.SimpleEntry<>(
          InputManager.RelevantEventLabel.OWN_CITY_CREATED.name(), "own city created"),
      new AbstractMap.SimpleEntry<>(
          InputManager.RelevantEventLabel.SETTLERS_UNIT_PRODUCED.name(), "settlers produced"),
      new AbstractMap.SimpleEntry<>(
          InputManager.RelevantEventLabel.WARRIOR_PRODUCED.name(), "warrior produced"),
      new AbstractMap.SimpleEntry<>(
          InputManager.RelevantEventLabel.FACTORY_ACTION_FINISHED.name(), "factory production finished"),
      new AbstractMap.SimpleEntry<>(
          InputManager.RelevantEventLabel.BATTLE_FINISHED.name(), "battle finished"),
      new AbstractMap.SimpleEntry<>(
          InputManager.RelevantEventLabel.PRAWN_ITINERARY_FINISHED.name(), "own prawn's itinerary finished"),
      new AbstractMap.SimpleEntry<>(
          InputManager.RelevantEventLabel.PERIODIC_INTERRUPT.name(), "periodic interrupt"),
      new AbstractMap.SimpleEntry<>("Relevant events:", "Relevant events:"),
      new AbstractMap.SimpleEntry<>("NO RELEVANT EVENTS\n(other player's events caused the interruption)",
          "NO RELEVANT EVENTS\n(other player's events caused the interruption)"),
      new AbstractMap.SimpleEntry<>("IDLE FACTORY", "IDLE FACTORY"),
      new AbstractMap.SimpleEntry<>("Waiting for the game to start ...", "Waiting for the game to start ..."),

      new AbstractMap.SimpleEntry<>("Game won", "Game won"),
      new AbstractMap.SimpleEntry<>("Game lost", "Game lost"),
      new AbstractMap.SimpleEntry<>("Game finished with a draw", "Game finished with a draw"),

      new AbstractMap.SimpleEntry<>("Are you sure you want to abandon the game?",
          "Are you sure you want to abandon the game?")
      )
      .collect(Collectors.toMap((item) -> item.getKey(), item -> item.getValue())));

  @Inject
  public TextManager(@Nonnull Locale locale) {
    this.locale = locale;
  }

  /**
   * Returns localized text based on the key.
   * @param key the key based on which the text is returned
   * @return localized text based on the key
   * @throws NoSuchElementException when localized text cannot be inferred from the key
   */
  public String getText(@Nonnull String key) {
    final Map<String, String> textMap;
    if (LOCALIZED_TEXT_MAP.containsKey(locale)) {
      textMap = LOCALIZED_TEXT_MAP.get(locale);
    } else {
      textMap = DEFAULT_TEXT_MAP;
    }
    if (!textMap.containsKey(key)) {
      throw new NoSuchElementException("Key " + key + " not found in Map " + textMap);
    }
    return textMap.get(key);
  }

  /**
   * Assuming that every year has 365 days, converts a timestamp in milliseconds to a Date String .
   * @param pseudoTimestampMillis timestamp in milliseconds since the Epoch, assuming that every year has 365 days
   * @return pseudoTimestampMillis converted to a Date String
   */
  public String pseudoTimestampToDateString(@Nonnegative long pseudoTimestampMillis) {
    final int[] cumulativeMonths = {
        0, // Non-existent month 0.
        31,
        31 + 28,
        31 + 28 + 31,
        31 + 28 + 31 + 30,
        31 + 28 + 31 + 30 + 31,
        31 + 28 + 31 + 30 + 31 + 30,
        31 + 28 + 31 + 30 + 31 + 30 + 31,
        31 + 28 + 31 + 30 + 31 + 30 + 31 + 31,
        31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30,
        31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31,
        31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30,
        31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30 + 31,
    };
    final long year = pseudoTimestampMillis / 365 + 1;
    final long yearDay = pseudoTimestampMillis % 365;
    int month;
    for (month = 1; cumulativeMonths[month] <= yearDay; ++month) {}
    final long dayOfMonth = yearDay - cumulativeMonths[month - 1] + 1;
    final LocalDate date = LocalDate.of((int) year, month, (int) dayOfMonth);
    return date.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG).withLocale(locale))
        .replaceFirst("0(\\d\\d\\d)", "$1").replaceFirst("^0(. )", "$1");
  }

}
