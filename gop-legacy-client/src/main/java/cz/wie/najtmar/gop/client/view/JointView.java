package cz.wie.najtmar.gop.client.view;

import cz.wie.najtmar.gop.common.Point2D;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Client-side representation of class Joint .
 * @author najtmar
 */
public class JointView {

  /**
   * Index of the Joint in the internal list of Joints in {@link Joint#board}.
   */
  private final int index;

  /**
   * Name of the Joint, by default assigned to the City built on the Joint .
   */
  private String name;

  /**
   * (x, y) coordinates of the Joint.
   */
  private final Point2D.Double coordinatesPair;

  /**
   * All RoadSections adjacent to the Joint.
   */
  private final ArrayList<RoadSectionView> roadSections;

  /**
   * Position of the Joint, on the first RoadSection in roadSections. null if no RoadSection added yet.
   */
  private PositionView normalizedPosition;

  /**
   * The City located on the Joint, or null if there is no such City .
   */
  @Nullable private CityView city;

  /**
   * Constructor.
   * @param index the index of the Joint in the Game
   * @param name name of the Joint, by default assigned to the City built on the Joint
   * @param coordinatesPair the coordinated of the Joint on the Board
   */
  public JointView(int index, @Nonnull String name, Point2D.Double coordinatesPair) {
    this.index = index;
    this.name = name;
    this.coordinatesPair = coordinatesPair;
    this.roadSections = new ArrayList<RoadSectionView>();
  }

  public int getIndex() {
    return index;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Point2D.Double getCoordinatesPair() {
    return coordinatesPair;
  }

  public List<RoadSectionView> getRoadSections() {
    return roadSections;
  }

  /**
   * Adds roadSection to the Joint's RoadSections. Updates normalizedPosition if needed.
   * @param roadSection the RoadSection to be added
   * @throws ClientViewException when calculating normalizedPosition fails
   */
  public void addRoadSection(final RoadSectionView roadSection) throws ClientViewException {
    roadSections.add(roadSection);
    if (roadSections.size() == 1) {
      normalizedPosition = roadSection.getPositions()[roadSection.getJointIndex(this)];
    }
  }

  public CityView getCity() {
    return city;
  }

  /**
   * Sets city on the Joint .
   * @param city the City to be set
   * @throws ClientViewException when the Joint already has a City set
   */
  void setCity(CityView city) throws ClientViewException {
    if (this.city != null) {
      throw new ClientViewException("" + this + " already has a City set: " + this.city);
    }
    this.city = city;
  }

  /**
   * Removes city from the Joint.
   * @throws ClientViewException when city is not located on the Joint
   */
  void removeCity(@Nonnull CityView city) throws ClientViewException {
    if (this.city != city) {
      throw new ClientViewException("" + city + " is not located on " + this + ".");
    }
    this.city = null;
  }

  /**
   * Returns the normalized Position of the Joint.
   * @return the normalized Position of the Joint
   */
  public PositionView getNormalizedPosition() {
    return normalizedPosition;
  }

  @Override
  public String toString() {
    return coordinatesPair.toString();
  }

}
