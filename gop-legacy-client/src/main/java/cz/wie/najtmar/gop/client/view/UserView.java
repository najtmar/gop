package cz.wie.najtmar.gop.client.view;

import java.util.UUID;

import javax.annotation.Nonnull;

/**
 * Client-side representation of class User .
 * @author najtmar
 */
public class UserView {

  /**
   * The name of the User .
   */
  private final String name;

  /**
   * Unique id of the User .
   */
  private final UUID id;

  public UserView(@Nonnull String name, @Nonnull UUID id) {
    this.name = name;
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public UUID getId() {
    return id;
  }

  @Override
  public int hashCode() {
    return id.hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof UserView)) {
      return false;
    }
    final UserView otherUser = (UserView) obj;
    return id.equals(otherUser.id) && name.equals(otherUser.name);
  }

  @Override
  public String toString() {
    return "UserView [name=" + name + ", id=" + id + "]";
  }

}
