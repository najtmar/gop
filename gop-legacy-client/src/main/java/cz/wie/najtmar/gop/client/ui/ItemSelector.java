package cz.wie.najtmar.gop.client.ui;

import cz.wie.najtmar.gop.client.view.ClientViewException;
import cz.wie.najtmar.gop.client.view.ClientViewProperties;
import cz.wie.najtmar.gop.client.view.DistinctivePositionView;
import cz.wie.najtmar.gop.client.view.DistinctivePositionView.DistinctionProperty;
import cz.wie.najtmar.gop.client.view.GameView;
import cz.wie.najtmar.gop.client.view.PlayerView;
import cz.wie.najtmar.gop.client.view.PositionSelection;
import cz.wie.najtmar.gop.client.view.PositionView;
import cz.wie.najtmar.gop.client.view.PrawnSelection;
import cz.wie.najtmar.gop.client.view.PrawnView;
import cz.wie.najtmar.gop.client.view.RoadSectionView;
import cz.wie.najtmar.gop.common.Point2D;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;

/**
 * Used to select Positions, Prawns, and other items on the Board of a Game.
 * @author najtmar
 */
public class ItemSelector {

  /**
   * Determines the type of selection performed in the game.
   * @author najtmar
   *
   */
  public enum LookupType {
    /**
     * Only selecting regular Positions (no DistinctivePosition recognition performed).
     */
    NO_DISTINCTIVE_POSITIONS_SELECTION {
      @Override
      Set<DistinctionProperty> getSelectionPropertySet() {
        return new HashSet<DistinctionProperty>();
      }
    },

    /**
     * Selecting an item (a Prawn or a City).
     */
    ITEM_SELECTION {
      @Override
      Set<DistinctionProperty> getSelectionPropertySet() {
        return new HashSet<DistinctionProperty>(Arrays.asList(DistinctionProperty.OWN_CITY,
            DistinctionProperty.OWN_PRAWN));
      }
    },

    /**
     * Selecting a Position (e.g., on an Itinerary).
     */
    POSITION_SELECTION {
      @Override
      Set<DistinctionProperty> getSelectionPropertySet() {
        return new HashSet<DistinctionProperty>(Arrays.asList(DistinctionProperty.OWN_CITY,
            DistinctionProperty.OPPONENTS_CITY, DistinctionProperty.JOINT, DistinctionProperty.MIDPOINT,
            DistinctionProperty.OWN_CITY_ADJACENT, DistinctionProperty.OPPONENTS_CITY_ADJACENT,
            DistinctionProperty.JOINT_ADJACENT, DistinctionProperty.MIDPOINT_ADJACENT, DistinctionProperty.OWN_PRAWN,
            DistinctionProperty.OPPONENTS_PRAWN, DistinctionProperty.OWN_PRAWN_ADJACENT,
            DistinctionProperty.OPPONENTS_PRAWN_ADJACENT));
      }
    },

    /**
     * Used for test only.
     */
    TEST {
      @Override
      Set<DistinctionProperty> getSelectionPropertySet() {
        return new HashSet<DistinctionProperty>(Arrays.asList(DistinctionProperty.JOINT));
      }
    };

    /**
     * For a given LookupType, returns a set of DistinctionProperties that define relevant distinctive Positions.
     * @return a set of DistinctionProperties that define relevant distinctive Positions for a given LookupType
     */
    abstract Set<DistinctionProperty> getSelectionPropertySet();
  }

  /**
   * The Game where the Positions are to be selected.
   */
  // TODO(najtmar): Remove this warning suppression.
  @SuppressWarnings("unused")
  private final GameView game;

  /**
   * The size of a side (either the X-side or the Y-side) of a quadratic cage where Positions are placed.
   */
  private final int cageSideSize;

  /**
   * Number of cages along the X dimension.
   */
  private final int cageXsize;

  /**
   * Number of cages along the Y dimension.
   */
  private final int cageYsize;

  /**
   * Contains Positions that intersect cages of size cageSideSize X cageSideSize.
   */
  private final HashSet<PositionView>[][] positionCages;

  /**
   * The maximum distance from the click Position to another Position for the other Position to be selected.
   */
  private final double clickDistance;

  /**
   * For a distinctive Position (a Joint, a Position adjacent to a Joint, a RoadSection midpoint), the radius of
   * the circle clicking on which would select the Position.
   */
  private final double distinctivePositionRadius;

  /**
   * For the magnified Game view, the exact width of the view.
   */
  private final int magnifiedViewWidth;

  /**
   * For the magnified Game view, the exact height of the view.
   */
  private final int magnifiedViewHeight;

  /**
   * The ratio between the actual length of the same line section in the magnified view and in the main view.
   */
  private final double magnificationRatio;

  /**
   * In the magnified view, the maximum distance from the click Position to another Position for the other Position
   * to be selected.
   */
  private final double magnifiedViewClickDistance;

  /**
   * The width of the PrawnSelection box, represented as the number of Prawn boxes.
   */
  private final int prawnSelectionWidth;

  /**
   * The height of the PrawnSelection box, represented as the number of Prawn boxes.
   */
  private final int prawnSelectionHeight;

  /**
   * The number of pixels of the side of a square representing a Prawn box.
   */
  private final int prawnBoxSide;

  /**
   * The maximum number of CityFactory boxes shown in the City view.
   */
  @SuppressWarnings("unused")
  private final int cityFactoryBoxMaxCount;

  /**
   * The width in pixels of the CityFactory box.
   */
  @SuppressWarnings("unused")
  private final int cityFactoryBoxWidth;

  /**
   * The height in pixels of the CityFactory box.
   */
  @SuppressWarnings("unused")
  private final int cityFactoryBoxHeight;

  /**
   * Constructor.
   * @param game the Game for which Positions will be selected
   * @param clientViewProperties the Properties based on which the object is initialized
   * @throws ClientViewException when the object could not be created
   */
  @SuppressWarnings("unchecked")
  public ItemSelector(@Nonnull GameView game, @Nonnull ClientViewProperties clientViewProperties)
      throws ClientViewException {
    this.game = game;
    this.clickDistance = clientViewProperties.getClickDistance();
    this.cageSideSize = (int) Math.floor(this.clickDistance / 2);
    if (game.getXsize() <= 0 || game.getYsize() <= 0) {
      throw new ClientViewException("Game Board dimensions must be positive (found (" + game.getXsize() + ", "
          + game.getYsize() + ")).");
    }
    this.cageXsize = (int) Math.ceil((double) game.getXsize() / this.cageSideSize);
    this.cageYsize = (int) Math.ceil((double) game.getYsize() / this.cageSideSize);

    this.positionCages = new HashSet[this.cageXsize][this.cageYsize];
    for (int j = 0; j < this.cageYsize; ++j) {
      for (int i = 0; i < this.cageXsize; ++i) {
        this.positionCages[i][j] = new HashSet<>();
      }
    }
    for (RoadSectionView roadSection : game.getRoadSections()) {
      for (PositionView position : roadSection.getPositions()) {
        final int xcoordinate = (int) Math.floor(position.getCoordinatesPair().getX() / cageSideSize);
        final int ycoordinate = (int) Math.floor(position.getCoordinatesPair().getY() / cageSideSize);
        if (xcoordinate < 0 || cageXsize <= xcoordinate || ycoordinate < 0 || cageYsize <= ycoordinate) {
          throw new ClientViewException("Position outside the Board size of " + game.getXsize() + " x "
              + game.getYsize() + " (" + position.getCoordinatesPair() + ").");
        }
        this.positionCages[xcoordinate][ycoordinate].add(position);
      }
    }

    this.distinctivePositionRadius = clientViewProperties.getDistinctivePositionRadius();
    this.magnifiedViewWidth = clientViewProperties.getMagnifiedViewWidth();
    this.magnifiedViewHeight = clientViewProperties.getMagnifiedViewHeight();
    this.magnificationRatio = clientViewProperties.getMagnificationRatio();
    this.magnifiedViewClickDistance = clientViewProperties.getDistinctivePositionRadius() * magnificationRatio;
    this.prawnSelectionWidth = clientViewProperties.getPrawnSelectionWidth();
    this.prawnSelectionHeight = clientViewProperties.getPrawnSelectionHeight();
    this.prawnBoxSide = clientViewProperties.getPrawnBoxSide();
    this.cityFactoryBoxMaxCount = clientViewProperties.getCityFactoryBoxMaxCount();
    this.cityFactoryBoxWidth = clientViewProperties.getCityFactoryBoxWidth();
    this.cityFactoryBoxHeight = clientViewProperties.getCityFactoryBoxHeight();
  }

  double getCageSideSize() {
    return cageSideSize;
  }

  HashSet<PositionView>[][] getPositionCages() {
    return positionCages;
  }

  double getDistinctivePositionRadius() {
    return distinctivePositionRadius;
  }

  /**
   * Given an x-coordinate, returns the index of the cage holding a Position with this coordinate.
   * @param xcoordinate the X-coordinate of a Position
   * @return the x-index of the cage to which the given Position belongs
   */
  int getCageXindex(double xcoordinate) {
    int index = (int) (xcoordinate / cageSideSize);
    if (index < 0) {
      index = 0;
    } else if (cageXsize - 1 <= xcoordinate) {
      return cageXsize - 1;
    }
    return index;
  }

  /**
   * Given a y-coordinate, returns the index of the cage holding a Position with this coordinate.
   * @param ycoordinate the Y-coordinate of a Position
   * @return the y-index of the cage to which the given Position belongs
   */
  int getCageYindex(double ycoordinate) {
    int index = (int) (ycoordinate / cageSideSize);
    if (index < 0) {
      index = 0;
    } else if (cageXsize - 1 <= ycoordinate) {
      return cageXsize - 1;
    }
    return index;
  }

  /**
   * Given a Point on the plane, returns the set of all cages where relevant Positions can be located.
   * @param point the Point for which relevant cages are looked for
   * @return the set of all cages where relevant Positions can be located
   */
  Set<HashSet<PositionView>> getRelevantCages(@Nonnull Point2D.Double point) {
    int minXCoord = (int) Math.floor((point.getX() - clickDistance) / cageSideSize);
    if (minXCoord < 0) {
      minXCoord = 0;
    }
    int maxXCoord = (int) Math.floor((point.getX() + clickDistance) / cageSideSize);
    if (maxXCoord > cageXsize - 1) {
      maxXCoord = cageXsize - 1;
    }
    int minYCoord = (int) Math.floor((point.getY() - clickDistance) / cageSideSize);
    if (minYCoord < 0) {
      minYCoord = 0;
    }
    int maxYCoord = (int) Math.floor((point.getY() + clickDistance) / cageSideSize);
    if (maxYCoord > cageYsize - 1) {
      maxYCoord = cageYsize - 1;
    }
    Set<HashSet<PositionView>> result = new HashSet<HashSet<PositionView>>();
    for (int j = minYCoord; j <= maxYCoord; ++j) {
      for (int i = minXCoord; i <= maxXCoord; ++i) {
        result.add(positionCages[i][j]);
      }
    }
    return result;
  }

  /**
   * Looks up Positions closest to point, based on lookupType, player, and clickDistance.
   * @param point the Point for which Positions are selected
   * @param player the Player for which DistinctivePositions are selected
   * @param lookupType a Set of DistinctionProperties used to look for DistinctivePositions
   * @return ordered Set of Positions closest to point
   */
  public PositionSelection selectPositions(@Nonnull Point2D.Double point, @Nonnull PlayerView player,
      @Nonnull LookupType lookupType) {
    final Set<RoadSectionView> roadSections = new HashSet<>();
    final Set<DistinctivePositionView> distinctivePositions = new HashSet<>();
    final Set<HashSet<PositionView>> relevantCages = getRelevantCages(point);
    for (Set<PositionView> relevantCage : relevantCages) {
      for (PositionView position : relevantCage) {
        if (position.calculateDistance(point) < clickDistance) {
          roadSections.add(position.getRoadSection());
          final DistinctivePositionView distinctivePosition =
              DistinctivePositionView.determinePositionDistinction(position, player,
                  lookupType.getSelectionPropertySet());
          if (distinctivePosition != null) {
            distinctivePositions.add(distinctivePosition);
          }
        }
      }
    }

    final Set<PositionView> closestVisiblePositions = new HashSet<>();
    for (RoadSectionView roadSection : roadSections) {
      final PositionView closestVisiblePosition = roadSection.getClosestVisiblePosition(point);
      if (closestVisiblePosition != null && closestVisiblePosition.calculateDistance(point) < clickDistance) {
        closestVisiblePositions.add(closestVisiblePosition);
      }
    }

    return new PositionSelection(player, point, closestVisiblePositions, distinctivePositions);
  }

  /**
   * If point matches a DistinctivePosition in adjustedPositionSelection, then the matching Position returned.
   * Otherwise, returns the closest Position found or null if no Position matches point .
   * @param point the Point for which the Position is selected
   * @param adjustedPositionSelection an adjusted PositionSelection with which point is matched
   * @return a DistinctivePosition from positionSelection, another matching Position, or null
   * @throws ClientViewException when looking for Position in adjustedPositionSelection fails
   */
  public PositionView selectPosition(@Nonnull Point2D.Double point,
      @Nonnull PositionSelection adjustedPositionSelection) throws ClientViewException {
    if (adjustedPositionSelection.getState() != PositionSelection.State.ADJUSTED) {
      throw new ClientViewException("PositionSelection is not adjusted (" + adjustedPositionSelection + ").");
    }
    // Look for a DistinctivePosition .
    for (DistinctivePositionView distinctivePosition : adjustedPositionSelection.getDistinctivePositions()) {
      final Point2D.Double adjustedPoint = distinctivePosition.getCoordinatesPair();
      if (adjustedPoint.distance(point) < distinctivePositionRadius) {
        return distinctivePosition.getPosition();
      }
    }

    // Look for any Position .
    final PositionSelection positionSelection =
        selectPositions(point, adjustedPositionSelection.getPlayer(), LookupType.NO_DISTINCTIVE_POSITIONS_SELECTION);
    PositionView closestPosition = null;
    double closestDistance = Double.MAX_VALUE;
    for (PositionView position : positionSelection.getAllPositions()) {
      final double distance = position.calculateDistance(point);
      if (closestPosition == null || distance < closestDistance) {
        closestPosition = position;
        closestDistance = distance;
      }
    }

    return closestPosition;
  }

  /**
   * Given a Map between indexes and DistinctivePositions on a RoadSection, adjusts all the DistinctivePositions .
   * @param distinctivePositionsMap the Map of DistinctivePositions to adjust
   * @param roadSection the RoadSection on which the DistinctivePositions are adjusted
   * @param direction iff FORWARD, then the indexes are iterated upward and DistinctivePositions are adjusted forward
   *     on roadSection; iff BACKWARD, then the indexes are iterated downward and DistinctivePositions are adjusted
   *     backward on roadSection
   * @param adjustedDistinctivePositions the input/output array that keeps adjusted DinstinctivePositions
   * @param adjustedDistinctivePositionsCount the current index in array adjustedDistinctivePositions; all
   *     elements with lower indexes already contain adjusted DistinctivePositions; the element with the current index
   *     should be populated next
   * @return the next index in array adjustedDistinctivePositions to be populated
   * @throws ClientViewException when DistinctivePosition adjustment process fails
   */
  private int adjustDistinctivePositionsFromMap(
      @Nonnull NavigableMap<Integer, DistinctivePositionView> distinctivePositionsMap,
      @Nonnull RoadSectionView roadSection,
      @Nonnull RoadSectionView.Direction direction,
      @Nonnull DistinctivePositionView[] adjustedDistinctivePositions,
      @Nonnegative int adjustedDistinctivePositionsCount) throws ClientViewException {
    final double minAllowedDistance = 2 * magnifiedViewClickDistance / magnificationRatio;
    final int delta;
    final Iterator<Integer> keyIterator;
    if (direction == RoadSectionView.Direction.FORWARD) {
      delta = 1;
      keyIterator = distinctivePositionsMap.navigableKeySet().iterator();
    } else {
      delta = -1;
      keyIterator = distinctivePositionsMap.navigableKeySet().descendingIterator();
    }
    while (keyIterator.hasNext()) {
      final int key = keyIterator.next();
      final DistinctivePositionView distinctivePosition = distinctivePositionsMap.get(key);
      if (distinctivePosition.getState() == DistinctivePositionView.State.ADJUSTED) {
        continue;
      }
      Point2D.Double coordinatesPair = null;
      for (int index = key; true; index += delta) {
        coordinatesPair = roadSection.calculateCoordinatesPair(index);
        boolean collisionFound = false;
        for (int i = 0; i < adjustedDistinctivePositionsCount; ++i) {
          final Point2D.Double establishedPoint = adjustedDistinctivePositions[i].getCoordinatesPair();
          final double distanceToEstablishedPoint = establishedPoint.distance(coordinatesPair);
          if (distanceToEstablishedPoint < minAllowedDistance) {
            collisionFound = true;
            break;
          }
        }
        if (!collisionFound) {
          break;
        }
      }
      distinctivePosition.setCoordinatesPair(coordinatesPair);
      adjustedDistinctivePositions[adjustedDistinctivePositionsCount++] = distinctivePosition;
    }
    return adjustedDistinctivePositionsCount;
  }

  /**
   * Compares objects of type DistinctivePositionView .
   */
  private static final Comparator<DistinctivePositionView> ADJUSTMENT_ORDER_COMPARATOR =
      new Comparator<DistinctivePositionView>() {
        @Override
        public int compare(DistinctivePositionView first, DistinctivePositionView second) {
          final int priorityComparison =
              Integer.compare(first.getDistinctionProperties()[0].getPriority(),
                  second.getDistinctionProperties()[0].getPriority());
          if (priorityComparison != 0) {
            return priorityComparison;
          }
          return Integer.compare(first.getPosition().getDistanceToJoint(), second.getPosition().getDistanceToJoint());
        }
      };

  /**
   * Adjusts a CREATED PositionSelection .
   * @param positionSelection the PositionSelection to be adjusted
   * @throws ClientViewException when the adjustment process fails
   */
  public void adjust(@Nonnull PositionSelection positionSelection) throws ClientViewException {
    if (positionSelection.getState() != PositionSelection.State.CREATED) {
      throw new ClientViewException("Only PositionSelections is State CREATED can be adjusted.");
    }

    // Coordinates of the magnification frame.
    final double width = magnifiedViewHeight / magnificationRatio;
    final double height = magnifiedViewWidth / magnificationRatio;
    final double xpos = positionSelection.getSelectionPoint().getX() - width / 2;
    final double ypos = positionSelection.getSelectionPoint().getY() - height / 2;
    positionSelection.setMagnificationFrame(xpos, ypos, width, height);

    // Initialize auxiliary Maps and Sets .
    final DistinctivePositionView[] distinctivePositions = positionSelection.getDistinctivePositions().clone();
    Arrays.sort(distinctivePositions, ADJUSTMENT_ORDER_COMPARATOR);
    final DistinctivePositionView[] adjustedDistinctivePositions =
        new DistinctivePositionView[distinctivePositions.length];
    int adjustedDistinctivePositionsCount = 0;
    final Set<RoadSectionView> roadSectionsToCover = new HashSet<>();
    final Map<RoadSectionView, DistinctivePositionView> roadSectionStarts = new HashMap<>();
    final Map<RoadSectionView, NavigableMap<Integer, DistinctivePositionView>> roadSectionDistinctivePositions =
        new HashMap<>();
    for (DistinctivePositionView distinctivePosition : distinctivePositions) {
      final PositionView position = distinctivePosition.getPosition();
      final Set<RoadSectionView> roadSections = position.getRoadSections();
      roadSectionsToCover.addAll(roadSections);
      for (RoadSectionView roadSection : roadSections) {
        final PositionView projectedPosition = roadSection.getProjectedPosition(position);
        if (!roadSectionDistinctivePositions.containsKey(roadSection)) {
          roadSectionDistinctivePositions.put(roadSection, new TreeMap<>());
        }
        roadSectionDistinctivePositions.get(roadSection).put(projectedPosition.getIndex(), distinctivePosition);
      }
    }

    // Find RoadSection starts.
    for (int i = 0; !roadSectionsToCover.isEmpty(); ++i) {
      final DistinctivePositionView distinctivePosition = distinctivePositions[i];
      final PositionView position = distinctivePosition.getPosition();
      for (RoadSectionView roadSection : position.getRoadSections()) {
        if (roadSectionsToCover.contains(roadSection)) {
          roadSectionsToCover.remove(roadSection);
          roadSectionStarts.put(roadSection, distinctivePosition);
        }
      }
    }

    // Adjust DistinctivePositions .
    final RoadSectionView[] sortedRoadSections = roadSectionStarts.keySet().toArray(new RoadSectionView[]{});
    Arrays.sort(sortedRoadSections,
        (first, second) -> ADJUSTMENT_ORDER_COMPARATOR.compare(
            roadSectionStarts.get(first), roadSectionStarts.get(second)));
    for (RoadSectionView roadSection : sortedRoadSections) {
      final DistinctivePositionView startDistinctivePosition = roadSectionStarts.get(roadSection);
      final PositionView projectedPosition = roadSection.getProjectedPosition(startDistinctivePosition.getPosition());
      final int index = projectedPosition.getIndex();
      if (index <= roadSection.getMidpointIndex()) {
        adjustedDistinctivePositionsCount = adjustDistinctivePositionsFromMap(
            roadSectionDistinctivePositions.get(roadSection).tailMap(index, true), roadSection,
            RoadSectionView.Direction.FORWARD, adjustedDistinctivePositions, adjustedDistinctivePositionsCount);
        adjustedDistinctivePositionsCount = adjustDistinctivePositionsFromMap(
            roadSectionDistinctivePositions.get(roadSection).headMap(index, false), roadSection,
            RoadSectionView.Direction.BACKWARD, adjustedDistinctivePositions, adjustedDistinctivePositionsCount);
      } else {
        adjustedDistinctivePositionsCount = adjustDistinctivePositionsFromMap(
            roadSectionDistinctivePositions.get(roadSection).headMap(index, true), roadSection,
            RoadSectionView.Direction.BACKWARD, adjustedDistinctivePositions, adjustedDistinctivePositionsCount);
        adjustedDistinctivePositionsCount = adjustDistinctivePositionsFromMap(
            roadSectionDistinctivePositions.get(roadSection).tailMap(index, false), roadSection,
            RoadSectionView.Direction.FORWARD, adjustedDistinctivePositions, adjustedDistinctivePositionsCount);
      }
    }
    positionSelection.markAdjusted();
  }

  /**
   * Given a Set of Prawns located on the same Position, creates a PrawnSelection with these Prawns, to be later handled
   * and visualized.
   * @param prawns the Prawns to be added to the PrawnSelection created
   * @param player the Player to whom prawns belong
   * @return a PrawnSelection with prawns
   * @throws ClientViewException when prawns don't form a valid PrawnSelection
   */
  public PrawnSelection createPrawnSelection(@Nonnull Set<PrawnView> prawns, @Nonnull PlayerView player)
      throws ClientViewException {
    PositionView position = null;
    for (PrawnView prawn : prawns) {
      if (position != null) {
        if (!position.equals(prawn.getCurrentPosition())) {
          throw new ClientViewException("Prawns located on different Positions: " + prawns);
        }
      } else {
        position = prawn.getCurrentPosition();
      }
      if (!prawn.getPlayer().equals(player)) {
        throw new ClientViewException("" + prawn + " not owned by " + player + " .");
      }
    }
    return new PrawnSelection(prawns, prawnSelectionWidth, prawnSelectionHeight, prawnBoxSide);
  }

  /**
   * If point matches a Prawn in prawnSelection, then the Prawn returned, otherwise null is returned. If points matches
   * a control (left or right slider), then the corresponding action is performed.
   * @param point the Point for which the Position is selected
   * @param prawnSelection a PrawnSelection from which Prawns or actions are selected
   * @return a Prawn from prawnSelection, or null
   * @throws ClientViewException when selecting a Prawn or invoking an action fails
   */
  public PrawnView selectPrawnOrProcessPrawnSelection(@Nonnull Point2D.Double point,
      @Nonnull PrawnSelection prawnSelection) throws ClientViewException {
    final int indexX = (int) (point.getX() / prawnBoxSide);
    final int indexY = (int) (point.getY() / prawnBoxSide);
    if (indexX < 0 || prawnSelectionWidth <= indexX || indexY < 0 || prawnSelectionHeight < indexY) {
      throw new ClientViewException("" + point + " doesn't fit in the PrawnSelection box (" + prawnSelectionWidth + ", "
          + prawnSelectionHeight + ") x " + prawnBoxSide);
    }
    final PrawnSelection.PrawnBox prawnBox = prawnSelection.getPrawnBoxes()[indexX][indexY];
    switch (prawnBox.getType()) {
      case PRAWN: {
        return prawnBox.getPrawn();
      }
      case LEFT_SLIDER: {
        prawnSelection.tryDecreaseStartPosition();
        break;
      }
      case RIGHT_SLIDER: {
        prawnSelection.tryIncreaseStartPosition();
        break;
      }
      case EMPTY: {
        // Do nothing.
        break;
      }
      default: {
        throw new ClientViewException("Unknown PrawnBoxType: " + prawnBox.getType());
      }
    }
    return null;
  }

}
