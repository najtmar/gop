package cz.wie.najtmar.gop.client.view;

import cz.wie.najtmar.gop.common.Point;

import java8.util.stream.StreamSupport;

import java.util.Set;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;

/**
 * The selection of Prawns located on the same Position . Required for visualization.
 * @author najtmar
 */
public class PrawnSelection {

  /**
   * Type of a PrawnBox.
   * @author najtmar
   */
  public enum PrawnBoxType {
    /**
     * The PrawnBox is empty.
     */
    EMPTY,

    /**
     * The PrawnBox contains a Prawn .
     */
    PRAWN,

    /**
     * The PrawnBox contains a left slider.
     */
    LEFT_SLIDER,

    /**
     * The PrawnBox contains a right slider.
     */
    RIGHT_SLIDER,
  }

  /**
   * Represents a box in which a Prawn, or a control, is located.
   * @author najtmar
   */
  public class PrawnBox {

    /**
     * The type of the PrawnBox .
     */
    private PrawnBoxType type;

    /**
     * For a PrawnBox of type PRAWN, contains a Prawn .
     */
    private PrawnView prawn;

    /**
     * In the two-dimensional PrawnSelection box, the index on the X-axis of the PrawnBox .
     */
    @SuppressWarnings("unused")
    private final int indexX;

    /**
     * In the two-dimensional PrawnSelection box, the index on the Y-axis of the PrawnBox .
     */
    @SuppressWarnings("unused")
    private final int indexY;

    /**
     * Min X coordinate of the PrawnBox. 0 for a PrawnBox with indexX equal to 0 .
     */
    private final int coordX;

    /**
     * Min Y coordinate of the PrawnBox. 0 for a PrawnBox with indexY equal to 0 .
     */
    private final int coordY;

    /**
     * Constructor.
     * @param indexX the index on the X-axis of the PrawnBox
     * @param indexY the index on the Y-axis of the PrawnBox
     * @param prawnBoxSide the number of pixels of the side of a square representing a PrawnBox
     */
    public PrawnBox(@Nonnegative int indexX, @Nonnegative int indexY, @Nonnegative int prawnBoxSide) {
      this.indexX = indexX;
      this.indexY = indexY;
      this.coordX = indexX * prawnBoxSide;
      this.coordY = indexY * prawnBoxSide;
      this.type = PrawnBoxType.EMPTY;
    }

    /**
     * Getter for field Prawn .
     * @return the Prawn
     * @throws ClientViewException when PrawnBoxType is not PRAWN
     */
    public PrawnView getPrawn() throws ClientViewException {
      if (type != PrawnBoxType.PRAWN) {
        throw new ClientViewException("Method getPrawn() can only be invoked for PrawnBoxes of Type PRAWN (found "
            + type + ").");
      }
      return prawn;
    }

    public void setPrawn(PrawnView prawn) {
      this.prawn = prawn;
      this.type = PrawnBoxType.PRAWN;
    }

    /**
     * Sets the PrawnBoxType to EMPTY .
     */
    public void setEmpty() {
      this.prawn = null;
      this.type = PrawnBoxType.EMPTY;
    }

    /**
     * Sets the PrawnBoxType to LEFT_SLIDER .
     */
    public void setLeftSlider() {
      this.prawn = null;
      this.type = PrawnBoxType.LEFT_SLIDER;
    }

    /**
     * Sets the PrawnBoxType to RIGHT_SLIDER .
     */
    public void setRightSlider() {
      this.prawn = null;
      this.type = PrawnBoxType.RIGHT_SLIDER;
    }

    public PrawnBoxType getType() {
      return type;
    }

    public int getCoordX() {
      return coordX;
    }

    public int getCoordY() {
      return coordY;
    }

  }

  /**
   * The array of all Prawns, ordered by their ids.
   */
  private final PrawnView[] prawns;

  /**
   * PrawnBoxes in which Prawns or controls are located.
   */
  private final PrawnBox[][] prawnBoxes;

  /**
   * The width of the PrawnSelection box, represented as the number of Prawn boxes.
   */
  private final int prawnSelectionWidth;

  /**
   * The height of the PrawnSelection box, represented as the number of Prawn boxes.
   */
  private final int prawnSelectionHeight;

  /**
   * The number of pixels of the side of a square representing a PrawnBox .
   */
  private final int prawnBoxSide;

  /**
   * The index of the first Prawn in the array that is shown.
   */
  private int startPosition;

  /**
   * Number of prawns not shown, with indexes larger than startPosition .
   */
  private int nextCount;

  /**
   * Constructor.
   * @param prawns the set of Prawns to be selected
   * @param prawnSelectionWidth the width of the PrawnSelection box, represented as the number of Prawn boxes
   * @param prawnSelectionHeight the height of the PrawnSelection box, represented as the number of Prawn boxes
   * @param prawnBoxSide the number of pixels of the side of a square representing a Prawn box
   */
  public PrawnSelection(@Nonnull Set<PrawnView> prawns, @Nonnegative int prawnSelectionWidth,
      @Nonnegative int prawnSelectionHeight, @Nonnegative int prawnBoxSide) {
    this.prawns = StreamSupport.stream(prawns).sorted((first, second) -> Integer.compare(first.getId(), second.getId()))
        .toArray(size -> new PrawnView[size]);
    this.prawnSelectionWidth = prawnSelectionWidth;
    this.prawnSelectionHeight = prawnSelectionHeight;
    this.prawnBoxSide = prawnBoxSide;
    this.prawnBoxes = new PrawnBox[prawnSelectionWidth][prawnSelectionHeight];
    for (int i = 0; i < prawnSelectionWidth; ++i) {
      for (int j = 0; j < prawnSelectionHeight; ++j) {
        this.prawnBoxes[i][j] = new PrawnBox(i, j, prawnBoxSide);
      }
    }
    this.startPosition = 0;
    this.nextCount = calculateNextCount(startPosition);
    setPrawnBoxes();
  }

  /**
   * Given startPosition, returns the calculated number for nextCount .
   * @param startPosition the index of the first Prawn in the array that is shown
   * @return the calculated number for previousCount
   */
  private int calculateNextCount(@Nonnegative int startPosition) {
    if (startPosition == 0) {
      if (prawns.length <= prawnSelectionWidth * prawnSelectionHeight) {
        return 0;
      } else {
        return prawns.length - (prawnSelectionWidth * prawnSelectionHeight - 1);
      }
    } else {
      if (prawns.length <= startPosition + prawnSelectionWidth * prawnSelectionHeight - 1) {
        return 0;
      } else {
        return prawns.length - (startPosition + prawnSelectionWidth * prawnSelectionHeight - 2);
      }
    }
  }

  /**
   * Assigns Prawns and controls to PrawnBoxes, based on the values of startPosition and nextCount .
   */
  private void setPrawnBoxes() {
    int pos = startPosition;
    for (int j = 0; j < prawnSelectionHeight; ++j) {
      for (int i = 0; i < prawnSelectionWidth; ++i) {
        if (prawns.length <= pos) {
          prawnBoxes[i][j].setEmpty();
        } else if (i == 0 && j == 0 && startPosition > 0) {
          prawnBoxes[i][j].setLeftSlider();
        } else if (i == prawnSelectionWidth - 1 && j == prawnSelectionHeight - 1 && nextCount > 0) {
          prawnBoxes[i][j].setRightSlider();
        } else {
          prawnBoxes[i][j].setPrawn(prawns[pos++]);
        }
      }
    }
  }

  /**
   * Increases startPosition, or does nothing if the increased startPosition would reach the number of prawns .
   * @returns true off startPosition has been increased
   */
  public boolean tryIncreaseStartPosition() {
    int extraBoxes = 0;
    if (startPosition > 0) {
      ++extraBoxes;
    }
    if (nextCount > 0) {
      ++extraBoxes;
    }
    if (extraBoxes == 0) {
      return false;
    }
    final int newStartPosition = startPosition + prawnSelectionWidth * prawnSelectionHeight - extraBoxes;
    if (newStartPosition < prawns.length) {
      startPosition = newStartPosition;
      nextCount = calculateNextCount(newStartPosition);
      setPrawnBoxes();
      return true;
    }
    return false;
  }

  /**
   * Decreases startPosition, or does nothing if the decreased startPosition would go below 0.
   * @returns true off startPosition has been decreased
   */
  public boolean tryDecreaseStartPosition() {
    if (startPosition == 0) {
      return false;
    }
    startPosition -= prawnSelectionWidth * prawnSelectionHeight - 1;
    if (startPosition > 0) {
      startPosition++;  // Extra box for left slider.
    } else if (startPosition < 0) {  // Should never happen.
      startPosition = 0;
    }
    nextCount = calculateNextCount(startPosition);
    setPrawnBoxes();
    return true;
  }

  PrawnView[] getPrawns() {
    return prawns;
  }

  public PrawnBox[][] getPrawnBoxes() {
    return prawnBoxes;
  }

  public int getPrawnSelectionWidth() {
    return prawnSelectionWidth;
  }

  public int getPrawnSelectionHeight() {
    return prawnSelectionHeight;
  }

  public int getPrawnBoxSide() {
    return prawnBoxSide;
  }

  public int getStartPosition() {
    return startPosition;
  }

  public int getNextCount() {
    return nextCount;
  }

  /**
   * Returns the (x, y) indexes of the box containing a given prawn .
   * @param prawn the Prawn to be l;ocated
   * @return the (x, y) indexes of the box containing a given prawn
   * @throws ClientViewException when prawn is not present in the PrawnSelection
   */
  public Point getPrawnBoxIndexes(@Nonnull PrawnView prawn) throws ClientViewException {
    for (int j = 0; j < prawnSelectionHeight; ++j) {
      for (int i = 0; i < prawnSelectionWidth; ++i) {
        if (prawnBoxes[i][j].getPrawn().equals(prawn)) {
          return new Point(i, j);
        }
      }
    }
    throw new ClientViewException("" + prawn + " not present in " + this + " .");
  }

}
