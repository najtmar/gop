package cz.wie.najtmar.gop.client.view;

import cz.wie.najtmar.gop.common.Point2D;
import java8.util.stream.StreamSupport;

import java.util.Arrays;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;

/**
 * Client-side representation of class Battle .
 * @author najtmar
 */
public class BattleView {

  public enum State {
    /**
     * The Battle is going on.
     */
    EXISTING,

    /**
     * The Battle has finished.
     */
    FINISHED,
  }

  /**
   * Returned by method hashCode().
   */
  private final int hashCode;

  /**
   * State of the Prawn .
   */
  private State state;

  /**
   * Prawns participating in the Battle . Ordered by id.
   */
  private final PrawnView[] participants;

  /**
   * The Point between the two Positions where the Battle takes place.
   */
  private final Point2D.Double point;

  /**
   * Normalized Positions where participants are located.
   */
  private final PositionView[] normalizedPositions;

  /**
   * The time when the Battle started.
   */
  private final long startTime;

  /**
   * The time when the Battle finished.  Long.MAX_VALUE when not finished yet.
   */
  private long endTime;

  /**
   * Constructor.
   * @param participant0 the first participant of the Battle
   * @param participant1 the second participant of the Battle
   * @param startTime the time when the Battle started
   * @throws ClientViewException when the Battle cannot be created
   */
  public BattleView(@Nonnull PrawnView participant0, @Nonnull PrawnView participant1, @Nonnegative long startTime)
      throws ClientViewException {
    if (participant0.getPlayer().equals(participant1.getPlayer())) {
      throw new ClientViewException("A Battle must be between two different Players: " + participant0 + ", "
          + participant1);
    }
    if (participant0.getId() < participant1.getId()) {
      this.participants = new PrawnView[]{participant0, participant1};
    } else {
      this.participants = new PrawnView[]{participant1, participant0};
    }
    this.normalizedPositions = StreamSupport.stream(Arrays.asList(participants))
        .map(participant -> {
          final PositionView position = participant.getCurrentPosition();
          if (position.getJoint() == null) {
            return position;
          } else {
            return position.getJoint().getNormalizedPosition();
          }
        }).toArray(unused -> new PositionView[2]);
    if (!normalizedPositions[0].getAdjacentPositions().contains(normalizedPositions[1])) {
      throw new ClientViewException(Arrays.toString(normalizedPositions) + " must be adjacent.");
    }
    final Point2D.Double point0 = normalizedPositions[0].getCoordinatesPair();
    final Point2D.Double point1 = normalizedPositions[1].getCoordinatesPair();
    this.point = new Point2D.Double((point0.getX() + point1.getX()) / 2, (point0.getY() + point1.getY()) / 2);
    this.startTime = startTime;
    this.endTime = Long.MAX_VALUE;
    this.state = State.EXISTING;

    this.hashCode = calculateHashCode(participants[0], participants[1], normalizedPositions[0], normalizedPositions[1]);

    // Must be executed at the end, when hashCode is calculated.
    for (PrawnView participant : participants) {
      if (participant.getState() == PrawnView.State.REMOVED) {
        throw new ClientViewException("" + participant + " is removed.");
      }
      if (participant.getBattles().contains(this)) {
        throw new ClientViewException("" + participant + " already involved in " + this);
      }
      participant.getBattles().add(this);
    }
    for (PositionView position : normalizedPositions) {
      if (position.getBattles().contains(this)) {
        throw new ClientViewException("" + position + " already involved in " + this);
      }
      position.getBattles().add(this);
    }
  }

  public State getState() {
    return state;
  }

  /**
   * Finishes the Battle .
   * @param endTime the time when the Battle ended
   * @throws ClientViewException when the Battle cannot be finished.
   */
  void finish(@Nonnegative long endTime) throws ClientViewException {
    if (this.state == State.FINISHED) {
      throw new ClientViewException("Battle already FINISHED .");
    }
    if (endTime < startTime) {
      throw new ClientViewException("Battle must not finish earlier than it started (" + endTime + " < " + startTime
          + ").");
    }
    this.endTime = endTime;
    for (PrawnView participant : participants) {
      participant.getBattles().remove(this);
    }
    for (PositionView position : normalizedPositions) {
      position.getBattles().remove(this);
    }
    this.state = State.FINISHED;
  }

  public PrawnView[] getParticipants() {
    return participants;
  }

  public Point2D.Double getPoint() {
    return point;
  }

  public PositionView[] getNormalizedPositions() {
    return normalizedPositions;
  }

  public long getStartTime() {
    return startTime;
  }

  public long getEndTime() {
    return endTime;
  }

  /**
   * Checks whether the Battle matches the arguments.
   * @param participant0 the first participant in the Battle
   * @param participant1 the second participant in the Battle
   * @param normalizedPosition0 the normalized Position of participant0
   * @param normalizedPosition1 the normalized Position of participant1
   * @return true iff the Battle matches the arguments
   */
  public boolean matches(@Nonnull PrawnView participant0, @Nonnull PrawnView participant1,
      @Nonnull PositionView normalizedPosition0, @Nonnull PositionView normalizedPosition1) {
    return participants[0].equals(participant0)
        && participants[1].equals(participant1)
        && normalizedPositions[0].equals(normalizedPosition0)
        && normalizedPositions[1].equals(normalizedPosition1);
  }

  /**
   * Calculates the hashCode for a Battle, based on its distinctive features. The order of the arguments does matter.
   * @param participant0 the first participant in the Battle
   * @param participant1 the second participant in the Battle
   * @param normalizedPosition0 the normalized Position of participant0
   * @param normalizedPosition1 the normalized Position of participant1
   * @return the hashCode calculated for a Battle with the features specified in the arguments
   */
  public static int calculateHashCode(@Nonnull PrawnView participant0, @Nonnull PrawnView participant1,
      @Nonnull PositionView normalizedPosition0, @Nonnull PositionView normalizedPosition1) {
    return 31 * (31 * (31 * (31 * (31
                    + participant0.hashCode())
                + participant1.hashCode())
            + normalizedPosition0.hashCode())
        + normalizedPosition1.hashCode());
  }

  @Override
  public int hashCode() {
    return hashCode;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof BattleView)) {
      return false;
    }
    final BattleView otherBattle = (BattleView) obj;
    return participants[0].equals(otherBattle.participants[0])
        && participants[1].equals(otherBattle.participants[1])
        && point.equals(otherBattle.point)
        && normalizedPositions[0].equals(otherBattle.normalizedPositions[0])
        && normalizedPositions[1].equals(otherBattle.normalizedPositions[1])
        && startTime == otherBattle.startTime;
  }

  @Override
  public String toString() {
    return "BattleView [hashCode=" + hashCode + ", state=" + state + ", participants="
        + Arrays.toString(participants) + ", point=" + point + ", positions="
        + Arrays.toString(normalizedPositions) + ", startTime=" + startTime + ", endTime=" + endTime + "]";
  }

}
