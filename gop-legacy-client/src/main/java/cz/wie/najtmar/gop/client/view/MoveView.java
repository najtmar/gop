package cz.wie.najtmar.gop.client.view;

import javax.annotation.Nonnull;

/**
 * Client-side representation of class Move.
 * @author najtmar
 */
public class MoveView {

  /**
   * The new Position of the Prawn.
   */
  private final PositionView position;

  /**
   * The time when the Move is performed.
   */
  private final long moveTime;

  /**
   * Determines whether the Move finishes the Itinerary or not.
   */
  private final boolean stop;

  /**
   * Constructor.
   * @param position the new Position of the Prawn
   * @param moveTime the time when the Move is performed
   * @param stop whether the Move finishes the Itinerary or not
   */
  public MoveView(@Nonnull PositionView position, long moveTime, boolean stop) {
    this.position = position;
    this.moveTime = moveTime;
    this.stop = stop;
  }

  public PositionView getPosition() {
    return position;
  }

  public long getMoveTime() {
    return moveTime;
  }

  public boolean isStop() {
    return stop;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof MoveView)) {
      return false;
    }
    final MoveView otherMove = (MoveView) obj;
    return position.equals(otherMove.position) && moveTime == otherMove.moveTime && stop == otherMove.stop;
  }

  @Override
  public String toString() {
    return "MoveView [position=" + position + ", moveTime=" + moveTime + ", stop=" + stop + "]";
  }

}
