package cz.wie.najtmar.gop.client.view;

import java.util.Properties;

import javax.annotation.Nonnull;

/**
 * Class that encapsulates and provides access to all Properties of a Game view.
 * @author najtmar
 */
public class ClientViewProperties {

  // Literals denoting Properties names.
  public static final String CLICK_DISTANCE_PROPERTY_NAME = "ItemSelector.clickDistance";
  public static final String DISTINCTIVE_POSITION_RADIUS_PROPERTY_NAME = "ItemSelector.distinctivePositionRadius";
  public static final String MAIN_VIEW_WIDTH_PROPERTY_NAME = "GameView.mainViewWidth";
  public static final String MAIN_VIEW_HEIGHT_PROPERTY_NAME = "GameView.mainViewHeight";
  public static final String MAGNIFIED_VIEW_WIDTH_PROPERTY_NAME = "ItemSelector.magnifiedViewWidth";
  public static final String MAGNIFIED_VIEW_HEIGHT_PROPERTY_NAME = "ItemSelector.magnifiedViewHeight";
  public static final String MAGNIFICATION_RATIO_PROPERTY_NAME = "GameView.magnificationRatio";
  public static final String PRAWN_SELECTION_WIDTH_PROPERTY_NAME = "PrawnSelection.prawnSelectionWidth";
  public static final String PRAWN_SELECTION_HEIGHT_PROPERTY_NAME = "PrawnSelection.prawnSelectionHeight";
  public static final String PRAWN_BOX_SIDE_PROPERTY_NAME = "PrawnSelection.prawnBoxSide";
  public static final String CITY_FACTORY_BOX_MAX_COUNT = "ItemSelector.cityFactoryBoxMaxCount";
  public static final String CITY_FACTORY_BOX_WIDTH = "ItemSelector.cityFactoryBoxWidth";
  public static final String CITY_FACTORY_BOX_HEIGHT = "ItemSelector.cityFactoryBoxHeight";
  public static final String CITY_PRAWN_SELECTION_WIDTH_PROPERTY_NAME = "PrawnSelection.cityPrawnSelectionWidth";
  public static final String CITY_PRAWN_SELECTION_HEIGHT_PROPERTY_NAME = "PrawnSelection.cityPrawnSelectionHeight";
  public static final String CITY_PRAWN_BOX_SIDE_PROPERTY_NAME = "PrawnSelection.cityPrawnBoxSide";
  public static final String WARRIOR_DIRECTION_SELECTION_BOX_SIDE_PROPERTY_NAME =
      "InputEngine.warriorDirectionSelectionBoxSide";
  public static final String PROMPT_BOX_WIDTH = "OutputEngine.promptBoxWidth";
  public static final String PROMPT_BOX_HEIGHT = "OutputEngine.promptBoxHeight";
  public static final String CITY_BOX_SIDE_PROPERTY_NAME = "OutputEngine.cityBoxSide";
  public static final String CITY_CREATION_BOX_WIDTH_PROPERTY_NAME = "InputEngine.cityCreationBoxWidth";
  public static final String CITY_CREATION_BOX_HEIGHT_PROPERTY_NAME = "InputEngine.cityCreationBoxHeight";
  public static final String PRAWN_REPAIR_PROMPT_WIDTH_PROPERTY_NAME = "OutputEngine.prawnRepairPromptWidth";
  public static final String PRAWN_REPAIR_PROMPT_HEIGHT_PROPERTY_NAME = "OutputEngine.prawnRepairPromptHeight";

  /**
   * Internal representation of Client view Properties.
   */
  private final Properties clientViewPropertiesSet;

  /**
   * In the main view, the maximum distance from the click Position to another Position for the other Position to be
   * selected.
   */
  private final double clickDistance;

  /**
   * For a distinctive Position (a Joint, a Position adjacent to a Joint, a RoadSection midpoint), the radius of
   * the circle clicking on which would select the Position.
   */
  private final double distinctivePositionRadius;

  /**
   * For the main Game view, the preferred width of the view.
   */
  private final int mainViewWidth;

  /**
   * For the main Game view, the preferred height of the view.
   */
  private final int mainViewHeight;

  /**
   * For the magnified Game view, the exact width of the view.
   */
  private final int magnifiedViewWidth;

  /**
   * For the magnified Game view, the exact height of the view.
   */
  private final int magnifiedViewHeight;

  /**
   * The ratio between the actual length of the same line section in the magnified view and in the main view.
   */
  private final double magnificationRatio;

  /**
   * The width of the PrawnSelection box, represented as the number of Prawn boxes.
   */
  private final int prawnSelectionWidth;

  /**
   * The height of the PrawnSelection box, represented as the number of Prawn boxes.
   */
  private final int prawnSelectionHeight;

  /**
   * The number of pixels of the side of a square representing a Prawn box.
   */
  private final int prawnBoxSide;

  /**
   * The maximum number of CityFactory boxes shown in the City view.
   */
  private final int cityFactoryBoxMaxCount;

  /**
   * The width in pixels of the CityFactory box.
   */
  private final int cityFactoryBoxWidth;

  /**
   * The height in pixels of the CityFactory box.
   */
  private final int cityFactoryBoxHeight;

  /**
   * The width of the PrawnSelection box, represented as the number of Prawn boxes in the City view.
   */
  private final int cityPrawnSelectionWidth;

  /**
   * The height of the PrawnSelection box, represented as the number of Prawn boxes in the City view.
   */
  private final int cityPrawnSelectionHeight;

  /**
   * The number of pixels of the side of a square representing a Prawn box in the City view.
   */
  private final int cityPrawnBoxSide;

  /**
   * The side of a square box to select the direction of a Warrior to be built.
   */
  private final int warriorDirectionSelectionBoxSide;

  /**
   * The width of a rectangular box containing a prompt.
   */
  private final int promptBoxWidth;

  /**
   * The height of a rectangular box containing a prompt.
   */
  private final int promptBoxHeight;

  /**
   * The side of the box in which a City is visualized in the City view.
   */
  private final int cityBoxSide;

  /**
   * The width of the box used to set up City creation.
   */
  private final int cityCreationBoxWidth;

  /**
   * The height of the box used to set up City creation.
   */
  private final int cityCreationBoxHeight;

  /**
   * The width of the box containing the Prawn repair prompt.
   */
  private final int prawnRepairPromptWidth;

  /**
   * The height of the box containing the Prawn repair prompt.
   */
  private final int prawnRepairPromptHeight;

  /**
   * Constructor.
   * @param clientViewPropertiesSet Properties set to initialize the object from
   * @throws ClientViewException when ClientViewProperties cannot be created
   */
  public ClientViewProperties(@Nonnull Properties clientViewPropertiesSet) throws ClientViewException {
    this.clientViewPropertiesSet = clientViewPropertiesSet;
    this.clickDistance = getDoubleProperty(CLICK_DISTANCE_PROPERTY_NAME);
    this.distinctivePositionRadius = getDoubleProperty(DISTINCTIVE_POSITION_RADIUS_PROPERTY_NAME);
    this.mainViewWidth = getIntegerProperty(MAIN_VIEW_WIDTH_PROPERTY_NAME);
    this.mainViewHeight = getIntegerProperty(MAIN_VIEW_HEIGHT_PROPERTY_NAME);
    this.magnifiedViewWidth = getIntegerProperty(MAGNIFIED_VIEW_WIDTH_PROPERTY_NAME);
    this.magnifiedViewHeight = getIntegerProperty(MAGNIFIED_VIEW_HEIGHT_PROPERTY_NAME);
    this.magnificationRatio = getDoubleProperty(MAGNIFICATION_RATIO_PROPERTY_NAME);
    this.prawnSelectionWidth = getIntegerProperty(PRAWN_SELECTION_WIDTH_PROPERTY_NAME);
    this.prawnSelectionHeight = getIntegerProperty(PRAWN_SELECTION_HEIGHT_PROPERTY_NAME);
    this.prawnBoxSide = getIntegerProperty(PRAWN_BOX_SIDE_PROPERTY_NAME);
    this.cityFactoryBoxMaxCount = getIntegerProperty(CITY_FACTORY_BOX_MAX_COUNT);
    this.cityFactoryBoxWidth = getIntegerProperty(CITY_FACTORY_BOX_WIDTH);
    this.cityFactoryBoxHeight = getIntegerProperty(CITY_FACTORY_BOX_HEIGHT);
    this.cityPrawnSelectionWidth = getIntegerProperty(CITY_PRAWN_SELECTION_WIDTH_PROPERTY_NAME);
    this.cityPrawnSelectionHeight = getIntegerProperty(CITY_PRAWN_SELECTION_HEIGHT_PROPERTY_NAME);
    this.cityPrawnBoxSide = getIntegerProperty(CITY_PRAWN_BOX_SIDE_PROPERTY_NAME);
    this.warriorDirectionSelectionBoxSide = getIntegerProperty(WARRIOR_DIRECTION_SELECTION_BOX_SIDE_PROPERTY_NAME);
    this.promptBoxWidth = getIntegerProperty(PROMPT_BOX_WIDTH);
    this.promptBoxHeight = getIntegerProperty(PROMPT_BOX_HEIGHT);
    this.cityBoxSide = getIntegerProperty(CITY_BOX_SIDE_PROPERTY_NAME);
    this.cityCreationBoxWidth = getIntegerProperty(CITY_CREATION_BOX_WIDTH_PROPERTY_NAME);
    this.cityCreationBoxHeight = getIntegerProperty(CITY_CREATION_BOX_HEIGHT_PROPERTY_NAME);
    this.prawnRepairPromptWidth = getIntegerProperty(PRAWN_REPAIR_PROMPT_WIDTH_PROPERTY_NAME);
    this.prawnRepairPromptHeight = getIntegerProperty(PRAWN_REPAIR_PROMPT_HEIGHT_PROPERTY_NAME);
  }

  /**
   * Parses and returns a double property.
   * @param key property
   * @return parsed double property
   * @throws ClientViewException if property reading or parsing fails
   */
  private double getDoubleProperty(String key) throws ClientViewException {
    try {
      return Double.parseDouble(clientViewPropertiesSet.getProperty(key));
    } catch (NumberFormatException | NullPointerException exception) {
      throw new ClientViewException("Double property " + key + " could not be read.", exception);
    }
  }

  /**
   * Parses and returns an integer property.
   * @param key property
   * @return parsed integer property
   * @throws ClientViewException if property reading or parsing fails
   */
  private int getIntegerProperty(String key) throws ClientViewException {
    try {
      return Integer.parseInt(clientViewPropertiesSet.getProperty(key));
    } catch (NumberFormatException | NullPointerException exception) {
      throw new ClientViewException("Integer property " + key + " could not be read.", exception);
    }
  }

  public Properties getClientViewPropertiesSet() {
    return clientViewPropertiesSet;
  }

  public double getClickDistance() {
    return clickDistance;
  }

  public double getDistinctivePositionRadius() {
    return distinctivePositionRadius;
  }

  public int getMainViewWidth() {
    return mainViewWidth;
  }

  public int getMainViewHeight() {
    return mainViewHeight;
  }

  public int getMagnifiedViewWidth() {
    return magnifiedViewWidth;
  }

  public int getMagnifiedViewHeight() {
    return magnifiedViewHeight;
  }

  public double getMagnificationRatio() {
    return magnificationRatio;
  }

  public int getPrawnSelectionWidth() {
    return prawnSelectionWidth;
  }

  public int getPrawnSelectionHeight() {
    return prawnSelectionHeight;
  }

  public int getPrawnBoxSide() {
    return prawnBoxSide;
  }

  public int getCityFactoryBoxMaxCount() {
    return cityFactoryBoxMaxCount;
  }

  public int getCityFactoryBoxWidth() {
    return cityFactoryBoxWidth;
  }

  public int getCityFactoryBoxHeight() {
    return cityFactoryBoxHeight;
  }

  public int getCityPrawnSelectionWidth() {
    return cityPrawnSelectionWidth;
  }

  public int getCityPrawnSelectionHeight() {
    return cityPrawnSelectionHeight;
  }

  public int getCityPrawnBoxSide() {
    return cityPrawnBoxSide;
  }

  public int getWarriorDirectionSelectionBoxSide() {
    return warriorDirectionSelectionBoxSide;
  }

  public int getPromptBoxWidth() {
    return promptBoxWidth;
  }

  public int getPromptBoxHeight() {
    return promptBoxHeight;
  }

  public int getCityBoxSide() {
    return cityBoxSide;
  }

  public int getCityCreationBoxWidth() {
    return cityCreationBoxWidth;
  }

  public int getCityCreationBoxHeight() {
    return cityCreationBoxHeight;
  }

  public int getPrawnRepairPromptWidth() {
    return prawnRepairPromptWidth;
  }

  public int getPrawnRepairPromptHeight() {
    return prawnRepairPromptHeight;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof ClientViewProperties)) {
      return false;
    }
    final ClientViewProperties otherGameProperties = (ClientViewProperties) obj;
    return this.clientViewPropertiesSet.equals(otherGameProperties.clientViewPropertiesSet);
  }

}
