package cz.wie.najtmar.gop.client.view;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.Nonnull;

/**
 * Client-side representation of class Itinerary .
 * @author najtmar
 */
public class ItineraryView {

  /**
   * Nodes of which the Itinerary consists.
   */
  private final List<PositionView> nodes;

  /**
   * Determines whether the Itinerary is finished or not.
   * @return true iff the Itinerary is finished
   */
  public boolean isFinished() {
    return nodes.size() <= 1;
  }

  public List<PositionView> getNodes() {
    return nodes;
  }

  /**
   * Updates Itinerary based on the move to be performed.
   * @param position determines the next move to be performed; must be on the same RoadSection as the current node
   * @throws ClientViewException if the move specified by position is invalid
   */
  public void updateItinerary(@Nonnull PositionView position) throws ClientViewException {
    if (nodes.size() <= 1) {
      throw new ClientViewException("The Itinerary is finished.");
    }

    // Normalize first and next node to enable iterating over a common RoadSection.
    final RoadSectionView roadSection = PositionView.findCommonRoadSection(nodes.get(0), position);
    if (roadSection == null) {
      throw new ClientViewException("Positions " + nodes.get(0) + " and " + position
          + " are not on the same RoadSection .");
    }
    int firstIndex = roadSection.getProjectedPosition(nodes.get(0)).getIndex();
    int nextIndex = roadSection.getProjectedPosition(position).getIndex();

    while (firstIndex != nextIndex) {
      if (nodes.get(0).equals(roadSection.getPositions()[firstIndex])) {
        if (nodes.size() >= 2) {
          final PositionView futureNode = roadSection.getProjectedPosition(nodes.get(1));
          if (futureNode != null) {
            if ((firstIndex < nextIndex && futureNode.getIndex() < firstIndex)
                || (nextIndex < firstIndex && firstIndex < futureNode.getIndex())) {
              throw new ClientViewException("" + position + " not on Itinerary " + nodes + ".");
            }
          }
        }
        nodes.remove(0);
      }
      if (nodes.isEmpty()) {
        throw new ClientViewException("Destination of the move (" + position + ") is beyond the end of the Itinerary.");
      }
      if (firstIndex < nextIndex) {
        ++firstIndex;
      } else {
        --firstIndex;
      }
    }
    if (!nodes.get(0).equals(roadSection.getPositions()[firstIndex])) {
      nodes.add(0, position);
    }
  }

  @Override
  public String toString() {
    return "ItineraryView [nodes=" + nodes + "]";
  }

  public static class Builder {

    private final List<PositionView> nodes = new LinkedList<>();

    /**
     * Determines whether the last action (adding/removing nodes) succeeded or not.
     */
    private boolean lastActionSuccessful;

    public Builder(@Nonnull PositionView initialNode) {
      this.nodes.add(initialNode);
      this.lastActionSuccessful = true;
    }

    public boolean isLastActionSuccessful() {
      return lastActionSuccessful;
    }

    /**
     * Returns the last node added with a successful call to addNode().
     * @return the last node added with a successful call to addNode()
     */
    public PositionView getLastNodeAdded() {
      return nodes.get(nodes.size() - 1);
    }

    /**
     * Adds positions at the end of the Itinerary built. If the position does not match the Itinerary then does nothing
     * and marks the last action as not successful.
     * @param node the node to be added to the Itinerary
     * @return the current instance of Builder
     * @throws ClientViewException if adding position fails
     */
    public Builder addNode(@Nonnull PositionView node) throws ClientViewException {
      final PositionView lastNode = nodes.get(nodes.size() - 1);
      if (lastNode.equals(node)) {
        lastActionSuccessful = false;
        return this;
      }
      if (PositionView.findCommonRoadSection(lastNode, node) != null) {
        nodes.add(node);
        lastActionSuccessful = true;
      } else {
        lastActionSuccessful = false;
      }
      return this;
    }

    /**
     * Removes the last node from the Itinerary. If the Itinerary has only one node, then it remains unchanged
     * and the last action is marked as not successful.
     * @return the current instance of Builder
     */
    public Builder removeLastNode() {
      if (nodes.size() > 1) {
        nodes.remove(nodes.size() - 1);
        lastActionSuccessful = true;
      } else {
        lastActionSuccessful = false;
      }
      return this;
    }

    /**
     * Builds returns an instance of Itinerary, it it contains at least two nodes.
     * @return a new instance of Itinerary, or null if there is only one node defined
     */
    public ItineraryView build() {
      if (nodes.size() >= 2) {
        return new ItineraryView(new LinkedList<>(nodes));
      }
      return null;
    }

  }

  private ItineraryView(@Nonnull List<PositionView> nodes) {
    this.nodes = nodes;
  }

}
