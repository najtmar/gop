package cz.wie.najtmar.gop.client.view;

import cz.wie.najtmar.gop.board.BoardException;
import cz.wie.najtmar.gop.board.BoardProperties;
import cz.wie.najtmar.gop.common.Point2D;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonValue;

/**
 * View of the Game from the point of view of a Player . Used in client applications.
 * @author najtmar
 */
public class GameView {

  /**
   * Unique Game id.
   */
  private final UUID id;

  /**
   * Clock time timestamp used to identify the Game .
   */
  private final long timestamp;

  /**
   * The current Game time.
   */
  private long time;

  /**
   * The index of the Player on whose behalf the Game is viewed.
   */
  private final int currentPlayerIndex;

  /**
   * Whether the Game is initialized.
   */
  private boolean initialized;

  /**
   * X-size of the Board.
   */
  private int xsize;

  /**
   * Y-size of the Board.
   */
  private int ysize;

  /**
   * All Joints on the Board in the Game.
   */
  private JointView[] joints;

  /**
   * All RoadSections on the Board in the Game.
   */
  private RoadSectionView[] roadSections;

  /**
   * Players taking part in the Game.
   */
  private PlayerView[] players;

  /**
   * Container of Prawns taking part in the Game. Indexed by Prawn id.
   */
  private final HashMap<Integer, PrawnView> prawnMap = new HashMap<>();

  /**
   * Maps a Player to the set of all its Prawns that currently exist.
   */
  private final HashMap<PlayerView, HashSet<PrawnView>> playerPrawns = new HashMap<>();

  /**
   * Container of Cities taking part in the Game. Indexed by City id. Cities once added are kept forever.
   */
  private final HashMap<Integer, CityView> cityMap = new HashMap<>();

  /**
   * Maps a Player to the set of all its Cities that currently exist.
   */
  private final HashMap<PlayerView, HashSet<CityView>> playerCities = new HashMap<>();

  /**
   * The List of all Battles in the Game.
   */
  private final List<BattleView> battles = new ArrayList<>();

  /**
   * Maps a Battle hashCode to all the Battles that match that hashCode .
   */
  private final Map<Integer, Set<BattleView>> hashToBattles = new HashMap<>();

  /**
   * Constructor.
   * @param serializedGame the Game, in the serialized Json format
   * @param currentPlayerIndex the index of the Player on whose behalf the Game is viewed
   * @throws ClientViewException when the object could not be created
   */
  public GameView(@Nonnull JsonObject serializedGame, @Nonnegative int currentPlayerIndex) throws ClientViewException {
    this.id = UUID.fromString(serializedGame.getString("id"));
    this.timestamp = serializedGame.getJsonNumber("timestamp").longValue();
    try {
      initializeBoard(serializedGame.getJsonObject("board"));
    } catch (BoardException exception) {
      throw new ClientViewException("Board could not be initialized.", exception);
    }
    initializePlayers(serializedGame.getJsonArray("players"));
    this.currentPlayerIndex = currentPlayerIndex;
    this.time = 0;
    this.initialized = true;
  }

  public UUID getId() {
    return id;
  }

  public long getTimestamp() {
    return timestamp;
  }

  public long getTime() {
    return time;
  }

  /**
   * Advances Game time to time. The time cannot go back.
   * @param time the time to advance to
   * @throws ClientViewException when advancing time fails
   */
  public void advanceTimeTo(long time) throws ClientViewException {
    if (time < this.time) {
      throw new ClientViewException("Time cannot go backward (" + time + " < " + this.time + ").");
    }
    this.time = time;
  }

  public int getCurrentPlayerIndex() {
    return currentPlayerIndex;
  }

  public Map<Integer, PrawnView> getPrawnMap() {
    return prawnMap;
  }

  /**
   * Deserializes the Board for the Game and initializes corresponding fields. To be executed once in the constructor.
   * @param serializedBoard the Board to deserialize
   * @throws ClientViewException when Board cannot be deserialized of the Game initialized
   * @throws BoardException when reading BoardProperties fails
   */
  private void initializeBoard(@Nonnull JsonObject serializedBoard) throws ClientViewException, BoardException {
    if (initialized) {
      throw new ClientViewException("The Game is already initialized.");
    }

    // boardProperties
    final Properties boardPropertiesSet = new Properties();
    final JsonArray boardPropertiesArray = serializedBoard.getJsonArray("properties");
    for (JsonValue item : boardPropertiesArray) {
      final JsonObject propertyItem = (JsonObject) item;
      boardPropertiesSet.setProperty(propertyItem.getString("key"), propertyItem.getString("value"));
    }
    final BoardProperties boardProperties = new BoardProperties(boardPropertiesSet);
    final double stepSize = boardProperties.getStepSize();
    this.xsize = boardProperties.getXsize();
    this.ysize = boardProperties.getYsize();

    // joints
    {
      final JsonArray jointsArray = serializedBoard.getJsonArray("joints");
      this.joints = new JointView[jointsArray.size()];
      for (int i = 0; i < jointsArray.size(); ++i) {
        final JsonObject joint = (JsonObject) jointsArray.get(i);
        final int index = joint.getInt("index");
        if (index != i) {
          throw new ClientViewException("Joint " + i + " should have index " + i + "(" + joint + ").");
        }
        this.joints[i] = new JointView(index, joint.getString("name"),
            new Point2D.Double(joint.getJsonNumber("x").doubleValue(), joint.getJsonNumber("y").doubleValue()));
      }
    }

    // roadSections
    {
      final JsonArray roadSectionsArray = serializedBoard.getJsonArray("roadSections");
      this.roadSections = new RoadSectionView[roadSectionsArray.size()];
      for (int i = 0; i < roadSectionsArray.size(); ++i) {
        final JsonObject roadSection = (JsonObject) roadSectionsArray.get(i);
        final int index = roadSection.getInt("index");
        if (index != i) {
          throw new ClientViewException("RoadSection " + i + " should have index " + i + "(" + roadSection + ").");
        }
        this.roadSections[i] = new RoadSectionView(index,
            this.joints[roadSection.getInt("joint0")], this.joints[roadSection.getInt("joint1")], stepSize);
      }
    }
  }

  /**
   * Deserializes the Players for the Game and initializes corresponding fields. To be executed once in the constructor.
   * @param serializedPlayers an array of Players to deserialize
   * @throws ClientViewException when Players cannot be deserialized of the Game initialized
   */
  private void initializePlayers(@Nonnull JsonArray serializedPlayers) throws ClientViewException {
    if (initialized) {
      throw new ClientViewException("The Game is already initialized.");
    }

    this.players = new PlayerView[serializedPlayers.size()];
    for (int i = 0; i < serializedPlayers.size(); ++i) {
      final JsonObject player = (JsonObject) serializedPlayers.get(i);
      final JsonObject user = (JsonObject) player.getJsonObject("user");
      this.players[i] = new PlayerView(new UserView(user.getString("name"), UUID.fromString(user.getString("uuid"))),
          player.getInt("index"));
    }
  }

  public JointView[] getJoints() {
    return joints;
  }

  public RoadSectionView[] getRoadSections() {
    return roadSections;
  }

  public int getXsize() {
    return xsize;
  }

  public int getYsize() {
    return ysize;
  }

  public PlayerView[] getPlayers() {
    return players;
  }

  /**
   * Adds a Prawn to the Game.
   * @throws ClientViewException when prawn cannot be added
   */
  public void addPrawn(@Nonnull PrawnView prawn) throws ClientViewException {
    if (prawnMap.containsKey(prawn.getId())) {
      throw new ClientViewException("Prawn with id " + prawn.getId() + " already exists in the Game.");
    }
    prawnMap.put(prawn.getId(), prawn);
    final PlayerView player = prawn.getPlayer();
    if (!playerPrawns.containsKey(player)) {
      playerPrawns.put(player, new HashSet<>());
    }
    playerPrawns.get(player).add(prawn);
    if (prawn.getPlayer().equals(players[currentPlayerIndex])) {
      prawn.setVisibility(PrawnView.Visibility.VISIBLE);
    }
  }

  /**
   * Returns all Prawns for player.
   * @param player the Player whose Prawns should be returned
   * @return the Set of all Prawns for player
   */
  public Set<PrawnView> getPlayerPrawns(@Nonnull PlayerView player) {
    if (playerPrawns.containsKey(player)) {
      return playerPrawns.get(player);
    } else {
      return new HashSet<>();
    }
  }

  /**
   * Looks up a Prawn with a given id.
   * @param id the id of the Prawn to look up
   * @return the Prawn with a given id or null if such Prawn does not exist in the Game
   */
  public PrawnView lookupPrawnById(int id) {
    return prawnMap.get(id);
  }

  /**
   * Marks city as now owned by player.
   * @param city the City captured
   * @param defender the Player that originally owned city
   * @param attacker the Player that captures city
   * @throws ClientViewException when city capturing fails
   */
  public void captureCity(@Nonnull CityView city, @Nonnull PlayerView defender, @Nonnull PlayerView attacker)
      throws ClientViewException {
    if (!playerCities.containsKey(defender) || !playerCities.get(defender).contains(city)) {
      throw new ClientViewException("" + city + " not marked as owned by " + defender + " .");
    }
    if (playerCities.containsKey(attacker) && playerCities.get(attacker).contains(city)) {
      throw new ClientViewException("" + city + " already marked as owned by " + defender + " .");
    }
    playerCities.get(defender).remove(city);
    if (!playerCities.containsKey(attacker)) {
      playerCities.put(attacker, new HashSet<>());
    }
    playerCities.get(attacker).add(city);
  }

  /**
   * Adds a City of size 1 to the Game.
   * @throws ClientViewException when city cannot be added
   */
  public void addCity(@Nonnull CityView city) throws ClientViewException {
    if (city.getState() != CityView.State.EXISTING) {
      throw new ClientViewException("" + city + " is not is state EXISTING .");
    }
    if (cityMap.containsKey(city.getId())) {
      throw new ClientViewException("City with id " + city.getId() + " already exists in the Game.");
    }
    cityMap.put(city.getId(), city);
    final PlayerView player = city.getPlayer();
    if (!playerCities.containsKey(player)) {
      playerCities.put(player, new HashSet<>());
    }
    playerCities.get(player).add(city);
    if (city.getPlayer().equals(players[currentPlayerIndex])) {
      city.setVisibility(CityView.Visibility.DISCOVERED);
    }
  }

  /**
   * Removes city from the Game.
   * @throws ClientViewException when city cannot be removed
   */
  public void removeCity(@Nonnull CityView city) throws ClientViewException {
    if (city.getState() != CityView.State.REMOVED) {
      throw new ClientViewException("" + city + " is not is state REMOVED .");
    }
    final PlayerView player = city.getPlayer();
    final Set<CityView> cities;
    if (playerCities.containsKey(player)) {
      cities = playerCities.get(player);
    } else {
      cities = new HashSet<>();
    }
    if (!cityMap.containsKey(city.getId()) || !cities.contains(city)) {
      throw new ClientViewException("" + city + " cannot be removed because it does not exist in the Game.");
    }
    cities.remove(city);
    if (city.getPlayer().equals(players[currentPlayerIndex])) {
      city.setVisibility(CityView.Visibility.DESTROYED);
    }
  }

  /**
   * Looks up a City with a given id.
   * @param id the id of the City to look up
   * @return the City with a given id or null if such City does not exist in the Game
   */
  public CityView lookupCityById(int id) {
    return cityMap.get(id);
  }

  /**
   * Returns all Cities for player.
   * @param player the Player whose Cities should be returned
   * @return the Set of all Cities for player
   */
  public Set<CityView> getPlayerCities(@Nonnull PlayerView player) {
    if (playerCities.containsKey(player)) {
      return playerCities.get(player);
    } else {
      return new HashSet<>();
    }
  }

  /**
   * Adds a Battle to the internal collections of Battles .
   * @param battle the Battle to be added
   */
  public void addBattle(@Nonnull BattleView battle) {
    battles.add(battle);
    if (!hashToBattles.containsKey(battle.hashCode())) {
      hashToBattles.put(battle.hashCode(), new HashSet<>());
    }
    hashToBattles.get(battle.hashCode()).add(battle);
  }

  public List<BattleView> getBattles() {
    return battles;
  }

  /**
   * Returns the Set of all Battles matching the arguments. The order of the arguments does matter.
   * @param participant0 the first participant in the Battle
   * @param participant1 the second participant in the Battle
   * @param normalizedPosition0 the normalized Position of participant0
   * @param normalizedPosition1 the normalized Position of participant1
   * @return the Set of all Battles matching the arguments; an empty Set if no matching Battles are found
   */
  public Set<BattleView> getMatchingBattles(@Nonnull PrawnView participant0, @Nonnull PrawnView participant1,
      @Nonnull PositionView normalizedPosition0, @Nonnull PositionView normalizedPosition1) {
    final int battleHashCode = BattleView.calculateHashCode(participant0, participant1,
        normalizedPosition0, normalizedPosition1);
    final Set<BattleView> battleSet;
    if (hashToBattles.containsKey(battleHashCode)) {
      battleSet = hashToBattles.get(battleHashCode);
    } else {
      battleSet = new HashSet<>();
    }
    return StreamSupport.stream(battleSet).filter((el) -> el.matches(
        participant0, participant1, normalizedPosition0, normalizedPosition1)).collect(Collectors.toSet());
  }

  /**
   * Returns a Set of all Cities existing in the Game .
   * @return a Set of all Cities existing in the Game
   */
  public Set<CityView> getCities() {
    return new HashSet<>(cityMap.values());
  }

  /**
   * Given roadSection and city adjacent to roadSection, calculates the number of RoadSection.Halves captured on
   * roadSection .
   * @param roadSection the RoadSection to check
   * @param city the City to check
   * @return the number of RoadSection.Halves captured on roadSection
   * @throws ClientViewException when the number of captured RoadSection.Halves cannot be calculated
   */
  public int calculateCapturedRoadSectionHalvesCount(@Nonnull RoadSectionView roadSection, @Nonnull CityView city)
      throws ClientViewException {
    int capturedRoadSectionHalvesCount = 0;
    final RoadSectionView.Direction cityDirection;
    if (city.getJoint().equals(roadSection.getJoints()[0])) {
      cityDirection = RoadSectionView.Direction.BACKWARD;
    } else if (city.getJoint().equals(roadSection.getJoints()[1])) {
      cityDirection = RoadSectionView.Direction.FORWARD;
    } else {
      throw new ClientViewException("" + city + " not adjacent to " + roadSection + " .");
    }
    // Check the opponents' Prawns.
    for (PlayerView opponent : players) {
      if (!opponent.equals(city.getPlayer())) {
        opponentPrawnLoop:
        for (PrawnView opponentPrawn : getPlayerPrawns(opponent)) {
          if (opponentPrawn.getState() == PrawnView.State.REMOVED) {
            continue;
          }
          PositionView opponentPrawnPosition;
          opponentPrawnPosition = roadSection.getProjectedPosition(opponentPrawn.getCurrentPosition());
          if (opponentPrawnPosition == null) {
            continue opponentPrawnLoop;
          }
          if ((cityDirection.equals(RoadSectionView.Direction.BACKWARD)
               && opponentPrawnPosition.getIndex() <= roadSection.getMidpointIndex())
              || (cityDirection.equals(RoadSectionView.Direction.FORWARD)
                  && roadSection.getMidpointIndex() <= opponentPrawnPosition.getIndex())) {
            // The whole RoadSection is captured.
            return 2;
          } else {
            capturedRoadSectionHalvesCount = 1;
          }
        }
      }
    }

    if (capturedRoadSectionHalvesCount == 1) {
      // Skipping City checking, as it would not change the contribution.
      return 1;
    }

    // Check other cities.
    for (PlayerView anyPlayer : players) {
      for (CityView otherCity : getPlayerCities(anyPlayer)) {
        if (!otherCity.equals(city)) {
          if (roadSection.adjacentTo(otherCity.getJoint())) {
            // Skipping further checking, as it would not change the contribution.
            return 1;
          }
        }
      }
    }

    if (capturedRoadSectionHalvesCount != 0) {
      throw new ClientViewException("Unexpected capturedRoadSectionHalvesCount value: "
          + capturedRoadSectionHalvesCount);
    }
    return 0;
  }

  /**
   * Given the viewport size, calculates the scroll bar initial position.
   * @param viewportWidth the width of the viewport
   * @param viewportHeight the height of the viewport
   * @return the scroll bar initial position, expressed as a pair between (0, 0) and (1, 1)
   */
  public Point2D.Double calculateScrolledBoardBarInitialSetup(int viewportWidth,
      int viewportHeight) {
    double xcoord = 0.0;
    double ycoord = 0.0;
    int prawnCount = 0;
    for (PrawnView ownPrawn : getPlayerPrawns(getPlayers()[currentPlayerIndex])) {
      final Point2D.Double ownPrawnCoordinates = ownPrawn.getCurrentPosition().getCoordinatesPair();
      xcoord += ownPrawnCoordinates.getX();
      ycoord += ownPrawnCoordinates.getY();
      ++prawnCount;
    }
    if (0.0 < prawnCount) {
      xcoord /= prawnCount;
      ycoord /= prawnCount;
    }
    double hvalue;
    if (getXsize() == viewportWidth) {
      hvalue = 0.0;
    } else {
      hvalue = (xcoord - viewportWidth / 2) / (getXsize() - viewportWidth);
      if (hvalue < 0.0) {
        hvalue = 0.0;
      } else if (1.0 < hvalue) {
        hvalue = 1.0;
      }
    }
    double vvalue;
    if (getYsize() == viewportHeight) {
      vvalue = 0.0;
    } else {
      vvalue = (ycoord - viewportHeight / 2) / (getYsize() - viewportHeight);
      if (vvalue < 0.0) {
        vvalue = 0.0;
      } else if (1.0 < vvalue) {
        vvalue = 1.0;
      }
    }
    return new Point2D.Double(hvalue, vvalue);
  }

  @Override
  public String toString() {
    return "GameView [xsize=" + xsize + ", ysize=" + ysize + ", joints=" + Arrays.toString(joints)
        + ", roadSections=" + Arrays.toString(roadSections) + ", currentPlayerIndex=" + currentPlayerIndex
        + ", players=" + Arrays.toString(players) + ", initialized=" + initialized + "]";
  }

}
