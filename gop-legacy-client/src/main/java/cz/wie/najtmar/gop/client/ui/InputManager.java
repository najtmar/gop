package cz.wie.najtmar.gop.client.ui;

import cz.wie.najtmar.gop.client.view.CityView;
import cz.wie.najtmar.gop.client.view.ClientViewException;
import cz.wie.najtmar.gop.client.view.ClientViewProperties;
import cz.wie.najtmar.gop.client.view.DistinctivePositionView;
import cz.wie.najtmar.gop.client.view.EventFactory;
import cz.wie.najtmar.gop.client.view.FactoryView;
import cz.wie.najtmar.gop.client.view.FactoryView.ActionType;
import cz.wie.najtmar.gop.client.view.GameView;
import cz.wie.najtmar.gop.client.view.ItineraryView;
import cz.wie.najtmar.gop.client.view.JointView;
import cz.wie.najtmar.gop.client.view.ObjectSerializer;
import cz.wie.najtmar.gop.client.view.PlayerView;
import cz.wie.najtmar.gop.client.view.PositionSelection;
import cz.wie.najtmar.gop.client.view.PositionView;
import cz.wie.najtmar.gop.client.view.PrawnSelection;
import cz.wie.najtmar.gop.client.view.PrawnView;
import cz.wie.najtmar.gop.client.view.RoadSectionView;
import cz.wie.najtmar.gop.common.Point;
import cz.wie.najtmar.gop.common.Point2D;
import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.event.EventProcessingException;
import cz.wie.najtmar.gop.event.SerializationException;
import cz.wie.najtmar.gop.game.PropertiesReader;
import java8.util.stream.StreamSupport;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NavigableMap;
import java.util.Set;
import java.util.Stack;
import java.util.TreeMap;
import java.util.logging.Logger;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.GuardedBy;

/**
 * Platform-independent class responsible for the collection of user input.
 * @author najtmar
 */
public class InputManager {

  private static final Logger LOGGER = Logger.getLogger(InputManager.class.getName());

  /**
   * The State of an InputManager .
   * @author najtmar
   */
  public enum State {
    /**
     * The initial State of the interface. Equivalent to an empty stateStack .
     */
    NO_GAME,
    /**
     * The Game has started. No input accepted.
     */
    INITIALIZED,
    /**
     * The basic state of an InputManager . The Board is shown, the User can select an element.
     */
    BOARD,
    /**
     * Similar to the BOARD State, except that only a magnified part of the Board is shown.
     */
    MAGNIFIED,
    /**
     * A Prawn is selected on the Board, to set or clean its Itinerary, or to build a City .
     */
    PRAWN,
    /**
     * A PrawnSelection box is shown on the Board to select a Prawn from a Position .
     */
    PRAWN_SELECTION,
    /**
     * An Itinerary for a Prawn is currently being built.
     */
    ITINERARY_BUILT,
    /**
     * A City is selected, to further select one one of its Factories or Prawns that are located there.
     */
    CITY,
    /**
     * A SettlersUnit is being ordered to build a City .
     */
    CITY_CREATION,
    /**
     * A City Factory is selected, to set or unset its Action .
     */
    FACTORY,
    /**
     * A Prawn for repair is being selected.
     */
    PRAWN_REPAIR,
    /**
     * Warrior direction is being selected to a Warrior to be built.
     */
    WARRIOR_DIRECTION,
    /**
     * The Player is about to abandon the Game.
     */
    GAME_ABANDONING,
    /**
     * The Game is finished.
     */
    GAME_FINISHED,
    /**
     * The User is notified that the Connection to the Game server has been broken.
     */
    GAME_SERVER_RECONNECTION,
    /**
     * Due to some unexpected behavior, the object is an INVALID State .
     */
    INVALID,
  }

  /**
   * Reads Properties used.
   */
  private final PropertiesReader propertiesReader;

  /**
   * ClientViewProperties used.
   */
  private ClientViewProperties clientViewProperties;

  /**
   * The Game managed by the object.
   */
  private GameView game;

  /**
   * EventFactory used to create Events .
   */
  private EventFactory eventFactory;

  /**
   * ObjectSerializer used to read Events .
   */
  private ObjectSerializer objectSerializer;

  /**
   * The Player whose input is handled by the object.
   */
  private PlayerView player;

  /**
   * The Stack of States. The top State is the current State. Empty stateStack is equivalent to the NO_GAME State .
   */
  @GuardedBy("this")
  private final Stack<State> stateStack;

  /**
   * If the object is in the INVALID State, then this field may contain the relevant error message.
   */
  private String errorMessage;

  /**
   * The platform-dependent engine to handle user input.
   */
  private final InputEngine inputEngine;

  /**
   * The platform-dependent engine to draw Game elements.
   */
  private final OutputEngine outputEngine;

  /**
   * Used to select Positions based on user actions.
   */
  private ItemSelector itemSelector;

  /**
   * Input Events provided in the last invocation of interact().
   */
  private Event[] inputEvents;

  /**
   * The list of Events generated within the current Input session.
   */
  private List<Event> eventsGenerated;

  /**
   * Represents the Point selected in the last relevant operation.
   */
  private Point2D.Double pointSelected;

  /**
   * Represents the PositionSelection made in the last relevant operation.
   */
  private PositionSelection positionSelection;

  /**
   * Represents the PrawnSelection made in the last relevant operation.
   */
  private PrawnSelection prawnSelection;

  /**
   * Represents the Prawn selected in the last relevant operation.
   */
  private PrawnView prawnSelected;

  /**
   * Represents the Prawn Itinerary currently being built.
   */
  private ItineraryView.Builder itineraryBuilder;

  /**
   * The maximum number of CityFactory boxes shown in the City view.
   */
  private int cityFactoryBoxMaxCount;

  /**
   * Represents the City selected in the last relevant operation.
   */
  private CityView citySelected;

  /**
   * The width in pixels of the CityFactory box.
   */
  private int cityFactoryBoxWidth;

  /**
   * The height in pixels of the CityFactory box.
   */
  private int cityFactoryBoxHeight;

  /**
   * Represents the CityFactory selected in the last relevant operation.
   */
  private FactoryView factorySelected;

  /**
   * The index of the CityFactory box selected in the last relevant operation.
   */
  private int factoryBoxIndexSelected;

  /**
   * For each RoadSection adjacent to the citySelected, the number of RoadSection.Halves that are captured on
   * that RoadSection.
   */
  private int[] selectedCityCapturedHalves;

  /**
   * The number from interval [0.0, 1.0] denoting the productivity of citySelected .
   */
  private double selectedCityProductivity;

  /**
   * Constructor.
   * @param propertiesReader reads Properties used
   * @param inputEngine the platform-dependent engine to handle user input
   * @param outputEngine the platform-dependent engine to draw Game elements
   */
  public InputManager(@Nonnull PropertiesReader propertiesReader, @Nonnull InputEngine inputEngine,
      @Nonnull OutputEngine outputEngine) {
    this.stateStack = new Stack<>();
    this.propertiesReader = propertiesReader;
    this.inputEngine = inputEngine;
    this.outputEngine = outputEngine;
  }

  /**
   * Initializes the manager for game .
   * @param game the Game to be managed by the manager
   * @throws ClientViewException when starting process fails.
   */
  public void initialize(@Nonnull GameView game) throws ClientViewException {
    if (getState() != State.NO_GAME) {
      throw new ClientViewException("start() can only be invoked in State NO_GAME (State stack: " + stateStack + ").");
    }
    this.game = game;
    this.objectSerializer = new ObjectSerializer(game);
    this.eventFactory = new EventFactory(this.objectSerializer);
    this.player = game.getPlayers()[game.getCurrentPlayerIndex()];
    this.inputEngine.initializeInput(game, this);
    try {
      this.clientViewProperties = new ClientViewProperties(propertiesReader.getClientViewProperties());
    } catch (IOException exception) {
      throw new ClientViewException("Error reading Client view Properties .", exception);
    }
    this.itemSelector = new ItemSelector(game, this.clientViewProperties);
    this.cityFactoryBoxMaxCount = clientViewProperties.getCityFactoryBoxMaxCount();
    this.cityFactoryBoxWidth = clientViewProperties.getCityFactoryBoxWidth();
    this.cityFactoryBoxHeight = clientViewProperties.getCityFactoryBoxHeight();
    pushState(State.INITIALIZED);
  }

  /**
   * Returns the current State of the object.
   * @return the current State of the object; the stateStack is empty, then State INITIAL is returned
   */
  public synchronized State getState() {
    if (!stateStack.isEmpty()) {
      return stateStack.peek();
    }
    return State.NO_GAME;
  }

  /**
   * Changes the State by putting it on top of the current State .
   * @param state the State to change to
   */
  private synchronized void pushState(@Nonnull State state) {
    stateStack.push(state);
  }

  /**
   * Changes the State by removing the current State and applying the one earlier on the Stack .
   * @return the State being removed
   * @throws ClientViewException if stateStack is empty
   */
  private synchronized State popState() throws ClientViewException {
    if (stateStack.isEmpty()) {
      throw new ClientViewException("stateStack is empty.");
    }
    return stateStack.pop();
  }

  public Event[] getInputEvents() {
    return inputEvents;
  }

  public Point2D.Double getPointSelected() {
    return pointSelected;
  }

  public PrawnView getPrawnSelected() {
    return prawnSelected;
  }

  public CityView getCitySelected() {
    return citySelected;
  }

  /**
   * Labels of relevant Events used for user information. Ordered by relevance.
   * @author najtmar
   */
  public enum RelevantEventLabel {
    GAME_STARTED,
    GAME_FINISHED,
    GAME_WON,
    GAME_LOST,
    GAME_ABANDONED,
    GAME_DRAWN,
    OPPONENT_ABANDONED,
    OWN_CITY_CAPTURED,
    OWN_CITY_DESTROYED,
    OPPONENTS_CITY_CAPTURED,
    OPPONENTS_CITY_DESTROYED,
    BATTLE_STARTED,
    OWN_CITY_GROWN,
    OWN_CITY_SHRINKED,
    SETTLERS_UNIT_LOST,
    WARRIOR_LOST,
    OWN_CITY_CREATED,
    SETTLERS_UNIT_PRODUCED,
    WARRIOR_PRODUCED,
    FACTORY_ACTION_FINISHED,
    BATTLE_FINISHED,
    PRAWN_ITINERARY_FINISHED,
    PERIODIC_INTERRUPT,
  }

  /**
   * Increases by 1 in map the count of Events with a given label.
   * @param label the label of the Event
   * @param map the Map in which the count should be increased
   */
  private void incrementEventCountMap(@Nonnull RelevantEventLabel label, @Nonnull NavigableMap<String, Integer> map) {
    final int eventCount;
    if (map.containsKey(label.name())) {
      eventCount = map.get(label.name());
    } else {
      eventCount = 0;
    }
    map.put(label.name(), eventCount + 1);
  }

  /**
   * Given the list of inputEvents, generates an ordered Map of relevant Events that happened since the last
   * interaction.
   * @return an ordered Map of relevant Events that happened since the last interaction
   * @throws ClientViewException when creating the Map fails
   */
  private NavigableMap<String, Integer> createRelevantEventCountMap() throws ClientViewException {
    if (!getState().equals(State.BOARD)) {
      throw new ClientViewException("createRelevantEventCountMap() can only be invoked in the BOARD State "
          + "(State stack: " + stateStack + ").");
    }
    final NavigableMap<String, Integer> result = new TreeMap<>(
        (first, second) -> Integer.compare(
            RelevantEventLabel.valueOf(first).ordinal(), RelevantEventLabel.valueOf(second).ordinal()));
    for (Event inputEvent : inputEvents) {
      final boolean isEventRelevant;
      try {
        isEventRelevant = objectSerializer.getInterruptedPlayers(inputEvent).contains(player);
      } catch (SerializationException exception) {
        throw new ClientViewException(exception);
      }
      if (isEventRelevant) {
        switch (inputEvent.getType()) {
          case ACTION_FINISHED: {
            final CityView city;
            try {
              city = objectSerializer.deserializeCity(inputEvent.getBody().getJsonObject("factory").getInt("city"));
            } catch (SerializationException exception) {
              throw new ClientViewException("Reading an Event failed: " + inputEvent, exception);
            }
            if (city.getPlayer().equals(player)) {
              incrementEventCountMap(RelevantEventLabel.FACTORY_ACTION_FINISHED, result);
            }
            break;
          }
          case ADD_BATTLE: {
            final PrawnView prawn0;
            final PrawnView prawn1;
            try {
              prawn0 = objectSerializer.deserializePrawn(inputEvent.getBody().getJsonObject("battle").getInt("prawn0"));
              prawn1 = objectSerializer.deserializePrawn(inputEvent.getBody().getJsonObject("battle").getInt("prawn1"));
            } catch (SerializationException exception) {
              throw new ClientViewException("Reading an Event failed: " + inputEvent, exception);
            }
            if (prawn0.getPlayer().equals(player) || prawn1.getPlayer().equals(player)) {
              incrementEventCountMap(RelevantEventLabel.BATTLE_STARTED, result);
            }
            break;
          }
          case CAPTURE_OR_DESTROY_CITY: {
            final PlayerView defender;
            final PlayerView attacker;
            final CityView city;
            try {
              defender = objectSerializer.deserializePlayer(inputEvent.getBody().getInt("defender"));
              attacker = objectSerializer.deserializePlayer(inputEvent.getBody().getInt("attacker"));
              city = objectSerializer.deserializeCity(inputEvent.getBody().getInt("city"));
            } catch (SerializationException exception) {
              throw new ClientViewException("Reading an Event failed: " + inputEvent, exception);
            }
            if (defender.equals(player)) {
              if (city.getState().equals(CityView.State.EXISTING)) {
                incrementEventCountMap(RelevantEventLabel.OWN_CITY_CAPTURED, result);
              } else {
                incrementEventCountMap(RelevantEventLabel.OWN_CITY_DESTROYED, result);
              }
            } else if (attacker.equals(player)) {
              if (city.getState().equals(CityView.State.EXISTING)) {
                incrementEventCountMap(RelevantEventLabel.OPPONENTS_CITY_CAPTURED, result);
              } else {
                incrementEventCountMap(RelevantEventLabel.OPPONENTS_CITY_DESTROYED, result);
              }
            }
            break;
          }
          case CHANGE_PLAYER_STATE: {
            final PlayerView currentPlayer;
            try {
              currentPlayer = objectSerializer.deserializePlayer(inputEvent.getBody().getInt("player"));
            } catch (SerializationException exception) {
              throw new ClientViewException("Reading an Event failed: " + inputEvent, exception);
            }
            final String playerState = inputEvent.getBody().getString("state");
            if (currentPlayer.equals(player)) {
              switch (playerState) {
                case "LOST": {
                  incrementEventCountMap(RelevantEventLabel.GAME_LOST, result);
                  break;
                }
                case "ABANDONED": {
                  incrementEventCountMap(RelevantEventLabel.GAME_ABANDONED, result);
                  break;
                }
                case "WON": {
                  incrementEventCountMap(RelevantEventLabel.GAME_WON, result);
                  break;
                }
                case "UNDECIDED": {
                  incrementEventCountMap(RelevantEventLabel.GAME_DRAWN, result);
                  break;
                }
                default: {
                  throw new ClientViewException("Unexpected Player state: " + playerState);
                }
              }
            } else {
              if (playerState.equals("ABANDONED")) {
                incrementEventCountMap(RelevantEventLabel.OPPONENT_ABANDONED, result);
              }
            }
            break;
          }
          case CREATE_CITY: {
            final CityView city;
            try {
              city = objectSerializer.deserializeCity(inputEvent.getBody().getInt("id"));
            } catch (SerializationException exception) {
              throw new ClientViewException("Reading an Event failed: " + inputEvent, exception);
            }
            if (city.getPlayer().equals(player)) {
              incrementEventCountMap(RelevantEventLabel.OWN_CITY_CREATED, result);
            }
            break;
          }
          case DESTROY_PRAWN: {
            final PrawnView prawn;
            try {
              prawn = objectSerializer.deserializePrawn(inputEvent.getBody().getInt("prawn"));
            } catch (SerializationException exception) {
              throw new ClientViewException("Reading an Event failed: " + inputEvent, exception);
            }
            if (prawn.getType() == PrawnView.Type.WARRIOR) {
              incrementEventCountMap(RelevantEventLabel.WARRIOR_LOST, result);
            } else {
              incrementEventCountMap(RelevantEventLabel.SETTLERS_UNIT_LOST, result);
            }
            break;
          }
          case FINISH_GAME: {
            incrementEventCountMap(RelevantEventLabel.GAME_FINISHED, result);
            break;
          }
          case GROW_CITY: {
            final CityView city;
            try {
              city = objectSerializer.deserializeCity(inputEvent.getBody().getInt("city"));
            } catch (SerializationException exception) {
              throw new ClientViewException("Reading an Event failed: " + inputEvent, exception);
            }
            if (city.getPlayer().equals(player)) {
              incrementEventCountMap(RelevantEventLabel.OWN_CITY_GROWN, result);
            }
            break;
          }
          case INITIALIZE_GAME: {
            incrementEventCountMap(RelevantEventLabel.GAME_STARTED, result);
            break;
          }
          case MOVE_PRAWN: {
            final PrawnView prawn;
            final boolean isStop;
            try {
              prawn = objectSerializer.deserializePrawn(inputEvent.getBody().getInt("prawn"));
              isStop = inputEvent.getBody().getJsonObject("move").getBoolean("stop");
            } catch (SerializationException exception) {
              throw new ClientViewException("Reading an Event failed: " + inputEvent, exception);
            }
            if (prawn.getPlayer().equals(player) && isStop) {
              incrementEventCountMap(RelevantEventLabel.PRAWN_ITINERARY_FINISHED, result);
            }
            break;
          }
          case PERIODIC_INTERRUPT: {
            incrementEventCountMap(RelevantEventLabel.PERIODIC_INTERRUPT, result);
            break;
          }
          case PRODUCE_SETTLERS_UNIT: {
            final PrawnView settlersUnit;
            try {
              settlersUnit = objectSerializer.deserializePrawn(inputEvent.getBody().getInt("id"));
            } catch (SerializationException exception) {
              throw new ClientViewException("Reading an Event failed: " + inputEvent, exception);
            }
            if (settlersUnit.getPlayer().equals(player)) {
              incrementEventCountMap(RelevantEventLabel.SETTLERS_UNIT_PRODUCED, result);
            }
            break;
          }
          case PRODUCE_WARRIOR: {
            final PrawnView warrior;
            try {
              warrior = objectSerializer.deserializePrawn(inputEvent.getBody().getInt("id"));
            } catch (SerializationException exception) {
              throw new ClientViewException("Reading an Event failed: " + inputEvent, exception);
            }
            if (warrior.getPlayer().equals(player)) {
              incrementEventCountMap(RelevantEventLabel.WARRIOR_PRODUCED, result);
            }
            break;
          }
          case REMOVE_BATTLE: {
            final PrawnView prawn0;
            final PrawnView prawn1;
            try {
              prawn0 = objectSerializer.deserializePrawn(inputEvent.getBody().getJsonObject("battle").getInt("prawn0"));
              prawn1 = objectSerializer.deserializePrawn(inputEvent.getBody().getJsonObject("battle").getInt("prawn1"));
            } catch (SerializationException exception) {
              throw new ClientViewException("Reading an Event failed: " + inputEvent, exception);
            }
            if (prawn0.getPlayer().equals(player) || prawn1.getPlayer().equals(player)) {
              incrementEventCountMap(RelevantEventLabel.BATTLE_FINISHED, result);
            }
            break;
          }
          case SHRINK_OR_DESTROY_CITY: {
            final CityView city;
            try {
              city = objectSerializer.deserializeCity(inputEvent.getBody().getInt("city"));
            } catch (SerializationException exception) {
              throw new ClientViewException("Reading an Event failed: " + inputEvent, exception);
            }
            if (city.getState().equals(CityView.State.EXISTING)) {
              incrementEventCountMap(RelevantEventLabel.OWN_CITY_SHRINKED, result);
            } else {
              incrementEventCountMap(RelevantEventLabel.OWN_CITY_DESTROYED, result);
            }
            break;
          }
          default: {
            // Event ignored.
            break;
          }
        }
      }
    }
    return result;
  }

  /**
   * Checks whether the Game is finished or not for the current Player .
   * @return true iff the Game is finished
   * @throws ClientViewException when checking the Game State fails
   */
  private boolean isGameFinished() throws ClientViewException {
    if (!getState().equals(State.BOARD)) {
      throw new ClientViewException("isGameFinished() can only be invoked in the BOARD State "
          + "(State stack: " + stateStack + ").");
    }
    if (player.getState() == PlayerView.State.ABANDONED) {
      return true;
    }
    for (PlayerView player : game.getPlayers()) {
      if (player.getState() == PlayerView.State.UNDECIDED) {
        return true;
      }
    }
    for (PlayerView player : game.getPlayers()) {
      if (player.getState() == PlayerView.State.IN_GAME) {
        return false;
      }
    }
    return true;
  }

  /**
   * User interaction which results with providing a list of Events to be executed.
   * @param inputEvents Game Events from the current round
   * @return Events provided by the user
   * @throws ClientViewException when user interaction fails
   */
  public Event[] interact(@Nonnull Event[] inputEvents) throws ClientViewException {
    if (getState() != State.INITIALIZED) {
      throw new ClientViewException("input() can only be invoked in the INITIALIZED State (State stack: " + stateStack
          + ").");
    }
    this.inputEvents = inputEvents;
    eventsGenerated = new LinkedList<>();
    pushState(State.BOARD);

    outputEngine.onTimeUpdated(game.getTime());
    outputEngine.onPeriodicInfoUpdated(createRelevantEventCountMap());
    if (!isGameFinished()) {
      inputEngine.onStateChange(State.INITIALIZED);

      if (getState() == State.NO_GAME) {
        LOGGER.info("State NO_GAME indicates that the Game has been force quit.");
        return eventsGenerated.toArray(new Event[]{});
      }
      if (getState() == State.INVALID) {
        throw new ClientViewException(errorMessage);
      }
      if (getState() != State.BOARD) {
        throw new ClientViewException("Interaction must always finish in the BOARD State (found " + stateStack + ").");
      }
      popStateView();
      if (getState() != State.INITIALIZED) {
        throw new ClientViewException("Unexpected State detected under State BOARD: " + stateStack);
      }
    } else {
      pushState(State.GAME_FINISHED);
      inputEngine.onStateChange(State.INITIALIZED);
    }

    return eventsGenerated.toArray(new Event[]{});
  }

  /**
   * Draws a City creation attempt.
   * @param settlersUnit the SettlersUnit that attempts to build the City
   */
  private void drawCityCreation(@Nonnull PrawnView settlersUnit) {
    outputEngine.onHighlightingRemoved();
    final Event cityCreationEvent;
    try {
      cityCreationEvent = settlersUnit.getCityCreationEvent();
    } catch (ClientViewException exception) {
      throw new RuntimeException("Cannot get the City creation Event.", exception);
    }
    outputEngine.onCityCreationHighlighted(settlersUnit.getCurrentPosition(),
        cityCreationEvent.getBody().getString("name"));
    drawSelectableSections(settlersUnit, null, false);
  }

  /**
   * Draws a Prawn itinerary on the Board .
   * @param itinerary the Itinerary to draw
   */
  private void drawItinerary(@Nonnull ItineraryView itinerary) {
    outputEngine.onHighlightingRemoved();
    PositionView beginning = null;
    for (PositionView end : itinerary.getNodes()) {
      if (beginning != null) {
        outputEngine.onLineSectionHighlighted(beginning, end);
      }
      beginning = end;
    }
  }

  /**
   * Returns the index of the end of the visible part of roadSection, starting from startIndex, and looking in
   * the direction of the given end .
   * @param roadSection the RoadSection analyzed
   * @param startIndex the start point to look from
   * @param end the end of roadSection
   * @return the end of the visible part of roadSection
   */
  private int getVisibleEndIndex(@Nonnull RoadSectionView roadSection, @Nonnegative int startIndex,
      @Nonnull RoadSectionView.Direction end) {
    final int furtherHalfIndex;
    final int closerHalfIndex;
    final boolean isOnFurtherHalf;
    final boolean isOnCloserHalf;
    if (end.equals(RoadSectionView.Direction.BACKWARD)) {
      furtherHalfIndex = 0;
      closerHalfIndex = 1;
      isOnFurtherHalf = startIndex <= roadSection.getMidpointIndex();
      isOnCloserHalf = roadSection.getMidpointIndex() <= startIndex;
    } else {
      furtherHalfIndex = 1;
      closerHalfIndex = 0;
      isOnFurtherHalf = roadSection.getMidpointIndex() <= startIndex;
      isOnCloserHalf = startIndex <= roadSection.getMidpointIndex();
    }
    if (!roadSection.getHalfVisibilities()[furtherHalfIndex].equals(RoadSectionView.Visibility.INVISIBLE)
        && (!roadSection.getHalfVisibilities()[closerHalfIndex].equals(RoadSectionView.Visibility.INVISIBLE)
            || isOnFurtherHalf)) {
      return roadSection.getJointPositionIndexes()[furtherHalfIndex];
    } else if (!roadSection.getHalfVisibilities()[closerHalfIndex].equals(RoadSectionView.Visibility.INVISIBLE)
        && isOnCloserHalf) {
      return roadSection.getMidpointIndex();
    } else {
      return startIndex;
    }
  }

  /**
   * Draws parts of RoadSections that can be selected with one click, and which don't belong to itinerary .
   * @param prawn the Prawn for which the selectable sections are drawn
   * @param itinerary the Itinerary currently constructed for the Prawn
   * @param isResumed if true, then itinerary is currently being built; otherwise, the Itinerary will be built from
   *     scratch
   */
  private void drawSelectableSections(@Nonnull PrawnView prawn, @Nullable ItineraryView itinerary, boolean isResumed) {
    final List<PositionView[]> selectableSections = new LinkedList<>();

    // Selectable sections, including itinerary .
    final PositionView startPosition;
    if (!isResumed || itinerary == null || itinerary.getNodes().size() == 0) {
      startPosition = prawn.getCurrentPosition();
    } else {
      startPosition = itinerary.getNodes().get(itinerary.getNodes().size() - 1);
    }
    if (startPosition.getJoint() == null) {
      final RoadSectionView roadSection = startPosition.getRoadSection();

      // Lower part.
      final int lowerIndex =
          getVisibleEndIndex(roadSection, startPosition.getIndex(), RoadSectionView.Direction.BACKWARD);
      if (lowerIndex != startPosition.getIndex()) {
        selectableSections.add(
            new PositionView[]{
                roadSection.getPositions()[startPosition.getIndex()], roadSection.getPositions()[lowerIndex]});
      }

      // Upper part.
      final int upperIndex =
          getVisibleEndIndex(roadSection, startPosition.getIndex(), RoadSectionView.Direction.FORWARD);
      if (upperIndex != startPosition.getIndex()) {
        selectableSections.add(
            new PositionView[]{
                roadSection.getPositions()[startPosition.getIndex()], roadSection.getPositions()[upperIndex]});
      }
    } else {
      final JointView startPositionJoint = startPosition.getJoint();
      for (RoadSectionView roadSection : startPositionJoint.getRoadSections()) {
        if (startPositionJoint.equals(roadSection.getJoints()[0])) {
          final int endIndex = getVisibleEndIndex(roadSection, roadSection.getJointPositionIndexes()[0],
              RoadSectionView.Direction.FORWARD);
          if (endIndex != roadSection.getJointPositionIndexes()[0]) {
            selectableSections.add(
                new PositionView[]{
                    roadSection.getPositions()[roadSection.getJointPositionIndexes()[0]],
                    roadSection.getPositions()[endIndex]});
          }
        } else if (startPositionJoint.equals(roadSection.getJoints()[1])) {
          final int endIndex = getVisibleEndIndex(roadSection, roadSection.getJointPositionIndexes()[1],
              RoadSectionView.Direction.BACKWARD);
          if (endIndex != roadSection.getJointPositionIndexes()[1]) {
            selectableSections.add(
                new PositionView[]{
                    roadSection.getPositions()[roadSection.getJointPositionIndexes()[1]],
                    roadSection.getPositions()[endIndex]});
          }
        } else {
          throw new RuntimeException("This should never happen (" + startPositionJoint + " not on " + roadSection
              + ").");
        }
      }
    }

    // Draw the result.
    for (PositionView[] selectableSection : selectableSections) {
      outputEngine.onSelectableLineSectionHighlighted(selectableSection[0], selectableSection[1]);
    }
  }

  /**
   * Draws a PrawnSelection box with all Prawns and controls it contains.
   * @param prawnSelection the PrawnSelection to draw
   * @throws ClientViewException when PrawnSelection drawing fails
   */
  private void drawPrawnSelection(@Nonnull PrawnSelection prawnSelection) throws ClientViewException {
    outputEngine.onPrawnSelectionShown();
    final PrawnSelection.PrawnBox[][] prawnBoxes = prawnSelection.getPrawnBoxes();
    for (int j = 0; j < prawnSelection.getPrawnSelectionHeight(); ++j) {
      for (int i = 0; i < prawnSelection.getPrawnSelectionWidth(); ++i) {
        final Point2D.Double prawnBoxPoint = new Point2D.Double(
            prawnBoxes[i][j].getCoordX() + prawnSelection.getPrawnBoxSide() / 2.0,
            prawnBoxes[i][j].getCoordY() + prawnSelection.getPrawnBoxSide() / 2.0);
        switch (prawnBoxes[i][j].getType()) {
          case EMPTY: {
            // TODO(najtmar): Implement this.
            break;
          }
          case PRAWN: {
            outputEngine.onPrawnSelectionPrawnDrawn(prawnBoxes[i][j].getPrawn(), prawnBoxPoint);
            break;
          }
          case LEFT_SLIDER: {
            outputEngine.onPrawnSelectionLeftSlider(prawnBoxPoint, prawnSelection.getStartPosition());
            break;
          }
          case RIGHT_SLIDER: {
            outputEngine.onPrawnSelectionRightSlider(prawnBoxPoint, prawnSelection.getNextCount());
            break;
          }
          default: {
            throw new RuntimeException("Unknown PrawnBoxType: " + prawnBoxes[i][j].getType());
          }
        }
      }
    }
  }

  /**
   * Invoked when a Point is selected in State BOARD .
   * @param point the Point selected
   */
  public void onBoardPointSelected(@Nonnull Point2D.Double point) {
    if (getState() == State.INITIALIZED) {
      // Ignore this event.
      return;
    }
    if (getState() != State.BOARD) {
      throw new RuntimeException("onBoardPointSelected() can only be invoked in State BOARD (found " + getState()
          + ").");
    }
    pointSelected = point;
    positionSelection = itemSelector.selectPositions(point, player, ItemSelector.LookupType.ITEM_SELECTION);
    try {
      itemSelector.adjust(positionSelection);
    } catch (ClientViewException exception) {
      throw new RuntimeException("Adjusting a PositionSelection failed.", exception);
    }
    if (positionSelection.getDistinctivePositions().length == 1) {
      final PositionView position = positionSelection.getDistinctivePositions()[0].getPosition();
      final JointView joint = position.getJoint();
      if (joint != null && joint.getCity() != null) {
        citySelected = joint.getCity();
        try {
          prawnSelection = itemSelector.createPrawnSelection(joint.getNormalizedPosition().getPrawns(),
              citySelected.getPlayer());
        } catch (ClientViewException exception) {
          throw new RuntimeException(exception);
        }
        onCityViewShown();
      } else if (position.getPrawns().size() == 1) {
        prawnSelected = position.getPrawns().iterator().next();
        pushState(State.PRAWN);
        try {
          inputEngine.onStateChange(State.BOARD);
        } catch (ClientViewException exception) {
          throw new RuntimeException("Running handler failed.", exception);
        }
      } else if (position.getPrawns().size() > 1) {
        try {
          prawnSelection = itemSelector.createPrawnSelection(position.getPrawns(), player);
        } catch (ClientViewException exception) {
          throw new RuntimeException(exception);
        }
        try {
          drawPrawnSelection(prawnSelection);
        } catch (ClientViewException exception) {
          throw new RuntimeException(exception);
        }
        pushState(State.PRAWN_SELECTION);
        try {
          inputEngine.onStateChange(State.BOARD);
        } catch (ClientViewException exception) {
          throw new RuntimeException("Running handler failed.", exception);
        }
      }
    } else if (positionSelection.getDistinctivePositions().length > 1) {
      onMagnificationOn();
    }
  }

  /**
   * Invoked when a Prawn is selected.
   */
  public void onPrawnSelected() {
    if (getState() != State.PRAWN) {
      throw new RuntimeException("onPrawnSelected() can only be invoked in State PRAWN (found " + getState()
          + ").");
    }
    final Event cityCreationEvent;
    if (prawnSelected.getType() == PrawnView.Type.SETTLERS_UNIT) {
      try {
        cityCreationEvent = prawnSelected.getCityCreationEvent();
      } catch (ClientViewException exception) {
        throw new RuntimeException("This should never happen.");
      }
    } else {
      cityCreationEvent = null;
    }
    if (cityCreationEvent != null) {
      drawCityCreation(prawnSelected);
    } else {
      if (prawnSelected.getItinerary() != null) {
        drawItinerary(prawnSelected.getItinerary());
      }
      drawSelectableSections(prawnSelected, prawnSelected.getItinerary(), false);
    }
  }

  /**
   * Closes the view of the current State, and changes to the previous State.
   */
  private void popStateView() {
    outputEngine.onHighlightingRemoved();
    final State oldState;
    try {
      oldState = popState();
    } catch (ClientViewException exception) {
      throw new RuntimeException(exception);
    }
    try {
      inputEngine.onStateChange(oldState);
    } catch (ClientViewException exception) {
      throw new RuntimeException("Running handler failed.", exception);
    }
  }

  /**
   * Invoked when PRAWN State view is closed.
   */
  public void onPrawnViewClosed() {
    popStateView();
    switch (getState()) {
      case CITY:
        try {
          redrawCity(citySelected);
        } catch (ClientViewException exception) {
          throw new RuntimeException(exception);
        }
        break;
      case PRAWN_SELECTION:
        try {
          drawPrawnSelection(prawnSelection);
        } catch (ClientViewException exception) {
          throw new RuntimeException(exception);
        }
        break;
      default:
        LOGGER.warning("onPrawnViewClosed(): Unexpected State " + getState());
        break;
    }
  }

  /**
   * Invoked when Prawn Itinerary is cleared.
   */
  public void onPrawnItineraryCleared() {
    if (getState() != State.PRAWN) {
      throw new RuntimeException("Prawn Itinerary can only be cleared in State PRAWN (found " + getState() + ").");
    }
    if (prawnSelected.getType() == PrawnView.Type.SETTLERS_UNIT) {
      removeOldCityCreationEventIfPresent();
    }
    prawnSelected.clearItinerary();
    try {
      eventsGenerated.add(eventFactory.createClearPrawnItineraryEvent(prawnSelected));
    } catch (EventProcessingException exception) {
      throw new RuntimeException("CLEAR_PRAWN_ITINERARY Event generation failed.");
    }
    popStateView();
    if (getState() == State.CITY) {
      try {
        redrawCity(citySelected);
      } catch (ClientViewException exception) {
        throw new RuntimeException(exception);
      }
    }
  }

  /**
   * Starts building Prawn Itinerary.
   */
  private void startPrawnItineraryBuild() {
    if (getState() != State.PRAWN) {
      throw new RuntimeException("Prawn Itinerary build can only be started in State PRAWN (found " + getState()
          + ").");
    }
    itineraryBuilder = new ItineraryView.Builder(prawnSelected.getCurrentPosition());
    outputEngine.onHighlightingRemoved();
    pushState(State.ITINERARY_BUILT);
    try {
      inputEngine.onStateChange(State.PRAWN);
    } catch (ClientViewException exception) {
      throw new RuntimeException("Running handler failed.", exception);
    }
    drawSelectableSections(prawnSelected, null, false);
  }

  /**
   * Makes method startPrawnItineraryBuild() available to legacy users.
   * @deprecated building Itinerary should now be triggered with onPrawnItineraryBuildProgress()
   */
  public void legacyOnPrawnItineraryBuildStart() {
    startPrawnItineraryBuild();
  }

  /**
   * Invoked when a Point is selected when building a Prawn Itinerary .
   * @param point the Point selected
   */
  public void onPrawnItineraryBuildProgress(@Nonnull Point2D.Double point) {
    if (getState() != State.ITINERARY_BUILT && getState() != State.PRAWN) {
      throw new RuntimeException("Prawn Itinerary build can only be performed in States ITINERARY_BUILT or PRAWN "
          + "(found " + getState() + ").");
    }
    pointSelected = point;
    positionSelection = itemSelector.selectPositions(point, player, ItemSelector.LookupType.POSITION_SELECTION);
    try {
      itemSelector.adjust(positionSelection);
    } catch (ClientViewException exception) {
      throw new RuntimeException("Adjusting a PositionSelection failed.", exception);
    }
    if (!positionSelection.getAllPositions().isEmpty()) {
      if (getState().equals(State.PRAWN)) {
        startPrawnItineraryBuild();
      }
      if (positionSelection.getAllPositions().size() == 1) {
        final PositionView node = positionSelection.getAllPositions().iterator().next();
        tryAddPrawnItineraryBuildNode(node);
      } else {
        onMagnificationOn();
      }
    }
  }

  /**
   * Invoked when building Prawn Itinerary is resumed.
   */
  public void onPrawnItineraryBuildResumed() {
    if (getState() != State.ITINERARY_BUILT) {
      throw new RuntimeException("Prawn Itinerary build can only be resumed in State ITINERARY_BUILT (found "
          + getState() + ").");
    }
    outputEngine.onHighlightingRemoved();
    final ItineraryView partialItinerary = itineraryBuilder.build();
    if (partialItinerary != null) {
      drawItinerary(partialItinerary);
    }
    drawSelectableSections(prawnSelected, partialItinerary, true);
  }

  /**
   * Tries to add a Node to the Itinerary being built.
   * @param node the node to be added
   * @return true iff the operation succeeded
   */
  boolean tryAddPrawnItineraryBuildNode(@Nonnull PositionView node) {
    if (getState() != State.ITINERARY_BUILT) {
      throw new RuntimeException("Prawn Itinerary build can only be performed in State ITINERARY_BUILT (found "
          + getState() + ").");
    }
    try {
      itineraryBuilder.addNode(node);
    } catch (ClientViewException exception) {
      throw new RuntimeException("Adding Node to the Itinerary failed.", exception);
    }
    if (itineraryBuilder.isLastActionSuccessful()) {
      drawItinerary(itineraryBuilder.build());
      drawSelectableSections(prawnSelected, itineraryBuilder.build(), true);
      return true;
    }
    return false;
  }

  /**
   * Invoked when the Itinerary being built is confirmed.
   */
  public void onPrawnItineraryBuildConfirmed() {
    if (getState() != State.ITINERARY_BUILT) {
      throw new RuntimeException("Prawn Itinerary build can only be confirmed in State ITINERARY_BUILT (found "
          + getState() + ").");
    }
    final ItineraryView newItinerary = itineraryBuilder.build();
    if (newItinerary == null) {
      LOGGER.warning("No Itinerary set. Confirmation ignored.");
      return;
    }
    if (prawnSelected.getType() == PrawnView.Type.SETTLERS_UNIT) {
      removeOldCityCreationEventIfPresent();
    }
    prawnSelected.setItinerary(newItinerary);
    try {
      eventsGenerated.add(eventFactory.createSetPrawnItineraryEvent(prawnSelected, prawnSelected.getItinerary()));
    } catch (EventProcessingException exception) {
      throw new RuntimeException("SET_PRAWN_ITINERARY Event generation failed.", exception);
    }
    popStateView();
  }

  /**
   * Invoked when the Itinerary being built is abandoned.
   */
  public void onPrawnItineraryBuildAbandoned() {
    if (getState() != State.ITINERARY_BUILT) {
      throw new RuntimeException("Prawn Itinerary build can only be abandoned in State ITINERARY_BUILT (found "
          + getState() + ").");
    }
    popStateView();
  }

  /**
   * Invoked when building a City is initiated.
   */
  public void onCityCreationStarted() {
    if (getState() != State.PRAWN) {
      throw new IllegalStateException("Building a City can only be initialized in State PRAWN (found " + getState()
          + ").");
    }
    if (prawnSelected.getType() != PrawnView.Type.SETTLERS_UNIT) {
      throw new IllegalArgumentException("Prawn to build a City must be a SettlersUnit: " + prawnSelected);
    }
    pushState(State.CITY_CREATION);
    try {
      inputEngine.onStateChange(State.BOARD);
    } catch (ClientViewException exception) {
      throw new RuntimeException("Running handler failed.", exception);
    }
  }

  /**
   * Removes from prawnSelected and eventsGenerated the City creation Event scheduled in this round, or do nothing if
   * the Event cannot be found.
   */
  private void removeOldCityCreationEventIfPresent() {
    if (prawnSelected == null) {
      return;
    }
    final Event oldCityCreationEvent;
    try {
      oldCityCreationEvent = prawnSelected.getCityCreationEvent();
    } catch (ClientViewException exception) {
      throw new RuntimeException("Cannot get old ORDER_CITY_CREATION Event from " + prawnSelected, exception);
    }
    if (oldCityCreationEvent != null) {
      try {
        prawnSelected.setCityCreationEvent(null);
      } catch (ClientViewException exception) {
        throw new RuntimeException("Should never happen.", exception);
      }
      eventsGenerated.remove(oldCityCreationEvent);
    }
  }

  /**
   * Invoked when building a City is confirmed.
   * @param name the name of the City to be built
   */
  public void onCityCreationConfirmed(@Nonnull String name) {
    if (getState() != State.CITY_CREATION) {
      throw new RuntimeException("Building a City can only be confirmed in State CITY_CREATION (found "
          + getState() + ").");
    }
    final Event cityCreationEvent;
    try {
      cityCreationEvent = eventFactory.createOrderCityCreationEvent(prawnSelected, name);
    } catch (EventProcessingException exception) {
      throw new RuntimeException("ORDER_CITY_CREATION Event generation failed.", exception);
    }
    removeOldCityCreationEventIfPresent();
    try {
      prawnSelected.setCityCreationEvent(cityCreationEvent);
    } catch (ClientViewException exception) {
      throw new RuntimeException("Setting ORDER_CITY_CREATION Event failed.", exception);
    }
    eventsGenerated.add(cityCreationEvent);
    popStateView();
  }

  /**
   * Invoked when building a City is abandoned.
   */
  public void onCityCreationAbandoned() {
    if (getState() != State.CITY_CREATION) {
      throw new RuntimeException("City creation can only be abandoned in State CITY_CREATION (found "
          + getState() + ").");
    }
    popStateView();
  }

  /**
   * Invoked when the UI enters State MAGNIFIED .
   */
  private void onMagnificationOn() {
    final State oldState = getState();
    pushState(State.MAGNIFIED);
    outputEngine.onMagnificationShown(pointSelected);

    // Draw DistinctivePositions .
    final DistinctivePositionView[] distinctivePositions = positionSelection.getDistinctivePositions();
    final Set<DistinctivePositionView> citiesToDraw = new HashSet<>();
    final Set<DistinctivePositionView> prawnsToDraw = new HashSet<>();
    int maxPrawnIndex = 0;
    final Set<DistinctivePositionView> positionsToDraw = new HashSet<>();
    for (DistinctivePositionView distinctivePosition : distinctivePositions) {
      final PositionView position = distinctivePosition.getPosition();
      final JointView joint = position.getJoint();
      if (joint != null && joint.getCity() != null) {
        citiesToDraw.add(distinctivePosition);
      } else if (!position.getPrawns().isEmpty()) {
        prawnsToDraw.add(distinctivePosition);
        final int prawnCount = distinctivePosition.getPosition().getPrawns().size();
        if (prawnCount > maxPrawnIndex) {
          maxPrawnIndex = prawnCount;
        }
      } else {
        positionsToDraw.add(distinctivePosition);
      }
    }
    // Draw Prawns .
    for (int i = maxPrawnIndex; i >= 0; --i) {
      for (DistinctivePositionView distinctivePosition : prawnsToDraw) {
        final Set<PrawnView> prawnSet = distinctivePosition.getPosition().getPrawns();
        if (i < prawnSet.size()) {
          Iterator<PrawnView> iter = prawnSet.iterator();
          for (int j = 0; j < i; ++j) {
            iter.next();
          }
          final PrawnView prawn = iter.next();
          try {
            outputEngine.onMagnifiedPrawnDrawn(prawn, distinctivePosition.getCoordinatesPair(), i);
          } catch (ClientViewException exception) {
            throw new RuntimeException(exception);
          }
        }
      }
    }
    // Draw Positions .
    for (DistinctivePositionView distinctivePosition : positionsToDraw) {
      try {
        outputEngine.onMagnifiedPositionDrawn(distinctivePosition.getPosition(),
            distinctivePosition.getCoordinatesPair());
      } catch (ClientViewException exception) {
        throw new RuntimeException(exception);
      }
    }
    // Draw Cities .
    for (DistinctivePositionView distinctivePosition : citiesToDraw) {
      final CityView city = distinctivePosition.getPosition().getJoint().getCity();
      try {
        outputEngine.onMagnifiedCityDrawn(city, distinctivePosition.getCoordinatesPair());
      } catch (ClientViewException exception) {
        throw new RuntimeException(exception);
      }
    }

    try {
      inputEngine.onStateChange(oldState);
    } catch (ClientViewException exception) {
      throw new RuntimeException("Running handler failed.", exception);
    }
  }

  /**
   * Invoked when a Point is selected in State MAGNIFIED .
   * @param point the Point selected
   */
  public void onMagnifiedPointSelected(@Nonnull Point2D.Double point) {
    if (getState() != State.MAGNIFIED) {
      throw new RuntimeException("State should be equal to MAGNIFIED (found " + getState() + ").");
    }
    final PositionView position;
    try {
      position = itemSelector.selectPosition(point, positionSelection);
    } catch (ClientViewException exception) {
      throw new RuntimeException(exception);
    }
    if (position == null) {
      return;
    }
    // TODO(najtmar): Consider not popping the State here, but instead handle State changing within the switch .
    try {
      popState();
    } catch (ClientViewException exception) {
      throw new IllegalStateException("Popping the State failed.", exception);
    }
    boolean operationSucceeded = true;
    switch (getState()) {
      case ITINERARY_BUILT: {
        operationSucceeded = tryAddPrawnItineraryBuildNode(position);
        break;
      }
      case BOARD: {
        final JointView joint = position.getJoint();
        if (joint != null && joint.getCity() != null) {
          citySelected = joint.getCity();
          try {
            prawnSelection = itemSelector.createPrawnSelection(joint.getNormalizedPosition().getPrawns(),
                citySelected.getPlayer());
          } catch (ClientViewException exception) {
            throw new RuntimeException(exception);
          }
          onCityViewShown();
        } else if (!position.getPrawns().isEmpty()) {
          if (position.getPrawns().size() == 1) {
            prawnSelected = position.getPrawns().iterator().next();
            pushState(State.PRAWN);
          } else {
            try {
              prawnSelection = itemSelector.createPrawnSelection(position.getPrawns(), player);
            } catch (ClientViewException exception) {
              throw new RuntimeException(exception);
            }
            try {
              drawPrawnSelection(prawnSelection);
            } catch (ClientViewException exception) {
              throw new RuntimeException(exception);
            }
            pushState(State.PRAWN_SELECTION);
            try {
              inputEngine.onStateChange(State.BOARD);
            } catch (ClientViewException exception) {
              throw new RuntimeException("Running handler failed.", exception);
            }
          }
        }
        break;
      }
      default: {
        // Just ignore.
        break;
      }
    }
    if (operationSucceeded) {
      outputEngine.onMagnificationHidden();
      outputEngine.onHighlightingRemoved();
      try {
        inputEngine.onStateChange(State.MAGNIFIED);
      } catch (ClientViewException exception) {
        throw new RuntimeException("Running handler failed.", exception);
      }
    } else {
      pushState(State.MAGNIFIED);
    }
  }

  /**
   * Invoked when the UI leaves State MAGNIFIED .
   */
  public void onMagnificationOff() {
    if (getState() != State.MAGNIFIED) {
      throw new RuntimeException("State should be equal to MAGNIFIED (found " + getState() + ").");
    }
    outputEngine.onMagnificationHidden();
    popStateView();
    /*
    try {
      inputEngine.onStateChange(State.MAGNIFIED);
    } catch (ClientViewException exception) {
      throw new RuntimeException("Running handler failed.", exception);
    }
    */

    // If the itinerary built is empty, leave the State ITINERARY_BUILT .
    if (getState().equals(State.ITINERARY_BUILT) && itineraryBuilder.build() == null) {
      popStateView();
    }
  }

  /**
   * Invoked when a Point is selected in State PRAWN_SELECTION .
   * @param point the Point selected
   */
  public void onPrawnSelectionPointSelected(@Nonnull Point2D.Double point) {
    if (getState() != State.PRAWN_SELECTION) {
      throw new RuntimeException("State should be equal to PRAWN_SELECTION (found " + getState() + ").");
    }
    final int oldStartPosition = prawnSelection.getStartPosition();
    try {
      prawnSelected = itemSelector.selectPrawnOrProcessPrawnSelection(point, prawnSelection);
    } catch (ClientViewException exception) {
      throw new RuntimeException("Point selection failed.", exception);
    }
    if (prawnSelected != null) {
      outputEngine.onPrawnSelectionHidden();
      pushState(State.PRAWN);
      try {
        inputEngine.onStateChange(State.PRAWN_SELECTION);
      } catch (ClientViewException exception) {
        throw new RuntimeException("Running handler failed.", exception);
      }
    } else if (prawnSelection.getStartPosition() != oldStartPosition) {
      outputEngine.onPrawnSelectionHidden();
      try {
        drawPrawnSelection(prawnSelection);
      } catch (ClientViewException exception) {
        throw new RuntimeException(exception);
      }
    }
  }

  /**
   * Invoked when the UI leaves State PRAWN_SELECTION .
   */
  public void onPrawnSelectionOff() {
    if (getState() != State.PRAWN_SELECTION) {
      throw new RuntimeException("State should be equal to PRAWN_SELECTION (found " + getState() + ").");
    }
    outputEngine.onPrawnSelectionHidden();
    popStateView();
    try {
      inputEngine.onStateChange(State.PRAWN_SELECTION);
    } catch (ClientViewException exception) {
      throw new RuntimeException("Running handler failed.", exception);
    }
  }

  /**
   * Invoked when the UI leaves State CITY .
   */
  public void onCityViewOff() {
    if (getState() != State.CITY) {
      throw new RuntimeException("State should be equal to CITY (found " + getState() + ").");
    }
    outputEngine.onCityViewHidden();
    popStateView();
    try {
      inputEngine.onStateChange(State.CITY);
    } catch (ClientViewException exception) {
      throw new RuntimeException("Running handler failed.", exception);
    }
  }

  /**
   * Calculates the values of properties of citySelected: selectedCityCapturedHalves, selectedCityProductivity, and
   * selectedCityPropertiesCalculationTime .
   * @throws ClientViewException when calculating City properties fails
   */
  private void calculateCitySelectedProperties() throws ClientViewException {
    final List<RoadSectionView> roadSections = citySelected.getJoint().getRoadSections();
    selectedCityCapturedHalves = new int[roadSections.size()];
    int productivityPoints = 0;
    for (int i = 0; i < roadSections.size(); ++i) {
      selectedCityCapturedHalves[i] = game.calculateCapturedRoadSectionHalvesCount(roadSections.get(i), citySelected);
      productivityPoints += 2 - selectedCityCapturedHalves[i];
    }
    selectedCityProductivity = ((double) productivityPoints) / (2 * roadSections.size());
  }

  /**
   * Draws a City with CityFactories it contains.
   * @param city the City to draw
   * @throws ClientViewException when drawing the City fails
   */
  private void redrawCity(@Nonnull CityView city) throws ClientViewException {
    outputEngine.onCityViewShown();

    // Drawn City box.
    calculateCitySelectedProperties();
    outputEngine.onCityBoxDrawn(citySelected, selectedCityCapturedHalves, selectedCityProductivity);

    // Draw Prawns in the City .
    outputEngine.onCityPrawnSelectionShown();
    final PrawnSelection.PrawnBox[][] prawnBoxes = prawnSelection.getPrawnBoxes();
    for (int j = 0; j < prawnSelection.getPrawnSelectionHeight(); ++j) {
      for (int i = 0; i < prawnSelection.getPrawnSelectionWidth(); ++i) {
        final Point2D.Double prawnBoxPoint = new Point2D.Double(
            prawnBoxes[i][j].getCoordX() + prawnSelection.getPrawnBoxSide() / 2.0,
            prawnBoxes[i][j].getCoordY() + prawnSelection.getPrawnBoxSide() / 2.0);
        switch (prawnBoxes[i][j].getType()) {
          case EMPTY: {
            // TODO(najtmar): Implement this.
            break;
          }
          case PRAWN: {
            outputEngine.onCityPrawnSelectionPrawnDrawn(prawnBoxes[i][j].getPrawn(), prawnBoxPoint);
            break;
          }
          case LEFT_SLIDER: {
            outputEngine.onCityPrawnSelectionLeftSlider(prawnBoxPoint, prawnSelection.getStartPosition());
            break;
          }
          case RIGHT_SLIDER: {
            outputEngine.onCityPrawnSelectionRightSlider(prawnBoxPoint, prawnSelection.getNextCount());
            break;
          }
          default: {
            throw new RuntimeException("Unknown PrawnBoxType: " + prawnBoxes[i][j].getType());
          }
        }
      }
    }

    // Draw CityFactories .
    int factoryBoxIndex = 0;
    int factoryIndex = city.getFirstFactoryShownIndex();
    if (factoryIndex > 0) {
      outputEngine.onCityFactoryUpperSliderShown(factoryBoxIndex++);
    }
    while (factoryBoxIndex < cityFactoryBoxMaxCount && factoryIndex < city.getFactories().size()) {
      if (factoryBoxIndex < cityFactoryBoxMaxCount - 1 || factoryIndex == city.getFactories().size() - 1) {
        outputEngine.onCityFactoryShown(city.getFactories().get(factoryIndex++), factoryBoxIndex);
      } else {
        outputEngine.onCityFactoryLowerSliderShown(factoryBoxIndex);
      }
      ++factoryBoxIndex;
    }
  }

  /**
   * Invoked when the UI enters State CITY .
   */
  public void onCityViewShown() {
    final State oldState = getState();
    pushState(State.CITY);
    try {
      redrawCity(citySelected);
    } catch (ClientViewException exception) {
      throw new RuntimeException(exception);
    }
    try {
      inputEngine.onStateChange(oldState);
    } catch (ClientViewException exception) {
      throw new RuntimeException("Running handler failed.", exception);
    }
  }

  /**
   * Invoked when a point on a CityFactory in the City view is selected.
   * @param point the Point selected
   */
  public void onCityFactoryPointSelected(@Nonnull Point2D.Double point) {
    if (getState() != State.CITY) {
      throw new RuntimeException("State should be equal to CITY (found " + getState() + ").");
    }
    // TODO(najtmar): Consider moving the index calculating part to class ItemSelector .
    if (cityFactoryBoxWidth <= point.getX()
        || cityFactoryBoxHeight * cityFactoryBoxMaxCount <= point.getY()) {
      throw new RuntimeException("" + point + " does not fit in the box (" + cityFactoryBoxWidth + ", "
          + (cityFactoryBoxHeight * cityFactoryBoxMaxCount) + ").");
    }
    factoryBoxIndexSelected = (int) (point.getY() / cityFactoryBoxHeight);
    if (factoryBoxIndexSelected < citySelected.getFactories().size()) {
      if (factoryBoxIndexSelected == 0 && cityFactoryBoxMaxCount < citySelected.getFactories().size()
          && 0 < citySelected.getFirstFactoryShownIndex()) {
        // TODO(najtmar): Support upper slider.
      } else  if (factoryBoxIndexSelected == cityFactoryBoxMaxCount - 1
          && cityFactoryBoxMaxCount < citySelected.getFactories().size()
          && citySelected.getFirstFactoryShownIndex() + cityFactoryBoxMaxCount - 1
              < citySelected.getFactories().size()) {
        // TODO(najtmar): Support upper slider.
      } else {
        final int factoryIndex;
        if (citySelected.getFirstFactoryShownIndex() == 0) {
          factoryIndex = factoryBoxIndexSelected;
        } else {
          factoryIndex = factoryBoxIndexSelected + citySelected.getFirstFactoryShownIndex() - 1;
        }
        factorySelected = citySelected.getFactories().get(factoryIndex);
        pushState(State.FACTORY);
        outputEngine.onCityFactoryHighlighted(factoryBoxIndexSelected);
        try {
          inputEngine.onStateChange(State.CITY);
        } catch (ClientViewException exception) {
          throw new RuntimeException("Running handler failed.", exception);
        }
      }
    }
  }

  /**
   * Invoked when the Factory is set to produce a SettlersUnit .
   */
  public void onSetProduceSettlersUnit() {
    if (getState() != State.FACTORY) {
      throw new RuntimeException("SettlersUnit production can only be set in State FACTORY (found " + getState()
      + ").");
    }
    try {
      factorySelected.setAction(ActionType.PRODUCE_SETTLERS_UNIT, null, -1.0);
    } catch (ClientViewException exception) {
      throw new RuntimeException("Setting CityFactory action failed.", exception);
    }
    try {
      eventsGenerated.add(eventFactory.createSetProduceSettlersUnitActionEvent(factorySelected));
    } catch (EventProcessingException exception) {
      throw new RuntimeException("SET_PRODUCE_SETTLERS_UNIT_ACTION Event generation failed.");
    }
    try {
      redrawCity(citySelected);
    } catch (ClientViewException exception) {
      throw new RuntimeException(exception);
    }
    popStateView();
  }

  /**
   * Invoked when the Factory is set to produce a Warrior .
   * @param warriorDirection the Warrior direction of the Warrior to be built
   */
  public void onSetProduceWarrior(@Nonnegative double warriorDirection) {
    if (getState() != State.WARRIOR_DIRECTION) {
      throw new RuntimeException("Warrior production can only be set in State WARRIOR_DIRECTION (found " + getState()
      + ").");
    }
    try {
      factorySelected.setAction(ActionType.PRODUCE_WARRIOR, null, warriorDirection);
    } catch (ClientViewException exception) {
      throw new RuntimeException("Setting CityFactory action failed.", exception);
    }
    try {
      eventsGenerated.add(eventFactory.createSetProduceWarriorActionEvent(factorySelected, warriorDirection));
    } catch (EventProcessingException exception) {
      throw new RuntimeException("SET_PRODUCE_WARRIOR_ACTION Event generation failed.");
    }
    outputEngine.onWarriorDirectionSelectionHidden();
    try {
      redrawCity(citySelected);
    } catch (ClientViewException exception) {
      throw new RuntimeException(exception);
    }
    popStateView();
    popStateView();
  }

  /**
   * Invoked when the Factory is set to grow the City .
   */
  public void onSetGrowCity() {
    if (getState() != State.FACTORY) {
      throw new RuntimeException("Growing the City can only be set in State FACTORY (found " + getState()
      + ").");
    }
    try {
      factorySelected.setAction(ActionType.GROW, null, -1.0);
    } catch (ClientViewException exception) {
      throw new RuntimeException("Setting CityFactory action failed.", exception);
    }
    try {
      eventsGenerated.add(eventFactory.createSetGrowCityActionEvent(factorySelected));
    } catch (EventProcessingException exception) {
      throw new RuntimeException("SET_GROW_CITY_ACTION Event generation failed.");
    }
    try {
      redrawCity(citySelected);
    } catch (ClientViewException exception) {
      throw new RuntimeException(exception);
    }
    popStateView();
  }

  /**
   * Invoked when the Factory is set to repair prawnSelected .
   */
  public void onSetRepairPrawn() {
    if (getState() != State.PRAWN_REPAIR) {
      throw new RuntimeException("Repairing a Prawn can only be set in State PRAWN_REPAIR (found " + getState()
      + ").");
    }
    try {
      for (FactoryView factory : citySelected.getFactories()) {
        if (!factory.equals(factorySelected) && factory.getActionType() != null
            && factory.getActionType().equals(ActionType.REPAIR_PRAWN)
            && factory.getPrawnRepaired().equals(prawnSelected)) {
          LOGGER.info("" + prawnSelected + " already being repaired. Ignoring.");
          return;
        }
      }
      factorySelected.setAction(ActionType.REPAIR_PRAWN, prawnSelected, -1.0);
    } catch (ClientViewException exception) {
      throw new RuntimeException("Setting CityFactory action failed.", exception);
    }
    try {
      eventsGenerated.add(eventFactory.createSetRepairPrawnActionEvent(factorySelected, prawnSelected));
    } catch (EventProcessingException exception) {
      throw new RuntimeException("SET_REPAIR_PRAWN_ACTION Event generation failed.");
    }
    try {
      redrawCity(citySelected);
    } catch (ClientViewException exception) {
      throw new RuntimeException(exception);
    }
    onPrawnRepairSelectionOff();
    popStateView();
  }

  /**
   * Invoked when the UI leaves State FACTORY .
   */
  public void onCityFactoryViewOff() {
    if (getState() != State.FACTORY) {
      throw new RuntimeException("State should be equal to FACTORY (found " + getState() + ").");
    }
    outputEngine.onHighlightingRemoved();
    popStateView();
    try {
      inputEngine.onStateChange(State.FACTORY);
    } catch (ClientViewException exception) {
      throw new RuntimeException("Running handler failed.", exception);
    }
  }

  /**
   * Invoked when a Point is selected in State CITY .
   * @param point the Point selected
   */
  public void onCityPrawnSelectionPointSelected(@Nonnull Point2D.Double point) {
    if (getState() != State.CITY) {
      throw new RuntimeException("State should be equal to CITY (found " + getState() + ").");
    }
    final int oldStartPosition = prawnSelection.getStartPosition();
    try {
      prawnSelected = itemSelector.selectPrawnOrProcessPrawnSelection(point, prawnSelection);
    } catch (ClientViewException exception) {
      throw new RuntimeException("Point selection failed.", exception);
    }
    if (prawnSelected != null) {
      outputEngine.onCityViewHidden();
      pushState(State.PRAWN);
      try {
        inputEngine.onStateChange(State.CITY);
      } catch (ClientViewException exception) {
        throw new RuntimeException("Running handler failed.", exception);
      }
    } else if (prawnSelection.getStartPosition() != oldStartPosition) {
      try {
        redrawCity(citySelected);
      } catch (ClientViewException exception) {
        throw new RuntimeException(exception);
      }
    }
  }

  /**
   * Invoked when the UI enters State WARRIOR_DIRECTION .
   */
  public void onWarriorDirectionSelectionShown() {
    final State oldState = getState();
    pushState(State.WARRIOR_DIRECTION);
    outputEngine.onWarriorDirectionSelectionShown();
    try {
      inputEngine.onStateChange(oldState);
    } catch (ClientViewException exception) {
      throw new RuntimeException("Running handler failed.", exception);
    }
  }

  /**
   * Invoked when the UI leaves State WARRIOR_DIRECTION .
   */
  public void onWarriorDirectionSelectionOff() {
    if (getState() != State.WARRIOR_DIRECTION) {
      throw new RuntimeException("State should be equal to WARRIOR_DIRECTION (found " + getState() + ").");
    }
    outputEngine.onWarriorDirectionSelectionHidden();
    popStateView();
    outputEngine.onCityFactoryHighlighted(factoryBoxIndexSelected);
    try {
      inputEngine.onStateChange(State.WARRIOR_DIRECTION);
    } catch (ClientViewException exception) {
      throw new RuntimeException("Running handler failed.", exception);
    }
  }

  /**
   * Invoked when the UI enters State GAME_ABANDONING .
   */
  public void onGameAbandoningPromptShown() {
    final State oldState = getState();
    pushState(State.GAME_ABANDONING);
    outputEngine.onGameAbandoningPromptShown();
    try {
      inputEngine.onStateChange(oldState);
    } catch (ClientViewException exception) {
      throw new RuntimeException("Running handler failed.", exception);
    }
  }

  /**
   * Invoked when the UI leaves State GAME_ABANDONING .
   */
  public void onGameAbandoningPromptOff() {
    if (getState() != State.GAME_ABANDONING) {
      throw new RuntimeException("State should be equal to GAME_ABANDONING (found " + getState() + ").");
    }
    outputEngine.onGameAbandoningPromptHidden();
    popStateView();
    try {
      inputEngine.onStateChange(State.GAME_ABANDONING);
    } catch (ClientViewException exception) {
      throw new RuntimeException("Running handler failed.", exception);
    }
  }

  /**
   * Invoked when the Player abandons the Game .
   */
  public void onGameAbandoned() {
    if (getState() != State.GAME_ABANDONING) {
      throw new RuntimeException("State should be equal to GAME_ABANDONING (found " + getState() + ").");
    }

    // Set the State of the Game .
    player.setState(PlayerView.State.LOST);
    final long playersInGameCount =
        StreamSupport.stream(Arrays.asList(game.getPlayers())).filter(player -> player.getState()
            .equals(PlayerView.State.IN_GAME)).count();
    if (playersInGameCount == 1) {
      for (PlayerView gamePlayer : game.getPlayers()) {
        if (gamePlayer.getState() == PlayerView.State.IN_GAME) {
          gamePlayer.setState(PlayerView.State.WON);
        }
      }
    }

    try {
      eventsGenerated.add(eventFactory.createAbandonPlayerEvent(player));
    } catch (EventProcessingException exception) {
      throw new RuntimeException("ABANDON_PLAYER Event generation failed.");
    }
    popStateView();
    outputEngine.onGameAbandoned();
  }

  /**
   * Invoked when the Player force quits the Game.
   */
  public void onGameForceQuit() {
    if (getState() == State.NO_GAME) {
      throw new RuntimeException("Game cannot be force quit as it is in State NO_GAME.");
    }
    // Set the State of the Game .
    player.setState(PlayerView.State.UNDECIDED);
    for (PlayerView gamePlayer : game.getPlayers()) {
      if (player.getState() == PlayerView.State.IN_GAME) {
        gamePlayer.setState(PlayerView.State.UNDECIDED);
      }
    }

    // If the interface is in State INITIALIZE (i.e, waiting for user interaction), go to State NO_GAME .
    if (getState() == State.INITIALIZED) {
      popStateView();
    } else {
      while (getState() != State.INITIALIZED) {
        popStateView();
      }
    }
    outputEngine.onGameForceQuit();
  }

  /**
   * Invoked when the UI enters State PRAWN_REPAIR .
   */
  public void onPrawnRepairSelectionShown() {
    prawnSelected = null;
    final State oldState = getState();
    pushState(State.PRAWN_REPAIR);
    outputEngine.onPrawnRepairSelectionShown();
    try {
      inputEngine.onStateChange(oldState);
    } catch (ClientViewException exception) {
      throw new RuntimeException("Running handler failed.", exception);
    }
  }

  /**
   * Invoked when a Point is selected in State PRAWN_REPAIR .
   * @param point the Point selected
   */
  public void onPrawnRepairSelectionPointSelected(@Nonnull Point2D.Double point) {
    if (getState() != State.PRAWN_REPAIR) {
      throw new RuntimeException("State should be equal to PRAWN_REPAIR (found " + getState() + ").");
    }
    try {
      prawnSelected = itemSelector.selectPrawnOrProcessPrawnSelection(point, prawnSelection);
    } catch (ClientViewException exception) {
      throw new RuntimeException("Point selection failed.", exception);
    }
    if (prawnSelected != null) {
      outputEngine.onHighlightingRemoved();
      final Point prawnCoordinatesPair;
      try {
        prawnCoordinatesPair = prawnSelection.getPrawnBoxIndexes(prawnSelected);
      } catch (ClientViewException exception) {
        throw new RuntimeException("This should never happen.", exception);
      }
      outputEngine.onPrawnRepairSelectionHighlighted(
          (int) prawnCoordinatesPair.getX(), (int) prawnCoordinatesPair.getY());
    }
  }

  /**
   * Invoked when the UI leaves State PRAWN_REPAIR .
   */
  public void onPrawnRepairSelectionOff() {
    if (getState() != State.PRAWN_REPAIR) {
      throw new RuntimeException("State should be equal to PRAWN_REPAIR (found " + getState() + ").");
    }
    outputEngine.onPrawnRepairSelectionHidden();
    popStateView();
    outputEngine.onCityFactoryHighlighted(factoryBoxIndexSelected);
    try {
      inputEngine.onStateChange(State.PRAWN_REPAIR);
    } catch (ClientViewException exception) {
      throw new RuntimeException("Running handler failed.", exception);
    }
  }

  /**
   * Notifies the User that own connection to the Game server has changed.
   * @param connected whether the Connection is established and working
   * @throws ClientViewException if the notification process fails
   */
  public void notifyOwnGameServerConnection(boolean connected) throws ClientViewException {
    outputEngine.notifyOwnGameServerConnection(connected);
  }

  /**
   * Notifies the User that other Game clients' connections to the Game server have changed.
   * @param connected whether the Connection of all other Game clients is established and working
   * @throws ClientViewException if the notification process fails
   */
  public void notifyOthersGameServerConnection(boolean connected) throws ClientViewException {
    outputEngine.notifyOthersGameServerConnection(connected);
  }

  /**
   * Notifies the Client that the Game has been finished with State UNDECIDED .
   * @throws ClientViewException when the notification process fails
   */
  public void notifyGameUndecided() throws ClientViewException {
    outputEngine.notifyGameUndecided();
  }

  /**
   * Resets the instance.
   * @throws ClientViewException when the instance cannot be reset
   */
  public void reset() throws ClientViewException {
    while (!getState().equals(State.NO_GAME)) {
      popState();
    }
  }

  public GameView getGame() {
    return game;
  }

}
