package cz.wie.najtmar.gop.client.ui;

import cz.wie.najtmar.gop.client.view.ClientViewException;
import cz.wie.najtmar.gop.client.view.GameView;
import cz.wie.najtmar.gop.client.view.PlayerView;
import cz.wie.najtmar.gop.event.Event;

import javax.annotation.Nonnull;

/**
 * Any type of UI client working on behalf of a User.
 * @author najtmar
 */
public interface Session {

  /**
   * Defines Factory class for classes inherited from UiSession .
   * @author najtmar
   *
   * @param <T> the class whose instances should be produced
   */
  public static interface Factory<T extends Session> {

    public T create(@Nonnull GameView readOnlyGame);

  }

  /**
   * Initializes the Session .
   * @param gameClient the UI Client managing the Game UI
   * @param game the Game with which the Session is initialized
   * @throws ClientViewException when the initialization fails
   */
  public void initialize(@Nonnull Client gameClient, @Nonnull GameView game) throws ClientViewException;

  /**
   * Reads and processes Events. Returns a list of Events produces as the result.
   * @param inputEvents Events to be processed
   * @param player the Player to be notified; important for multi-Player UIs
   * @return Events produced based on inputEvents
   * @throws ClientViewException when Event processing fails
   */
  public Event[] processEvents(@Nonnull Event[] inputEvents, @Nonnull PlayerView player)  throws ClientViewException;

}
