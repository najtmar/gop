package cz.wie.najtmar.gop.client.ui;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import cz.wie.najtmar.gop.client.ui.ItemSelector;
import cz.wie.najtmar.gop.client.view.ClientViewException;
import cz.wie.najtmar.gop.client.view.ClientViewProperties;
import cz.wie.najtmar.gop.client.view.DistinctivePositionView;
import cz.wie.najtmar.gop.client.view.GameView;
import cz.wie.najtmar.gop.client.view.PlayerView;
import cz.wie.najtmar.gop.client.view.PositionSelection;
import cz.wie.najtmar.gop.client.view.PositionView;
import cz.wie.najtmar.gop.client.view.PrawnSelection;
import cz.wie.najtmar.gop.client.view.PrawnSelection.PrawnBoxType;
import cz.wie.najtmar.gop.client.view.PrawnView;
import cz.wie.najtmar.gop.client.view.RoadSectionView;
import cz.wie.najtmar.gop.client.view.UserView;
import cz.wie.najtmar.gop.common.Point2D;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.StringReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.Properties;
import java.util.UUID;

import javax.json.Json;

public class ItemSelectorTest {

  static final double CLICK_DISTANCE = 10.0;
  static final double DISTINCTIVE_POSITION_RADIUS = 2.0;
  static final int PRAWN_BOX_SIZE = 20;

  static Properties SIMPLE_CLIENT_VIEW_PROPERTIES_SET;
  static String GAME_ID_STRING =
      "\"id\": \"00000000-0000-5ba0-0000-000000013435\", "
      + "\"timestamp\": 1234567";
  static String BOARD_PROPERTIES_SERIALIZED_TEMPLATE_STRING =
      "\"properties\": [\n"
      + "  {\n"
      + "    \"key\": \"Board.xsize\",\n"
      + "    \"value\":\"%d\"\n"
      + "  },\n"
      + "  {\n"
      + "    \"key\": \"Board.ysize\",\n"
      + "    \"value\":\"%d\"\n"
      + "  },\n"
      + "  {\n"
      + "    \"key\": \"Board.stepSize\",\n"
      + "    \"value\":\"1.0\"\n"
      + "  }\n"
      + "]";
  static String JOINTS_SERIALIZED_TEMPLATE_STRING =
      "\"joints\": [\n"
      + "  {\n"
      + "    \"index\": 0,\n"
      + "    \"name\": \"joint0\",\n"
      + "    \"x\": %f,\n"
      + "    \"y\": %f\n"
      + "  },\n"
      + "  {\n"
      + "    \"index\": 1,\n"
      + "    \"name\": \"joint1\",\n"
      + "    \"x\": %f,\n"
      + "    \"y\": %f\n"
      + "  },\n"
      + "  {\n"
      + "    \"index\": 2,\n"
      + "    \"name\": \"joint2\",\n"
      + "    \"x\": %f,\n"
      + "    \"y\": %f\n"
      + "  }\n"
      + "]";
  static String ROAD_SECTIONS_SERIALIZED_STRING =
      "\"roadSections\": [\n"
      + "  {\n"
      + "    \"index\": 0,\n"
      + "    \"joint0\": 0,\n"
      + "    \"joint1\": 1\n"
      + "  },\n"
      + "  {\n"
      + "    \"index\": 1,\n"
      + "    \"joint0\": 1,\n"
      + "    \"joint1\": 2\n"
      + "  }\n"
      + "]";
  static String PLAYERS_SERIALIZED_STRING =
      "\"players\": [\n"
      + "  {\n"
      + "    \"index\": 0, "
      + "    \"user\": { "
      + "      \"name\":\"playerA\", "
      + "      \"uuid\": \"00000000-0000-3039-0000-000000010932\" "
      + "    } "
      + "  },\n"
      + "  {\n"
      + "    \"index\": 1, "
      + "    \"user\": { "
      + "      \"name\":\"playerB\", "
      + "      \"uuid\": \"00000000-0001-81cd-0000-00000000a8ca\" "
      + "    } "
      + "  }\n"
      + "]\n";
  static PlayerView PLAYER_A;
  static PlayerView PLAYER_B;

  GameView gameView;
  ItemSelector itemSelector;

  /**
   * Method setUpTestCase().
   * @throws Exception when method execution fails
   */
  @BeforeClass
  public static void setUpTestCase() throws Exception {
    SIMPLE_CLIENT_VIEW_PROPERTIES_SET = new Properties();
    SIMPLE_CLIENT_VIEW_PROPERTIES_SET.load(ItemSelectorTest.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/client/view/simple-client-view.properties"));
    PLAYER_A = new PlayerView(new UserView("playerA", new UUID(12345, 67890)), 0);
    PLAYER_B = new PlayerView(new UserView("playerB", new UUID(98765, 43210)), 1);
  }

  /**
   * Method setUp().
   * @throws Exception when method execution fails
   */
  @Before
  public void setUp() throws Exception {
    gameView = new GameView(
        Json.createReader(
            new StringReader(
                String.format("{ " + GAME_ID_STRING + ", \"board\": {" + BOARD_PROPERTIES_SERIALIZED_TEMPLATE_STRING
                    + ", \"joints\": [], \"roadSections\": []}, " + PLAYERS_SERIALIZED_STRING + "}",
                    150, 100))).readObject(), 0);
    itemSelector = new ItemSelector(gameView, new ClientViewProperties(SIMPLE_CLIENT_VIEW_PROPERTIES_SET));
  }

  @Test
  public void testInstanceCreatedProperly() {
    assertEquals(CLICK_DISTANCE / 2, itemSelector.getCageSideSize(), 0.0);
    assertEquals(DISTINCTIVE_POSITION_RADIUS, itemSelector.getDistinctivePositionRadius(), 0.0);
  }

  @Test
  public void testCageIndex() throws Exception {
    // Cage grid: 3 x 2
    {
      ItemSelector testSelector = new ItemSelector(
          new GameView(
              Json.createReader(
                  new StringReader(
                      String.format("{ " + GAME_ID_STRING + ", \"board\": {"
                          + BOARD_PROPERTIES_SERIALIZED_TEMPLATE_STRING + ", \"joints\": [], \"roadSections\": []}, "
                          + PLAYERS_SERIALIZED_STRING + "}", 15, 10))).readObject(), 0),
          new ClientViewProperties(SIMPLE_CLIENT_VIEW_PROPERTIES_SET));
      assertArrayEquals(new HashSet[][]{
        {new HashSet<>(), new HashSet<>(), },
        {new HashSet<>(), new HashSet<>(), },
        {new HashSet<>(), new HashSet<>(), },
      }, testSelector.getPositionCages());
    }
    {
      ItemSelector testSelector = new ItemSelector(
          new GameView(
              Json.createReader(
                  new StringReader(
                      String.format("{ " + GAME_ID_STRING + ", \"board\": {"
                          + BOARD_PROPERTIES_SERIALIZED_TEMPLATE_STRING + ", \"joints\": [], \"roadSections\": []}, "
                          + PLAYERS_SERIALIZED_STRING + "}", 14, 9))).readObject(), 0),
          new ClientViewProperties(SIMPLE_CLIENT_VIEW_PROPERTIES_SET));
      assertArrayEquals(new HashSet[][]{
        {new HashSet<>(), new HashSet<>(), },
        {new HashSet<>(), new HashSet<>(), },
        {new HashSet<>(), new HashSet<>(), },
      }, testSelector.getPositionCages());
    }
    {
      ItemSelector testSelector = new ItemSelector(
          new GameView(
              Json.createReader(
                  new StringReader(
                      String.format("{ " + GAME_ID_STRING + ", \"board\": {"
                          + BOARD_PROPERTIES_SERIALIZED_TEMPLATE_STRING + ", \"joints\": [], \"roadSections\": []}, "
                          + PLAYERS_SERIALIZED_STRING + "}", 11, 6))).readObject(), 0),
          new ClientViewProperties(SIMPLE_CLIENT_VIEW_PROPERTIES_SET));
      assertArrayEquals(new HashSet[][]{
        {new HashSet<>(), new HashSet<>(), },
        {new HashSet<>(), new HashSet<>(), },
        {new HashSet<>(), new HashSet<>(), },
      }, testSelector.getPositionCages());
    }

    // Cage grid: 2 x 1
    {
      ItemSelector testSelector = new ItemSelector(
          new GameView(
              Json.createReader(
                  new StringReader(
                      String.format("{ " + GAME_ID_STRING + ", \"board\": {"
                          + BOARD_PROPERTIES_SERIALIZED_TEMPLATE_STRING + ", \"joints\": [], \"roadSections\": []}, "
                          + PLAYERS_SERIALIZED_STRING + "}", 10, 5))).readObject(), 0),
          new ClientViewProperties(SIMPLE_CLIENT_VIEW_PROPERTIES_SET));
      assertArrayEquals(new HashSet[][]{
        {new HashSet<>(), },
        {new HashSet<>(), },
      }, testSelector.getPositionCages());
    }

    // Cage grid: 1 x 1
    {
      ItemSelector testSelector = new ItemSelector(
          new GameView(
              Json.createReader(
                  new StringReader(
                      String.format("{ " + GAME_ID_STRING + ", \"board\": {"
                          + BOARD_PROPERTIES_SERIALIZED_TEMPLATE_STRING + ", \"joints\": [], \"roadSections\": []}, "
                          + PLAYERS_SERIALIZED_STRING + "}", 1, 1))).readObject(), 0),
          new ClientViewProperties(SIMPLE_CLIENT_VIEW_PROPERTIES_SET));
      assertArrayEquals(new HashSet[][]{
        {new HashSet<>(), },
      }, testSelector.getPositionCages());
    }
  }

  @Test
  public void testCagePositionAssignment() throws Exception {
    GameView testGame = new GameView(
        Json.createReader(
            new StringReader(
                String.format(Locale.US, "{ " + GAME_ID_STRING + ", \"board\": {"
                    + BOARD_PROPERTIES_SERIALIZED_TEMPLATE_STRING + ", " + JOINTS_SERIALIZED_TEMPLATE_STRING + ", "
                    + ROAD_SECTIONS_SERIALIZED_STRING + "}, " + PLAYERS_SERIALIZED_STRING + "}", 10, 15,
                    3.0, 2.0, 8.0, 2.0, 8.0, 4.0))).readObject(), 0);
    ItemSelector testSelector =
        new ItemSelector(testGame, new ClientViewProperties(SIMPLE_CLIENT_VIEW_PROPERTIES_SET));
    final PositionView[] positions0 = testGame.getRoadSections()[0].getPositions();
    final PositionView[] positions1 = testGame.getRoadSections()[1].getPositions();
    assertArrayEquals(new HashSet[][]{
      {new HashSet<>(Arrays.asList(positions0[0], positions0[1])), new HashSet<>(), new HashSet<>(), },
      {new HashSet<>(Arrays.asList(positions0[2], positions0[3], positions0[4], positions0[5],
          positions1[0], positions1[1], positions1[2])),
        new HashSet<>(), new HashSet<>(), },
    }, testSelector.getPositionCages());
    assertArrayEquals(new HashSet[][]{
      {new HashSet<>(Arrays.asList(positions0[0], positions0[1])), new HashSet<>(), new HashSet<>(), },
      {new HashSet<>(Arrays.asList(positions0[2], positions0[3], positions0[4], positions0[5],
          /* positions1[0], // optional */ positions1[1], positions1[2])),
        new HashSet<>(), new HashSet<>(), },
    }, testSelector.getPositionCages());
    assertArrayEquals(new HashSet[][]{
      {new HashSet<>(Arrays.asList(positions0[0], positions0[1])), new HashSet<>(), new HashSet<>(), },
      {new HashSet<>(Arrays.asList(positions0[2], positions0[3], positions0[4], /* positions0[5], // optional */
          positions1[0], positions1[1], positions1[2])),
        new HashSet<>(), new HashSet<>(), },
    }, testSelector.getPositionCages());
  }

  @Test
  public void testCagePositionAssignment1() throws Exception {
    GameView testGame = new GameView(
        Json.createReader(
            new StringReader(
                String.format(Locale.US, "{ " + GAME_ID_STRING + ", \"board\": {"
                    + BOARD_PROPERTIES_SERIALIZED_TEMPLATE_STRING + ", " + JOINTS_SERIALIZED_TEMPLATE_STRING + ", "
                    + ROAD_SECTIONS_SERIALIZED_STRING + "}, " + PLAYERS_SERIALIZED_STRING + "}", 10, 15,
                    3.0, 3.0, 7.0, 7.0, 9.0, 7.0))).readObject(), 0);
    ItemSelector testSelector =
        new ItemSelector(testGame, new ClientViewProperties(SIMPLE_CLIENT_VIEW_PROPERTIES_SET));
    final PositionView[] positions0 = testGame.getRoadSections()[0].getPositions();
    final PositionView[] positions1 = testGame.getRoadSections()[1].getPositions();
    assertArrayEquals(new HashSet[][]{
      {new HashSet<>(Arrays.asList(positions0[0], positions0[1], positions0[2])), new HashSet<>(), new HashSet<>(), },
      {new HashSet<>(Arrays.asList()), new HashSet<>(Arrays.asList(positions0[3], positions0[4], positions0[5],
          positions1[0], positions1[1], positions1[2])), new HashSet<>(), },
    }, testSelector.getPositionCages());
  }

  @Test
  public void testRelevantCagesFound() throws Exception {
    GameView testGame = new GameView(
        Json.createReader(
            new StringReader(
                String.format(Locale.US,
                    "{ " + GAME_ID_STRING + ", \"board\": {" + BOARD_PROPERTIES_SERIALIZED_TEMPLATE_STRING
                    + ", "
                    + "\"joints\": [ "
                    + "  { "
                    + "    \"index\": 0, "
                    + "    \"name\": \"joint0\", "
                    + "    \"x\": 0.0, "
                    + "    \"y\": 0.0 "
                    + "  }, "
                    + "  { "
                    + "    \"index\": 1, "
                    + "    \"name\": \"joint1\", "
                    + "    \"x\": 5.0, "
                    + "    \"y\": 35.0 "
                    + "  }, "
                    + "  { "
                    + "    \"index\": 2, "
                    + "    \"name\": \"joint2\", "
                    + "    \"x\": 10.0, "
                    + "    \"y\": 0.0 "
                    + "  }, "
                    + "  { "
                    + "    \"index\": 3, "
                    + "    \"name\": \"joint3\", "
                    + "    \"x\": 15.0, "
                    + "    \"y\": 35.0 "
                    + "  }, "
                    + "  { "
                    + "    \"index\": 4, "
                    + "    \"name\": \"joint4\", "
                    + "    \"x\": 20.0, "
                    + "    \"y\": 0.0 "
                    + "  }, "
                    + "  { "
                    + "    \"index\": 5, "
                    + "    \"name\": \"joint5\", "
                    + "    \"x\": 25.0, "
                    + "    \"y\": 35.0 "
                    + "  }, "
                    + "  { "
                    + "    \"index\": 6, "
                    + "    \"name\": \"joint6\", "
                    + "    \"x\": 30.0, "
                    + "    \"y\": 0.0 "
                    + "  }, "
                    + "  { "
                    + "    \"index\": 7, "
                    + "    \"name\": \"joint7\", "
                    + "    \"x\": 35.0, "
                    + "    \"y\": 35.0 "
                    + "  } "
                    + "], "
                    + "\"roadSections\": [ "
                    + "  { "
                    + "    \"index\": 0, "
                    + "    \"joint0\": 0, "
                    + "    \"joint1\": 1 "
                    + "  }, "
                    + "  { "
                    + "    \"index\": 1, "
                    + "    \"joint0\": 1, "
                    + "    \"joint1\": 2 "
                    + "  }, "
                    + "  { "
                    + "    \"index\": 2, "
                    + "    \"joint0\": 2, "
                    + "    \"joint1\": 3 "
                    + "  }, "
                    + "  { "
                    + "    \"index\": 3, "
                    + "    \"joint0\": 3, "
                    + "    \"joint1\": 4 "
                    + "  }, "
                    + "  { "
                    + "    \"index\": 4, "
                    + "    \"joint0\": 4, "
                    + "    \"joint1\": 5 "
                    + "  }, "
                    + "  { "
                    + "    \"index\": 5, "
                    + "    \"joint0\": 5, "
                    + "    \"joint1\": 6 "
                    + "  }, "
                    + "  { "
                    + "    \"index\": 6, "
                    + "    \"joint0\": 6, "
                    + "    \"joint1\": 7 "
                    + "  } "
                    + "] " + "}, " + PLAYERS_SERIALIZED_STRING + "}", 36, 36))).readObject(), 0);
    ItemSelector testSelector =
        new ItemSelector(testGame, new ClientViewProperties(SIMPLE_CLIENT_VIEW_PROPERTIES_SET));
    final HashSet<PositionView>[][] testCages = testSelector.getPositionCages();
    assertEquals(new HashSet<>(Arrays.asList(
        testCages[1][5], testCages[2][5], testCages[3][5], testCages[4][5], testCages[5][5],
        testCages[1][4], testCages[2][4], testCages[3][4], testCages[4][4], testCages[5][4],
        testCages[1][3], testCages[2][3], testCages[3][3], testCages[4][3], testCages[5][3],
        testCages[1][2], testCages[2][2], testCages[3][2], testCages[4][2], testCages[5][2],
        testCages[1][1], testCages[2][1], testCages[3][1], testCages[4][1], testCages[5][1])),
        testSelector.getRelevantCages(new Point2D.Double(15.0, 15.0)));
    assertEquals(new HashSet<>(Arrays.asList(
        testCages[1][5], testCages[2][5], testCages[3][5], testCages[4][5], testCages[5][5],
        testCages[1][4], testCages[2][4], testCages[3][4], testCages[4][4], testCages[5][4],
        testCages[1][3], testCages[2][3], testCages[3][3], testCages[4][3], testCages[5][3],
        testCages[1][2], testCages[2][2], testCages[3][2], testCages[4][2], testCages[5][2],
        testCages[1][1], testCages[2][1], testCages[3][1], testCages[4][1], testCages[5][1])),
        testSelector.getRelevantCages(new Point2D.Double(15.001, 15.001)));
    assertEquals(new HashSet<>(Arrays.asList(
        testCages[0][4], testCages[1][4], testCages[2][4], testCages[3][4], testCages[4][4],
        testCages[0][3], testCages[1][3], testCages[2][3], testCages[3][3], testCages[4][3],
        testCages[0][2], testCages[1][2], testCages[2][2], testCages[3][2], testCages[4][2],
        testCages[0][1], testCages[1][1], testCages[2][1], testCages[3][1], testCages[4][1],
        testCages[0][0], testCages[1][0], testCages[2][0], testCages[3][0], testCages[4][0])),
        testSelector.getRelevantCages(new Point2D.Double(14.999, 14.999)));
    assertEquals(new HashSet<>(Arrays.asList(
        testCages[1][4], testCages[2][4], testCages[3][4], testCages[4][4], testCages[5][4],
        testCages[1][3], testCages[2][3], testCages[3][3], testCages[4][3], testCages[5][3],
        testCages[1][2], testCages[2][2], testCages[3][2], testCages[4][2], testCages[5][2],
        testCages[1][1], testCages[2][1], testCages[3][1], testCages[4][1], testCages[5][1],
        testCages[1][0], testCages[2][0], testCages[3][0], testCages[4][0], testCages[5][0])),
        testSelector.getRelevantCages(new Point2D.Double(15.0, 14.999)));
    assertEquals(new HashSet<>(Arrays.asList(
        testCages[0][5], testCages[1][5], testCages[2][5], testCages[3][5], testCages[4][5],
        testCages[0][4], testCages[1][4], testCages[2][4], testCages[3][4], testCages[4][4],
        testCages[0][3], testCages[1][3], testCages[2][3], testCages[3][3], testCages[4][3],
        testCages[0][2], testCages[1][2], testCages[2][2], testCages[3][2], testCages[4][2],
        testCages[0][1], testCages[1][1], testCages[2][1], testCages[3][1], testCages[4][1])),
        testSelector.getRelevantCages(new Point2D.Double(14.999, 15.00)));
    assertEquals(new HashSet<>(Arrays.asList(
        testCages[0][2], testCages[1][2], testCages[2][2],
        testCages[0][1], testCages[1][1], testCages[2][1],
        testCages[0][0], testCages[1][0], testCages[2][0])),
        testSelector.getRelevantCages(new Point2D.Double(4.999, 4.999)));
    assertEquals(new HashSet<>(Arrays.asList(
        testCages[4][7], testCages[5][7], testCages[6][7], testCages[7][7],
        testCages[4][6], testCages[5][6], testCages[6][6], testCages[7][6],
        testCages[4][5], testCages[5][5], testCages[6][5], testCages[7][5],
        testCages[4][4], testCages[5][4], testCages[6][4], testCages[7][4])),
        testSelector.getRelevantCages(new Point2D.Double(30.0, 30.0)));
  }

  @Test
  public void testPositionSelectionWorks() throws Exception {
    GameView testGame = new GameView(
        Json.createReader(
            new StringReader(
                String.format(Locale.US, "{ " + GAME_ID_STRING + ", \"board\": {"
                    + BOARD_PROPERTIES_SERIALIZED_TEMPLATE_STRING + ", " + JOINTS_SERIALIZED_TEMPLATE_STRING + ", "
                    + ROAD_SECTIONS_SERIALIZED_STRING + "}, " + PLAYERS_SERIALIZED_STRING + "}", 25, 25,
                    4.0, 6.0, 4.0, 20.0, 24.0, 20.0))).readObject(), 0);
    ItemSelector testSelector =
        new ItemSelector(testGame, new ClientViewProperties(SIMPLE_CLIENT_VIEW_PROPERTIES_SET));
    {
      final PositionSelection selection =
          testSelector.selectPositions(new Point2D.Double(14.0, 10.0), PLAYER_A, ItemSelector.LookupType.TEST);
      assertEquals(new Point2D.Double(14.0, 10.0), selection.getSelectionPoint());
      assertEquals(new HashSet<PositionView>(), selection.getAllPositions());
      assertArrayEquals(new DistinctivePositionView[]{}, selection.getDistinctivePositions());
      assertEquals(PositionSelection.State.CREATED, selection.getState());
    }
    {
      testGame.getRoadSections()[0].setHalfVisibility(RoadSectionView.Direction.BACKWARD,
          RoadSectionView.Visibility.DISCOVERED);
      testGame.getRoadSections()[1].setHalfVisibility(RoadSectionView.Direction.BACKWARD,
          RoadSectionView.Visibility.DISCOVERED);
      final PositionSelection selection =
          testSelector.selectPositions(new Point2D.Double(13.0, 11.0), PLAYER_A, ItemSelector.LookupType.TEST);
      assertEquals(new HashSet<PositionView>(Arrays.asList(
          testGame.getRoadSections()[0].getPositions()[5],  // (4, 11)
          testGame.getRoadSections()[1].getPositions()[9]   // (13, 20)
          )), selection.getAllPositions());
      assertArrayEquals(new DistinctivePositionView[]{}, selection.getDistinctivePositions());
    }
    {
      testGame.getRoadSections()[0].setHalfVisibility(RoadSectionView.Direction.BACKWARD,
          RoadSectionView.Visibility.VISIBLE);
      testGame.getRoadSections()[1].setHalfVisibility(RoadSectionView.Direction.BACKWARD,
          RoadSectionView.Visibility.INVISIBLE);
      final PositionSelection selection =
          testSelector.selectPositions(new Point2D.Double(13.0, 11.0), PLAYER_A, ItemSelector.LookupType.TEST);
      assertEquals(new HashSet<PositionView>(Arrays.asList(
          testGame.getRoadSections()[0].getPositions()[5]  // (4, 11)
          // testGame.getRoadSections()[1].getPositions()[9]   // (13, 20) - INVISIBLE
          )), selection.getAllPositions());
      assertArrayEquals(new DistinctivePositionView[]{}, selection.getDistinctivePositions());
    }
    {
      testGame.getRoadSections()[0].setHalfVisibility(RoadSectionView.Direction.BACKWARD,
          RoadSectionView.Visibility.DISCOVERED);
      testGame.getRoadSections()[1].setHalfVisibility(RoadSectionView.Direction.BACKWARD,
          RoadSectionView.Visibility.VISIBLE);
      final PositionSelection selection =
          testSelector.selectPositions(new Point2D.Double(11.0, 13.0), PLAYER_A, ItemSelector.LookupType.TEST);
      assertEquals(new HashSet<PositionView>(Arrays.asList(
          testGame.getRoadSections()[0].getPositions()[7],  // (4, 13)
          testGame.getRoadSections()[1].getPositions()[7],  // (11, 20)
          testGame.getRoadSections()[0].getPositions()[0],  // (4, 6) - a joint
          testGame.getRoadSections()[1].getPositions()[0]   // (4, 20) - a joint
          )), selection.getAllPositions());
      final DistinctivePositionView[] distinctivePositions = selection.getDistinctivePositions();
      assertEquals(2, distinctivePositions.length);
      assertEquals(testGame.getRoadSections()[0].getPositions()[0], distinctivePositions[0].getPosition());
      assertEquals(testGame.getRoadSections()[1].getPositions()[0], distinctivePositions[1].getPosition());
      assertEquals(testGame.getRoadSections()[0].getPositions()[14], distinctivePositions[1].getPosition());
    }
    {
      testGame.getRoadSections()[0].setHalfVisibility(RoadSectionView.Direction.FORWARD,
          RoadSectionView.Visibility.DISCOVERED);
      testGame.getRoadSections()[1].setHalfVisibility(RoadSectionView.Direction.BACKWARD,
          RoadSectionView.Visibility.VISIBLE);
      final PositionSelection selection =
          testSelector.selectPositions(new Point2D.Double(11.0, 14.0), PLAYER_A, ItemSelector.LookupType.TEST);
      assertEquals(new HashSet<PositionView>(Arrays.asList(
          testGame.getRoadSections()[0].getPositions()[8],  // (4, 14)
          testGame.getRoadSections()[1].getPositions()[7],  // (11, 20)
          testGame.getRoadSections()[1].getPositions()[0]   // (4, 20) - a joint
          )), selection.getAllPositions());
      final DistinctivePositionView[] distinctivePositions = selection.getDistinctivePositions();
      assertEquals(1, distinctivePositions.length);
      assertEquals(testGame.getRoadSections()[1].getPositions()[0], distinctivePositions[0].getPosition());
      assertEquals(testGame.getRoadSections()[0].getPositions()[14], distinctivePositions[0].getPosition());
    }
    {
      testGame.getRoadSections()[0].setHalfVisibility(RoadSectionView.Direction.BACKWARD,
          RoadSectionView.Visibility.INVISIBLE);
      testGame.getRoadSections()[0].setHalfVisibility(RoadSectionView.Direction.FORWARD,
          RoadSectionView.Visibility.INVISIBLE);
      testGame.getRoadSections()[1].setHalfVisibility(RoadSectionView.Direction.BACKWARD,
          RoadSectionView.Visibility.VISIBLE);
      final PositionSelection selection =
          testSelector.selectPositions(new Point2D.Double(11.0, 14.0), PLAYER_A, ItemSelector.LookupType.TEST);
      assertEquals(new HashSet<PositionView>(Arrays.asList(
          // testGame.getRoadSections()[0].getPositions()[8],  // (4, 14) - INVISIBLE
          testGame.getRoadSections()[1].getPositions()[7],  // (11, 20)
          testGame.getRoadSections()[1].getPositions()[0]   // (4, 20) - a joint
          )), selection.getAllPositions());
      final DistinctivePositionView[] distinctivePositions = selection.getDistinctivePositions();
      assertEquals(1, distinctivePositions.length);
      assertEquals(testGame.getRoadSections()[1].getPositions()[0], distinctivePositions[0].getPosition());
      assertEquals(testGame.getRoadSections()[0].getPositions()[14], distinctivePositions[0].getPosition());
    }
    {
      testGame.getRoadSections()[0].setHalfVisibility(RoadSectionView.Direction.BACKWARD,
          RoadSectionView.Visibility.INVISIBLE);
      testGame.getRoadSections()[0].setHalfVisibility(RoadSectionView.Direction.FORWARD,
          RoadSectionView.Visibility.INVISIBLE);
      testGame.getRoadSections()[1].setHalfVisibility(RoadSectionView.Direction.BACKWARD,
          RoadSectionView.Visibility.INVISIBLE);
      final PositionSelection selection =
          testSelector.selectPositions(new Point2D.Double(11.0, 14.0), PLAYER_A, ItemSelector.LookupType.TEST);
      assertEquals(new HashSet<PositionView>(Arrays.asList(
          // testGame.getRoadSections()[0].getPositions()[8],  // (4, 14) - INVISIBLE
          // testGame.getRoadSections()[1].getPositions()[7],  // (11, 20) - INVISIBLE
          // testGame.getRoadSections()[1].getPositions()[0]   // (4, 20) - a joint, INVISIBLE
          )), selection.getAllPositions());
      final DistinctivePositionView[] distinctivePositions = selection.getDistinctivePositions();
      assertEquals(0, distinctivePositions.length);  // The Joint is INVISIBLE .
    }
  }

  /**
   * Creates a Game used is testing PositionSelection adjustment.
   * @return a Game used is testing PositionSelection adjustment
   * @throws ClientViewException when Game creation fails
   */
  private GameView createPositionSelectionAdjustmentTestGame() throws ClientViewException {
    final GameView testGame = new GameView(
        Json.createReader(
            new StringReader(
                String.format(Locale.US, "{ " + GAME_ID_STRING + ", \"board\": {"
                    + BOARD_PROPERTIES_SERIALIZED_TEMPLATE_STRING + ", " + JOINTS_SERIALIZED_TEMPLATE_STRING + ", "
                    + ROAD_SECTIONS_SERIALIZED_STRING + "}, " + PLAYERS_SERIALIZED_STRING + "}", 20, 10,
                    15.0, 1.0, 1.0, 1.0, 11.0, 6.0))).readObject(), 0);
    for (RoadSectionView roadSection : testGame.getRoadSections()) {
      for (RoadSectionView.Direction end :
          new RoadSectionView.Direction[]{RoadSectionView.Direction.BACKWARD, RoadSectionView.Direction.FORWARD}) {
        roadSection.setHalfVisibility(end, RoadSectionView.Visibility.VISIBLE);
      }
    }
    return testGame;
  }

  @Test
  public void testPositionSelectionAdjustment() throws Exception {
    final GameView testGame = createPositionSelectionAdjustmentTestGame();
    final PrawnView warriorA = new PrawnView(PrawnView.Type.WARRIOR, Math.PI, PLAYER_A, 0,
        testGame.getRoadSections()[0].getPositions()[0]);
    warriorA.setCurrentPosition(testGame.getRoadSections()[0].getPositions()[8]);
    final PrawnView settlersUnitB = new PrawnView(PrawnView.Type.SETTLERS_UNIT, 0.0, PLAYER_B, 1,
        testGame.getRoadSections()[1].getPositions()[0]);
    settlersUnitB.setCurrentPosition(testGame.getRoadSections()[0].getPositions()[9]);
    testGame.addPrawn(warriorA);
    testGame.addPrawn(settlersUnitB);

    ItemSelector testSelector =
        new ItemSelector(testGame, new ClientViewProperties(SIMPLE_CLIENT_VIEW_PROPERTIES_SET));
    PositionSelection positionSelection = new PositionSelection(PLAYER_A, new Point2D.Double(7.0, 1.0),
        new HashSet<>(),
        new HashSet<>(Arrays.asList(
            DistinctivePositionView.determinePositionDistinction(warriorA.getCurrentPosition(), PLAYER_A,
                ItemSelector.LookupType.POSITION_SELECTION.getSelectionPropertySet()),
            DistinctivePositionView.determinePositionDistinction(settlersUnitB.getCurrentPosition(), PLAYER_A,
                ItemSelector.LookupType.POSITION_SELECTION.getSelectionPropertySet()))));
    assertEquals(PositionSelection.State.CREATED, positionSelection.getState());
    DistinctivePositionView[] distinctivePositions = positionSelection.getDistinctivePositions();
    assertEquals(2, distinctivePositions.length);
    for (int i = 0; i < distinctivePositions.length; ++i) {
      assertEquals("Index " + i, DistinctivePositionView.State.CREATED, distinctivePositions[i].getState());
    }

    testSelector.adjust(positionSelection);

    assertEquals(PositionSelection.State.ADJUSTED, positionSelection.getState());
    for (int i = 0; i < distinctivePositions.length; ++i) {
      assertEquals("Index " + i, DistinctivePositionView.State.ADJUSTED, distinctivePositions[i].getState());
    }
    assertEquals(new Point2D.Double(7.0, 1.0), distinctivePositions[0].getCoordinatesPair());
    assertEquals(new Point2D.Double(3.0, 1.0), distinctivePositions[1].getCoordinatesPair());
  }

  @Test
  public void testPositionSelectionTwoPrawnsAdjustment() throws Exception {
    final GameView testGame = createPositionSelectionAdjustmentTestGame();
    final PrawnView warriorA = new PrawnView(PrawnView.Type.WARRIOR, Math.PI, PLAYER_A, 0,
        testGame.getRoadSections()[0].getPositions()[0]);
    warriorA.setCurrentPosition(testGame.getRoadSections()[0].getPositions()[7]);
    final PrawnView settlersUnitB = new PrawnView(PrawnView.Type.SETTLERS_UNIT, 0.0, PLAYER_B, 1,
        testGame.getRoadSections()[1].getPositions()[0]);
    settlersUnitB.setCurrentPosition(testGame.getRoadSections()[0].getPositions()[5]);
    final PrawnView warriorB = new PrawnView(PrawnView.Type.WARRIOR, 3 * Math.PI / 2, PLAYER_B, 2,
        testGame.getRoadSections()[1].getPositions()[0]);
    warriorB.setCurrentPosition(testGame.getRoadSections()[0].getPositions()[6]);
    testGame.addPrawn(warriorA);
    testGame.addPrawn(settlersUnitB);
    testGame.addPrawn(warriorB);

    ItemSelector testSelector =
        new ItemSelector(testGame, new ClientViewProperties(SIMPLE_CLIENT_VIEW_PROPERTIES_SET));
    PositionSelection positionSelection = new PositionSelection(PLAYER_A, new Point2D.Double(7.0, 1.0),
        new HashSet<>(),
        new HashSet<>(Arrays.asList(
            DistinctivePositionView.determinePositionDistinction(warriorA.getCurrentPosition(), PLAYER_A,
                ItemSelector.LookupType.POSITION_SELECTION.getSelectionPropertySet()),
            DistinctivePositionView.determinePositionDistinction(settlersUnitB.getCurrentPosition(), PLAYER_A,
                ItemSelector.LookupType.POSITION_SELECTION.getSelectionPropertySet()),
            DistinctivePositionView.determinePositionDistinction(warriorB.getCurrentPosition(), PLAYER_A,
                ItemSelector.LookupType.POSITION_SELECTION.getSelectionPropertySet()))));
    assertEquals(PositionSelection.State.CREATED, positionSelection.getState());
    DistinctivePositionView[] distinctivePositions = positionSelection.getDistinctivePositions();
    assertEquals(3, distinctivePositions.length);
    for (int i = 0; i < distinctivePositions.length; ++i) {
      assertEquals("Index " + i, DistinctivePositionView.State.CREATED, distinctivePositions[i].getState());
    }

    testSelector.adjust(positionSelection);

    assertEquals(PositionSelection.State.ADJUSTED, positionSelection.getState());
    for (int i = 0; i < distinctivePositions.length; ++i) {
      assertEquals("Index " + i, DistinctivePositionView.State.ADJUSTED, distinctivePositions[i].getState());
    }
    assertEquals(new Point2D.Double(8.0, 1.0), distinctivePositions[0].getCoordinatesPair());
    assertEquals(warriorA.getCurrentPosition(), distinctivePositions[0].getPosition());
    assertEquals(new Point2D.Double(12.0, 1.0), distinctivePositions[1].getCoordinatesPair());
    assertEquals(warriorB.getCurrentPosition(), distinctivePositions[1].getPosition());
    // NOTE: Outside the RoadSection ((1, 1) -> (15, 1)).
    assertEquals(new Point2D.Double(16.0, 1.0), distinctivePositions[2].getCoordinatesPair());
    assertEquals(settlersUnitB.getCurrentPosition(), distinctivePositions[2].getPosition());
  }

  @Test
  public void testPositionSelectionTwoPrawnsAdjustment1() throws Exception {
    final GameView testGame = createPositionSelectionAdjustmentTestGame();
    final PrawnView warriorA = new PrawnView(PrawnView.Type.WARRIOR, Math.PI, PLAYER_A, 0,
        testGame.getRoadSections()[0].getPositions()[0]);
    warriorA.setCurrentPosition(testGame.getRoadSections()[0].getPositions()[7]);
    final PrawnView settlersUnitB = new PrawnView(PrawnView.Type.SETTLERS_UNIT, 0.0, PLAYER_B, 1,
        testGame.getRoadSections()[1].getPositions()[0]);
    settlersUnitB.setCurrentPosition(testGame.getRoadSections()[0].getPositions()[9]);
    final PrawnView warriorB = new PrawnView(PrawnView.Type.WARRIOR, 3 * Math.PI / 2, PLAYER_B, 2,
        testGame.getRoadSections()[1].getPositions()[0]);
    warriorB.setCurrentPosition(testGame.getRoadSections()[0].getPositions()[8]);
    testGame.addPrawn(warriorA);
    testGame.addPrawn(settlersUnitB);
    testGame.addPrawn(warriorB);

    ItemSelector testSelector =
        new ItemSelector(testGame, new ClientViewProperties(SIMPLE_CLIENT_VIEW_PROPERTIES_SET));
    PositionSelection positionSelection = new PositionSelection(PLAYER_A, new Point2D.Double(7.0, 1.0),
        new HashSet<>(),
        new HashSet<>(Arrays.asList(
            DistinctivePositionView.determinePositionDistinction(warriorA.getCurrentPosition(), PLAYER_A,
                ItemSelector.LookupType.POSITION_SELECTION.getSelectionPropertySet()),
            DistinctivePositionView.determinePositionDistinction(settlersUnitB.getCurrentPosition(), PLAYER_A,
                ItemSelector.LookupType.POSITION_SELECTION.getSelectionPropertySet()),
            DistinctivePositionView.determinePositionDistinction(warriorB.getCurrentPosition(), PLAYER_A,
                ItemSelector.LookupType.POSITION_SELECTION.getSelectionPropertySet()))));
    assertEquals(PositionSelection.State.CREATED, positionSelection.getState());
    DistinctivePositionView[] distinctivePositions = positionSelection.getDistinctivePositions();
    assertEquals(3, distinctivePositions.length);
    for (int i = 0; i < distinctivePositions.length; ++i) {
      assertEquals("Index " + i, DistinctivePositionView.State.CREATED, distinctivePositions[i].getState());
    }

    testSelector.adjust(positionSelection);

    assertEquals(PositionSelection.State.ADJUSTED, positionSelection.getState());
    for (int i = 0; i < distinctivePositions.length; ++i) {
      assertEquals("Index " + i, DistinctivePositionView.State.ADJUSTED, distinctivePositions[i].getState());
    }
    assertEquals(new Point2D.Double(8.0, 1.0), distinctivePositions[0].getCoordinatesPair());
    assertEquals(warriorA.getCurrentPosition(), distinctivePositions[0].getPosition());
    assertEquals(new Point2D.Double(4.0, 1.0), distinctivePositions[1].getCoordinatesPair());
    assertEquals(warriorB.getCurrentPosition(), distinctivePositions[1].getPosition());
    assertEquals(new Point2D.Double(0.0, 1.0), distinctivePositions[2].getCoordinatesPair());
    assertEquals(settlersUnitB.getCurrentPosition(), distinctivePositions[2].getPosition());
  }

  @Test
  public void testPositionSelectionTwoPrawnsOnAnotherRoadSectionAdjustment() throws Exception {
    final GameView testGame = createPositionSelectionAdjustmentTestGame();
    final PrawnView warriorA = new PrawnView(PrawnView.Type.WARRIOR, Math.PI, PLAYER_A, 0,
        testGame.getRoadSections()[0].getPositions()[0]);
    warriorA.setCurrentPosition(testGame.getRoadSections()[0].getPositions()[7]);
    final PrawnView settlersUnitB = new PrawnView(PrawnView.Type.SETTLERS_UNIT, 0.0, PLAYER_B, 1,
        testGame.getRoadSections()[1].getPositions()[0]);
    settlersUnitB.setCurrentPosition(testGame.getRoadSections()[1].getPositions()[6]);
    final PrawnView warriorB = new PrawnView(PrawnView.Type.WARRIOR, 3 * Math.PI / 2, PLAYER_B, 2,
        testGame.getRoadSections()[1].getPositions()[0]);
    warriorB.setCurrentPosition(testGame.getRoadSections()[1].getPositions()[7]);
    testGame.addPrawn(warriorA);
    testGame.addPrawn(settlersUnitB);
    testGame.addPrawn(warriorB);

    ItemSelector testSelector =
        new ItemSelector(testGame, new ClientViewProperties(SIMPLE_CLIENT_VIEW_PROPERTIES_SET));
    PositionSelection positionSelection = new PositionSelection(PLAYER_A, new Point2D.Double(7.0, 1.0),
        new HashSet<>(),
        new HashSet<>(Arrays.asList(
            DistinctivePositionView.determinePositionDistinction(warriorA.getCurrentPosition(), PLAYER_A,
                ItemSelector.LookupType.POSITION_SELECTION.getSelectionPropertySet()),
            DistinctivePositionView.determinePositionDistinction(settlersUnitB.getCurrentPosition(), PLAYER_A,
                ItemSelector.LookupType.POSITION_SELECTION.getSelectionPropertySet()),
            DistinctivePositionView.determinePositionDistinction(warriorB.getCurrentPosition(), PLAYER_A,
                ItemSelector.LookupType.POSITION_SELECTION.getSelectionPropertySet()))));
    assertEquals(PositionSelection.State.CREATED, positionSelection.getState());
    DistinctivePositionView[] distinctivePositions = positionSelection.getDistinctivePositions();
    assertEquals(3, distinctivePositions.length);
    for (int i = 0; i < distinctivePositions.length; ++i) {
      assertEquals("Index " + i, DistinctivePositionView.State.CREATED, distinctivePositions[i].getState());
    }

    testSelector.adjust(positionSelection);

    assertEquals(PositionSelection.State.ADJUSTED, positionSelection.getState());
    for (int i = 0; i < distinctivePositions.length; ++i) {
      assertEquals("Index " + i, DistinctivePositionView.State.ADJUSTED, distinctivePositions[i].getState());
    }
    assertEquals(new Point2D.Double(3.0, 1.0), distinctivePositions[0].getCoordinatesPair());
    assertEquals(warriorA.getCurrentPosition(), distinctivePositions[0].getPosition());
    assertEquals(new Point2D.Double(6.454545454545454, 3.727272727272727),
        distinctivePositions[1].getCoordinatesPair());
    assertEquals(settlersUnitB.getCurrentPosition(), distinctivePositions[1].getPosition());
    assertEquals(new Point2D.Double(10.090909090909092, 5.545454545454546),
        distinctivePositions[2].getCoordinatesPair());
    assertEquals(warriorB.getCurrentPosition(), distinctivePositions[2].getPosition());
  }

  @Test
  public void testTwoRoadSectionAtJoint() throws Exception {
    final GameView testGame = createPositionSelectionAdjustmentTestGame();
    final PrawnView warriorA = new PrawnView(PrawnView.Type.WARRIOR, Math.PI, PLAYER_A, 0,
        testGame.getRoadSections()[0].getPositions()[0]);
    warriorA.setCurrentPosition(testGame.getRoadSections()[0].getPositions()[13]);
    final PrawnView settlersUnitA = new PrawnView(PrawnView.Type.WARRIOR, Math.PI, PLAYER_A, 1,
        testGame.getRoadSections()[0].getPositions()[0]);
    settlersUnitA.setCurrentPosition(testGame.getRoadSections()[0].getPositions()[12]);
    final PrawnView settlersUnitB = new PrawnView(PrawnView.Type.SETTLERS_UNIT, 0.0, PLAYER_B, 2,
        testGame.getRoadSections()[1].getPositions()[0]);
    settlersUnitB.setCurrentPosition(testGame.getRoadSections()[1].getPositions()[3]);
    final PrawnView warriorB = new PrawnView(PrawnView.Type.WARRIOR, 3 * Math.PI / 2, PLAYER_B, 3,
        testGame.getRoadSections()[1].getPositions()[0]);
    warriorB.setCurrentPosition(testGame.getRoadSections()[1].getPositions()[2]);
    testGame.addPrawn(warriorA);
    testGame.addPrawn(settlersUnitA);
    testGame.addPrawn(warriorB);
    testGame.addPrawn(settlersUnitB);

    ItemSelector testSelector =
        new ItemSelector(testGame, new ClientViewProperties(SIMPLE_CLIENT_VIEW_PROPERTIES_SET));
    PositionSelection positionSelection = new PositionSelection(PLAYER_A, new Point2D.Double(7.0, 1.0),
        new HashSet<>(),
        new HashSet<>(Arrays.asList(
            DistinctivePositionView.determinePositionDistinction(warriorA.getCurrentPosition(), PLAYER_A,
                ItemSelector.LookupType.POSITION_SELECTION.getSelectionPropertySet()),
            DistinctivePositionView.determinePositionDistinction(settlersUnitA.getCurrentPosition(), PLAYER_A,
                ItemSelector.LookupType.POSITION_SELECTION.getSelectionPropertySet()),
            DistinctivePositionView.determinePositionDistinction(settlersUnitB.getCurrentPosition(), PLAYER_A,
                ItemSelector.LookupType.POSITION_SELECTION.getSelectionPropertySet()),
            DistinctivePositionView.determinePositionDistinction(warriorB.getCurrentPosition(), PLAYER_A,
                ItemSelector.LookupType.POSITION_SELECTION.getSelectionPropertySet()))));
    assertEquals(PositionSelection.State.CREATED, positionSelection.getState());
    DistinctivePositionView[] distinctivePositions = positionSelection.getDistinctivePositions();
    assertEquals(4, distinctivePositions.length);
    for (int i = 0; i < distinctivePositions.length; ++i) {
      assertEquals("Index " + i, DistinctivePositionView.State.CREATED, distinctivePositions[i].getState());
    }

    testSelector.adjust(positionSelection);

    assertEquals(PositionSelection.State.ADJUSTED, positionSelection.getState());
    for (int i = 0; i < distinctivePositions.length; ++i) {
      assertEquals("Index " + i, DistinctivePositionView.State.ADJUSTED, distinctivePositions[i].getState());
    }
    assertEquals(new Point2D.Double(2.0, 1.0), distinctivePositions[0].getCoordinatesPair());
    assertEquals(warriorA.getCurrentPosition(), distinctivePositions[0].getPosition());
    assertEquals(new Point2D.Double(6.0, 1.0), distinctivePositions[1].getCoordinatesPair());
    assertEquals(settlersUnitA.getCurrentPosition(), distinctivePositions[1].getPosition());
    assertEquals(new Point2D.Double(8.272727272727273, 4.636363636363637),
        distinctivePositions[2].getCoordinatesPair());
    assertEquals(warriorB.getCurrentPosition(), distinctivePositions[2].getPosition());
    assertEquals(new Point2D.Double(11.909090909090908, 6.454545454545454),
        distinctivePositions[3].getCoordinatesPair());
    assertEquals(settlersUnitB.getCurrentPosition(), distinctivePositions[3].getPosition());
  }

  @Test
  public void testTwoRoadSectionAtJoint1() throws Exception {
    final GameView testGame = createPositionSelectionAdjustmentTestGame();
    final PrawnView warriorA = new PrawnView(PrawnView.Type.WARRIOR, Math.PI, PLAYER_A, 0,
        testGame.getRoadSections()[0].getPositions()[0]);
    warriorA.setCurrentPosition(testGame.getRoadSections()[0].getPositions()[12]);
    final PrawnView settlersUnitA = new PrawnView(PrawnView.Type.WARRIOR, Math.PI, PLAYER_A, 1,
        testGame.getRoadSections()[0].getPositions()[0]);
    settlersUnitA.setCurrentPosition(testGame.getRoadSections()[0].getPositions()[11]);
    final PrawnView settlersUnitB = new PrawnView(PrawnView.Type.SETTLERS_UNIT, 0.0, PLAYER_B, 2,
        testGame.getRoadSections()[1].getPositions()[0]);
    settlersUnitB.setCurrentPosition(testGame.getRoadSections()[1].getPositions()[2]);
    final PrawnView warriorB = new PrawnView(PrawnView.Type.WARRIOR, 3 * Math.PI / 2, PLAYER_B, 3,
        testGame.getRoadSections()[1].getPositions()[0]);
    warriorB.setCurrentPosition(testGame.getRoadSections()[1].getPositions()[1]);
    testGame.addPrawn(warriorA);
    testGame.addPrawn(settlersUnitA);
    testGame.addPrawn(warriorB);
    testGame.addPrawn(settlersUnitB);

    ItemSelector testSelector =
        new ItemSelector(testGame, new ClientViewProperties(SIMPLE_CLIENT_VIEW_PROPERTIES_SET));
    PositionSelection positionSelection = new PositionSelection(PLAYER_A, new Point2D.Double(7.0, 1.0),
        new HashSet<>(),
        new HashSet<>(Arrays.asList(
            DistinctivePositionView.determinePositionDistinction(warriorA.getCurrentPosition(), PLAYER_A,
                ItemSelector.LookupType.POSITION_SELECTION.getSelectionPropertySet()),
            DistinctivePositionView.determinePositionDistinction(settlersUnitA.getCurrentPosition(), PLAYER_A,
                ItemSelector.LookupType.POSITION_SELECTION.getSelectionPropertySet()),
            DistinctivePositionView.determinePositionDistinction(settlersUnitB.getCurrentPosition(), PLAYER_A,
                ItemSelector.LookupType.POSITION_SELECTION.getSelectionPropertySet()),
            DistinctivePositionView.determinePositionDistinction(warriorB.getCurrentPosition(), PLAYER_A,
                ItemSelector.LookupType.POSITION_SELECTION.getSelectionPropertySet()))));
    assertEquals(PositionSelection.State.CREATED, positionSelection.getState());
    DistinctivePositionView[] distinctivePositions = positionSelection.getDistinctivePositions();
    assertEquals(4, distinctivePositions.length);
    for (int i = 0; i < distinctivePositions.length; ++i) {
      assertEquals("Index " + i, DistinctivePositionView.State.CREATED, distinctivePositions[i].getState());
    }

    testSelector.adjust(positionSelection);

    assertEquals(PositionSelection.State.ADJUSTED, positionSelection.getState());
    for (int i = 0; i < distinctivePositions.length; ++i) {
      assertEquals("Index " + i, DistinctivePositionView.State.ADJUSTED, distinctivePositions[i].getState());
    }
    assertEquals(new Point2D.Double(1.9090909090909092, 1.4545454545454546),
        distinctivePositions[0].getCoordinatesPair());
    assertEquals(warriorB.getCurrentPosition(), distinctivePositions[0].getPosition());
    assertEquals(new Point2D.Double(13.0, 1.0), distinctivePositions[1].getCoordinatesPair());
    assertEquals(settlersUnitA.getCurrentPosition(), distinctivePositions[1].getPosition());
    assertEquals(new Point2D.Double(9.0, 1.0), distinctivePositions[2].getCoordinatesPair());
    assertEquals(warriorA.getCurrentPosition(), distinctivePositions[2].getPosition());
    assertEquals(new Point2D.Double(5.545454545454546, 3.272727272727273),
        distinctivePositions[3].getCoordinatesPair());
    assertEquals(settlersUnitB.getCurrentPosition(), distinctivePositions[3].getPosition());
  }

  @Test
  public void testAssistedPositionSelectionWorks() throws Exception {
    final GameView testGame = createPositionSelectionAdjustmentTestGame();
    final PrawnView warriorA = new PrawnView(PrawnView.Type.WARRIOR, Math.PI, PLAYER_A, 0,
        testGame.getRoadSections()[0].getPositions()[0]);
    warriorA.setCurrentPosition(testGame.getRoadSections()[0].getPositions()[13]);
    final PrawnView settlersUnitA = new PrawnView(PrawnView.Type.WARRIOR, Math.PI, PLAYER_A, 1,
        testGame.getRoadSections()[0].getPositions()[0]);
    settlersUnitA.setCurrentPosition(testGame.getRoadSections()[0].getPositions()[12]);
    testGame.addPrawn(warriorA);
    testGame.addPrawn(settlersUnitA);
    ItemSelector testSelector =
        new ItemSelector(testGame, new ClientViewProperties(SIMPLE_CLIENT_VIEW_PROPERTIES_SET));
    PositionSelection positionSelection = new PositionSelection(PLAYER_A, new Point2D.Double(7.0, 1.0),
        new HashSet<>(),
        new HashSet<>(Arrays.asList(
            DistinctivePositionView.determinePositionDistinction(warriorA.getCurrentPosition(), PLAYER_A,
                ItemSelector.LookupType.POSITION_SELECTION.getSelectionPropertySet()),
            DistinctivePositionView.determinePositionDistinction(settlersUnitA.getCurrentPosition(), PLAYER_A,
                ItemSelector.LookupType.POSITION_SELECTION.getSelectionPropertySet()))));
    testSelector.adjust(positionSelection);

    assertEquals(warriorA.getCurrentPosition(),
        testSelector.selectPosition(new Point2D.Double(3.0, 1.0), positionSelection));
    assertEquals(settlersUnitA.getCurrentPosition(),
        testSelector.selectPosition(new Point2D.Double(5.0, 1.0), positionSelection));
    assertEquals(testGame.getRoadSections()[0].getPositions()[11],
        testSelector.selectPosition(new Point2D.Double(4.0, 0.5), positionSelection));
    assertEquals(testGame.getRoadSections()[1].getPositions()[3],
        testSelector.selectPosition(new Point2D.Double(4.0, 2.0), positionSelection));
    assertNull(testSelector.selectPosition(new Point2D.Double(2.0, 13.0), positionSelection));
  }

  @Test
  public void testPrawnSelectionCreated() throws Exception {
    final GameView testGame = new GameView(
        Json.createReader(
            new StringReader(
                String.format(Locale.US, "{ " + GAME_ID_STRING + ", \"board\": {"
                    + BOARD_PROPERTIES_SERIALIZED_TEMPLATE_STRING + ", " + JOINTS_SERIALIZED_TEMPLATE_STRING + ", "
                    + ROAD_SECTIONS_SERIALIZED_STRING + "}, " + PLAYERS_SERIALIZED_STRING + "}", 20, 10,
                    15.0, 1.0, 1.0, 1.0, 11.0, 6.0))).readObject(), 0);
    final PositionView testPosition = testGame.getRoadSections()[0].getPositions()[1];
    final PrawnView settlersUnit = new PrawnView(PrawnView.Type.SETTLERS_UNIT, 0.0, PLAYER_A, 0,
        testGame.getRoadSections()[0].getPositions()[0]);
    final PrawnView warrior = new PrawnView(PrawnView.Type.WARRIOR, Math.PI, PLAYER_A, 1,
        testGame.getRoadSections()[0].getPositions()[0]);
    settlersUnit.setCurrentPosition(testPosition);
    warrior.setCurrentPosition(testPosition);
    PrawnSelection prawnSelection =
        itemSelector.createPrawnSelection(new HashSet<>(Arrays.asList(warrior, settlersUnit)), PLAYER_A);

    assertEquals(2, prawnSelection.getPrawnSelectionHeight());
    assertEquals(3, prawnSelection.getPrawnSelectionWidth());
    assertEquals(0, prawnSelection.getStartPosition());
    assertEquals(0, prawnSelection.getNextCount());

    assertEquals(PrawnBoxType.PRAWN, prawnSelection.getPrawnBoxes()[0][0].getType());
    assertEquals(settlersUnit, prawnSelection.getPrawnBoxes()[0][0].getPrawn());
    assertEquals(0, prawnSelection.getPrawnBoxes()[0][0].getCoordX());
    assertEquals(0, prawnSelection.getPrawnBoxes()[0][0].getCoordY());
    assertEquals(PrawnBoxType.PRAWN, prawnSelection.getPrawnBoxes()[1][0].getType());
    assertEquals(warrior, prawnSelection.getPrawnBoxes()[1][0].getPrawn());
    assertEquals(20, prawnSelection.getPrawnBoxes()[1][0].getCoordX());
    assertEquals(0, prawnSelection.getPrawnBoxes()[1][0].getCoordY());
    assertEquals(PrawnBoxType.EMPTY, prawnSelection.getPrawnBoxes()[2][0].getType());
    assertEquals(PrawnBoxType.EMPTY, prawnSelection.getPrawnBoxes()[0][1].getType());
    assertEquals(PrawnBoxType.EMPTY, prawnSelection.getPrawnBoxes()[1][1].getType());
    assertEquals(PrawnBoxType.EMPTY, prawnSelection.getPrawnBoxes()[2][1].getType());
  }

  @Test
  public void testPrawnSelectionProcessed() throws Exception {
    final GameView testGame = new GameView(
        Json.createReader(
            new StringReader(
                String.format(Locale.US, "{ " + GAME_ID_STRING + ", \"board\": {"
                    + BOARD_PROPERTIES_SERIALIZED_TEMPLATE_STRING + ", " + JOINTS_SERIALIZED_TEMPLATE_STRING + ", "
                    + ROAD_SECTIONS_SERIALIZED_STRING + "}, " + PLAYERS_SERIALIZED_STRING + "}", 20, 10,
                    15.0, 1.0, 1.0, 1.0, 11.0, 6.0))).readObject(), 0);
    final PositionView testPosition = testGame.getRoadSections()[0].getPositions()[1];
    final PrawnView settlersUnit0 = new PrawnView(PrawnView.Type.SETTLERS_UNIT, 0.0, PLAYER_A, 0,
        testGame.getRoadSections()[0].getPositions()[0]);
    final PrawnView settlersUnit1 = new PrawnView(PrawnView.Type.SETTLERS_UNIT, 0.0, PLAYER_A, 1,
        testGame.getRoadSections()[0].getPositions()[0]);
    final PrawnView settlersUnit2 = new PrawnView(PrawnView.Type.SETTLERS_UNIT, 0.0, PLAYER_A, 2,
        testGame.getRoadSections()[0].getPositions()[0]);
    final PrawnView settlersUnit3 = new PrawnView(PrawnView.Type.SETTLERS_UNIT, 0.0, PLAYER_A, 3,
        testGame.getRoadSections()[0].getPositions()[0]);
    final PrawnView settlersUnit4 = new PrawnView(PrawnView.Type.SETTLERS_UNIT, 0.0, PLAYER_A, 4,
        testGame.getRoadSections()[0].getPositions()[0]);
    final PrawnView settlersUnit5 = new PrawnView(PrawnView.Type.SETTLERS_UNIT, 0.0, PLAYER_A, 5,
        testGame.getRoadSections()[0].getPositions()[0]);
    final PrawnView settlersUnit6 = new PrawnView(PrawnView.Type.SETTLERS_UNIT, 0.0, PLAYER_A, 6,
        testGame.getRoadSections()[0].getPositions()[0]);
    settlersUnit0.setCurrentPosition(testPosition);
    settlersUnit1.setCurrentPosition(testPosition);
    settlersUnit2.setCurrentPosition(testPosition);
    settlersUnit3.setCurrentPosition(testPosition);
    settlersUnit4.setCurrentPosition(testPosition);
    settlersUnit5.setCurrentPosition(testPosition);
    settlersUnit6.setCurrentPosition(testPosition);
    PrawnSelection prawnSelection =
        itemSelector.createPrawnSelection(new HashSet<>(Arrays.asList(
            settlersUnit0, settlersUnit1, settlersUnit2, settlersUnit3, settlersUnit4, settlersUnit5, settlersUnit6)),
            PLAYER_A);

    assertEquals(settlersUnit0, itemSelector.selectPrawnOrProcessPrawnSelection(
        new Point2D.Double(0 * PRAWN_BOX_SIZE, 0 * PRAWN_BOX_SIZE), prawnSelection));
    assertEquals(settlersUnit1, itemSelector.selectPrawnOrProcessPrawnSelection(
        new Point2D.Double(1 * PRAWN_BOX_SIZE + PRAWN_BOX_SIZE - 1, 0 * PRAWN_BOX_SIZE), prawnSelection));
    assertEquals(settlersUnit2, itemSelector.selectPrawnOrProcessPrawnSelection(
        new Point2D.Double(2 * PRAWN_BOX_SIZE, 0 * PRAWN_BOX_SIZE + PRAWN_BOX_SIZE - 1), prawnSelection));
    assertEquals(settlersUnit3, itemSelector.selectPrawnOrProcessPrawnSelection(
        new Point2D.Double(0 * PRAWN_BOX_SIZE + PRAWN_BOX_SIZE - 1, 1 * PRAWN_BOX_SIZE + PRAWN_BOX_SIZE - 1),
        prawnSelection));
    assertEquals(settlersUnit4, itemSelector.selectPrawnOrProcessPrawnSelection(
        new Point2D.Double(1 * PRAWN_BOX_SIZE, 1 * PRAWN_BOX_SIZE), prawnSelection));
    assertEquals(0, prawnSelection.getStartPosition());
    assertEquals(2, prawnSelection.getNextCount());
    assertNull(itemSelector.selectPrawnOrProcessPrawnSelection(
        new Point2D.Double(2 * PRAWN_BOX_SIZE + PRAWN_BOX_SIZE / 2, 1 * PRAWN_BOX_SIZE + PRAWN_BOX_SIZE / 2),
        prawnSelection));
    assertEquals(5, prawnSelection.getStartPosition());
    assertEquals(0, prawnSelection.getNextCount());
    assertEquals(settlersUnit5, itemSelector.selectPrawnOrProcessPrawnSelection(
        new Point2D.Double(1 * PRAWN_BOX_SIZE, 0 * PRAWN_BOX_SIZE), prawnSelection));
    assertEquals(settlersUnit6, itemSelector.selectPrawnOrProcessPrawnSelection(
        new Point2D.Double(2 * PRAWN_BOX_SIZE, 0 * PRAWN_BOX_SIZE), prawnSelection));
    assertNull(itemSelector.selectPrawnOrProcessPrawnSelection(
        new Point2D.Double(1 * PRAWN_BOX_SIZE, 1 * PRAWN_BOX_SIZE), prawnSelection));
    assertEquals(5, prawnSelection.getStartPosition());
    assertEquals(0, prawnSelection.getNextCount());
    assertNull(itemSelector.selectPrawnOrProcessPrawnSelection(
        new Point2D.Double(0 * PRAWN_BOX_SIZE, 0 * PRAWN_BOX_SIZE), prawnSelection));
    assertEquals(0, prawnSelection.getStartPosition());
    assertEquals(2, prawnSelection.getNextCount());
    assertEquals(settlersUnit0, itemSelector.selectPrawnOrProcessPrawnSelection(
        new Point2D.Double(0 * PRAWN_BOX_SIZE, 0 * PRAWN_BOX_SIZE), prawnSelection));
  }

}
