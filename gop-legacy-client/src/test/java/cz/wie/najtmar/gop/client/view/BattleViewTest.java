package cz.wie.najtmar.gop.client.view;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import cz.wie.najtmar.gop.common.Point2D;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.UUID;

public class BattleViewTest {

  static final long START_TIME = 966;
  static final double STEP_SIZE = 1.0;
  JointView joint01;
  JointView joint31;
  JointView joint34;
  RoadSectionView roadSection0;
  RoadSectionView roadSection1;
  RoadSectionView roadSection2;
  PlayerView playerA;
  PlayerView playerB;
  PrawnView settlersUnitA;
  PrawnView warriorA;
  PrawnView warriorB;
  PrawnView warriorB1;

  PositionView position0;
  PositionView position1;
  PositionView position2;

  /**
   * @throws java.lang.Exception when test cannot be set up
   */
  @Before
  public void setUp() throws Exception {
    joint01 = new JointView(0, "joint01", new Point2D.Double(0.0, 1.0));
    joint31 = new JointView(1, "joint31", new Point2D.Double(3.0, 1.0));
    joint34 = new JointView(2, "joint34", new Point2D.Double(3.0, 4.0));
    roadSection0 = new RoadSectionView(0, joint01, joint31, STEP_SIZE);
    roadSection1 = new RoadSectionView(1, joint31, joint34, STEP_SIZE);
    roadSection2 = new RoadSectionView(2, joint01, joint34, STEP_SIZE);
    playerA = new PlayerView(new UserView("playerA", new UUID(12345, 67890)), 0);
    playerB = new PlayerView(new UserView("playerB", new UUID(98765, 43210)), 1);
    position0 = roadSection0.getPositions()[1];
    position1 = roadSection0.getPositions()[2];
    position2 = roadSection0.getPositions()[3];
    settlersUnitA = new PrawnView(PrawnView.Type.SETTLERS_UNIT, 0.0, playerA, 0, joint01.getNormalizedPosition());
    settlersUnitA.setCurrentPosition(position0);
    warriorA = new PrawnView(PrawnView.Type.WARRIOR, Math.PI, playerA, 1, joint01.getNormalizedPosition());
    warriorA.setCurrentPosition(position2);
    warriorB = new PrawnView(PrawnView.Type.WARRIOR, Math.PI, playerB, 2, joint01.getNormalizedPosition());
    warriorB.setCurrentPosition(position1);
    warriorB1 = new PrawnView(PrawnView.Type.WARRIOR, Math.PI, playerB, 3, joint01.getNormalizedPosition());
  }

  @Test
  public void testBattleCreatedCorrectly() throws Exception {
    assertEquals(new HashSet<>(Arrays.asList()), settlersUnitA.getBattles());
    assertEquals(new HashSet<>(Arrays.asList()), warriorB.getBattles());
    assertEquals(new HashSet<>(Arrays.asList()), position0.getBattles());
    assertEquals(new HashSet<>(Arrays.asList()), position1.getBattles());
    final BattleView battle = new BattleView(settlersUnitA, warriorB, START_TIME + 1);
    assertEquals(BattleView.State.EXISTING, battle.getState());
    assertArrayEquals(new PrawnView[]{settlersUnitA, warriorB}, battle.getParticipants());
    assertEquals(new Point2D.Double(1.5, 1.0), battle.getPoint());
    assertEquals(START_TIME + 1, battle.getStartTime());
    assertEquals(new HashSet<>(Arrays.asList(battle)), settlersUnitA.getBattles());
    assertEquals(new HashSet<>(Arrays.asList(battle)), warriorB.getBattles());
    assertEquals(new HashSet<>(Arrays.asList(battle)), position0.getBattles());
    assertEquals(new HashSet<>(Arrays.asList(battle)), position1.getBattles());
    assertArrayEquals(new PositionView[]{position0, position1}, battle.getNormalizedPositions());

    // matches()
    assertTrue(battle.matches(settlersUnitA, warriorB, position0, position1));
    assertFalse(battle.matches(warriorB, settlersUnitA, position1, position0));
    assertFalse(battle.matches(warriorA, warriorB, position0, position1));
    assertFalse(battle.matches(settlersUnitA, warriorB1, position0, position1));
    assertFalse(battle.matches(settlersUnitA, warriorB, position2, position1));
    assertFalse(battle.matches(settlersUnitA, warriorB, position0, position2));
  }

  @Test
  public void testReverseBattleCreatedCorrectly() throws Exception {
    final BattleView battle = new BattleView(warriorB, settlersUnitA, START_TIME + 2);
    assertEquals(BattleView.State.EXISTING, battle.getState());
    assertArrayEquals(new PrawnView[]{settlersUnitA, warriorB}, battle.getParticipants());
    assertEquals(new Point2D.Double(1.5, 1.0), battle.getPoint());
    assertEquals(START_TIME + 2, battle.getStartTime());
    assertArrayEquals(new PositionView[]{position0, position1}, battle.getNormalizedPositions());
  }

  @Test
  public void testBattleCreatedCorrectly1() throws Exception {
    assertEquals(new HashSet<>(Arrays.asList()), position1.getBattles());
    assertEquals(new HashSet<>(Arrays.asList()), position2.getBattles());
    final BattleView battle = new BattleView(warriorA, warriorB, START_TIME + 2);
    assertEquals(BattleView.State.EXISTING, battle.getState());
    assertArrayEquals(new PrawnView[]{warriorA, warriorB}, battle.getParticipants());
    assertEquals(new Point2D.Double(2.5, 1.0), battle.getPoint());
    assertEquals(START_TIME + 2, battle.getStartTime());
    assertEquals(new HashSet<>(Arrays.asList(battle)), position1.getBattles());
    assertEquals(new HashSet<>(Arrays.asList(battle)), position2.getBattles());
    assertEquals(new HashSet<>(Arrays.asList(battle)), roadSection1.getPositions()[0].getBattles());
    assertArrayEquals(new PositionView[]{position2, position1}, battle.getNormalizedPositions());
  }

  @Test
  public void testPositionNormalizationWorks() throws Exception {
    assertEquals(new HashSet<>(Arrays.asList()), position1.getBattles());
    assertEquals(new HashSet<>(Arrays.asList()), position2.getBattles());
    warriorA.setCurrentPosition(roadSection1.getPositions()[0]);
    final BattleView battle = new BattleView(warriorA, warriorB, START_TIME + 2);
    assertEquals(BattleView.State.EXISTING, battle.getState());
    assertArrayEquals(new PrawnView[]{warriorA, warriorB}, battle.getParticipants());
    assertEquals(new Point2D.Double(2.5, 1.0), battle.getPoint());
    assertEquals(START_TIME + 2, battle.getStartTime());
    assertEquals(new HashSet<>(Arrays.asList(battle)), position1.getBattles());
    assertEquals(new HashSet<>(Arrays.asList(battle)), position2.getBattles());
    assertEquals(new HashSet<>(Arrays.asList(battle)), roadSection1.getPositions()[0].getBattles());
    assertEquals(roadSection0, battle.getNormalizedPositions()[0].getRoadSection());
    assertEquals(3, battle.getNormalizedPositions()[0].getIndex());
  }

  @Test
  public void testBattlesFinishedCorrectly() throws Exception {
    assertEquals(new HashSet<>(Arrays.asList()), settlersUnitA.getBattles());
    assertEquals(new HashSet<>(Arrays.asList()), warriorB.getBattles());
    assertEquals(new HashSet<>(Arrays.asList()), position0.getBattles());
    assertEquals(new HashSet<>(Arrays.asList()), position1.getBattles());

    final BattleView battle = new BattleView(settlersUnitA, warriorB, START_TIME + 1);
    assertEquals(BattleView.State.EXISTING, battle.getState());
    assertEquals(START_TIME + 1, battle.getStartTime());
    assertEquals(Long.MAX_VALUE, battle.getEndTime());
    assertEquals(new HashSet<>(Arrays.asList(battle)), settlersUnitA.getBattles());
    assertEquals(new HashSet<>(Arrays.asList(battle)), warriorB.getBattles());
    assertEquals(new HashSet<>(Arrays.asList(battle)), position0.getBattles());
    assertEquals(new HashSet<>(Arrays.asList(battle)), position1.getBattles());

    final BattleView battle1 = new BattleView(warriorA, warriorB, START_TIME + 2);
    assertEquals(BattleView.State.EXISTING, battle1.getState());
    assertEquals(START_TIME + 2, battle1.getStartTime());
    assertEquals(Long.MAX_VALUE, battle1.getEndTime());
    assertEquals(new HashSet<>(Arrays.asList(battle)), settlersUnitA.getBattles());
    assertEquals(new HashSet<>(Arrays.asList(battle, battle1)), warriorB.getBattles());
    assertEquals(new HashSet<>(Arrays.asList(battle1)), warriorA.getBattles());
    assertEquals(new HashSet<>(Arrays.asList(battle)), position0.getBattles());
    assertEquals(new HashSet<>(Arrays.asList(battle, battle1)), position1.getBattles());
    assertEquals(new HashSet<>(Arrays.asList(battle1)), position2.getBattles());
    assertEquals(new HashSet<>(Arrays.asList(battle1)), roadSection1.getPositions()[0].getBattles());

    battle1.finish(START_TIME + 3);
    assertEquals(BattleView.State.FINISHED, battle1.getState());
    assertEquals(START_TIME + 2, battle1.getStartTime());
    assertEquals(START_TIME + 3, battle1.getEndTime());
    assertEquals(new HashSet<>(Arrays.asList(battle)), settlersUnitA.getBattles());
    assertEquals(new HashSet<>(Arrays.asList(battle)), warriorB.getBattles());
    assertEquals(new HashSet<>(Arrays.asList(battle)), position0.getBattles());
    assertEquals(new HashSet<>(Arrays.asList(battle)), position1.getBattles());

    battle.finish(START_TIME + 4);
    assertEquals(BattleView.State.FINISHED, battle.getState());
    assertEquals(START_TIME + 1, battle.getStartTime());
    assertEquals(START_TIME + 4, battle.getEndTime());
    assertEquals(new HashSet<>(Arrays.asList()), settlersUnitA.getBattles());
    assertEquals(new HashSet<>(Arrays.asList()), warriorB.getBattles());
    assertEquals(new HashSet<>(Arrays.asList()), position0.getBattles());
    assertEquals(new HashSet<>(Arrays.asList()), position1.getBattles());
  }

  @Test
  public void testEqualsAndHashCodeWork() throws Exception {
    final BattleView battle = new BattleView(settlersUnitA, warriorB, START_TIME + 1);
    final BattleView battleAtDifferentTime = new BattleView(settlersUnitA, warriorB, START_TIME + 2);
    settlersUnitA.setCurrentPosition(position1);
    warriorB.setCurrentPosition(position0);
    final BattleView battleAtSwappedPositions = new BattleView(settlersUnitA, warriorB, START_TIME + 1);
    settlersUnitA.setCurrentPosition(position0);
    warriorB.setCurrentPosition(position1);

    // Trick done to allow creating the same Battle twice.
    settlersUnitA.getBattles().remove(battle);
    warriorB.getBattles().remove(battle);
    position0.getBattles().remove(battle);
    position1.getBattles().remove(battle);

    final BattleView sameBattle = new BattleView(warriorB, settlersUnitA, START_TIME + 1);
    warriorA.setCurrentPosition(position0);
    final BattleView battleWithDifferentParticipants = new BattleView(warriorA, warriorB, START_TIME + 1);

    assertEquals(battle, battle);
    assertEquals(battle, sameBattle);
    assertEquals(sameBattle, battle);
    assertEquals(battle.hashCode(), battle.hashCode());
    assertEquals(battle.hashCode(), sameBattle.hashCode());

    assertNotEquals(battle, battleAtDifferentTime);
    assertNotEquals(battleAtDifferentTime, battle);
    // Time is not used for calculating hashCode .
    assertEquals(battle.hashCode(), battleAtDifferentTime.hashCode());

    assertNotEquals(battle, battleAtSwappedPositions);
    assertNotEquals(battleAtSwappedPositions, battle);
    assertNotEquals(battle.hashCode(), battleAtSwappedPositions.hashCode());


    assertNotEquals(battle, battleWithDifferentParticipants);
    assertNotEquals(battleWithDifferentParticipants, battle);
    assertNotEquals(battle.hashCode(), battleWithDifferentParticipants.hashCode());

    assertEquals(28689756, battle.hashCode());

    assertEquals(new HashSet<>(Arrays.asList(battle, battleAtDifferentTime, battleAtSwappedPositions)),
        settlersUnitA.getBattles());
    assertEquals(new HashSet<>(
        Arrays.asList(battle, battleAtDifferentTime, battleAtSwappedPositions, battleWithDifferentParticipants)),
        warriorB.getBattles());
    assertEquals(new HashSet<>(Arrays.asList(battleWithDifferentParticipants)), warriorA.getBattles());
  }

}
