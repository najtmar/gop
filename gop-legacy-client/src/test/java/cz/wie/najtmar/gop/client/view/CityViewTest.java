package cz.wie.najtmar.gop.client.view;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import cz.wie.najtmar.gop.common.Constants;
import cz.wie.najtmar.gop.common.Point2D;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.UUID;

public class CityViewTest {

  static final double STEP_SIZE = 1.0;
  static PlayerView PLAYER_A;
  static PlayerView PLAYER_B;

  JointView joint00;
  JointView joint30;
  JointView joint33;
  RoadSectionView roadSection0;
  RoadSectionView roadSection1;
  RoadSectionView roadSection2;
  CityView testCity;

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    PLAYER_A = new PlayerView(new UserView("Player A", new UUID(12345, 67890)), 0);
    PLAYER_B = new PlayerView(new UserView("Player B", new UUID(98765, 43210)), 1);
  }

  /**
   * Method setUp().
   * @throws Exception when something unexpected happens
   */
  @Before
  public void setUp() throws Exception {
    joint00 = new JointView(0, "joint00", new Point2D.Double(0.0, 0.0));
    joint30 = new JointView(1, "joint30", new Point2D.Double(3.0, 0.0));
    joint33 = new JointView(2, "joint33", new Point2D.Double(3.0, 3.0));
    roadSection0 = new RoadSectionView(0, joint00, joint30, STEP_SIZE);
    roadSection1 = new RoadSectionView(1, joint30, joint33, STEP_SIZE);
    roadSection2 = new RoadSectionView(2, joint00, joint33, STEP_SIZE);
    testCity = new CityView("City", 7, joint00, PLAYER_A);
  }

  @Test
  public void testCityCreatedAndBoundCorrectly() {
    assertEquals(1, testCity.getFactories().size());
    assertEquals(0, testCity.getFactories().get(0).getIndex());
    assertEquals(7, testCity.getId());
    assertEquals(CityView.State.EXISTING, testCity.getState());
    assertEquals(CityView.Visibility.INVISIBLE, testCity.getVisibility());
    assertEquals(joint00, testCity.getJoint());
    assertEquals("City", testCity.getName());
    assertEquals(PLAYER_A, testCity.getPlayer());
    assertEquals(testCity, joint00.getCity());
    assertNull(joint30.getCity());
  }

  @Test
  public void testCityGrownAndShrinkedCorrectly() throws Exception {
    assertEquals(1, testCity.getFactories().size());
    testCity.grow();
    assertEquals(2, testCity.getFactories().size());
    assertEquals(1, testCity.getFactories().get(1).getIndex());
    assertEquals(CityView.State.EXISTING, testCity.getState());
    testCity.shrinkOrDestroy(testCity.getFactories().size() - 1);
    assertEquals(1, testCity.getFactories().size());
    assertEquals(0, testCity.getFactories().get(0).getIndex());
    assertEquals(joint00, testCity.getJoint());
    assertEquals(PLAYER_A, testCity.getPlayer());
    assertEquals(testCity, joint00.getCity());
    assertEquals(CityView.State.EXISTING, testCity.getState());
  }

  @Test
  public void testCityShrinkedForNonLastFactoryCorrectly() throws Exception {
    assertEquals(1, testCity.getFactories().size());
    testCity.grow();
    assertEquals(2, testCity.getFactories().size());
    assertEquals(0, testCity.getFactories().get(0).getIndex());
    assertEquals(1, testCity.getFactories().get(1).getIndex());
    assertEquals(CityView.State.EXISTING, testCity.getState());
    testCity.shrinkOrDestroy(0);
    assertEquals(1, testCity.getFactories().size());
    assertEquals(0, testCity.getFactories().get(0).getIndex());
    assertEquals(joint00, testCity.getJoint());
    assertEquals(PLAYER_A, testCity.getPlayer());
    assertEquals(testCity, joint00.getCity());
    assertEquals(CityView.State.EXISTING, testCity.getState());
  }

  @Test
  public void testCityFactoryIndexChangedCorrectly() throws Exception {
    testCity.grow();
    testCity.grow();
    testCity.grow();
    assertEquals(4, testCity.getFactories().size());
    assertEquals(0, testCity.getFirstFactoryShownIndex());
    assertTrue(testCity.tryIncrementFirstFactoryShownIndex(3));
    assertEquals(1, testCity.getFirstFactoryShownIndex());
    assertTrue(testCity.tryIncrementFirstFactoryShownIndex(3));
    assertEquals(2, testCity.getFirstFactoryShownIndex());
    assertFalse(testCity.tryIncrementFirstFactoryShownIndex(3));
    assertEquals(2, testCity.getFirstFactoryShownIndex());
    assertTrue(testCity.tryDecrementFirstFactoryShownIndex());
    assertEquals(1, testCity.getFirstFactoryShownIndex());
    assertTrue(testCity.tryDecrementFirstFactoryShownIndex());
    assertEquals(0, testCity.getFirstFactoryShownIndex());
    assertFalse(testCity.tryDecrementFirstFactoryShownIndex());
    assertEquals(0, testCity.getFirstFactoryShownIndex());
    assertTrue(testCity.tryIncrementFirstFactoryShownIndex(3));
    assertEquals(1, testCity.getFirstFactoryShownIndex());
    testCity.resetFirstFactoryShownIndex();
    assertEquals(0, testCity.getFirstFactoryShownIndex());
  }

  @Test
  public void testCityFactoryIndexChangedCorrectly1() throws Exception {
    testCity.grow();
    testCity.grow();
    testCity.grow();
    assertEquals(4, testCity.getFactories().size());
    assertEquals(0, testCity.getFirstFactoryShownIndex());
    assertFalse(testCity.tryIncrementFirstFactoryShownIndex(4));
    assertEquals(0, testCity.getFirstFactoryShownIndex());
    assertFalse(testCity.tryDecrementFirstFactoryShownIndex());
    assertEquals(0, testCity.getFirstFactoryShownIndex());
  }

  @Test
  public void testCityCapturedCorrectly() throws Exception {
    testCity.grow();
    testCity.getFactories().get(0).setAction(FactoryView.ActionType.PRODUCE_SETTLERS_UNIT, null, -1.0);
    assertEquals(FactoryView.ActionType.PRODUCE_SETTLERS_UNIT, testCity.getFactories().get(0).getActionType());
    assertEquals(FactoryView.State.IN_ACTION, testCity.getFactories().get(0).getState());
    testCity.captureOrDestroy(PLAYER_B);
    assertEquals(PLAYER_B, testCity.getPlayer());
    assertEquals(1, testCity.getFactories().size());
    assertEquals(7, testCity.getId());
    assertEquals(joint00, testCity.getJoint());
    assertEquals("City", testCity.getName());
    assertEquals(testCity, joint00.getCity());
    assertNull(testCity.getFactories().get(0).getActionType());
    assertEquals(FactoryView.State.IDLE, testCity.getFactories().get(0).getState());
  }

  @Test
  public void testCityShrinkedAndDestroyedCorrectly() throws Exception {
    assertEquals(CityView.State.EXISTING, testCity.getState());
    testCity.shrinkOrDestroy(testCity.getFactories().size() - 1);
    assertEquals(0, testCity.getFactories().size());
    assertNull(joint00.getCity());
    assertEquals(CityView.State.REMOVED, testCity.getState());
  }

  @Test
  public void testCityCapturedAndDestroyedCorrectly() throws Exception {
    assertEquals(CityView.State.EXISTING, testCity.getState());
    testCity.captureOrDestroy(PLAYER_B);
    assertEquals(PLAYER_B, testCity.getPlayer());
    assertEquals(0, testCity.getFactories().size());
    assertNull(joint00.getCity());
    assertEquals(CityView.State.REMOVED, testCity.getState());
  }

  @Test
  public void testRoadSectionAnglesCalculatedCorrectly() throws Exception {
    final CityView testCity1 = new CityView("City", 8, joint30, PLAYER_A);
    final CityView testCity2 = new CityView("City", 9, joint33, PLAYER_A);
    assertArrayEquals(new double[]{0.0, Math.PI / 4}, testCity.getRoadSectionAngles(), Constants.DOUBLE_DELTA);
    assertArrayEquals(new double[]{Math.PI, Math.PI / 2}, testCity1.getRoadSectionAngles(), Constants.DOUBLE_DELTA);
    assertArrayEquals(new double[]{3 * Math.PI / 2, 5 * Math.PI / 4},
        testCity2.getRoadSectionAngles(), Constants.DOUBLE_DELTA);
  }

}
