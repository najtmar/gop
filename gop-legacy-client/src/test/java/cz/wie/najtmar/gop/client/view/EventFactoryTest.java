package cz.wie.najtmar.gop.client.view;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import cz.wie.najtmar.gop.board.BoardException;
import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.event.EventProcessingException;

import org.junit.Before;
import org.junit.Test;

import java.io.StringReader;
import java.util.Arrays;
import java.util.List;

import javax.json.Json;

public class EventFactoryTest {

  GameView mockedGame;
  ObjectSerializer mockedObjectSerializer;
  EventFactory eventFactory;

  /**
   * Method setUp().
   * @throws Exception when something goes wrong
   */
  @Before
  public void setUp() throws Exception {
    mockedGame = mock(GameView.class);
    mockedObjectSerializer = mock(ObjectSerializer.class);
    when(mockedObjectSerializer.getGame()).thenReturn(mockedGame);
    when(mockedGame.getTime()).thenReturn(123456L);
    eventFactory = new EventFactory(mockedObjectSerializer);
  }

  @Test
  public void testSetPrawnItineraryEventCreationWorks() throws EventProcessingException, BoardException {
    final PrawnView prawn = mock(PrawnView.class);
    final List<PositionView> nodes = Arrays.asList(mock(PositionView.class), mock(PositionView.class));
    final ItineraryView itinerary = mock(ItineraryView.class);
    when(itinerary.getNodes()).thenReturn(nodes);
    when(mockedObjectSerializer.serializePosition(nodes.get(0))).thenReturn(
        Json.createReader(new StringReader(
            "{ "
          + "  \"roadSection\": 0, "
          + "  \"index\": 1 "
          + "}")).readObject());
    when(mockedObjectSerializer.serializePosition(nodes.get(1))).thenReturn(
        Json.createReader(new StringReader(
            "{ "
          + "  \"roadSection\": 0, "
          + "  \"index\": 2 "
          + "}")).readObject());
    when(mockedObjectSerializer.serializePrawn(prawn)).thenReturn(123);
    final Event setPrawnItineraryEvent = eventFactory.createSetPrawnItineraryEvent(prawn, itinerary);
    assertEquals(Event.Type.SET_PRAWN_ITINERARY, setPrawnItineraryEvent.getType());
    assertEquals(123456, setPrawnItineraryEvent.getTime());
    assertTrue(setPrawnItineraryEvent.toString(), setPrawnItineraryEvent.getBody().equals(
        Json.createReader(new StringReader(
          "{ "
          + "  \"type\": \"SET_PRAWN_ITINERARY\", "
          + "  \"time\": 123456, "
          + "  \"prawn\": 123, "
          + "  \"nodes\": ["
          + "    { "
          + "      \"roadSection\": 0, "
          + "      \"index\": 1 "
          + "    }, "
          + "    { "
          + "      \"roadSection\": 0, "
          + "      \"index\": 2 "
          + "    } "
          + "  ] "
          + "}")).readObject()));
    verify(mockedObjectSerializer, times(1)).serializePosition(nodes.get(0));
    verify(mockedObjectSerializer, times(1)).serializePosition(nodes.get(1));
    verify(mockedObjectSerializer, times(1)).serializePrawn(prawn);
  }

  @Test
  public void testClearPrawnItineraryEventCreationWorks() throws EventProcessingException, BoardException {
    final PrawnView prawn = mock(PrawnView.class);
    when(mockedObjectSerializer.serializePrawn(prawn)).thenReturn(123);
    final Event clearPrawnItineraryEvent = eventFactory.createClearPrawnItineraryEvent(prawn);
    assertEquals(Event.Type.CLEAR_PRAWN_ITINERARY, clearPrawnItineraryEvent.getType());
    assertEquals(123456, clearPrawnItineraryEvent.getTime());
    assertTrue(clearPrawnItineraryEvent.toString(), clearPrawnItineraryEvent.getBody().equals(
        Json.createReader(new StringReader(
          "{ "
          + "  \"type\": \"CLEAR_PRAWN_ITINERARY\", "
          + "  \"time\": 123456, "
          + "  \"prawn\": 123 "
          + "}")).readObject()));
    verify(mockedObjectSerializer, times(1)).serializePrawn(prawn);
  }

  @Test
  public void testAbandonPlayerEventCreationWorks() throws EventProcessingException, BoardException {
    final PlayerView player = mock(PlayerView.class);
    when(mockedObjectSerializer.serializePlayer(player)).thenReturn(2);
    final Event abandonPlayerEvent = eventFactory.createAbandonPlayerEvent(player);
    assertEquals(Event.Type.ABANDON_PLAYER, abandonPlayerEvent.getType());
    assertEquals(123456, abandonPlayerEvent.getTime());
    assertEquals(
        Json.createReader(new StringReader(
          "{ "
          + "  \"type\": \"ABANDON_PLAYER\", "
          + "  \"time\": 123456, "
          + "  \"player\": 2 "
          + "}")).readObject(),
        abandonPlayerEvent.getBody());
    verify(mockedObjectSerializer, times(1)).serializePlayer(player);
  }

  @Test
  public void testSetProduceWarriorActionEventCreationWorks() throws EventProcessingException, BoardException {
    final FactoryView factory = mock(FactoryView.class);
    when(mockedObjectSerializer.serializeFactory(factory)).thenReturn(Json.createReader(new StringReader(
        "{"
        + "  \"city\": 123, "
        + "  \"index\": 2 "
        + "}")).readObject());

    final Event setProduceWarriorActionEvent = eventFactory.createSetProduceWarriorActionEvent(factory, 0.375);
    assertEquals(Event.Type.SET_PRODUCE_WARRIOR_ACTION, setProduceWarriorActionEvent.getType());
    assertEquals(123456, setProduceWarriorActionEvent.getTime());
    assertTrue(setProduceWarriorActionEvent.getBody().equals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"SET_PRODUCE_WARRIOR_ACTION\", "
          + "  \"time\": 123456, "
          + "  \"factory\": { "
          + "    \"city\": 123, "
          + "    \"index\": 2 "
          + "  }, "
          + "  \"direction\": 0.375 "
          + "}")).readObject()));
    verify(mockedObjectSerializer, times(1)).serializeFactory(factory);
  }

  @Test
  public void testSetProduceSettlersUnitActionEventCreationWorks() throws EventProcessingException, BoardException {
    final FactoryView factory = mock(FactoryView.class);
    when(mockedObjectSerializer.serializeFactory(factory)).thenReturn(Json.createReader(new StringReader(
        "{"
        + "  \"city\": 123, "
        + "  \"index\": 2 "
        + "}")).readObject());

    final Event setProduceSettlersUnitActionEvent = eventFactory.createSetProduceSettlersUnitActionEvent(factory);
    assertEquals(Event.Type.SET_PRODUCE_SETTLERS_UNIT_ACTION, setProduceSettlersUnitActionEvent.getType());
    assertEquals(123456, setProduceSettlersUnitActionEvent.getTime());
    assertTrue(setProduceSettlersUnitActionEvent.getBody().equals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"SET_PRODUCE_SETTLERS_UNIT_ACTION\", "
          + "  \"time\": 123456, "
          + "  \"factory\": { "
          + "    \"city\": 123, "
          + "    \"index\": 2 "
          + "  } "
          + "}")).readObject()));
    verify(mockedObjectSerializer, times(1)).serializeFactory(factory);
  }

  @Test
  public void testSetRepairPrawnActionEventCreationWorks() throws EventProcessingException, BoardException {
    final FactoryView factory = mock(FactoryView.class);
    final PrawnView prawn = mock(PrawnView.class);
    when(mockedObjectSerializer.serializeFactory(factory)).thenReturn(Json.createReader(new StringReader(
        "{"
        + "  \"city\": 123, "
        + "  \"index\": 2 "
        + "}")).readObject());
    when(mockedObjectSerializer.serializePrawn(prawn)).thenReturn(5);

    final Event setRepairPrawnActionEvent = eventFactory.createSetRepairPrawnActionEvent(factory, prawn);
    assertEquals(Event.Type.SET_REPAIR_PRAWN_ACTION, setRepairPrawnActionEvent.getType());
    assertEquals(123456, setRepairPrawnActionEvent.getTime());
    assertTrue(setRepairPrawnActionEvent.getBody().equals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"SET_REPAIR_PRAWN_ACTION\", "
          + "  \"time\": 123456, "
          + "  \"factory\": { "
          + "    \"city\": 123, "
          + "    \"index\": 2 "
          + "  }, "
          + "  \"prawn\": 5 "
          + "}")).readObject()));
    verify(mockedObjectSerializer, times(1)).serializeFactory(factory);
    verify(mockedObjectSerializer, times(1)).serializePrawn(prawn);
  }

  @Test
  public void testSetGrowCityActionEventCreationWorks() throws EventProcessingException, BoardException {
    final FactoryView factory = mock(FactoryView.class);
    when(mockedObjectSerializer.serializeFactory(factory)).thenReturn(Json.createReader(new StringReader(
        "{"
        + "  \"city\": 123, "
        + "  \"index\": 2 "
        + "}")).readObject());

    final Event setGrowCityActionEvent = eventFactory.createSetGrowCityActionEvent(factory);
    assertEquals(Event.Type.SET_GROW_CITY_ACTION, setGrowCityActionEvent.getType());
    assertEquals(123456, setGrowCityActionEvent.getTime());
    assertTrue(setGrowCityActionEvent.getBody().equals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"SET_GROW_CITY_ACTION\", "
          + "  \"time\": 123456, "
          + "  \"factory\": { "
          + "    \"city\": 123, "
          + "    \"index\": 2 "
          + "  } "
          + "}")).readObject()));
    verify(mockedObjectSerializer, times(1)).serializeFactory(factory);
  }

  @Test
  public void testOrderCityCreationEventCreationWorks() throws EventProcessingException, BoardException {
    final PrawnView settlersUnit = mock(PrawnView.class);
    when(mockedObjectSerializer.serializePrawn(settlersUnit)).thenReturn(123);

    final Event orderCityCreationEvent = eventFactory.createOrderCityCreationEvent(settlersUnit, "cityName");
    assertEquals(Event.Type.ORDER_CITY_CREATION, orderCityCreationEvent.getType());
    assertEquals(123456, orderCityCreationEvent.getTime());
    assertEquals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"ORDER_CITY_CREATION\", "
          + "  \"time\": 123456, "
          + "  \"settlersUnit\": 123, "
          + "  \"name\": \"cityName\" "
          + "}")).readObject(),
        orderCityCreationEvent.getBody());
    verify(mockedObjectSerializer, times(1)).serializePrawn(settlersUnit);
  }

  @Test
  public void testClientFailureEventCreationWorks() throws EventProcessingException, BoardException {
    final PlayerView player = mock(PlayerView.class);
    when(mockedObjectSerializer.serializePlayer(player)).thenReturn(2);

    final Event setClientFailureEvent = eventFactory.createClientFailureEvent(player, "something went wrong");
    assertEquals(Event.Type.CLIENT_FAILURE, setClientFailureEvent.getType());
    assertEquals(123456, setClientFailureEvent.getTime());
    assertEquals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"CLIENT_FAILURE\", "
          + "  \"time\": 123456, "
          + "  \"player\": 2, "
          + "  \"message\": \"something went wrong\" "
          + "}")).readObject(),
        setClientFailureEvent.getBody());
    verify(mockedObjectSerializer, times(1)).serializePlayer(player);
  }

}
