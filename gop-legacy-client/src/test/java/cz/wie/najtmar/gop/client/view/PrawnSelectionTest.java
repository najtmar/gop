package cz.wie.najtmar.gop.client.view;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import cz.wie.najtmar.gop.client.view.PrawnSelection.PrawnBoxType;
import cz.wie.najtmar.gop.client.view.PrawnView.Type;
import cz.wie.najtmar.gop.common.Point;
import cz.wie.najtmar.gop.common.Point2D;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.UUID;

public class PrawnSelectionTest {

  static final double STEP_SIZE = 1.0;
  static PlayerView PLAYER;

  JointView joint00;
  JointView joint30;
  JointView joint33;
  RoadSectionView roadSection0;
  RoadSectionView roadSection1;
  RoadSectionView roadSection2;
  PrawnView settlersUnit0;
  PrawnView settlersUnit1;
  PrawnView settlersUnit2;
  PrawnView settlersUnit3;
  PrawnView warrior0;
  PrawnView warrior1;
  PrawnView warrior2;
  PrawnView warrior3;
  PositionView testPosition;

  /**
   * Method setUpBeforeClass().
   * @throws Exception when something unexpected happens
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    PLAYER = new PlayerView(new UserView("Player", new UUID(12345, 67890)), 0);
  }

  /**
   * Method setUp().
   * @throws Exception when something unexpected happens
   */
  @Before
  public void setUp() throws Exception {
    joint00 = new JointView(0, "joint00", new Point2D.Double(0.0, 0.0));
    joint30 = new JointView(1, "joint30", new Point2D.Double(3.0, 0.0));
    joint33 = new JointView(2, "joint33", new Point2D.Double(3.0, 3.0));
    roadSection0 = new RoadSectionView(0, joint00, joint30, STEP_SIZE);
    roadSection1 = new RoadSectionView(1, joint30, joint33, STEP_SIZE);
    roadSection2 = new RoadSectionView(2, joint00, joint33, STEP_SIZE);
    settlersUnit0 = new PrawnView(Type.SETTLERS_UNIT, 0.0, PLAYER, 0, joint00.getNormalizedPosition());
    settlersUnit1 = new PrawnView(Type.SETTLERS_UNIT, 0.0, PLAYER, 1, joint00.getNormalizedPosition());
    settlersUnit2 = new PrawnView(Type.SETTLERS_UNIT, 0.0, PLAYER, 2, joint00.getNormalizedPosition());
    settlersUnit3 = new PrawnView(Type.SETTLERS_UNIT, 0.0, PLAYER, 3, joint00.getNormalizedPosition());
    warrior0 = new PrawnView(Type.WARRIOR, 0.0, PLAYER, 4, joint00.getNormalizedPosition());
    warrior1 = new PrawnView(Type.WARRIOR, Math.PI / 2, PLAYER, 5, joint00.getNormalizedPosition());
    warrior2 = new PrawnView(Type.WARRIOR, Math.PI, PLAYER, 6, joint00.getNormalizedPosition());
    warrior3 = new PrawnView(Type.WARRIOR, 3 * Math.PI / 2, PLAYER, 7, joint00.getNormalizedPosition());
    testPosition = roadSection0.getPositions()[2];
    roadSection0.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.VISIBLE);
  }

  @Test
  public void testMoreBoxesThanPrawns() throws Exception {
    settlersUnit0.setCurrentPosition(testPosition);
    settlersUnit1.setCurrentPosition(testPosition);
    settlersUnit2.setCurrentPosition(testPosition);
    settlersUnit3.setCurrentPosition(testPosition);
    warrior0.setCurrentPosition(testPosition);
    warrior1.setCurrentPosition(testPosition);
    final PrawnSelection prawnSelection = new PrawnSelection(
        new HashSet<>(Arrays.asList(settlersUnit3, settlersUnit2, settlersUnit1, settlersUnit0, warrior1, warrior0)),
        4, 2, 20);
    assertEquals(20, prawnSelection.getPrawnBoxSide());
    assertArrayEquals(new PrawnView[]{settlersUnit0, settlersUnit1, settlersUnit2, settlersUnit3, warrior0, warrior1},
        prawnSelection.getPrawns());
    assertEquals(2, prawnSelection.getPrawnSelectionHeight());
    assertEquals(4, prawnSelection.getPrawnSelectionWidth());
    assertEquals(0, prawnSelection.getStartPosition());
    assertEquals(0, prawnSelection.getNextCount());

    assertEquals(PrawnBoxType.PRAWN, prawnSelection.getPrawnBoxes()[0][0].getType());
    assertEquals(settlersUnit0, prawnSelection.getPrawnBoxes()[0][0].getPrawn());
    assertEquals(0, prawnSelection.getPrawnBoxes()[0][0].getCoordX());
    assertEquals(0, prawnSelection.getPrawnBoxes()[0][0].getCoordY());
    assertEquals(settlersUnit1, prawnSelection.getPrawnBoxes()[1][0].getPrawn());
    assertEquals(settlersUnit2, prawnSelection.getPrawnBoxes()[2][0].getPrawn());
    assertEquals(settlersUnit3, prawnSelection.getPrawnBoxes()[3][0].getPrawn());
    assertEquals(warrior0, prawnSelection.getPrawnBoxes()[0][1].getPrawn());
    assertEquals(warrior1, prawnSelection.getPrawnBoxes()[1][1].getPrawn());
    assertEquals(20, prawnSelection.getPrawnBoxes()[1][1].getCoordX());
    assertEquals(20, prawnSelection.getPrawnBoxes()[1][1].getCoordY());
    assertEquals(PrawnBoxType.EMPTY, prawnSelection.getPrawnBoxes()[2][1].getType());
    assertEquals(PrawnBoxType.EMPTY, prawnSelection.getPrawnBoxes()[3][1].getType());
  }

  @Test
  public void testAsManyBoxesAsPrawns() throws Exception {
    settlersUnit0.setCurrentPosition(testPosition);
    settlersUnit1.setCurrentPosition(testPosition);
    settlersUnit2.setCurrentPosition(testPosition);
    settlersUnit3.setCurrentPosition(testPosition);
    final PrawnSelection prawnSelection = new PrawnSelection(
        new HashSet<>(Arrays.asList(settlersUnit2, settlersUnit1, settlersUnit0, settlersUnit3)), 2, 2, 20);
    assertEquals(20, prawnSelection.getPrawnBoxSide());
    assertArrayEquals(new PrawnView[]{settlersUnit0, settlersUnit1, settlersUnit2, settlersUnit3},
        prawnSelection.getPrawns());
    assertEquals(0, prawnSelection.getStartPosition());
    assertEquals(0, prawnSelection.getNextCount());

    assertEquals(settlersUnit0, prawnSelection.getPrawnBoxes()[0][0].getPrawn());
    assertEquals(settlersUnit1, prawnSelection.getPrawnBoxes()[1][0].getPrawn());
    assertEquals(settlersUnit2, prawnSelection.getPrawnBoxes()[0][1].getPrawn());
    assertEquals(settlersUnit3, prawnSelection.getPrawnBoxes()[1][1].getPrawn());
  }

  @Test
  public void testFewerBoxesThanPrawns() throws Exception {
    settlersUnit0.setCurrentPosition(testPosition);
    settlersUnit1.setCurrentPosition(testPosition);
    settlersUnit2.setCurrentPosition(testPosition);
    settlersUnit3.setCurrentPosition(testPosition);
    warrior0.setCurrentPosition(testPosition);
    warrior1.setCurrentPosition(testPosition);
    warrior2.setCurrentPosition(testPosition);
    final PrawnSelection prawnSelection = new PrawnSelection(
        new HashSet<>(Arrays.asList(settlersUnit3, settlersUnit2, settlersUnit1, settlersUnit0, warrior2, warrior1,
            warrior0)), 2, 2, 20);
    assertEquals(20, prawnSelection.getPrawnBoxSide());
    assertArrayEquals(
        new PrawnView[]{settlersUnit0, settlersUnit1, settlersUnit2, settlersUnit3, warrior0, warrior1, warrior2},
        prawnSelection.getPrawns());

    assertEquals(0, prawnSelection.getStartPosition());
    assertEquals(4, prawnSelection.getNextCount());
    assertEquals(settlersUnit0, prawnSelection.getPrawnBoxes()[0][0].getPrawn());
    assertEquals(settlersUnit1, prawnSelection.getPrawnBoxes()[1][0].getPrawn());
    assertEquals(settlersUnit2, prawnSelection.getPrawnBoxes()[0][1].getPrawn());
    assertEquals(PrawnBoxType.RIGHT_SLIDER, prawnSelection.getPrawnBoxes()[1][1].getType());

    assertTrue(prawnSelection.tryIncreaseStartPosition());
    assertEquals(3, prawnSelection.getStartPosition());
    assertEquals(2, prawnSelection.getNextCount());
    assertEquals(PrawnBoxType.LEFT_SLIDER, prawnSelection.getPrawnBoxes()[0][0].getType());
    assertEquals(settlersUnit3, prawnSelection.getPrawnBoxes()[1][0].getPrawn());
    assertEquals(warrior0, prawnSelection.getPrawnBoxes()[0][1].getPrawn());
    assertEquals(PrawnBoxType.RIGHT_SLIDER, prawnSelection.getPrawnBoxes()[1][1].getType());

    assertTrue(prawnSelection.tryIncreaseStartPosition());
    assertEquals(5, prawnSelection.getStartPosition());
    assertEquals(0, prawnSelection.getNextCount());
    assertEquals(PrawnBoxType.LEFT_SLIDER, prawnSelection.getPrawnBoxes()[0][0].getType());
    assertEquals(warrior1, prawnSelection.getPrawnBoxes()[1][0].getPrawn());
    assertEquals(warrior2, prawnSelection.getPrawnBoxes()[0][1].getPrawn());
    assertEquals(PrawnBoxType.EMPTY, prawnSelection.getPrawnBoxes()[1][1].getType());

    assertFalse(prawnSelection.tryIncreaseStartPosition());
    assertEquals(5, prawnSelection.getStartPosition());
    assertEquals(0, prawnSelection.getNextCount());
    assertEquals(PrawnBoxType.LEFT_SLIDER, prawnSelection.getPrawnBoxes()[0][0].getType());
    assertEquals(warrior1, prawnSelection.getPrawnBoxes()[1][0].getPrawn());
    assertEquals(warrior2, prawnSelection.getPrawnBoxes()[0][1].getPrawn());
    assertEquals(PrawnBoxType.EMPTY, prawnSelection.getPrawnBoxes()[1][1].getType());

    assertTrue(prawnSelection.tryDecreaseStartPosition());
    assertEquals(3, prawnSelection.getStartPosition());
    assertEquals(2, prawnSelection.getNextCount());
    assertEquals(PrawnBoxType.LEFT_SLIDER, prawnSelection.getPrawnBoxes()[0][0].getType());
    assertEquals(settlersUnit3, prawnSelection.getPrawnBoxes()[1][0].getPrawn());
    assertEquals(warrior0, prawnSelection.getPrawnBoxes()[0][1].getPrawn());
    assertEquals(PrawnBoxType.RIGHT_SLIDER, prawnSelection.getPrawnBoxes()[1][1].getType());

    assertTrue(prawnSelection.tryDecreaseStartPosition());
    assertEquals(0, prawnSelection.getStartPosition());
    assertEquals(4, prawnSelection.getNextCount());
    assertEquals(settlersUnit0, prawnSelection.getPrawnBoxes()[0][0].getPrawn());
    assertEquals(settlersUnit1, prawnSelection.getPrawnBoxes()[1][0].getPrawn());
    assertEquals(settlersUnit2, prawnSelection.getPrawnBoxes()[0][1].getPrawn());
    assertEquals(PrawnBoxType.RIGHT_SLIDER, prawnSelection.getPrawnBoxes()[1][1].getType());

    assertFalse(prawnSelection.tryDecreaseStartPosition());
    assertEquals(0, prawnSelection.getStartPosition());
    assertEquals(4, prawnSelection.getNextCount());
    assertEquals(settlersUnit0, prawnSelection.getPrawnBoxes()[0][0].getPrawn());
    assertEquals(settlersUnit1, prawnSelection.getPrawnBoxes()[1][0].getPrawn());
    assertEquals(settlersUnit2, prawnSelection.getPrawnBoxes()[0][1].getPrawn());
    assertEquals(PrawnBoxType.RIGHT_SLIDER, prawnSelection.getPrawnBoxes()[1][1].getType());
  }

  @Test
  public void testFewerBoxesThanPrawns1() throws Exception {
    settlersUnit0.setCurrentPosition(testPosition);
    settlersUnit1.setCurrentPosition(testPosition);
    settlersUnit2.setCurrentPosition(testPosition);
    settlersUnit3.setCurrentPosition(testPosition);
    warrior0.setCurrentPosition(testPosition);
    final PrawnSelection prawnSelection = new PrawnSelection(
        new HashSet<>(Arrays.asList(settlersUnit3, settlersUnit2, settlersUnit1, settlersUnit0, warrior0)), 2, 2, 20);
    assertEquals(20, prawnSelection.getPrawnBoxSide());
    assertArrayEquals(
        new PrawnView[]{settlersUnit0, settlersUnit1, settlersUnit2, settlersUnit3, warrior0},
        prawnSelection.getPrawns());

    assertEquals(0, prawnSelection.getStartPosition());
    assertEquals(2, prawnSelection.getNextCount());
    assertEquals(settlersUnit0, prawnSelection.getPrawnBoxes()[0][0].getPrawn());
    assertEquals(settlersUnit1, prawnSelection.getPrawnBoxes()[1][0].getPrawn());
    assertEquals(settlersUnit2, prawnSelection.getPrawnBoxes()[0][1].getPrawn());
    assertEquals(PrawnBoxType.RIGHT_SLIDER, prawnSelection.getPrawnBoxes()[1][1].getType());

    assertTrue(prawnSelection.tryIncreaseStartPosition());
    assertEquals(3, prawnSelection.getStartPosition());
    assertEquals(0, prawnSelection.getNextCount());
    assertEquals(PrawnBoxType.LEFT_SLIDER, prawnSelection.getPrawnBoxes()[0][0].getType());
    assertEquals(settlersUnit3, prawnSelection.getPrawnBoxes()[1][0].getPrawn());
    assertEquals(warrior0, prawnSelection.getPrawnBoxes()[0][1].getPrawn());
    assertEquals(PrawnBoxType.EMPTY, prawnSelection.getPrawnBoxes()[1][1].getType());

    assertFalse(prawnSelection.tryIncreaseStartPosition());
    assertEquals(3, prawnSelection.getStartPosition());
    assertEquals(0, prawnSelection.getNextCount());
    assertEquals(PrawnBoxType.LEFT_SLIDER, prawnSelection.getPrawnBoxes()[0][0].getType());
    assertEquals(settlersUnit3, prawnSelection.getPrawnBoxes()[1][0].getPrawn());
    assertEquals(warrior0, prawnSelection.getPrawnBoxes()[0][1].getPrawn());
    assertEquals(PrawnBoxType.EMPTY, prawnSelection.getPrawnBoxes()[1][1].getType());

    assertTrue(prawnSelection.tryDecreaseStartPosition());
    assertEquals(0, prawnSelection.getStartPosition());
    assertEquals(2, prawnSelection.getNextCount());
    assertEquals(settlersUnit0, prawnSelection.getPrawnBoxes()[0][0].getPrawn());
    assertEquals(settlersUnit1, prawnSelection.getPrawnBoxes()[1][0].getPrawn());
    assertEquals(settlersUnit2, prawnSelection.getPrawnBoxes()[0][1].getPrawn());
    assertEquals(PrawnBoxType.RIGHT_SLIDER, prawnSelection.getPrawnBoxes()[1][1].getType());

    assertFalse(prawnSelection.tryDecreaseStartPosition());
    assertEquals(0, prawnSelection.getStartPosition());
    assertEquals(2, prawnSelection.getNextCount());
    assertEquals(settlersUnit0, prawnSelection.getPrawnBoxes()[0][0].getPrawn());
    assertEquals(settlersUnit1, prawnSelection.getPrawnBoxes()[1][0].getPrawn());
    assertEquals(settlersUnit2, prawnSelection.getPrawnBoxes()[0][1].getPrawn());
    assertEquals(PrawnBoxType.RIGHT_SLIDER, prawnSelection.getPrawnBoxes()[1][1].getType());
  }

  @Test
  public void testGetPrawnBoxIndexesWorks() throws Exception {
    settlersUnit0.setCurrentPosition(testPosition);
    settlersUnit1.setCurrentPosition(testPosition);
    settlersUnit2.setCurrentPosition(testPosition);
    settlersUnit3.setCurrentPosition(testPosition);
    final PrawnSelection prawnSelection = new PrawnSelection(
        new HashSet<>(Arrays.asList(settlersUnit2, settlersUnit1, settlersUnit0, settlersUnit3)), 2, 2, 20);
    assertEquals(new Point(0, 0), prawnSelection.getPrawnBoxIndexes(settlersUnit0));
    assertEquals(new Point(1, 0), prawnSelection.getPrawnBoxIndexes(settlersUnit1));
    assertEquals(new Point(0, 1), prawnSelection.getPrawnBoxIndexes(settlersUnit2));
    assertEquals(new Point(1, 1), prawnSelection.getPrawnBoxIndexes(settlersUnit3));
  }

}
