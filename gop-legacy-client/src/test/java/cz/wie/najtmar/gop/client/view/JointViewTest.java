package cz.wie.najtmar.gop.client.view;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import cz.wie.najtmar.gop.common.Point2D;

import org.junit.Before;
import org.junit.Test;

public class JointViewTest {

  static final double STEP_SIZE = 1.0;

  JointView joint00;
  JointView joint001;
  JointView joint30;
  JointView joint33;
  RoadSectionView roadSection0;

  /**
   * Method setUp().
   * @throws Exception when test cannot be set up
   */
  @Before
  public void setUp() throws Exception {
    joint00 = new JointView(0, "joint00", new Point2D.Double(0.0, 0.0));
    joint001 = new JointView(0, "joint001", new Point2D.Double(0.0, 0.0));
    joint30 = new JointView(1, "joint30", new Point2D.Double(3.0, 0.0));
    joint33 = new JointView(1, "joint33", new Point2D.Double(3.0, 3.0));
    roadSection0 = new RoadSectionView(0, joint30, joint33, STEP_SIZE);
  }

  @Test
  public void testComparisonWorks() {
    assertTrue(joint00.equals(joint00));
    assertFalse(joint00.equals(joint001));
    assertFalse(joint00.equals(joint30));
    assertFalse(joint00.equals(roadSection0));
  }

  @Test
  public void testDeterminingNormalizedPositionWorks() throws Exception {
    assertEquals(roadSection0.getPositions()[3], joint33.getNormalizedPosition());
    assertEquals(null, joint00.getNormalizedPosition());
    final RoadSectionView roadSection1 = new RoadSectionView(0, joint00, joint30, STEP_SIZE);
    assertEquals(roadSection1.getPositions()[0], joint00.getNormalizedPosition());
    assertEquals(roadSection1, joint00.getNormalizedPosition().getRoadSection());
    @SuppressWarnings("unused")
    final RoadSectionView roadSection2 = new RoadSectionView(1, joint00, joint33, STEP_SIZE);
    assertEquals(roadSection1.getPositions()[0], joint00.getNormalizedPosition());
    assertEquals(roadSection1, joint00.getNormalizedPosition().getRoadSection());
  }

}
