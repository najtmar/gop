package cz.wie.najtmar.gop.client.view;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import cz.wie.najtmar.gop.common.Point2D;

import org.junit.Before;
import org.junit.Test;

import java.io.StringReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.UUID;

import javax.json.Json;
import javax.json.JsonObject;

/**
 * Tests for class GameView.
 * @author najtmar
 */
public class GameViewTest {

  static final long START_TIME = 966;
  static final double STEP_SIZE = 0.5;
  static final JsonObject SIMPLE_GAME_SERIALIZED = Json.createReader(new StringReader(
      "{\n"
      + "  \"id\": \"00000000-0000-5ba0-0000-000000013435\", "
      + "  \"timestamp\": 1234567, "
      + "  \"board\": {\n"
      + "    \"properties\": [\n"
      + "      {\n"
      + "        \"key\": \"Board.ysize\",\n"
      + "        \"value\":\"10\"\n"
      + "      },\n"
      + "      {\n"
      + "        \"key\": \"Board.stepSize\",\n"
      + "        \"value\":\"" + STEP_SIZE + "\"\n"
      + "      },\n"
      + "      {\n"
      + "        \"key\": \"Board.xsize\",\n"
      + "        \"value\":\"15\"\n"
      + "      }\n"
      + "    ],\n"
      + "    \"joints\": [\n"
      + "      {\n"
      + "        \"index\": 0,\n"
      + "        \"name\": \"joint0\",\n"
      + "        \"x\": 0.0,\n"
      + "        \"y\": 0.0\n"
      + "      },\n"
      + "      {\n"
      + "        \"index\": 1,\n"
      + "        \"name\": \"joint1\",\n"
      + "        \"x\": 3.0,\n"
      + "        \"y\": 0.0\n"
      + "      },\n"
      + "      {\n"
      + "        \"index\": 2,\n"
      + "        \"name\": \"joint2\",\n"
      + "        \"x\": 0.0,\n"
      + "        \"y\": 3.0\n"
      + "      }\n"
      + "    ],\n"
      + "    \"roadSections\": [\n"
      + "      {\n"
      + "        \"index\": 0,\n"
      + "        \"joint0\": 0,\n"
      + "        \"joint1\": 1\n"
      + "      },\n"
      + "      {\n"
      + "        \"index\": 1,\n"
      + "        \"joint0\": 0,\n"
      + "        \"joint1\": 2\n"
      + "      }\n"
      + "    ]\n"
      + "  },\n"
      + "  \"players\": [\n"
      + "    {\n"
      + "      \"index\": 0, "
      + "      \"user\": { "
      + "        \"name\":\"playerA\", "
      + "        \"uuid\": \"00000000-0000-3039-0000-000000010932\" "
      + "      } "
      + "    },\n"
      + "    {\n"
      + "      \"index\": 1, "
      + "      \"user\": { "
      + "        \"name\":\"playerB\", "
      + "        \"uuid\": \"00000000-0001-81cd-0000-00000000a8ca\" "
      + "      } "
      + "    }\n"
      + "  ]\n"
      + "}"
      )).readObject();

  static final int MOCKED_BOARD_WIDTH = 400;
  static final int MOCKED_HEIGHT = 300;

  GameView gameView;
  PlayerView playerA;
  PlayerView playerB;
  PrawnView settlersUnitA;
  PrawnView warriorB;
  PrawnView warriorB1;
  CityView cityA;

  GameView mockedGame;

  PositionView position0x0;
  PositionView position100x100;
  PositionView position300x200;
  PositionView position399x299;

  PlayerView mockedPlayer;

  PrawnView mockedPrawn;

  /**
   * Method setUp().
   * @throws Exception when field creation fails
   */
  @Before
  public void setUp() throws Exception {
    gameView = new GameView(SIMPLE_GAME_SERIALIZED, 1);
    playerA = new PlayerView(new UserView("playerA", new UUID(12345, 67890)), 0);
    playerB = new PlayerView(new UserView("playerB", new UUID(98765, 43210)), 1);
    settlersUnitA =
        new PrawnView(PrawnView.Type.SETTLERS_UNIT, 0.0, playerA, 0, gameView.getJoints()[0].getNormalizedPosition());
    warriorB =
        new PrawnView(
            PrawnView.Type.WARRIOR, Math.PI / 2.0, playerB, 1, gameView.getJoints()[1].getNormalizedPosition());
    warriorB1 =
        new PrawnView(
            PrawnView.Type.WARRIOR, Math.PI, playerB, 2, gameView.getJoints()[2].getNormalizedPosition());
    cityA = new CityView("cityA", 0, gameView.getJoints()[2], playerA);

    mockedGame = spy(new GameView(SIMPLE_GAME_SERIALIZED, 0));
    when(mockedGame.getXsize()).thenReturn(MOCKED_BOARD_WIDTH);
    when(mockedGame.getYsize()).thenReturn(MOCKED_HEIGHT);

    position0x0 = mock(PositionView.class);
    when(position0x0.getCoordinatesPair()).thenReturn(new Point2D.Double(0, 0));
    position100x100 = mock(PositionView.class);
    when(position100x100.getCoordinatesPair()).thenReturn(new Point2D.Double(100, 100));
    position300x200 = mock(PositionView.class);
    when(position300x200.getCoordinatesPair()).thenReturn(new Point2D.Double(300, 200));
    position399x299 = mock(PositionView.class);
    when(position399x299.getCoordinatesPair()).thenReturn(new Point2D.Double(399, 299));

    mockedPrawn = mock(PrawnView.class);
    when(mockedGame.getPlayers()).thenReturn(new PlayerView[]{mockedPlayer});
    when(mockedGame.getPlayerPrawns(mockedPlayer)).thenReturn(new HashSet<>(Arrays.asList(mockedPrawn)));
  }

  @Test
  public void testGameCreationWorks() throws Exception {
    assertEquals(UUID.fromString("00000000-0000-5ba0-0000-000000013435"), gameView.getId());
    assertEquals(1234567, gameView.getTimestamp());

    // joints
    assertEquals(3, gameView.getJoints().length);
    assertEquals(0, gameView.getJoints()[0].getIndex());
    assertEquals(1, gameView.getJoints()[1].getIndex());
    assertEquals(2, gameView.getJoints()[2].getIndex());
    assertEquals(new Point2D.Double(0.0, 0.0), gameView.getJoints()[0].getCoordinatesPair());
    assertEquals(new Point2D.Double(3.0, 0.0), gameView.getJoints()[1].getCoordinatesPair());
    assertEquals(new Point2D.Double(0.0, 3.0), gameView.getJoints()[2].getCoordinatesPair());

    // roadSections
    assertEquals(2, gameView.getRoadSections().length);
    assertEquals(0, gameView.getRoadSections()[0].getIndex());
    {
      final RoadSectionView roadSection = gameView.getRoadSections()[0];
      assertEquals(7, roadSection.getPositions().length);
      for (int i = 0; i < roadSection.getPositions().length; ++i) {
        assertEquals("Index " + i, i, roadSection.getPositions()[i].getIndex());
        assertEquals("Index " + i, new Point2D.Double(i * STEP_SIZE, 0.0),
            roadSection.getPositions()[i].getCoordinatesPair());
      }
    }
    {
      final RoadSectionView roadSection = gameView.getRoadSections()[1];
      assertEquals(7, roadSection.getPositions().length);
      for (int i = 0; i < roadSection.getPositions().length; ++i) {
        assertEquals("Index " + i, i, roadSection.getPositions()[i].getIndex());
        assertEquals("Index " + i, new Point2D.Double(0.0, i * STEP_SIZE),
            roadSection.getPositions()[i].getCoordinatesPair());
      }
    }

    // players
    assertArrayEquals(new PlayerView[]{playerA, playerB}, gameView.getPlayers());
  }

  @Test
  public void testAddingRemovingPrawnsWorks() throws Exception {
    assertEquals(new HashSet<>(), gameView.getPlayerPrawns(playerA));
    assertEquals(new HashSet<>(), gameView.getPlayerPrawns(playerB));
    assertNull(gameView.lookupPrawnById(settlersUnitA.getId()));
    assertNull(gameView.lookupPrawnById(warriorB.getId()));
    assertEquals(new HashSet<>(Arrays.asList(settlersUnitA)),
        gameView.getJoints()[0].getNormalizedPosition().getPrawns());
    assertEquals(PrawnView.Visibility.INVISIBLE, settlersUnitA.getVisibility());
    gameView.addPrawn(settlersUnitA);
    assertEquals(PrawnView.Visibility.INVISIBLE, settlersUnitA.getVisibility());  // An opponent's Prawn .
    assertEquals(settlersUnitA, gameView.lookupPrawnById(settlersUnitA.getId()));
    assertEquals(new HashSet<>(Arrays.asList(settlersUnitA)), gameView.getPlayerPrawns(playerA));
    assertEquals(new HashSet<>(Arrays.asList(settlersUnitA)),
        gameView.getJoints()[0].getNormalizedPosition().getPrawns());
    assertEquals(new HashSet<>(Arrays.asList(warriorB)), gameView.getJoints()[1].getNormalizedPosition().getPrawns());
    assertEquals(PrawnView.Visibility.INVISIBLE, warriorB.getVisibility());
    gameView.addPrawn(warriorB);
    assertEquals(PrawnView.Visibility.VISIBLE, warriorB.getVisibility());  // Own Prawn .
    assertEquals(warriorB, gameView.lookupPrawnById(warriorB.getId()));
    assertEquals(new HashSet<>(Arrays.asList(warriorB)), gameView.getPlayerPrawns(playerB));
    assertEquals(new HashSet<>(Arrays.asList(warriorB)), gameView.getJoints()[1].getNormalizedPosition().getPrawns());
    warriorB.remove();
    assertEquals(warriorB, gameView.lookupPrawnById(warriorB.getId()));
    assertEquals(new HashSet<>(Arrays.asList(warriorB)), gameView.getPlayerPrawns(playerB));
    assertEquals(PrawnView.State.REMOVED, warriorB.getState());
    assertEquals(new HashSet<>(), gameView.getJoints()[1].getNormalizedPosition().getPrawns());
    settlersUnitA.remove();
    assertEquals(settlersUnitA, gameView.lookupPrawnById(settlersUnitA.getId()));
    assertEquals(new HashSet<>(Arrays.asList(settlersUnitA)), gameView.getPlayerPrawns(playerA));
    assertEquals(PrawnView.State.REMOVED, settlersUnitA.getState());
    assertEquals(new HashSet<>(), gameView.getJoints()[0].getNormalizedPosition().getPrawns());
  }

  @Test
  public void testAddingRemovingCitiesWorks() throws Exception {
    assertNull(gameView.lookupCityById(0));
    assertEquals(new HashSet<>(), gameView.getPlayerCities(playerA));
    assertEquals(new HashSet<>(), gameView.getCities());
    assertEquals(CityView.Visibility.INVISIBLE, cityA.getVisibility());
    gameView.addCity(cityA);
    assertEquals(CityView.Visibility.INVISIBLE, cityA.getVisibility());  // An opponent's City .
    assertNotNull(gameView.lookupCityById(0));
    assertEquals(cityA, gameView.lookupCityById(0));
    assertEquals(new HashSet<>(Arrays.asList(cityA)), gameView.getPlayerCities(playerA));
    assertEquals(new HashSet<>(Arrays.asList(cityA)), gameView.getCities());
    cityA.shrinkOrDestroy(cityA.getFactories().size() - 1);
    gameView.removeCity(cityA);
    assertEquals(CityView.Visibility.INVISIBLE, cityA.getVisibility());  // An opponent's invisible City .
    assertEquals(cityA, gameView.lookupCityById(0));
    assertEquals(CityView.State.REMOVED, cityA.getState());
    assertEquals(new HashSet<>(), gameView.getPlayerCities(playerA));
    assertEquals(new HashSet<>(Arrays.asList(cityA)), gameView.getCities());

    final CityView cityB = new CityView("cityB", 1, gameView.getJoints()[1], playerB);
    assertEquals(CityView.Visibility.INVISIBLE, cityA.getVisibility());
    gameView.addCity(cityB);
    assertEquals(CityView.Visibility.DISCOVERED, cityB.getVisibility());  // Own City .
    cityB.shrinkOrDestroy(cityB.getFactories().size() - 1);
    gameView.removeCity(cityB);
    assertEquals(CityView.Visibility.DESTROYED, cityB.getVisibility());  // Own City .
  }

  @Test
  public void testCityCapturingWorks() throws Exception {
    assertNull(gameView.lookupCityById(0));
    assertEquals(new HashSet<>(), gameView.getPlayerCities(playerA));
    assertEquals(new HashSet<>(), gameView.getCities());
    gameView.addCity(cityA);
    assertNotNull(gameView.lookupCityById(0));
    assertEquals(cityA, gameView.lookupCityById(0));
    assertEquals(new HashSet<>(Arrays.asList(cityA)), gameView.getPlayerCities(playerA));
    assertEquals(new HashSet<>(), gameView.getPlayerCities(playerB));
    assertEquals(new HashSet<>(Arrays.asList(cityA)), gameView.getCities());
    gameView.captureCity(cityA, playerA, playerB);
    assertEquals(new HashSet<>(), gameView.getPlayerCities(playerA));
    assertEquals(new HashSet<>(Arrays.asList(cityA)), gameView.getPlayerCities(playerB));
    assertEquals(new HashSet<>(Arrays.asList(cityA)), gameView.getCities());
  }

  @Test
  public void testBattleManagementWorks() throws Exception {
    settlersUnitA.setCurrentPosition(gameView.getRoadSections()[0].getPositions()[1]);
    warriorB.setCurrentPosition(gameView.getRoadSections()[0].getPositions()[2]);
    warriorB1.setCurrentPosition(gameView.getRoadSections()[1].getPositions()[0]);

    final BattleView battleAvsB = new BattleView(settlersUnitA, warriorB, START_TIME + 1);
    assertEquals(new LinkedList<>(), gameView.getBattles());
    gameView.addBattle(battleAvsB);
    assertEquals(Arrays.asList(battleAvsB), gameView.getBattles());
    assertEquals(new HashSet<>(Arrays.asList(battleAvsB)), gameView.getMatchingBattles(settlersUnitA, warriorB,
        gameView.getRoadSections()[0].getPositions()[1], gameView.getRoadSections()[0].getPositions()[2]));
    // Orders of arguments does matter.
    assertEquals(new HashSet<>(), gameView.getMatchingBattles(warriorB, settlersUnitA,
        gameView.getRoadSections()[0].getPositions()[2], gameView.getRoadSections()[0].getPositions()[1]));

    assertEquals(BattleView.State.EXISTING, gameView.getBattles().get(0).getState());
    battleAvsB.finish(START_TIME + 2);
    assertEquals(BattleView.State.FINISHED, gameView.getBattles().get(0).getState());

    final BattleView battleAvsBv1 = new BattleView(settlersUnitA, warriorB, START_TIME + 3);
    gameView.addBattle(battleAvsBv1);
    assertEquals(Arrays.asList(battleAvsB, battleAvsBv1), gameView.getBattles());
    assertEquals(new HashSet<>(Arrays.asList(battleAvsB, battleAvsBv1)),
        gameView.getMatchingBattles(settlersUnitA, warriorB, gameView.getRoadSections()[0].getPositions()[1],
            gameView.getRoadSections()[0].getPositions()[2]));

    final BattleView battleAvsB1 = new BattleView(settlersUnitA, warriorB1, START_TIME + 4);
    gameView.addBattle(battleAvsB1);
    assertEquals(Arrays.asList(battleAvsB, battleAvsBv1, battleAvsB1), gameView.getBattles());
    assertEquals(new HashSet<>(Arrays.asList(battleAvsB, battleAvsBv1)),
        gameView.getMatchingBattles(settlersUnitA, warriorB, gameView.getRoadSections()[0].getPositions()[1],
            gameView.getRoadSections()[0].getPositions()[2]));
    assertEquals(new HashSet<>(Arrays.asList(battleAvsB1)),
        gameView.getMatchingBattles(settlersUnitA, warriorB1, gameView.getRoadSections()[0].getPositions()[1],
            gameView.getRoadSections()[1].getPositions()[0]));
    // Positions on Joints auto-normalized.
    assertEquals(new HashSet<>(Arrays.asList(battleAvsB1)),
        gameView.getMatchingBattles(settlersUnitA, warriorB1, gameView.getRoadSections()[0].getPositions()[1],
            gameView.getRoadSections()[0].getPositions()[0]));
  }

  @Test
  public void testCapturedRoadSectionHalvesOpponentsCity() throws Exception {
    gameView.addCity(cityA);
    final CityView cityB = new CityView("cityB", 1, gameView.getJoints()[0], playerB);
    gameView.addCity(cityB);
    assertEquals(1, gameView.calculateCapturedRoadSectionHalvesCount(gameView.getRoadSections()[1], cityB));
    assertEquals(0, gameView.calculateCapturedRoadSectionHalvesCount(gameView.getRoadSections()[0], cityB));
  }

  @Test
  public void testCapturedRoadSectionHalvesOpponentsPrawnBackward() throws Exception {
    settlersUnitA.setCurrentPosition(gameView.getRoadSections()[1].getPositions()[1]);
    gameView.addPrawn(settlersUnitA);
    final CityView cityB = new CityView("cityB", 1, gameView.getJoints()[0], playerB);
    gameView.addCity(cityB);
    assertEquals(2, gameView.calculateCapturedRoadSectionHalvesCount(gameView.getRoadSections()[1], cityB));
    assertEquals(0, gameView.calculateCapturedRoadSectionHalvesCount(gameView.getRoadSections()[0], cityB));
    settlersUnitA.setCurrentPosition(gameView.getRoadSections()[1].getPositions()[2]);
    assertEquals(2, gameView.calculateCapturedRoadSectionHalvesCount(gameView.getRoadSections()[1], cityB));
    settlersUnitA.setCurrentPosition(gameView.getRoadSections()[1].getPositions()[3]);
    assertEquals(2, gameView.calculateCapturedRoadSectionHalvesCount(gameView.getRoadSections()[1], cityB));
    settlersUnitA.setCurrentPosition(gameView.getRoadSections()[1].getPositions()[4]);
    assertEquals(1, gameView.calculateCapturedRoadSectionHalvesCount(gameView.getRoadSections()[1], cityB));
    assertEquals(0, gameView.calculateCapturedRoadSectionHalvesCount(gameView.getRoadSections()[0], cityB));
  }

  @Test
  public void testCapturedRoadSectionHalvesOwnPrawnIgnored() throws Exception {
    warriorB.setCurrentPosition(gameView.getRoadSections()[1].getPositions()[1]);
    gameView.addPrawn(warriorB);
    final CityView cityB = new CityView("cityB", 1, gameView.getJoints()[0], playerB);
    gameView.addCity(cityB);
    assertEquals(0, gameView.calculateCapturedRoadSectionHalvesCount(gameView.getRoadSections()[1], cityB));
    warriorB.setCurrentPosition(gameView.getRoadSections()[1].getPositions()[4]);
    assertEquals(0, gameView.calculateCapturedRoadSectionHalvesCount(gameView.getRoadSections()[1], cityB));
  }

  @Test
  public void testCapturedRoadSectionHalvesOpponentsPrawnForward() throws Exception {
    settlersUnitA.setCurrentPosition(gameView.getRoadSections()[0].getPositions()[5]);
    gameView.addPrawn(settlersUnitA);
    final CityView cityB = new CityView("cityB", 1, gameView.getJoints()[1], playerB);
    gameView.addCity(cityB);
    assertEquals(2, gameView.calculateCapturedRoadSectionHalvesCount(gameView.getRoadSections()[0], cityB));
    settlersUnitA.setCurrentPosition(gameView.getRoadSections()[0].getPositions()[4]);
    assertEquals(2, gameView.calculateCapturedRoadSectionHalvesCount(gameView.getRoadSections()[0], cityB));
    settlersUnitA.setCurrentPosition(gameView.getRoadSections()[0].getPositions()[3]);
    assertEquals(2, gameView.calculateCapturedRoadSectionHalvesCount(gameView.getRoadSections()[0], cityB));
    settlersUnitA.setCurrentPosition(gameView.getRoadSections()[0].getPositions()[2]);
    assertEquals(1, gameView.calculateCapturedRoadSectionHalvesCount(gameView.getRoadSections()[0], cityB));
  }

  @Test
  public void testCapturedRoadSectionHalvesMultipleOpponentsPrawns() throws Exception {
    settlersUnitA.setCurrentPosition(gameView.getRoadSections()[1].getPositions()[5]);
    gameView.addPrawn(settlersUnitA);
    PrawnView warriorA =
        new PrawnView(
            PrawnView.Type.WARRIOR, Math.PI / 2.0, playerA, 3, gameView.getJoints()[1].getNormalizedPosition());
    warriorA.setCurrentPosition(gameView.getRoadSections()[1].getPositions()[4]);
    gameView.addPrawn(warriorA);
    final CityView cityB = new CityView("cityB", 1, gameView.getJoints()[0], playerB);
    gameView.addCity(cityB);
    assertEquals(1, gameView.calculateCapturedRoadSectionHalvesCount(gameView.getRoadSections()[1], cityB));
    warriorA.setCurrentPosition(gameView.getRoadSections()[1].getPositions()[3]);
    assertEquals(2, gameView.calculateCapturedRoadSectionHalvesCount(gameView.getRoadSections()[1], cityB));
  }

  @Test
  public void testCapturedRoadSectionHalvesOwnCity() throws Exception {
    final CityView cityB1 = new CityView("cityB1", 2, gameView.getJoints()[1], playerB);
    gameView.addCity(cityB1);
    final CityView cityB = new CityView("cityB", 1, gameView.getJoints()[0], playerB);
    gameView.addCity(cityB);
    assertEquals(1, gameView.calculateCapturedRoadSectionHalvesCount(gameView.getRoadSections()[0], cityB));
    assertEquals(0, gameView.calculateCapturedRoadSectionHalvesCount(gameView.getRoadSections()[1], cityB));
  }

  @Test
  public void testCapturedRoadSectionHalvesRemovedOpponentsPrawnIgnored() throws Exception {
    settlersUnitA.setCurrentPosition(gameView.getRoadSections()[1].getPositions()[1]);
    gameView.addPrawn(settlersUnitA);
    final CityView cityB = new CityView("cityB", 1, gameView.getJoints()[0], playerB);
    gameView.addCity(cityB);
    assertEquals(2, gameView.calculateCapturedRoadSectionHalvesCount(gameView.getRoadSections()[1], cityB));
    settlersUnitA.remove();
    assertEquals(0, gameView.calculateCapturedRoadSectionHalvesCount(gameView.getRoadSections()[1], cityB));
  }

  @Test
  public void testCapturedRoadSectionHalvesRemovedOpponentsCityIgnored() throws Exception {
    gameView.addCity(cityA);
    final CityView cityB = new CityView("cityB", 1, gameView.getJoints()[0], playerB);
    gameView.addCity(cityB);
    assertEquals(1, gameView.calculateCapturedRoadSectionHalvesCount(gameView.getRoadSections()[1], cityB));
    cityA.shrinkOrDestroy(cityA.getFactories().size() - 1);
    gameView.removeCity(cityA);
    assertEquals(0, gameView.calculateCapturedRoadSectionHalvesCount(gameView.getRoadSections()[1], cityB));
    assertTrue(gameView.getCities().contains(cityA));
  }

  @Test
  public void testCalculateScrolledBoardBarInitialSetup0x0() {
    when(mockedPrawn.getCurrentPosition()).thenReturn(position0x0);

    assertEquals(new Point2D.Double(0.0, 0.0), mockedGame.calculateScrolledBoardBarInitialSetup(100, 100));
    assertEquals(new Point2D.Double(0.0, 0.0),
        mockedGame.calculateScrolledBoardBarInitialSetup(MOCKED_BOARD_WIDTH, MOCKED_HEIGHT));
  }

  @Test
  public void testCalculateScrolledBoardBarInitialSetup100x100() {
    when(mockedPrawn.getCurrentPosition()).thenReturn(position100x100);

    assertEquals(new Point2D.Double(0.0, 0.0),
        mockedGame.calculateScrolledBoardBarInitialSetup(MOCKED_BOARD_WIDTH, MOCKED_HEIGHT));
    assertEquals(new Point2D.Double(0.16666666666666666, 0.25),
        mockedGame.calculateScrolledBoardBarInitialSetup(100, 100));
    assertEquals(new Point2D.Double(0.0, 0.0), mockedGame.calculateScrolledBoardBarInitialSetup(200, 200));
  }

  @Test
  public void testCalculateScrolledBoardBarInitialSetup300x200() {
    when(mockedPrawn.getCurrentPosition()).thenReturn(position300x200);

    assertEquals(new Point2D.Double(0.0, 0.0),
        mockedGame.calculateScrolledBoardBarInitialSetup(MOCKED_BOARD_WIDTH, MOCKED_HEIGHT));
    assertEquals(new Point2D.Double(1 - 0.16666666666666666, 1 - 0.25),
        mockedGame.calculateScrolledBoardBarInitialSetup(100, 100));
    assertEquals(new Point2D.Double(1 - 0.0, 1 - 0.0), mockedGame.calculateScrolledBoardBarInitialSetup(200, 200));
  }

  @Test
  public void testCalculateScrolledBoardBarInitialSetup399x299() {
    when(mockedPrawn.getCurrentPosition()).thenReturn(position399x299);

    assertEquals(new Point2D.Double(0.0, 0.0),
        mockedGame.calculateScrolledBoardBarInitialSetup(MOCKED_BOARD_WIDTH, MOCKED_HEIGHT));
    assertEquals(new Point2D.Double(1.0, 1.0), mockedGame.calculateScrolledBoardBarInitialSetup(100, 100));
    assertEquals(new Point2D.Double(1.0, 1.0), mockedGame.calculateScrolledBoardBarInitialSetup(200, 200));
  }

}
