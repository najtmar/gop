package cz.wie.najtmar.gop.client.view;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import cz.wie.najtmar.gop.common.Constants;
import cz.wie.najtmar.gop.common.Point2D;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class RoadSectionViewTest {

  static final double STEP_SIZE = 1.0;

  JointView joint11;
  JointView joint14;
  JointView joint21;
  JointView joint41;
  JointView joint4P1;
  JointView joint4M1;
  JointView joint44;

  /**
   * setUp method.
   * @throws Exception when something unexpected happens
   */
  @Before
  public void setUp() throws Exception {
    joint11 = new JointView(0, "joint11", new Point2D.Double(1.0, 1.0));
    joint14 = new JointView(1, "joint14", new Point2D.Double(1.0, 4.0));
    joint21 = new JointView(2, "joint21", new Point2D.Double(2.0, 1.0));
    joint41 = new JointView(3, "joint41", new Point2D.Double(4.0, 1.0));
    joint4P1 = new JointView(4, "joint4P1", new Point2D.Double(4.0 + 0.1, 1.0));
    joint4M1 = new JointView(5, "joint4M1", new Point2D.Double(4.0 - 0.1, 1.0));
    joint44 = new JointView(6, "joint44", new Point2D.Double(4.0, 4.0));
  }

  @Test
  public void testHorizontalLine() throws Exception {
    assertEquals(new ArrayList<>(), joint11.getRoadSections());
    assertEquals(new ArrayList<>(), joint41.getRoadSections());
    RoadSectionView horizontalLine = new RoadSectionView(0, joint11, joint41, STEP_SIZE);
    assertEquals(new ArrayList<>(Arrays.asList(horizontalLine)), joint11.getRoadSections());
    assertEquals(new ArrayList<>(Arrays.asList(horizontalLine)), joint41.getRoadSections());
    assertEquals(0, horizontalLine.getIndex());
    assertArrayEquals(new JointView[]{joint11, joint41}, horizontalLine.getJoints());
    assertEquals(4, horizontalLine.getPositions().length);
    assertEquals(0, horizontalLine.getPositions()[0].getIndex());
    assertEquals(1, horizontalLine.getPositions()[1].getIndex());
    assertEquals(2, horizontalLine.getPositions()[2].getIndex());
    assertEquals(3, horizontalLine.getPositions()[3].getIndex());
    assertEquals(new Point2D.Double(1.0, 1.0), horizontalLine.getPositions()[0].getCoordinatesPair());
    assertEquals(new Point2D.Double(2.0, 1.0), horizontalLine.getPositions()[1].getCoordinatesPair());
    assertEquals(new Point2D.Double(3.0, 1.0), horizontalLine.getPositions()[2].getCoordinatesPair());
    assertEquals(new Point2D.Double(4.0, 1.0), horizontalLine.getPositions()[3].getCoordinatesPair());
    assertEquals(new Point2D.Double(1.0, 1.0), horizontalLine.calculateCoordinatesPair(0));
    assertEquals(new Point2D.Double(4.0, 1.0), horizontalLine.calculateCoordinatesPair(3));
    assertEquals(new Point2D.Double(0.0, 1.0), horizontalLine.calculateCoordinatesPair(0 - 1));
    assertEquals(new Point2D.Double(5.0, 1.0), horizontalLine.calculateCoordinatesPair(3 + 1));
    assertEquals(3.0, horizontalLine.getLength(), 0.0);
    assertArrayEquals(new int[]{0, 3}, horizontalLine.getJointPositionIndexes());
    assertArrayEquals(new int[]{1, 2}, horizontalLine.getJointAdjacentPointIndexes());
    assertEquals(2, horizontalLine.getMidpointIndex());
    assertEquals(STEP_SIZE, horizontalLine.getRealStepSize(), Constants.DOUBLE_DELTA);
    assertEquals(new Point2D.Double(3.0, 0.0), horizontalLine.getVector());
    assertArrayEquals(new Point2D.Double[]{new Point2D.Double(2.0, 0.0), new Point2D.Double(1.0, 0.0), },
        horizontalLine.getHalfVectors());
    assertArrayEquals(new double[]{2.0, 1.0}, horizontalLine.getHalfLengths(), 0.0);

    // Whole RoadSection VISIBLE .
    horizontalLine.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.VISIBLE);
    horizontalLine.setHalfVisibility(RoadSectionView.Direction.FORWARD, RoadSectionView.Visibility.VISIBLE);
    assertEquals(horizontalLine.getPositions()[2],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(3.0, 2.0)));
    assertEquals(horizontalLine.getPositions()[2],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(2.6, 2.0)));
    assertEquals(horizontalLine.getPositions()[2],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(3.4, 2.0)));
    assertEquals(horizontalLine.getPositions()[1],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(2.4, 2.0)));
    assertEquals(horizontalLine.getPositions()[3],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(3.6, 2.0)));
    assertEquals(horizontalLine.getPositions()[0],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(1.0, 2.0)));
    assertEquals(horizontalLine.getPositions()[0],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(0.0, 2.0)));
    assertEquals(horizontalLine.getPositions()[0],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(-1.0, 2.0)));
    assertEquals(horizontalLine.getPositions()[3],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(4.0, 2.0)));
    assertEquals(horizontalLine.getPositions()[3],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(5.0, 2.0)));
    assertEquals(horizontalLine.getPositions()[3],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(6.0, 2.0)));
    assertEquals(horizontalLine.getPositions()[2],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(3.0, 0.0)));
    assertEquals(horizontalLine.getPositions()[2],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(2.6, 0.0)));
    assertEquals(horizontalLine.getPositions()[2],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(3.4, 0.0)));
    assertEquals(horizontalLine.getPositions()[1],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(2.4, 0.0)));
    assertEquals(horizontalLine.getPositions()[3],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(3.6, 0.0)));
    assertEquals(horizontalLine.getPositions()[0],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(1.0, 0.0)));
    assertEquals(horizontalLine.getPositions()[0],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(0.0, 0.0)));
    assertEquals(horizontalLine.getPositions()[0],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(-1.0, 0.0)));
    assertEquals(horizontalLine.getPositions()[3],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(4.0, 0.0)));
    assertEquals(horizontalLine.getPositions()[3],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(5.0, 0.0)));
    assertEquals(horizontalLine.getPositions()[3],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(6.0, 0.0)));
    assertEquals(horizontalLine.getPositions()[2],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(3.0, 1.0)));
    assertEquals(horizontalLine.getPositions()[2],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(2.6, 1.0)));
    assertEquals(horizontalLine.getPositions()[2],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(3.4, 1.0)));
    assertEquals(horizontalLine.getPositions()[1],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(2.4, 1.0)));
    assertEquals(horizontalLine.getPositions()[3],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(3.6, 1.0)));
    assertEquals(horizontalLine.getPositions()[0],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(1.0, 1.0)));
    assertEquals(horizontalLine.getPositions()[0],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(0.0, 1.0)));
    assertEquals(horizontalLine.getPositions()[0],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(-1.0, 1.0)));
    assertEquals(horizontalLine.getPositions()[3],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(4.0, 1.0)));
    assertEquals(horizontalLine.getPositions()[3],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(5.0, 1.0)));
    assertEquals(horizontalLine.getPositions()[3],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(6.0, 1.0)));

    // BACKWARD RoadSection.Half VISIBLE .
    horizontalLine.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.DISCOVERED);
    horizontalLine.setHalfVisibility(RoadSectionView.Direction.FORWARD, RoadSectionView.Visibility.INVISIBLE);
    assertEquals(horizontalLine.getPositions()[2],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(3.0, 3.0)));
    assertEquals(horizontalLine.getPositions()[2],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(4.0, 3.0)));
    assertEquals(horizontalLine.getPositions()[0],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(1.0, 3.0)));
    assertEquals(horizontalLine.getPositions()[0],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(0.0, 3.0)));

    // FORWARD RoadSection.Half VISIBLE .
    horizontalLine.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.INVISIBLE);
    horizontalLine.setHalfVisibility(RoadSectionView.Direction.FORWARD, RoadSectionView.Visibility.VISIBLE);
    assertEquals(horizontalLine.getPositions()[3],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(4.0, 3.0)));
    assertEquals(horizontalLine.getPositions()[3],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(5.0, 3.0)));
    assertEquals(horizontalLine.getPositions()[2],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(2.0, 3.0)));
    assertEquals(horizontalLine.getPositions()[2],
        horizontalLine.getClosestVisiblePosition(new Point2D.Double(1.0, 3.0)));

    // Whole RoadSection INVISIBLE .
    horizontalLine.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.INVISIBLE);
    horizontalLine.setHalfVisibility(RoadSectionView.Direction.FORWARD, RoadSectionView.Visibility.INVISIBLE);
    assertNull(horizontalLine.getClosestVisiblePosition(new Point2D.Double(4.0, 3.0)));
    assertNull(horizontalLine.getClosestVisiblePosition(new Point2D.Double(5.0, 3.0)));
    assertNull(horizontalLine.getClosestVisiblePosition(new Point2D.Double(1.0, 3.0)));
    assertNull(horizontalLine.getClosestVisiblePosition(new Point2D.Double(0.0, 3.0)));
  }

  @Test
  public void testTwoLines() throws Exception {
    assertEquals(new ArrayList<>(), joint11.getRoadSections());
    assertEquals(new ArrayList<>(), joint14.getRoadSections());
    assertEquals(new ArrayList<>(), joint41.getRoadSections());
    RoadSectionView horizontalLine = new RoadSectionView(0, joint11, joint41, STEP_SIZE);
    RoadSectionView verticalLine = new RoadSectionView(0, joint11, joint14, STEP_SIZE);
    assertEquals(new ArrayList<>(Arrays.asList(horizontalLine, verticalLine)), joint11.getRoadSections());
    assertEquals(new ArrayList<>(Arrays.asList(verticalLine)), joint14.getRoadSections());
    assertEquals(new ArrayList<>(Arrays.asList(horizontalLine)), joint41.getRoadSections());
    assertEquals(STEP_SIZE, horizontalLine.getRealStepSize(), Constants.DOUBLE_DELTA);
    assertEquals(STEP_SIZE, verticalLine.getRealStepSize(), Constants.DOUBLE_DELTA);
    assertEquals(new Point2D.Double(3.0, 0.0), horizontalLine.getVector());
    assertEquals(new Point2D.Double(0.0, 3.0), verticalLine.getVector());
    assertArrayEquals(new Point2D.Double[]{new Point2D.Double(0.0, 2.0), new Point2D.Double(0.0, 1.0), },
        verticalLine.getHalfVectors());
    assertArrayEquals(new double[]{2.0, 1.0}, verticalLine.getHalfLengths(), 0.0);
  }

  @Test
  public void testReverseHorizontalLine() throws Exception {
    RoadSectionView reverseHorizontalLine = new RoadSectionView(0, joint41, joint11, STEP_SIZE);
    assertEquals(0, reverseHorizontalLine.getIndex());
    assertArrayEquals(new JointView[]{joint41, joint11}, reverseHorizontalLine.getJoints());
    assertEquals(4, reverseHorizontalLine.getPositions().length);
    assertEquals(0, reverseHorizontalLine.getPositions()[0].getIndex());
    assertEquals(1, reverseHorizontalLine.getPositions()[1].getIndex());
    assertEquals(2, reverseHorizontalLine.getPositions()[2].getIndex());
    assertEquals(3, reverseHorizontalLine.getPositions()[3].getIndex());
    assertEquals(new Point2D.Double(4.0, 1.0), reverseHorizontalLine.getPositions()[0].getCoordinatesPair());
    assertEquals(new Point2D.Double(3.0, 1.0), reverseHorizontalLine.getPositions()[1].getCoordinatesPair());
    assertEquals(new Point2D.Double(2.0, 1.0), reverseHorizontalLine.getPositions()[2].getCoordinatesPair());
    assertEquals(new Point2D.Double(1.0, 1.0), reverseHorizontalLine.getPositions()[3].getCoordinatesPair());
    assertArrayEquals(new int[]{0, 3}, reverseHorizontalLine.getJointPositionIndexes());
    assertArrayEquals(new int[]{1, 2}, reverseHorizontalLine.getJointAdjacentPointIndexes());
    assertEquals(2, reverseHorizontalLine.getMidpointIndex());
    assertEquals(STEP_SIZE, reverseHorizontalLine.getRealStepSize(), Constants.DOUBLE_DELTA);
    assertEquals(new Point2D.Double(-3.0, 0.0), reverseHorizontalLine.getVector());
    assertArrayEquals(new double[]{2.0, 1.0}, reverseHorizontalLine.getHalfLengths(), 0.0);

    // Whole RoadSection visible.
    reverseHorizontalLine.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.VISIBLE);
    reverseHorizontalLine.setHalfVisibility(RoadSectionView.Direction.FORWARD, RoadSectionView.Visibility.VISIBLE);
    assertEquals(reverseHorizontalLine.getPositions()[1],
        reverseHorizontalLine.getClosestVisiblePosition(new Point2D.Double(3.0, 2.0)));
    assertEquals(reverseHorizontalLine.getPositions()[1],
        reverseHorizontalLine.getClosestVisiblePosition(new Point2D.Double(2.6, 2.0)));
    assertEquals(reverseHorizontalLine.getPositions()[1],
        reverseHorizontalLine.getClosestVisiblePosition(new Point2D.Double(3.4, 2.0)));
    assertEquals(reverseHorizontalLine.getPositions()[2],
        reverseHorizontalLine.getClosestVisiblePosition(new Point2D.Double(2.4, 2.0)));
    assertEquals(reverseHorizontalLine.getPositions()[0],
        reverseHorizontalLine.getClosestVisiblePosition(new Point2D.Double(3.6, 2.0)));
    assertEquals(reverseHorizontalLine.getPositions()[3],
        reverseHorizontalLine.getClosestVisiblePosition(new Point2D.Double(1.0, 2.0)));
    assertEquals(reverseHorizontalLine.getPositions()[3],
        reverseHorizontalLine.getClosestVisiblePosition(new Point2D.Double(0.0, 2.0)));
    assertEquals(reverseHorizontalLine.getPositions()[0],
        reverseHorizontalLine.getClosestVisiblePosition(new Point2D.Double(4.0, 2.0)));
    assertEquals(reverseHorizontalLine.getPositions()[0],
        reverseHorizontalLine.getClosestVisiblePosition(new Point2D.Double(5.0, 2.0)));
    assertEquals(reverseHorizontalLine.getPositions()[1],
        reverseHorizontalLine.getClosestVisiblePosition(new Point2D.Double(3.0, 0.0)));
    assertEquals(reverseHorizontalLine.getPositions()[1],
        reverseHorizontalLine.getClosestVisiblePosition(new Point2D.Double(2.6, 0.0)));
    assertEquals(reverseHorizontalLine.getPositions()[1],
        reverseHorizontalLine.getClosestVisiblePosition(new Point2D.Double(3.4, 0.0)));
    assertEquals(reverseHorizontalLine.getPositions()[2],
        reverseHorizontalLine.getClosestVisiblePosition(new Point2D.Double(2.4, 0.0)));
    assertEquals(reverseHorizontalLine.getPositions()[0],
        reverseHorizontalLine.getClosestVisiblePosition(new Point2D.Double(3.6, 0.0)));
    assertEquals(reverseHorizontalLine.getPositions()[3],
        reverseHorizontalLine.getClosestVisiblePosition(new Point2D.Double(1.0, 0.0)));
    assertEquals(reverseHorizontalLine.getPositions()[3],
        reverseHorizontalLine.getClosestVisiblePosition(new Point2D.Double(0.0, 0.0)));
    assertEquals(reverseHorizontalLine.getPositions()[0],
        reverseHorizontalLine.getClosestVisiblePosition(new Point2D.Double(4.0, 0.0)));
    assertEquals(reverseHorizontalLine.getPositions()[0],
        reverseHorizontalLine.getClosestVisiblePosition(new Point2D.Double(5.0, 0.0)));

    // BACKWARD RoadSection.Half VISIBLE .
    reverseHorizontalLine.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.DISCOVERED);
    reverseHorizontalLine.setHalfVisibility(RoadSectionView.Direction.FORWARD, RoadSectionView.Visibility.INVISIBLE);
    assertEquals(reverseHorizontalLine.getPositions()[0],
        reverseHorizontalLine.getClosestVisiblePosition(new Point2D.Double(4.0, 3.0)));
    assertEquals(reverseHorizontalLine.getPositions()[0],
        reverseHorizontalLine.getClosestVisiblePosition(new Point2D.Double(5.0, 3.0)));
    assertEquals(reverseHorizontalLine.getPositions()[2],
        reverseHorizontalLine.getClosestVisiblePosition(new Point2D.Double(2.0, 3.0)));
    assertEquals(reverseHorizontalLine.getPositions()[2],
        reverseHorizontalLine.getClosestVisiblePosition(new Point2D.Double(1.0, 3.0)));

    // FORWARD RoadSection.Half VISIBLE .
    reverseHorizontalLine.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.INVISIBLE);
    reverseHorizontalLine.setHalfVisibility(RoadSectionView.Direction.FORWARD, RoadSectionView.Visibility.VISIBLE);
    assertEquals(reverseHorizontalLine.getPositions()[2],
        reverseHorizontalLine.getClosestVisiblePosition(new Point2D.Double(2.0, 3.0)));
    assertEquals(reverseHorizontalLine.getPositions()[2],
        reverseHorizontalLine.getClosestVisiblePosition(new Point2D.Double(3.0, 3.0)));
    assertEquals(reverseHorizontalLine.getPositions()[3],
        reverseHorizontalLine.getClosestVisiblePosition(new Point2D.Double(1.0, 3.0)));
    assertEquals(reverseHorizontalLine.getPositions()[3],
        reverseHorizontalLine.getClosestVisiblePosition(new Point2D.Double(0.0, 3.0)));

    // Whole RoadSection INVISIBLE .
    reverseHorizontalLine.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.INVISIBLE);
    reverseHorizontalLine.setHalfVisibility(RoadSectionView.Direction.FORWARD, RoadSectionView.Visibility.INVISIBLE);
    assertNull(reverseHorizontalLine.getClosestVisiblePosition(new Point2D.Double(4.0, 3.0)));
    assertNull(reverseHorizontalLine.getClosestVisiblePosition(new Point2D.Double(5.0, 3.0)));
    assertNull(reverseHorizontalLine.getClosestVisiblePosition(new Point2D.Double(1.0, 3.0)));
    assertNull(reverseHorizontalLine.getClosestVisiblePosition(new Point2D.Double(0.0, 3.0)));
  }

  @Test
  public void testVerticalLine() throws Exception {
    RoadSectionView verticalLine = new RoadSectionView(0, joint11, joint14, STEP_SIZE);
    assertEquals(0, verticalLine.getIndex());
    assertArrayEquals(new JointView[]{joint11, joint14}, verticalLine.getJoints());
    assertEquals(4, verticalLine.getPositions().length);
    assertArrayEquals(new int[]{0, 3}, verticalLine.getJointPositionIndexes());
    assertArrayEquals(new int[]{1, 2}, verticalLine.getJointAdjacentPointIndexes());
    assertEquals(2, verticalLine.getMidpointIndex());
    assertEquals(STEP_SIZE, verticalLine.getRealStepSize(), Constants.DOUBLE_DELTA);
    assertEquals(new Point2D.Double(0.0, 3.0), verticalLine.getVector());

    // Whole RoadSection VISIBLE .
    verticalLine.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.VISIBLE);
    verticalLine.setHalfVisibility(RoadSectionView.Direction.FORWARD, RoadSectionView.Visibility.VISIBLE);
    assertEquals(verticalLine.getPositions()[2], verticalLine.getClosestVisiblePosition(new Point2D.Double(2.0, 3.0)));
    assertEquals(verticalLine.getPositions()[2], verticalLine.getClosestVisiblePosition(new Point2D.Double(2.0, 2.6)));
    assertEquals(verticalLine.getPositions()[2], verticalLine.getClosestVisiblePosition(new Point2D.Double(2.0, 3.4)));
    assertEquals(verticalLine.getPositions()[1], verticalLine.getClosestVisiblePosition(new Point2D.Double(2.0, 2.4)));
    assertEquals(verticalLine.getPositions()[3], verticalLine.getClosestVisiblePosition(new Point2D.Double(2.0, 3.6)));
    assertEquals(verticalLine.getPositions()[0], verticalLine.getClosestVisiblePosition(new Point2D.Double(2.0, 1.0)));
    assertEquals(verticalLine.getPositions()[0], verticalLine.getClosestVisiblePosition(new Point2D.Double(2.0, 0.0)));
    assertEquals(verticalLine.getPositions()[3], verticalLine.getClosestVisiblePosition(new Point2D.Double(2.0, 4.0)));
    assertEquals(verticalLine.getPositions()[3], verticalLine.getClosestVisiblePosition(new Point2D.Double(2.0, 5.0)));
    assertEquals(verticalLine.getPositions()[2], verticalLine.getClosestVisiblePosition(new Point2D.Double(0.0, 3.0)));
    assertEquals(verticalLine.getPositions()[2], verticalLine.getClosestVisiblePosition(new Point2D.Double(0.0, 2.6)));
    assertEquals(verticalLine.getPositions()[2], verticalLine.getClosestVisiblePosition(new Point2D.Double(0.0, 3.4)));
    assertEquals(verticalLine.getPositions()[1], verticalLine.getClosestVisiblePosition(new Point2D.Double(0.0, 2.4)));
    assertEquals(verticalLine.getPositions()[3], verticalLine.getClosestVisiblePosition(new Point2D.Double(0.0, 3.6)));
    assertEquals(verticalLine.getPositions()[0], verticalLine.getClosestVisiblePosition(new Point2D.Double(0.0, 1.0)));
    assertEquals(verticalLine.getPositions()[0], verticalLine.getClosestVisiblePosition(new Point2D.Double(0.0, 0.0)));
    assertEquals(verticalLine.getPositions()[3], verticalLine.getClosestVisiblePosition(new Point2D.Double(0.0, 4.0)));
    assertEquals(verticalLine.getPositions()[3], verticalLine.getClosestVisiblePosition(new Point2D.Double(0.0, 5.0)));

    // BACKWARD RoadSection.Half VISIBLE .
    verticalLine.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.DISCOVERED);
    verticalLine.setHalfVisibility(RoadSectionView.Direction.FORWARD, RoadSectionView.Visibility.INVISIBLE);
    assertEquals(verticalLine.getPositions()[2], verticalLine.getClosestVisiblePosition(new Point2D.Double(-1.0, 3.0)));
    assertEquals(verticalLine.getPositions()[2], verticalLine.getClosestVisiblePosition(new Point2D.Double(-1.0, 4.0)));
    assertEquals(verticalLine.getPositions()[0], verticalLine.getClosestVisiblePosition(new Point2D.Double(-1.0, 1.0)));
    assertEquals(verticalLine.getPositions()[0], verticalLine.getClosestVisiblePosition(new Point2D.Double(-1.0, 0.0)));

    // FORWARD RoadSection.Half VISIBLE .
    verticalLine.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.INVISIBLE);
    verticalLine.setHalfVisibility(RoadSectionView.Direction.FORWARD, RoadSectionView.Visibility.VISIBLE);
    assertEquals(verticalLine.getPositions()[3], verticalLine.getClosestVisiblePosition(new Point2D.Double(-1.0, 4.0)));
    assertEquals(verticalLine.getPositions()[3], verticalLine.getClosestVisiblePosition(new Point2D.Double(-1.0, 5.0)));
    assertEquals(verticalLine.getPositions()[2], verticalLine.getClosestVisiblePosition(new Point2D.Double(-1.0, 2.0)));
    assertEquals(verticalLine.getPositions()[2], verticalLine.getClosestVisiblePosition(new Point2D.Double(-1.0, 1.0)));

    // Whole RoadSection INVISIBLE .
    verticalLine.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.INVISIBLE);
    verticalLine.setHalfVisibility(RoadSectionView.Direction.FORWARD, RoadSectionView.Visibility.INVISIBLE);
    assertNull(verticalLine.getClosestVisiblePosition(new Point2D.Double(-1.0, 4.0)));
    assertNull(verticalLine.getClosestVisiblePosition(new Point2D.Double(-1.0, 5.0)));
    assertNull(verticalLine.getClosestVisiblePosition(new Point2D.Double(-1.0, 1.0)));
    assertNull(verticalLine.getClosestVisiblePosition(new Point2D.Double(-1.0, 0.0)));
  }

  @Test
  public void testReverseVerticalLine() throws Exception {
    RoadSectionView reverseVerticalLine = new RoadSectionView(0, joint14, joint11, STEP_SIZE);
    assertEquals(0, reverseVerticalLine.getIndex());
    assertArrayEquals(new JointView[]{joint14, joint11}, reverseVerticalLine.getJoints());
    assertEquals(4, reverseVerticalLine.getPositions().length);
    assertArrayEquals(new int[]{0, 3}, reverseVerticalLine.getJointPositionIndexes());
    assertArrayEquals(new int[]{1, 2}, reverseVerticalLine.getJointAdjacentPointIndexes());
    assertEquals(2, reverseVerticalLine.getMidpointIndex());
    assertEquals(STEP_SIZE, reverseVerticalLine.getRealStepSize(), Constants.DOUBLE_DELTA);
    assertEquals(new Point2D.Double(0.0, -3.0), reverseVerticalLine.getVector());

    reverseVerticalLine.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.VISIBLE);
    reverseVerticalLine.setHalfVisibility(RoadSectionView.Direction.FORWARD, RoadSectionView.Visibility.VISIBLE);
    assertEquals(reverseVerticalLine.getPositions()[1],
        reverseVerticalLine.getClosestVisiblePosition(new Point2D.Double(2.0, 3.0)));
    assertEquals(reverseVerticalLine.getPositions()[1],
        reverseVerticalLine.getClosestVisiblePosition(new Point2D.Double(2.0, 2.6)));
    assertEquals(reverseVerticalLine.getPositions()[1],
        reverseVerticalLine.getClosestVisiblePosition(new Point2D.Double(2.0, 3.4)));
    assertEquals(reverseVerticalLine.getPositions()[2],
        reverseVerticalLine.getClosestVisiblePosition(new Point2D.Double(2.0, 2.4)));
    assertEquals(reverseVerticalLine.getPositions()[0],
        reverseVerticalLine.getClosestVisiblePosition(new Point2D.Double(2.0, 3.6)));
    assertEquals(reverseVerticalLine.getPositions()[3],
        reverseVerticalLine.getClosestVisiblePosition(new Point2D.Double(2.0, 1.0)));
    assertEquals(reverseVerticalLine.getPositions()[3],
        reverseVerticalLine.getClosestVisiblePosition(new Point2D.Double(2.0, 0.0)));
    assertEquals(reverseVerticalLine.getPositions()[0],
        reverseVerticalLine.getClosestVisiblePosition(new Point2D.Double(2.0, 4.0)));
    assertEquals(reverseVerticalLine.getPositions()[0],
        reverseVerticalLine.getClosestVisiblePosition(new Point2D.Double(2.0, 5.0)));
    assertEquals(reverseVerticalLine.getPositions()[1],
        reverseVerticalLine.getClosestVisiblePosition(new Point2D.Double(0.0, 3.0)));
    assertEquals(reverseVerticalLine.getPositions()[1],
        reverseVerticalLine.getClosestVisiblePosition(new Point2D.Double(0.0, 2.6)));
    assertEquals(reverseVerticalLine.getPositions()[1],
        reverseVerticalLine.getClosestVisiblePosition(new Point2D.Double(0.0, 3.4)));
    assertEquals(reverseVerticalLine.getPositions()[2],
        reverseVerticalLine.getClosestVisiblePosition(new Point2D.Double(0.0, 2.4)));
    assertEquals(reverseVerticalLine.getPositions()[0],
        reverseVerticalLine.getClosestVisiblePosition(new Point2D.Double(0.0, 3.6)));
    assertEquals(reverseVerticalLine.getPositions()[3],
        reverseVerticalLine.getClosestVisiblePosition(new Point2D.Double(0.0, 1.0)));
    assertEquals(reverseVerticalLine.getPositions()[3],
        reverseVerticalLine.getClosestVisiblePosition(new Point2D.Double(0.0, 0.0)));
    assertEquals(reverseVerticalLine.getPositions()[0],
        reverseVerticalLine.getClosestVisiblePosition(new Point2D.Double(0.0, 4.0)));
    assertEquals(reverseVerticalLine.getPositions()[0],
        reverseVerticalLine.getClosestVisiblePosition(new Point2D.Double(0.0, 5.0)));
  }

  @Test
  public void testLongerLine() throws Exception {
    RoadSectionView longerLine = new RoadSectionView(0, joint11, joint4P1, STEP_SIZE);
    assertArrayEquals(new JointView[]{joint11, joint4P1}, longerLine.getJoints());
    assertEquals(3 + 1, longerLine.getPositions().length);
    assertEquals(new Point2D.Double(1.0, 1.0), longerLine.getPositions()[0].getCoordinatesPair());
    assertEquals(new Point2D.Double(2.033333333333333, 1.0), longerLine.getPositions()[1].getCoordinatesPair());
    assertEquals(new Point2D.Double(3.0666666666666664, 1.0), longerLine.getPositions()[2].getCoordinatesPair());
    assertEquals(new Point2D.Double(4.0 + 0.1, 1.0), longerLine.getPositions()[3].getCoordinatesPair());
    assertArrayEquals(new int[]{0, 3}, longerLine.getJointPositionIndexes());
    assertArrayEquals(new int[]{1, 2}, longerLine.getJointAdjacentPointIndexes());
    assertEquals(2, longerLine.getMidpointIndex());
    assertEquals(1.033333333333333, longerLine.getRealStepSize(), Constants.DOUBLE_DELTA);
    assertEquals(new Point2D.Double(3.0 + 0.0999999999999996, 0.0), longerLine.getVector());
  }

  @Test
  public void testShorterLine() throws Exception {
    RoadSectionView shorterLine = new RoadSectionView(0, joint11, joint4M1, STEP_SIZE);
    assertArrayEquals(new JointView[]{joint11, joint4M1}, shorterLine.getJoints());
    assertEquals(3 - 0, shorterLine.getPositions().length);
    assertEquals(new Point2D.Double(1.0, 1.0), shorterLine.getPositions()[0].getCoordinatesPair());
    assertEquals(new Point2D.Double(2.45, 1.0), shorterLine.getPositions()[1].getCoordinatesPair());
    assertEquals(new Point2D.Double(4.0 - 0.1, 1.0), shorterLine.getPositions()[2].getCoordinatesPair());
    assertArrayEquals(new int[]{0, 2}, shorterLine.getJointPositionIndexes());
    assertArrayEquals(new int[]{1, 1}, shorterLine.getJointAdjacentPointIndexes());
    assertEquals(1, shorterLine.getMidpointIndex());
    assertEquals(1.45, shorterLine.getRealStepSize(), Constants.DOUBLE_DELTA);
    assertEquals(new Point2D.Double(3.0 - 0.1, 0.0), shorterLine.getVector());
  }

  @Test
  public void testObliqueLine() throws Exception {
    RoadSectionView obliqueLine = new RoadSectionView(0, joint11, joint44, STEP_SIZE);
    assertArrayEquals(new JointView[]{joint11, joint44}, obliqueLine.getJoints());
    assertEquals(4 + 1, obliqueLine.getPositions().length);  // length = ceil(sqrt(18))
    assertEquals(Math.sqrt(18.0), obliqueLine.getLength(), Constants.DOUBLE_DELTA);
    assertArrayEquals(new int[]{0, 4}, obliqueLine.getJointPositionIndexes());
    assertArrayEquals(new int[]{1, 3}, obliqueLine.getJointAdjacentPointIndexes());
    assertEquals(2, obliqueLine.getMidpointIndex());
    assertEquals(Math.sqrt(18.0) / 4, obliqueLine.getRealStepSize(), Constants.DOUBLE_DELTA);
    assertEquals(new Point2D.Double(3.0, 3.0), obliqueLine.getVector());
    assertArrayEquals(new Point2D.Double[]{new Point2D.Double(1.5, 1.5), new Point2D.Double(1.5, 1.5), },
        obliqueLine.getHalfVectors());
    assertArrayEquals(new double[]{2.1213203435596424, 2.1213203435596424}, obliqueLine.getHalfLengths(),
        Constants.DOUBLE_DELTA);

    obliqueLine.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.VISIBLE);
    obliqueLine.setHalfVisibility(RoadSectionView.Direction.FORWARD, RoadSectionView.Visibility.VISIBLE);
    assertEquals(obliqueLine.getPositions()[2], obliqueLine.getClosestVisiblePosition(new Point2D.Double(3.0, 2.0)));
    assertEquals(obliqueLine.getPositions()[1], obliqueLine.getClosestVisiblePosition(new Point2D.Double(2.5, 1.5)));
    assertEquals(obliqueLine.getPositions()[3], obliqueLine.getClosestVisiblePosition(new Point2D.Double(3.5, 2.5)));
    assertEquals(obliqueLine.getPositions()[1], obliqueLine.getClosestVisiblePosition(new Point2D.Double(2.0, 1.0)));
    assertEquals(obliqueLine.getPositions()[3], obliqueLine.getClosestVisiblePosition(new Point2D.Double(4.0, 3.0)));
    assertEquals(obliqueLine.getPositions()[0], obliqueLine.getClosestVisiblePosition(new Point2D.Double(1.5, 0.5)));
    assertEquals(obliqueLine.getPositions()[4], obliqueLine.getClosestVisiblePosition(new Point2D.Double(4.5, 3.5)));
    assertEquals(obliqueLine.getPositions()[0], obliqueLine.getClosestVisiblePosition(new Point2D.Double(1.0, 0.0)));
    assertEquals(obliqueLine.getPositions()[4], obliqueLine.getClosestVisiblePosition(new Point2D.Double(5.0, 4.0)));
    assertEquals(obliqueLine.getPositions()[2], obliqueLine.getClosestVisiblePosition(new Point2D.Double(2.0, 3.0)));
    assertEquals(obliqueLine.getPositions()[1], obliqueLine.getClosestVisiblePosition(new Point2D.Double(1.5, 2.5)));
    assertEquals(obliqueLine.getPositions()[3], obliqueLine.getClosestVisiblePosition(new Point2D.Double(2.5, 3.5)));
    assertEquals(obliqueLine.getPositions()[1], obliqueLine.getClosestVisiblePosition(new Point2D.Double(1.0, 2.0)));
    assertEquals(obliqueLine.getPositions()[3], obliqueLine.getClosestVisiblePosition(new Point2D.Double(3.0, 4.0)));
    assertEquals(obliqueLine.getPositions()[0], obliqueLine.getClosestVisiblePosition(new Point2D.Double(0.5, 1.5)));
    assertEquals(obliqueLine.getPositions()[4], obliqueLine.getClosestVisiblePosition(new Point2D.Double(3.5, 4.5)));
    assertEquals(obliqueLine.getPositions()[0], obliqueLine.getClosestVisiblePosition(new Point2D.Double(0.0, 1.0)));
    assertEquals(obliqueLine.getPositions()[4], obliqueLine.getClosestVisiblePosition(new Point2D.Double(4.0, 5.0)));
  }

  @Test
  public void testReverseObliqueLine() throws Exception {
    RoadSectionView  reverseObliqueLine = new RoadSectionView(0, joint44, joint11, STEP_SIZE);
    assertArrayEquals(new JointView[]{joint44, joint11}, reverseObliqueLine.getJoints());
    assertEquals(4 + 1, reverseObliqueLine.getPositions().length);  // length = ceil(sqrt(18))
    assertEquals(new Point2D.Double(4.0, 4.0), reverseObliqueLine.getPositions()[0].getCoordinatesPair());
    assertEquals(new Point2D.Double(3.25, 3.25), reverseObliqueLine.getPositions()[1].getCoordinatesPair());
    assertEquals(new Point2D.Double(2.5, 2.5), reverseObliqueLine.getPositions()[2].getCoordinatesPair());
    assertEquals(new Point2D.Double(1.75, 1.75), reverseObliqueLine.getPositions()[3].getCoordinatesPair());
    assertEquals(new Point2D.Double(1.0, 1.0), reverseObliqueLine.getPositions()[4].getCoordinatesPair());
    assertArrayEquals(new int[]{0, 4}, reverseObliqueLine.getJointPositionIndexes());
    assertArrayEquals(new int[]{1, 3}, reverseObliqueLine.getJointAdjacentPointIndexes());
    assertEquals(2, reverseObliqueLine.getMidpointIndex());
    assertEquals(Math.sqrt(18.0) / 4, reverseObliqueLine.getRealStepSize(), Constants.DOUBLE_DELTA);
    assertEquals(new Point2D.Double(-3.0, -3.0), reverseObliqueLine.getVector());
    assertArrayEquals(new double[]{2.1213203435596424, 2.1213203435596424}, reverseObliqueLine.getHalfLengths(),
        Constants.DOUBLE_DELTA);

    // Whole RoadSection VISIBLE .
    reverseObliqueLine.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.VISIBLE);
    reverseObliqueLine.setHalfVisibility(RoadSectionView.Direction.FORWARD, RoadSectionView.Visibility.VISIBLE);
    assertEquals(reverseObliqueLine.getPositions()[2],
        reverseObliqueLine.getClosestVisiblePosition(new Point2D.Double(3.0, 2.0)));
    assertEquals(reverseObliqueLine.getPositions()[3],
        reverseObliqueLine.getClosestVisiblePosition(new Point2D.Double(2.5, 1.5)));
    assertEquals(reverseObliqueLine.getPositions()[1],
        reverseObliqueLine.getClosestVisiblePosition(new Point2D.Double(3.5, 2.5)));
    assertEquals(reverseObliqueLine.getPositions()[3],
        reverseObliqueLine.getClosestVisiblePosition(new Point2D.Double(2.0, 1.0)));
    assertEquals(reverseObliqueLine.getPositions()[1],
        reverseObliqueLine.getClosestVisiblePosition(new Point2D.Double(4.0, 3.0)));
    assertEquals(reverseObliqueLine.getPositions()[4],
        reverseObliqueLine.getClosestVisiblePosition(new Point2D.Double(1.5, 0.5)));
    assertEquals(reverseObliqueLine.getPositions()[0],
        reverseObliqueLine.getClosestVisiblePosition(new Point2D.Double(4.5, 3.5)));
    assertEquals(reverseObliqueLine.getPositions()[4],
        reverseObliqueLine.getClosestVisiblePosition(new Point2D.Double(1.0, 0.0)));
    assertEquals(reverseObliqueLine.getPositions()[0],
        reverseObliqueLine.getClosestVisiblePosition(new Point2D.Double(5.0, 4.0)));
    assertEquals(reverseObliqueLine.getPositions()[2],
        reverseObliqueLine.getClosestVisiblePosition(new Point2D.Double(2.0, 3.0)));
    assertEquals(reverseObliqueLine.getPositions()[3],
        reverseObliqueLine.getClosestVisiblePosition(new Point2D.Double(1.5, 2.5)));
    assertEquals(reverseObliqueLine.getPositions()[1],
        reverseObliqueLine.getClosestVisiblePosition(new Point2D.Double(2.5, 3.5)));
    assertEquals(reverseObliqueLine.getPositions()[3],
        reverseObliqueLine.getClosestVisiblePosition(new Point2D.Double(1.0, 2.0)));
    assertEquals(reverseObliqueLine.getPositions()[1],
        reverseObliqueLine.getClosestVisiblePosition(new Point2D.Double(3.0, 4.0)));
    assertEquals(reverseObliqueLine.getPositions()[4],
        reverseObliqueLine.getClosestVisiblePosition(new Point2D.Double(0.5, 1.5)));
    assertEquals(reverseObliqueLine.getPositions()[0],
        reverseObliqueLine.getClosestVisiblePosition(new Point2D.Double(3.5, 4.5)));
    assertEquals(reverseObliqueLine.getPositions()[4],
        reverseObliqueLine.getClosestVisiblePosition(new Point2D.Double(0.0, 1.0)));
    assertEquals(reverseObliqueLine.getPositions()[0],
        reverseObliqueLine.getClosestVisiblePosition(new Point2D.Double(4.0, 5.0)));

    // BACKWARD RoadSection.Half VISIBLE .
    reverseObliqueLine.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.DISCOVERED);
    reverseObliqueLine.setHalfVisibility(RoadSectionView.Direction.FORWARD, RoadSectionView.Visibility.INVISIBLE);
    assertEquals(reverseObliqueLine.getPositions()[2],
        reverseObliqueLine.getClosestVisiblePosition(new Point2D.Double(1.5, 3.5)));
    assertEquals(reverseObliqueLine.getPositions()[2],
        reverseObliqueLine.getClosestVisiblePosition(new Point2D.Double(0.5, 2.5)));
    assertEquals(reverseObliqueLine.getPositions()[0],
        reverseObliqueLine.getClosestVisiblePosition(new Point2D.Double(3.0, 5.0)));
    assertEquals(reverseObliqueLine.getPositions()[0],
        reverseObliqueLine.getClosestVisiblePosition(new Point2D.Double(4.0, 6.0)));

    // FORWARD RoadSection.Half VISIBLE .
    reverseObliqueLine.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.INVISIBLE);
    reverseObliqueLine.setHalfVisibility(RoadSectionView.Direction.FORWARD, RoadSectionView.Visibility.VISIBLE);
    assertEquals(reverseObliqueLine.getPositions()[4],
        reverseObliqueLine.getClosestVisiblePosition(new Point2D.Double(2.0, 0.0)));
    assertEquals(reverseObliqueLine.getPositions()[4],
        reverseObliqueLine.getClosestVisiblePosition(new Point2D.Double(-1.0, 1.0)));
    assertEquals(reverseObliqueLine.getPositions()[2],
        reverseObliqueLine.getClosestVisiblePosition(new Point2D.Double(1.5, 3.5)));
    assertEquals(reverseObliqueLine.getPositions()[2],
        reverseObliqueLine.getClosestVisiblePosition(new Point2D.Double(2.5, 4.5)));

    // Whole RoadSection INVISIBLE .
    reverseObliqueLine.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.INVISIBLE);
    reverseObliqueLine.setHalfVisibility(RoadSectionView.Direction.FORWARD, RoadSectionView.Visibility.INVISIBLE);
    assertNull(reverseObliqueLine.getClosestVisiblePosition(new Point2D.Double(3.0, 5.0)));
    assertNull(reverseObliqueLine.getClosestVisiblePosition(new Point2D.Double(4.0, 6.0)));
    assertNull(reverseObliqueLine.getClosestVisiblePosition(new Point2D.Double(2.0, 0.0)));
    assertNull(reverseObliqueLine.getClosestVisiblePosition(new Point2D.Double(-1.0, -1.0)));
  }

  @Test
  public void testIllegalLineZeroLength() {
    try {
      new RoadSectionView(0, joint11, joint11, STEP_SIZE);
      fail("Should have thrown an exception.");
    } catch (ClientViewException exception) {
      assertEquals("A RoadSection must have a non-zero length.", exception.getMessage());
    }
  }

  @Test
  public void testIllegalLineTooShort() {
    try {
      new RoadSectionView(0, joint11, joint21, STEP_SIZE);
      fail("Should have thrown an exception.");
    } catch (ClientViewException exception) {
      assertTrue(exception.getMessage().contains("A RoadSection must have at least 3 positions."));
    }
  }

  @Test
  public void testComparisonWorks() throws Exception {
    RoadSectionView roadSection0 = new RoadSectionView(0, joint11, joint41, STEP_SIZE);
    RoadSectionView roadSection01 = new RoadSectionView(0, joint11, joint41, STEP_SIZE);
    RoadSectionView roadSection1 = new RoadSectionView(1, joint11, joint44, STEP_SIZE);
    assertTrue(roadSection0.equals(roadSection0));
    assertFalse(roadSection0.equals(roadSection01));
    assertFalse(roadSection0.equals(roadSection1));
    assertFalse(roadSection0.equals(joint11));
  }

  @Test
  public void testAdjacentToJointWorks() throws Exception {
    RoadSectionView roadSection = new RoadSectionView(0, joint11, joint41, STEP_SIZE);
    assertTrue(roadSection.adjacentTo(joint11));
    assertTrue(roadSection.adjacentTo(joint41));
    assertFalse(roadSection.adjacentTo(joint14));
  }

  @Test
  public void testGetJointIndexWorks() throws Exception {
    RoadSectionView roadSection = new RoadSectionView(0, joint11, joint41, STEP_SIZE);
    assertEquals(0, roadSection.getJointIndex(joint11));
    assertEquals(3, roadSection.getJointIndex(joint41));
    try {
      roadSection.getJointIndex(joint14);
      fail("Should have thrown an exception.");
    } catch (ClientViewException exception) {
      assertEquals("Joint Point2D.Double[1.0, 4.0] is not on RoadSectionView "
          + "[index=0, joints=[Point2D.Double[1.0, 1.0], Point2D.Double[4.0, 1.0]]] .", exception.getMessage());
    }
  }

  @Test
  public void testPositionProjectionWorks() throws Exception {
    RoadSectionView roadSection0 = new RoadSectionView(0, joint11, joint41, STEP_SIZE);
    RoadSectionView roadSection1 = new RoadSectionView(1, joint41, joint44, STEP_SIZE);
    RoadSectionView roadSection2 = new RoadSectionView(2, joint11, joint44, STEP_SIZE);
    // Same RoadSection.
    {
      PositionView position = roadSection0.getProjectedPosition(roadSection0.getPositions()[0]);
      assertTrue(position.equals(roadSection0.getPositions()[0]));
      assertTrue(position.getRoadSection().equals(roadSection0));
    }
    {
      PositionView position = roadSection0.getProjectedPosition(roadSection0.getPositions()[3]);
      assertTrue(position.equals(roadSection0.getPositions()[3]));
      assertTrue(position.getRoadSection().equals(roadSection0));
    }
    {
      PositionView position = roadSection0.getProjectedPosition(roadSection0.getPositions()[1]);
      assertTrue(position.equals(roadSection0.getPositions()[1]));
      assertTrue(position.getRoadSection().equals(roadSection0));
    }

    // Different RoadSections.
    {
      PositionView position = roadSection0.getProjectedPosition(roadSection2.getPositions()[0]);
      assertTrue(position.equals(roadSection2.getPositions()[0]));
      assertTrue(position.getRoadSection().equals(roadSection0));
    }
    {
      PositionView position = roadSection2.getProjectedPosition(roadSection0.getPositions()[0]);
      assertTrue(position.equals(roadSection0.getPositions()[0]));
      assertTrue(position.getRoadSection().equals(roadSection2));
    }
    {
      PositionView position = roadSection0.getProjectedPosition(roadSection1.getPositions()[0]);
      assertTrue(position.equals(roadSection1.getPositions()[0]));
      assertTrue(position.getRoadSection().equals(roadSection0));
    }
    {
      PositionView position = roadSection1.getProjectedPosition(roadSection0.getPositions()[3]);
      assertTrue(position.equals(roadSection0.getPositions()[3]));
      assertTrue(position.getRoadSection().equals(roadSection1));
    }
    {
      PositionView position = roadSection1.getProjectedPosition(roadSection2.getPositions()[4]);
      assertTrue(position.equals(roadSection2.getPositions()[4]));
      assertTrue(position.getRoadSection().equals(roadSection1));
    }
    {
      PositionView position = roadSection2.getProjectedPosition(roadSection1.getPositions()[3]);
      assertTrue(position.equals(roadSection1.getPositions()[3]));
      assertTrue(position.getRoadSection().equals(roadSection2));
    }
    assertEquals(null, roadSection0.getProjectedPosition(roadSection1.getPositions()[1]));
    assertEquals(null, roadSection1.getProjectedPosition(roadSection0.getPositions()[2]));
    assertEquals(null, roadSection0.getProjectedPosition(roadSection1.getPositions()[3]));
    assertEquals(null, roadSection1.getProjectedPosition(roadSection0.getPositions()[0]));
  }

  @Test
  public void testRoadSectionHalfVisibilityManagementWorks() throws Exception {
    RoadSectionView roadSection = new RoadSectionView(0, joint11, joint41, STEP_SIZE);
    assertArrayEquals(
        new RoadSectionView.Visibility[]{RoadSectionView.Visibility.INVISIBLE, RoadSectionView.Visibility.INVISIBLE},
        roadSection.getHalfVisibilities());
    roadSection.setHalfVisibility(RoadSectionView.Direction.FORWARD, RoadSectionView.Visibility.VISIBLE);
    assertArrayEquals(
        new RoadSectionView.Visibility[]{RoadSectionView.Visibility.INVISIBLE, RoadSectionView.Visibility.VISIBLE},
        roadSection.getHalfVisibilities());
    roadSection.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.DISCOVERED);
    assertArrayEquals(
        new RoadSectionView.Visibility[]{RoadSectionView.Visibility.DISCOVERED, RoadSectionView.Visibility.VISIBLE},
        roadSection.getHalfVisibilities());
  }

}
