package cz.wie.najtmar.gop.client.view;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import cz.wie.najtmar.gop.client.view.DistinctivePositionView.DistinctionProperty;
import cz.wie.najtmar.gop.client.view.PrawnView.Type;
import cz.wie.najtmar.gop.common.Point2D;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.UUID;

public class DistinctivePositionViewTest {

  static final double STEP_SIZE = 1.0;
  static PlayerView PLAYER_A;
  static PlayerView PLAYER_B;

  JointView joint00;
  JointView joint30;
  JointView joint33;
  RoadSectionView roadSection0;
  RoadSectionView roadSection1;
  RoadSectionView roadSection2;
  PrawnView settlersUnitA;
  PrawnView warriorA;
  PrawnView settlersUnitB;
  PrawnView warriorB;
  CityView city;

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    PLAYER_A = new PlayerView(new UserView("Player A", new UUID(12345, 67890)), 0);
    PLAYER_B = new PlayerView(new UserView("Player B", new UUID(98765, 43210)), 1);
  }

  /**
   * Method setUp().
   * @throws Exception when something unexpected happens
   */
  @Before
  public void setUp() throws Exception {
    joint00 = new JointView(0, "joinr00", new Point2D.Double(0.0, 0.0));
    joint30 = new JointView(1, "joint30", new Point2D.Double(3.0, 0.0));
    joint33 = new JointView(2, "joint33", new Point2D.Double(3.0, 3.0));
    roadSection0 = new RoadSectionView(0, joint00, joint30, STEP_SIZE);
    roadSection1 = new RoadSectionView(1, joint30, joint33, STEP_SIZE);
    roadSection2 = new RoadSectionView(2, joint00, joint33, STEP_SIZE);
    settlersUnitA = new PrawnView(Type.SETTLERS_UNIT, 0.0, PLAYER_A, 0, joint00.getNormalizedPosition());
    settlersUnitA.setCurrentPosition(roadSection0.getPositions()[1]);
    warriorA = new PrawnView(Type.WARRIOR, Math.PI / 2.0, PLAYER_A, 1, joint00.getNormalizedPosition());
    warriorA.setCurrentPosition(roadSection0.getPositions()[3]);
    settlersUnitB = new PrawnView(Type.SETTLERS_UNIT, 0.0, PLAYER_B, 2, joint00.getNormalizedPosition());
    settlersUnitB.setCurrentPosition(roadSection1.getPositions()[3]);
    warriorB = new PrawnView(Type.WARRIOR, 3.0 * Math.PI / 2.0, PLAYER_B, 3, joint00.getNormalizedPosition());
    warriorB.setCurrentPosition(roadSection1.getPositions()[0]);
    city = new CityView("City", 7, joint00, PLAYER_A);
  }

  @Test
  public void testOwnCityRecognized() throws Exception {
    assertFalse(DistinctionProperty.OWN_CITY.appliesToAndVisible(roadSection0.getPositions()[0], PLAYER_A));
    city.setVisibility(CityView.Visibility.DISCOVERED);
    assertTrue(DistinctionProperty.OWN_CITY.appliesToAndVisible(roadSection0.getPositions()[0], PLAYER_A));
    assertTrue(DistinctionProperty.OWN_CITY.appliesToAndVisible(roadSection2.getPositions()[0], PLAYER_A));
    assertFalse(DistinctionProperty.OWN_CITY.appliesToAndVisible(roadSection0.getPositions()[0], PLAYER_B));
    assertFalse(DistinctionProperty.OWN_CITY.appliesToAndVisible(roadSection2.getPositions()[0], PLAYER_B));
    assertFalse(DistinctionProperty.OWN_CITY.appliesToAndVisible(roadSection0.getPositions()[3], PLAYER_A));
    assertFalse(DistinctionProperty.OWN_CITY.appliesToAndVisible(roadSection1.getPositions()[0], PLAYER_A));
    assertFalse(DistinctionProperty.OWN_CITY.appliesToAndVisible(roadSection1.getPositions()[3], PLAYER_A));
    assertFalse(DistinctionProperty.OWN_CITY.appliesToAndVisible(roadSection2.getPositions()[4], PLAYER_A));
    assertFalse(DistinctionProperty.OWN_CITY.appliesToAndVisible(roadSection0.getPositions()[1], PLAYER_A));
    assertFalse(DistinctionProperty.OWN_CITY.appliesToAndVisible(roadSection1.getPositions()[2], PLAYER_A));
  }

  @Test
  public void testOpponentsCityRecognized() throws Exception {
    assertFalse(DistinctionProperty.OPPONENTS_CITY.appliesToAndVisible(roadSection0.getPositions()[0], PLAYER_B));
    city.setVisibility(CityView.Visibility.DISCOVERED);
    assertTrue(DistinctionProperty.OPPONENTS_CITY.appliesToAndVisible(roadSection0.getPositions()[0], PLAYER_B));
    assertTrue(DistinctionProperty.OPPONENTS_CITY.appliesToAndVisible(roadSection2.getPositions()[0], PLAYER_B));
    assertFalse(DistinctionProperty.OPPONENTS_CITY.appliesToAndVisible(roadSection0.getPositions()[0], PLAYER_A));
    assertFalse(DistinctionProperty.OPPONENTS_CITY.appliesToAndVisible(roadSection2.getPositions()[0], PLAYER_A));
    assertFalse(DistinctionProperty.OPPONENTS_CITY.appliesToAndVisible(roadSection0.getPositions()[3], PLAYER_B));
    assertFalse(DistinctionProperty.OPPONENTS_CITY.appliesToAndVisible(roadSection1.getPositions()[0], PLAYER_B));
    assertFalse(DistinctionProperty.OPPONENTS_CITY.appliesToAndVisible(roadSection1.getPositions()[3], PLAYER_B));
    assertFalse(DistinctionProperty.OPPONENTS_CITY.appliesToAndVisible(roadSection2.getPositions()[4], PLAYER_B));
    assertFalse(DistinctionProperty.OPPONENTS_CITY.appliesToAndVisible(roadSection0.getPositions()[1], PLAYER_B));
    assertFalse(DistinctionProperty.OPPONENTS_CITY.appliesToAndVisible(roadSection1.getPositions()[2], PLAYER_B));
  }

  @Test
  public void testJointsRecognized() throws Exception {
    assertFalse(DistinctionProperty.JOINT.appliesToAndVisible(roadSection0.getPositions()[0], null));
    roadSection0.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.VISIBLE);
    assertTrue(DistinctionProperty.JOINT.appliesToAndVisible(roadSection0.getPositions()[0], null));
    assertTrue(DistinctionProperty.JOINT.appliesToAndVisible(roadSection2.getPositions()[0], null));
    assertFalse(DistinctionProperty.JOINT.appliesToAndVisible(roadSection0.getPositions()[3], null));
    roadSection0.setHalfVisibility(RoadSectionView.Direction.FORWARD, RoadSectionView.Visibility.VISIBLE);
    assertTrue(DistinctionProperty.JOINT.appliesToAndVisible(roadSection0.getPositions()[3], null));
    assertTrue(DistinctionProperty.JOINT.appliesToAndVisible(roadSection1.getPositions()[0], null));
    assertFalse(DistinctionProperty.JOINT.appliesToAndVisible(roadSection1.getPositions()[3], null));
    roadSection1.setHalfVisibility(RoadSectionView.Direction.FORWARD, RoadSectionView.Visibility.VISIBLE);
    assertTrue(DistinctionProperty.JOINT.appliesToAndVisible(roadSection1.getPositions()[3], null));
    assertTrue(DistinctionProperty.JOINT.appliesToAndVisible(roadSection2.getPositions()[4], null));
    assertFalse(DistinctionProperty.JOINT.appliesToAndVisible(roadSection0.getPositions()[1], null));
    assertFalse(DistinctionProperty.JOINT.appliesToAndVisible(roadSection1.getPositions()[2], null));
  }

  @Test
  public void testMidpointsRecognized() throws Exception {
    assertFalse(DistinctionProperty.MIDPOINT.appliesToAndVisible(roadSection0.getPositions()[2], null));
    roadSection0.setHalfVisibility(RoadSectionView.Direction.FORWARD, RoadSectionView.Visibility.DISCOVERED);
    assertTrue(DistinctionProperty.MIDPOINT.appliesToAndVisible(roadSection0.getPositions()[2], null));
    assertFalse(DistinctionProperty.MIDPOINT.appliesToAndVisible(roadSection1.getPositions()[2], null));
    roadSection1.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.VISIBLE);
    assertTrue(DistinctionProperty.MIDPOINT.appliesToAndVisible(roadSection1.getPositions()[2], null));
    assertFalse(DistinctionProperty.MIDPOINT.appliesToAndVisible(roadSection2.getPositions()[2], null));
    roadSection2.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.DISCOVERED);
    assertTrue(DistinctionProperty.MIDPOINT.appliesToAndVisible(roadSection2.getPositions()[2], null));
    assertFalse(DistinctionProperty.MIDPOINT.appliesToAndVisible(roadSection0.getPositions()[0], null));
    assertFalse(DistinctionProperty.MIDPOINT.appliesToAndVisible(roadSection1.getPositions()[1], null));
    assertFalse(DistinctionProperty.MIDPOINT.appliesToAndVisible(roadSection2.getPositions()[3], null));
    assertFalse(DistinctionProperty.MIDPOINT.appliesToAndVisible(roadSection0.getPositions()[3], null));
  }

  @Test
  public void testOwnCityAdjacentPositionsRecognized() throws Exception {
    assertFalse(DistinctionProperty.OWN_CITY_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[1], PLAYER_A));
    roadSection0.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.DISCOVERED);
    assertTrue(DistinctionProperty.OWN_CITY_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[1], PLAYER_A));
    assertFalse(DistinctionProperty.OWN_CITY_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[1], PLAYER_A));
    roadSection2.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.VISIBLE);
    assertTrue(DistinctionProperty.OWN_CITY_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[1], PLAYER_A));
    assertFalse(DistinctionProperty.OWN_CITY_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[0], PLAYER_A));
    assertFalse(DistinctionProperty.OWN_CITY_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[0], PLAYER_A));
    assertFalse(DistinctionProperty.OWN_CITY_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[1], PLAYER_B));
    assertFalse(DistinctionProperty.OWN_CITY_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[1], PLAYER_B));
    assertFalse(DistinctionProperty.OWN_CITY_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[3], PLAYER_A));
    assertFalse(DistinctionProperty.OWN_CITY_ADJACENT.appliesToAndVisible(roadSection1.getPositions()[0], PLAYER_A));
    assertFalse(DistinctionProperty.OWN_CITY_ADJACENT.appliesToAndVisible(roadSection1.getPositions()[3], PLAYER_A));
    assertFalse(DistinctionProperty.OWN_CITY_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[4], PLAYER_A));
  }

  @Test
  public void testOpponentsCityAdjacentPositionsRecognized() throws Exception {
    assertFalse(
        DistinctionProperty.OPPONENTS_CITY_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[1], PLAYER_B));
    roadSection0.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.DISCOVERED);
    assertTrue(
        DistinctionProperty.OPPONENTS_CITY_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[1], PLAYER_B));
    assertFalse(
        DistinctionProperty.OPPONENTS_CITY_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[1], PLAYER_B));
    roadSection2.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.VISIBLE);
    assertTrue(
        DistinctionProperty.OPPONENTS_CITY_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[1], PLAYER_B));
    assertFalse(
        DistinctionProperty.OPPONENTS_CITY_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[0], PLAYER_B));
    assertFalse(
        DistinctionProperty.OPPONENTS_CITY_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[0], PLAYER_B));
    assertFalse(
        DistinctionProperty.OPPONENTS_CITY_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[1], PLAYER_A));
    assertFalse(
        DistinctionProperty.OPPONENTS_CITY_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[1], PLAYER_A));
    assertFalse(
        DistinctionProperty.OPPONENTS_CITY_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[3], PLAYER_B));
    assertFalse(
        DistinctionProperty.OPPONENTS_CITY_ADJACENT.appliesToAndVisible(roadSection1.getPositions()[0], PLAYER_B));
    assertFalse(
        DistinctionProperty.OPPONENTS_CITY_ADJACENT.appliesToAndVisible(roadSection1.getPositions()[3], PLAYER_B));
    assertFalse(
        DistinctionProperty.OPPONENTS_CITY_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[4], PLAYER_B));
  }

  @Test
  public void testJointAdjacentPositionsRecognized() throws Exception {
    assertFalse(DistinctionProperty.JOINT_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[1], null));
    roadSection0.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.DISCOVERED);
    assertTrue(DistinctionProperty.JOINT_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[1], null));
    assertFalse(DistinctionProperty.JOINT_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[1], null));
    roadSection2.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.DISCOVERED);
    assertTrue(DistinctionProperty.JOINT_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[1], null));
    assertTrue(DistinctionProperty.JOINT_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[2], null));
    assertFalse(DistinctionProperty.JOINT_ADJACENT.appliesToAndVisible(roadSection1.getPositions()[1], null));
    roadSection1.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.DISCOVERED);
    assertTrue(DistinctionProperty.JOINT_ADJACENT.appliesToAndVisible(roadSection1.getPositions()[1], null));
    assertTrue(DistinctionProperty.JOINT_ADJACENT.appliesToAndVisible(roadSection1.getPositions()[2], null));
    assertFalse(DistinctionProperty.JOINT_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[3], null));
    roadSection2.setHalfVisibility(RoadSectionView.Direction.FORWARD, RoadSectionView.Visibility.DISCOVERED);
    assertTrue(DistinctionProperty.JOINT_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[3], null));
    assertFalse(DistinctionProperty.JOINT_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[0], null));
    assertFalse(DistinctionProperty.JOINT_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[0], null));
    assertFalse(DistinctionProperty.JOINT_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[4], null));
    assertFalse(DistinctionProperty.JOINT_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[2], null));
  }

  @Test
  public void testMidpointAdjacentPositionsRecognized() throws Exception {
    assertFalse(DistinctionProperty.MIDPOINT_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[1], null));
    roadSection0.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.VISIBLE);
    assertTrue(DistinctionProperty.MIDPOINT_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[1], null));
    assertFalse(DistinctionProperty.MIDPOINT_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[3], null));
    roadSection0.setHalfVisibility(RoadSectionView.Direction.FORWARD, RoadSectionView.Visibility.DISCOVERED);
    assertTrue(DistinctionProperty.MIDPOINT_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[3], null));
    assertFalse(DistinctionProperty.MIDPOINT_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[1], null));
    roadSection2.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.DISCOVERED);
    assertTrue(DistinctionProperty.MIDPOINT_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[1], null));
    assertFalse(DistinctionProperty.MIDPOINT_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[3], null));
    roadSection2.setHalfVisibility(RoadSectionView.Direction.FORWARD, RoadSectionView.Visibility.DISCOVERED);
    assertTrue(DistinctionProperty.MIDPOINT_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[3], null));
    assertFalse(DistinctionProperty.MIDPOINT_ADJACENT.appliesToAndVisible(roadSection1.getPositions()[1], null));
    roadSection1.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.DISCOVERED);
    assertTrue(DistinctionProperty.MIDPOINT_ADJACENT.appliesToAndVisible(roadSection1.getPositions()[1], null));
    assertTrue(DistinctionProperty.MIDPOINT_ADJACENT.appliesToAndVisible(roadSection1.getPositions()[3], null));
    assertFalse(DistinctionProperty.MIDPOINT_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[2], null));
    assertFalse(DistinctionProperty.MIDPOINT_ADJACENT.appliesToAndVisible(roadSection1.getPositions()[2], null));
    assertFalse(DistinctionProperty.MIDPOINT_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[2], null));
    assertFalse(DistinctionProperty.MIDPOINT_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[0], null));
    assertFalse(DistinctionProperty.MIDPOINT_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[0], null));
    assertFalse(DistinctionProperty.MIDPOINT_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[4], null));
  }

  @Test
  public void testOwnPrawnsRecognized() throws Exception {
    assertFalse(DistinctionProperty.OWN_PRAWN.appliesToAndVisible(roadSection0.getPositions()[1], PLAYER_A));
    settlersUnitA.setVisibility(PrawnView.Visibility.VISIBLE);
    assertTrue(DistinctionProperty.OWN_PRAWN.appliesToAndVisible(roadSection0.getPositions()[1], PLAYER_A));
    assertFalse(DistinctionProperty.OWN_PRAWN.appliesToAndVisible(roadSection0.getPositions()[3], PLAYER_A));
    warriorA.setVisibility(PrawnView.Visibility.VISIBLE);
    assertTrue(DistinctionProperty.OWN_PRAWN.appliesToAndVisible(roadSection0.getPositions()[3], PLAYER_A));
    assertTrue(DistinctionProperty.OWN_PRAWN.appliesToAndVisible(roadSection1.getPositions()[0], PLAYER_A));
    assertFalse(DistinctionProperty.OWN_PRAWN.appliesToAndVisible(roadSection0.getPositions()[1], PLAYER_B));
    assertFalse(DistinctionProperty.OWN_PRAWN.appliesToAndVisible(roadSection1.getPositions()[3], PLAYER_B));
    settlersUnitB.setVisibility(PrawnView.Visibility.VISIBLE);
    assertTrue(DistinctionProperty.OWN_PRAWN.appliesToAndVisible(roadSection1.getPositions()[3], PLAYER_B));
    assertTrue(DistinctionProperty.OWN_PRAWN.appliesToAndVisible(roadSection2.getPositions()[4], PLAYER_B));
    assertFalse(DistinctionProperty.OWN_PRAWN.appliesToAndVisible(roadSection0.getPositions()[3], PLAYER_B));
    warriorB.setVisibility(PrawnView.Visibility.VISIBLE);
    assertTrue(DistinctionProperty.OWN_PRAWN.appliesToAndVisible(roadSection0.getPositions()[3], PLAYER_B));
    assertTrue(DistinctionProperty.OWN_PRAWN.appliesToAndVisible(roadSection1.getPositions()[0], PLAYER_B));
    assertFalse(DistinctionProperty.OWN_PRAWN.appliesToAndVisible(roadSection2.getPositions()[4], PLAYER_A));
    assertFalse(DistinctionProperty.OWN_PRAWN.appliesToAndVisible(roadSection0.getPositions()[0], PLAYER_A));
    assertFalse(DistinctionProperty.OWN_PRAWN.appliesToAndVisible(roadSection2.getPositions()[0], PLAYER_A));
    assertFalse(DistinctionProperty.OWN_PRAWN.appliesToAndVisible(roadSection0.getPositions()[0], PLAYER_B));
    assertFalse(DistinctionProperty.OWN_PRAWN.appliesToAndVisible(roadSection2.getPositions()[0], PLAYER_B));
    assertFalse(DistinctionProperty.OWN_PRAWN.appliesToAndVisible(roadSection0.getPositions()[2], PLAYER_A));
    assertFalse(DistinctionProperty.OWN_PRAWN.appliesToAndVisible(roadSection0.getPositions()[2], PLAYER_B));
  }

  @Test
  public void testOpponentsPrawnsRecognized() throws Exception {
    assertFalse(DistinctionProperty.OPPONENTS_PRAWN.appliesToAndVisible(roadSection0.getPositions()[1], PLAYER_B));
    settlersUnitA.setVisibility(PrawnView.Visibility.VISIBLE);
    assertTrue(DistinctionProperty.OPPONENTS_PRAWN.appliesToAndVisible(roadSection0.getPositions()[1], PLAYER_B));
    assertFalse(DistinctionProperty.OPPONENTS_PRAWN.appliesToAndVisible(roadSection0.getPositions()[3], PLAYER_B));
    warriorA.setVisibility(PrawnView.Visibility.VISIBLE);
    assertTrue(DistinctionProperty.OPPONENTS_PRAWN.appliesToAndVisible(roadSection0.getPositions()[3], PLAYER_B));
    assertTrue(DistinctionProperty.OPPONENTS_PRAWN.appliesToAndVisible(roadSection1.getPositions()[0], PLAYER_B));
    assertFalse(DistinctionProperty.OPPONENTS_PRAWN.appliesToAndVisible(roadSection0.getPositions()[1], PLAYER_A));
    assertFalse(DistinctionProperty.OPPONENTS_PRAWN.appliesToAndVisible(roadSection1.getPositions()[3], PLAYER_A));
    settlersUnitB.setVisibility(PrawnView.Visibility.VISIBLE);
    assertTrue(DistinctionProperty.OPPONENTS_PRAWN.appliesToAndVisible(roadSection1.getPositions()[3], PLAYER_A));
    assertTrue(DistinctionProperty.OPPONENTS_PRAWN.appliesToAndVisible(roadSection2.getPositions()[4], PLAYER_A));
    assertFalse(DistinctionProperty.OPPONENTS_PRAWN.appliesToAndVisible(roadSection0.getPositions()[3], PLAYER_A));
    warriorB.setVisibility(PrawnView.Visibility.VISIBLE);
    assertTrue(DistinctionProperty.OPPONENTS_PRAWN.appliesToAndVisible(roadSection0.getPositions()[3], PLAYER_A));
    assertTrue(DistinctionProperty.OPPONENTS_PRAWN.appliesToAndVisible(roadSection1.getPositions()[0], PLAYER_A));
    assertFalse(DistinctionProperty.OPPONENTS_PRAWN.appliesToAndVisible(roadSection2.getPositions()[4], PLAYER_B));
    assertFalse(DistinctionProperty.OPPONENTS_PRAWN.appliesToAndVisible(roadSection0.getPositions()[0], PLAYER_B));
    assertFalse(DistinctionProperty.OPPONENTS_PRAWN.appliesToAndVisible(roadSection2.getPositions()[0], PLAYER_B));
    assertFalse(DistinctionProperty.OPPONENTS_PRAWN.appliesToAndVisible(roadSection0.getPositions()[0], PLAYER_A));
    assertFalse(DistinctionProperty.OPPONENTS_PRAWN.appliesToAndVisible(roadSection2.getPositions()[0], PLAYER_A));
    assertFalse(DistinctionProperty.OPPONENTS_PRAWN.appliesToAndVisible(roadSection0.getPositions()[2], PLAYER_B));
    assertFalse(DistinctionProperty.OPPONENTS_PRAWN.appliesToAndVisible(roadSection0.getPositions()[2], PLAYER_A));
  }

  @Test
  public void testOwnPrawnAdjacentPositionsRecognized() throws Exception {
    assertFalse(DistinctionProperty.OWN_PRAWN_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[0], PLAYER_A));
    roadSection0.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.VISIBLE);
    assertTrue(DistinctionProperty.OWN_PRAWN_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[0], PLAYER_A));
    assertTrue(DistinctionProperty.OWN_PRAWN_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[0], PLAYER_A));
    assertTrue(DistinctionProperty.OWN_PRAWN_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[2], PLAYER_A));
    assertFalse(DistinctionProperty.OWN_PRAWN_ADJACENT.appliesToAndVisible(roadSection1.getPositions()[1], PLAYER_A));
    roadSection1.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.VISIBLE);
    assertTrue(DistinctionProperty.OWN_PRAWN_ADJACENT.appliesToAndVisible(roadSection1.getPositions()[1], PLAYER_A));
    assertFalse(DistinctionProperty.OWN_PRAWN_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[1], PLAYER_A));
    assertFalse(DistinctionProperty.OWN_PRAWN_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[3], PLAYER_A));
    assertFalse(DistinctionProperty.OWN_PRAWN_ADJACENT.appliesToAndVisible(roadSection1.getPositions()[0], PLAYER_A));
    assertFalse(DistinctionProperty.OWN_PRAWN_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[0], PLAYER_B));
    assertTrue(DistinctionProperty.OWN_PRAWN_ADJACENT.appliesToAndVisible(roadSection1.getPositions()[2], PLAYER_B));
    assertFalse(DistinctionProperty.OWN_PRAWN_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[3], PLAYER_B));
    roadSection2.setHalfVisibility(RoadSectionView.Direction.FORWARD, RoadSectionView.Visibility.VISIBLE);
    assertTrue(DistinctionProperty.OWN_PRAWN_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[3], PLAYER_B));
    assertTrue(DistinctionProperty.OWN_PRAWN_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[2], PLAYER_B));
    assertTrue(DistinctionProperty.OWN_PRAWN_ADJACENT.appliesToAndVisible(roadSection1.getPositions()[1], PLAYER_B));
    assertFalse(DistinctionProperty.OWN_PRAWN_ADJACENT.appliesToAndVisible(roadSection1.getPositions()[3], PLAYER_B));
    assertFalse(DistinctionProperty.OWN_PRAWN_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[4], PLAYER_B));
    assertFalse(DistinctionProperty.OWN_PRAWN_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[2], PLAYER_B));
    assertFalse(DistinctionProperty.OWN_PRAWN_ADJACENT.appliesToAndVisible(roadSection1.getPositions()[2], PLAYER_A));
    assertFalse(DistinctionProperty.OWN_PRAWN_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[3], PLAYER_A));
  }

  @Test
  public void testOpponentsPrawnAdjacentPositionsRecognized() throws Exception {
    assertFalse(
        DistinctionProperty.OPPONENTS_PRAWN_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[0], PLAYER_B));
    roadSection0.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.VISIBLE);
    assertTrue(
        DistinctionProperty.OPPONENTS_PRAWN_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[0], PLAYER_B));
    assertTrue(
        DistinctionProperty.OPPONENTS_PRAWN_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[0], PLAYER_B));
    assertTrue(
        DistinctionProperty.OPPONENTS_PRAWN_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[2], PLAYER_B));
    assertFalse(
        DistinctionProperty.OPPONENTS_PRAWN_ADJACENT.appliesToAndVisible(roadSection1.getPositions()[1], PLAYER_B));
    roadSection1.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.VISIBLE);
    assertTrue(
        DistinctionProperty.OPPONENTS_PRAWN_ADJACENT.appliesToAndVisible(roadSection1.getPositions()[1], PLAYER_B));
    assertFalse(
        DistinctionProperty.OPPONENTS_PRAWN_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[1], PLAYER_B));
    assertFalse(
        DistinctionProperty.OPPONENTS_PRAWN_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[3], PLAYER_B));
    assertFalse(
        DistinctionProperty.OPPONENTS_PRAWN_ADJACENT.appliesToAndVisible(roadSection1.getPositions()[0], PLAYER_B));
    assertFalse(
        DistinctionProperty.OPPONENTS_PRAWN_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[0], PLAYER_A));
    assertTrue(
        DistinctionProperty.OPPONENTS_PRAWN_ADJACENT.appliesToAndVisible(roadSection1.getPositions()[2], PLAYER_A));
    assertFalse(
        DistinctionProperty.OPPONENTS_PRAWN_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[3], PLAYER_A));
    roadSection2.setHalfVisibility(RoadSectionView.Direction.FORWARD, RoadSectionView.Visibility.VISIBLE);
    assertTrue(
        DistinctionProperty.OPPONENTS_PRAWN_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[3], PLAYER_A));
    assertTrue(
        DistinctionProperty.OPPONENTS_PRAWN_ADJACENT.appliesToAndVisible(roadSection0.getPositions()[2], PLAYER_A));
    assertTrue(
        DistinctionProperty.OPPONENTS_PRAWN_ADJACENT.appliesToAndVisible(roadSection1.getPositions()[1], PLAYER_A));
    assertFalse(
        DistinctionProperty.OPPONENTS_PRAWN_ADJACENT.appliesToAndVisible(roadSection1.getPositions()[3], PLAYER_A));
    assertFalse(
        DistinctionProperty.OPPONENTS_PRAWN_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[4], PLAYER_A));
    assertFalse(
        DistinctionProperty.OPPONENTS_PRAWN_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[2], PLAYER_A));
    assertFalse(
        DistinctionProperty.OPPONENTS_PRAWN_ADJACENT.appliesToAndVisible(roadSection1.getPositions()[2], PLAYER_B));
    assertFalse(
        DistinctionProperty.OPPONENTS_PRAWN_ADJACENT.appliesToAndVisible(roadSection2.getPositions()[3], PLAYER_B));
  }

  @Test
  public void testDistinctivePositionsRecognized() throws Exception {
    {
      final DistinctivePositionView distinctivePosition =
          DistinctivePositionView.determinePositionDistinction(roadSection0.getPositions()[1], PLAYER_A,
              new HashSet<DistinctionProperty>(Arrays.asList(DistinctionProperty.OWN_PRAWN_ADJACENT,
                  DistinctionProperty.OWN_PRAWN, DistinctionProperty.OPPONENTS_PRAWN_ADJACENT,
                  DistinctionProperty.OPPONENTS_PRAWN, DistinctionProperty.MIDPOINT_ADJACENT,
                  DistinctionProperty.MIDPOINT, DistinctionProperty.JOINT_ADJACENT)));
      assertNull(distinctivePosition);
    }
    {
      roadSection0.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.DISCOVERED);
      final DistinctivePositionView distinctivePosition =
          DistinctivePositionView.determinePositionDistinction(roadSection0.getPositions()[1], PLAYER_A,
              new HashSet<DistinctionProperty>(Arrays.asList(DistinctionProperty.OWN_PRAWN_ADJACENT,
                  DistinctionProperty.OWN_PRAWN, DistinctionProperty.OPPONENTS_PRAWN_ADJACENT,
                  DistinctionProperty.OPPONENTS_PRAWN, DistinctionProperty.MIDPOINT_ADJACENT,
                  DistinctionProperty.MIDPOINT, DistinctionProperty.JOINT_ADJACENT)));
      assertNotNull(distinctivePosition);
      assertEquals(roadSection0.getPositions()[1], distinctivePosition.getPosition());
      assertArrayEquals(new DistinctionProperty[]{DistinctionProperty.JOINT_ADJACENT,
          DistinctionProperty.MIDPOINT_ADJACENT},
          distinctivePosition.getDistinctionProperties());
    }
    {
      settlersUnitA.setVisibility(PrawnView.Visibility.VISIBLE);
      final DistinctivePositionView distinctivePosition =
          DistinctivePositionView.determinePositionDistinction(roadSection0.getPositions()[1], PLAYER_A,
              new HashSet<DistinctionProperty>(Arrays.asList(DistinctionProperty.OWN_PRAWN_ADJACENT,
                  DistinctionProperty.OWN_PRAWN, DistinctionProperty.OPPONENTS_PRAWN_ADJACENT,
                  DistinctionProperty.OPPONENTS_PRAWN, DistinctionProperty.MIDPOINT_ADJACENT,
                  DistinctionProperty.MIDPOINT, DistinctionProperty.JOINT_ADJACENT)));
      assertNotNull(distinctivePosition);
      assertEquals(roadSection0.getPositions()[1], distinctivePosition.getPosition());
      assertArrayEquals(new DistinctionProperty[]{DistinctionProperty.JOINT_ADJACENT,
          DistinctionProperty.MIDPOINT_ADJACENT, DistinctionProperty.OWN_PRAWN},
          distinctivePosition.getDistinctionProperties());
    }
    {
      final DistinctivePositionView distinctivePosition =
          DistinctivePositionView.determinePositionDistinction(roadSection0.getPositions()[1], PLAYER_A,
              new HashSet<DistinctionProperty>(Arrays.asList(DistinctionProperty.JOINT_ADJACENT,
                  DistinctionProperty.MIDPOINT_ADJACENT, DistinctionProperty.OWN_PRAWN)));
      assertNotNull(distinctivePosition);
      assertEquals(roadSection0.getPositions()[1], distinctivePosition.getPosition());
      assertArrayEquals(new DistinctionProperty[]{DistinctionProperty.JOINT_ADJACENT,
          DistinctionProperty.MIDPOINT_ADJACENT, DistinctionProperty.OWN_PRAWN},
          distinctivePosition.getDistinctionProperties());
    }
    {
      final DistinctivePositionView distinctivePosition =
          DistinctivePositionView.determinePositionDistinction(roadSection0.getPositions()[1], PLAYER_A,
              new HashSet<DistinctionProperty>(Arrays.asList(DistinctionProperty.JOINT_ADJACENT,
                  DistinctionProperty.MIDPOINT_ADJACENT)));
      assertNotNull(distinctivePosition);
      assertEquals(roadSection0.getPositions()[1], distinctivePosition.getPosition());
      assertArrayEquals(new DistinctionProperty[]{DistinctionProperty.JOINT_ADJACENT,
          DistinctionProperty.MIDPOINT_ADJACENT}, distinctivePosition.getDistinctionProperties());
    }
    assertNull(DistinctivePositionView.determinePositionDistinction(roadSection0.getPositions()[1], PLAYER_A,
        new HashSet<DistinctionProperty>(Arrays.asList(DistinctionProperty.JOINT,
            DistinctionProperty.MIDPOINT, DistinctionProperty.OPPONENTS_PRAWN,
            DistinctionProperty.OPPONENTS_PRAWN_ADJACENT, DistinctionProperty.OWN_PRAWN_ADJACENT))));
  }

}
