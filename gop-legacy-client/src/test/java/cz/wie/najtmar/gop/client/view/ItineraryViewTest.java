package cz.wie.najtmar.gop.client.view;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import cz.wie.najtmar.gop.common.Point2D;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;

public class ItineraryViewTest {

  static final double STEP_SIZE = 1.0;
  JointView joint00;
  JointView joint30;
  JointView joint33;
  RoadSectionView roadSection0;
  RoadSectionView roadSection1;
  RoadSectionView roadSection2;

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
  }

  /**
   * Method setUp().
   * @throws Exception when something unexpected happens
   */
  @Before
  public void setUp() throws Exception {
    joint00 = new JointView(0, "joint00", new Point2D.Double(0.0, 0.0));
    joint30 = new JointView(1, "joint30", new Point2D.Double(3.0, 0.0));
    joint33 = new JointView(2, "joint33", new Point2D.Double(3.0, 3.0));
    roadSection0 = new RoadSectionView(0, joint00, joint30, STEP_SIZE);
    roadSection1 = new RoadSectionView(1, joint30, joint33, STEP_SIZE);
    roadSection2 = new RoadSectionView(2, joint00, joint33, STEP_SIZE);
  }

  @Test
  public void testItineraryCreatedCorrectly() throws Exception {
    ItineraryView.Builder builder = new ItineraryView.Builder(roadSection0.getPositions()[1]);
    assertTrue(builder.isLastActionSuccessful());
    assertEquals(roadSection0.getPositions()[1], builder.getLastNodeAdded());
    assertNull(builder.build());

    // roadSection0[2]
    builder = builder.addNode(roadSection0.getPositions()[2]);
    assertNotNull(builder);
    assertTrue(builder.isLastActionSuccessful());
    assertEquals(roadSection0.getPositions()[2], builder.getLastNodeAdded());
    assertEquals(Arrays.asList(roadSection0.getPositions()[1], roadSection0.getPositions()[2]),
        builder.build().getNodes());

    // roadSection0[3]
    builder.addNode(roadSection0.getPositions()[3]);
    assertTrue(builder.isLastActionSuccessful());
    assertEquals(roadSection0.getPositions()[3], builder.getLastNodeAdded());
    assertEquals(Arrays.asList(
        roadSection0.getPositions()[1], roadSection0.getPositions()[2], roadSection0.getPositions()[3]),
        builder.build().getNodes());

    // roadSection2[3]
    builder = builder.addNode(roadSection2.getPositions()[3]);
    assertNotNull(builder);
    assertFalse(builder.isLastActionSuccessful());
    assertEquals(roadSection0.getPositions()[3], builder.getLastNodeAdded());
    assertEquals(Arrays.asList(
        roadSection0.getPositions()[1], roadSection0.getPositions()[2], roadSection0.getPositions()[3]),
        builder.build().getNodes());

    // roadSection2[4]
    builder.addNode(roadSection2.getPositions()[4]);
    assertTrue(builder.isLastActionSuccessful());
    assertEquals(roadSection2.getPositions()[4], builder.getLastNodeAdded());
    assertEquals(Arrays.asList(
        roadSection0.getPositions()[1], roadSection0.getPositions()[2], roadSection0.getPositions()[3],
        roadSection2.getPositions()[4]), builder.build().getNodes());

    // roadSection2[1]
    builder.addNode(roadSection2.getPositions()[1]);
    assertTrue(builder.isLastActionSuccessful());
    assertEquals(roadSection2.getPositions()[1], builder.getLastNodeAdded());
    assertEquals(Arrays.asList(
        roadSection0.getPositions()[1], roadSection0.getPositions()[2], roadSection0.getPositions()[3],
        roadSection2.getPositions()[4], roadSection2.getPositions()[1]), builder.build().getNodes());

    // repetition
    builder.addNode(roadSection2.getPositions()[1]);
    assertFalse(builder.isLastActionSuccessful());
    assertEquals(roadSection2.getPositions()[1], builder.getLastNodeAdded());
    assertEquals(Arrays.asList(
        roadSection0.getPositions()[1], roadSection0.getPositions()[2], roadSection0.getPositions()[3],
        roadSection2.getPositions()[4], roadSection2.getPositions()[1]), builder.build().getNodes());

    // roadSection2[3]
    builder.addNode(roadSection2.getPositions()[3]);
    assertTrue(builder.isLastActionSuccessful());
    assertEquals(roadSection2.getPositions()[3], builder.getLastNodeAdded());
    assertEquals(Arrays.asList(
        roadSection0.getPositions()[1], roadSection0.getPositions()[2], roadSection0.getPositions()[3],
        roadSection2.getPositions()[4], roadSection2.getPositions()[1], roadSection2.getPositions()[3]),
        builder.build().getNodes());

    builder = builder.removeLastNode();
    assertNotNull(builder);
    assertTrue(builder.isLastActionSuccessful());
    assertEquals(Arrays.asList(
        roadSection0.getPositions()[1], roadSection0.getPositions()[2], roadSection0.getPositions()[3],
        roadSection2.getPositions()[4], roadSection2.getPositions()[1]), builder.build().getNodes());

    builder.removeLastNode().removeLastNode().removeLastNode().removeLastNode();
    assertTrue(builder.isLastActionSuccessful());
    assertEquals(roadSection0.getPositions()[1], builder.getLastNodeAdded());
    assertNull(builder.build());

    builder.removeLastNode();
    assertFalse(builder.isLastActionSuccessful());
    assertEquals(roadSection0.getPositions()[1], builder.getLastNodeAdded());
  }

  @Test
  public void testMovingCorrectlyWorks() throws Exception {
    ItineraryView itinerary =
        (new ItineraryView.Builder(roadSection0.getPositions()[1]))
        .addNode(roadSection0.getPositions()[2])
        .addNode(roadSection0.getPositions()[3])
        .addNode(roadSection1.getPositions()[1])
        .addNode(roadSection2.getPositions()[4])
        .build();
    assertEquals(Arrays.asList(
        roadSection0.getPositions()[1], roadSection0.getPositions()[2], roadSection0.getPositions()[3],
        roadSection1.getPositions()[1], roadSection2.getPositions()[4]), itinerary.getNodes());
    assertFalse(itinerary.isFinished());

    itinerary.updateItinerary(roadSection0.getPositions()[2]);
    assertEquals(Arrays.asList(
        roadSection0.getPositions()[2], roadSection0.getPositions()[3], roadSection1.getPositions()[1],
        roadSection2.getPositions()[4]), itinerary.getNodes());
    assertFalse(itinerary.isFinished());

    itinerary.updateItinerary(roadSection0.getPositions()[3]);
    assertEquals(Arrays.asList(
        roadSection0.getPositions()[3], roadSection1.getPositions()[1], roadSection2.getPositions()[4]),
        itinerary.getNodes());
    assertFalse(itinerary.isFinished());

    itinerary.updateItinerary(roadSection1.getPositions()[2]);
    assertEquals(Arrays.asList(roadSection1.getPositions()[2], roadSection2.getPositions()[4]), itinerary.getNodes());
    assertFalse(itinerary.isFinished());

    itinerary.updateItinerary(roadSection1.getPositions()[3]);
    assertEquals(Arrays.asList(roadSection2.getPositions()[4]), itinerary.getNodes());
    assertTrue(itinerary.isFinished());
  }

  @Test
  public void testMovingCorrectlyWorks1() throws Exception {
    ItineraryView itinerary =
        (new ItineraryView.Builder(roadSection0.getPositions()[1]))
        .addNode(roadSection0.getPositions()[2])
        .addNode(roadSection0.getPositions()[3])
        .addNode(roadSection1.getPositions()[1])
        .addNode(roadSection2.getPositions()[4])
        .build();
    assertEquals(Arrays.asList(
        roadSection0.getPositions()[1], roadSection0.getPositions()[2], roadSection0.getPositions()[3],
        roadSection1.getPositions()[1], roadSection2.getPositions()[4]), itinerary.getNodes());
    assertFalse(itinerary.isFinished());

    itinerary.updateItinerary(roadSection1.getPositions()[0]);
    assertEquals(Arrays.asList(
        roadSection0.getPositions()[3], roadSection1.getPositions()[1], roadSection2.getPositions()[4]),
        itinerary.getNodes());

    itinerary.updateItinerary(roadSection2.getPositions()[4]);
    assertEquals(Arrays.asList(roadSection2.getPositions()[4]), itinerary.getNodes());
    assertTrue(itinerary.isFinished());
  }

  @Test
  public void testMovingCorrectlyWorks2() throws Exception {
    ItineraryView itinerary =
        (new ItineraryView.Builder(roadSection0.getPositions()[1]))
        .addNode(roadSection0.getPositions()[3])
        .addNode(roadSection1.getPositions()[2])
        .addNode(roadSection1.getPositions()[1])
        .build();
    assertEquals(Arrays.asList(
        roadSection0.getPositions()[1], roadSection0.getPositions()[3], roadSection1.getPositions()[2],
        roadSection1.getPositions()[1]), itinerary.getNodes());
    assertFalse(itinerary.isFinished());

    itinerary.updateItinerary(roadSection0.getPositions()[3]);
    assertEquals(Arrays.asList(
        roadSection0.getPositions()[3], roadSection1.getPositions()[2], roadSection1.getPositions()[1]),
        itinerary.getNodes());

    itinerary.updateItinerary(roadSection1.getPositions()[1]);
    assertEquals(Arrays.asList(
        roadSection1.getPositions()[1], roadSection1.getPositions()[2], roadSection1.getPositions()[1]),
        itinerary.getNodes());

    itinerary.updateItinerary(roadSection1.getPositions()[2]);
    assertEquals(Arrays.asList(
        roadSection1.getPositions()[2], roadSection1.getPositions()[1]), itinerary.getNodes());

    itinerary.updateItinerary(roadSection1.getPositions()[1]);
    assertEquals(Arrays.asList(roadSection1.getPositions()[1]), itinerary.getNodes());
    assertTrue(itinerary.isFinished());
  }

  @Test
  public void testTooLongMoveFails() throws Exception {
    ItineraryView itinerary =
        (new ItineraryView.Builder(roadSection0.getPositions()[1]))
        .addNode(roadSection0.getPositions()[2])
        .addNode(roadSection0.getPositions()[3])
        .addNode(roadSection1.getPositions()[1])
        .addNode(roadSection2.getPositions()[4])
        .build();
    assertEquals(Arrays.asList(
        roadSection0.getPositions()[1], roadSection0.getPositions()[2], roadSection0.getPositions()[3],
        roadSection1.getPositions()[1], roadSection2.getPositions()[4]), itinerary.getNodes());
    assertFalse(itinerary.isFinished());

    try {
      itinerary.updateItinerary(roadSection1.getPositions()[1]);
      fail("Should have thrown an exception.");
    } catch (ClientViewException exception) {
      assertEquals("Positions PositionView [roadSection=RoadSectionView [index=0, joints=[Point2D.Double[0.0, 0.0], "
          + "Point2D.Double[3.0, 0.0]]], index=1, coordinatesPair=Point2D.Double[1.0, 0.0]] and "
          + "PositionView [roadSection=RoadSectionView [index=1, joints=[Point2D.Double[3.0, 0.0], "
          + "Point2D.Double[3.0, 3.0]]], index=1, coordinatesPair=Point2D.Double[3.0, 1.0]] "
          + "are not on the same RoadSection .", exception.getMessage());
    }
  }

  @Test
  public void testMoveBeyondItineraryFails() throws Exception {
    ItineraryView itinerary =
        (new ItineraryView.Builder(roadSection0.getPositions()[1]))
        .addNode(roadSection0.getPositions()[3])
        .addNode(roadSection1.getPositions()[1])
        .build();
    assertEquals(Arrays.asList(
        roadSection0.getPositions()[1], roadSection0.getPositions()[3], roadSection1.getPositions()[1]),
        itinerary.getNodes());
    assertFalse(itinerary.isFinished());

    itinerary.updateItinerary(roadSection0.getPositions()[3]);
    try {
      itinerary.updateItinerary(roadSection1.getPositions()[2]);
      fail("Should have thrown an exception.");
    } catch (ClientViewException exception) {
      assertEquals("Destination of the move (PositionView [roadSection=RoadSectionView [index=1, "
          + "joints=[Point2D.Double[3.0, 0.0], Point2D.Double[3.0, 3.0]]], "
          + "index=2, coordinatesPair=Point2D.Double[3.0, 2.0]]) is beyond the end of the Itinerary.",
          exception.getMessage());
    }
  }

  @Test
  public void testMoveOutOfItineraryFails() throws Exception {
    ItineraryView itinerary =
        (new ItineraryView.Builder(roadSection0.getPositions()[1]))
        .addNode(roadSection0.getPositions()[3])
        .addNode(roadSection1.getPositions()[2])
        .addNode(roadSection1.getPositions()[1])
        .build();
    assertEquals(Arrays.asList(
        roadSection0.getPositions()[1], roadSection0.getPositions()[3], roadSection1.getPositions()[2],
        roadSection1.getPositions()[1]), itinerary.getNodes());
    assertFalse(itinerary.isFinished());

    itinerary.updateItinerary(roadSection0.getPositions()[3]);
    try {
      itinerary.updateItinerary(roadSection1.getPositions()[3]);
      fail("Should have thrown an exception.");
    } catch (ClientViewException exception) {
      assertEquals("PositionView [roadSection=RoadSectionView [index=1, joints=[Point2D.Double[3.0, 0.0], "
          + "Point2D.Double[3.0, 3.0]]], index=3, coordinatesPair=Point2D.Double[3.0, 3.0]] not on "
          + "Itinerary [PositionView [roadSection=RoadSectionView [index=1, joints=[Point2D.Double[3.0, 0.0], "
          + "Point2D.Double[3.0, 3.0]]], index=2, coordinatesPair=Point2D.Double[3.0, 2.0]], "
          + "PositionView [roadSection=RoadSectionView [index=1, joints=[Point2D.Double[3.0, 0.0], "
          + "Point2D.Double[3.0, 3.0]]], index=1, coordinatesPair=Point2D.Double[3.0, 1.0]]].", exception.getMessage());
    }
  }

  @Test
  public void testMoveOutOfItineraryFails1() throws Exception {
    ItineraryView itinerary =
        (new ItineraryView.Builder(roadSection0.getPositions()[1]))
        .addNode(roadSection0.getPositions()[3])
        .addNode(roadSection1.getPositions()[2])
        .addNode(roadSection1.getPositions()[1])
        .build();
    assertEquals(Arrays.asList(
        roadSection0.getPositions()[1], roadSection0.getPositions()[3], roadSection1.getPositions()[2],
        roadSection1.getPositions()[1]), itinerary.getNodes());
    assertFalse(itinerary.isFinished());

    itinerary.updateItinerary(roadSection0.getPositions()[3]);
    itinerary.updateItinerary(roadSection1.getPositions()[2]);
    try {
      itinerary.updateItinerary(roadSection1.getPositions()[3]);
      fail("Should have thrown an exception.");
    } catch (ClientViewException exception) {
      assertEquals("PositionView [roadSection=RoadSectionView [index=1, joints=[Point2D.Double[3.0, 0.0], "
          + "Point2D.Double[3.0, 3.0]]], index=3, coordinatesPair=Point2D.Double[3.0, 3.0]] not on "
          + "Itinerary [PositionView [roadSection=RoadSectionView [index=1, joints=[Point2D.Double[3.0, 0.0], "
          + "Point2D.Double[3.0, 3.0]]], index=2, coordinatesPair=Point2D.Double[3.0, 2.0]], "
          + "PositionView [roadSection=RoadSectionView [index=1, joints=[Point2D.Double[3.0, 0.0], "
          + "Point2D.Double[3.0, 3.0]]], index=1, coordinatesPair=Point2D.Double[3.0, 1.0]]].", exception.getMessage());
    }
  }

  @Test
  public void testMoveOutOfItineraryFails2() throws Exception {
    ItineraryView itinerary =
        (new ItineraryView.Builder(roadSection2.getPositions()[1]))
        .addNode(roadSection2.getPositions()[4])
        .addNode(roadSection1.getPositions()[1])
        .addNode(roadSection1.getPositions()[2])
        .build();
    assertEquals(Arrays.asList(
        roadSection2.getPositions()[1], roadSection2.getPositions()[4], roadSection1.getPositions()[1],
        roadSection1.getPositions()[2]), itinerary.getNodes());
    assertFalse(itinerary.isFinished());

    itinerary.updateItinerary(roadSection2.getPositions()[4]);
    try {
      itinerary.updateItinerary(roadSection1.getPositions()[0]);
      fail("Should have thrown an exception.");
    } catch (ClientViewException exception) {
      assertEquals("PositionView [roadSection=RoadSectionView [index=1, joints=[Point2D.Double[3.0, 0.0], "
          + "Point2D.Double[3.0, 3.0]]], index=0, coordinatesPair=Point2D.Double[3.0, 0.0]] not on "
          + "Itinerary [PositionView [roadSection=RoadSectionView [index=1, joints=[Point2D.Double[3.0, 0.0], "
          + "Point2D.Double[3.0, 3.0]]], index=1, coordinatesPair=Point2D.Double[3.0, 1.0]], "
          + "PositionView [roadSection=RoadSectionView [index=1, joints=[Point2D.Double[3.0, 0.0], "
          + "Point2D.Double[3.0, 3.0]]], index=2, coordinatesPair=Point2D.Double[3.0, 2.0]]].", exception.getMessage());
    }
  }

  @Test
  public void testMoveOutOfItineraryFails3() throws Exception {
    ItineraryView itinerary =
        (new ItineraryView.Builder(roadSection2.getPositions()[1]))
        .addNode(roadSection2.getPositions()[4])
        .addNode(roadSection1.getPositions()[1])
        .addNode(roadSection1.getPositions()[2])
        .build();
    assertEquals(Arrays.asList(
        roadSection2.getPositions()[1], roadSection2.getPositions()[4], roadSection1.getPositions()[1],
        roadSection1.getPositions()[2]), itinerary.getNodes());
    assertFalse(itinerary.isFinished());

    itinerary.updateItinerary(roadSection2.getPositions()[4]);
    itinerary.updateItinerary(roadSection1.getPositions()[1]);
    try {
      itinerary.updateItinerary(roadSection1.getPositions()[0]);
      fail("Should have thrown an exception.");
    } catch (ClientViewException exception) {
      assertEquals("PositionView [roadSection=RoadSectionView [index=1, joints=[Point2D.Double[3.0, 0.0], "
          + "Point2D.Double[3.0, 3.0]]], index=0, coordinatesPair=Point2D.Double[3.0, 0.0]] not on "
          + "Itinerary [PositionView [roadSection=RoadSectionView [index=1, joints=[Point2D.Double[3.0, 0.0], "
          + "Point2D.Double[3.0, 3.0]]], index=1, coordinatesPair=Point2D.Double[3.0, 1.0]], "
          + "PositionView [roadSection=RoadSectionView [index=1, joints=[Point2D.Double[3.0, 0.0], "
          + "Point2D.Double[3.0, 3.0]]], index=2, coordinatesPair=Point2D.Double[3.0, 2.0]]].", exception.getMessage());
    }
  }

  @Test
  public void testDestinationBeyondTheEndOfItineraryFails() throws Exception {
    ItineraryView itinerary =
        (new ItineraryView.Builder(roadSection0.getPositions()[1]))
        .addNode(roadSection0.getPositions()[2])
        .build();
    assertEquals(Arrays.asList(
        roadSection0.getPositions()[1], roadSection0.getPositions()[2]), itinerary.getNodes());
    assertFalse(itinerary.isFinished());

    try {
      itinerary.updateItinerary(roadSection0.getPositions()[3]);
      fail("Should have thrown an exception.");
    } catch (ClientViewException exception) {
      assertEquals("Destination of the move (PositionView [roadSection=RoadSectionView [index=0, "
          + "joints=[Point2D.Double[0.0, 0.0], Point2D.Double[3.0, 0.0]]], index=3, "
          + "coordinatesPair=Point2D.Double[3.0, 0.0]]) is beyond the end of the Itinerary.", exception.getMessage());
    }
  }

  @Test
  public void testDestinationBeyondTheEndOfItineraryFails1() throws Exception {
    ItineraryView itinerary =
        (new ItineraryView.Builder(roadSection0.getPositions()[1]))
        .addNode(roadSection0.getPositions()[2])
        .build();
    assertEquals(Arrays.asList(
        roadSection0.getPositions()[1], roadSection0.getPositions()[2]), itinerary.getNodes());
    assertFalse(itinerary.isFinished());

    itinerary.updateItinerary(roadSection0.getPositions()[2]);
    try {
      itinerary.updateItinerary(roadSection0.getPositions()[3]);
      fail("Should have thrown an exception.");
    } catch (ClientViewException exception) {
      assertEquals("The Itinerary is finished.", exception.getMessage());
    }
  }

  @Test
  public void testDestinationBeyondTheEndOfItineraryFails2() throws Exception {
    ItineraryView itinerary =
        (new ItineraryView.Builder(roadSection0.getPositions()[2]))
        .addNode(roadSection0.getPositions()[1])
        .build();
    assertEquals(Arrays.asList(
        roadSection0.getPositions()[2], roadSection0.getPositions()[1]), itinerary.getNodes());
    assertFalse(itinerary.isFinished());

    try {
      itinerary.updateItinerary(roadSection0.getPositions()[0]);
      fail("Should have thrown an exception.");
    } catch (ClientViewException exception) {
      assertEquals("Destination of the move (PositionView [roadSection=RoadSectionView [index=0, "
          + "joints=[Point2D.Double[0.0, 0.0], Point2D.Double[3.0, 0.0]]], index=0, "
          + "coordinatesPair=Point2D.Double[0.0, 0.0]]) is beyond the end of the Itinerary.", exception.getMessage());
    }
  }

  @Test
  public void testDestinationBeyondTheEndOfItineraryFails3() throws Exception {
    ItineraryView itinerary =
        (new ItineraryView.Builder(roadSection0.getPositions()[2]))
        .addNode(roadSection0.getPositions()[1])
        .build();
    assertEquals(Arrays.asList(
        roadSection0.getPositions()[2], roadSection0.getPositions()[1]), itinerary.getNodes());
    assertFalse(itinerary.isFinished());

    itinerary.updateItinerary(roadSection0.getPositions()[1]);
    try {
      itinerary.updateItinerary(roadSection0.getPositions()[0]);
      fail("Should have thrown an exception.");
    } catch (ClientViewException exception) {
      assertEquals("The Itinerary is finished.", exception.getMessage());
    }
  }

}
