package cz.wie.najtmar.gop.client.view;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import cz.wie.najtmar.gop.client.view.PlayerView;

import org.junit.Before;
import org.junit.Test;

import java.io.StringReader;
import java.util.UUID;

import javax.json.Json;

public class PlayerViewTest {

  static final double STEP_SIZE = 1.0;

  PlayerView playerA;
  PlayerView playerB;


  /**
   * Method setUp().
   * @throws Exception when something unexpected happens
   */
  @Before
  public void setUp() throws Exception {
    playerA = new PlayerView(new UserView("playerA", new UUID(12345, 67890)), 0);
    playerB = new PlayerView(new UserView("playerB", new UUID(98765, 43210)), 1);
  }

  @Test
  public void testAddPlayerEventCreatedCorrectly() throws Exception {
    assertEquals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"ADD_PLAYER\", "
          + "  \"time\": 123456, "
          + "  \"player\": { "
          + "    \"index\": 0, "
          + "    \"user\": { "
          + "      \"name\":\"playerA\", "
          + "      \"uuid\": \"00000000-0000-3039-0000-000000010932\" "
          + "    } "
          + "  } "
          + "}")).readObject(),
        playerA.createAddPlayerEvent(123456).getBody());
    assertEquals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"ADD_PLAYER\", "
          + "  \"time\": 123456, "
          + "  \"player\": { "
          + "    \"index\": 1, "
          + "    \"user\": { "
          + "      \"name\":\"playerB\", "
          + "      \"uuid\": \"00000000-0001-81cd-0000-00000000a8ca\" "
          + "    } "
          + "  } "
          + "}")).readObject(),
        playerB.createAddPlayerEvent(123456).getBody());
  }

  @Test
  public void testClientFailureEventCreatedCorrectly() throws Exception {
    assertEquals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"CLIENT_FAILURE\", "
          + "  \"time\": 123456, "
          + "  \"player\": 1, "
          + "  \"message\": \"something went wrong\" "
          + "}")).readObject(),
        playerB.createClientFailureEvent(123456, "something went wrong").getBody());
  }

  @Test
  public void testEqualsAndHashCodeWorkAsIntended() {
    assertNotEquals(playerA, playerB);
    assertEquals(playerA, playerA);
    assertEquals(playerA, new PlayerView(new UserView("playerA", new UUID(12345, 67890)), 0));
    assertNotEquals(playerA, new PlayerView(new UserView("playerA1", new UUID(98765, 43210)), 0));
    assertEquals(0, playerA.hashCode());
    assertEquals(1, playerB.hashCode());
  }

}
