package cz.wie.najtmar.gop.client.view;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import cz.wie.najtmar.gop.common.Constants;
import cz.wie.najtmar.gop.common.Point2D;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.UUID;

public class PositionViewTest {

  static final double STEP_SIZE = 1.0;
  JointView joint00;
  JointView joint30;
  JointView joint33;
  RoadSectionView roadSection0;
  RoadSectionView roadSection1;
  RoadSectionView roadSection2;
  PlayerView player;
  PrawnView settlersUnit;
  PrawnView warrior0;
  PrawnView warrior1;

  PositionView position0;
  PositionView position1;
  PositionView position11;
  PositionView position2;
  PositionView position3;
  PositionView position4;

  /**
   * @throws java.lang.Exception when test cannot be set up
   */
  @Before
  public void setUp() throws Exception {
    joint00 = new JointView(0, "joint00", new Point2D.Double(0.0, 0.0));
    joint30 = new JointView(1, "joint30", new Point2D.Double(3.0, 0.0));
    joint33 = new JointView(2, "joint33", new Point2D.Double(3.0, 3.0));
    roadSection0 = new RoadSectionView(0, joint00, joint30, STEP_SIZE);
    roadSection1 = new RoadSectionView(1, joint30, joint33, STEP_SIZE);
    roadSection2 = new RoadSectionView(2, joint00, joint33, STEP_SIZE);
    player = new PlayerView(new UserView("player", new UUID(12345, 67890)), 0);
    position0 = roadSection0.getPositions()[0];
    position1 = roadSection0.getPositions()[1];
    position11 = new PositionView(roadSection0, 1, new Point2D.Double(1.0, 0.0));
    position2 = roadSection0.getPositions()[3];
    position3 = roadSection1.getPositions()[3];
    position4 = roadSection2.getPositions()[4];
    settlersUnit = new PrawnView(PrawnView.Type.SETTLERS_UNIT, 0.0, player, 0, position0);
    settlersUnit.setCurrentPosition(position1);
    warrior0 = new PrawnView(PrawnView.Type.WARRIOR, Math.PI / 2.0, player, 1, position2);
    warrior1 = new PrawnView(PrawnView.Type.WARRIOR, Math.PI / 2.0, player, 1, position4);
  }

  @Test
  public void testPositionCreationSucceeds() throws Exception {
    assertEquals(0, position0.getIndex());
    assertEquals(roadSection0, position0.getRoadSection());
    assertEquals(3, position2.getIndex());
    assertEquals(position1.hashCode(), position1.hashCode());
    assertNotEquals(position1.hashCode(), position2.hashCode());
    assertEquals(position11.hashCode(), position1.hashCode());
    assertEquals(position3.hashCode(), position4.hashCode());
  }

  @Test
  public void testAdjacentPositionsFound() throws Exception {
    assertEquals(new HashSet<PositionView>(
        Arrays.asList(joint00.getNormalizedPosition(), roadSection0.getPositions()[2])),
        roadSection0.getPositions()[1].getAdjacentPositions());
    assertEquals(new HashSet<PositionView>(
        Arrays.asList(roadSection2.getPositions()[0], roadSection0.getPositions()[2])),
        roadSection0.getPositions()[1].getAdjacentPositions());
    assertEquals(new HashSet<PositionView>(
        Arrays.asList(roadSection0.getPositions()[1], joint30.getNormalizedPosition())),
        roadSection0.getPositions()[2].getAdjacentPositions());
    assertEquals(new HashSet<PositionView>(
        Arrays.asList(roadSection0.getPositions()[1], roadSection1.getPositions()[0])),
        roadSection0.getPositions()[2].getAdjacentPositions());

    assertEquals(new HashSet<PositionView>(
        Arrays.asList(roadSection0.getPositions()[1], roadSection2.getPositions()[1])),
        roadSection0.getPositions()[0].getAdjacentPositions());
    assertEquals(new HashSet<PositionView>(
        Arrays.asList(roadSection0.getPositions()[1], roadSection2.getPositions()[1])),
        roadSection2.getPositions()[0].getAdjacentPositions());
    assertEquals(new HashSet<PositionView>(
        Arrays.asList(roadSection0.getPositions()[2], roadSection1.getPositions()[1])),
        roadSection0.getPositions()[3].getAdjacentPositions());
    assertEquals(new HashSet<PositionView>(
        Arrays.asList(roadSection0.getPositions()[2], roadSection1.getPositions()[1])),
        roadSection1.getPositions()[0].getAdjacentPositions());
  }

  @Test
  public void testPositionOutOfBoundFails() {
    try {
      new PositionView(roadSection0, -1, new Point2D.Double(0.0, 0.0));
      fail("Should have thrown an exception.");
    } catch (ClientViewException exception) {
      assertEquals("index must be between 0 and 3 (found -1).", exception.getMessage());
    }
    try {
      new PositionView(roadSection0, 4, new Point2D.Double(4.0, 0.0));
      fail("Should have thrown an exception.");
    } catch (ClientViewException exception) {
      assertEquals("index must be between 0 and 3 (found 4).", exception.getMessage());
    }
  }

  @Test
  public void testGetJointWorks() {
    assertEquals(joint00, position0.getJoint());
    assertEquals(joint30, position2.getJoint());
    assertEquals(null, position1.getJoint());
  }

  @Test
  public void testGetRoadSectionsWorks() {
    assertEquals(new HashSet<>(Arrays.asList(roadSection0, roadSection2)),
        roadSection0.getPositions()[0].getRoadSections());
    assertEquals(new HashSet<>(Arrays.asList(roadSection0, roadSection2)),
        roadSection2.getPositions()[0].getRoadSections());
    assertEquals(new HashSet<>(Arrays.asList(roadSection0, roadSection1)),
        roadSection0.getPositions()[3].getRoadSections());
    assertEquals(new HashSet<>(Arrays.asList(roadSection0, roadSection1)),
        roadSection1.getPositions()[0].getRoadSections());
    assertEquals(new HashSet<>(Arrays.asList(roadSection1, roadSection2)),
        roadSection1.getPositions()[3].getRoadSections());
    assertEquals(new HashSet<>(Arrays.asList(roadSection1, roadSection2)),
        roadSection2.getPositions()[4].getRoadSections());
    assertEquals(new HashSet<>(Arrays.asList(roadSection0)), position1.getRoadSections());
  }

  @Test
  public void testDistanceToJointWorks() {
    assertEquals(0, roadSection0.getPositions()[0].getDistanceToJoint());
    assertEquals(1, roadSection0.getPositions()[1].getDistanceToJoint());
    assertEquals(1, roadSection0.getPositions()[2].getDistanceToJoint());
    assertEquals(0, roadSection0.getPositions()[3].getDistanceToJoint());
  }

  @Test
  public void testComparisonWorks() throws Exception {
    assertEquals(position1, position1);
    assertNotEquals(position11, position1);
    assertNotEquals(position0, position1);
    assertNotEquals(joint00, position1);
    assertEquals(position4, position3);
  }

  @Test
  public void testComparisonOnJointWorks() throws Exception {
    PositionView position3 = new PositionView(roadSection1, 0, new Point2D.Double(3.0, 0.0));
    assertTrue(position2.equals(position3));
    assertEquals(position2.hashCode(), position3.hashCode());
  }

  @Test
  public void testDistanceCalculationWorks() {
    assertEquals(2.0, position3.calculateDistance(new Point2D.Double(1.0, 3.0)), Constants.DOUBLE_DELTA);
    assertEquals(2.0, position3.calculateDistance(new Point2D.Double(5.0, 3.0)), Constants.DOUBLE_DELTA);
    assertEquals(2.0, position3.calculateDistance(new Point2D.Double(3.0, 1.0)), Constants.DOUBLE_DELTA);
    assertEquals(2.0, position3.calculateDistance(new Point2D.Double(3.0, 5.0)), Constants.DOUBLE_DELTA);
    assertEquals(0.0, position3.calculateDistance(new Point2D.Double(3.0, 3.0)), Constants.DOUBLE_DELTA);
    assertEquals(Math.sqrt(2.0), position3.calculateDistance(new Point2D.Double(2.0, 2.0)), Constants.DOUBLE_DELTA);
    assertEquals(Math.sqrt(2.0), position3.calculateDistance(new Point2D.Double(4.0, 4.0)), Constants.DOUBLE_DELTA);
  }

  @Test
  public void testCommonRoadSectionCorrectlyFound() {
    assertEquals(roadSection0,
        PositionView.findCommonRoadSection(roadSection0.getPositions()[1], roadSection0.getPositions()[2]));
    assertEquals(roadSection0,
        PositionView.findCommonRoadSection(roadSection0.getPositions()[0], roadSection0.getPositions()[2]));
    assertEquals(roadSection0,
        PositionView.findCommonRoadSection(roadSection0.getPositions()[1], roadSection0.getPositions()[3]));
    assertEquals(roadSection0,
        PositionView.findCommonRoadSection(roadSection0.getPositions()[1], roadSection1.getPositions()[0]));
    assertEquals(roadSection0,
        PositionView.findCommonRoadSection(roadSection0.getPositions()[2], roadSection0.getPositions()[2]));
    assertEquals(roadSection0,
        PositionView.findCommonRoadSection(roadSection2.getPositions()[0], roadSection1.getPositions()[0]));
    assertEquals(roadSection1,
        PositionView.findCommonRoadSection(roadSection0.getPositions()[3], roadSection2.getPositions()[4]));
  }

  @Test
  public void testPrawnsOnPositionsFoundCorrectly() {
    assertEquals(new HashSet<>(), position0.getPrawns());
    assertEquals(new HashSet<>(Arrays.asList(settlersUnit)), position1.getPrawns());
    assertEquals(new HashSet<>(), position11.getPrawns());
    assertEquals(new HashSet<>(Arrays.asList(warrior0)), position2.getPrawns());
    assertEquals(new HashSet<>(Arrays.asList(warrior1)), position3.getPrawns());
    assertEquals(new HashSet<>(Arrays.asList(warrior1)), position4.getPrawns());
  }

  @Test
  public void testVisibilityCheck() throws Exception {
    assertEquals(RoadSectionView.Visibility.INVISIBLE, roadSection0.getPositions()[0].getVisibility());
    assertEquals(RoadSectionView.Visibility.INVISIBLE, roadSection0.getPositions()[1].getVisibility());
    assertEquals(RoadSectionView.Visibility.INVISIBLE, roadSection0.getPositions()[2].getVisibility());
    assertEquals(RoadSectionView.Visibility.INVISIBLE, roadSection0.getPositions()[3].getVisibility());
    assertEquals(RoadSectionView.Visibility.INVISIBLE, roadSection2.getPositions()[0].getVisibility());
    assertEquals(RoadSectionView.Visibility.INVISIBLE, roadSection1.getPositions()[3].getVisibility());
    assertEquals(RoadSectionView.Visibility.INVISIBLE, roadSection2.getPositions()[4].getVisibility());
  }

  @Test
  public void testVisibilityCheck1() throws Exception {
    roadSection0.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.DISCOVERED);
    assertEquals(RoadSectionView.Visibility.DISCOVERED, roadSection0.getPositions()[0].getVisibility());
    assertEquals(RoadSectionView.Visibility.DISCOVERED, roadSection0.getPositions()[1].getVisibility());
    assertEquals(RoadSectionView.Visibility.DISCOVERED, roadSection0.getPositions()[2].getVisibility());
    assertEquals(RoadSectionView.Visibility.INVISIBLE, roadSection0.getPositions()[3].getVisibility());
    assertEquals(RoadSectionView.Visibility.DISCOVERED, roadSection2.getPositions()[0].getVisibility());
    assertEquals(RoadSectionView.Visibility.INVISIBLE, roadSection1.getPositions()[3].getVisibility());
    assertEquals(RoadSectionView.Visibility.INVISIBLE, roadSection2.getPositions()[4].getVisibility());
  }

  @Test
  public void testVisibilityCheck2() throws Exception {
    roadSection0.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.DISCOVERED);
    roadSection2.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.VISIBLE);
    assertEquals(RoadSectionView.Visibility.VISIBLE, roadSection0.getPositions()[0].getVisibility());
    assertEquals(RoadSectionView.Visibility.DISCOVERED, roadSection0.getPositions()[1].getVisibility());
    assertEquals(RoadSectionView.Visibility.DISCOVERED, roadSection0.getPositions()[2].getVisibility());
    assertEquals(RoadSectionView.Visibility.INVISIBLE, roadSection0.getPositions()[3].getVisibility());
    assertEquals(RoadSectionView.Visibility.VISIBLE, roadSection2.getPositions()[0].getVisibility());
    assertEquals(RoadSectionView.Visibility.VISIBLE, roadSection2.getPositions()[1].getVisibility());
    assertEquals(RoadSectionView.Visibility.INVISIBLE, roadSection1.getPositions()[3].getVisibility());
    assertEquals(RoadSectionView.Visibility.INVISIBLE, roadSection2.getPositions()[4].getVisibility());
  }

  @Test
  public void testVisibilityCheck3() throws Exception {
    roadSection0.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.VISIBLE);
    roadSection2.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.DISCOVERED);
    assertEquals(RoadSectionView.Visibility.VISIBLE, roadSection0.getPositions()[0].getVisibility());
    assertEquals(RoadSectionView.Visibility.VISIBLE, roadSection0.getPositions()[1].getVisibility());
    assertEquals(RoadSectionView.Visibility.VISIBLE, roadSection0.getPositions()[2].getVisibility());
    assertEquals(RoadSectionView.Visibility.INVISIBLE, roadSection0.getPositions()[3].getVisibility());
    assertEquals(RoadSectionView.Visibility.VISIBLE, roadSection2.getPositions()[0].getVisibility());
    assertEquals(RoadSectionView.Visibility.DISCOVERED, roadSection2.getPositions()[1].getVisibility());
    assertEquals(RoadSectionView.Visibility.INVISIBLE, roadSection1.getPositions()[3].getVisibility());
    assertEquals(RoadSectionView.Visibility.INVISIBLE, roadSection2.getPositions()[4].getVisibility());
  }

  @Test
  public void testVisibilityCheck4() throws Exception {
    roadSection0.setHalfVisibility(RoadSectionView.Direction.FORWARD, RoadSectionView.Visibility.DISCOVERED);
    assertEquals(RoadSectionView.Visibility.INVISIBLE, roadSection0.getPositions()[0].getVisibility());
    assertEquals(RoadSectionView.Visibility.INVISIBLE, roadSection0.getPositions()[1].getVisibility());
    assertEquals(RoadSectionView.Visibility.DISCOVERED, roadSection0.getPositions()[2].getVisibility());
    assertEquals(RoadSectionView.Visibility.DISCOVERED, roadSection0.getPositions()[3].getVisibility());
    assertEquals(RoadSectionView.Visibility.INVISIBLE, roadSection2.getPositions()[0].getVisibility());
    assertEquals(RoadSectionView.Visibility.INVISIBLE, roadSection1.getPositions()[3].getVisibility());
    assertEquals(RoadSectionView.Visibility.INVISIBLE, roadSection2.getPositions()[4].getVisibility());
  }

  @Test
  public void testVisibilityCheck5() throws Exception {
    roadSection0.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.DISCOVERED);
    roadSection0.setHalfVisibility(RoadSectionView.Direction.FORWARD, RoadSectionView.Visibility.VISIBLE);
    assertEquals(RoadSectionView.Visibility.DISCOVERED, roadSection0.getPositions()[0].getVisibility());
    assertEquals(RoadSectionView.Visibility.DISCOVERED, roadSection0.getPositions()[1].getVisibility());
    assertEquals(RoadSectionView.Visibility.VISIBLE, roadSection0.getPositions()[2].getVisibility());
    assertEquals(RoadSectionView.Visibility.VISIBLE, roadSection0.getPositions()[3].getVisibility());
    assertEquals(RoadSectionView.Visibility.DISCOVERED, roadSection2.getPositions()[0].getVisibility());
    assertEquals(RoadSectionView.Visibility.VISIBLE, roadSection1.getPositions()[0].getVisibility());
    assertEquals(RoadSectionView.Visibility.INVISIBLE, roadSection1.getPositions()[3].getVisibility());
    assertEquals(RoadSectionView.Visibility.INVISIBLE, roadSection2.getPositions()[4].getVisibility());
  }

  @Test
  public void testVisibilityCheck6() throws Exception {
    roadSection0.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.VISIBLE);
    roadSection0.setHalfVisibility(RoadSectionView.Direction.FORWARD, RoadSectionView.Visibility.DISCOVERED);
    assertEquals(RoadSectionView.Visibility.VISIBLE, roadSection0.getPositions()[0].getVisibility());
    assertEquals(RoadSectionView.Visibility.VISIBLE, roadSection0.getPositions()[1].getVisibility());
    assertEquals(RoadSectionView.Visibility.VISIBLE, roadSection0.getPositions()[2].getVisibility());
    assertEquals(RoadSectionView.Visibility.DISCOVERED, roadSection0.getPositions()[3].getVisibility());
    assertEquals(RoadSectionView.Visibility.VISIBLE, roadSection2.getPositions()[0].getVisibility());
    assertEquals(RoadSectionView.Visibility.DISCOVERED, roadSection1.getPositions()[0].getVisibility());
    assertEquals(RoadSectionView.Visibility.INVISIBLE, roadSection1.getPositions()[3].getVisibility());
    assertEquals(RoadSectionView.Visibility.INVISIBLE, roadSection2.getPositions()[4].getVisibility());
  }

}
