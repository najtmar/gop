package cz.wie.najtmar.gop.client.view;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import cz.wie.najtmar.gop.client.view.JointView;
import cz.wie.najtmar.gop.client.view.PlayerView;
import cz.wie.najtmar.gop.client.view.PrawnView;
import cz.wie.najtmar.gop.client.view.PrawnView.Type;
import cz.wie.najtmar.gop.client.view.RoadSectionView;
import cz.wie.najtmar.gop.common.Point2D;
import cz.wie.najtmar.gop.event.Event;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;

import javax.json.Json;

public class PrawnViewTest {

  static final double STEP_SIZE = 1.0;
  static PlayerView PLAYER_A;
  static PlayerView PLAYER_B;

  JointView joint00;
  JointView joint30;
  JointView joint33;
  RoadSectionView roadSection0;
  RoadSectionView roadSection1;
  RoadSectionView roadSection2;
  PrawnView settlersUnitA;
  PrawnView warriorA;
  PrawnView settlersUnitB;
  PrawnView warriorB;

  /**
   * Method setUpBeforeClass().
   * @throws Exception when something unexpected happens
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    PLAYER_A = new PlayerView(new UserView("Player A", new UUID(12345, 67890)), 0);
    PLAYER_B = new PlayerView(new UserView("Player B", new UUID(98765, 43210)), 1);
  }

  /**
   * Method setUp().
   * @throws Exception when something unexpected happens
   */
  @Before
  public void setUp() throws Exception {
    joint00 = new JointView(0, "joint00", new Point2D.Double(0.0, 0.0));
    joint30 = new JointView(1, "joint30", new Point2D.Double(3.0, 0.0));
    joint33 = new JointView(2, "joint33", new Point2D.Double(3.0, 3.0));
    roadSection0 = new RoadSectionView(0, joint00, joint30, STEP_SIZE);
    roadSection1 = new RoadSectionView(1, joint33, joint30, STEP_SIZE);
    roadSection2 = new RoadSectionView(2, joint00, joint33, STEP_SIZE);
    settlersUnitA = new PrawnView(Type.SETTLERS_UNIT, 0.0, PLAYER_A, 0, joint00.getNormalizedPosition());
    warriorA = new PrawnView(Type.WARRIOR, Math.PI / 2.0, PLAYER_A, 1, joint00.getNormalizedPosition());
    settlersUnitB = new PrawnView(Type.SETTLERS_UNIT, 0.0, PLAYER_B, 3, joint30.getNormalizedPosition());
    warriorB = new PrawnView(Type.WARRIOR, 3.0 * Math.PI / 2.0, PLAYER_B, 2, joint30.getNormalizedPosition());
  }

  @Test
  public void testPrawnsCreatedCorrectly() {
    assertEquals(joint00.getNormalizedPosition(), settlersUnitA.getCurrentPosition());
    assertEquals(PrawnView.MAX_ENERGY, settlersUnitA.getEnergy(), 0.0);
    assertEquals(0, settlersUnitA.getId());
    assertEquals(PLAYER_A, settlersUnitA.getPlayer());
    assertEquals(PrawnView.Type.SETTLERS_UNIT, settlersUnitA.getType());
    assertEquals(0.0, settlersUnitA.getWarriorDirection(), 0.0);
    assertEquals(new HashSet<>(), settlersUnitA.getBattles());
    assertEquals(PrawnView.Visibility.INVISIBLE, settlersUnitA.getVisibility());

    assertEquals(joint30.getNormalizedPosition(), warriorB.getCurrentPosition());
    assertEquals(PrawnView.MAX_ENERGY, warriorB.getEnergy(), 0.0);
    assertEquals(2, warriorB.getId());
    assertEquals(PLAYER_B, warriorB.getPlayer());
    assertEquals(PrawnView.Type.WARRIOR, warriorB.getType());
    assertEquals(3.0 * Math.PI / 2.0, warriorB.getWarriorDirection(), 0.0);
    assertEquals(new HashSet<>(), warriorB.getBattles());
    assertEquals(PrawnView.Visibility.INVISIBLE, warriorB.getVisibility());
  }

  @Test
  public void testPositionPrawnBindingsCreatedCorrectly() {
    PrawnView prawn;

    final Set<PrawnView> joint00Prawns = joint00.getNormalizedPosition().getPrawns();
    final Iterator<PrawnView> joint00PrawnsIter = joint00Prawns.iterator();
    assertTrue(joint00PrawnsIter.hasNext());
    prawn = joint00PrawnsIter.next();
    assertEquals(settlersUnitA, prawn);
    assertTrue(joint00PrawnsIter.hasNext());
    prawn = joint00PrawnsIter.next();
    assertEquals(warriorA, prawn);
    assertFalse(joint00PrawnsIter.hasNext());

    final Set<PrawnView> joint30Prawns = joint30.getNormalizedPosition().getPrawns();
    Iterator<PrawnView> joint30PrawnsIter = joint30Prawns.iterator();
    assertTrue(joint30PrawnsIter.hasNext());
    prawn = joint30PrawnsIter.next();
    assertEquals(warriorB, prawn);
    assertTrue(joint30PrawnsIter.hasNext());
    prawn = joint30PrawnsIter.next();
    assertEquals(settlersUnitB, prawn);
    assertFalse(joint30PrawnsIter.hasNext());

    assertEquals(joint00Prawns, roadSection0.getPositions()[0].getPrawns());
    assertEquals(joint00Prawns, roadSection2.getPositions()[0].getPrawns());
    assertEquals(joint30Prawns, roadSection0.getPositions()[3].getPrawns());
    assertEquals(joint30Prawns, roadSection1.getPositions()[3].getPrawns());
    assertEquals(new HashSet<PrawnView>(), roadSection0.getPositions()[1].getPrawns());
  }

  @Test
  public void testMovingPrawnsWorks() throws Exception {
    PrawnView prawn;

    warriorA.setCurrentPosition(roadSection0.getPositions()[1]);
    assertEquals(new HashSet<PrawnView>(Arrays.asList(settlersUnitA)), joint00.getNormalizedPosition().getPrawns());
    assertEquals(new HashSet<PrawnView>(Arrays.asList(warriorA)), roadSection0.getPositions()[1].getPrawns());

    settlersUnitA.setCurrentPosition(roadSection0.getPositions()[1]);
    final Iterator<PrawnView> position1Iter = roadSection0.getPositions()[1].getPrawns().iterator();
    assertTrue(position1Iter.hasNext());
    prawn = position1Iter.next();
    assertEquals(settlersUnitA, prawn);
    assertTrue(position1Iter.hasNext());
    prawn = position1Iter.next();
    assertEquals(warriorA, prawn);
    assertFalse(position1Iter.hasNext());
    assertEquals(new HashSet<PrawnView>(), joint00.getNormalizedPosition().getPrawns());
    assertEquals(new HashSet<PrawnView>(), roadSection0.getPositions()[0].getPrawns());
    assertEquals(new HashSet<PrawnView>(), roadSection2.getPositions()[0].getPrawns());

    settlersUnitA.setCurrentPosition(roadSection0.getPositions()[0]);
    warriorA.setCurrentPosition(roadSection0.getPositions()[0]);
    assertEquals(new HashSet<PrawnView>(), roadSection0.getPositions()[1].getPrawns());
    final Set<PrawnView> joint00Prawns = joint00.getNormalizedPosition().getPrawns();
    assertEquals(new HashSet<PrawnView>(Arrays.asList(settlersUnitA, warriorA)), joint00Prawns);
    assertEquals(joint00Prawns, roadSection0.getPositions()[0].getPrawns());
    assertEquals(joint00Prawns, roadSection2.getPositions()[0].getPrawns());
  }

  @Test
  public void testRemovingPrawnsWorks() throws Exception {
    assertEquals(PrawnView.State.EXISTING, warriorA.getState());
    warriorA.setVisibility(PrawnView.Visibility.VISIBLE);
    assertEquals(PrawnView.Visibility.VISIBLE, warriorA.getVisibility());
    warriorA.remove();
    assertEquals(PrawnView.State.REMOVED, warriorA.getState());
    assertEquals(PrawnView.Visibility.INVISIBLE, warriorA.getVisibility());
    assertEquals(new HashSet<PrawnView>(Arrays.asList(settlersUnitA)), joint00.getNormalizedPosition().getPrawns());
    assertEquals(new HashSet<PrawnView>(Arrays.asList(settlersUnitA)), roadSection0.getPositions()[0].getPrawns());
    assertEquals(new HashSet<PrawnView>(Arrays.asList(settlersUnitA)), roadSection2.getPositions()[0].getPrawns());

    settlersUnitA.setCurrentPosition(roadSection0.getPositions()[1]);
    assertEquals(new HashSet<PrawnView>(Arrays.asList(settlersUnitA)), roadSection0.getPositions()[1].getPrawns());
    assertEquals(new HashSet<PrawnView>(), joint00.getNormalizedPosition().getPrawns());
    assertEquals(PrawnView.State.EXISTING, settlersUnitA.getState());
    settlersUnitA.remove();
    assertEquals(PrawnView.State.REMOVED, settlersUnitA.getState());
    assertEquals(new HashSet<PrawnView>(), roadSection0.getPositions()[1].getPrawns());
  }

  @Test
  public void testEqualsAndHashCodeWorkAsIntended() throws Exception {
    assertNotEquals(warriorA, warriorB);
    assertEquals(warriorA, warriorA);
    assertEquals(warriorA, new PrawnView(Type.WARRIOR, Math.PI / 2.0, PLAYER_A, 1, joint33.getNormalizedPosition()));
    assertEquals(1, warriorA.hashCode());
    assertEquals(2, warriorB.hashCode());
  }

  @Test
  public void testSettingCityCreationEventWorks() throws Exception {
    assertNull(settlersUnitA.getCityCreationEvent());
    settlersUnitA.setCityCreationEvent(new Event(Json.createObjectBuilder()
        .add("type", "ORDER_CITY_CREATION")
        .add("time", 123456)
        .add("settlersUnit", settlersUnitA.getId())
        .add("name", "My City")
        .build()));
    assertEquals(Json.createObjectBuilder()
        .add("type", "ORDER_CITY_CREATION")
        .add("time", 123456)
        .add("settlersUnit", settlersUnitA.getId())
        .add("name", "My City")
        .build(), settlersUnitA.getCityCreationEvent().getBody());
    settlersUnitA.setCityCreationEvent(new Event(Json.createObjectBuilder()
        .add("type", "ORDER_CITY_CREATION")
        .add("time", 123456)
        .add("settlersUnit", settlersUnitA.getId())
        .add("name", "My City 1")
        .build()));
    assertEquals(Json.createObjectBuilder()
        .add("type", "ORDER_CITY_CREATION")
        .add("time", 123456)
        .add("settlersUnit", settlersUnitA.getId())
        .add("name", "My City 1")
        .build(), settlersUnitA.getCityCreationEvent().getBody());
    settlersUnitA.setCityCreationEvent(null);
    assertNull(settlersUnitA.getCityCreationEvent());
  }

}
