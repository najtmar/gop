package cz.wie.najtmar.gop.client.view;

import static org.junit.Assert.assertEquals;

import cz.wie.najtmar.gop.client.view.DistinctivePositionView.DistinctionProperty;
import cz.wie.najtmar.gop.client.view.PrawnView.Type;
import cz.wie.najtmar.gop.common.Point2D;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

public class PositionSelectionTest {

  static final double STEP_SIZE = 1.0;
  static PlayerView PLAYER_A;
  static PlayerView PLAYER_B;
  static Set<DistinctionProperty> ALL_DISTINCTION_PROPERTIES;

  JointView joint00;
  JointView joint30;
  JointView joint33;
  RoadSectionView roadSection0;
  RoadSectionView roadSection1;
  RoadSectionView roadSection2;
  PrawnView settlersUnitA;
  PrawnView warriorA;
  PrawnView settlersUnitB;
  PrawnView warriorB;
  CityView city;
  PositionView[] testPositions;
  DistinctivePositionView[] testDistinctivePositions;

  /**
   * Method setUpBeforeClass().
   * @throws Exception when something unexpected happens
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    PLAYER_A = new PlayerView(new UserView("Player A", new UUID(12345, 67890)), 0);
    PLAYER_B = new PlayerView(new UserView("Player B", new UUID(98765, 43210)), 1);
    ALL_DISTINCTION_PROPERTIES = new HashSet<>(Arrays.asList(DistinctionProperty.values()));
  }

  /**
   * Method setUp().
   * @throws Exception when something unexpected happens
   */
  @Before
  public void setUp() throws Exception {
    joint00 = new JointView(0, "joint00", new Point2D.Double(0.0, 0.0));
    joint30 = new JointView(1, "joint30", new Point2D.Double(3.0, 0.0));
    joint33 = new JointView(2, "joint33", new Point2D.Double(3.0, 3.0));
    roadSection0 = new RoadSectionView(0, joint00, joint30, STEP_SIZE);
    roadSection1 = new RoadSectionView(1, joint30, joint33, STEP_SIZE);
    roadSection2 = new RoadSectionView(2, joint00, joint33, STEP_SIZE);
    settlersUnitA = new PrawnView(Type.SETTLERS_UNIT, 0.0, PLAYER_A, 0, joint00.getNormalizedPosition());
    settlersUnitA.setCurrentPosition(roadSection0.getPositions()[1]);
    warriorA = new PrawnView(Type.WARRIOR, Math.PI / 2.0, PLAYER_A, 1, joint00.getNormalizedPosition());
    warriorA.setCurrentPosition(roadSection0.getPositions()[3]);
    settlersUnitB = new PrawnView(Type.SETTLERS_UNIT, 0.0, PLAYER_B, 2, joint00.getNormalizedPosition());
    settlersUnitB.setCurrentPosition(roadSection1.getPositions()[3]);
    warriorB = new PrawnView(Type.WARRIOR, 3.0 * Math.PI / 2.0, PLAYER_B, 3, joint00.getNormalizedPosition());
    warriorB.setCurrentPosition(roadSection1.getPositions()[0]);
    city = new CityView("City", 7, joint00, PLAYER_A);
    testPositions = new PositionView[]{
        roadSection0.getPositions()[0],  // [0] + CITY
        roadSection0.getPositions()[1],  // [1] + CITY_ADJACENT
        roadSection0.getPositions()[2],  // [2] + MIDPOINT
        roadSection0.getPositions()[3],  // [3] + JOINT
        roadSection1.getPositions()[1],  // [4] + JOINT_ADJACENT
        roadSection1.getPositions()[2],  // [5] + MIDPOINT
        roadSection1.getPositions()[3],  // [6] - JOINT
        roadSection2.getPositions()[1],  // [7] - CITY_ADJACENT
        roadSection2.getPositions()[2],  // [8] - MIDPOINT
        roadSection2.getPositions()[3],  // [9] + JOINT_ADJACENT
    };
    city.setVisibility(CityView.Visibility.DISCOVERED);
    roadSection0.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.VISIBLE);
    roadSection1.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.VISIBLE);
    roadSection2.setHalfVisibility(RoadSectionView.Direction.BACKWARD, RoadSectionView.Visibility.VISIBLE);
    roadSection2.setHalfVisibility(RoadSectionView.Direction.FORWARD, RoadSectionView.Visibility.VISIBLE);
    testDistinctivePositions = new DistinctivePositionView[]{
      DistinctivePositionView.determinePositionDistinction(testPositions[0], PLAYER_A, ALL_DISTINCTION_PROPERTIES),
      DistinctivePositionView.determinePositionDistinction(testPositions[1], PLAYER_A, ALL_DISTINCTION_PROPERTIES),
      DistinctivePositionView.determinePositionDistinction(testPositions[2], PLAYER_A, ALL_DISTINCTION_PROPERTIES),
      DistinctivePositionView.determinePositionDistinction(testPositions[3], PLAYER_A, ALL_DISTINCTION_PROPERTIES),
      DistinctivePositionView.determinePositionDistinction(testPositions[4], PLAYER_A, ALL_DISTINCTION_PROPERTIES),
      DistinctivePositionView.determinePositionDistinction(testPositions[5], PLAYER_A, ALL_DISTINCTION_PROPERTIES),
      DistinctivePositionView.determinePositionDistinction(testPositions[9], PLAYER_A, ALL_DISTINCTION_PROPERTIES),
    };
  }

  @Test
  public void testPositionSelectionCreated() {
    final PositionSelection selection = new PositionSelection(PLAYER_A, new Point2D.Double(2.0, 1.0),
        new HashSet<PositionView>(Arrays.asList(testPositions)),
        new HashSet<DistinctivePositionView>(Arrays.asList(testDistinctivePositions)));
    assertEquals(new Point2D.Double(2.0, 1.0), selection.getSelectionPoint());
    assertEquals(10, selection.getAllPositions().size());
    assertEquals(new HashSet<PositionView>(Arrays.asList(testPositions)), selection.getAllPositions());
    final DistinctivePositionView[] distinctivePositions = selection.getDistinctivePositions();
    assertEquals(7, distinctivePositions.length);
    assertEquals(testPositions[0], distinctivePositions[0].getPosition());  // CITY
    assertEquals(testPositions[3], distinctivePositions[1].getPosition());  // JOINT
    assertEquals(testPositions[2], distinctivePositions[2].getPosition());  // MIDPOINT
    assertEquals(testPositions[5], distinctivePositions[3].getPosition());  // MIDPOINT
    assertEquals(testPositions[1], distinctivePositions[4].getPosition());  // CITY_ADJACENT
    assertEquals(testPositions[4], distinctivePositions[5].getPosition());  // JOINT_ADJACENT
    assertEquals(testPositions[9], distinctivePositions[6].getPosition());  // JOINT_ADJACENT
    assertEquals(PLAYER_A, selection.getPlayer());
  }

  @Test
  public void testDistinctivePositionSortingOrderStable() {
    final Random random = new Random(1234567890);
    for (int i = 0; i < 10; ++i) {
      final List<DistinctivePositionView> testDistinctivePositionsShuffled = Arrays.asList(testDistinctivePositions);
      Collections.shuffle(testDistinctivePositionsShuffled, random);
      final PositionSelection selection = new PositionSelection(PLAYER_A, new Point2D.Double(2.0, 1.0),
          new HashSet<PositionView>(Arrays.asList(testPositions)),
          new HashSet<DistinctivePositionView>(testDistinctivePositionsShuffled));
      assertEquals(10, selection.getAllPositions().size());
      assertEquals(new HashSet<PositionView>(Arrays.asList(testPositions)), selection.getAllPositions());
      final DistinctivePositionView[] distinctivePositions = selection.getDistinctivePositions();
      assertEquals(7, distinctivePositions.length);
      assertEquals("Iteration " + i,  testPositions[0], distinctivePositions[0].getPosition());
      assertEquals("Iteration " + i, testPositions[3], distinctivePositions[1].getPosition());
      assertEquals("Iteration " + i, testPositions[2], distinctivePositions[2].getPosition());
      assertEquals("Iteration " + i, testPositions[5], distinctivePositions[3].getPosition());
      assertEquals("Iteration " + i, testPositions[1], distinctivePositions[4].getPosition());
      assertEquals("Iteration " + i, testPositions[4], distinctivePositions[5].getPosition());
      assertEquals("Iteration " + i, testPositions[9], distinctivePositions[6].getPosition());
    }
  }

}
