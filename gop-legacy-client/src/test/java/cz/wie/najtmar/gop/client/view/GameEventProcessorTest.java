package cz.wie.najtmar.gop.client.view;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import cz.wie.najtmar.gop.common.Constants;
import cz.wie.najtmar.gop.common.Point2D;
import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.event.EventProcessingException;
import cz.wie.najtmar.gop.event.SerializationException;
import cz.wie.najtmar.gop.io.Channel;
import cz.wie.najtmar.gop.io.Message;

import org.junit.Before;
import org.junit.Test;

import java.io.StringReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;

import javax.json.Json;
import javax.json.JsonObject;

public class GameEventProcessorTest {

  static final double STEP_SIZE = 0.5;
  static final int PLAYER_COUNT = 2;
  static final JsonObject SIMPLE_GAME_SERIALIZED = Json.createReader(new StringReader(
      "{\n"
      + "  \"id\": \"00000000-0000-5ba0-0000-000000013435\", "
      + "  \"timestamp\": 1234567, "
      + "  \"board\": {\n"
      + "    \"properties\": [\n"
      + "      {\n"
      + "        \"key\": \"Board.ysize\",\n"
      + "        \"value\":\"10\"\n"
      + "      },\n"
      + "      {\n"
      + "        \"key\": \"Board.stepSize\",\n"
      + "        \"value\":\"" + STEP_SIZE + "\"\n"
      + "      },\n"
      + "      {\n"
      + "        \"key\": \"Board.xsize\",\n"
      + "        \"value\":\"15\"\n"
      + "      }\n"
      + "    ],\n"
      + "    \"joints\": [\n"
      + "      {\n"
      + "        \"index\": 0,\n"
      + "        \"name\": \"joint0\",\n"
      + "        \"x\": 0.0,\n"
      + "        \"y\": 0.0\n"
      + "      },\n"
      + "      {\n"
      + "        \"index\": 1,\n"
      + "        \"name\": \"joint1\",\n"
      + "        \"x\": 3.0,\n"
      + "        \"y\": 0.0\n"
      + "      },\n"
      + "      {\n"
      + "        \"index\": 2,\n"
      + "        \"name\": \"joint2\",\n"
      + "        \"x\": 0.0,\n"
      + "        \"y\": 3.0\n"
      + "      }\n"
      + "    ],\n"
      + "    \"roadSections\": [\n"
      + "      {\n"
      + "        \"index\": 0,\n"
      + "        \"joint0\": 0,\n"
      + "        \"joint1\": 1\n"
      + "      },\n"
      + "      {\n"
      + "        \"index\": 1,\n"
      + "        \"joint0\": 0,\n"
      + "        \"joint1\": 2\n"
      + "      }\n"
      + "    ]\n"
      + "  },\n"
      + "  \"players\": [\n"
      + "    {\n"
      + "      \"index\": 0, "
      + "      \"user\": { "
      + "        \"name\":\"playerA\", "
      + "        \"uuid\": \"00000000-0000-3039-0000-000000010932\" "
      + "      } "
      + "    },\n"
      + "    {\n"
      + "      \"index\": 1, "
      + "      \"user\": { "
      + "        \"name\":\"playerB\", "
      + "        \"uuid\": \"00000000-0001-81cd-0000-00000000a8ca\" "
      + "      } "
      + "    }\n"
      + "  ]\n"
      + "}"
      )).readObject();

  PlayerView playerA;
  PlayerView playerB;
  Channel channelA;
  Channel channelB;
  LinkedBlockingQueue<Message> outgoingQueueA;
  LinkedBlockingQueue<Message> ingoingQueueA;
  LinkedBlockingQueue<Message> outgoingQueueB;
  LinkedBlockingQueue<Message> ingoingQueueB;
  GameEventProcessor gameProcessorA;
  GameEventProcessor gameProcessorB;

  volatile Exception gameProcessorBThreadException;
  volatile Exception readWriteMessageThreadException;
  volatile Exception gameInitializationThreadException;

  /**
   * Method setUp().
   * @throws Exception when field creation fails
   */
  @SuppressWarnings("deprecation")
  @Before
  public void setUp() throws Exception {
    outgoingQueueB = new LinkedBlockingQueue<>();
    ingoingQueueB = new LinkedBlockingQueue<>();
    channelA = new Channel(ingoingQueueA, outgoingQueueA);
    channelB = new Channel(ingoingQueueB, outgoingQueueB);
    gameProcessorA = new GameEventProcessor(new UserView("playerA", new UUID(12345, 67890)), 0,
        new Channel(outgoingQueueA, ingoingQueueA));
    gameProcessorB = new GameEventProcessor(new UserView("playerB", new UUID(98765, 43210)), 1,
        new Channel(outgoingQueueB, ingoingQueueB));
    readWriteMessageThreadException = null;
    gameProcessorBThreadException = null;
    gameInitializationThreadException = null;
  }

  private void initializeGameProcessorB() throws Exception {
    final Thread gameProcessorBThread = new Thread(new Runnable() {
      @SuppressWarnings("deprecation")
      @Override
      public void run() {
        try {
          gameProcessorB.initialize();
          playerA = gameProcessorB.getGame().getPlayers()[0];
          playerB = gameProcessorB.getGame().getPlayers()[1];
        } catch (ClientViewException exception) {
          gameProcessorBThreadException = exception;
        }
      }
    });
    gameProcessorBThread.start();

    final Message addPlayerMessage = channelB.readMessage();
    assertEquals(1, addPlayerMessage.getEvents().length);
    assertEquals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"ADD_PLAYER\", "
          + "  \"time\": 0, "
          + "  \"player\": { "
          + "    \"user\": { "
          + "      \"name\": \"playerB\", "
          + "      \"uuid\": \"00000000-0001-81cd-0000-00000000a8ca\" "
          + "    }, "
          + "    \"index\": 1 "
          + "  } "
          + "}")).readObject(),
        addPlayerMessage.getEvents()[0].getBody());

    channelB.writeMessage(new Message(new Event[]{
        new Event(
            Json.createObjectBuilder()
            .add("type", "INITIALIZE_GAME")
            .add("time", 0)
            .add("game", SIMPLE_GAME_SERIALIZED)
            .build()),
        new Event(
            Json.createObjectBuilder()
            .add("type", "PRODUCE_SETTLERS_UNIT")
            .add("time", 0)
            .add("id", 0)
            .add("position",
                Json.createObjectBuilder()
                .add("roadSection", 0)
                .add("index", 0)
                .build())
            .add("player", 0)
            .build()),
        new Event(
            Json.createObjectBuilder()
            .add("type", "PRODUCE_SETTLERS_UNIT")
            .add("time", 0)
            .add("id", 1)
            .add("position",
                Json.createObjectBuilder()
                .add("roadSection", 0)
                .add("index", 6)
                .build())
            .add("player", 1)
            .build()),
        new Event(
            Json.createObjectBuilder()
            .add("type", "ROAD_SECTION_HALF_DISCOVERED")
            .add("time", 0)
            .add("roadSectionHalf",
                Json.createObjectBuilder()
                .add("roadSection", 0)
                .add("end", "BACKWARD")
                .build())
            .add("player", 1)
            .build()),
        new Event(
            Json.createObjectBuilder()
            .add("type", "ROAD_SECTION_HALF_DISCOVERED")
            .add("time", 0)
            .add("roadSectionHalf",
                Json.createObjectBuilder()
                .add("roadSection", 0)
                .add("end", "FORWARD")
                .build())
            .add("player", 1)
            .build()),
        }));
    gameProcessorBThread.join();
    assertNull(gameProcessorBThreadException);
  }

  @Test
  public void testProcessorProperlyInitialized() throws Exception {
    assertFalse(gameProcessorB.isInitialized());
    initializeGameProcessorB();
    final GameView gameB = gameProcessorB.getGame();
    assertNotNull(gameB);
    final JointView[] joints = gameB.getJoints();
    assertEquals(3, joints.length);
    assertEquals(new Point2D.Double(3.0, 0.0), joints[1].getCoordinatesPair());
    final RoadSectionView[] roadSections = gameB.getRoadSections();
    assertEquals(2, roadSections.length);
    assertEquals(0, roadSections[1].getJoints()[0].getIndex());
    assertEquals(2, roadSections[1].getJoints()[1].getIndex());
    final Map<Integer, PrawnView> prawnMap = gameB.getPrawnMap();
    assertEquals(2, prawnMap.size());
    assertEquals(joints[1], prawnMap.get(1).getCurrentPosition().getJoint());
    assertArrayEquals(
        new RoadSectionView.Visibility[]{RoadSectionView.Visibility.VISIBLE, RoadSectionView.Visibility.VISIBLE},
        gameB.getRoadSections()[0].getHalfVisibilities());
    assertArrayEquals(
        new RoadSectionView.Visibility[]{RoadSectionView.Visibility.INVISIBLE, RoadSectionView.Visibility.INVISIBLE},
        gameB.getRoadSections()[1].getHalfVisibilities());
  }

  @Test
  public void testReadAndWriteEventsWorks() throws Exception {
    final Event[] produceSettlersEvents = new Event[]{
        new Event(
            Json.createObjectBuilder()
            .add("type", "PRODUCE_SETTLERS_UNIT")
            .add("time", 10)
            .add("id", 0)
            .add("position",
                Json.createObjectBuilder()
                .add("roadSection", 0)
                .add("index", 0)
                .build())
            .add("player", 1)
            .build()),
        new Event(
            Json.createObjectBuilder()
            .add("type", "PRODUCE_SETTLERS_UNIT")
            .add("time", 10)
            .add("id", 1)
            .add("position",
                Json.createObjectBuilder()
                .add("roadSection", 0)
                .add("index", 6)
                .build())
            .add("player", 1)
            .build()),
        };

    assertFalse(gameProcessorB.isInitialized());
    final Thread gameInitializationThread = new Thread(new Runnable() {
      @SuppressWarnings("deprecation")
      @Override
      public void run() {
        try {
          gameProcessorB.initialize();
          playerA = gameProcessorB.getGame().getPlayers()[0];
          playerB = gameProcessorB.getGame().getPlayers()[1];
        } catch (ClientViewException exception) {
          gameInitializationThreadException = exception;
        }
      }
    });
    gameInitializationThread.start();

    final Message addPlayerMessage = channelB.readMessage();
    assertEquals(1, addPlayerMessage.getEvents().length);
    assertEquals(
        Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"ADD_PLAYER\", "
          + "  \"time\": 0, "
          + "  \"player\": { "
          + "    \"user\": { "
          + "      \"name\": \"playerB\", "
          + "      \"uuid\": \"00000000-0001-81cd-0000-00000000a8ca\" "
          + "    }, "
          + "    \"index\": 1 "
          + "  } "
          + "}")).readObject(),
        addPlayerMessage.getEvents()[0].getBody());

    channelB.writeMessage(new Message(new Event[]{
        new Event(
            Json.createObjectBuilder()
            .add("type", "INITIALIZE_GAME")
            .add("time", 0)
            .add("game", SIMPLE_GAME_SERIALIZED)
            .build())
        }));
    gameInitializationThread.join();
    assertNull(gameInitializationThreadException);
    final GameView gameB = gameProcessorB.getGame();
    assertEquals(0, gameB.getTime());

    final Thread readWriteMessageThread = new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          assertArrayEquals(produceSettlersEvents, gameProcessorB.readAndProcessEvents());
          gameProcessorB.writeEvents(new Event[]{
              new Event(
                  Json.createObjectBuilder()
                  .add("type", "TEST")
                  .add("time", 10)
                  .build()),
              new Event(
                  Json.createObjectBuilder()
                  .add("type", "TEST")
                  .add("time", 10)
                  .build()),
              });
        } catch (ClientViewException | EventProcessingException | SerializationException exception) {
          readWriteMessageThreadException = exception;
        }
      }
    });
    readWriteMessageThread.start();

    channelB.writeMessage(new Message(produceSettlersEvents));

    readWriteMessageThread.join();
    assertNull(readWriteMessageThreadException);
    final Message messageRead = channelB.readMessage();
    assertEquals(2, messageRead.getEvents().length);
    assertEquals(
        new Event(
            Json.createObjectBuilder()
            .add("type", "TEST")
            .add("time", 10)
            .build()),
        messageRead.getEvents()[1]);
    assertEquals(10, gameB.getTime());
  }

  @Test
  public void testIgnoringPlayersChecked() throws Exception {
    initializeGameProcessorB();
    final ObjectSerializer mockedObjectSerializer = spy(gameProcessorB.getObjectSerializer());
    gameProcessorB.injectObjectSerializer(mockedObjectSerializer);
    final GameView gameB = gameProcessorB.getGame();
    final Event sampleEvent = new Event(
        Json.createObjectBuilder()
        .add("type", "DESTROY_PRAWN")
        .add("time", 10)
        .add("prawn", 1)
        .build());

    when(mockedObjectSerializer.getIgnoringPlayers(sampleEvent))
        .thenReturn(new HashSet<>(Arrays.asList(playerB)))
        .thenReturn(new HashSet<>(Arrays.asList(playerA)));
    assertTrue(gameB.getPrawnMap().containsKey(1));
    assertEquals(PrawnView.State.EXISTING, gameB.getPrawnMap().get(1).getState());
    assertFalse(gameB.getRoadSections()[0].getPositions()[6].getPrawns().isEmpty());
    // First attempt.
    gameProcessorB.processEvent(sampleEvent);
    assertTrue(gameB.getPrawnMap().containsKey(1));
    assertEquals(PrawnView.State.EXISTING, gameB.getPrawnMap().get(1).getState());
    assertFalse(gameB.getRoadSections()[0].getPositions()[6].getPrawns().isEmpty());
    // Second attempt.
    gameProcessorB.processEvent(sampleEvent);
    assertTrue(gameB.getPrawnMap().containsKey(1));
    assertEquals(PrawnView.State.REMOVED, gameB.getPrawnMap().get(1).getState());
    assertTrue(gameB.getRoadSections()[0].getPositions()[6].getPrawns().isEmpty());

    verify(mockedObjectSerializer, times(2)).getIgnoringPlayers(sampleEvent);
  }

  @Test
  public void testEventProcessingWorks() throws Exception {
    initializeGameProcessorB();
    final GameView gameB = gameProcessorB.getGame();
    final JointView[] joints = gameB.getJoints();
    final RoadSectionView[] roadSections = gameB.getRoadSections();
    final CityView cityA = new CityView("cityA", 0, joints[2], playerA);
    final CityView cityB = new CityView("cityB", 1, joints[1], playerB);
    final PrawnView settlersUnitA = gameB.getPrawnMap().get(0);
    cityA.grow();
    cityA.getFactories().get(1).setAction(FactoryView.ActionType.GROW, null, -1.0);
    gameB.addCity(cityA);
    cityB.grow();
    cityB.getFactories().get(1).setAction(FactoryView.ActionType.GROW, null, -1.0);
    gameB.addCity(cityB);

    // ACTION_FINISHED
    assertEquals(FactoryView.State.IN_ACTION, cityB.getFactories().get(1).getState());
    assertEquals(FactoryView.ActionType.GROW, cityB.getFactories().get(1).getActionType());
    gameProcessorB.processEvent(new Event(
                  Json.createObjectBuilder()
                  .add("type", "ACTION_FINISHED")
                  .add("time", 10)
                  .add("factory", Json.createObjectBuilder()
                      .add("city", 1)
                      .add("index", 1)
                      .build())
                  .build()));
    assertEquals(FactoryView.State.IDLE, cityB.getFactories().get(1).getState());
    assertNull(cityB.getFactories().get(1).getActionType());

    // ACTION_FINISHED (Factory index out of bound)
    gameProcessorB.processEvent(new Event(
        Json.createObjectBuilder()
        .add("type", "ACTION_FINISHED")
        .add("time", 10)
        .add("factory", Json.createObjectBuilder()
            .add("city", 1)
            .add("index", 12)
            .build())
        .build()));

    // CAPTURE_OR_DESTROY_CITY (0)
    assertEquals(playerA, cityA.getPlayer());
    assertEquals(2, cityA.getFactories().size());
    assertEquals(CityView.State.EXISTING, cityA.getState());
    assertEquals(new HashSet<>(Arrays.asList(cityA)), gameB.getPlayerCities(playerA));
    assertEquals(new HashSet<>(Arrays.asList(cityB)), gameB.getPlayerCities(playerB));
    gameProcessorB.processEvent(new Event(
        Json.createObjectBuilder()
        .add("type", "CAPTURE_OR_DESTROY_CITY")
        .add("time", 10)
        .add("city", 0)
        .add("defender", 0)
        .add("attacker", 1)
        .build()));
    assertEquals(playerB, cityA.getPlayer());
    assertEquals(1, cityA.getFactories().size());
    assertEquals(CityView.State.EXISTING, cityA.getState());
    assertEquals(new HashSet<>(Arrays.asList()), gameB.getPlayerCities(playerA));
    assertEquals(new HashSet<>(Arrays.asList(cityA, cityB)), gameB.getPlayerCities(playerB));

    // DECREASE_PRAWN_ENERGY
    assertEquals(PrawnView.MAX_ENERGY, settlersUnitA.getEnergy(), 0.0);
    gameProcessorB.processEvent(new Event(
        Json.createObjectBuilder()
        .add("type", "DECREASE_PRAWN_ENERGY")
        .add("time", 10)
        .add("prawn", 0)
        .add("delta", 0.75)
        .build()));
    assertEquals(PrawnView.MAX_ENERGY - 0.75, settlersUnitA.getEnergy(), Constants.DOUBLE_DELTA);

    // DESTROY_PRAWN
    assertTrue(gameB.getPrawnMap().containsKey(1));
    assertEquals(PrawnView.State.EXISTING, gameB.getPrawnMap().get(1).getState());
    assertFalse(gameB.getRoadSections()[0].getPositions()[6].getPrawns().isEmpty());
    gameProcessorB.processEvent(new Event(
        Json.createObjectBuilder()
        .add("type", "DESTROY_PRAWN")
        .add("time", 10)
        .add("prawn", 1)
        .build()));
    assertTrue(gameB.getPrawnMap().containsKey(1));
    assertEquals(PrawnView.State.REMOVED, gameB.getPrawnMap().get(1).getState());
    assertTrue(gameB.getRoadSections()[0].getPositions()[6].getPrawns().isEmpty());

    // PRODUCE_SETTLERS_UNIT
    assertFalse(gameB.getPrawnMap().containsKey(2));
    gameProcessorB.processEvent(new Event(
        Json.createObjectBuilder()
        .add("type", "PRODUCE_SETTLERS_UNIT")
        .add("time", 10)
        .add("id", 2)
        .add("position", Json.createObjectBuilder()
            .add("roadSection", 0)
            .add("index", 6)
            .build())
        .add("player", 0)
        .build()));
    assertTrue(gameB.getPrawnMap().containsKey(2));
    assertEquals(2, gameB.getPrawnMap().get(2).getId());

    // PRODUCE_WARRIOR
    assertFalse(gameB.getPrawnMap().containsKey(3));
    gameProcessorB.processEvent(new Event(
        Json.createObjectBuilder()
        .add("type", "PRODUCE_WARRIOR")
        .add("time", 10)
        .add("id", 3)
        .add("position", Json.createObjectBuilder()
            .add("roadSection", 1)
            .add("index", 6)
            .build())
        .add("direction", 0.375)
        .add("player", 1)
        .build()));
    assertTrue(gameB.getPrawnMap().containsKey(3));
    assertEquals(0.375, gameB.getPrawnMap().get(3).getWarriorDirection(), Constants.DOUBLE_DELTA);

    // MOVE_PRAWN
    final PrawnView warriorB = gameB.getPrawnMap().get(3);
    warriorB.setItinerary(
        (new ItineraryView.Builder(warriorB.getCurrentPosition()))
        .addNode(roadSections[1].getPositions()[4])
        .addNode(roadSections[1].getPositions()[6])
        .build());
    assertEquals(roadSections[1].getPositions()[6], warriorB.getCurrentPosition());
    assertNotNull(warriorB.getItinerary());
    assertEquals(Arrays.asList(roadSections[1].getPositions()[6], roadSections[1].getPositions()[4],
        roadSections[1].getPositions()[6]), warriorB.getItinerary().getNodes());
    gameProcessorB.processEvent(new Event(
        Json.createObjectBuilder()
        .add("type", "MOVE_PRAWN")
        .add("time", 10)
        .add("prawn", 3)
        .add("move", Json.createObjectBuilder()
            .add("position", Json.createObjectBuilder()
                .add("roadSection", 1)
                .add("index", 4)
                .build())
            .add("moveTime", 9)
            .add("stop", false))
        .build()));
    assertEquals(roadSections[1].getPositions()[4], warriorB.getCurrentPosition());
    assertNotNull(warriorB.getItinerary());
    assertEquals(Arrays.asList(roadSections[1].getPositions()[4], roadSections[1].getPositions()[6]),
        warriorB.getItinerary().getNodes());
    gameProcessorB.processEvent(new Event(
        Json.createObjectBuilder()
        .add("type", "MOVE_PRAWN")
        .add("time", 10)
        .add("prawn", 3)
        .add("move", Json.createObjectBuilder()
            .add("position", Json.createObjectBuilder()
                .add("roadSection", 1)
                .add("index", 6)
                .build())
            .add("moveTime", 9)
            .add("stop", true))
        .build()));
    assertEquals(roadSections[1].getPositions()[6], warriorB.getCurrentPosition());
    assertNull(warriorB.getItinerary());

    // REPAIR_PRAWN
    assertEquals(PrawnView.MAX_ENERGY - 0.75, settlersUnitA.getEnergy(), Constants.DOUBLE_DELTA);
    gameProcessorB.processEvent(new Event(
        Json.createObjectBuilder()
        .add("type", "REPAIR_PRAWN")
        .add("time", 10)
        .add("prawn", 0)
        .add("delta", 0.5)
        .build()));
    assertEquals(PrawnView.MAX_ENERGY - 0.75 + 0.5, settlersUnitA.getEnergy(), Constants.DOUBLE_DELTA);

    // SET_FACTORY_PROGRESS
    cityB.getFactories().get(0).setAction(FactoryView.ActionType.PRODUCE_SETTLERS_UNIT, null, -1.0);
    assertEquals(0.0, cityB.getFactories().get(0).getProgress(), 0.0);
    gameProcessorB.processEvent(new Event(
        Json.createObjectBuilder()
        .add("type", "SET_FACTORY_PROGRESS")
        .add("time", 10)
        .add("factory", Json.createObjectBuilder()
            .add("city", 1)
            .add("index", 0)
            .build())
        .add("progress", 0.25)
        .build()));
    assertEquals(0.25, cityB.getFactories().get(0).getProgress(), 0.0);

    // GROW_CITY
    assertEquals(1, cityA.getFactories().size());
    gameProcessorB.processEvent(new Event(
        Json.createObjectBuilder()
        .add("type", "GROW_CITY")
        .add("time", 10)
        .add("city", 0)
        .build()));
    assertEquals(2, cityA.getFactories().size());

    // SHRINK_OR_DESTROY_CITY
    assertEquals(cityA, gameB.lookupCityById(0));
    assertEquals(new HashSet<>(Arrays.asList(cityA, cityB)), gameB.getPlayerCities(playerB));
    assertEquals(CityView.State.EXISTING, cityA.getState());
    assertEquals(2, cityA.getFactories().size());
    gameProcessorB.processEvent(new Event(
        Json.createObjectBuilder()
        .add("type", "SHRINK_OR_DESTROY_CITY")
        .add("time", 10)
        .add("city", 0)
        .add("factoryIndex", 0)
        .build()));
    assertEquals(cityA, gameB.lookupCityById(0));
    assertEquals(new HashSet<>(Arrays.asList(cityA, cityB)), gameB.getPlayerCities(playerB));
    assertEquals(CityView.State.EXISTING, cityA.getState());
    assertEquals(1, cityA.getFactories().size());
    gameProcessorB.processEvent(new Event(
        Json.createObjectBuilder()
        .add("type", "SHRINK_OR_DESTROY_CITY")
        .add("time", 10)
        .add("city", 0)
        .add("factoryIndex", 0)
        .build()));
    assertEquals(cityA, gameB.lookupCityById(0));
    assertEquals(new HashSet<>(Arrays.asList(cityB)), gameB.getPlayerCities(playerB));
    assertEquals(CityView.State.REMOVED, cityA.getState());

    // CAPTURE_OR_DESTROY_CITY (1)
    final CityView cityB1 = new CityView("cityB1", 2, joints[2], playerB);
    gameB.addCity(cityB1);
    assertEquals(playerB, cityB1.getPlayer());
    assertEquals(1, cityB1.getFactories().size());
    assertNotNull(gameB.lookupCityById(1));
    assertEquals(cityB1, gameB.lookupCityById(2));
    assertEquals(new HashSet<>(Arrays.asList(cityB, cityB1)), gameB.getPlayerCities(playerB));
    assertEquals(CityView.State.EXISTING, cityB1.getState());
    gameProcessorB.processEvent(new Event(
        Json.createObjectBuilder()
        .add("type", "CAPTURE_OR_DESTROY_CITY")
        .add("time", 10)
        .add("city", 2)
        .add("defender", 1)
        .add("attacker", 0)
        .build()));
    assertEquals(cityB1, gameB.lookupCityById(2));
    assertEquals(new HashSet<>(Arrays.asList(cityB)), gameB.getPlayerCities(playerB));
    assertEquals(CityView.State.REMOVED, cityB1.getState());

    // CREATE_CITY
    {
      assertNotNull(gameB.getPrawnMap().get(0));
      assertEquals(PrawnView.State.EXISTING, gameB.getPrawnMap().get(0).getState());
      assertNull(gameB.lookupCityById(3));
      assertNull(joints[0].getCity());
      settlersUnitA.setCurrentPosition(joints[0].getNormalizedPosition());
      gameProcessorB.processEvent(new Event(
          Json.createObjectBuilder()
          .add("type", "CREATE_CITY")
          .add("time", 10)
          .add("settlersUnit", 0)
          .add("id", 3)
          .add("name", "Sieradz")
          .build()));
      assertNotNull(gameB.getPrawnMap().get(0));
      assertEquals(PrawnView.State.REMOVED, gameB.getPrawnMap().get(0).getState());
      final CityView newCity = gameB.lookupCityById(3);
      assertNotNull(newCity);
      assertEquals(newCity, joints[0].getCity());
      assertEquals("Sieradz", newCity.getName());
      assertEquals(3, newCity.getId());
      assertEquals(playerA, newCity.getPlayer());
    }

    // ROAD_SECTION_HALF_DISCOVERED
    assertArrayEquals(
        new RoadSectionView.Visibility[]{RoadSectionView.Visibility.INVISIBLE, RoadSectionView.Visibility.INVISIBLE},
        gameB.getRoadSections()[1].getHalfVisibilities());
    gameProcessorB.processEvent(new Event(
        Json.createObjectBuilder()
        .add("type", "ROAD_SECTION_HALF_DISCOVERED")
        .add("time", 0)
        .add("roadSectionHalf",
            Json.createObjectBuilder()
            .add("roadSection", 1)
            .add("end", "FORWARD")
            .build())
        .add("player", 1)
        .build()));
    assertArrayEquals(
        new RoadSectionView.Visibility[]{RoadSectionView.Visibility.INVISIBLE, RoadSectionView.Visibility.VISIBLE},
        gameB.getRoadSections()[1].getHalfVisibilities());

    // ROAD_SECTION_HALF_DISAPPEARS
    gameProcessorB.processEvent(new Event(
        Json.createObjectBuilder()
        .add("type", "ROAD_SECTION_HALF_DISAPPEARS")
        .add("time", 0)
        .add("roadSectionHalf",
            Json.createObjectBuilder()
            .add("roadSection", 1)
            .add("end", "FORWARD")
            .build())
        .add("player", 1)
        .build()));
    assertArrayEquals(
        new RoadSectionView.Visibility[]{RoadSectionView.Visibility.INVISIBLE, RoadSectionView.Visibility.DISCOVERED},
        gameB.getRoadSections()[1].getHalfVisibilities());

    // ROAD_SECTION_HALF_APPEARS
    gameProcessorB.processEvent(new Event(
        Json.createObjectBuilder()
        .add("type", "ROAD_SECTION_HALF_APPEARS")
        .add("time", 0)
        .add("roadSectionHalf",
            Json.createObjectBuilder()
            .add("roadSection", 1)
            .add("end", "FORWARD")
            .build())
        .add("player", 1)
        .build()));
    assertArrayEquals(
        new RoadSectionView.Visibility[]{RoadSectionView.Visibility.INVISIBLE, RoadSectionView.Visibility.VISIBLE},
        gameB.getRoadSections()[1].getHalfVisibilities());

    // CITY_APPEARS
    final CityView cityA1 = new CityView("cityA1", 4, joints[2], playerA);
    gameB.addCity(cityA1);
    assertEquals(CityView.Visibility.INVISIBLE, cityA1.getVisibility());
    gameProcessorB.processEvent(new Event(
        Json.createObjectBuilder()
        .add("type", "CITY_APPEARS")
        .add("time", 0)
        .add("city", cityA1.getId())
        .add("player", 1)
        .build()));
    assertEquals(CityView.Visibility.DISCOVERED, cityA1.getVisibility());

    // CITY_SHOWN_DESTROYED
    cityA1.shrinkOrDestroy(cityA1.getFactories().size() - 1);
    gameB.removeCity(cityA1);
    assertEquals(CityView.Visibility.DISCOVERED, cityA1.getVisibility());
    gameProcessorB.processEvent(new Event(
        Json.createObjectBuilder()
        .add("type", "CITY_SHOWN_DESTROYED")
        .add("time", 0)
        .add("city", cityA1.getId())
        .add("player", 1)
        .build()));
    assertEquals(CityView.Visibility.DESTROYED, cityA1.getVisibility());

    // PRAWN_APPEARS
    assertEquals(PrawnView.Visibility.INVISIBLE, settlersUnitA.getVisibility());
    gameProcessorB.processEvent(new Event(
        Json.createObjectBuilder()
        .add("type", "PRAWN_APPEARS")
        .add("time", 0)
        .add("prawn", settlersUnitA.getId())
        .add("player", 1)
        .build()));
    assertEquals(PrawnView.Visibility.VISIBLE, settlersUnitA.getVisibility());

    // PRAWN_DISAPPEARS
    gameProcessorB.processEvent(new Event(
        Json.createObjectBuilder()
        .add("type", "PRAWN_DISAPPEARS")
        .add("time", 0)
        .add("prawn", settlersUnitA.getId())
        .add("player", 1)
        .build()));
    assertEquals(PrawnView.Visibility.INVISIBLE, settlersUnitA.getVisibility());

    // CHANGE_PLAYER_STATE
    gameProcessorB.processEvent(new Event(
        Json.createObjectBuilder()
        .add("type", "CHANGE_PLAYER_STATE")
        .add("time", 0)
        .add("player", 1)
        .add("state", "WON")
        .build()));
    assertEquals(PlayerView.State.IN_GAME, playerA.getState());
    assertEquals(PlayerView.State.WON, playerB.getState());
    gameProcessorB.processEvent(new Event(
        Json.createObjectBuilder()
        .add("type", "CHANGE_PLAYER_STATE")
        .add("time", 0)
        .add("player", 0)
        .add("state", "LOST")
        .build()));
    assertEquals(PlayerView.State.LOST, playerA.getState());
    assertEquals(PlayerView.State.WON, playerB.getState());
  }

}
