package cz.wie.najtmar.gop.client.view;

import static org.junit.Assert.assertEquals;

import cz.wie.najtmar.gop.client.view.PrawnView.Type;
import cz.wie.najtmar.gop.common.Constants;
import cz.wie.najtmar.gop.common.Point2D;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.UUID;

public class FactoryViewTest {

  static final double STEP_SIZE = 1.0;
  static PlayerView PLAYER;

  JointView joint00;
  JointView joint30;
  RoadSectionView roadSection;
  PrawnView prawn;
  CityView city;
  FactoryView testFactory;

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    PLAYER = new PlayerView(new UserView("Player", new UUID(12345, 67890)), 0);
  }

  /**
   * Method setUp().
   * @throws Exception when something unexpected happens
   */
  @Before
  public void setUp() throws Exception {
    joint00 = new JointView(0, "joint00", new Point2D.Double(0.0, 0.0));
    joint30 = new JointView(1, "joint30", new Point2D.Double(3.0, 0.0));
    roadSection = new RoadSectionView(0, joint00, joint30, STEP_SIZE);
    prawn = new PrawnView(Type.SETTLERS_UNIT, 0.0, PLAYER, 0, joint00.getNormalizedPosition());
    city = new CityView("city", 2, joint00, PLAYER);
    testFactory = new FactoryView(city, 2);
  }

  @Test
  public void testActionsProperlyStartedAndFinished() throws Exception {
    assertEquals(city, testFactory.getCity());
    assertEquals(2, testFactory.getIndex());
    assertEquals(FactoryView.State.IDLE, testFactory.getState());

    testFactory.setAction(FactoryView.ActionType.GROW, null, -1.0);
    assertEquals(FactoryView.State.IN_ACTION, testFactory.getState());
    assertEquals(FactoryView.ActionType.GROW, testFactory.getActionType());
    assertEquals(0.0, testFactory.getProgress(), 0.0);
    testFactory.finishAction();
    assertEquals(FactoryView.State.IDLE, testFactory.getState());

    testFactory.setAction(FactoryView.ActionType.PRODUCE_SETTLERS_UNIT, null, -1.0);
    assertEquals(FactoryView.State.IN_ACTION, testFactory.getState());
    assertEquals(FactoryView.ActionType.PRODUCE_SETTLERS_UNIT, testFactory.getActionType());
    assertEquals(0.0, testFactory.getProgress(), 0.0);

    prawn.setEnergy(0.5);
    testFactory.setAction(FactoryView.ActionType.REPAIR_PRAWN, prawn, -1.0);
    assertEquals(FactoryView.State.IN_ACTION, testFactory.getState());
    assertEquals(FactoryView.ActionType.REPAIR_PRAWN, testFactory.getActionType());
    assertEquals(0.5, testFactory.getProgress(), Constants.DOUBLE_DELTA);

    testFactory.setAction(FactoryView.ActionType.PRODUCE_WARRIOR, null, Math.E);
    assertEquals(FactoryView.State.IN_ACTION, testFactory.getState());
    assertEquals(FactoryView.ActionType.PRODUCE_WARRIOR, testFactory.getActionType());
    assertEquals(Math.E, testFactory.getWarriorDirection(), 0.0);
    assertEquals(0.0, testFactory.getProgress(), 0.0);
  }

}
