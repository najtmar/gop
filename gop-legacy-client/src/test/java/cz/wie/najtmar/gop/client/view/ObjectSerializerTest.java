package cz.wie.najtmar.gop.client.view;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import cz.wie.najtmar.gop.client.view.CityView;
import cz.wie.najtmar.gop.client.view.FactoryView;
import cz.wie.najtmar.gop.client.view.PlayerView;
import cz.wie.najtmar.gop.client.view.PrawnView;
import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.event.SerializationException;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import java.io.StringReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;

public class ObjectSerializerTest {

  static final long START_TIME = 966;
  static final String SERIALIZED_GAME_STRING =
      "{\n"
      + "  \"id\": \"00000000-0000-5ba0-0000-000000013435\", "
      + "  \"timestamp\": 1234567, "
      + "  \"board\": {\n"
      + "    \"properties\": [\n"
      + "      { "
      + "        \"key\": \"Board.xsize\", "
      + "        \"value\":\"25\" "
      + "      }, "
      + "      { "
      + "        \"key\": \"Board.ysize\", "
      + "        \"value\":\"15\" "
      + "      }, "
      + "      { "
      + "        \"key\": \"Board.stepSize\", "
      + "        \"value\":\"1.0\" "
      + "      } "
      + "    ],\n"
      + "    \"joints\": [\n"
      + "      {\n"
      + "        \"index\": 0,\n"
      + "        \"name\": \"joint0\",\n"
      + "        \"x\": 0.0,\n"
      + "        \"y\": 0.0\n"
      + "      },\n"
      + "      {\n"
      + "        \"index\": 1,\n"
      + "        \"name\": \"joint1\",\n"
      + "        \"x\": 3.0,\n"
      + "        \"y\": 0.0\n"
      + "      },\n"
      + "      {\n"
      + "        \"index\": 2,\n"
      + "        \"name\": \"joint2\",\n"
      + "        \"x\": 3.0,\n"
      + "        \"y\": 3.0\n"
      + "      }\n"
      + "    ],\n"
      + "    \"roadSections\": [\n"
      + "      {\n"
      + "        \"index\": 0,\n"
      + "        \"joint0\": 0,\n"
      + "        \"joint1\": 1\n"
      + "      },\n"
      + "      {\n"
      + "        \"index\": 1,\n"
      + "        \"joint0\": 1,\n"
      + "        \"joint1\": 2\n"
      + "      }\n"
      + "    ]\n"
      + "  },\n"
      + "  \"players\": [\n"
      + "    {\n"
      + "      \"index\": 0, "
      + "      \"user\": { "
      + "        \"name\":\"playerA\", "
      + "        \"uuid\": \"00000000-0000-3039-0000-000000010932\" "
      + "      } "
      + "    },\n"
      + "    {\n"
      + "      \"index\": 1, "
      + "      \"user\": { "
      + "        \"name\":\"playerB\", "
      + "        \"uuid\": \"00000000-0001-81cd-0000-00000000a8ca\" "
      + "      } "
      + "    }\n"
      + "  ]\n"
      + "}";

  GameView game;
  JointView joint00;
  JointView joint30;
  JointView joint33;
  RoadSectionView roadSection0;
  RoadSectionView roadSection1;
  PlayerView playerA;
  PlayerView playerB;
  ObjectSerializer objectSerializer;
  PositionView position0;
  PositionView position1;
  CityView cityA;
  CityView cityB;
  FactoryView cityBFactory1;
  PrawnView warriorA;
  PrawnView warriorB;
  PrawnView settlersUnit;
  /*
  Battle battle;
  */

  /**
   * Method setUp().
   * @throws Exception when method execution fails
   */
  @Before
  public void setUp() throws Exception {
    game = new GameView(Json.createReader(new StringReader(SERIALIZED_GAME_STRING)).readObject(), 1);
    joint00 = game.getJoints()[0];
    joint30 = game.getJoints()[1];
    joint33 = game.getJoints()[2];
    roadSection0 = game.getRoadSections()[0];
    roadSection1 = game.getRoadSections()[1];
    playerA = new PlayerView(new UserView("playerA", new UUID(12345, 67890)), 0);
    playerB = new PlayerView(new UserView("playerB", new UUID(98765, 43210)), 1);
    objectSerializer = new ObjectSerializer(game);
    position0 = roadSection0.getPositions()[0];
    position1 = roadSection0.getPositions()[1];
    cityA = new CityView("cityA", 0, joint00, playerA);
    cityB = new CityView("cityB", 1, joint30, playerB);
    cityB.grow();
    cityBFactory1 = cityB.getFactories().get(1);
    warriorA = new PrawnView(PrawnView.Type.WARRIOR, Math.PI / 2.0, playerA, 0, joint00.getNormalizedPosition());
    warriorB = new PrawnView(PrawnView.Type.WARRIOR, 3.0 * Math.PI / 2.0, playerB, 1, joint30.getNormalizedPosition());
    settlersUnit = new PrawnView(PrawnView.Type.SETTLERS_UNIT, 0.0, playerA, 2, joint33.getNormalizedPosition());
    warriorA.setItinerary((new ItineraryView.Builder(joint00.getNormalizedPosition()))
        .addNode(roadSection0.getPositions()[2])
        .addNode(roadSection0.getPositions()[1])
        .build());
    game.addPrawn(warriorA);
    game.addPrawn(warriorB);
    game.addPrawn(settlersUnit);
    game.addCity(cityA);
    game.addCity(cityB);
    /*
    battle = new Battle(warriorA, new Position(ROAD_SECTION_0, 1), new Position(ROAD_SECTION_0, 2), warriorB,
        new Position(ROAD_SECTION_0, 2), new Position(ROAD_SECTION_0, 1));
    */
  }

  @Test
  public void testPositionSerializationWorks() throws SerializationException {
    final JsonObject positionSerialized = objectSerializer.serializePosition(position1);
    assertEquals(
        Json.createReader(new StringReader(
            "{"
          + "  \"roadSection\": 0, "
          + "  \"index\": 1 "
          + "}"))
          .readObject(),
        positionSerialized);
    final PositionView positionDeserialized = objectSerializer.deserializePosition(positionSerialized);
    assertEquals(position1, positionDeserialized);
  }

  @Test
  public void testItinerarySerializationWorks() throws SerializationException {
    final JsonArray itinerarySerialized = objectSerializer.serializeItinerary(warriorA.getItinerary());
    final JsonArray nullItinerarySerialized = objectSerializer.serializeItinerary(null);
    assertEquals(
        Json.createReader(new StringReader(
            "[\n"
          + "  {\n"
          + "    \"roadSection\": 0,\n"
          + "    \"index\": 0\n"
          + "  },\n"
          + "  {\n"
          + "    \"roadSection\": 0,\n"
          + "    \"index\": 2\n"
          + "  },\n"
          + "  {\n"
          + "    \"roadSection\": 0,\n"
          + "    \"index\": 1\n"
          + "  }\n"
          + "]"))
          .readArray(),
        itinerarySerialized);
    assertTrue(nullItinerarySerialized.equals(Json.createArrayBuilder().build()));
    final ItineraryView itineraryDeserialized = objectSerializer.deserializeNewItinerary(itinerarySerialized);
    assertEquals(
        Arrays.asList(roadSection0.getPositions()[0], roadSection0.getPositions()[2], roadSection0.getPositions()[1]),
        itineraryDeserialized.getNodes());
    assertNull(objectSerializer.deserializeNewItinerary(nullItinerarySerialized));
  }

  @Test
  public void testPrawnSerializationWorks() throws SerializationException {
    final int prawnSerialized = objectSerializer.serializePrawn(warriorB);
    assertEquals(warriorB.getId(), prawnSerialized);
    final PrawnView prawnDeserialized = objectSerializer.deserializePrawn(prawnSerialized);
    assertEquals(warriorB, prawnDeserialized);
  }

  @Test
  public void testPlayerSerializationWorks() throws SerializationException {
    final int playerSerialized = objectSerializer.serializePlayer(playerB);
    assertEquals(playerB.getIndex(), playerSerialized);
    final PlayerView playerDeserialized = objectSerializer.deserializePlayer(playerSerialized);
    assertEquals(playerB, playerDeserialized);
  }

  @Test
  public void testCitySerializationWorks() throws SerializationException {
    final int citySerialized = objectSerializer.serializeCity(cityB);
    assertEquals(cityB.getId(), citySerialized);
    final CityView cityDeserialized = objectSerializer.deserializeCity(citySerialized);
    assertEquals(cityB, cityDeserialized);
  }

  @Test
  public void testFactorySerializationWorks() throws SerializationException {
    final JsonObject factorySerialized = objectSerializer.serializeFactory(cityBFactory1);
    assertEquals(
        Json.createReader(new StringReader(
            "{"
          + "  \"city\": 1, "
          + "  \"index\": 1 "
          + "}"))
          .readObject(),
        factorySerialized);
    final FactoryView factoryDeserialized = objectSerializer.deserializeFactoryOrNull(factorySerialized);
    assertEquals(cityB, factoryDeserialized.getCity());
    assertEquals(1, factoryDeserialized.getIndex());

    final JsonObject nonExistentFactorySerialized = Json.createReader(new StringReader(
          "{"
        + "  \"city\": 1, "
        + "  \"index\": 2 "
        + "}")).readObject();
    assertNull(objectSerializer.deserializeFactoryOrNull(nonExistentFactorySerialized));
  }

  @Test
  public void testMoveDeserializationWorks() throws Exception {
    assertEquals(
        new MoveView(roadSection0.getPositions()[1], 123456, true),
        objectSerializer.deserializeNewMove(Json.createReader(new StringReader(
            "{"
            + "  \"position\": {"
            + "    \"roadSection\": 0, "
            + "    \"index\": 1 "
            + "  }, "
            + "  \"moveTime\": 123456, "
            + "  \"stop\": true "
            + "}"))
            .readObject()));
  }

  @Test
  public void testExistingBattleDeerializationWorks() throws Exception {
    warriorA.setCurrentPosition(roadSection0.getPositions()[1]);
    warriorB.setCurrentPosition(roadSection0.getPositions()[2]);
    final BattleView battle = new BattleView(warriorA, warriorB, START_TIME + 1);
    game.addBattle(battle);
    assertEquals(
        battle,
        objectSerializer.deserializeExistingBattle(
            Json.createReader(new StringReader(
                "{"
              + "  \"prawn0\": " + warriorA.getId() + ", "
              + "  \"prawn1\": " + warriorB.getId() + ", "
              + "  \"normalizedPosition0\": {"
              + "    \"roadSection\": 0, "
              + "    \"index\": 1 "
              + "  }, "
              + "  \"normalizedPosition1\": {"
              + "    \"roadSection\": 0, "
              + "    \"index\": 2 "
              + "  } "
              + "}"))
              .readObject()));
    battle.finish(START_TIME + 2);
    try {
      objectSerializer.deserializeExistingBattle(
          Json.createReader(new StringReader(
              "{"
            + "  \"prawn0\": " + warriorA.getId() + ", "
            + "  \"prawn1\": " + warriorB.getId() + ", "
            + "  \"normalizedPosition0\": {"
            + "    \"roadSection\": 0, "
            + "    \"index\": 1 "
            + "  }, "
            + "  \"normalizedPosition1\": {"
            + "    \"roadSection\": 0, "
            + "    \"index\": 2 "
            + "  } "
            + "}"))
            .readObject());
      fail("Should have thrown an exception.");
    } catch (SerializationException exception) {
      assertThat(exception.getMessage(), Matchers.containsString("There must be exactly one matching Battle"));
    }
    final BattleView battle1 = new BattleView(warriorA, warriorB, START_TIME + 3);
    game.addBattle(battle1);
    assertEquals(
        battle1,
        objectSerializer.deserializeExistingBattle(
            Json.createReader(new StringReader(
                "{"
              + "  \"prawn0\": " + warriorA.getId() + ", "
              + "  \"prawn1\": " + warriorB.getId() + ", "
              + "  \"normalizedPosition0\": {"
              + "    \"roadSection\": 0, "
              + "    \"index\": 1 "
              + "  }, "
              + "  \"normalizedPosition1\": {"
              + "    \"roadSection\": 0, "
              + "    \"index\": 2 "
              + "  } "
              + "}"))
              .readObject()));
  }

  @Test
  public void testInterruptedPlayersCorrectlyRecognized() throws Exception {
    final Map<Event.Type, Event> testEvents = new HashMap<>();
    final Map<Event.Type, Set<PlayerView>> expectedResults = new HashMap<>();

    // ACTION_FINISHED
    testEvents.put(
        Event.Type.ACTION_FINISHED,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"ACTION_FINISHED\", "
          + "  \"time\": 123456, "
          + "  \"factory\": { "
          + "    \"city\": 1, "
          + "    \"index\": 1 "
          + "  } "
          + "}")).readObject()));
    expectedResults.put(Event.Type.ACTION_FINISHED, new HashSet<>(Arrays.asList(playerB)));

    // ADD_BATTLE
    testEvents.put(
        Event.Type.ADD_BATTLE,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"ADD_BATTLE\", "
          + "  \"time\": 123456, "
          + "  \"battle\": { "
          + "    \"prawn0\": 0, "
          + "    \"prawn1\": 1, "
          + "    \"normalizedPosition0\": {"
          + "      \"roadSection\": 0, "
          + "      \"index\": 1 "
          + "    }, "
          + "    \"normalizedPosition1\": {"
          + "      \"roadSection\": 0, "
          + "      \"index\": 2 "
          + "    } "
          + "  } "
          + "}")).readObject()));
    expectedResults.put(Event.Type.ADD_BATTLE, new HashSet<>(Arrays.asList(playerA, playerB)));

    // CAPTURE_OR_DESTROY_CITY
    testEvents.put(
        Event.Type.CAPTURE_OR_DESTROY_CITY,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"CAPTURE_OR_DESTROY_CITY\", "
          + "  \"time\": 123456, "
          + "  \"city\": 1, "
          + "  \"defender\": 1, "
          + "  \"attacker\": 0 "
          + "}")).readObject()));
    expectedResults.put(Event.Type.CAPTURE_OR_DESTROY_CITY, new HashSet<>(Arrays.asList(playerA, playerB)));

    // CHANGE_PLAYER_STATE
    testEvents.put(
        Event.Type.CHANGE_PLAYER_STATE,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"CHANGE_PLAYER_STATE\", "
          + "  \"time\": 123456, "
          + "  \"player\": 0, "
          + "  \"state\": \"LOST\" "
          + "}")).readObject()));
    expectedResults.put(Event.Type.CHANGE_PLAYER_STATE, new HashSet<>(Arrays.asList(playerA, playerB)));

    // CITY_APPEARS
    testEvents.put(
        Event.Type.CITY_APPEARS,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"CITY_APPEARS\", "
          + "  \"time\": 123456, "
          + "  \"city\": 7, "
          + "  \"player\": 1 "
          + "}")).readObject()));
    expectedResults.put(Event.Type.CITY_APPEARS, new HashSet<>());

    // CITY_SHOWN_DESTROYED
    testEvents.put(
        Event.Type.CITY_SHOWN_DESTROYED,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"CITY_SHOWN_DESTROYED\", "
          + "  \"time\": 123456, "
          + "  \"city\": 7, "
          + "  \"player\": 1 "
          + "}")).readObject()));
    expectedResults.put(Event.Type.CITY_SHOWN_DESTROYED, new HashSet<>());

    // CREATE_CITY
    testEvents.put(
        Event.Type.CREATE_CITY,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"CREATE_CITY\", "
          + "  \"time\": 123456, "
          + "  \"settlersUnit\": 2, "
          + "  \"name\": \"cityName\" "
          + "}")).readObject()));
    expectedResults.put(Event.Type.CREATE_CITY, new HashSet<>(Arrays.asList(playerA)));

    // DESTROY_PRAWN
    testEvents.put(
        Event.Type.DESTROY_PRAWN,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"DESTROY_PRAWN\", "
          + "  \"time\": 123456, "
          + "  \"prawn\": 1 "
          + "}")).readObject()));
    expectedResults.put(Event.Type.DESTROY_PRAWN, new HashSet<>(Arrays.asList(playerB)));

    // FINISH_GAME
    testEvents.put(
        Event.Type.FINISH_GAME,
        new Event(Json.createReader(new StringReader(
            "{\n"
          + "  \"type\": \"FINISH_GAME\",\n"
          + "  \"time\": 123456\n"
          + "}")).readObject()));
    expectedResults.put(Event.Type.FINISH_GAME, new HashSet<>(Arrays.asList(playerA, playerB)));

    // GROW_CITY
    testEvents.put(
        Event.Type.GROW_CITY,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"GROW_CITY\", "
          + "  \"time\": 123456, "
          + "  \"city\": 0 "
          + "}")).readObject()));
    expectedResults.put(Event.Type.GROW_CITY, new HashSet<>(Arrays.asList(playerA)));

    // INITIALIZE_GAME
    testEvents.put(
        Event.Type.INITIALIZE_GAME,
        new Event(Json.createReader(new StringReader(
            "{\n"
          + "  \"type\": \"INITIALIZE_GAME\",\n"
          + "  \"time\": 123456,\n"
          + "  \"game\": " + SERIALIZED_GAME_STRING
          + "}")).readObject()));
    expectedResults.put(Event.Type.INITIALIZE_GAME, new HashSet<>(Arrays.asList(playerA, playerB)));

    // MOVE_PRAWN
    testEvents.put(
        Event.Type.MOVE_PRAWN,
        new Event(Json.createReader(new StringReader(
            "{ "
            + "  \"type\": \"MOVE_PRAWN\", "
            + "  \"time\": 123456, "
            + "  \"prawn\": 1, "
            + "  \"move\": {"
            + "    \"position\": { "
            + "      \"roadSection\": 0, "
            + "      \"index\": 1 "
            + "    }, "
            + "    \"moveTime\": 123455, "
            + "    \"stop\": true "
            + "  } "
            + "}")).readObject()));
    expectedResults.put(Event.Type.MOVE_PRAWN, new HashSet<>(Arrays.asList(playerB)));

    // PERIODIC_INTERRUPT
    testEvents.put(
        Event.Type.PERIODIC_INTERRUPT,
        new Event(Json.createReader(new StringReader(
            "{ "
            + "  \"type\": \"PERIODIC_INTERRUPT\", "
            + "  \"time\": 123456 "
            + "}")).readObject()));
    expectedResults.put(Event.Type.PERIODIC_INTERRUPT, new HashSet<>(Arrays.asList(playerA, playerB)));

    // PRAWN_APPEARS
    testEvents.put(
        Event.Type.PRAWN_APPEARS,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"PRAWN_APPEARS\", "
          + "  \"time\": 123456, "
          + "  \"prawn\": 123, "
          + "  \"player\": 1 "
          + "}")).readObject()));
    expectedResults.put(Event.Type.PRAWN_APPEARS, new HashSet<>());

    // PRAWN_DISAPPEARS
    testEvents.put(
        Event.Type.PRAWN_DISAPPEARS,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"PRAWN_DISAPPEARS\", "
          + "  \"time\": 123456, "
          + "  \"prawn\": 123, "
          + "  \"player\": 1 "
          + "}")).readObject()));
    expectedResults.put(Event.Type.PRAWN_DISAPPEARS, new HashSet<>());

    // PRODUCE_SETTLERS_UNIT
    testEvents.put(
        Event.Type.PRODUCE_SETTLERS_UNIT,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"PRODUCE_SETTLERS_UNIT\", "
          + "  \"time\": 123456, "
          + "  \"id\": 7, "
          + "  \"position\": { "
          + "    \"roadSection\": 0, "
          + "    \"index\": 3 "
          + "  }, "
          + "  \"player\": 1 "
          + "}")).readObject()));
    expectedResults.put(Event.Type.PRODUCE_SETTLERS_UNIT, new HashSet<>(Arrays.asList(playerB)));

    // PRODUCE_WARRIOR
    testEvents.put(
        Event.Type.PRODUCE_WARRIOR,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"PRODUCE_WARRIOR\", "
          + "  \"time\": 123456, "
          + "  \"id\": 7, "
          + "  \"position\": { "
          + "    \"roadSection\": 0, "
          + "    \"index\": 3 "
          + "  }, "
          + "  \"direction\": 0.375, "
          + "  \"player\": 0 "
          + "}")).readObject()));
    expectedResults.put(Event.Type.PRODUCE_WARRIOR, new HashSet<>(Arrays.asList(playerA)));

    // REMOVE_BATTLE
    testEvents.put(
        Event.Type.REMOVE_BATTLE,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"REMOVE_BATTLE\", "
          + "  \"time\": 123456, "
          + "  \"battle\": { "
          + "    \"prawn0\": 0, "
          + "    \"prawn1\": 1, "
          + "    \"normalizedPosition0\": {"
          + "      \"roadSection\": 0, "
          + "      \"index\": 1 "
          + "    }, "
          + "    \"normalizedPosition1\": {"
          + "      \"roadSection\": 0, "
          + "      \"index\": 2 "
          + "    } "
          + "  } "
          + "}")).readObject()));
    expectedResults.put(Event.Type.REMOVE_BATTLE, new HashSet<>(Arrays.asList(playerA, playerB)));

    // ROAD_SECTION_HALF_APPEARS
    testEvents.put(
        Event.Type.ROAD_SECTION_HALF_APPEARS,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"ROAD_SECTION_HALF_APPEARS\", "
          + "  \"time\": 123456, "
          + "  \"roadSectionHalf\": { "
          + "    \"roadSection\": 1, "
          + "    \"end\": \"FORWARD\" "
          + "  }, "
          + "  \"player\": 1 "
          + "}")).readObject()));
    expectedResults.put(Event.Type.ROAD_SECTION_HALF_APPEARS, new HashSet<>());

    // ROAD_SECTION_HALF_DISAPPEARS
    testEvents.put(
        Event.Type.ROAD_SECTION_HALF_DISAPPEARS,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"ROAD_SECTION_HALF_DISAPPEARS\", "
          + "  \"time\": 123456, "
          + "  \"roadSectionHalf\": { "
          + "    \"roadSection\": 1, "
          + "    \"end\": \"FORWARD\" "
          + "  }, "
          + "  \"player\": 1 "
          + "}")).readObject()));
    expectedResults.put(Event.Type.ROAD_SECTION_HALF_DISAPPEARS, new HashSet<>());

    // ROAD_SECTION_HALF_DISCOVERED
    testEvents.put(
        Event.Type.ROAD_SECTION_HALF_DISCOVERED,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"ROAD_SECTION_HALF_DISCOVERED\", "
          + "  \"time\": 123456, "
          + "  \"roadSectionHalf\": { "
          + "    \"roadSection\": 1, "
          + "    \"end\": \"FORWARD\" "
          + "  }, "
          + "  \"player\": 1 "
          + "}")).readObject()));
    expectedResults.put(Event.Type.ROAD_SECTION_HALF_DISCOVERED, new HashSet<>());

    // ROAD_SECTION_HALF_DISCOVERED
    testEvents.put(
        Event.Type.ROAD_SECTION_HALF_DISCOVERED,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"ROAD_SECTION_HALF_APPEARS\", "
          + "  \"time\": 123456, "
          + "  \"roadSectionHalf\": { "
          + "    \"roadSection\": 1, "
          + "    \"end\": \"FORWARD\" "
          + "  }, "
          + "  \"player\": 1 "
          + "}")).readObject()));
    expectedResults.put(Event.Type.ROAD_SECTION_HALF_DISCOVERED, new HashSet<>());

    // SHRINK_OR_DESTROY_CITY
    testEvents.put(
        Event.Type.SHRINK_OR_DESTROY_CITY,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"SHRINK_OR_DESTROY_CITY\", "
          + "  \"time\": 123456, "
          + "  \"city\": 0, "
          + "  \"factoryIndex\": 1 "
          + "}")).readObject()));
    expectedResults.put(Event.Type.SHRINK_OR_DESTROY_CITY, new HashSet<>(Arrays.asList(playerA)));

    // ABANDON_PLAYER
    testEvents.put(
        Event.Type.ABANDON_PLAYER,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"ABANDON_PLAYER\", "
          + "  \"time\": 123456, "
          + "  \"player\": 1 "
          + "}")).readObject()));
    expectedResults.put(Event.Type.ABANDON_PLAYER, new HashSet<>(Arrays.asList()));

    // ADD_PLAYER
    testEvents.put(
        Event.Type.ADD_PLAYER,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"ADD_PLAYER\", "
          + "  \"time\": 0, "
          + "  \"player\": { "
          + "    \"name\": \"playerB\", "
          + "    \"index\": 1 "
          + "  } "
          + "}")).readObject()));
    expectedResults.put(Event.Type.ADD_PLAYER, new HashSet<>(Arrays.asList()));

    // CLEAR_PRAWN_ITINERARY
    testEvents.put(
        Event.Type.CLEAR_PRAWN_ITINERARY,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"CLEAR_PRAWN_ITINERARY\", "
          + "  \"time\": 123456, "
          + "  \"prawn\": 1 "
          + "}")).readObject()));
    expectedResults.put(Event.Type.CLEAR_PRAWN_ITINERARY, new HashSet<>(Arrays.asList()));

    // CLIENT_FAILURE
    testEvents.put(
        Event.Type.CLIENT_FAILURE,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"CLIENT_FAILURE\", "
          + "  \"time\": 123456, "
          + "  \"player\": 2, "
          + "  \"message\": \"something went wrong\" "
          + "}")).readObject()));
    expectedResults.put(Event.Type.CLIENT_FAILURE, new HashSet<>(Arrays.asList()));

    // DECREASE_PRAWN_ENERGY
    testEvents.put(
        Event.Type.DECREASE_PRAWN_ENERGY,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"DECREASE_PRAWN_ENERGY\", "
          + "  \"time\": 123456, "
          + "  \"prawn\": 0, "
          + "  \"delta\": 0.25 "
          + "}")).readObject()));
    expectedResults.put(Event.Type.DECREASE_PRAWN_ENERGY, new HashSet<>(Arrays.asList()));

    // INCREASE_PROGRESS_BY_UNIVERSAL_DELTA
    testEvents.put(
        Event.Type.INCREASE_PROGRESS_BY_UNIVERSAL_DELTA,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"INCREASE_PROGRESS_BY_UNIVERSAL_DELTA\", "
          + "  \"time\": 123456, "
          + "  \"factory\": { "
          + "    \"city\": 1, "
          + "    \"index\": 0 "
          + "  }, "
          + "  \"universalDelta\": 0.25 "
          + "}")).readObject()));
    expectedResults.put(Event.Type.INCREASE_PROGRESS_BY_UNIVERSAL_DELTA, new HashSet<>(Arrays.asList()));

    // ORDER_CITY_CREATION
    testEvents.put(
        Event.Type.ORDER_CITY_CREATION,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"ORDER_CITY_CREATION\", "
          + "  \"time\": 123456, "
          + "  \"settlersUnit\": 0, "
          + "  \"name\": \"cityName\" "
          + "}")).readObject()));
    expectedResults.put(Event.Type.ORDER_CITY_CREATION, new HashSet<>(Arrays.asList()));

    // REPAIR_PRAWN
    testEvents.put(
        Event.Type.REPAIR_PRAWN,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"REPAIR_PRAWN\", "
          + "  \"time\": 123456, "
          + "  \"prawn\": 1, "
          + "  \"delta\": 0.25 "
          + "}")).readObject()));
    expectedResults.put(Event.Type.REPAIR_PRAWN, new HashSet<>(Arrays.asList()));

    // SET_FACTORY_PROGRESS
    testEvents.put(
        Event.Type.SET_FACTORY_PROGRESS,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"SET_FACTORY_PROGRESS\", "
          + "  \"time\": 123456, "
          + "  \"factory\": { "
          + "    \"city\": 0, "
          + "    \"index\": 1 "
          + "  }, "
          + "  \"progress\": 0.25 "
          + "}")).readObject()));
    expectedResults.put(Event.Type.SET_FACTORY_PROGRESS, new HashSet<>(Arrays.asList()));

    // SET_GROW_CITY_ACTION
    testEvents.put(
        Event.Type.SET_GROW_CITY_ACTION,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"SET_GROW_CITY_ACTION\", "
          + "  \"time\": 123456, "
          + "  \"factory\": { "
          + "    \"city\": 1, "
          + "    \"index\": 1 "
          + "  } "
          + "}")).readObject()));
    expectedResults.put(Event.Type.SET_GROW_CITY_ACTION, new HashSet<>(Arrays.asList()));

    // SET_PRAWN_ITINERARY
    testEvents.put(
        Event.Type.SET_PRAWN_ITINERARY,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"SET_PRAWN_ITINERARY\", "
          + "  \"time\": 123456, "
          + "  \"prawn\": 0, "
          + "  \"nodes\": ["
          + "    { "
          + "      \"roadSection\": 0, "
          + "      \"index\": 1 "
          + "    }, "
          + "    { "
          + "      \"roadSection\": 0, "
          + "      \"index\": 2 "
          + "    } "
          + "  ] "
          + "}")).readObject()));
    expectedResults.put(Event.Type.SET_PRAWN_ITINERARY, new HashSet<>(Arrays.asList()));

    // SET_PRODUCE_SETTLERS_UNIT_ACTION
    testEvents.put(
        Event.Type.SET_PRODUCE_SETTLERS_UNIT_ACTION,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"SET_PRODUCE_SETTLERS_UNIT_ACTION\", "
          + "  \"time\": 123456, "
          + "  \"factory\": { "
          + "    \"city\": 1, "
          + "    \"index\": 0 "
          + "  } "
          + "}")).readObject()));
    expectedResults.put(Event.Type.SET_PRODUCE_SETTLERS_UNIT_ACTION, new HashSet<>(Arrays.asList()));

    // SET_PRODUCE_WARRIOR_ACTION
    testEvents.put(
        Event.Type.SET_PRODUCE_WARRIOR_ACTION,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"SET_PRODUCE_WARRIOR_ACTION\", "
          + "  \"time\": 123456, "
          + "  \"factory\": { "
          + "    \"city\": 0, "
          + "    \"index\": 0 "
          + "  }, "
          + "  \"direction\": 0.375 "
          + "}")).readObject()));
    expectedResults.put(Event.Type.SET_PRODUCE_WARRIOR_ACTION, new HashSet<>(Arrays.asList()));

    // SET_REPAIR_PRAWN_ACTION
    testEvents.put(
        Event.Type.SET_REPAIR_PRAWN_ACTION,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"SET_REPAIR_PRAWN_ACTION\", "
          + "  \"time\": 123456, "
          + "  \"factory\": { "
          + "    \"city\": 1, "
          + "    \"index\": 1 "
          + "  }, "
          + "  \"prawn\": 1 "
          + "}")).readObject()));
    expectedResults.put(Event.Type.SET_REPAIR_PRAWN_ACTION, new HashSet<>(Arrays.asList()));

    // STAY_PRAWN
    testEvents.put(
        Event.Type.STAY_PRAWN,
        new Event(Json.createReader(new StringReader(
            "{ "
          + "  \"type\": \"STAY_PRAWN\", "
          + "  \"time\": 123456, "
          + "  \"prawn\": 0, "
          + "  \"stayTime\": 123456 "
          + "}")).readObject()));
    expectedResults.put(Event.Type.STAY_PRAWN, new HashSet<>(Arrays.asList()));

    // TEST
    testEvents.put(
        Event.Type.TEST,
        new Event(Json.createReader(new StringReader(
            "{"
          + "  \"type\": \"TEST\", "
          + "  \"time\": 123456, "
          + "  \"key0\": \"value0\", "
          + "  \"key1\": 12 "
          + "}")).readObject()));
    expectedResults.put(Event.Type.TEST, new HashSet<>(Arrays.asList()));

    for (Event.Type eventType : Event.Type.values()) {
      if (!testEvents.containsKey(eventType)) {
        fail("Event Type not tested: " + eventType);
      }
      assertEquals(testEvents.get(eventType).toString(),
          expectedResults.get(eventType), objectSerializer.getInterruptedPlayers(testEvents.get(eventType)));
    }
  }

  @Test
  public void testIgnoringPlayersCorrectlyRecognized() throws Exception {
    // ACTION_FINISHED
    assertEquals(
        new HashSet<PlayerView>(Arrays.asList(playerA)),
        objectSerializer.getIgnoringPlayers(
          new Event(Json.createReader(new StringReader(
              "{ "
            + "  \"type\": \"ACTION_FINISHED\", "
            + "  \"time\": 123456, "
            + "  \"factory\": { "
            + "    \"city\": 1, "
            + "    \"index\": 1 "
            + "  } "
            + "}")).readObject())));

    // CREATE_CITY
    assertEquals(new HashSet<PlayerView>(Arrays.asList()),
        objectSerializer.getIgnoringPlayers(
          new Event(Json.createReader(new StringReader(
              "{ "
            + "  \"type\": \"CREATE_CITY\", "
            + "  \"time\": 123456, "
            + "  \"settlersUnit\": 2, "
            + "  \"name\": \"cityName\" "
            + "}")).readObject())));

    // SET_FACTORY_PROGRESS
    assertEquals(new HashSet<PlayerView>(Arrays.asList(playerB)),
        objectSerializer.getIgnoringPlayers(
          new Event(Json.createReader(new StringReader(
              "{ "
            + "  \"type\": \"SET_FACTORY_PROGRESS\", "
            + "  \"time\": 123456, "
            + "  \"factory\": { "
            + "    \"city\": 0, "
            + "    \"index\": 0 "
            + "  }, "
            + "  \"progress\": 0.25 "
            + "}")).readObject())));
  }

}
