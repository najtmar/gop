package cz.wie.najtmar.gop.server;

import com.google.inject.Inject;
import com.google.inject.name.Named;

import cz.wie.najtmar.gop.board.BoardException;
import cz.wie.najtmar.gop.board.BoardGenerator;
import cz.wie.najtmar.gop.client.view.ClientViewException;
import cz.wie.najtmar.gop.game.Game;
import cz.wie.najtmar.gop.game.GameException;
import cz.wie.najtmar.gop.game.PropertiesReader;
import cz.wie.najtmar.gop.io.Channel;

import java.io.IOException;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;

/**
 * Simple simulator of the behavior of Players that performs valid actions. Used in tests and sample runs.
 * @author najtmar
 */
public class SimpleGameSimulator implements ConnectionManager {

  /**
   * The State of the SimpleGameSimulator .
   */
  private State state;

  /**
   * The Game being simulated.
   */
  private final Game game;

  /**
   * The UiSession used to communicate with the User.
   */
  private final GoP.ConfirmationOnlySession uiSession;

  /**
   * The number of Players taking part in the Game.
   */
  private int playerCount;

  /**
   * The seed used in Game simulation.
   */
  private final long seed;

  /**
   * The SimplePlayerSimulators taking part in the Game.
   */
  private SimplePlayerSimulator[] playerSimulators;

  /**
   * The Channels used to communicate with Players.
   */
  private Channel[] playerChannels;

  public static class Factory implements ConnectionManager.Factory<SimpleGameSimulator> {

    private final PropertiesReader propertiesReader;
    private final BoardGenerator boardGenerator;
    private final GoP.ConfirmationOnlySession uiSession;
    private final int playerCount;
    private final long seed;

    @Inject
    Factory(@Nonnull PropertiesReader propertiesReader, @Nonnull BoardGenerator boardGenerator,
        @Nonnull GoP.ConfirmationOnlySession uiSession, @Named("player count") @Nonnegative int playerCount,
        @Named("random seed") long seed) {
      this.propertiesReader = propertiesReader;
      this.boardGenerator = boardGenerator;
      this.uiSession = uiSession;
      this.playerCount = playerCount;
      this.seed = seed;
    }

    @Override
    public SimpleGameSimulator create() throws ServerException {
      try {
        final Game game = new Game(propertiesReader.getGameProperties());
        game.setBoard(boardGenerator.generate());
        return new SimpleGameSimulator(game, uiSession, playerCount, seed);
      } catch (GameException | IOException | BoardException exception) {
        throw new ServerException("Failed to instantiate class SimpleGameSimulator .", exception);
      }
    }

  }

  private SimpleGameSimulator(@Nonnull Game game, @Nonnull GoP.ConfirmationOnlySession uiSession,
      @Nonnegative int playerCount, long seed) {
    this.game = game;
    this.uiSession = uiSession;
    this.playerCount = playerCount;
    this.seed = seed;
    this.state = State.NOT_INITIALIZED;
  }

  public Game getGame() {
    return game;
  }

  SimplePlayerSimulator[] getPlayerSimulators() {
    return playerSimulators;
  }

  @Override
  public Channel[] establishConnectionChannels(GameSession unusedGameSession) throws ServerException {
    if (playerSimulators == null) {
      playerSimulators = new SimplePlayerSimulator[playerCount];
      playerChannels = new Channel[playerCount];
      for (int i = 0; i < playerChannels.length; ++i) {
        try {
          if (i == 0) {
            playerSimulators[i] = new SimplePlayerSimulator(uiSession, i, "Player " + i, seed);
          } else {
            playerSimulators[i] = new SimplePlayerSimulator(null, i, "Player " + i, seed);
          }
        } catch (IOException exception) {
          throw new ServerException("SimplePlayerSimulator failed to be created.", exception);
        }
        playerChannels[i] = playerSimulators[i].getOuterChannel();
        playerSimulators[i].start();
      }
      uiSession.setSimulators(playerSimulators);
      (new Thread(new Runnable() {
        @Override
        public void run() {
          try {
            uiSession.initialize(null, null);
          } catch (ClientViewException exception) {
            throw new RuntimeException("UI Session initialization failed.", exception);
          }
        }
      })).start();
    }
    return playerChannels;
  }

  @Override
  public void initialize() {
    state = State.INITIALIZED;
  }

  @Override
  public State getState() {
    return state;
  }

}
