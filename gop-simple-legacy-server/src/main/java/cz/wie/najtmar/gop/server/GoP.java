package cz.wie.najtmar.gop.server;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Names;

import cz.wie.najtmar.gop.board.BoardGenerator;
import cz.wie.najtmar.gop.board.ClockBoardGenerator;
import cz.wie.najtmar.gop.client.ui.Client;
import cz.wie.najtmar.gop.client.ui.InputEngine;
import cz.wie.najtmar.gop.client.ui.OutputEngine;
import cz.wie.najtmar.gop.client.ui.Session;
import cz.wie.najtmar.gop.client.view.ClientViewException;
import cz.wie.najtmar.gop.client.view.GameView;
import cz.wie.najtmar.gop.client.view.PlayerView;
import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.game.PropertiesReader;
import cz.wie.najtmar.gop.game.SimplePropertiesReader;
import cz.wie.najtmar.gop.game.User;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;

import java.util.Locale;
import java.util.UUID;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.GuardedBy;

public class GoP extends Application {

  /**
   * The UiSession used to communicate with the user.
   */
  public static final ConfirmationOnlySession UI_SESSION = new ConfirmationOnlySession();

  public static final int PLAYER_COUNT = 2;

  /**
   * Whether the start() method has been finished.
   */
  @GuardedBy("this")
  private volatile boolean started;

  static class ConfirmationOnlySession implements Session {

    /**
     * The Client used to communicate with the user.
     */
    private Client client;

    /**
     * The simulators used to visualize the Game.
     */
    private SimplePlayerSimulator[] simulators;

    /**
     * The UI application.
     */
    private GoP application;

    /**
     * Whether the session is initialized or not.
     */
    @GuardedBy("this")
    private volatile boolean initialized;

    public ConfirmationOnlySession() {
    }

    public void setSimulators(SimplePlayerSimulator[] simulators) {
      this.simulators = simulators;
    }

    @Override
    public void initialize(@Nullable Client unusedClient, @Nullable GameView unusedGame) throws ClientViewException {
      if (simulators == null) {
        throw new ClientViewException("Simulators not set.");
      }
      if (simulators.length == 0) {
        throw new ClientViewException("At least 1 simulator required (found 0).");
      }
      boolean simulatorsInitialized;
      while (true) {
        simulatorsInitialized = true;
        for (SimplePlayerSimulator simulator : simulators) {
          if (simulator == null) {
            throw new ClientViewException("Simulator not initialized.");
          }
          if (!simulator.getGameProcessor().isInitialized()) {
            simulatorsInitialized = false;
            break;
          }
        }
        if (simulatorsInitialized) {
          break;
        } else {
          try {
            Thread.sleep(1);
          } catch (InterruptedException exception) {
            throw new ClientViewException("sleep() interrupted.", exception);
          }
        }
      }
      synchronized (this) {
        initialized = true;
        notify();
      }
    }

    /**
     * Returns a GameView.
     * @return a GameView
     */
    private synchronized GameView getGame() {
      while (!initialized) {
        try {
          wait();
        } catch (InterruptedException exception) {
          throw new RuntimeException("Failed while wait()ing.", exception);
        }
      }
      notify();
      try {
        return simulators[0].getGame();
      } catch (ClientViewException exception) {
        throw new RuntimeException("Error getting the Game.", exception);
      }
    }

    public void setApplication(GoP application) {
      this.application = application;
    }

    @Override
    public Event[] processEvents(Event[] inputEvents, PlayerView player) throws ClientViewException {
      synchronized (application) {
        while (!application.started) {
          try {
            application.wait();
          } catch (InterruptedException exception) {
            throw new ClientViewException("Interrupted while wait()ing.", exception);
          }
        }
        application.notify();
      }

      final Event[] userEvents;
      // TODO(najtmar): Checking whether inputEvents is empty is a workaround. A cleaner solution would be to introduce
      // a new type of Event that specifies that no Event is interrupting.
      if (inputEvents.length > 0) {
        client.snapshot(inputEvents);
        // Using this to avoid "java.lang.IllegalStateException: Not on FX application thread;" errors.
        Platform.runLater(new Runnable() {
          @Override
              public void run() {
                try {
                  client.paint();
                } catch (ClientViewException exception) {
                  throw new RuntimeException("Error while painting.");
                }

              }
        });
        userEvents = client.interact(inputEvents);
      } else {
        userEvents = new Event[]{};
      }

      return userEvents;
    }

  }

  private class GopModule extends AbstractModule {

    /**
     * The user acting as the primary Player .
     */
    private final User primaryUser = new User("User", new UUID(12345, 67890));

    /**
     * The primary Stage used in the Game.
     */
    private Stage primaryStage;

    public void setPrimaryStage(@Nonnull Stage primaryStage) {
      this.primaryStage = primaryStage;
    }

    @Override
    protected void configure() {
      bind(PropertiesReader.class).to(SimplePropertiesReader.class);
      bind(BoardGenerator.class).to(ClockBoardGenerator.class);
      bind(InitialPrawnDeployer.class).to(CrossBoardPrawnDeployer.class);
      bind(ConnectionManager.Factory.class).to(SimpleGameSimulator.Factory.class);
      bind(GoP.ConfirmationOnlySession.class).toInstance(UI_SESSION);
      bind(Integer.class).annotatedWith(Names.named("player count")).toInstance(PLAYER_COUNT);
      bind(Long.class).annotatedWith(Names.named("random seed")).toInstance(85L);
      bind(OutputEngine.class).to(JavaFxEngine.class).in(Singleton.class);
      bind(InputEngine.class).to(JavaFxEngine.class).in(Singleton.class);
      bind(Locale.class).toInstance(Locale.UK);
    }

    @Provides
    Stage provideStage() {
      return primaryStage;
    }

    @Provides
    User providePrimaryUser() {
      return primaryUser;
    }

  }

  private GopModule createGopModule() {
    return new GopModule();
  }

  /**
   * main() method.
   * @param args main() method arguments
   * @throws ServerException when something goes wrong
   */
  public static void main(String[] args) throws ServerException {
    final GoP gop = new GoP();
    final Injector injector = Guice.createInjector(gop.createGopModule());
    final GameSession gameSession = injector.getInstance(GameSession.class);
    gameSession.start();

    // Launch the graphical interface.
    launch(args);
  }

  @Override
  public void init() throws Exception {
    UI_SESSION.setApplication(this);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    final GopModule gopModule = createGopModule();
    gopModule.setPrimaryStage(primaryStage);
    final Injector injector = Guice.createInjector(gopModule);
    UI_SESSION.client = injector.getInstance(Client.class);
    UI_SESSION.client.initialize(UI_SESSION.getGame());

    primaryStage.show();
    synchronized (this) {
      started = true;
      notify();
    }
  }

}
