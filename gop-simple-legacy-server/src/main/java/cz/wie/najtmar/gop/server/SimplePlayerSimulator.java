package cz.wie.najtmar.gop.server;

import cz.wie.najtmar.gop.client.ui.Session;
import cz.wie.najtmar.gop.client.view.CityView;
import cz.wie.najtmar.gop.client.view.ClientViewException;
import cz.wie.najtmar.gop.client.view.EventFactory;
import cz.wie.najtmar.gop.client.view.FactoryView;
import cz.wie.najtmar.gop.client.view.FactoryView.ActionType;
import cz.wie.najtmar.gop.client.view.GameEventProcessor;
import cz.wie.najtmar.gop.client.view.GameView;
import cz.wie.najtmar.gop.client.view.ItineraryView;
import cz.wie.najtmar.gop.client.view.JointView;
import cz.wie.najtmar.gop.client.view.ObjectSerializer;
import cz.wie.najtmar.gop.client.view.PlayerView;
import cz.wie.najtmar.gop.client.view.PositionView;
import cz.wie.najtmar.gop.client.view.PrawnView;
import cz.wie.najtmar.gop.client.view.RoadSectionView;
import cz.wie.najtmar.gop.client.view.UserView;
import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.event.EventProcessingException;
import cz.wie.najtmar.gop.event.SerializationException;
import cz.wie.najtmar.gop.io.Channel;
import cz.wie.najtmar.gop.io.Message;

import java.io.IOException;
import java.io.StringReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Logger;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.json.Json;

// TODO(najtmar): Implement this class.
/**
 * Simple simulator of a Player taking part in a Game.
 * @author najtmar
 */
public class SimplePlayerSimulator extends Thread {

  private static final Logger LOGGER = Logger.getLogger(SimplePlayerSimulator.class.getName());

  /**
   * The Player being simulated.
   */
  private PlayerView player;

  /**
   * The UiSession used to communicate with the User. If null then no visualization is performed.
   */
  private final Session uiSession;

  /**
   * Processes the Game being simulated.
   */
  private final GameEventProcessor gameProcessor;

  /**
   * Random number generator used for Player behavior simulation.
   */
  private final Random random;

  /**
   * The Channel used by outer entities to communicate with the simulator.
   */
  private final Channel outerChannel;

  /**
   * Constructor.
   * @param uiSession the UiSession used to visualize the Game; if null then no visualization is performed
   * @param index the index of the Player being simulated
   * @param name the name of the User being simulated
   * @param seed the random seed used to simulate the Game
   * @throws IOException when communication with the client fails
   */
  @SuppressWarnings("deprecation")
  public SimplePlayerSimulator(@Nullable Session uiSession, @Nonnegative int index, @Nonnull String name, long seed)
      throws IOException {
    final UserView user = new UserView(name, new UUID(1234, index));
    this.uiSession = uiSession;
    this.random = new Random(seed);
    LinkedBlockingQueue<Message> outgoingQueue = new LinkedBlockingQueue<>();
    LinkedBlockingQueue<Message> ingoingQueue = new LinkedBlockingQueue<>();
    this.outerChannel = new Channel(ingoingQueue, outgoingQueue);
    this.gameProcessor = new GameEventProcessor(user, index, new Channel(outgoingQueue, ingoingQueue));
  }

  public Channel getOuterChannel() {
    return outerChannel;
  }

  public GameEventProcessor getGameProcessor() {
    return gameProcessor;
  }

  public GameView getGame() throws ClientViewException {
    return gameProcessor.getGame();
  }

  /**
   * Prepares a new Itinerary for prawn. Returns a corresponding Event.
   * @param prawn the Prawn for which the Itinerary should be prepared
   * @return an Event to create the Itinerary for prawn
   * @throws ClientViewException when Itinerary preparation fails
   * @throws EventProcessingException when Event creation fails
   */
  private Event prepareNewItinerary(@Nonnull PrawnView prawn) throws ClientViewException, EventProcessingException {
    ItineraryView.Builder builder = new ItineraryView.Builder(prawn.getCurrentPosition());
    boolean started = false;
    for (PositionView position = prawn.getCurrentPosition(); true; ) {
      if (started && random.nextBoolean()) {
        break;
      }
      final PositionView projectedPosition;
      if (position.getJoint() != null) {
        final JointView joint = position.getJoint();
        final RoadSectionView roadSection = joint.getRoadSections().get(random.nextInt(joint.getRoadSections().size()));
        projectedPosition = roadSection.getProjectedPosition(position);
      } else {
        projectedPosition = position;
      }
      final boolean direction = random.nextBoolean();
      final RoadSectionView roadSection = projectedPosition.getRoadSection();
      if ((direction && projectedPosition.getIndex() < roadSection.getJointAdjacentPointIndexes()[1])
          || (!direction && projectedPosition.getIndex() > roadSection.getJointAdjacentPointIndexes()[0])) {
        PositionView nextPosition;
        if (random.nextBoolean()) {
          if (direction) {
            nextPosition = roadSection.getJoints()[1].getNormalizedPosition();
          } else {
            nextPosition = roadSection.getJoints()[0].getNormalizedPosition();
          }
        } else {
          final int delta;
          if (direction) {
            delta =
                random.nextInt(roadSection.getJointAdjacentPointIndexes()[1] - projectedPosition.getIndex()) + 1;
          } else {
            delta =
                -random.nextInt(projectedPosition.getIndex() - roadSection.getJointAdjacentPointIndexes()[0]) - 1;
          }
          nextPosition = roadSection.getPositions()[projectedPosition.getIndex() + delta];
        }
        builder.addNode(nextPosition);
        position = nextPosition;
        started = true;
      }
    }
    final ItineraryView itinerary = builder.build();
    prawn.setItinerary(itinerary);
    return gameProcessor.getEventFactory().createSetPrawnItineraryEvent(prawn, itinerary);
  }

  /**
   * Orders creating a City by settlersUnit. Returns a corresponding Event.
   * @param settlersUnit the SettlersUnit creating the City
   * @return an Event to create a City by settlersUnit
   * @throws ClientViewException when City creation fails
   * @throws EventProcessingException when Event creation fails
   */
  private Event orderCityCreation(
      @Nonnull PrawnView settlersUnit) throws ClientViewException, EventProcessingException {
    if (settlersUnit.getType() != PrawnView.Type.SETTLERS_UNIT) {
      throw new ClientViewException("Only a SettlersUnit can create a City: " + settlersUnit);
    }
    final JointView joint = settlersUnit.getCurrentPosition().getJoint();
    if (joint == null || joint.getCity() != null) {
      throw new ClientViewException(
          "A City can only be created on a Joint that doesn't have a City at it: " + settlersUnit);
    }
    return gameProcessor.getEventFactory().createOrderCityCreationEvent(settlersUnit, "City " + settlersUnit.getId());
  }

  /**
   * Set the production in factory. Returns a corresponding Event.
   * @param factory the Factory whose production should be set
   * @return an Event to set production in factory
   * @throws when setting the Factory Action fails
   * @throws EventProcessingException when Event creation fails
   */
  private Event setFactoryProduction(@Nonnull FactoryView factory)
      throws ClientViewException, EventProcessingException {
    final EventFactory eventFactory = gameProcessor.getEventFactory();
    switch (random.nextInt(3)) {
      case 0:
        factory.setAction(ActionType.PRODUCE_SETTLERS_UNIT, null, -1.0);
        return eventFactory.createSetProduceSettlersUnitActionEvent(factory);
      case 1:
        final double warriorDirection = random.nextDouble() * 2.0 * Math.PI;
        factory.setAction(ActionType.PRODUCE_WARRIOR, null, warriorDirection);
        return eventFactory.createSetProduceWarriorActionEvent(factory, warriorDirection);
      case 2:
        if (factory.getCity().getFactories().size() < factory.getCity().getJoint().getRoadSections().size() - 1) {
          factory.setAction(ActionType.GROW, null, -1.0);
          return eventFactory.createSetGrowCityActionEvent(factory);
        } else {
          return setFactoryProduction(factory);
        }
      default:
        throw new RuntimeException("Unexpected value.");
    }
  }

  @SuppressWarnings("deprecation")
  @Override
  public void run() {
    final GameView game;
    try {
      gameProcessor.initialize();
      game = gameProcessor.getGame();
      player = game.getPlayers()[game.getCurrentPlayerIndex()];
    } catch (ClientViewException exception) {
      throw new RuntimeException(exception);
    }

    // Set initial Itineraries of SettlersUnits.
    try {
      if (uiSession != null) {
        gameProcessor.writeEvents(new Event[]{});
      } else {
        gameProcessor.writeEvents(
            game.getPrawnMap().values().stream().filter(prawn -> prawn.getPlayer().equals(player)).map((prawn) -> {
              try {
                return prepareNewItinerary(prawn);
              } catch (ClientViewException | EventProcessingException exception) {
                throw new RuntimeException(exception);
              }
            }).toArray(size -> new Event[size]));
      }
    } catch (ClientViewException exception) {
      throw new RuntimeException(exception);
    }
    if (uiSession != null) {
      try {
        uiSession.processEvents(new Event[]{
            // TODO(najtmar): A workaround to make the UI draw the GameView .
            new Event(Json.createReader(new StringReader(
                "{"
              + "  \"type\": \"TEST\", "
              + "  \"time\": 123456, "
              + "  \"key0\": \"value0\", "
              + "  \"key1\": 12 "
              + "}"))
            .readObject())}, player);
      } catch (ClientViewException | EventProcessingException exception) {
        throw new RuntimeException(exception);
      }
    }

    final ObjectSerializer objectSerializer = gameProcessor.getObjectSerializer();
    while (true) {
      // Process Events received.
      final Event[] events;
      try {
        events = gameProcessor.readAndProcessEvents();
      } catch (ClientViewException | SerializationException exception) {
        throw new RuntimeException(player.toString(), exception);
      }

      List<Event> eventsGenerated = new LinkedList<>();
      boolean anyEventInterrupting = false;
      final HashMap<PrawnView, Event> prawnToCreateItineraryEvent = new HashMap<>();
      for (Event event : events) {
        final boolean isEventInterrupting;
        try {
          isEventInterrupting = objectSerializer.getInterruptedPlayers(event).contains(player);
        } catch (ClientViewException | SerializationException exception) {
          throw new RuntimeException(exception);
        }
        if (isEventInterrupting) {
          if (uiSession == null) {
            try {
              switch (event.getType()) {
                // Standard interrupt Actions.
                case MOVE_PRAWN: {
                  final PrawnView prawn = objectSerializer.deserializePrawn(event.getBody().getInt("prawn"));
                  if (prawn.getState() == PrawnView.State.REMOVED) {
                    break;
                  }
                  boolean cityCreated = false;
                  if (prawn.getType() == PrawnView.Type.SETTLERS_UNIT) {
                    final JointView joint = prawn.getCurrentPosition().getJoint();
                    if (joint != null && joint.getCity() == null) {
                      // TODO(najtmar): Do the same check in the production code.
                      boolean cityPlanned = false;
                      for (PrawnView otherPrawn : prawn.getCurrentPosition().getPrawns()) {
                        if (!otherPrawn.equals(prawn) && otherPrawn.getType() == PrawnView.Type.SETTLERS_UNIT
                            && otherPrawn.getState() == PrawnView.State.EXISTING
                            && otherPrawn.getCityCreationEvent() != null) {
                          cityPlanned = true;
                          break;
                        }
                      }
                      if (!cityPlanned) {
                        if (random.nextBoolean()) {
                          final Event cityCreationEvent = orderCityCreation(prawn);
                          prawn.setCityCreationEvent(cityCreationEvent);
                          eventsGenerated.add(cityCreationEvent);
                          LOGGER.info(() -> "Ordered city creation by SettlersUnit " + prawn.getId()
                              + " owned by Player " + prawn.getPlayer().getIndex() + " on " + joint + " .");
                          cityCreated = true;
                        }
                      }
                    }
                  }
                  if (!cityCreated) {
                    eventsGenerated.add(prepareNewItinerary(prawn));
                  }
                  break;
                }
                case CREATE_CITY: {
                  final CityView city = objectSerializer.deserializeCity(event.getBody().getInt("id"));
                  if (city.getState() == CityView.State.REMOVED) {
                    break;
                  }
                  eventsGenerated.add(setFactoryProduction(city.getFactories().get(0)));
                  break;
                }
                case ACTION_FINISHED: {
                  final CityView city =
                      objectSerializer.deserializeCity(event.getBody().getJsonObject("factory").getInt("city"));
                  if (city.getState() == CityView.State.REMOVED) {
                    break;
                  }
                  final FactoryView factory =
                      objectSerializer.deserializeFactoryOrNull(event.getBody().getJsonObject("factory"));
                  LOGGER.info(() -> "Action of a Factory in City " +  city.getName() + " finished.");
                  if (factory != null) {
                    final Event newEvent = setFactoryProduction(factory);
                    LOGGER.info(() -> "New Action started: " + newEvent);
                    eventsGenerated.add(newEvent);
                  }
                  break;
                }
                case PRODUCE_SETTLERS_UNIT:
                  final PrawnView settlersUnit = objectSerializer.deserializePrawn(event.getBody().getInt("id"));
                  if (settlersUnit.getState() == PrawnView.State.REMOVED) {
                    break;
                  }
                  eventsGenerated.add(prepareNewItinerary(settlersUnit));
                  break;
                case PRODUCE_WARRIOR:
                  final PrawnView warrior = objectSerializer.deserializePrawn(event.getBody().getInt("id"));
                  if (warrior.getState() == PrawnView.State.REMOVED) {
                    break;
                  }
                  eventsGenerated.add(prepareNewItinerary(warrior));
                  break;
                case PERIODIC_INTERRUPT: {
                  for (Event currentEvent : events) {
                    if (objectSerializer.getInterruptedPlayers(currentEvent).contains(player)) {
                      if (currentEvent.getType() == Event.Type.STAY_PRAWN) {
                        final PrawnView prawn =
                            objectSerializer.deserializePrawn(currentEvent.getBody().getInt("prawn"));
                        if (prawn.getState() == PrawnView.State.EXISTING
                            && !prawnToCreateItineraryEvent.containsKey(prawn) && random.nextBoolean()) {
                          final Event newItineraryEvent = prepareNewItinerary(prawn);
                          eventsGenerated.add(newItineraryEvent);
                          prawnToCreateItineraryEvent.put(prawn, newItineraryEvent);
                        }
                      }
                    }
                  }
                  break;
                }
                case ADD_BATTLE:
                  // TODO(najtmar): Support this Event .
                  break;
                case REMOVE_BATTLE:
                  // TODO(najtmar): Support this Event .
                  break;
                // Additional interrupt Actions.
                case DESTROY_PRAWN:
                  break;
                case REPAIR_PRAWN:
                  break;
                case SHRINK_OR_DESTROY_CITY: {
                  break;
                }
                case CAPTURE_OR_DESTROY_CITY: {
                  final CityView city = objectSerializer.deserializeCity(event.getBody().getInt("city"));
                  if (city.getState() == CityView.State.REMOVED) {
                    break;
                  }
                  for (FactoryView factory : city.getFactories()) {
                    eventsGenerated.add(setFactoryProduction(factory));
                  }
                  break;
                }
                case CHANGE_PLAYER_STATE:
                  break;
                case GROW_CITY: {
                  final CityView city = objectSerializer.deserializeCity(event.getBody().getInt("city"));
                  if (city.getState() == CityView.State.REMOVED) {
                    break;
                  }
                  eventsGenerated.add(setFactoryProduction(city.getFactories().get(city.getFactories().size() - 1)));
                  break;
                }
                case FINISH_GAME:
                  // Just ignore.
                  break;
                default:
                  throw new RuntimeException("Unexpected Event Type: " + event);
              }
            } catch (ClientViewException exception) {
              throw new RuntimeException(exception);
            } catch (SerializationException exception) {
              throw new RuntimeException("Object deserialization problem.", exception);
            } catch (EventProcessingException exception) {
              throw new RuntimeException("Problem generating an Event.", exception);
            }
          }
          anyEventInterrupting = true;
        }
      }
      // TODO(najtmar): This is a workaround. A cleaner solution would be to introduce a new type of Event that
      // specifies that no Event is interrupting.
      final Event[] eventsToSend;
      if (anyEventInterrupting) {
        eventsToSend = events;
      } else {
        eventsToSend = new Event[]{};
      }
      if (uiSession != null) {
        try {
          final Event[] userEvents = uiSession.processEvents(eventsToSend, player);
          eventsGenerated = Arrays.asList(userEvents);
        } catch (ClientViewException exception) {
          throw new RuntimeException(exception);
        }
      }
      for (Event event : events) {
        if (event.getType().equals(Event.Type.FINISH_GAME)) {
          return;
        }
      }
      try {
        gameProcessor.writeEvents(eventsGenerated.toArray(new Event[]{}));
      } catch (ClientViewException exception) {
        throw new RuntimeException(exception);
      }
    }
  }

}
