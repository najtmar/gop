package cz.wie.najtmar.gop.server;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import cz.wie.najtmar.gop.board.BoardException;
import cz.wie.najtmar.gop.board.BoardProperties;
import cz.wie.najtmar.gop.client.ui.InputEngine;
import cz.wie.najtmar.gop.client.ui.InputManager;
import cz.wie.najtmar.gop.client.ui.InputManager.State;
import cz.wie.najtmar.gop.client.ui.OutputEngine;
import cz.wie.najtmar.gop.client.view.BattleView;
import cz.wie.najtmar.gop.client.view.CityView;
import cz.wie.najtmar.gop.client.view.ClientViewException;
import cz.wie.najtmar.gop.client.view.ClientViewProperties;
import cz.wie.najtmar.gop.client.view.FactoryView;
import cz.wie.najtmar.gop.client.view.GameView;
import cz.wie.najtmar.gop.client.view.JointView;
import cz.wie.najtmar.gop.client.view.PlayerView;
import cz.wie.najtmar.gop.client.view.PositionView;
import cz.wie.najtmar.gop.client.view.PrawnView;
import cz.wie.najtmar.gop.client.view.RoadSectionView;
import cz.wie.najtmar.gop.client.view.RoadSectionView.Direction;
import cz.wie.najtmar.gop.client.view.TextManager;
import cz.wie.najtmar.gop.common.Point2D;
import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.game.PropertiesReader;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.effect.BlendMode;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.SVGPath;
import javafx.scene.shape.Shape;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Transform;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NavigableMap;
import java.util.concurrent.Semaphore;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;

@Singleton
public class JavaFxEngine implements OutputEngine, InputEngine {

  public static final int PLAYER_COUNT = 2;

  /**
   * Padding used for the canvas Group .
   */
  static final double CANVAS_PADDING = 25;

  static final Paint[] PLAYER_COLORS = {
      // RadialGradient.valueOf("center 0px 0px, radius 40px, white  0%, black 100%"),
      LinearGradient.valueOf("from -17px 17px to 17px -17px, black 0% , white 50%, black 100%"),
      // RadialGradient.valueOf("center 0px 0px, radius 40px, black  0%, white 100%"),
      LinearGradient.valueOf("from -17px 17px to 17px -17px, white 0% , black 50%, white 100%"),
      Color.RED, Color.GREEN, Color.BLUE};

  static final Color[] COMPLEMENTARY_PLAYER_COLORS = {Color.BLACK, Color.WHITE, Color.GREEN, Color.RED, Color.ORANGE};

  static final Paint BATTLE_COLOR = Color.valueOf("ff5100");

  static final Paint HIGHLIGHT_COLOR = Color.color(1.0, 1.0, 1.0, 0.5);

  static final Paint BOX_COLOR = Color.color(0.1, 0.1, 0.1, 0.9);

  static final Paint TEXT_COLOR = Color.color(0.8, 0.8, 0.8);

  static final double BOARD_PRAWN_RADIUS = 15.0;

  static final double BOARD_CITY_RADIUS = 25.0;

  static final double BOARD_VISIBLE_ROAD_WIDTH = 2.0;

  static final double BOARD_DISCOVERED_ROAD_WIDTH = 1.0;

  static final double BOARD_HIGHLIGHTED_ROAD_WIDTH = 4.0;

  /**
   * Responsible for handing user interaction.
   */
  private InputManager inputManager;

  /**
   * The Player on behalf of which the Game is visualized.
   */
  private PlayerView currentPlayer;

  /**
   * The X size of the Board.
   */
  private int xsize;

  /**
   * The Y size of the Board.
   */
  private int ysize;

  /**
   * The X size of the viewport.
   */
  private int viewportWidth;

  /**
   * The Y size of the viewport.
   */
  private int viewportHeight;

  /**
   * Next round at which the client should stop.
   */
  private volatile int breakpointRound;

  /**
   * The number of the current round.
   */
  private volatile int roundCount;

  /**
   * Maps RoadSections to the UI representations of their respective Halves .
   */
  private final Map<RoadSectionView, Shape[]> roadSectionToShapes = new HashMap<>();

  /**
   * Maps Prawns to their UI representations.
   */
  private final Map<PrawnView, Node> prawnToNodes = new HashMap<>();

  /**
   * Maps Cities to their UI representations.
   */
  private final Map<CityView, Node> cityToImage = new HashMap<>();

  /**
   * Maps Battles to their UI representations.
   */
  private final Map<BattleView, Shape> battleToShape = new HashMap<>();

  /**
   * Represents a Warrior to be built.
   */
  private Polygon warriorToBeBuilt;

  /**
   * Represents the Warrior direction of warriorToBeBuilt .
   */
  private double warriorToBeBuiltDirection;

  /**
   * Controls the number of clicks of the "Ok" button.
   */
  private final Semaphore okSemaphore;

  /**
   * The primary Stage for the Game visualization.
   */
  private final Stage primaryStage;

  /**
   * User to read Properties .
   */
  private final PropertiesReader propertiesReader;

  /**
   * The Pane with controllers.
   */
  private final FlowPane controlPane;

  /**
   * The scene Group to draw in.
   */
  private final Group canvas;

  // Scrolled board.
  private final Group scrolledBoard;
  private final ScrollPane scrollPane;

  // Layers.
  private final Group backgroundLayer;
  private final Group boardLayer;
  private final Group prawnLayer;
  private final Group cityLayer;
  private final Group battleLayer;
  private final Group magnifyLayer;
  private final Group highlightLayer;
  private final Group prawnSelectionLayer;
  private final Group cityViewLayer;
  private final Group floatingHighlightLayer;
  private final Group warriorDirectionSelectionLayer;

  // Magnification parameters.
  private int magnificationMinX;
  private int magnificationMinY;
  private int magnificationWidth;
  private int magnificationHeight;
  private double magnificationTranslatedMinX;
  private double magnificationTranslatedMinY;
  private double magnificationRatio;
  private double magnifiedViewClickDistance;

  // PrawnSelection parameters.
  private int prawnSelectionMinX;
  private int prawnSelectionMinY;
  private int prawnSelectionWidth;
  private int prawnSelectionHeight;
  private int prawnSelectionBoxWidth;
  private int prawnSelectionBoxHeight;
  private int prawnBoxSide;

  // City view parameters.
  private int cityViewFactoriesMinX;
  private int cityViewFactoriesMinY;
  private int cityViewFactoryBoxWidth;
  private int cityViewFactoryBoxHeight;
  private int cityViewFactoryBoxMaxCount;
  private int cityViewFactoryItemMinX;
  private int cityViewFactoryStatusBarMinX;

  // City view PrawnSelection parameters.
  private int cityViewPrawnSelectionMinX;
  private int cityViewPrawnSelectionMinY;
  private int cityViewPrawnSelectionWidth;
  private int cityViewPrawnSelectionHeight;
  private int cityViewPrawnSelectionBoxWidth;
  private int cityViewPrawnSelectionBoxHeight;
  private int cityViewPrawnBoxSide;

  // Warrior direction selection parameters.
  private int warriorDirectionSelectionBoxMinX;
  private int warriorDirectionSelectionBoxMinY;
  private int warriorDirectionSelectionBoxSide;

  // City box.
  private int cityBoxSide;
  private int cityBoxMinX;
  private int cityBoxMinY;
  private int cityBoxCenterX;
  private int cityBoxCenterY;
  private int cityDescriptionBoxMinX;
  private int cityDescriptionBoxMinY;
  private int cityDescriptionBoxWidth;
  private int cityDescriptionBoxHeight;

  // City built box.
  private int cityCreationBoxMinX;
  private int cityCreationBoxMinY;
  private int cityCreationBoxWidth;
  private int cityCreationBoxHeight;

  // Prawn repair prompt.
  private int prawnRepairPromptMinX;
  private int prawnRepairPromptMinY;
  private int prawnRepairPromptWidth;
  private int prawnRepairPromptHeight;

  // Game status.
  private Text stateLabel;

  // Game controls.
  private TextField breakpointRoundField;
  private Button okButton;
  private Button changeItineraryButton;
  private Button clearItineraryButton;
  private Button buildCityButton;
  private Button closePrawnButton;
  private Button itineraryConfirmButton;
  private Button itineraryAbandonButton;
  private Button closeMagnificationButton;
  private Button closePrawnSelectionButton;
  private Button closeCityViewButton;
  private Button setProduceSettlersUnitButton;
  private Button setProduceWarriorButton;
  private Button setRepairPrawnButton;
  private Button setGrowCityButton;
  private Button closeCityFactoryButton;
  private Button warriorDirectionConfirmButton;
  private Button warriorDirectionAbandonButton;
  private TextField cityCreationNameField;
  private Button cityCreationConfirmButton;
  private Button cityCreationAbandonButton;
  private Button prawnRepairConfirmButton;
  private Button prawnRepairAbandonButton;

  /**
   * The bar to display the status of the data being displayed.
   */
  private final TextFlow statusBar;
  private final ScrollPane statusBarScrollPane;

  /**
   * Shows the current Game time.
   */
  private Text timeBox;

  /**
   * Shows the relevant Events since with last interrupt.
   */
  private Text relevantEventsLabel;

  /**
   * TextManager used for text provisioning.
   */
  @Inject
  private TextManager textManager;

  /**
   * Constructor.
   * @param primaryStage the main JavaFX Stage of the UI
   * @param propertiesReader used to read Properties
   * @param locale the Locale used in the Game
   */
  @Inject
  JavaFxEngine(Stage primaryStage, PropertiesReader propertiesReader, Locale locale) {
    this.okSemaphore = new Semaphore(0);

    this.primaryStage = primaryStage;
    this.propertiesReader = propertiesReader;
    Locale.setDefault(locale);
    this.controlPane = new FlowPane();

    this.scrolledBoard = new Group();
    this.scrollPane = new ScrollPane();

    this.canvas = new Group();
    this.backgroundLayer = new Group();
    this.boardLayer = new Group();
    this.prawnLayer = new Group();
    this.cityLayer = new Group();
    this.battleLayer = new Group();
    this.magnifyLayer = new Group();
    this.highlightLayer = new Group();
    this.floatingHighlightLayer = new Group();
    this.prawnSelectionLayer = new Group();
    this.cityViewLayer = new Group();
    this.warriorDirectionSelectionLayer = new Group();

    this.scrolledBoard.getChildren().addAll(backgroundLayer, boardLayer, highlightLayer, cityLayer, battleLayer,
        prawnLayer);
    this.scrollPane.setContent(this.scrolledBoard);
    // CARGO CULT: This prevents the ScrollPane from getting blurry.
    this.scrollPane.setStyle(
        "-fx-background-color: -fx-outer-border, -fx-inner-border, -fx-body-color; -fx-background-insets: 0, 1, 2; "
        + "-fx-background-radius: 5, 4, 3;");
    this.canvas.getChildren().addAll(scrollPane, magnifyLayer, prawnSelectionLayer, cityViewLayer,
        floatingHighlightLayer, warriorDirectionSelectionLayer);

    this.statusBar = new TextFlow();
    this.statusBarScrollPane = new ScrollPane();
  }

  @Override
  public void initializeOutput(GameView game) throws ClientViewException {
    currentPlayer = game.getPlayers()[game.getCurrentPlayerIndex()];

    final BoardProperties boardProperties;
    try {
      boardProperties = new BoardProperties(propertiesReader.getBoardProperties());
    } catch (IOException | BoardException exception) {
      throw new ClientViewException("BoardProperties could not be read.", exception);
    }
    xsize = boardProperties.getXsize();
    ysize = boardProperties.getYsize();

    final ClientViewProperties clientViewProperties;
    try {
      clientViewProperties = new ClientViewProperties(propertiesReader.getClientViewProperties());
    } catch (IOException exception) {
      throw new ClientViewException("BoardProperties could not be read.", exception);
    }
    viewportWidth = clientViewProperties.getMainViewWidth();
    viewportHeight = clientViewProperties.getMainViewHeight();

    primaryStage.setTitle("Game of Prawns");

    final Border border =
        new Border(new BorderStroke(Color.GRAY, BorderStrokeStyle.SOLID, new CornerRadii(0), new BorderWidths(0.0)));

    scrollPane.setPrefSize(viewportWidth/* + 2 * CANVAS_PADDING*/, viewportHeight);
    scrollPane.setPannable(true);
    scrollPane.setHbarPolicy(ScrollBarPolicy.NEVER);
    scrollPane.setVbarPolicy(ScrollBarPolicy.NEVER);
    scrollPane.setHvalue(1.0);
    scrollPane.setVvalue(1.0);

    final Rectangle boardRectangle =
        new Rectangle(-CANVAS_PADDING, -CANVAS_PADDING, xsize + 2 * CANVAS_PADDING, ysize + 2 * CANVAS_PADDING);
    boardRectangle.setFill(Color.TRANSPARENT);
    scrolledBoard.getChildren().add(boardRectangle);
    final ImageView backgroundImage = new ImageView(new Image("/images/gop-background.jpg", xsize + 2 * CANVAS_PADDING,
        ysize + 2 * CANVAS_PADDING, false, true));
    backgroundImage.setTranslateX(-CANVAS_PADDING);
    backgroundImage.setTranslateY(-CANVAS_PADDING);
    backgroundLayer.getChildren().add(backgroundImage);

    // Round changer.
    breakpointRoundField = new TextField();
    breakpointRoundField.setAlignment(Pos.BASELINE_RIGHT);
    okButton = new Button("OK");
    okButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        try {
          breakpointRound = Integer.parseInt(breakpointRoundField.getText());
        } catch (NumberFormatException exception) {
          breakpointRoundField.setText("0");
        }
        okSemaphore.release();
      }
    });

    // Prawn control.
    changeItineraryButton = new Button("Change itinerary");
    changeItineraryButton.setOnAction(new EventHandler<ActionEvent>() {
      @SuppressWarnings("deprecation")
      @Override
      public void handle(ActionEvent event) {
        inputManager.legacyOnPrawnItineraryBuildStart();
      }
    });
    clearItineraryButton = new Button("Clear itinerary");
    clearItineraryButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onPrawnItineraryCleared();
      }
    });
    buildCityButton = new Button("Build city");
    buildCityButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onCityCreationStarted();
      }
    });
    closePrawnButton = new Button("Close");
    closePrawnButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onPrawnViewClosed();
      }
    });
    itineraryConfirmButton = new Button("Confirm itinerary");
    itineraryConfirmButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onPrawnItineraryBuildConfirmed();
      }
    });
    itineraryAbandonButton = new Button("Abandon itinerary");
    itineraryAbandonButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onPrawnItineraryBuildAbandoned();
      }
    });
    closeMagnificationButton = new Button("Close");
    closeMagnificationButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onMagnificationOff();
      }
    });
    closePrawnSelectionButton = new Button("Close");
    closePrawnSelectionButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onPrawnSelectionOff();
      }
    });
    closeCityViewButton = new Button("Close");
    closeCityViewButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onCityViewOff();
      }
    });
    setProduceSettlersUnitButton = new Button("Produce settlers");
    setProduceSettlersUnitButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onSetProduceSettlersUnit();
      }
    });
    setProduceWarriorButton = new Button("Produce warrior");
    setProduceWarriorButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onWarriorDirectionSelectionShown();
      }
    });
    setRepairPrawnButton = new Button("Repair prawn");
    setRepairPrawnButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onPrawnRepairSelectionShown();
      }
    });
    setGrowCityButton = new Button("Grow city");
    setGrowCityButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onSetGrowCity();
      }
    });
    closeCityFactoryButton = new Button("Close");
    closeCityFactoryButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onCityFactoryViewOff();
      }
    });
    warriorDirectionConfirmButton = new Button("Confirm warrior");
    warriorDirectionConfirmButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onSetProduceWarrior(warriorToBeBuiltDirection);
      }
    });
    warriorDirectionAbandonButton = new Button("Abandon warrior");
    warriorDirectionAbandonButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onWarriorDirectionSelectionOff();
      }
    });
    cityCreationNameField = new TextField();
    cityCreationNameField.setPrefColumnCount(10);
    cityCreationNameField.setAlignment(Pos.CENTER_LEFT);
    cityCreationConfirmButton = new Button("Create city");
    cityCreationConfirmButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onCityCreationConfirmed(cityCreationNameField.getText());
      }
    });
    cityCreationAbandonButton = new Button("Cancel");
    cityCreationAbandonButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onCityCreationAbandoned();
      }
    });
    prawnRepairConfirmButton = new Button("Confirm");
    prawnRepairConfirmButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onSetRepairPrawn();
      }
    });
    prawnRepairAbandonButton = new Button("Cancel");
    prawnRepairAbandonButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        inputManager.onPrawnRepairSelectionOff();
      }
    });

    stateLabel = new Text();
    stateLabel.setFont(new Font("Arial Bold", 30));
    stateLabel.setFill(Color.WHITE);

    timeBox = new Text();
    timeBox.setFont(new Font("Arial Italic", 15));
    timeBox.setFill(Color.WHITE);

    relevantEventsLabel = new Text();
    relevantEventsLabel.setFont(new Font("Arial", 15));
    relevantEventsLabel.setFill(Color.WHITE);

    statusBar.setPrefSize(viewportWidth/* + 2 * CANVAS_PADDING*/, 100);
    statusBar.setBackground(new Background(new BackgroundFill(BOX_COLOR, new CornerRadii(0.0),
        new Insets(0, 0, 0, 0))));
    statusBar.setPadding(new Insets(10.0, 10.0, 10.0, 10.0));
    statusBar.getChildren().addAll(stateLabel, timeBox, relevantEventsLabel);

    statusBarScrollPane.setPannable(true);
    statusBarScrollPane.setHbarPolicy(ScrollBarPolicy.NEVER);
    statusBarScrollPane.setVbarPolicy(ScrollBarPolicy.NEVER);
    statusBarScrollPane.setContent(statusBar);

    controlPane.setPrefSize(viewportWidth/* + 2 * CANVAS_PADDING*/, 60);
    controlPane.setBackground(new Background(new BackgroundFill(BOX_COLOR, new CornerRadii(0.0),
        new Insets(0, 0, 0, 0))));
    controlPane.setPadding(new Insets(10, 10, 10, 10));
    controlPane.setOrientation(Orientation.HORIZONTAL);
    controlPane.setLayoutY(viewportHeight);
    controlPane.setAlignment(Pos.BASELINE_LEFT);
    controlPane.setBorder(border);
    controlPane.getChildren().addAll(breakpointRoundField, okButton);

    final Separator separator = new Separator();
    separator.setPrefWidth(statusBar.getPrefWidth());

    final FlowPane primaryPane = new FlowPane();
    primaryPane.setOrientation(Orientation.HORIZONTAL);
    primaryPane.setAlignment(Pos.TOP_LEFT);
    primaryPane.getChildren().add(statusBarScrollPane);
    primaryPane.getChildren().add(separator);
    primaryPane.getChildren().add(canvas);
    primaryPane.getChildren().add(controlPane);
    primaryPane.setPrefHeight(statusBar.getPrefHeight() + scrollPane.getPrefHeight() + controlPane.getPrefHeight());

    Scene rootScene = new Scene(primaryPane);

    primaryStage.setScene(rootScene);

    rootScene.widthProperty().addListener(new ChangeListener<Number>() {
      @Override public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneWidth,
          Number newSceneWidth) {
        scrollPane.setMinWidth(newSceneWidth.intValue());
        scrollPane.setPrefWidth(newSceneWidth.intValue());
      }
    });
    rootScene.heightProperty().addListener(new ChangeListener<Number>() {
      @Override public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneHeight,
          Number newSceneHeight) {
        scrollPane.setMinHeight(newSceneHeight.intValue() - statusBar.getPrefHeight() - controlPane.getPrefHeight());
        scrollPane.setPrefHeight(newSceneHeight.intValue() - statusBar.getPrefHeight() - controlPane.getPrefHeight());
      }
    });
    primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
      @Override
      public void handle(WindowEvent event) {
        System.exit(0);
      }
    });
  }

  @Override
  public void initializeInput(@Nonnull GameView unusedGame, @Nonnull InputManager inputManager)
      throws ClientViewException {
    this.inputManager = inputManager;


    final ClientViewProperties clientViewProperties;
    try {
      clientViewProperties = new ClientViewProperties(propertiesReader.getClientViewProperties());
    } catch (IOException exception) {
      throw new ClientViewException("Reading ClientViewProperties failed.", exception);
    }
    magnificationRatio = clientViewProperties.getMagnificationRatio();
    magnificationWidth = clientViewProperties.getMagnifiedViewWidth();
    magnificationHeight = clientViewProperties.getMagnifiedViewHeight();
    magnificationMinX = (viewportWidth - magnificationWidth) / 2;
    magnificationMinY = (viewportHeight - magnificationHeight) / 2;
    magnifiedViewClickDistance = clientViewProperties.getDistinctivePositionRadius() * magnificationRatio;

    prawnBoxSide = clientViewProperties.getPrawnBoxSide();
    prawnSelectionWidth = clientViewProperties.getPrawnSelectionWidth();
    prawnSelectionHeight = clientViewProperties.getPrawnSelectionHeight();
    prawnSelectionBoxWidth = prawnSelectionWidth * prawnBoxSide;
    prawnSelectionBoxHeight = prawnSelectionHeight * prawnBoxSide;
    prawnSelectionMinX = (viewportWidth - prawnSelectionBoxWidth) / 2;
    prawnSelectionMinY = (viewportHeight - prawnSelectionBoxHeight) / 2;

    cityBoxSide = clientViewProperties.getCityBoxSide();
    cityBoxMinX = viewportWidth - (viewportWidth - clientViewProperties.getCityFactoryBoxWidth()) / 2 - cityBoxSide;
    cityBoxMinY = 100;
    cityBoxCenterX = cityBoxMinX + cityBoxSide / 2;
    cityBoxCenterY = cityBoxMinY + cityBoxSide / 2;

    cityDescriptionBoxMinX = (viewportWidth - clientViewProperties.getCityFactoryBoxWidth()) / 2;
    cityDescriptionBoxMinY = cityBoxMinY;
    cityDescriptionBoxWidth = clientViewProperties.getCityFactoryBoxWidth() - cityBoxSide;
    cityDescriptionBoxHeight = cityBoxSide;

    cityViewPrawnBoxSide = clientViewProperties.getCityPrawnBoxSide();
    cityViewPrawnSelectionWidth = clientViewProperties.getCityPrawnSelectionWidth();
    cityViewPrawnSelectionHeight = clientViewProperties.getCityPrawnSelectionHeight();
    cityViewPrawnSelectionBoxWidth = cityViewPrawnSelectionWidth * cityViewPrawnBoxSide;
    cityViewPrawnSelectionBoxHeight = cityViewPrawnSelectionHeight * cityViewPrawnBoxSide;
    cityViewPrawnSelectionMinX = (viewportWidth - cityViewPrawnSelectionBoxWidth) / 2;
    cityViewPrawnSelectionMinY = cityBoxMinY + cityBoxSide;

    cityViewFactoryBoxWidth = clientViewProperties.getCityFactoryBoxWidth();
    cityViewFactoryBoxHeight = clientViewProperties.getCityFactoryBoxHeight();
    cityViewFactoriesMinX = (viewportWidth - cityViewFactoryBoxWidth) / 2;
    cityViewFactoriesMinY = cityViewPrawnSelectionMinY + 4 + cityViewPrawnSelectionBoxHeight;
    cityViewFactoryBoxMaxCount = clientViewProperties.getCityFactoryBoxMaxCount();
    cityViewFactoryItemMinX = cityViewFactoriesMinX + 2 * cityViewFactoryBoxHeight;
    cityViewFactoryStatusBarMinX = cityViewFactoryItemMinX + cityViewFactoryBoxHeight;

    warriorDirectionSelectionBoxSide = clientViewProperties.getWarriorDirectionSelectionBoxSide();
    warriorDirectionSelectionBoxMinX = (viewportWidth - warriorDirectionSelectionBoxSide) / 2;
    warriorDirectionSelectionBoxMinY = (viewportHeight - warriorDirectionSelectionBoxSide) / 2;

    cityCreationBoxWidth = clientViewProperties.getCityCreationBoxWidth();
    cityCreationBoxHeight = clientViewProperties.getCityCreationBoxHeight();
    cityCreationBoxMinX = (viewportWidth - cityCreationBoxWidth) / 2;
    cityCreationBoxMinY = (viewportHeight - cityCreationBoxHeight) / 2;

    prawnRepairPromptWidth = clientViewProperties.getPrawnRepairPromptWidth();
    prawnRepairPromptHeight = clientViewProperties.getPrawnRepairPromptHeight();
    prawnRepairPromptMinX = (viewportWidth - prawnRepairPromptWidth) / 2;
    prawnRepairPromptMinY = cityViewPrawnSelectionMinY - prawnRepairPromptHeight - 10;
  }

  @Override
  public void onTimeUpdated(long time) {
    final int[] cumulativeMonths = {
        0, // Non-existent month 0.
        31,
        31 + 28,
        31 + 28 + 31,
        31 + 28 + 31 + 30,
        31 + 28 + 31 + 30 + 31,
        31 + 28 + 31 + 30 + 31 + 30,
        31 + 28 + 31 + 30 + 31 + 30 + 31,
        31 + 28 + 31 + 30 + 31 + 30 + 31 + 31,
        31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30,
        31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31,
        31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30,
        31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30 + 31,
    };
    final long year = time / 365 + 1;
    final long yearDay = time % 365;
    int month;
    for (month = 1; cumulativeMonths[month] <= yearDay; ++month) {}
    final long dayOfMonth = yearDay - cumulativeMonths[month - 1] + 1;
    final LocalDate date = LocalDate.of((int) year, month, (int) dayOfMonth);
    final String timeLabelText = date.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG))
        .replaceFirst("0(\\d\\d\\d)", "$1");
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        timeBox.setText(timeLabelText + "\n");
        statusBar.autosize();
      }
    });
  }

  @Override
  public void onPeriodicInfoUpdated(NavigableMap<String, Integer> relevantEvents) throws ClientViewException {
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        if (!relevantEvents.isEmpty()) {
          final StringBuffer relevantEventsStringBuffer =
              new StringBuffer(textManager.getText("Relevant events:"));
          for (NavigableMap.Entry<String, Integer> entry : relevantEvents.entrySet()) {
            relevantEventsStringBuffer.append("\n ");
            if (entry.getValue() > 1) {
              relevantEventsStringBuffer.append("" + entry.getValue() + " x ");
            }
            relevantEventsStringBuffer.append(textManager.getText(entry.getKey()));
          }
          relevantEventsLabel.setText(relevantEventsStringBuffer.toString());
          statusBar.autosize();
        }
      }
    });
  }

  /**
   * Creates a view of a RoadSection.Half if it hasn't existed yet, and returns it.
   * @param roadSection the RoadSection
   * @param end the end of the RoadSection.Half
   * @return a view of the RoadSection.Half
   */
  private Line initializeRoadSectionHalfView(RoadSectionView roadSection, Direction end) {
    roadSectionToShapes.putIfAbsent(roadSection, new Shape[2]);
    final int index;
    if (end == RoadSectionView.Direction.BACKWARD) {
      index = 0;
    } else {
      index = 1;
    }
    Line line = (Line) roadSectionToShapes.get(roadSection)[index];
    if (line == null) {
      final JointView[] joints = roadSection.getJoints();
      final PositionView midpoint = roadSection.getPositions()[roadSection.getMidpointIndex()];
      final Point2D.Double startPoint;
      final Point2D.Double endPoint;
      if (end == RoadSectionView.Direction.BACKWARD) {
        startPoint = joints[0].getCoordinatesPair();
        endPoint = midpoint.getCoordinatesPair();
      } else {
        startPoint = midpoint.getCoordinatesPair();
        endPoint = joints[1].getCoordinatesPair();
      }
      line = new Line(startPoint.getX(), startPoint.getY(), endPoint.getX(), endPoint.getY());
      line.setStroke(Color.BLACK);
      line.setSmooth(true);
      roadSectionToShapes.get(roadSection)[index] = line;
      boardLayer.getChildren().add(line);
    }
    return line;
  }

  @Override
  public void onRoadSectionHalfVisible(RoadSectionView roadSection, Direction end) {
    final Line view = initializeRoadSectionHalfView(roadSection, end);
    view.setStrokeWidth(BOARD_VISIBLE_ROAD_WIDTH);
    view.getStrokeDashArray().clear();
    view.setVisible(true);
  }

  @Override
  public void onRoadSectionHalfDiscovered(RoadSectionView roadSection, Direction end) {
    final Shape view = initializeRoadSectionHalfView(roadSection, end);
    view.setStrokeWidth(BOARD_DISCOVERED_ROAD_WIDTH);
    view.getStrokeDashArray().setAll(1.0, 2.0);
    view.setVisible(true);
  }

  @Override
  public void onRoadSectionHalfInvisible(RoadSectionView roadSection, Direction end) {
    final Shape view = initializeRoadSectionHalfView(roadSection, end);
    view.setVisible(false);
  }

  /**
   * Creates a Polygon representing a Warrior shape.
   * @return a Polygon representing a Warrior shape
   */
  private Polygon createWarriorShape() {
    final Polygon warrior = new Polygon(
        -5.0, 0.0,
        -Math.sqrt(2) / 2 * 5, Math.sqrt(2) / 2 * 5,
        0.0, 5.0,
        7.5, 0.0,
        0.0, -5.0,
        -Math.sqrt(2) / 2 * 5, -Math.sqrt(2) / 2 * 5);
    return warrior;
  }

  /**
   * Creates a Node of prawn to be visualized, with a given size.
   * @param prawn the Prawn to be visualized
   * @param size the size of the Prawn
   * @return a visualization of prawn with the given size
   */
  private Node createPrawnShape(@Nonnull PrawnView prawn, @Nonnegative double size) {
    final Group result = new Group();

    // Shape.
    final Shape shape;
    if (prawn.getType() == PrawnView.Type.WARRIOR) {
      shape = createWarriorShape();
      shape.getTransforms().add(new Rotate(prawn.getWarriorDirection() * 180 / Math.PI, 0.0, 0.0));
      shape.setScaleX(size / 5);
      shape.setScaleY(size / 5);
    } else {
      shape = new Circle(size);
    }
    shape.setFill(PLAYER_COLORS[prawn.getPlayer().getIndex()]);
    shape.setStroke(Color.BLACK);
    shape.setStrokeWidth(0.1);
    result.getChildren().add(shape);

    // Energy label for own Prawns.
    if (prawn.getPlayer().equals(currentPlayer)) {
      Label energyLabel = new Label(Integer.toString((int) Math.ceil(10 * prawn.getEnergy())));
      energyLabel.setFont(new Font(size));
      energyLabel.setTextFill(COMPLEMENTARY_PLAYER_COLORS[prawn.getPlayer().getIndex()]);
      energyLabel.setTranslateX(-size / 3 * 2);
      energyLabel.setTranslateY(-size / 3 * 2);
      result.getChildren().add(energyLabel);
    }

    return result;
  }

  /**
   * Gets and updates the cached representation of prawn .
   * @param prawn the Prawn whose representation should be returned
   * @return a cached representation of prawn or null if no representation is cached
   */
  private Node getCachedPrawn(@Nonnull PrawnView prawn) {
    final Node prawnView = prawnToNodes.get(prawn);
    if (prawnView == null) {
      return null;
    }
    if (!(prawnView instanceof Group)) {
      throw new IllegalArgumentException("prawnView should be of type Group: " + prawnView);
    }
    final Group group = (Group) prawnView;
    for (Node node : group.getChildren()) {
      if (node instanceof Label) {
        final Label energyLabel = (Label) node;
        energyLabel.setText(Integer.toString((int) Math.ceil(10 * prawn.getEnergy())));
      }
    }
    return prawnView;
  }

  /**
   * Creates a view of a Prawn if it hasn't existed yet, and returns it.
   * @param prawn the Prawn
   * @return a view of the Prawn
   */
  private Node initializePrawnView(PrawnView prawn) {
    Node node = getCachedPrawn(prawn);
    if (node == null) {
      node = createPrawnShape(prawn, BOARD_PRAWN_RADIUS);
      prawnToNodes.put(prawn, node);
      prawnLayer.getChildren().add(node);
    }
    return node;
  }

  @Override
  public void onPrawnVisible(PrawnView prawn) {
    final Node view = initializePrawnView(prawn);
    final Point2D.Double coordinates = prawn.getCurrentPosition().getCoordinatesPair();
    view.setTranslateX(coordinates.getX());
    view.setTranslateY(coordinates.getY());
    view.setVisible(true);
  }

  @Override
  public void onPrawnInvisible(PrawnView prawn) {
    final Node view = initializePrawnView(prawn);
    view.setVisible(false);
  }

  /**
   * Given a Prawn view, marks it as if it was just removed.
   * @param prawnView the view of the Prawn to be marked
   */
  private void markPrawnShapeJustRemoved(@Nonnull Node prawnView) {
    if (!(prawnView instanceof Group)) {
      throw new IllegalArgumentException("prawnView should be of type Group: " + prawnView);
    }
    final Group group = (Group) prawnView;
    boolean marked = false;
    for (Node node : group.getChildren()) {
      if (node instanceof Shape) {
        final Shape shape = (Shape) node;
        shape.setStroke(Color.RED);
        shape.setStrokeWidth(2.0);
        marked = true;
      }
    }
    if (!marked) {
      throw new IllegalArgumentException("prawnView should contain a Prawn Shape: " + prawnView);
    }
  }

  @Override
  public void onPrawnJustRemoved(PrawnView prawn) {
    final Node view = initializePrawnView(prawn);
    markPrawnShapeJustRemoved(view);
    view.setVisible(true);
  }

  /**
   * Creates a Node of city to be visualized, with a given size.
   * @param city the City to be visualized
   * @param size the size of the City
   * @return a visualization of city with the given size
   */
  private Node createCityShape(@Nonnull CityView city, @Nonnegative double size) {
    final Group result = new Group();

    // Shape.
    final Shape shape = new Circle(size);
    shape.setFill(PLAYER_COLORS[city.getPlayer().getIndex()]);
    shape.setStroke(Color.DARKGRAY);
    shape.setStrokeWidth(0.1);
    result.getChildren().add(shape);

    // Size label for own Prawns.
    Label sizeLabel = new Label();
    sizeLabel.setAlignment(Pos.CENTER);
    sizeLabel.setPrefWidth(2 * size);
    sizeLabel.setPrefHeight(2 * size);
    sizeLabel.setFont(new Font(size));
    sizeLabel.setTextFill(COMPLEMENTARY_PLAYER_COLORS[city.getPlayer().getIndex()]);
    if (city.getPlayer().equals(currentPlayer)) {
      sizeLabel.setText(Integer.toString(city.getFactories().size()));
    } else {
      sizeLabel.setText("");
    }
    sizeLabel.setTranslateX(-size);
    sizeLabel.setTranslateY(-size);
    result.getChildren().add(sizeLabel);

    return result;
  }

  /**
   * Gets and updates the cached representation of city .
   * @param city the City whose representation should be returned
   * @return a cached representation of city or null if no representation is cached
   */
  private Node getCachedCity(@Nonnull CityView city) {
    final Node cityView = cityToImage.get(city);
    if (cityView == null) {
      return null;
    }
    if (!(cityView instanceof Group)) {
      throw new IllegalArgumentException("cityView should be of type Group: " + cityView);
    }
    final Group group = (Group) cityView;
    boolean circleFound = false;
    for (Node node : group.getChildren()) {
      if (node instanceof Label) {
        final Label sizeLabel = (Label) node;
        if (city.getPlayer().equals(currentPlayer)) {
          sizeLabel.setText(Integer.toString(city.getFactories().size()));
        } else {
          sizeLabel.setText("");
        }
      }
      if (node instanceof Circle) {
        final Circle circle = (Circle) node;
        circle.setFill(PLAYER_COLORS[city.getPlayer().getIndex()]);
        circleFound = true;
      }
    }
    if (!circleFound) {
      throw new IllegalArgumentException("circleView should contain a City Circle: " + cityView);
    }
    return cityView;
  }


  /**
   * Creates a view of a City if it hasn't existed yet, and returns it.
   * @param city the City
   * @return a view of the City
   */
  private Node initializeCityView(CityView city) {
    final Point2D.Double coordinates = city.getJoint().getCoordinatesPair();
    Node cityView = getCachedCity(city);
    if (cityView == null) {
      cityView = createCityShape(city, BOARD_CITY_RADIUS);
      cityToImage.put(city, cityView);
      cityLayer.getChildren().add(cityView);
    }
    cityView.setTranslateX(coordinates.getX());
    cityView.setTranslateY(coordinates.getY());
    return cityView;
  }

  @Override
  public void onCityDiscovered(CityView city) {
    final Node view = initializeCityView(city);
    view.setVisible(true);
  }

  @Override
  public void onCityInvisible(CityView city) {
    final Node view = initializeCityView(city);
    view.setVisible(false);
  }

  @Override
  public void onCityDestroyed(CityView city) {
    final Node view = initializeCityView(city);
    view.setVisible(false);
  }

  /**
   * Creates a view of a Battle if it hasn't existed yet, and returns it.
   * @param battle the Battle
   * @return a view of the Battle
   */
  private Shape initializeBattleView(BattleView battle) {
    final Point2D.Double coordinates = battle.getPoint();
    Shape shape = battleToShape.get(battle);
    if (!battleToShape.containsKey(battle)) {
      SVGPath svgPath = new SVGPath();
      svgPath.setContent("M 2.9999999,4.3622046 0.03893056,2.6927056 -3.011745,4.1922174 "
          + "-2.3389778,0.86016953 -4.7078089,-1.5778207 -1.3309466,-1.9676405 0.25571084,-4.9739132 "
          + "1.6699594,-1.8827872 5.0193987,-1.3027757 2.51659,0.99746511 Z");
      shape = svgPath;
      shape.setScaleX(3.0);
      shape.setScaleY(3.0);
      shape.setTranslateX(coordinates.getX());
      shape.setTranslateY(coordinates.getY());
      battleToShape.put(battle, shape);
      shape.setFill(BATTLE_COLOR);
      shape.setStroke(Color.DARKRED);
      shape.setStrokeWidth(0.1);
      battleLayer.getChildren().add(shape);
    }
    return shape;
  }

  @Override
  public void onBattleVisible(BattleView battle) {
    final Shape view = initializeBattleView(battle);
    view.setVisible(true);
  }

  @Override
  public void onBattleInvisible(BattleView battle) {
    final Shape view = initializeBattleView(battle);
    view.setVisible(false);
  }

  /**
   * Translate coordinates to the magnified view.
   * @param coordinates the coordinates to translate
   * @return coordinates translated to the magnified view
   */
  private Point2D.Double translateToMagnifiedCoordinates(@Nonnull Point2D.Double coordinates) {
    final double translatedX =
        (coordinates.getX() + CANVAS_PADDING) * magnificationRatio - magnificationTranslatedMinX + magnificationMinX;
    final double translatedY =
        (coordinates.getY() + CANVAS_PADDING) * magnificationRatio - magnificationTranslatedMinY + magnificationMinY;
    return new Point2D.Double(translatedX, translatedY);
  }

  /**
   * Translates coordinates in the magnified view to actual coordinates.
   * @param coordinates the coordinates to translate
   * @return coordinates translated to the actual view, or null if the coordinates could not be translated
   */
  private Point2D.Double translateFromMagnifiedCoordinates(@Nonnull Point2D.Double magnifiedCoordinates) {
    if (!inMagnificationBox(magnifiedCoordinates)) {
      return null;
    }
    final double pointX =
        (magnifiedCoordinates.getX() - magnificationMinX + magnificationTranslatedMinX) / magnificationRatio
            - CANVAS_PADDING;
    final double pointY =
        (magnifiedCoordinates.getY() - magnificationMinY + magnificationTranslatedMinY) / magnificationRatio
            - CANVAS_PADDING;
    return new Point2D.Double(pointX, pointY);
  }

  /**
   * Checks whether a magnified object with the given center coordinates fits in the magnification box.
   * @param coordinates the coordinates of the center of the object to be checked
   * @return true iff the object fits in the magnification box
   */
  private boolean inMagnificationBox(@Nonnull Point2D.Double coordinates) {
    if (coordinates.getX() + magnifiedViewClickDistance <= magnificationMinX) {
      return false;
    }
    if (coordinates.getX() - magnifiedViewClickDistance > magnificationMinX + magnificationWidth) {
      return false;
    }
    if (coordinates.getY() + magnifiedViewClickDistance <= magnificationMinY) {
      return false;
    }
    if (coordinates.getY() - magnifiedViewClickDistance > magnificationMinY + magnificationHeight) {
      return false;
    }
    return true;
  }

  /**
   * In the magnified view, draw a circle by clicking in which a DistinctivePosition will be selected.
   * @param translatedX the x-position on screen where the DistinctivePosition is located
   * @param translatedY the y-position on screen where the DistinctivePosition is located
   */
  private void drawMagnifiedDistinctivePositionClickCircle(int translatedX, int translatedY) {
    Circle circle = new Circle(magnifiedViewClickDistance);
    circle.setTranslateX(translatedX);
    circle.setTranslateY(translatedY);
    circle.setStroke(Color.BLACK);
    circle.setFill(Color.TRANSPARENT);
    circle.setStrokeWidth(BOARD_HIGHLIGHTED_ROAD_WIDTH);
    magnifyLayer.getChildren().add(circle);
  }

  @Override
  public void onMagnifiedCityDrawn(CityView city, Point2D.Double point) {
    final Point2D.Double translatedCoordinates = translateToMagnifiedCoordinates(point);
    if (inMagnificationBox(translatedCoordinates)) {
      final int translatedX = (int) translatedCoordinates.getX();
      final int translatedY = (int) translatedCoordinates.getY();
      final Node shape = createCityShape(city, magnifiedViewClickDistance);
      shape.setTranslateX(translatedX);
      shape.setTranslateY(translatedY);
      magnifyLayer.getChildren().add(shape);
      drawMagnifiedDistinctivePositionClickCircle(translatedX, translatedY);
    }
  }

  @Override
  public void onMagnifiedPrawnDrawn(PrawnView prawn, Point2D.Double point, int index) {
    final Point2D.Double translatedCoordinates = translateToMagnifiedCoordinates(point);
    if (inMagnificationBox(translatedCoordinates)) {
      final int translatedX = (int) translatedCoordinates.getX() + 2 * index;
      final int translatedY = (int) translatedCoordinates.getY() + 2 * index;

      final Node node = createPrawnShape(prawn, magnifiedViewClickDistance);
      node.setTranslateX(translatedX);
      node.setTranslateY(translatedY);
      magnifyLayer.getChildren().add(node);
      if (index == 0) {
        drawMagnifiedDistinctivePositionClickCircle(translatedX, translatedY);
      }
    }
  }

  @Override
  public void onMagnifiedPositionDrawn(PositionView position, Point2D.Double point) {
    final Point2D.Double translatedCoordinates = translateToMagnifiedCoordinates(point);
    if (inMagnificationBox(translatedCoordinates)) {
      final int translatedX = (int) translatedCoordinates.getX();
      final int translatedY = (int) translatedCoordinates.getY();
      drawMagnifiedDistinctivePositionClickCircle(translatedX, translatedY);
    }
  }

  @Override
  public void onStateChange(State oldState) throws ClientViewException {
    final State newState = inputManager.getState();
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        final String stateLabelText;
        if (!newState.equals(State.PRAWN)) {
          stateLabelText = textManager.getText(newState.name());
        } else {
          stateLabelText = textManager.getText(inputManager.getPrawnSelected().getType().name());
        }
        stateLabel.setText(stateLabelText + "\n");
        statusBarScrollPane.setVvalue(0.0);
      }
    });
    switch (oldState) {
      case INITIALIZED: {
        // Do nothing.
        break;
      }
      case BOARD: {
        scrolledBoard.setOnMouseClicked(null);
        break;
      }
      case MAGNIFIED: {
        magnifyLayer.setOnMouseClicked(null);
        break;
      }
      case PRAWN: {
        scrolledBoard.setOnMouseClicked(null);
        break;
      }
      case PRAWN_SELECTION: {
        prawnSelectionLayer.setOnMouseClicked(null);
        break;
      }
      case ITINERARY_BUILT: {
        scrolledBoard.setOnMouseClicked(null);
        break;
      }
      case CITY: {
        cityViewLayer.setOnMouseClicked(null);
        break;
      }
      case CITY_CREATION: {
        // Do nothing.
        break;
      }
      case FACTORY: {
        cityViewLayer.setOnMouseClicked(null);
        break;
      }
      case WARRIOR_DIRECTION: {
        warriorDirectionSelectionLayer.setOnMouseClicked(null);
        break;
      }
      case PRAWN_REPAIR: {
        cityViewLayer.setOnMouseClicked(null);
        break;
      }
      default:
        throw new ClientViewException("Unsupported State: " + newState);
    }

    switch (newState) {
      case INITIALIZED: {
        // Do nothing.
        break;
      }
      case BOARD: {
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            controlPane.getChildren().setAll(breakpointRoundField, okButton);
          }
        });

        // Set event handlers.
        scrolledBoard.setOnMouseClicked(new EventHandler<MouseEvent>() {
          @Override
          public void handle(MouseEvent event) {
            inputManager.onBoardPointSelected(new Point2D.Double(event.getX(), event.getY()));
          }
        });

        if (oldState == InputManager.State.INITIALIZED) {
          // Wait for the user.
          try {
            if (breakpointRound <= roundCount) {
              okSemaphore.acquire();
            }
          } catch (InterruptedException exception) {
            throw new ClientViewException("Acquiring okSemaphore failed.", exception);
          }
          ++roundCount;
        }
        break;
      }
      case MAGNIFIED: {
        @SuppressWarnings("unused")
        final Point2D.Double point = inputManager.getPointSelected();
        magnifyLayer.setOnMouseClicked(new EventHandler<MouseEvent>() {
          @Override
          public void handle(MouseEvent event) {
            final Point2D.Double pointSelected =
                translateFromMagnifiedCoordinates(new Point2D.Double(event.getX(), event.getY()));
            if (pointSelected != null) {
              inputManager.onMagnifiedPointSelected(pointSelected);
            }
          }
        });
        controlPane.getChildren().setAll(closeMagnificationButton);
        // TODO(najtmar): Add Event handling.
        break;
      }
      case PRAWN: {
        if (oldState.equals(InputManager.State.CITY_CREATION)) {
          cityViewLayer.getChildren().clear();
        }
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            controlPane.getChildren().setAll(changeItineraryButton, clearItineraryButton, buildCityButton,
                closePrawnButton);
          }
        });
        inputManager.onPrawnSelected();
        break;
      }
      case PRAWN_SELECTION: {
        prawnSelectionLayer.setOnMouseClicked(new EventHandler<MouseEvent>() {
          @Override
          public void handle(MouseEvent event) {
            final Point2D.Double pointSelected =
                new Point2D.Double(event.getX() - prawnSelectionMinX, event.getY() - prawnSelectionMinY);
            if (pointSelected.getX() < prawnSelectionBoxWidth && pointSelected.getY() < prawnSelectionMinY) {
              inputManager.onPrawnSelectionPointSelected(pointSelected);
            }
          }
        });
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            controlPane.getChildren().setAll(closePrawnSelectionButton);
          }
        });
        break;
      }
      case ITINERARY_BUILT: {
        scrolledBoard.setOnMouseClicked(new EventHandler<MouseEvent>() {
          @Override
          public void handle(MouseEvent event) {
            inputManager.onPrawnItineraryBuildProgress(new Point2D.Double(event.getX(), event.getY()));
          }
        });
        controlPane.getChildren().setAll(itineraryConfirmButton, itineraryAbandonButton);
        if (oldState == InputManager.State.MAGNIFIED) {
          inputManager.onPrawnItineraryBuildResumed();
        }
        break;
      }
      case CITY: {
        cityViewLayer.setOnMouseClicked(new EventHandler<MouseEvent>() {
          @Override
          public void handle(MouseEvent event) {
            Point2D.Double pointSelected = new Point2D.Double(event.getX() - cityViewPrawnSelectionMinX,
                event.getY() - cityViewPrawnSelectionMinY);
            if (0 <= pointSelected.getX() && pointSelected.getX() < cityViewPrawnSelectionBoxWidth
                && 0 <= pointSelected.getY() && pointSelected.getY() < cityViewPrawnSelectionBoxHeight) {
              inputManager.onCityPrawnSelectionPointSelected(pointSelected);
            } else {
              pointSelected =
                  new Point2D.Double(event.getX() - cityViewFactoriesMinX, event.getY() - cityViewFactoriesMinY);
              if (0 <= pointSelected.getX() && pointSelected.getX() < cityViewFactoryBoxWidth
                  && 0 <= pointSelected.getY()
                  && pointSelected.getY() < cityViewFactoryBoxHeight * cityViewFactoryBoxMaxCount) {
                inputManager.onCityFactoryPointSelected(pointSelected);
              }
            }
          }
        });
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            controlPane.getChildren().setAll(closeCityViewButton);
          }
        });
        break;
      }
      case CITY_CREATION: {
        final Rectangle cityCreationBox = new Rectangle(cityCreationBoxMinX, cityCreationBoxMinY, cityCreationBoxWidth,
            cityCreationBoxHeight);
        cityCreationBox.setFill(BOX_COLOR);
        cityCreationNameField.setTranslateX(cityCreationBoxMinX + 20);
        cityCreationNameField.setTranslateY(cityCreationBoxMinY + 40);
        final Label cityCreationNameLabel = new Label(textManager.getText("Enter city name:"));
        cityCreationNameLabel.setFont(new Font(cityViewFactoryBoxHeight / 3));
        cityCreationNameLabel.setTextFill(TEXT_COLOR);
        cityCreationNameLabel.setTranslateX(cityCreationBoxMinX + 20);
        cityCreationNameLabel.setTranslateY(cityCreationBoxMinY + 40 - cityViewFactoryBoxHeight / 3 - 5);
        cityViewLayer.getChildren().addAll(cityCreationBox, cityCreationNameLabel, cityCreationNameField);
        controlPane.getChildren().setAll(cityCreationConfirmButton, cityCreationAbandonButton);
        cityCreationNameField.requestFocus();
        break;
      }
      case FACTORY: {
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            controlPane.getChildren().setAll(setProduceWarriorButton, setProduceSettlersUnitButton,
                setRepairPrawnButton, setGrowCityButton, closeCityFactoryButton);
          }
        });
        break;
      }
      case WARRIOR_DIRECTION: {
        warriorDirectionSelectionLayer.setOnMouseClicked(new EventHandler<MouseEvent>() {
          @Override
          public void handle(MouseEvent event) {
            if (warriorToBeBuilt != null) {
              final Point2D.Double pointSelected = new Point2D.Double(event.getX() - warriorDirectionSelectionBoxMinX,
                  event.getY() - warriorDirectionSelectionBoxMinY);
              final Point2D.Double warriorSelectionCenter =
                  new Point2D.Double(warriorDirectionSelectionBoxSide / 2, warriorDirectionSelectionBoxSide / 2);
              if (warriorSelectionCenter.distance(pointSelected) < warriorDirectionSelectionBoxSide / 2
                  && warriorSelectionCenter.distance(pointSelected) > 2) {
                final double vectorAtan2 = Math.atan2(pointSelected.getY() - warriorSelectionCenter.getY(),
                    pointSelected.getX() - warriorSelectionCenter.getX());
                if (vectorAtan2 >= 0.0) {
                  warriorToBeBuiltDirection = vectorAtan2;
                } else {
                  warriorToBeBuiltDirection = vectorAtan2 + 2.0 * Math.PI;
                }
                warriorToBeBuilt.getTransforms().setAll(
                    new Rotate(warriorToBeBuiltDirection * 180 / Math.PI, 0.0, 0.0));
              }
            }
          }
        });
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            controlPane.getChildren().setAll(warriorDirectionConfirmButton, warriorDirectionAbandonButton);
          }
        });
        break;
      }
      case PRAWN_REPAIR: {
        cityViewLayer.setOnMouseClicked(new EventHandler<MouseEvent>() {
          @Override
          public void handle(MouseEvent event) {
            Point2D.Double pointSelected = new Point2D.Double(event.getX() - cityViewPrawnSelectionMinX,
                event.getY() - cityViewPrawnSelectionMinY);
            if (0 <= pointSelected.getX() && pointSelected.getX() < cityViewPrawnSelectionBoxWidth
                && 0 <= pointSelected.getY() && pointSelected.getY() < cityViewPrawnSelectionBoxHeight) {
              inputManager.onPrawnRepairSelectionPointSelected(pointSelected);
            }
          }
        });
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            controlPane.getChildren().setAll(prawnRepairConfirmButton, prawnRepairAbandonButton);
          }
        });
        break;
      }
      default:
        throw new ClientViewException("Unsupported State: " + newState);
    }
  }

  @Override
  public void onCityCreationHighlighted(PositionView position, String cityName) {
    final Point2D.Double point = position.getCoordinatesPair();
    final Circle circle = new Circle(point.getX(), point.getY(), BOARD_CITY_RADIUS);
    circle.setStrokeWidth(BOARD_HIGHLIGHTED_ROAD_WIDTH);
    circle.setStroke(HIGHLIGHT_COLOR);
    circle.setFill(Color.TRANSPARENT);
    circle.setBlendMode(BlendMode.DIFFERENCE);
    circle.setEffect(new GaussianBlur());
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        highlightLayer.getChildren().add(circle);
      }
    });
  }

  @Override
  public void onSelectableLineSectionHighlighted(PositionView beginning, PositionView end) {
    final Point2D.Double beginningPoint = beginning.getCoordinatesPair();
    final Point2D.Double endPoint = end.getCoordinatesPair();
    final Line line =
        new Line(beginningPoint.getX(), beginningPoint.getY(), endPoint.getX(), endPoint.getY());
    line.setStrokeWidth(BOARD_VISIBLE_ROAD_WIDTH);
    line.setStroke(HIGHLIGHT_COLOR);
    line.getStrokeDashArray().setAll(1.0, 2.0);
    line.setStrokeLineCap(StrokeLineCap.ROUND);
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        highlightLayer.getChildren().add(line);
      }
    });
  }

  @Override
  public void onLineSectionHighlighted(PositionView beginning, PositionView end) {
    final Point2D.Double beginningPoint = beginning.getCoordinatesPair();
    final Point2D.Double endPoint = end.getCoordinatesPair();
    final Line line =
        new Line(beginningPoint.getX(), beginningPoint.getY(), endPoint.getX(), endPoint.getY());
    line.setStrokeWidth(BOARD_HIGHLIGHTED_ROAD_WIDTH);
    line.setStroke(HIGHLIGHT_COLOR);
    line.setStrokeLineCap(StrokeLineCap.ROUND);
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        highlightLayer.getChildren().add(line);
      }
    });
  }

  @Override
  public void onHighlightingRemoved() {
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        highlightLayer.getChildren().clear();
        floatingHighlightLayer.getChildren().clear();
      }
    });
  }

  @Override
  public void onMagnificationShown(@Nonnull Point2D.Double point) {
    WritableImage wi = new WritableImage(magnificationWidth, magnificationHeight);
    SnapshotParameters parameters = new SnapshotParameters();
    parameters.setFill(Color.WHITE);
    magnificationTranslatedMinX = magnificationRatio * (point.getX() + CANVAS_PADDING) - magnificationWidth / 2;
    magnificationTranslatedMinY = magnificationRatio * (point.getY() + CANVAS_PADDING) - magnificationHeight / 2;
    parameters.setViewport(new Rectangle2D(
        magnificationTranslatedMinX, magnificationTranslatedMinY,
        magnificationWidth * magnificationRatio, magnificationHeight * magnificationRatio));

    parameters.setTransform(Transform.scale(magnificationRatio, magnificationRatio));
    prawnLayer.setVisible(false);
    cityLayer.setVisible(false);
    battleLayer.setVisible(false);
    magnifyLayer.setVisible(false);
    scrolledBoard.snapshot(parameters, wi);
    prawnLayer.setVisible(true);
    cityLayer.setVisible(true);
    battleLayer.setVisible(true);
    magnifyLayer.setVisible(true);
    final ImageView wiView = new ImageView(wi);
    wiView.setTranslateX(magnificationMinX);
    wiView.setTranslateY(magnificationMinY);
    magnifyLayer.getChildren().add(wiView);
  }

  @Override
  public void onMagnificationHidden() {
    magnifyLayer.getChildren().clear();
  }

  @Override
  public void onPrawnSelectionShown() {
    final List<Rectangle> prawnSelectionBoxes = new LinkedList<>();
    for (int j = 0; j < prawnSelectionHeight; ++j) {
      for (int i = 0; i < prawnSelectionWidth; ++i) {
        final Rectangle prawnSelectionBox = new Rectangle(prawnSelectionMinX + i * prawnBoxSide,
            prawnSelectionMinY + j * prawnBoxSide, prawnBoxSide - 2, prawnBoxSide - 2);
        prawnSelectionBox.setFill(BOX_COLOR);
        prawnSelectionBoxes.add(prawnSelectionBox);
      }
    }
    prawnSelectionLayer.getChildren().setAll(prawnSelectionBoxes);
  }

  @Override
  public void onPrawnSelectionHidden() {
    prawnSelectionLayer.getChildren().clear();
  }

  @Override
  public void onPrawnSelectionPrawnDrawn(PrawnView prawn, Point2D.Double point) {
    final int translatedX = (int) point.getX() + prawnSelectionMinX;
    final int translatedY = (int) point.getY() + prawnSelectionMinY;

    final Node node = createPrawnShape(prawn, prawnBoxSide / 2.0 / 2.0);
    node.setTranslateX(translatedX);
    node.setTranslateY(translatedY);
    prawnSelectionLayer.getChildren().add(node);
  }

  @Override
  public void onPrawnSelectionLeftSlider(Point2D.Double point, int previousPrawnsCount) {
    // TODO Auto-generated method stub
  }

  @Override
  public void onPrawnSelectionRightSlider(Point2D.Double point, int nextPrawnsCount) {
    // TODO Auto-generated method stub
  }

  @Override
  public void onCityViewShown() {
    cityViewLayer.getChildren().clear();
  }

  @Override
  public void onCityViewHidden() {
    cityViewLayer.getChildren().clear();
  }

  @Override
  public void onCityFactoryShown(FactoryView factory, int positionIndex) {
    // Box.
    final Rectangle cityFactoryViewBox = new Rectangle(cityViewFactoriesMinX,
        cityViewFactoriesMinY + positionIndex * cityViewFactoryBoxHeight, cityViewFactoryBoxWidth,
        cityViewFactoryBoxHeight - 2);
    cityFactoryViewBox.setFill(BOX_COLOR);
    cityViewLayer.getChildren().add(cityFactoryViewBox);

    if (factory.getState() == FactoryView.State.IN_ACTION) {
      final int labelX = cityViewFactoriesMinX + 10;
      final int labelY = cityViewFactoriesMinY + positionIndex * cityViewFactoryBoxHeight
          + cityViewFactoryBoxHeight - cityViewFactoryBoxHeight / 2 / 5 * 4;
      final int iconX = cityViewFactoryItemMinX + cityViewFactoryBoxHeight / 2;
      final int iconY =
          cityViewFactoriesMinY + positionIndex * cityViewFactoryBoxHeight
          + cityViewFactoryBoxHeight / 2;

      // Icon and label.
      final Text label;
      switch (factory.getActionType()) {
        case PRODUCE_WARRIOR: {
          label = new Text(textManager.getText("BUILD"));
          final Polygon warrior = createWarriorShape();
          try {
            warrior.getTransforms().add(new Rotate(factory.getWarriorDirection() * 180 / Math.PI, 0.0, 0.0));
          } catch (ClientViewException exception) {
            throw new RuntimeException(exception);
          }
          warrior.setScaleX(cityViewFactoryBoxHeight / 2.0 / 2.0 / 5.0);
          warrior.setScaleY(cityViewFactoryBoxHeight / 2.0 / 2.0 / 5.0);
          warrior.setFill(PLAYER_COLORS[factory.getCity().getPlayer().getIndex()]);
          warrior.setStroke(Color.BLACK);
          warrior.setStrokeWidth(0.1);
          warrior.setTranslateX(iconX);
          warrior.setTranslateY(iconY);
          cityViewLayer.getChildren().add(warrior);
          break;
        }
        case PRODUCE_SETTLERS_UNIT: {
          label = new Text(textManager.getText("BUILD"));
          Circle shape = new Circle(cityViewFactoryBoxHeight / 2.0 / 2.0);
          shape.setFill(PLAYER_COLORS[factory.getCity().getPlayer().getIndex()]);
          shape.setStroke(Color.BLACK);
          shape.setStrokeWidth(0.1);
          shape.setTranslateX(iconX);
          shape.setTranslateY(iconY);
          cityViewLayer.getChildren().add(shape);
          break;
        }
        case GROW: {
          // TODO(najtmar): Support this case.
          label = new Text(textManager.getText("GROW"));
          break;
        }
        case REPAIR_PRAWN: {
          label = new Text(textManager.getText("REPAIR"));
          final PrawnView prawnRepaired;
          try {
            prawnRepaired = factory.getPrawnRepaired();
          } catch (ClientViewException exception) {
            throw new RuntimeException("Should never happen.", exception);
          }
          if (prawnRepaired.getType().equals(PrawnView.Type.WARRIOR)) {
            final Polygon warrior = createWarriorShape();
            warrior.getTransforms().add(new Rotate(prawnRepaired.getWarriorDirection() * 180 / Math.PI, 0.0, 0.0));
            warrior.setScaleX(cityViewFactoryBoxHeight / 2.0 / 2.0 / 5.0);
            warrior.setScaleY(cityViewFactoryBoxHeight / 2.0 / 2.0 / 5.0);
            warrior.setFill(PLAYER_COLORS[factory.getCity().getPlayer().getIndex()]);
            warrior.setStroke(Color.BLACK);
            warrior.setStrokeWidth(0.1);
            warrior.setTranslateX(iconX);
            warrior.setTranslateY(iconY);
            cityViewLayer.getChildren().add(warrior);
          } else {
            Circle shape = new Circle(cityViewFactoryBoxHeight / 2.0 / 2.0);
            shape.setFill(PLAYER_COLORS[factory.getCity().getPlayer().getIndex()]);
            shape.setStroke(Color.BLACK);
            shape.setStrokeWidth(0.1);
            shape.setTranslateX(iconX);
            shape.setTranslateY(iconY);
            cityViewLayer.getChildren().add(shape);
          }
          break;
        }
        default: {
          throw new RuntimeException("Unknown ActionType: " + factory.getActionType());
        }
      }
      label.setFont(new Font(cityViewFactoryBoxHeight / 3));
      label.setFill(TEXT_COLOR);
      label.setTranslateX(labelX);
      label.setTranslateY(labelY);
      cityViewLayer.getChildren().add(label);
    }

    // Progress bar.
    if (factory.getState().equals(FactoryView.State.IN_ACTION)) {
      final int progressBarMinX = cityViewFactoryStatusBarMinX + 3;
      final int progressBarY = cityViewFactoriesMinY + positionIndex * cityViewFactoryBoxHeight
          + cityViewFactoryBoxHeight / 2;
      final int progressBarWidth = cityViewFactoriesMinX + cityViewFactoryBoxWidth - progressBarMinX - 6;
      final double factoryProgress;
      try {
        factoryProgress = factory.getProgress();
      } catch (ClientViewException exception) {
        throw new RuntimeException(exception);
      }
      final Line progressElapsed = new Line(progressBarMinX, progressBarY,
          progressBarMinX + progressBarWidth * factoryProgress, progressBarY);
      progressElapsed.setStrokeWidth(4.0);
      progressElapsed.setStroke(BATTLE_COLOR);
      final Line progressRemaining = new Line(progressBarMinX + progressBarWidth * factoryProgress, progressBarY,
          progressBarMinX + progressBarWidth, progressBarY);
      progressRemaining.setStrokeWidth(4.0);
      progressRemaining.setStroke(Color.BLACK);
      if (progressElapsed.getEndX() - progressElapsed.getStartX() > 0) {
        cityViewLayer.getChildren().add(progressElapsed);
      }
      if (progressRemaining.getEndX() - progressRemaining.getStartX() > 0) {
        cityViewLayer.getChildren().add(progressRemaining);
      }
    }
  }

  @Override
  public void onCityFactoryUpperSliderShown(int positionIndex) {
    ///
    System.err.println("Upper slider at position " + positionIndex);
    //
    // TODO Auto-generated method stub
  }

  @Override
  public void onCityFactoryLowerSliderShown(int positionIndex) {
    ///
    System.err.println("Lower slider at position " + positionIndex);
    //
    // TODO Auto-generated method stub
  }

  @Override
  public void onCityFactoryHighlighted(int positionIndex) {
    final Polyline polyline = new Polyline(
        cityViewFactoriesMinX, cityViewFactoriesMinY + positionIndex * cityViewFactoryBoxHeight,
        cityViewFactoriesMinX + cityViewFactoryBoxWidth,
          cityViewFactoriesMinY + positionIndex * cityViewFactoryBoxHeight,
        cityViewFactoriesMinX + cityViewFactoryBoxWidth,
          cityViewFactoriesMinY + (positionIndex + 1) * cityViewFactoryBoxHeight,
        cityViewFactoriesMinX, cityViewFactoriesMinY + (positionIndex + 1) * cityViewFactoryBoxHeight,
        cityViewFactoriesMinX, cityViewFactoriesMinY + positionIndex * cityViewFactoryBoxHeight);
    polyline.setStrokeWidth(BOARD_HIGHLIGHTED_ROAD_WIDTH);
    polyline.setStroke(HIGHLIGHT_COLOR);
    polyline.setBlendMode(BlendMode.DIFFERENCE);
    polyline.setEffect(new GaussianBlur());
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        floatingHighlightLayer.getChildren().add(polyline);
      }
    });
  }

  @Override
  public void onCityPrawnSelectionShown() {
    final List<Rectangle> prawnSelectionBoxes = new LinkedList<>();
    for (int j = 0; j < cityViewPrawnSelectionHeight; ++j) {
      for (int i = 0; i < cityViewPrawnSelectionWidth; ++i) {
        final Rectangle prawnSelectionBox = new Rectangle(cityViewPrawnSelectionMinX + i * cityViewPrawnBoxSide,
            cityViewPrawnSelectionMinY + j * cityViewPrawnBoxSide, cityViewPrawnBoxSide - 2, cityViewPrawnBoxSide - 2);
        prawnSelectionBox.setFill(BOX_COLOR);
        prawnSelectionBoxes.add(prawnSelectionBox);
      }
    }
    cityViewLayer.getChildren().addAll(prawnSelectionBoxes);
  }

  @Override
  public void onCityPrawnSelectionPrawnDrawn(PrawnView prawn, Point2D.Double point) {
    final int translatedX = (int) point.getX() + cityViewPrawnSelectionMinX;
    final int translatedY = (int) point.getY() + cityViewPrawnSelectionMinY;

    final Node shape = createPrawnShape(prawn, cityViewPrawnBoxSide / 2.0 / 2.0);
    shape.setTranslateX(translatedX);
    shape.setTranslateY(translatedY);
    cityViewLayer.getChildren().add(shape);
  }

  @Override
  public void onCityPrawnSelectionLeftSlider(Point2D.Double point, int previousPrawnsCount) {
    // TODO Auto-generated method stub
  }

  @Override
  public void onCityPrawnSelectionRightSlider(Point2D.Double point, int nextPrawnsCount) {
    // TODO Auto-generated method stub
  }

  @Override
  public void onWarriorDirectionSelectionShown() {
    final Rectangle selectionBox = new Rectangle(warriorDirectionSelectionBoxMinX, warriorDirectionSelectionBoxMinY,
         warriorDirectionSelectionBoxSide, warriorDirectionSelectionBoxSide);
    selectionBox.setFill(BOX_COLOR);
    final Circle selectionCircle = new Circle(warriorDirectionSelectionBoxMinX + warriorDirectionSelectionBoxSide / 2,
        warriorDirectionSelectionBoxMinY + warriorDirectionSelectionBoxSide / 2, warriorDirectionSelectionBoxSide / 2);
    selectionCircle.setStroke(Color.color(0.9, 0.9, 0.9));
    selectionCircle.setStrokeWidth(0.7);
    selectionCircle.setFill(Color.TRANSPARENT);

    warriorToBeBuiltDirection = 0.0;
    warriorToBeBuilt = createWarriorShape();
    warriorToBeBuilt.getTransforms().add(new Rotate(warriorToBeBuiltDirection, 0.0, 0.0));
    warriorToBeBuilt.setScaleX(warriorDirectionSelectionBoxSide / 4.0 / 2.0 / 5.0);
    warriorToBeBuilt.setScaleY(warriorDirectionSelectionBoxSide / 4.0 / 2.0 / 5.0);
    warriorToBeBuilt.setFill(PLAYER_COLORS[inputManager.getCitySelected().getPlayer().getIndex()]);
    warriorToBeBuilt.setStroke(Color.BLACK);
    warriorToBeBuilt.setStrokeWidth(0.1);
    warriorToBeBuilt.setTranslateX(warriorDirectionSelectionBoxMinX + warriorDirectionSelectionBoxSide / 2);
    warriorToBeBuilt.setTranslateY(warriorDirectionSelectionBoxMinY + warriorDirectionSelectionBoxSide / 2);

    warriorDirectionSelectionLayer.getChildren().addAll(selectionBox, selectionCircle, warriorToBeBuilt);
  }

  @Override
  public void onWarriorDirectionSelectionHidden() {
    warriorToBeBuilt = null;
    warriorDirectionSelectionLayer.getChildren().clear();
  }

  @Override
  public void onGameAbandoningPromptShown() {
    // TODO(najtmar): Implement this.
  }

  @Override
  public void onGameAbandoningPromptHidden() {
    // TODO(najtmar): Implement this.
  }

  @Override
  public void onGameAbandoned() {
    // TODO(najtmar): Implement this.
  }

  @Override
  public void onGameForceQuit() {
    // NOTE: Not implemented.
  }

  @Override
  public void onPrawnRepairSelectionShown() {
    final Rectangle prawnRepairPromptBox = new Rectangle(prawnRepairPromptMinX, prawnRepairPromptMinY,
        prawnRepairPromptWidth, prawnRepairPromptHeight);
    cityViewLayer.getChildren().addAll(prawnRepairPromptBox);
  }

  @Override
  public void onPrawnRepairSelectionHighlighted(int indexX, int indexY) {
    final Rectangle prawnSelectionHighlight = new Rectangle(prawnSelectionMinX + indexX * prawnBoxSide,
        prawnSelectionMinY + indexY * prawnBoxSide, prawnBoxSide - 2, prawnBoxSide - 2);
    prawnSelectionHighlight.setStrokeWidth(BOARD_HIGHLIGHTED_ROAD_WIDTH);
    prawnSelectionHighlight.setStroke(HIGHLIGHT_COLOR);
    prawnSelectionHighlight.setFill(Color.TRANSPARENT);
    prawnSelectionHighlight.setBlendMode(BlendMode.DIFFERENCE);
    prawnSelectionHighlight.setEffect(new GaussianBlur());
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        floatingHighlightLayer.getChildren().add(prawnSelectionHighlight);
      }
    });
  }

  @Override
  public void onPrawnRepairSelectionHidden() {
    final int nodeCount = cityViewLayer.getChildren().size();
    cityViewLayer.getChildren().remove(nodeCount - 1);
  }

  @Override
  public void onCityBoxDrawn(CityView city, int[] capturedHalves, double productivity) {
    final Rectangle cityDescriptionBox =
        new Rectangle(cityDescriptionBoxMinX, cityDescriptionBoxMinY, cityDescriptionBoxWidth - 2,
            cityDescriptionBoxHeight - 2);
    cityDescriptionBox.setFill(BOX_COLOR);
    cityViewLayer.getChildren().add(cityDescriptionBox);

    final Rectangle cityBox = new Rectangle(cityBoxMinX, cityBoxMinY, cityBoxSide - 2, cityBoxSide - 2);
    cityBox.setFill(BOX_COLOR);
    cityViewLayer.getChildren().add(cityBox);

    final double cityRadius = cityBoxSide / 2.0 / 2.0 / 2;
    final double lineLength = cityBoxSide / 2.0 - cityRadius - 10.0;
    for (int i = 0; i < city.getRoadSectionAngles().length; ++i) {
      final double roadSectionAngle = city.getRoadSectionAngles()[i];
      final double cos = Math.cos(roadSectionAngle);
      final double sin = Math.sin(roadSectionAngle);
      final Line line0 = new Line(cityBoxCenterX + cos * cityRadius, cityBoxCenterY + sin * cityRadius,
          cityBoxCenterX + cos * (cityRadius + lineLength / 2), cityBoxCenterY + sin * (cityRadius + lineLength / 2));
      final Line line1 = new Line(cityBoxCenterX + cos * (cityRadius + lineLength / 2),
          cityBoxCenterY + sin * (cityRadius + lineLength / 2), cityBoxCenterX + cos * (cityRadius + lineLength),
          cityBoxCenterY + sin * (cityRadius + lineLength));
      switch (capturedHalves[i]) {
        case 0:
          line0.setStrokeWidth(6.0);
          line1.setStrokeWidth(6.0);
          break;
        case 1:
          line0.setStrokeWidth(6.0);
          line1.setStrokeWidth(1.0);
          break;
        case 2:
          line0.setStrokeWidth(1.0);
          line1.setStrokeWidth(1.0);
          break;
        default:
          line0.setStrokeWidth(0.0);
          line1.setStrokeWidth(0.0);
          break;
      }
      cityViewLayer.getChildren().add(line0);
      cityViewLayer.getChildren().add(line1);
    }

    final Circle circle = new Circle(cityRadius);
    circle.setFill(PLAYER_COLORS[city.getPlayer().getIndex()]);
    circle.setStroke(Color.BLACK);
    circle.setStrokeWidth(0.1);
    circle.setTranslateX(cityBoxCenterX);
    circle.setTranslateY(cityBoxCenterY);
    cityViewLayer.getChildren().add(circle);

    final Text cityDescriptionText = new Text(city.getName() + "\n" + textManager.getText("Size") + ": "
        + city.getFactories().size() + "\n(" + textManager.getText("Max size") + ": "
        + city.getRoadSectionAngles().length + ")");
    cityDescriptionText.setFill(TEXT_COLOR);
    cityDescriptionText.setTranslateX(cityDescriptionBoxMinX + 10);
    cityDescriptionText.setTranslateY(cityDescriptionBoxMinY + 20);
    cityViewLayer.getChildren().add(cityDescriptionText);

    final Text productivityText = new Text(textManager.getText("Productivity") + ": "
        + (int) (productivity * 100) + "%");
    productivityText.setFill(TEXT_COLOR);
    productivityText.setTranslateX(cityBoxMinX + 10);
    productivityText.setTranslateY(cityBoxMinY + cityBoxSide - 10);
    cityViewLayer.getChildren().add(productivityText);
  }

  @Override
  public void notifyOwnGameServerConnection(boolean connected) throws ClientViewException {
    // NOTE: null implementation.
  }

  @Override
  public void notifyOthersGameServerConnection(boolean connected) throws ClientViewException {
    // NOTE: null implementation.
  }

  @Override
  public void notifyGameUndecided() throws ClientViewException {
    // NOTE: Not implemented.
  }

  @Override
  public Event[] interact(Event[] inputEvents) throws ClientViewException {
    // Wait for the user.
    try {
      if (breakpointRound <= roundCount) {
        okSemaphore.acquire();
      }
    } catch (InterruptedException exception) {
      throw new ClientViewException("Acquiring okSemaphore failed.", exception);
    }
    ++roundCount;
    return null;
  }

}
