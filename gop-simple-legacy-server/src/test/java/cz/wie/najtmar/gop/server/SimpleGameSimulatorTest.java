package cz.wie.najtmar.gop.server;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.name.Names;

import cz.wie.najtmar.gop.board.Board;
import cz.wie.najtmar.gop.board.BoardException;
import cz.wie.najtmar.gop.board.BoardGenerator;
import cz.wie.najtmar.gop.board.Joint;
import cz.wie.najtmar.gop.board.RoadSection;
import cz.wie.najtmar.gop.client.view.GameView;
import cz.wie.najtmar.gop.client.view.JointView;
import cz.wie.najtmar.gop.client.view.PlayerView;
import cz.wie.najtmar.gop.client.view.PrawnView;
import cz.wie.najtmar.gop.client.view.RoadSectionView;
import cz.wie.najtmar.gop.common.Point2D;
import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.game.EventFactory;
import cz.wie.najtmar.gop.game.Game;
import cz.wie.najtmar.gop.game.GameProperties;
import cz.wie.najtmar.gop.game.ObjectSerializer;
import cz.wie.najtmar.gop.game.Player;
import cz.wie.najtmar.gop.game.PropertiesReader;
import cz.wie.najtmar.gop.game.SettlersUnit;
import cz.wie.najtmar.gop.game.SimplePropertiesReader;
import cz.wie.najtmar.gop.game.User;
import cz.wie.najtmar.gop.io.Channel;
import cz.wie.najtmar.gop.io.Message;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.StringReader;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Nonnull;
import javax.json.Json;

/**
 * Tests for class GameView.
 * @author najtmar
 */
public class SimpleGameSimulatorTest {

  static final long START_TIME = 1000;
  static final double STEP_SIZE = 0.5;
  static final int PLAYER_COUNT = 2;

  Board board;
  Joint joint0;
  Joint joint1;
  Joint joint2;
  RoadSection roadSection0;
  RoadSection roadSection1;
  Game game;
  GameProperties gameProperties;
  GoP.ConfirmationOnlySession uiSession;
  SimpleGameSimulator simulator;

  static class TestBoardGenerator implements BoardGenerator {

    private final PropertiesReader propertiesReader;

    @Inject
    TestBoardGenerator(@Nonnull PropertiesReader propertiesReader) {
      this.propertiesReader = propertiesReader;
    }

    @Override
    public Board generate() throws BoardException {
      final Board board;
      try {
        board = new Board(propertiesReader.getBoardProperties());
      } catch (IOException exception) {
        throw new BoardException("Reading Board Properties failed.", exception);
      }
      board.addJoint(
          (new Joint.Builder())
            .setBoard(board)
            .setIndex(0)
            .setName("joint0")
            .setCoordinatesPair(new Point2D.Double(0.0, 0.0))
            .build());
      board.addJoint(
          (new Joint.Builder())
            .setBoard(board)
            .setIndex(1)
            .setName("joint1")
            .setCoordinatesPair(new Point2D.Double(3.0, 0.0))
            .build());
      board.addJoint(
          (new Joint.Builder())
            .setBoard(board)
            .setIndex(2)
            .setName("joint2")
            .setCoordinatesPair(new Point2D.Double(3.0, 3.0))
            .build());
      board.addRoadSection(
          (new RoadSection.Builder())
            .setBoard(board)
            .setIndex(0)
            .setJoints(board.getJoints().get(0), board.getJoints().get(1))
            .build());
      board.addRoadSection(
          (new RoadSection.Builder())
            .setBoard(board)
            .setIndex(1)
            .setJoints(board.getJoints().get(1), board.getJoints().get(2))
            .build());
      return board;
    }

  }

  private Module getTestModule() {
    return new AbstractModule() {
      @Override protected void configure() {
        uiSession = mock(GoP.ConfirmationOnlySession.class);
        bind(PropertiesReader.class).to(SimplePropertiesReader.class);
        bind(BoardGenerator.class).to(TestBoardGenerator.class);
        bind(GoP.ConfirmationOnlySession.class).toInstance(uiSession);
        bind(Integer.class).annotatedWith(Names.named("player count")).toInstance(PLAYER_COUNT);
        bind(Long.class).annotatedWith(Names.named("random seed")).toInstance(42L);
      }
    };
  }

  /**
   * Method setUp().
   * @throws Exception when field creation fails
   */
  @Before
  public void setUp() throws Exception {
    final Injector injector = Guice.createInjector(getTestModule());
    simulator = injector.getInstance(SimpleGameSimulator.Factory.class).create();
    simulator = spy(simulator);
    game = simulator.getGame();
    game.advanceTimeTo(START_TIME);
    gameProperties = game.getGameProperties();
    board = game.getBoard();
    joint0 = board.getJoints().get(0);
    joint1 = board.getJoints().get(1);
    joint2 = board.getJoints().get(2);
    roadSection0 = board.getRoadSections().get(0);
    roadSection1 = board.getRoadSections().get(1);
  }

  @Test
  public void testSimpleGameExecutionWorks() throws Exception {
    when(uiSession.processEvents(any(Event[].class), any(PlayerView.class))).thenReturn(new Event[]{});
    final GameSession unusedGameSession = mock(GameSession.class);
    final Channel[] clientChannels = simulator.establishConnectionChannels(unusedGameSession);
    verify(uiSession, times(1)).setSimulators(any(SimplePlayerSimulator[].class));
    assertEquals(PLAYER_COUNT, clientChannels.length);
    {
      final Message message = clientChannels[0].readMessage();
      assertEquals(1, message.getEvents().length);
      assertEquals(
          Json.createReader(new StringReader(
              "{ "
            + "  \"type\": \"ADD_PLAYER\", "
            // NOTE: ADD_PLAYER Events always have time 0.
            + "  \"time\": 0, "
            + "  \"player\": { "
            + "    \"index\": 0, "
            + "    \"user\": { "
            + "      \"name\":\"Player 0\", "
            + "      \"uuid\": \"00000000-0000-04d2-0000-000000000000\" "
            + "    } "
            + "  } "
            + "}")).readObject(),
          message.getEvents()[0].getBody());
    }
    final Player player0 = new Player(new User("Player 0", UUID.fromString("00000000-0000-04d2-0000-000000000000")),
        0, game);
    {
      final Message message = clientChannels[1].readMessage();
      assertEquals(1, message.getEvents().length);
      assertEquals(
          Json.createReader(new StringReader(
              "{ "
            + "  \"type\": \"ADD_PLAYER\", "
            // NOTE: ADD_PLAYER Events always have time 0.
            + "  \"time\": 0, "
            + "  \"player\": { "
            + "    \"index\": 1, "
            + "    \"user\": { "
            + "      \"name\":\"Player 1\", "
            + "      \"uuid\": \"00000000-0000-04d2-0000-000000000001\" "
            + "    } "
            + "  } "
            + "}")).readObject(),
          message.getEvents()[0].getBody());
    }
    final Player player1 = new Player(new User("Player 1", UUID.fromString("00000000-0000-04d2-0000-000000000001")),
        1, game);
    game.addPlayer(player0);
    game.addPlayer(player1);

    SettlersUnit settlersUnit0;
    SettlersUnit settlersUnit1;
    settlersUnit0 = (new SettlersUnit.Builder())
        .setCreationTime(START_TIME)
        .setGameProperties(gameProperties)
        .setPlayer(player0)
        .setCurrentPosition(joint0.getNormalizedPosition())
        .build();
    settlersUnit1 = (new SettlersUnit.Builder())
        .setCreationTime(START_TIME)
        .setGameProperties(gameProperties)
        .setPlayer(player1)
        .setCurrentPosition(joint1.getNormalizedPosition())
        .build();
    game.addPrawn(settlersUnit0);
    game.addPrawn(settlersUnit1);
    game.initialize();
    game.advanceTimeTo(START_TIME);

    final ObjectSerializer objectSerializer = new ObjectSerializer(game);
    final EventFactory eventFactory = new EventFactory(objectSerializer);

    final Message initializationMessage = new Message(new Event[]{
        eventFactory.createInitializeGameEvent(),
        eventFactory.createProduceSettlersUnitEvent(settlersUnit0.getId(), settlersUnit0.getCurrentPosition(),
            settlersUnit0.getPlayer()),
        eventFactory.createProduceSettlersUnitEvent(settlersUnit1.getId(), settlersUnit1.getCurrentPosition(),
            settlersUnit1.getPlayer()),
        });
    for (Channel clientChannel : clientChannels) {
      clientChannel.writeMessage(initializationMessage);
    }

    final SimplePlayerSimulator[] simulators = simulator.getPlayerSimulators();

    for (int i = 0; i < clientChannels.length; ++i) {
      final Channel clientChannel = clientChannels[i];
      final Message inputMessage = clientChannel.readMessage();
      final Event[] inputEvents = inputMessage.getEvents();
      if (i == 0) {
        assertEquals(0, inputEvents.length);
      } else {
        assertEquals(1, inputEvents.length);
        assertEquals(Event.Type.SET_PRAWN_ITINERARY, inputEvents[0].getType());
      }
      final GameView gameView = simulators[i].getGame();
      assertNotNull("Simulator " + i, gameView);
      final JointView[] joints = gameView.getJoints();
      assertEquals("Simulator " + i, 3, joints.length);
      assertEquals("Simulator " + i, joint1.getCoordinatesPair(), joints[1].getCoordinatesPair());
      final RoadSectionView[] roadSections = gameView.getRoadSections();
      assertEquals("Simulator " + i, 2, roadSections.length);
      assertEquals("Simulator " + i, roadSection1.getJoints()[0].getIndex(), roadSections[1].getJoints()[0].getIndex());
      assertEquals("Simulator " + i, roadSection1.getJoints()[1].getIndex(), roadSections[1].getJoints()[1].getIndex());
      final Map<Integer, PrawnView> prawnMap = gameView.getPrawnMap();
      assertEquals("Simulator " + i, 2, prawnMap.size());
      assertEquals("Simulator " + i, settlersUnit1.getId(), prawnMap.get(1).getId());
    }

    for (Channel clientChannel : clientChannels) {
      clientChannel.writeMessage(new Message(new Event[]{new Event(
          Json.createObjectBuilder()
          .add("type", "FINISH_GAME")
          .add("time", START_TIME)
          .build())}));
    }
    for (Thread simulator : simulators) {
      simulator.join();
    }
    verify(uiSession, times(1)).initialize(null, null);
    verify(uiSession, times(2)).processEvents(any(Event[].class), any(PlayerView.class));
  }

}
