package cz.wie.najtmar.gop.server;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.FrameworkMessage;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import com.esotericsoftware.minlog.Log;

import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.event.EventProcessingException;
import cz.wie.najtmar.gop.game.Game;
import cz.wie.najtmar.gop.game.GameException;
import cz.wie.najtmar.gop.game.GameProperties;
import cz.wie.najtmar.gop.game.Player;
import cz.wie.najtmar.gop.game.PropertiesReader;
import cz.wie.najtmar.gop.game.User;
import cz.wie.najtmar.gop.io.Channel;
import cz.wie.najtmar.gop.io.Message;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.GuardedBy;
import javax.json.JsonObject;
import javax.json.JsonValue;

/**
 * Communication manager based on the KryoNet technology. Works as a server run on behalf of a User .
 * @author najtmar
 */
@Singleton
public class KryoNetCommunicator implements ConnectionManager {

  /**
   * KryoNet buffer size used for communication.
   */
  public static final int KRYO_NET_BUFFER_SIZE = 1 * 1000 * 1000;

  /**
   * KryoNet buffer size used for simple operations (e.g., discovery).
   */
  public static final int KRYO_NET_SMALL_BUFFER_SIZE = 10000;

  /**
   * First TCP port tried to be used to KryoNet communication.
   */
  public static final int FIRST_TCP_PORT = 54555;

  /**
   * First UDP port tried to be used for KryoNet communication.
   */
  public static final int FIRST_UDP_PORT = 54777;

  /**
   * The number of ports tried to be used for KryoNet communication, starting from the first port.
   */
  public static final int PORT_TRIED_COUNT = 5;

  /**
   * Timeout for KryoNet operations, in milliseconds.
   */
  private static final int CONNECTION_TIMEOUT_MILLIS = 2000;

  /**
   * Value of CONNECTION_TIMEOUT_MILLIS adjusted in tests.
   */
  private static int connectionTimeoutMillis = CONNECTION_TIMEOUT_MILLIS;

  /**
   * Expected time interval between receiving two subsequent HEARTBEAT messages.
   */
  private static final long HEARTBEAT_THREAD_INTERVAL_MILLIS = 5000;

  /**
   * Value of HEARTBEAT_THREAD_INTERVAL_MILLIS adjusted in tests.
   */
  private static long heartbeatThreadInternalMillis = HEARTBEAT_THREAD_INTERVAL_MILLIS;

  /**
   * Value denoting a not initialized timestamp in milliseconds.
   */
  public static final long NOT_INITIALIZED_TIMESTAMP_MILLIS = -1;

  /**
   * The State of the KryoNetCommunicator .
   */
  @GuardedBy("this")
  private State state;

  /**
   * True if the local connection is being established.
   */
  @GuardedBy("this")
  private boolean establishingLocalConnection;

  /**
   * Defines the State of Game initialization.
   * @author najtmar
   */
  static enum GameInitializationState {
    /**
     * The Game is not yet initialized.
     */
    PENDING,
    /**
     * The Game is initialized.
     */
    INITIALIZED,
    /**
     * The Game has already finished.
     */
    FINISHED,
    /**
     * The Game is aborted and will never be initialized.
     */
    INTENT_ABORTED,
  }

  /**
   * Maps a locally hosted Game id to its GameInitializationState .
   */
  @GuardedBy("this")
  private final Map<UUID, GameInitializationState> gameInitializationStateMap;

  /**
   * Maps a locally hosted Game id to a Map between Users willing to participate in the Game and their respective Client
   * Connections .
   */
  @GuardedBy("this")
  private final Map<UUID, Map<UUID, Connection>> gameUserConnectionMap;

  /**
   * Maps a locally hosted Game id to a Map between Users willing to participate in the Game and their respective
   * Connection Channels .
   */
  @GuardedBy("this")
  private final Map<UUID, Map<UUID, Channel>> gameUserChannelMap;

  /**
   * Similar to gameUserChannelMap, except that it contains the Client version of the Channels (i.e., input and output
   * queues are reversed).
   */
  @GuardedBy("this")
  private final Map<UUID, Map<UUID, Channel>> gameUserServerChannelMap;

  /**
   * Maps a locally hosted Game id to its List of participants, in the order it will be returned by method
   * establishConnectionChannels(). localUserId is always to first element on the List .
   */
  @GuardedBy("this")
  private final Map<UUID, List<UUID>> gameOrderedConnectionsMap;

  /**
   * Maps a locally hosted Game id to a Map between Users and the ids of the last ServerMessages sent to them.
   */
  @GuardedBy("this")
  private final Map<UUID, Map<UUID, Integer>> gameUserLastMessageSentIdMap;

  /**
   * Maps a locally hosted Game id to a Map between Users and the ids of the last ClientMessages received from them.
   */
  @GuardedBy("this")
  private final Map<UUID, Map<UUID, Integer>> gameUserLastMessageReceivedIdMap;

  /**
   * Maps a locally hosted Game id to its respective GameSession .
   */
  @GuardedBy("this")
  private final Map<UUID, GameSession> gameSessionMap;

  /**
   * Maps a user id to its name.
   */
  private final Map<UUID, String> userNameMap;

  /**
   * Maps a locally hosted Game id to the Thread responsible for Event transmission.
   */
  @GuardedBy("this")
  private final Map<UUID, Thread> gameThreadMap;

  /**
   * Maps a locally hosted Game to a Map from Users to their respective Message hashIds to confirm.
   * Used for Game loop synchronization purposes.
   */
  @GuardedBy("this")
  private final Map<UUID, Map<UUID, Integer>> gameMessagesToConfirm;

  /**
   * Maps a locally hosted Game to a Map from Users to their respective timestamps when the last HEARTBEAT message was
   * received.
   */
  @GuardedBy("this")
  private final Map<UUID, Map<UUID, Long>> gameLastUserHeartbeatMap;

  /**
   * Maps a locally hosted Game to a Set of Users that have already abandoned that game.
   */
  @GuardedBy("this")
  private final Map<UUID, Set<UUID>> gameAbandonedUsersMap;

  /**
   * The id of the User acting as the local Client .
   */
  private UUID localUserId;

  /**
   * The connection to the local Client .
   */
  private Connection localClientConnection;

  /**
   * Used to read Properties .
   */
  private final PropertiesReader propertiesReader;

  /**
   * Server thread.
   */
  private final Server server;

  /**
   * The number of the local Server TCP port.
   */
  private int serverTcpPort;

  /**
   * The number of the local Server UDP port.
   */
  private int serverUdpPort;

  /**
   * The maximum number of Players taking part in the Game.
   */
  private int maxPlayerCount;

  /**
   * Represents an Overdue ServerMessage that failed to be sent to a specific User in a Game .
   * @author najtmar
   */
  class OverdueServerMessage {

    /**
     * The ServerMessage that failed to be sent.
     */
    private final ServerMessage message;

    /**
     * The id of the User to whom the ServerMessage failed to be sent.
     */
    private final UUID userId;

    /**
     * Number of times message was attempted to be sent.
     */
    private int tryCount;

    /**
     * Timestamp of the objext creation.
     */
    private final long timestampMillis;

    public OverdueServerMessage(ServerMessage message, UUID userId) {
      super();
      this.message = message;
      this.userId = userId;
      this.tryCount = 1;
      this.timestampMillis = System.currentTimeMillis();
    }

    public ServerMessage getMessage() {
      return message;
    }

    public UUID getUserId() {
      return userId;
    }

    public int getTryCount() {
      return tryCount;
    }

    /**
     * Increases tryCount by 1.
     */
    public void incrementTryCount() {
      ++tryCount;
    }

    public long getTimestampMillis() {
      return timestampMillis;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + getOuterType().hashCode();
      if (message == null) {
        result = prime * result;
      } else {
        result = prime * result + message.hashCode();
      }
      result = prime * result + tryCount;
      if (userId == null) {
        result = prime * result;
      } else {
        result = prime * result + userId.hashCode();
      }
      return result;
    }

    @Override
    public boolean equals(Object obj) {
      if (!(obj instanceof OverdueServerMessage)) {
        return false;
      }
      final OverdueServerMessage otherMessage = (OverdueServerMessage) obj;
      return message.equals(otherMessage.message) && userId.equals(otherMessage.userId)
          && tryCount == otherMessage.tryCount;
    }

    private KryoNetCommunicator getOuterType() {
      return KryoNetCommunicator.this;
    }

    @Override
    public String toString() {
      return "OverdueServerMessage [message=" + message + ", userId=" + userId + ", tryCount="
          + tryCount + "]";
    }

  }

  /**
   * Maps the id of a Game to a Queue keeping overdue messages, if these could not be sent.
   */
  @GuardedBy("this")
  private final Map<UUID, Queue<OverdueServerMessage>> overdueMessageQueueMap;

  /**
   * Maps the id of a Game to a Map between user ids and Queues keeping unconfirmed messages.
   */
  @GuardedBy("this")
  private final Map<UUID, Map<UUID, Queue<OverdueServerMessage>>> unconfirmedMessageQueueMap;

  /**
   * true iff overdue ServerMessages are now being sent.
   */
  private volatile boolean overdueServerMessageSendingInProgress;

  @Inject
  KryoNetCommunicator(PropertiesReader propertiesReader) {
    this.propertiesReader = propertiesReader;
    this.gameInitializationStateMap = new HashMap<>();
    this.gameUserConnectionMap = new HashMap<>();
    this.gameUserChannelMap = new HashMap<>();
    this.gameUserServerChannelMap = new HashMap<>();
    this.gameOrderedConnectionsMap = new HashMap<>();
    this.gameUserLastMessageSentIdMap = new HashMap<>();
    this.gameUserLastMessageReceivedIdMap = new HashMap<>();
    this.gameSessionMap = new HashMap<>();
    this.gameThreadMap = new HashMap<>();
    this.userNameMap = new HashMap<>();
    this.gameMessagesToConfirm = new HashMap<>();
    this.gameLastUserHeartbeatMap = new HashMap<>();
    this.gameAbandonedUsersMap = new HashMap<>();
    this.overdueMessageQueueMap = new HashMap<>();
    this.unconfirmedMessageQueueMap = new HashMap<>();
    this.server = new Server(KRYO_NET_BUFFER_SIZE, KRYO_NET_BUFFER_SIZE);
    this.state = State.NOT_INITIALIZED;
    this.overdueServerMessageSendingInProgress = false;
  }

  public static class Factory implements ConnectionManager.Factory<KryoNetCommunicator> {
    private final Provider<KryoNetCommunicator> provider;

    @Inject
    Factory(Provider<KryoNetCommunicator> provider) {
      this.provider = provider;
    }

    @Override
    public KryoNetCommunicator create() throws ServerException {
      return provider.get();
    }
  }

  /**
   * Sends overdue messages from queue for the Game with the given gameId .
   * @param queue the Queue to send the overdue ServerMessages from
   * @param gameId the id of the Game to send the ServerMessages for
   * @param remove whether successfully sent ServerMessages should be removed from queue
   * @return true iff all overdue ServerMessages have been sent successfully
   */
  private boolean sendMessagesFromOverdueQueue(@Nonnull Queue<OverdueServerMessage> queue, @Nonnull UUID gameId,
      boolean remove) {
    final Queue<OverdueServerMessage> actualQueue;
    if (remove) {
      actualQueue = queue;
    } else {
      actualQueue = new LinkedList<>(queue);
    }
    while (true) {
      synchronized (this) {
        if (actualQueue.isEmpty()) {
          return true;
        }
      }
      final OverdueServerMessage overdueMessage;
      final Connection overdueConnection;
      synchronized (this) {
        overdueMessage = actualQueue.peek();
        overdueConnection = gameUserConnectionMap.get(gameId).get(overdueMessage.getUserId());
      }
      int bytesSent = overdueConnection.sendTCP(overdueMessage.getMessage());
      if (bytesSent > 0) {
        synchronized (this) {
          actualQueue.remove();
        }
      } else {
        overdueMessage.incrementTryCount();
        return false;
      }
    }
  }

  /**
   * Sends overdue ServerMessages from the overdue ServerMessage Queue for the Game with the given gameId .
   * @param gameId the id of the Game for which ServerMessages are sent
   * @return true iff all overdue ServerMessages have been sent successfully
   */
  private boolean flushOverdueMessageQueue(@Nonnull UUID gameId) {
    final Queue<OverdueServerMessage> overdueMessageQueue;
    synchronized (this) {
      overdueMessageQueue = overdueMessageQueueMap.get(gameId);
      if (!gameUserConnectionMap.containsKey(gameId) && !overdueMessageQueue.isEmpty()) {
        Log.error("Game with id " + gameId + " not present in gameUserConnectionMap . Overdue mesages will not "
            + "be sent.");
        overdueMessageQueue.clear();
        return false;
      }
    }
    if (!sendMessagesFromOverdueQueue(overdueMessageQueue, gameId, true)) {
      final OverdueServerMessage overdueMessage = overdueMessageQueue.peek();
      Log.error("Sending ServerMessage from overdueMessageQueueMap failed, attempt " + overdueMessage.getTryCount()
          + ": " + overdueMessage.getMessage());
      return false;
    }
    return true;
  }

  /**
   * Confirms the last ServerMessage received by a User . Clears unconfirmedMessageQueueMap accordingly.
   * @param gameId the Game for which the ServerMessage id is confirmed
   * @param userId the User for which the ServerMessage id is confirmed
   * @param lastMessageId the id of the last ServerMessage received by the User in the Game
   */
  synchronized void confirmLastServerMessage(@Nonnull UUID gameId, @Nonnull UUID userId,
      @Nonnegative int lastMessageId) {
    if (!unconfirmedMessageQueueMap.containsKey(gameId)) {
      Log.warn("Game " + gameId + " not present in unconfirmedMessageQueueMap .");
      return;
    }
    if (!unconfirmedMessageQueueMap.get(gameId).containsKey(userId)) {
      Log.warn("User " + userId + " not present in unconfirmedMessageQueueMap for Game " + gameId + " .");
      return;
    }
    final Queue<OverdueServerMessage> queue = unconfirmedMessageQueueMap.get(gameId).get(userId);
    while (!queue.isEmpty() && queue.peek().getMessage().getId() <= lastMessageId) {
      queue.remove();
    }
  }

  /**
   * Sends overdue ServerMessages from the unconfirmed ServerMessage Queue for the Game with the given gameId .
   * @param gameId the id of the Game for which ServerMessages are sent
   * @param userId the id of the User to which the ServerMessages are sent
   * @return true iff all overdue ServerMessages have been sent successfully
   */
  private boolean flushUnconfirmedMessageQueue(@Nonnull UUID gameId, @Nonnull UUID userId) {
    final Queue<OverdueServerMessage> unconfirmedMessageQueue;
    synchronized (this) {
      if (!unconfirmedMessageQueueMap.containsKey(gameId)
          || !unconfirmedMessageQueueMap.get(gameId).containsKey(userId)) {
        return true;
      }
      unconfirmedMessageQueue = unconfirmedMessageQueueMap.get(gameId).get(userId);
      if ((!gameUserConnectionMap.containsKey(gameId) || !gameUserConnectionMap.get(gameId).containsKey(userId))
          && !unconfirmedMessageQueue.isEmpty()) {
        Log.error("Connection to User with id " + userId + " in Game with id " + gameId
            + " not present in gameUserConnectionMap . Overdue mesages will not be sent.");
        unconfirmedMessageQueue.clear();
        return false;
      }
    }
    if (!sendMessagesFromOverdueQueue(unconfirmedMessageQueue, gameId, false)) {
      final OverdueServerMessage unconfirmedMessage = unconfirmedMessageQueue.peek();
      Log.error("Sending ServerMessage from unconfirmedMessageQueueMap failed, attempt "
          + unconfirmedMessage.getTryCount() + ": " + unconfirmedMessage.getMessage());
      return false;
    }
    return true;
  }

  /**
   * Given message to be sent to the User with id userId in the Game with id gameId, assigns an incremental id
   * to message .
   * @param message the message to assign the id to
   * @param gameId the id of the Game in which message is sent
   * @param userId the id of the User to which message is sent
   * @throws ServerException when the id is already assigned
   */
  private synchronized void assignServerMessageId(@Nonnull ServerMessage message, @Nonnull UUID gameId,
      @Nonnull UUID userId) throws ServerException {
    if (message.getId() != ServerMessage.ID_NOT_SET) {
      throw new ServerException("Message id already set: " + message);
    }
    if (!gameUserLastMessageSentIdMap.containsKey(gameId)) {
      gameUserLastMessageSentIdMap.put(gameId, new HashMap<>());
    }
    if (!gameUserLastMessageSentIdMap.get(gameId).containsKey(userId)) {
      gameUserLastMessageSentIdMap.get(gameId).put(userId, 0);
    }
    final int newMessageId = gameUserLastMessageSentIdMap.get(gameId).get(userId) + 1;
    message.setId(newMessageId);
    gameUserLastMessageSentIdMap.get(gameId).put(userId, newMessageId);
  }

  /**
   * Adds message to the Queue of ServerMessages sent to a User in a Game that has not been confirmed.
   * @param message the ServerMessage not confirmed
   * @param gameId the id of the Game in which message is sent
   * @param userId the id of the User to which message is sent
   */
  private synchronized void addToUnconfirmedMessageQueue(@Nonnull ServerMessage message, @Nonnull UUID gameId,
      @Nonnull UUID userId) {
    if (!unconfirmedMessageQueueMap.containsKey(gameId)) {
      unconfirmedMessageQueueMap.put(gameId, new HashMap<>());
    }
    if (!unconfirmedMessageQueueMap.get(gameId).containsKey(userId)) {
      unconfirmedMessageQueueMap.get(gameId).put(userId, new LinkedList<>());
    }
    unconfirmedMessageQueueMap.get(gameId).get(userId).add(new OverdueServerMessage(message, userId));
  }

  /**
   * Sends all messages from overdueMessageQueue, and then message, using connection . Adds an incremental id to
   * message . If sending any message fails, the message is queued. If ServerMessages are now being sent, message
   * is queued immediately.
   * @param connection the Connection used to send message
   * @param message the ServerMessage to be sent or queued
   * @param gameId the id of the Game
   * @param userId the id of the User
   */
  void sendServerMessage(@Nonnull Connection connection, @Nonnull ServerMessage message, @Nonnull UUID gameId,
      @Nonnull UUID userId) {
    // Manage ServerMessage confirmation.
    try {
      assignServerMessageId(message, gameId, userId);
    } catch (ServerException exception) {
      Log.error("Error assigning id to a ServerMessage: " + exception);
      return;
    }
    flushUnconfirmedMessageQueue(gameId, userId);
    addToUnconfirmedMessageQueue(message, gameId, userId);

    final Queue<OverdueServerMessage> overdueMessageQueue;
    synchronized (this) {
      if (!overdueMessageQueueMap.containsKey(gameId)) {
        overdueMessageQueueMap.put(gameId, new LinkedList<>());
      }
      overdueMessageQueue = overdueMessageQueueMap.get(gameId);
    }

    // If overdue ServerMessages are being sent, just add message to the queue.
    synchronized (this) {
      if (overdueServerMessageSendingInProgress) {
        overdueMessageQueue.add(new OverdueServerMessage(message, userId));
        return;
      }
      overdueServerMessageSendingInProgress = true;
    }

    // Send the overdue Messages first.
    try {
      if (!flushOverdueMessageQueue(gameId)) {
        return;
      }
    } finally {
      synchronized (this) {
        overdueServerMessageSendingInProgress = false;
      }
    }

    // Send or queue message .
    int bytesSent = connection.sendTCP(message);
    if (bytesSent == 0) {
      synchronized (this) {
        overdueMessageQueue.add(new OverdueServerMessage(message, userId));
        Log.error("Sending Message failed, attempt 1: " + message);
        return;
      }
    }
  }

  /**
   * For a given Game and a User, creates a part of Channels used for communication and puts them into respective Maps .
   * @param gameId the id of the Game
   * @param userId the id of the User
   * @return true iff Channel creation succeeds
   */
  private boolean createGameUserChannels(@Nonnull UUID gameId, @Nonnull UUID userId) {
    final BlockingQueue<Message> inputQueue = new LinkedBlockingQueue<>();
    final BlockingQueue<Message> outputQueue = new LinkedBlockingQueue<>();
    try {
      gameUserChannelMap.get(gameId).put(userId, new Channel(inputQueue, outputQueue));
      gameUserServerChannelMap.get(gameId).put(userId, new Channel(outputQueue, inputQueue));
      // TODO(najtmar): Start checking here and in the code reading/writing Messages.
    } catch (IOException exception) {
      Log.error("Error creating communication Channels: " + exception);
      if (gameUserChannelMap.get(gameId).containsKey(userId)) {
        gameUserChannelMap.get(gameId).remove(userId);
      }
      if (gameUserServerChannelMap.get(gameId).containsKey(userId)) {
        gameUserServerChannelMap.get(gameId).remove(userId);
      }
      return false;
    }
    return true;
  }

  @Override
  public Channel[] establishConnectionChannels(GameSession gameSession) throws ServerException {
    synchronized (this) {
      if (!state.equals(State.STARTED)) {
        throw new ServerException("ConnectionManager not started.");
      }

      // Initialize Channel establishing.
      final UUID gameId = gameSession.getGame().getId();
      if (gameInitializationStateMap.containsKey(gameId)) {
        throw new ServerException("Connection Channels for Game with id " + gameId + " already requested and in State "
            + gameInitializationStateMap.get(gameId));
      }
      if (gameUserConnectionMap.containsKey(gameId)) {
        throw new ServerException("Game with id " + gameId + " already hosted: " + gameUserConnectionMap.get(gameId));
      }
      if (gameUserChannelMap.containsKey(gameId)) {
        throw new ServerException("Game with id " + gameId + " already hosted: " + gameUserChannelMap.get(gameId));
      }
      if (gameUserServerChannelMap.containsKey(gameId)) {
        throw new ServerException("Game with id " + gameId + " already hosted: "
            + gameUserServerChannelMap.get(gameId));
      }
      if (gameOrderedConnectionsMap.containsKey(gameId)) {
        throw new ServerException("Game with id " + gameId + " already started collection participants: "
            + gameOrderedConnectionsMap.get(gameId));
      }
      if (gameSessionMap.containsKey(gameId)) {
        throw new ServerException("Game with id " + gameId + " already hosted: " + gameSessionMap.get(gameId));
      }
      gameInitializationStateMap.put(gameId, GameInitializationState.PENDING);
      final Map<UUID, Connection> clientConnectionMap = new HashMap<>();
      clientConnectionMap.put(localUserId, localClientConnection);
      gameUserConnectionMap.put(gameId, clientConnectionMap);
      gameUserChannelMap.put(gameId, new HashMap<>());
      gameUserServerChannelMap.put(gameId, new HashMap<>());
      if (!createGameUserChannels(gameId, localUserId)) {
        throw new ServerException("Creating User Channels failed.");
      }
      gameOrderedConnectionsMap.put(gameId, new LinkedList<>(Arrays.asList(localUserId)));
      gameSessionMap.put(gameId, gameSession);
      testNotify();

      // Wait for Channels to be established.
      channelEstablishingLoop:
      while (true) {
        try {
          this.wait();
        } catch (InterruptedException exception) {
          throw new ServerException(exception);
        } finally {
          this.notify();
        }
        final GameInitializationState state = gameInitializationStateMap.get(gameId);
        switch (state) {
          case PENDING:
            break;
          case INITIALIZED:
            break channelEstablishingLoop;
          case INTENT_ABORTED:
            return null;
          case FINISHED:
          default:
            throw new ServerException("Unexpected State: " + state);
        }
      }

      return StreamSupport.stream(gameOrderedConnectionsMap.get(gameId))
          .map(userId -> gameUserServerChannelMap.get(gameId).get(userId)).toArray(size -> new Channel[size]);
    }
  }

  @Override
  public synchronized void initialize() throws ServerException {
    if (!state.equals(State.NOT_INITIALIZED)) {
      throw new ServerException("ConnectionManager already initialized.");
    }

    final GameProperties gameProperties;
    try {
      gameProperties = new GameProperties(propertiesReader.getGameProperties());
    } catch (GameException | IOException exception) {
      throw new ServerException(exception);
    }
    maxPlayerCount = gameProperties.getMaxPlayerCount();

    initializeServer();
    state = State.INITIALIZED;
  }

  /**
   * Invoked when a User wants to join a Game .
   * @param connection the Connection to the User
   * @param gameId the id of the Game to be joined
   * @param userId the id of the User to join
   * @param userName the name of the User
   */
  private synchronized void onJoinGame(@Nonnull Connection connection, @Nonnull UUID gameId, @Nonnull UUID userId,
      @Nonnull String userName) {
    if (!state.equals(State.STARTED)) {
      // NOTE: This is a best-effort Message. No queuing performed.
      connection.sendTCP(ServerMessage.createJoinGameResponse(gameId, userId, false));
      Log.error("ConnectionManager not yet started: " + state);
      return;
    }
    if (!gameInitializationStateMap.containsKey(gameId)) {
      // NOTE: This is a best-effort Message. No queuing performed.
      connection.sendTCP(ServerMessage.createJoinGameResponse(gameId, userId, false));
      Log.error("Game with id " + gameId + " does not exist.");
      return;
    }
    final GameInitializationState state = gameInitializationStateMap.get(gameId);
    if (!state.equals(GameInitializationState.PENDING)) {
      // NOTE: This is a best-effort Message. No queuing performed.
      connection.sendTCP(ServerMessage.createJoinGameResponse(gameId, userId, false));
      Log.error("Game with id " + gameId + " should be in State PENDING (found " + state + ").");
      return;
    }
    if (gameUserConnectionMap.get(gameId).containsKey(userId)) {
      // NOTE: This is a best-effort Message. No queuing performed.
      connection.sendTCP(ServerMessage.createJoinGameResponse(gameId, userId, false));
      Log.error("User with id " + userId + " has already joined Game with id " + gameId + " .");
      return;
    }
    if (maxPlayerCount <= gameUserConnectionMap.get(gameId).size()) {
      // NOTE: This is a best-effort Message. No queuing performed.
      connection.sendTCP(ServerMessage.createJoinGameResponse(gameId, userId, false));
      Log.error("Game with id " + gameId + " already has " + gameUserConnectionMap.get(gameId).size()
          + " participants.");
      return;
    }
    if (!createGameUserChannels(gameId, userId)) {
      return;
    }
    gameUserConnectionMap.get(gameId).put(userId, connection);
    gameOrderedConnectionsMap.get(gameId).add(userId);
    userNameMap.put(userId, userName);
    sendServerMessage(connection, ServerMessage.createJoinGameResponse(gameId, userId, true), gameId, userId);
    final int gameParticipantCount = gameUserConnectionMap.get(gameId).size();
    sendServerMessage(localClientConnection, ServerMessage.createOwnGameStartEnabledStatus(
        2 <= gameParticipantCount && gameParticipantCount <= maxPlayerCount,
        StreamSupport.stream(gameUserConnectionMap.get(gameId).keySet()).map(uuid -> userNameMap.get(uuid))
            .collect(Collectors.toList())), gameId, localUserId);
    testNotify();
  }

  /**
   * Invoked when a User wants to force quit a Game .
   * @param connection the Connection to the User
   * @param gameId the id of the Game to be force quit
   * @param userId the id of the User to force quit
   */
  private synchronized void onForceQuitGame(@Nonnull Connection connection, @Nonnull UUID gameId,
      @Nonnull UUID userId) {
    if (!state.equals(State.STARTED)) {
      Log.error("ConnectionManager not yet started: " + state);
      return;
    }
    if (!gameSessionMap.containsKey(gameId)) {
      Log.error("Game with id " + gameId + " does not exist.");
      return;
    }
    if (!gameUserConnectionMap.get(gameId).containsKey(userId)) {
      Log.error("User with id " + userId + " has not joined Game with id " + gameId + " .");
      return;
    }
    final Game game = gameSessionMap.get(gameId).getGame();
    if (game.getState() == Game.State.FINISHED) {
      Log.info("Game with id " + gameId + " already finished. FORCE_QUIT_GAME request ignored.");
      return;
    }
    for (Player player : game.getPlayers()) {
      if (player.getState() == Player.State.IN_GAME) {
        player.setState(Player.State.UNDECIDED);
      }
    }
    try {
      game.finish();
    } catch (GameException exception) {
      Log.error("Error finishing the Game: " + exception.getMessage());
      return;
    }
    testNotify();
  }

  /**
   * Starts a Thread to run a loop for Game Events transmission.
   * @param gameId the id of the Game to start the loop for
   * @return the Thread started or null if starting the Thread failed
   */
  private Thread startGameThread(@Nonnull UUID gameId) {
    if (!gameInitializationStateMap.get(gameId).equals(GameInitializationState.PENDING)) {
      Log.error("Game with id " + gameId + " should be in State PENDING (found " + state + ").");
      return null;
    }
    if (gameMessagesToConfirm.containsKey(gameId)) {
      Log.error("Game with id " + gameId + " already has Message Map initialized.");
      return null;
    }
    gameMessagesToConfirm.put(gameId, new HashMap<>());
    gameLastUserHeartbeatMap.put(gameId, new HashMap<>());
    for (UUID userId : gameOrderedConnectionsMap.get(gameId)) {
      gameLastUserHeartbeatMap.get(gameId).put(userId, NOT_INITIALIZED_TIMESTAMP_MILLIS);
    }
    gameAbandonedUsersMap.put(gameId, new HashSet<>());
    final Thread thread = new Thread(new Runnable() {
      @Override
      public void run() {
        boolean isGameFinished = false;
        while (!isGameFinished) {
          // Send Messages to be confirmed.
          final List<UUID> gameUsers;
          final Set<UUID> abandonedUsers;
          synchronized (KryoNetCommunicator.this) {
            gameUsers = new LinkedList<UUID>(gameOrderedConnectionsMap.get(gameId));
            abandonedUsers = gameAbandonedUsersMap.get(gameId);
          }
          final Set<UUID> newAbandonedUsers = new HashSet<>(abandonedUsers);
          for (UUID userId : gameUsers) {
            final Channel channel;
            final Connection connection;
            synchronized (KryoNetCommunicator.this) {
              channel = gameUserChannelMap.get(gameId).get(userId);
              connection = gameUserConnectionMap.get(gameId).get(userId);
            }

            // If the User has already abandoned the Game, respond on his behalf with an empty Message instead of
            // communicating with him.
            if (abandonedUsers.contains(userId)) {
              try {
                channel.writeMessage(new Message(new Event[]{}));
              } catch (InterruptedException exception) {
                Log.error("Sending an empty Message failed.");
              }
              continue;
            }


            final Message message;
            try {
              message = channel.readMessage();
            } catch (InterruptedException exception) {
              Log.error("Reading Message from " + channel + "failed");
              continue;
            }
            for (Event event : message.getEvents()) {
              if (event.getType() == Event.Type.FINISH_GAME) {
                isGameFinished = true;
              }
              if (event.getType() == Event.Type.CHANGE_PLAYER_STATE
                  && Player.State.valueOf(event.getBody().getString("state")) == Player.State.ABANDONED) {
                newAbandonedUsers.add(gameUsers.get(event.getBody().getInt("player")));
              }
            }
            synchronized (KryoNetCommunicator.this) {
              if (gameMessagesToConfirm.get(gameId).containsKey(userId)) {
                Log.error("User with id " + userId + " already has a pending Message with hashCode "
                    + gameMessagesToConfirm.get(gameId).get(userId) + " in Game with id " + gameId + " .");
                continue;
              }
              gameMessagesToConfirm.get(gameId).put(userId, message.hashCode());
            }
            sendServerMessage(connection, ServerMessage.createSendMessage(gameId, userId, message), gameId, userId);
          }

          // Wait for confirmation.
          while (true) {
            synchronized (KryoNetCommunicator.this) {
              if (gameMessagesToConfirm.get(gameId).isEmpty()) {
                break;
              }
              try {
                KryoNetCommunicator.this.wait(1);
              } catch (InterruptedException exception) {
                Log.warn("Waiting interrupted: " + exception);
              }
            }
          }

          synchronized (KryoNetCommunicator.this) {
            gameAbandonedUsersMap.put(gameId, newAbandonedUsers);
          }
        }
        synchronized (KryoNetCommunicator.this) {
          gameInitializationStateMap.put(gameId, GameInitializationState.FINISHED);
        }
      }
    });
    thread.start();
    return thread;
  }

  /**
   * Invoked when a User wants to initialize a Game .
   * @param connection the Connection to the User
   * @param gameId the id of the Game to be initialized
   * @param userId the id of the User to order Game initialization
   */
  private synchronized void onInitializeGame(@Nonnull Connection connection, @Nonnull UUID gameId,
      @Nonnull UUID userId) {
    if (!state.equals(State.STARTED)) {
      // NOTE: This is a best-effort Message. No queuing performed.
      connection.sendTCP(ServerMessage.createInitializeGameResponse(gameId, userId, false));
      Log.error("ConnectionManager not yet started: " + state);
      return;
    }
    if (!gameInitializationStateMap.containsKey(gameId)) {
      // NOTE: This is a best-effort Message. No queuing performed.
      connection.sendTCP(ServerMessage.createInitializeGameResponse(gameId, userId, false));
      Log.error("Game with id " + gameId + " does not exist.");
      return;
    }
    final GameInitializationState state = gameInitializationStateMap.get(gameId);
    if (!state.equals(GameInitializationState.PENDING)) {
      // NOTE: This is a best-effort Message. No queuing performed.
      connection.sendTCP(ServerMessage.createInitializeGameResponse(gameId, userId, false));
      Log.error("Game with id " + gameId + " should be in State PENDING (found " + state + ").");
      return;
    }
    final int participantCount = gameUserConnectionMap.get(gameId).size();
    if (participantCount < 2 || maxPlayerCount < participantCount) {
      // NOTE: This is a best-effort Message. No queuing performed.
      connection.sendTCP(ServerMessage.createInitializeGameResponse(gameId, userId, false));
      Log.error("Game with id " + gameId + " should be have between 2 and " + maxPlayerCount + " participants (found "
          + participantCount + ").");
      return;
    }
    if (!userId.equals(localUserId)) {
      // NOTE: This is a best-effort Message. No queuing performed.
      connection.sendTCP(ServerMessage.createInitializeGameResponse(gameId, userId, false));
      Log.error("Only the local User with id " + localUserId + " can initialize a Game (id found " + userId + ").");
      return;
    }
    if (gameThreadMap.containsKey(gameId)) {
      // NOTE: This is a best-effort Message. No queuing performed.
      connection.sendTCP(ServerMessage.createInitializeGameResponse(gameId, userId, false));
      Log.error("There is already an Event transmission Thread started for the Game with id " + gameId + " .");
      return;
    }
    final Thread thread = startGameThread(gameId);
    if (thread == null) {
      // NOTE: This is a best-effort Message. No queuing performed.
      connection.sendTCP(ServerMessage.createInitializeGameResponse(gameId, userId, false));
      return;
    }
    gameThreadMap.put(gameId, thread);
    gameInitializationStateMap.put(gameId, GameInitializationState.INITIALIZED);

    // Notify the participants that the Game has started.
    for (UUID participantId : gameUserConnectionMap.get(gameId).keySet()) {
      if (!participantId.equals(localUserId)) {
        final Connection participantConnection = gameUserConnectionMap.get(gameId).get(participantId);
        sendServerMessage(
            participantConnection, ServerMessage.createStartGame(gameId, participantId), gameId, participantId);
      }
    }

    notify();

    // Wait for Game to get initialized.
    while (true) {
      try {
        this.wait();
      } catch (InterruptedException exception) {
        Log.error("Waiting for Game initialization interrupted: " + exception);
        sendServerMessage(connection, ServerMessage.createInitializeGameResponse(gameId, userId, false), gameId,
            userId);
        return;
      } finally {
        this.notify();
      }
      final GameInitializationState gameState = gameInitializationStateMap.get(gameId);
      if (!gameState.equals(GameInitializationState.PENDING)) {
        break;
      }
    }

    sendServerMessage(connection, ServerMessage.createInitializeGameResponse(gameId, userId, true), gameId, userId);
    for (Map.Entry<UUID, Connection> entry : gameUserConnectionMap.get(gameId).entrySet()) {
      final UUID requestedUserId = entry.getKey();
      final Connection requestConnection = entry.getValue();
      final int index = gameOrderedConnectionsMap.get(gameId).indexOf(requestedUserId);
      sendServerMessage(requestConnection, ServerMessage.createRequestAddPlayer(gameId, requestedUserId, index), gameId,
          requestedUserId);
    }
    testNotify();
  }

  /**
   * Invoked when a User wants to cancel a Game intent.
   * @param connection the Connection to the User
   * @param gameId the id of the Game whose intent should be canceled
   * @param userId the id of the User to cancel the Game
   */
  private synchronized void onCancelGameIntent(@Nonnull Connection connection, @Nonnull UUID gameId,
      @Nonnull UUID userId) {
    if (!state.equals(State.STARTED)) {
      // NOTE: This is a best-effort Message. No queuing performed.
      connection.sendTCP(ServerMessage.createCancelGameIntentResponse(gameId, userId, false));
      Log.error("ConnectionManager not yet started: " + state);
      return;
    }
    if (!gameInitializationStateMap.containsKey(gameId)) {
      // NOTE: This is a best-effort Message. No queuing performed.
      connection.sendTCP(ServerMessage.createCancelGameIntentResponse(gameId, userId, false));
      Log.error("Game with id " + gameId + " does not exist.");
      return;
    }
    final GameInitializationState state = gameInitializationStateMap.get(gameId);
    if (!state.equals(GameInitializationState.PENDING)) {
      // NOTE: This is a best-effort Message. No queuing performed.
      connection.sendTCP(ServerMessage.createCancelGameIntentResponse(gameId, userId, false));
      Log.error("Game with id " + gameId + " should be in State PENDING (found " + state + ").");
      return;
    }
    if (!userId.equals(localUserId)) {
      // NOTE: This is a best-effort Message. No queuing performed.
      connection.sendTCP(ServerMessage.createCancelGameIntentResponse(gameId, userId, false));
      Log.error("Only the local User with id " + localUserId + " can cancel a Game (id found " + userId + ").");
      return;
    }
    if (gameThreadMap.containsKey(gameId)) {
      // NOTE: This is a best-effort Message. No queuing performed.
      connection.sendTCP(ServerMessage.createCancelGameIntentResponse(gameId, userId, false));
      Log.error("There is already an Event transmission Thread started for the Game with id " + gameId + " .");
      return;
    }
    gameInitializationStateMap.put(gameId, GameInitializationState.INTENT_ABORTED);
    for (Map.Entry<UUID, Connection> entry : gameUserConnectionMap.get(gameId).entrySet()) {
      final UUID otherUserId = entry.getKey();
      if (!otherUserId.equals(userId)) {
        final Connection otherUserConnection = entry.getValue();
        // NOTE: We cannot retry this message, because the Game is immediately removed from the Maps .
        otherUserConnection.sendTCP(ServerMessage.createJoinGameResponse(gameId, otherUserId, false));
      }
    }
    gameUserConnectionMap.remove(gameId);
    gameUserChannelMap.remove(gameId);
    gameUserServerChannelMap.remove(gameId);
    gameOrderedConnectionsMap.remove(gameId);
    gameSessionMap.remove(gameId);
    notify();
    sendServerMessage(localClientConnection, ServerMessage.createCancelGameIntentResponse(gameId, userId, true),
        gameId, localUserId);
    testNotify();
  }

  /**
   * Invoked when a User wants to leave a Game intent.
   * @param connection the Connection to the User
   * @param gameId the id of the Game whose intent should be left
   * @param userId the id of the User to leave the Game
   */
  private synchronized void onLeaveGameIntent(@Nonnull Connection connection, @Nonnull UUID gameId,
      @Nonnull UUID userId) {
    if (!state.equals(State.STARTED)) {
      // NOTE(0): This is a best-effort Message. No queuing performed.
      // NOTE(1): The result of the Action is true .
      connection.sendTCP(ServerMessage.createLeaveGameIntentResponse(gameId, userId, true));
      Log.error("ConnectionManager not yet started: " + state);
      return;
    }
    if (!gameInitializationStateMap.containsKey(gameId)) {
      // NOTE(0): This is a best-effort Message. No queuing performed.
      // NOTE(1): The result of the Action is true .
      connection.sendTCP(ServerMessage.createLeaveGameIntentResponse(gameId, userId, true));
      Log.error("Game with id " + gameId + " does not exist.");
      return;
    }
    final GameInitializationState state = gameInitializationStateMap.get(gameId);
    if (state == GameInitializationState.INTENT_ABORTED) {
      // NOTE(0): This is a best-effort Message. No queuing performed.
      // NOTE(1): The result of the Action is true .
      connection.sendTCP(ServerMessage.createLeaveGameIntentResponse(gameId, userId, true));
      Log.info("Game with id " + gameId + " has been aborted.");
      return;
    }
    if (state != GameInitializationState.PENDING) {
      // NOTE: This is a best-effort Message. No queuing performed.
      connection.sendTCP(ServerMessage.createLeaveGameIntentResponse(gameId, userId, false));
      Log.error("Game with id " + gameId + " cannot be left because it's not in State PENDING (found " + state + ").");
      return;
    }
    if (!gameUserConnectionMap.get(gameId).containsKey(userId)) {
      // NOTE(0): This is a best-effort Message. No queuing performed.
      // NOTE(1): The result of the Action is true .
      connection.sendTCP(ServerMessage.createLeaveGameIntentResponse(gameId, userId, true));
      Log.error("The User with id " + localUserId + " hasn't joined the Game with id " + gameId + " .");
      return;
    }
    if (userId.equals(localUserId)) {
      // NOTE: This is a best-effort Message. No queuing performed.
      connection.sendTCP(ServerMessage.createLeaveGameIntentResponse(gameId, userId, false));
      Log.error("The local User with id " + localUserId + " cannot leave a Game (id found " + userId + ").");
      return;
    }
    gameUserConnectionMap.get(gameId).remove(userId);
    gameOrderedConnectionsMap.get(gameId).remove(userId);
    sendServerMessage(connection, ServerMessage.createLeaveGameIntentResponse(gameId, userId, true), gameId, userId);
    final int gameParticipantCount = gameUserConnectionMap.get(gameId).size();
    sendServerMessage(localClientConnection, ServerMessage.createOwnGameStartEnabledStatus(
        2 <= gameParticipantCount && gameParticipantCount <= maxPlayerCount,
        StreamSupport.stream(gameUserConnectionMap.get(gameId).keySet()).map(uuid -> userNameMap.get(uuid))
            .collect(Collectors.toList())), gameId, localUserId);
    testNotify();
  }

  /**
   * Invoked when a User wants to get a list of pending Games .
   * @param connection the Connection to the User
   * @param userId the id of the User that orders the list of pending Games
   */
  private synchronized void onGetPendingGames(@Nonnull Connection connection, @Nonnull UUID userId) {
    if (!state.equals(State.STARTED)) {
      // NOTE: This is a best-effort Message. No queuing performed.
      connection.sendTCP(ServerMessage.createGetPendingGamesResponse(new GameSession[]{}, null));
      Log.error("ConnectionManager not yet started: " + state);
      return;
    }
    final GameSession[] pendingGameSessions = StreamSupport.stream(gameInitializationStateMap.entrySet())
        .filter(entry -> entry.getValue().equals(GameInitializationState.PENDING))
        .map(entry -> gameSessionMap.get(entry.getKey())).toArray(size -> new GameSession[size]);
    // NOTE: This is a best-effort Message. No queuing performed.
    connection.sendTCP(ServerMessage.createGetPendingGamesResponse(pendingGameSessions, userNameMap.get(localUserId)));
    testNotify();
  }

  /**
   * Invoked when a User sends a Message .
   * @param connection the Connection to the User
   * @param gameId the id of the Game for which the Message is sent
   * @param userId the id of the User that sends the Message
   * @param message the Message being sent
   */
  synchronized void onSendMessage(@Nonnull Connection connection, @Nonnull UUID gameId, @Nonnull UUID userId,
      @Nonnull Message message) {
    if (!state.equals(State.STARTED)) {
      // NOTE: This is a best-effort Message. No queuing performed.
      connection.sendTCP(ServerMessage.createClientMessageResponse(message.hashCode(), false));
      Log.error("ConnectionManager not yet started: " + state);
      return;
    }
    if (!gameUserChannelMap.containsKey(gameId)) {
      Log.error("Game with id " + gameId + " does not exist .");
      return;
    }
    if (!gameUserChannelMap.get(gameId).containsKey(userId)) {
      // NOTE: This is a best-effort Message. No queuing performed.
      connection.sendTCP(ServerMessage.createClientMessageResponse(message.hashCode(), false));
      Log.error("User with id " + userId + " not participating in Game with id " + gameId + " .");
      return;
    }
    final Channel channel = gameUserChannelMap.get(gameId).get(userId);
    final GameInitializationState state = gameInitializationStateMap.get(gameId);
    if (!state.equals(GameInitializationState.INITIALIZED) && !state.equals(GameInitializationState.FINISHED)) {
      // NOTE: This is a best-effort Message. No queuing performed.
      connection.sendTCP(ServerMessage.createClientMessageResponse(message.hashCode(), false));
      Log.error("Game with id " + gameId + " should be in State INITIALIZED or FINISHED or (found " + state + ").");
      return;
    }

    try {
      channel.writeMessage(message);
    } catch (InterruptedException exception) {
      // NOTE: This is a best-effort Message. No queuing performed.
      connection.sendTCP(ServerMessage.createClientMessageResponse(message.hashCode(), false));
      Log.error("Sending message " + message + "failed: " + exception);
      return;
    }

    // NOTE: This is a best-effort Message. No queuing performed.
    connection.sendTCP(ServerMessage.createClientMessageResponse(message.hashCode(), true));
    testNotify();
  }

  /**
   * Invoked when a User confirms receiving a Message .
   * @param connection the Connection to the User
   * @param gameId the id of the Game for which the Message is sent
   * @param userId the id of the User that sends the Message
   * @param hashCode the hashCode of the Message
   */
  private synchronized void onServerMessageResponse(@Nonnull Connection connection, @Nonnull UUID gameId,
      @Nonnull UUID userId, int hashCode) {
    if (!state.equals(State.STARTED)) {
      Log.error("ConnectionManager not yet started: " + state);
      return;
    }
    final GameInitializationState state = gameInitializationStateMap.get(gameId);
    if (!state.equals(GameInitializationState.INITIALIZED)) {
      Log.error("Game with id " + gameId + " should be in State INITIALIZED (found " + state + ").");
      return;
    }
    if (!gameMessagesToConfirm.containsKey(gameId)) {
      Log.error("Game with id " + gameId + " has no messages waiting for confirmation.");
      return;
    }
    if (!gameMessagesToConfirm.get(gameId).containsKey(userId)) {
      Log.error("Game with id " + gameId + " has no messages waiting for confirmation by User with id " + userId
          + " .");
      return;
    }
    if (gameMessagesToConfirm.get(gameId).get(userId) != hashCode) {
      Log.error("The message to confirm in Game with id " + gameId + " for User with id " + userId
          + " has a different hashCode than " + hashCode + " (" + gameMessagesToConfirm.get(gameId).get(userId)
          + ") .");
      return;
    }
    gameMessagesToConfirm.get(gameId).remove(userId);
    testNotify();
  }

  /**
   * Invoked when a Users tries to reconnect as a Player of a Game .
   * @param connection the Connection to the User
   * @param gameId the id of the Game to reconnect
   * @param userId the User that tries to reconnect
   * @param hostName the hostName of the address the User tries to reconnect to
   */
  private synchronized void onReconnectPlayer(@Nonnull Connection connection, @Nonnull UUID gameId,
      @Nonnull UUID userId, @Nonnull String hostName) {
    if (!gameUserConnectionMap.containsKey(gameId)) {
      // NOTE: This is a best-effort Message. No queuing performed.
      connection.sendTCP(ServerMessage.createReconnectPlayerResponse(gameId, userId, hostName, false));
      Log.error("Game with id " + gameId + " does not exist.");
      return;
    }
    final Map<UUID, Connection> userConnectionMap = gameUserConnectionMap.get(gameId);
    if (!userConnectionMap.containsKey(userId)) {
      // NOTE: This is a best-effort Message. No queuing performed.
      connection.sendTCP(ServerMessage.createReconnectPlayerResponse(gameId, userId, hostName, false));
      Log.error("User with id " + userId + " does not exist in Game with id " + gameId + " .");
      return;
    }
    userConnectionMap.put(userId, connection);

    // NOTE: Buffering a Message whose main purpose is to confirm that the connection is reestablished would have little
    // sense.
    connection.sendTCP(ServerMessage.createReconnectPlayerResponse(gameId, userId, hostName, true));

    // Send overdue ServerMessages .
    boolean sendOverdueMessages;
    synchronized (this) {
      if (!overdueServerMessageSendingInProgress) {
        overdueServerMessageSendingInProgress = true;
        sendOverdueMessages = true;
      } else {
        sendOverdueMessages = false;
      }
    }
    if (sendOverdueMessages) {
      flushOverdueMessageQueue(gameId);
      synchronized (this) {
        overdueServerMessageSendingInProgress = false;
      }
    }
    testNotify();
  }

  /**
   * Invoked when a HEARTBEAT Message is received.
   * @param connection the Connection to the User
   * @param id the id of the HEARTBEAT message received
   * @param gameId the id of the Game queried, or null if no Game is queried
   * @param userId the id of the User that sends the HEARTBEAT message
   * @param lastServerMessageId the id if the last confirmed ServerMessage
   */
  private synchronized void onHeartbeat(@Nonnull Connection connection, @Nonnegative int id, @Nullable UUID gameId,
      @Nonnull UUID userId, @Nonnegative int lastServerMessageId) {
    boolean gamePresent = true;
    boolean gameUndecided = false;
    final long currentTimeMillis = System.currentTimeMillis();
    final Set<UUID> unresponsiveUsersSet = new HashSet<>();
    if (gameId != null) {
      if (gameLastUserHeartbeatMap.containsKey(gameId)) {
        gameLastUserHeartbeatMap.get(gameId).put(userId, currentTimeMillis);
      }
      if (!gameSessionMap.containsKey(gameId)) {
        gamePresent = false;
      } else {
        final Game game = gameSessionMap.get(gameId).getGame();
        try {
          if (game.getState() == Game.State.FINISHED && game.getResult() == Game.Result.UNDECIDED) {
            gameUndecided = true;
          }
        } catch (GameException exception) {
          Log.warn("Error getting Game result: " + exception.getMessage());
        }
        // Add Users for whom messages are in overdueMessageQueueMap .
        if (overdueMessageQueueMap.containsKey(gameId)) {
          final Queue<OverdueServerMessage> overdueMessageQueue = overdueMessageQueueMap.get(gameId);
          for (OverdueServerMessage overdueMessage : overdueMessageQueue) {
            unresponsiveUsersSet.add(overdueMessage.getUserId());
          }
        }
        if (gameLastUserHeartbeatMap.containsKey(gameId)) {
          // Add users that haven't sent a HEARTBEAT for a long time.
          unresponsiveUsersSet.addAll(StreamSupport.stream(gameLastUserHeartbeatMap.get(gameId).entrySet())
              .filter(entry -> entry.getValue() != NOT_INITIALIZED_TIMESTAMP_MILLIS)
              .filter(entry -> !gameAbandonedUsersMap.get(gameId).contains(entry.getKey())
                  && 3 * heartbeatThreadInternalMillis < currentTimeMillis - entry.getValue())
              .map(entry -> entry.getKey()).collect(Collectors.toSet()));
          // Initialize not initialized timestamps with the current timestamp.
          for (Map.Entry<UUID, Long> entry : gameLastUserHeartbeatMap.get(gameId).entrySet()) {
            if (entry.getValue() == NOT_INITIALIZED_TIMESTAMP_MILLIS) {
              gameLastUserHeartbeatMap.get(gameId).put(entry.getKey(), currentTimeMillis);
            }
          }
        }
      }
    }

    // Manage unconfirmedMessageQueueMap .
    confirmLastServerMessage(gameId, userId, lastServerMessageId);
    flushUnconfirmedMessageQueue(gameId, userId);
    if (unconfirmedMessageQueueMap.containsKey(gameId)) {
      for (Map.Entry<UUID, Queue<OverdueServerMessage>> entry : unconfirmedMessageQueueMap.get(gameId).entrySet()) {
        if (!entry.getValue().isEmpty()) {
          final long messageTimestampMillis = entry.getValue().peek().getTimestampMillis();
          if (currentTimeMillis - messageTimestampMillis > 3 * heartbeatThreadInternalMillis) {
            unresponsiveUsersSet.add(entry.getKey());
          }
        }
      }
    }

    final List<UUID> unresponsiveUsers;
    if (gameId != null) {
      unresponsiveUsers = new LinkedList<>(unresponsiveUsersSet);
    } else {
      unresponsiveUsers = null;
    }

    final int lastClientMessageId;
    if (!gameUserLastMessageReceivedIdMap.containsKey(gameId)) {
      lastClientMessageId = 0;
    } else if (!gameUserLastMessageReceivedIdMap.get(gameId).containsKey(userId)) {
      lastClientMessageId = 0;
    } else {
      lastClientMessageId = gameUserLastMessageReceivedIdMap.get(gameId).get(userId);
    }

    // NOTE: This is a best-effort Message. No queuing performed.
    connection.sendTCP(
        ServerMessage.createHeartbeatResponse(id, gamePresent, gameUndecided, unresponsiveUsers, lastClientMessageId));
    testNotify();
  }

  /**
   * Invoked when a User wants to get a list of Games matching the request.
   * @param connection the Connection to the User
   * @param gameIdHashCode the hash code of the id of the Game to be looked for
   * @param userIdHashCode the hash code of the id of the User that orders the list of started Games
   */
  private synchronized void onGetMatchingGames(@Nonnull Connection connection, int gameIdHashCode, int userIdHashCode) {
    if (!state.equals(State.STARTED)) {
      // NOTE: This is a best-effort Message. No queuing performed.
      connection.sendTCP(ServerMessage.createGetMatchingGamesResponse(new GameSession[]{}));
      Log.error("ConnectionManager not yet started: " + state);
      return;
    }
    final List<GameSession> matchingGameSessionsList = new LinkedList<>();
    for (Map.Entry<UUID, GameSession> entry : gameSessionMap.entrySet()) {
      final UUID gameId = entry.getKey();
      if (gameId.hashCode() != gameIdHashCode) {
        continue;
      }
      final GameSession gameSession = entry.getValue();
      if (gameSession.getGame() == null) {
        continue;
      }
      final Game game = gameSession.getGame();
      boolean playerFound = false;
      for (Player player : game.getPlayers()) {
        final User user = player.getUser();
        if (user.getId().hashCode() == userIdHashCode) {
          playerFound = true;
          break;
        }
      }
      if (playerFound) {
        matchingGameSessionsList.add(gameSession);
      }
    }
    // NOTE: This is a best-effort Message. No queuing performed.
    connection.sendTCP(
        ServerMessage.createGetMatchingGamesResponse(matchingGameSessionsList.toArray(new GameSession[]{})));
    testNotify();
  }

  /**
   * Starts establishing local connection.
   */
  public synchronized void startEstablishingLocalConnection() {
    establishingLocalConnection = true;
  }

  /**
   * Filter message by its id. Update lastMessageId accordingly.
   * @param message the ClientMessage to be filtered
   * @return true iff the massage should be accepted
   */
  private synchronized boolean manageClientMessageFiltering(@Nonnull ClientMessage message) {
    final UUID gameId;
    final UUID userId;
    switch (message.getType()) {
      case SEND_MESSAGE:
        gameId = UUID.fromString(message.getBody().getString("gameId"));
        userId = UUID.fromString(message.getBody().getString("userId"));
        break;
      default:
        return true;
    }
    final int messageId = message.getId();
    if (messageId == ClientMessage.ID_NOT_SET) {
      return true;
    }
    final int lastMessageId;
    if (!gameUserLastMessageReceivedIdMap.containsKey(gameId)) {
      gameUserLastMessageReceivedIdMap.put(gameId, new HashMap<>());
      lastMessageId = 0;
    } else if (!gameUserLastMessageReceivedIdMap.get(gameId).containsKey(userId)) {
      lastMessageId = 0;
    } else {
      lastMessageId = gameUserLastMessageReceivedIdMap.get(gameId).get(userId);
    }
    if (messageId != lastMessageId + 1) {
      Log.warn("Message filtered by id. Expected: " + (lastMessageId + 1) + ", actual: " + messageId);
      return false;
    }
    gameUserLastMessageReceivedIdMap.get(gameId).put(userId, messageId);
    return true;
  }

  /**
   * Initializes the Server part of the KryoNetCommunicator .
   * @throws ServerException when Server initialization failed
   */
  private void initializeServer() throws ServerException {
    final Kryo kryo =  server.getKryo();
    kryo.register(ClientMessage.class);
    kryo.register(ClientMessage.Type.class);
    kryo.register(ServerMessage.class);
    kryo.register(ServerMessage.Type.class);

    server.addListener(new Listener() {

      @Override
      public void received(Connection connection, Object object) {
        if (object instanceof ClientMessage) {
          final ClientMessage request = (ClientMessage) object;
          if (!manageClientMessageFiltering(request)) {
            return;
          }
          switch (request.getType()) {
            case REGISTER_LOCAL_CLIENT: {
              synchronized (KryoNetCommunicator.this) {
                if (establishingLocalConnection) {
                  localUserId = UUID.fromString(request.getBody().getString("localUserId"));
                  userNameMap.put(localUserId, request.getBody().getString("localUserName"));
                  localClientConnection = connection;
                  if (state == State.INITIALIZED) {
                    state = State.STARTED;
                  }
                  for (Map<UUID, Connection> connectionMap : gameUserConnectionMap.values()) {
                    if (connectionMap.containsKey(localUserId)) {
                      connectionMap.put(localUserId, connection);
                    }
                  }
                  establishingLocalConnection = false;
                } else {
                  Log.warn("Flag establishingLocalConnection should be set. REGISTER_LOCAL_CLIENT Request ignored: "
                      + request);
                }
              }
              break;
            }
            case JOIN_GAME: {
              final UUID gameId = UUID.fromString(request.getBody().getString("gameId"));
              final UUID userId = UUID.fromString(request.getBody().getString("userId"));
              final String userName = request.getBody().getString("userName");
              onJoinGame(connection, gameId, userId, userName);
              break;
            }
            case FORCE_QUIT_GAME: {
              final UUID gameId = UUID.fromString(request.getBody().getString("gameId"));
              final UUID userId = UUID.fromString(request.getBody().getString("userId"));
              onForceQuitGame(connection, gameId, userId);
              break;
            }
            case INITIALIZE_GAME: {
              final UUID gameId = UUID.fromString(request.getBody().getString("gameId"));
              final UUID userId = UUID.fromString(request.getBody().getString("userId"));
              onInitializeGame(connection, gameId, userId);
              break;
            }
            case CANCEL_GAME_INTENT: {
              final UUID gameId = UUID.fromString(request.getBody().getString("gameId"));
              final UUID userId = UUID.fromString(request.getBody().getString("userId"));
              onCancelGameIntent(connection, gameId, userId);
              break;
            }
            case LEAVE_GAME_INTENT: {
              final UUID gameId = UUID.fromString(request.getBody().getString("gameId"));
              final UUID userId = UUID.fromString(request.getBody().getString("userId"));
              onLeaveGameIntent(connection, gameId, userId);
              break;
            }
            case GET_PENDING_GAMES: {
              final UUID userId = UUID.fromString(request.getBody().getString("userId"));
              onGetPendingGames(connection, userId);
              break;
            }
            case SEND_MESSAGE: {
              final UUID gameId = UUID.fromString(request.getBody().getString("gameId"));
              final UUID userId = UUID.fromString(request.getBody().getString("userId"));
              final int hashCode = request.getBody().getInt("hashCode");
              final List<Event> eventsList = new LinkedList<>();
              for (JsonValue eventValue : request.getBody().getJsonArray("events")) {
                final JsonObject eventObject = (JsonObject) eventValue;
                try {
                  eventsList.add(new Event(eventObject));
                } catch (EventProcessingException exception) {
                  Log.error("Event " + eventObject + " could not be parsed in " + request);
                  // NOTE: This is a best-effort Message. No queuing performed.
                  connection.sendTCP(ServerMessage.createClientMessageResponse(hashCode, false));
                  break;
                }
              }
              final Message message = new Message(eventsList.toArray(new Event[]{}));
              if (hashCode != message.hashCode()) {
                Log.error("hashCode " + hashCode + " doesn't match Message " + message);
                // NOTE: This is a best-effort Message. No queuing performed.
                connection.sendTCP(ServerMessage.createClientMessageResponse(hashCode, false));
                break;
              }
              onSendMessage(connection, gameId, userId, message);
              break;
            }
            case SERVER_MESSAGE_RESPONSE: {
              final UUID gameId = UUID.fromString(request.getBody().getString("gameId"));
              final UUID userId = UUID.fromString(request.getBody().getString("userId"));
              final int hashCode = request.getBody().getInt("hashCode");
              onServerMessageResponse(connection, gameId, userId, hashCode);
              break;
            }
            case HEARTBEAT: {
              final int id = request.getBody().getInt("id");
              final UUID gameId;
              if (request.getBody().containsKey("gameId")) {
                gameId = UUID.fromString(request.getBody().getString("gameId"));
              } else {
                gameId = null;
              }
              final UUID userId = UUID.fromString(request.getBody().getString("userId"));
              final int lastMessageId = request.getBody().getInt("lastMessageId");
              onHeartbeat(connection, id, gameId, userId, lastMessageId);
              break;
            }
            case RECONNECT_PLAYER: {
              final UUID gameId = UUID.fromString(request.getBody().getString("gameId"));
              final UUID userId = UUID.fromString(request.getBody().getString("userId"));
              final String hostString = request.getBody().getString("hostString");
              onReconnectPlayer(connection, gameId, userId, hostString);
              break;
            }
            case GET_MATCHING_GAMES: {
              final int gameIdHashCode = request.getBody().getInt("gameIdHashCode");
              final int userIdHashCode = request.getBody().getInt("userIdHashCode");
              onGetMatchingGames(connection, gameIdHashCode, userIdHashCode);
              break;
            }
            default: {
              Log.error("Unexpected Request type: " + request);
              break;
            }
          }
        } else {
          if (object instanceof FrameworkMessage.KeepAlive) {
            // Do nothing.
          } else {
            Log.warn("Unrecognized message of type " + object.getClass().getName() + ": " + object);
          }
        }
      }

    });

    boolean serverBound = false;
    int serverTcpPortAttempt = 0;
    int serverUdpPortAttempt = 0;
    for (int i = 0; !serverBound && i < PORT_TRIED_COUNT; ++i) {
      try {
        serverTcpPortAttempt = FIRST_TCP_PORT + i;
        serverUdpPortAttempt = FIRST_UDP_PORT + i;
        server.bind(serverTcpPortAttempt, serverUdpPortAttempt);
        serverBound = true;
      } catch (IOException exception) {
        Log.warn("Local port combination (tcp=" + serverTcpPortAttempt + ", udp=" + serverUdpPortAttempt
            + ") already taken: " + exception);
      }
    }
    if (!serverBound) {
      throw new ServerException("Binding Server ports failed.");
    }
    serverTcpPort = serverTcpPortAttempt;
    serverUdpPort = serverUdpPortAttempt;

    server.start();
  }

  @Override
  public synchronized State getState() {
    return state;
  }

  public synchronized boolean isEstablishingLocalConnection() {
    return establishingLocalConnection;
  }

  /**
   * Closes the Server .
   */
  public void close() {
    server.close();
  }

  UUID getLocalUserId() {
    return localUserId;
  }

  Connection getLocalClientConnection() {
    return localClientConnection;
  }

  public int getServerTcpPort() {
    return serverTcpPort;
  }

  public void setServerTcpPort(int serverTcpPort) {
    this.serverTcpPort = serverTcpPort;
  }

  public int getServerUdpPort() {
    return serverUdpPort;
  }

  void setServerUdpPort(int serverUdpPort) {
    this.serverUdpPort = serverUdpPort;
  }

  Map<UUID, GameInitializationState> getGameInitializationStateMap() {
    return gameInitializationStateMap;
  }

  Map<UUID, Map<UUID, Connection>> getGameUserConnectionMap() {
    return gameUserConnectionMap;
  }

  public static int getConnectionTimeoutMillis() {
    return connectionTimeoutMillis;
  }

  public static void injectConnectionTimeoutMillis(int connectionTimeoutMillis) {
    KryoNetCommunicator.connectionTimeoutMillis = connectionTimeoutMillis;
  }

  public static long getHeartbeatThreadInternalMillis() {
    return heartbeatThreadInternalMillis;
  }

  public static void injectHeartbeatThreadInternalMillis(long heartbeatThreadInternalMillis) {
    KryoNetCommunicator.heartbeatThreadInternalMillis = heartbeatThreadInternalMillis;
  }

  Map<UUID, Queue<OverdueServerMessage>> getOverdueMessageQueueMap() {
    return overdueMessageQueueMap;
  }

  Map<UUID, Map<UUID, Long>> getGameLastUserHeartbeatMap() {
    return gameLastUserHeartbeatMap;
  }

  Map<UUID, Set<UUID>> getGameAbandonedUsersMap() {
    return gameAbandonedUsersMap;
  }

  Map<UUID, Map<UUID, Integer>> getGameUserLastMessageSentIdMap() {
    return gameUserLastMessageSentIdMap;
  }

  Map<UUID, Map<UUID, Integer>> getGameUserLastMessageReceivedIdMap() {
    return gameUserLastMessageReceivedIdMap;
  }

  Map<UUID, Map<UUID, Queue<OverdueServerMessage>>> getUnconfirmedMessageQueueMap() {
    return unconfirmedMessageQueueMap;
  }

  /**
   * Used for synchronization purposes in tests.
   */
  void testNotify() {}

}
