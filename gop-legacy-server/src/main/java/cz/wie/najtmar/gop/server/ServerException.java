package cz.wie.najtmar.gop.server;

/**
 * Class of Exceptions generated on the server side.
 * @author najtmar
 */
public class ServerException extends Exception {

  private static final long serialVersionUID = -4839789106725336725L;

  public ServerException() {
  }

  public ServerException(String message) {
    super(message);
  }

  public ServerException(Throwable cause) {
    super(cause);
  }

  public ServerException(String message, Throwable cause) {
    super(message, cause);
  }

  public ServerException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

}
