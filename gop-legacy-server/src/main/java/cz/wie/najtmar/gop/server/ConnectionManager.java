package cz.wie.najtmar.gop.server;

import cz.wie.najtmar.gop.io.Channel;

import javax.annotation.Nonnull;

/**
 * Used to manage connections between Clients and a Server.
 * @author najtmar
 */
public interface ConnectionManager {

  /**
   * State of a ConnectionManager .
   * @author najtmar
   *
   */
  public enum State {
    /**
     * ConnectionManager not yet initialized.
     */
    NOT_INITIALIZED,

    /**
     * ConnectionManager initialized.
     */
    INITIALIZED,

    /**
     * ConnectionManager started.
     */
    STARTED,
  }

  /**
   * Defines Factory class for classes inherited from ConnectionManager.
   * @author najtmar
   *
   * @param <T> the class whose instances should be produced
   */
  public static interface Factory<T extends ConnectionManager> {

    /**
     * Creates an instance of class T.
     * @return a new instance of class T
     * @throws ServerException when class instance creation fails
     */
    public T create() throws ServerException;

  }

  /**
   * Initialized the instance of ConnectionManager .
   * @throws ServerException when ConnectionManager initialization fails
   */
  public void initialize() throws ServerException;

  /**
   * Returns the State of the ConnectionManager .
   * @return the State of the ConnectionManager
   */
  public State getState();

  /**
   * Establishes connections between Clients and a Server.
   * @param gameSession the GameSession with a Game for which the connection will be established
   * @return returns Clients-Server connections established, or null if the connections could not by established
   * @throws ServerException when connection Channels cannot be established
   */
  public Channel[] establishConnectionChannels(@Nonnull GameSession gameSession) throws ServerException;

}
