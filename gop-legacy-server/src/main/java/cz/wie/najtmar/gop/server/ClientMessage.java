package cz.wie.najtmar.gop.server;

import com.google.common.base.Objects;

import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.io.Message;

import java.io.StringReader;
import java.util.UUID;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

/**
 * Message from a Client to a Server .
 * @author najtmar
 */
public class ClientMessage {

  /**
   * Special value of the id, which denotes that it's not set.
   */
  public static final int ID_NOT_SET = -1;

  /**
   * Type of a ClientMessage .
   * @author najtmar
   */
  public enum Type {
    /**
     * Registering local Client to initialize the ConnectionManager .
     */
    REGISTER_LOCAL_CLIENT,
    /**
     * Joining a Game.
     */
    JOIN_GAME,
    /**
     * Quitting a Game, e.g., when the connection is lost and the User does not want to wait for it to be reestablished.
     */
    FORCE_QUIT_GAME,
    /**
     * Leaves a Game, before is has started.
     */
    LEAVE_GAME_INTENT,
    /**
     * Confirming a Game with pending initialization.
     */
    INITIALIZE_GAME,
    /**
     * Canceling a Game with pending initialization.
     */
    CANCEL_GAME_INTENT,
    /**
     * Requests a list of pending Games that a User can join.
     */
    GET_PENDING_GAMES,
    /**
     * Sending a Message .
     */
    SEND_MESSAGE,
    /**
     * Response to a Message sent from the Server .
     */
    SERVER_MESSAGE_RESPONSE,
    /**
     * Heartbeat Message send to the Server .
     */
    HEARTBEAT,
    /**
     * Reconnecting a User as a Player of a Game .
     */
    RECONNECT_PLAYER,
    /**
     * Requests a list of Games that match the request, so that the User can reconnect.
     */
    GET_MATCHING_GAMES,
  }

  /**
   * Type of the ClientMessage .
   */
  private Type type;

  /**
   * Serialized body of the ClientMessage .
   */
  private String serializedBody;

  /**
   * Hash code of the ClientMessage .
   */
  private int hashCode;

  /**
   * Incremental id of the ServerMessage .
   */
  private int id;

  /**
   * Creates a REGISTER_LOCAL_CLIENT message .
   * @param localUserId the id of the local User to register
   * @param localUserName the name of the local User
   * @return an instance of a REGISTER_LOCAL_CLIENT message
   */
  public static ClientMessage createRegisterLocalClient(@Nonnull UUID localUserId, @Nonnull String localUserName) {
    return new ClientMessage(Type.REGISTER_LOCAL_CLIENT, Json.createObjectBuilder()
        .add("localUserId", localUserId.toString())
        .add("localUserName", localUserName)
        .build());
  }

  /**
   * Creates a JOIN_GAME message .
   * @param gameId the id of the Game to join
   * @param userId the id of the User to join the Game
   * @param userName the name of the User
   * @return an instance of a JOIN_GAME message
   */
  public static ClientMessage createJoinGame(@Nonnull UUID gameId, @Nonnull UUID userId, @Nonnull String userName) {
    return new ClientMessage(Type.JOIN_GAME, Json.createObjectBuilder()
        .add("gameId", gameId.toString())
        .add("userId", userId.toString())
        .add("userName", userName)
        .build());
  }

  /**
   * Creates a FORCE_QUIT_GAME message .
   * @param gameId the id of the Game to force quit
   * @param userId the id of the User to force quit the Game
   * @return an instance of a FORCE_QUIT_GAME message
   */
  public static ClientMessage createForceQuitGame(@Nonnull UUID gameId, @Nonnull UUID userId) {
    return new ClientMessage(Type.FORCE_QUIT_GAME, Json.createObjectBuilder()
        .add("gameId", gameId.toString())
        .add("userId", userId.toString())
        .build());
  }

  /**
   * Creates a LEAVE_GAME_INTENT message .
   * @param gameId the id of the Game whose intent should be left
   * @param userId the id of the User to leave the Game intent
   * @return an instance of a LEAVE_GAME_INTENT message
   */
  public static ClientMessage createLeaveGameIntent(@Nonnull UUID gameId, @Nonnull UUID userId) {
    return new ClientMessage(Type.LEAVE_GAME_INTENT, Json.createObjectBuilder()
        .add("gameId", gameId.toString())
        .add("userId", userId.toString())
        .build());
  }

  /**
   * Creates a GET_PENDING_GAMES message .
   * @param userId the id of the User requesting the Games
   * @return an instance of a GET_PENDING_GAMES message
   */
  public static ClientMessage createGetPendingGames(@Nonnull UUID userId) {
    return new ClientMessage(Type.GET_PENDING_GAMES, Json.createObjectBuilder()
        .add("userId", userId.toString())
        .build());
  }

  /**
   * Creates an INITIALIZE_GAME message .
   * @param gameId the id of the Game to initialize
   * @param userId the id of the User to initialize the Game
   * @return an instance of an INITIALIZE_GAME message
   */
  public static ClientMessage createInitializeGame(@Nonnull UUID gameId, @Nonnull UUID userId) {
    return new ClientMessage(Type.INITIALIZE_GAME, Json.createObjectBuilder()
        .add("gameId", gameId.toString())
        .add("userId", userId.toString())
        .build());
  }

  /**
   * Creates an CANCEL_GAME_INTENT message .
   * @param gameId the id of the Game whose intent should be canceled
   * @param userId the id of the User to cancel the Game intent
   * @return an instance of an CANCEL_GAME_INTENT message
   */
  public static ClientMessage createCancelGameIntent(@Nonnull UUID gameId, @Nonnull UUID userId) {
    return new ClientMessage(Type.CANCEL_GAME_INTENT, Json.createObjectBuilder()
        .add("gameId", gameId.toString())
        .add("userId", userId.toString())
        .build());
  }

  /**
   * Creates a SEND_MESSAGE message .
   * @param gameId the id of the Game where the Message is sent to
   * @param userId the id of the User sending the Message
   * @return an instance of a SEND_MESSAGE message
   */
  public static ClientMessage createSendMessage(@Nonnull UUID gameId, @Nonnull UUID userId,
      @Nonnull Message message) {
    final JsonArrayBuilder eventsArrayBuilder = Json.createArrayBuilder();
    for (Event event : message.getEvents()) {
      eventsArrayBuilder.add(event.getBody());
    }
    return new ClientMessage(Type.SEND_MESSAGE, Json.createObjectBuilder()
        .add("gameId", gameId.toString())
        .add("userId", userId.toString())
        .add("hashCode", message.hashCode())
        .add("events", eventsArrayBuilder.build())
        .build());
  }

  /**
   * Creates a SERVER_MESSAGE_RESPONSE message .
   * @param gameId the id of the Game where the Message is sent to
   * @param userId the id of the User sending the Message
   * @param hashCode the identifier of the Message being responded
   * @return an instance of a SERVER_MESSAGE_RESPONSE message
   */
  public static ClientMessage createServerMessageResponse(@Nonnull UUID gameId, @Nonnull UUID userId,
      @Nonnull int hashCode) {
    return new ClientMessage(Type.SERVER_MESSAGE_RESPONSE, Json.createObjectBuilder()
        .add("gameId", gameId.toString())
        .add("userId", userId.toString())
        .add("hashCode", hashCode)
        .build());
  }

  /**
   * Creates a RECONNECT_PLAYER message .
   * @param gameId the id of the Game to reconnect to
   * @param userId the id of the User to act as the Player to reconnect
   * @param hostString the hostString of the address the User tries to connect to
   * @return an instance of a RECONNECT_PLAYER message
   */
  public static ClientMessage createReconnectPlayer(@Nonnull UUID gameId, @Nonnull UUID userId,
      @Nonnull String hostString) {
    return new ClientMessage(Type.RECONNECT_PLAYER, Json.createObjectBuilder()
        .add("gameId", gameId.toString())
        .add("userId", userId.toString())
        .add("hostString", hostString)
        .build());
  }

  /**
   * Creates a HEARTBEAT message.
   * @param id the id of the HEARTBEAT message
   * @param gameId the id of the Game for which the Message is sent, or null if no Game is specified
   * @param userId the id of the User sending the HEARTBEAT Message
   * @param lastMessageId the highest ServerMessage id that has been received and processed
   * @return an instance of a HEARTBEAT message
   */
  public static ClientMessage createHeartbeat(@Nonnegative int id, @Nullable UUID gameId, @Nonnull UUID userId,
      @Nonnegative int lastMessageId) {
    final JsonObjectBuilder messageObjectBuilder = Json.createObjectBuilder()
        .add("id", id);
    if (gameId != null) {
      messageObjectBuilder.add("gameId", gameId.toString());
    }
    messageObjectBuilder.add("userId", userId.toString());
    messageObjectBuilder.add("lastMessageId", lastMessageId);
    return new ClientMessage(Type.HEARTBEAT, messageObjectBuilder.build());
  }

  /**
   * Creates a GET_MATCHING_GAMES message.
   * @param gameId the id of the Game looked for
   * @param userId the id of the User looking for Games
   * @return an instance of a GET_MATCHING_GAMES message
   */
  public static ClientMessage createGetMatchingGames(@Nonnull UUID gameId, @Nonnull UUID userId) {
    return new ClientMessage(Type.GET_MATCHING_GAMES, Json.createObjectBuilder()
        .add("gameIdHashCode", gameId.hashCode())
        .add("userIdHashCode", userId.hashCode())
        .build());
  }

  public Type getType() {
    return type;
  }

  public JsonObject getBody() {
    return Json.createReader(new StringReader(serializedBody)).readObject();
  }

  /**
   * Constructor.
   * @param type the Type of the ClientMessage
   * @param body the Body of the ClientMessage
   */
  private ClientMessage(@Nonnull Type type, @Nonnull JsonObject body) {
    this.type = type;
    this.serializedBody = body.toString();
    this.hashCode = Objects.hashCode(this.type, this.serializedBody);
    this.id = ID_NOT_SET;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  /**
   * For KryoNet serialization.
   */
  private ClientMessage() {}

  @Override
  public int hashCode() {
    return hashCode;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof ClientMessage)) {
      return false;
    }
    final ClientMessage otherMessage = (ClientMessage) obj;
    return type.equals(otherMessage.type) && serializedBody.equals(otherMessage.serializedBody);
  }

  @Override
  public String toString() {
    return "ClientMessage [type=" + type + ", serializedBody=" + serializedBody + ", hashCode="
        + hashCode + ", id=" + id + "]";
  }

}
