package cz.wie.najtmar.gop.server;

import com.google.common.base.Objects;

import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.game.Game;
import cz.wie.najtmar.gop.io.Message;

import java.io.StringReader;
import java.util.List;
import java.util.UUID;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;

/**
 * Message from a Server to a Client .
 * @author najtmar
 */
public class ServerMessage {

  /**
   * Special value of the id, which denotes that it's not set.
   */
  public static final int ID_NOT_SET = -1;

  /**
   * Type of a ServerMessage .
   * @author najtmar
   */
  public enum Type {
    /**
     * Response to the JOIN_GAME Request .
     */
    JOIN_GAME_RESPONSE,
    /**
     * Response to the INITIALIZE_GAME Request .
     */
    INITIALIZE_GAME_RESPONSE,
    /**
     * Response to the CANCEL_GAME_INTENT Request .
     */
    CANCEL_GAME_INTENT_RESPONSE,
    /**
     * Response to the LEAVE_GAME_INTENT Request.
     */
    LEAVE_GAME_INTENT_RESPONSE,
    /**
     * Response to the GET_PENDING_GAMES Request .
     */
    GET_PENDING_GAMES_RESPONSE,
    /**
     * Requesting a User to start a Game .
     */
    START_GAME,
    /**
     * Sending a Message .
     */
    SEND_MESSAGE,
    /**
     * Response to a Message sent from the Client .
     */
    CLIENT_MESSAGE_RESPONSE,
    /**
     * Requests a User to send an introductory ADD_PLAYER Message .
     */
    REQUEST_ADD_PLAYER,
    /**
     * Notifies whether the local Game can start or not.
     */
    OWN_GAME_START_ENABLED_STATUS,
    /**
     * Response to a HEARTBEAT Message sent from the Client .
     */
    HEARTBEAT_RESPONSE,
    /**
     * Response to a RECONNECT_PLAYER Message sent from the Client .
     */
    RECONNECT_PLAYER_RESPONSE,
    /**
     * Response to the GET_MATCHING_GAMES Request .
     */
    GET_MATCHING_GAMES_RESPONSE,
  }

  /**
   * Type of the ServerMessage .
   */
  private Type type;

  /**
   * Serialized body of the ServerMessage .
   */
  private String serializedBody;

  /**
   * Hash code of the ServerMessage .
   */
  private int hashCode;

  /**
   * Incremental id of the ServerMessage .
   */
  private int id;

  /**
   * Creates a JOIN_GAME_RESPONSE message .
   * @param gameId the id of the Game to join
   * @param userId the id of the User to join the Game
   * @param result the result of the operation
   * @return an instance of a JOIN_GAME_RESPONSE message
   */
  public static ServerMessage createJoinGameResponse(@Nonnull UUID gameId, @Nonnull UUID userId, boolean result) {
    return new ServerMessage(Type.JOIN_GAME_RESPONSE, Json.createObjectBuilder()
        .add("gameId", gameId.toString())
        .add("userId", userId.toString())
        .add("result", result)
        .build());
  }

  /**
   * Creates an INITIALIZE_GAME_RESPONSE message .
   * @param gameId the id of the Game to initialize
   * @param userId the id of the User to initialize the Game
   * @param result the result of the operation
   * @return an instance of an INITIALIZE_GAME_RESPONSE message
   */
  public static ServerMessage createInitializeGameResponse(@Nonnull UUID gameId, @Nonnull UUID userId, boolean result) {
    return new ServerMessage(Type.INITIALIZE_GAME_RESPONSE, Json.createObjectBuilder()
        .add("gameId", gameId.toString())
        .add("userId", userId.toString())
        .add("result", result)
        .build());
  }

  /**
   * Creates an CANCEL_GAME_INTENT_RESPONSE message .
   * @param gameId the id of the Game whose intent should be canceled
   * @param userId the id of the User to cancel the Game intent
   * @param result the result of the operation
   * @return an instance of an CANCEL_GAME_INTENT_RESPONSE message
   */
  public static ServerMessage createCancelGameIntentResponse(@Nonnull UUID gameId, @Nonnull UUID userId,
      boolean result) {
    return new ServerMessage(Type.CANCEL_GAME_INTENT_RESPONSE, Json.createObjectBuilder()
        .add("gameId", gameId.toString())
        .add("userId", userId.toString())
        .add("result", result)
        .build());
  }

  /**
   * Creates an LEAVE_GAME_INTENT_RESPONSE message .
   * @param gameId the id of the Game whose intent should be left
   * @param userId the id of the User to leave the Game intent
   * @param result the result of the operation
   * @return an instance of an LEAVE_GAME_INTENT_RESPONSE message
   */
  public static ServerMessage createLeaveGameIntentResponse(@Nonnull UUID gameId, @Nonnull UUID userId,
      boolean result) {
    return new ServerMessage(Type.LEAVE_GAME_INTENT_RESPONSE, Json.createObjectBuilder()
        .add("gameId", gameId.toString())
        .add("userId", userId.toString())
        .add("result", result)
        .build());
  }

  /**
   * Creates a GET_PENDING_GAMES_RESPONSE message .
   * @param pendingGameSessions an array of pending GameSessions
   * @param creatorName the name of the User that initiated the Game
   * @return an instance of a GET_PENDING_GAMES_RESPONSE message
   */
  public static ServerMessage createGetPendingGamesResponse(@Nonnull GameSession[] pendingGameSessions,
      String creatorName) {
    final JsonArrayBuilder pendingGamesArrayBuilder = Json.createArrayBuilder();
    for (GameSession gameSession : pendingGameSessions) {
      final Game game = gameSession.getGame();
      pendingGamesArrayBuilder.add(Json.createObjectBuilder()
          .add("uuid", game.getId().toString())
          .add("timestamp", game.getTimestamp())
          .add("creatorName", creatorName)
          .build());
    }
    return new ServerMessage(Type.GET_PENDING_GAMES_RESPONSE, Json.createObjectBuilder()
        .add("pendingGames", pendingGamesArrayBuilder.build())
        .build());
  }

  /**
   * Creates a START_GAME message .
   * @param gameId the id of the Game to start
   * @param userId the id of the User to start the Game
   * @return an instance of a START_GAME message
   */
  public static ServerMessage createStartGame(@Nonnull UUID gameId, @Nonnull UUID userId) {
    return new ServerMessage(Type.START_GAME, Json.createObjectBuilder()
        .add("gameId", gameId.toString())
        .add("userId", userId.toString())
        .build());
  }

  /**
   * Creates a SEND_MESSAGE message .
   * @param gameId the id of the Game where the Message is sent to
   * @param userId the id of the User where the Message is sent to
   * @return an instance of a SEND_MESSAGE message
   */
  public static ServerMessage createSendMessage(@Nonnull UUID gameId, @Nonnull UUID userId,
      @Nonnull Message message) {
    final JsonArrayBuilder eventsArrayBuilder = Json.createArrayBuilder();
    for (Event event : message.getEvents()) {
      eventsArrayBuilder.add(event.getBody());
    }
    return new ServerMessage(Type.SEND_MESSAGE, Json.createObjectBuilder()
        .add("gameId", gameId.toString())
        .add("userId", userId.toString())
        .add("hashCode", message.hashCode())
        .add("events", eventsArrayBuilder.build())
        .build());
  }

  /**
   * Creates a CLIENT_MESSAGE_RESPONSE message .
   * @param hashCode the identifier of the Message being responded
   * @param accepted whether the Message has been accepted or not
   * @return an instance of a CLIENT_MESSAGE_RESPONSE message
   */
  public static ServerMessage createClientMessageResponse(@Nonnull int hashCode, boolean accepted) {
    return new ServerMessage(Type.CLIENT_MESSAGE_RESPONSE, Json.createObjectBuilder()
        .add("hashCode", hashCode)
        .add("accepted", accepted)
        .build());
  }

  /**
   * Creates a REQUEST_ADD_PLAYER message .
   * @param gameId the id of the Game where the message is sent to
   * @param userId the id of the User requested
   * @param index the index of the Player
   * @return an instance of a REQUEST_ADD_PLAYER message
   */
  public static ServerMessage createRequestAddPlayer(@Nonnull UUID gameId, @Nonnull UUID userId,
      @Nonnegative int index) {
    return new ServerMessage(Type.REQUEST_ADD_PLAYER, Json.createObjectBuilder()
        .add("gameId", gameId.toString())
        .add("userId", userId.toString())
        .add("index", index)
        .build());
  }

  /**
   * Creates an OWN_GAME_START_ENABLED_STATUS message .
   * @param isEnabled whether starting a local Game is enabled or not
   * @param participants list of Users that have joined the Game
   * @return an instance of an OWN_GAME_START_ENABLED_STATUS message
   */
  public static ServerMessage createOwnGameStartEnabledStatus(boolean isEnabled, @Nonnull List<String> participants) {
    final JsonArrayBuilder participantsArrayBuilder = Json.createArrayBuilder();
    for (String participant : participants) {
      participantsArrayBuilder.add(participant);
    }
    return new ServerMessage(Type.OWN_GAME_START_ENABLED_STATUS, Json.createObjectBuilder()
        .add("isEnabled", isEnabled)
        .add("participants", participantsArrayBuilder.build())
        .build());
  }

  /**
   * Creates a HEARTBEAT_RESPONSE message .
   * @param id the id of the HEARTBEAT message to respond to
   * @param gamePresent true if the Game queried is present, false otherwise; always true if no Game is queried
   * @param gameUndecided true if the Game is finished and its state is UNDECIDED
   * @param unresponsiveParticipants specifies the participants of the queried Game that are unresponsive, or null
   *     if no Game was specified in the query
   * @param lastMessageId the highest ClientMessage id that has been received and processed in the Game from
   *     the User
   * @return an instance of a HEARTBEAT_RESPONSE message
   */
  public static ServerMessage createHeartbeatResponse(@Nonnegative int id, boolean gamePresent,
      boolean gameUndecided, @Nullable List<UUID> unresponsiveParticipants, @Nonnegative int lastMessageId) {
    final JsonArrayBuilder unresponsiveParticipantsArrayBuilder = Json.createArrayBuilder();
    if (unresponsiveParticipants != null) {
      for (UUID unresponsiveParticipant : unresponsiveParticipants) {
        unresponsiveParticipantsArrayBuilder.add(unresponsiveParticipant.toString());
      }
    }
    return new ServerMessage(Type.HEARTBEAT_RESPONSE, Json.createObjectBuilder()
        .add("id", id)
        .add("gamePresent", gamePresent)
        .add("gameUndecided", gameUndecided)
        .add("unresponsiveParticipants", unresponsiveParticipantsArrayBuilder.build())
        .add("lastMessageId", lastMessageId)
        .build());
  }

  /**
   * Creates a RECONNECT_PLAYER_RESPONSE message .
   * @param gameId the id of the Game that the User intended to reconnect
   * @param userId the id of the User that intended to reconnect the Game
   * @param hostString the hostString of the address the User tries to connect to
   * @param result the result of the operation
   * @return an instance of a RECONNECT_PLAYER_RESPONSE message
   */
  public static ServerMessage createReconnectPlayerResponse(@Nonnull UUID gameId, @Nonnull UUID userId,
      @Nonnull String hostString, boolean result) {
    return new ServerMessage(Type.RECONNECT_PLAYER_RESPONSE, Json.createObjectBuilder()
        .add("gameId", gameId.toString())
        .add("userId", userId.toString())
        .add("hostString", hostString)
        .add("result", result)
        .build());
  }

  /**
   * Creates a GET_MATCHING_GAMES_RESPONSE message .
   * @param gameSessions an array of matching GameSessions
   * @return an instance of a GET_MATCHING_GAMES_RESPONSE message
   */
  public static ServerMessage createGetMatchingGamesResponse(@Nonnull GameSession[] gameSessions) {
    final JsonArrayBuilder gamesArrayBuilder = Json.createArrayBuilder();
    for (GameSession gameSession : gameSessions) {
      final Game game = gameSession.getGame();
      gamesArrayBuilder.add(Json.createObjectBuilder()
          .add("uuid", game.getId().toString())
          .build());
    }
    return new ServerMessage(Type.GET_MATCHING_GAMES_RESPONSE, Json.createObjectBuilder()
        .add("games", gamesArrayBuilder.build())
        .build());
  }

  /**
   * Constructor.
   * @param type the Type of the ServerMessage
   * @param body the Body of the ServerMessage
   */
  private ServerMessage(@Nonnull Type type, @Nonnull JsonObject body) {
    this.type = type;
    this.serializedBody = body.toString();
    this.hashCode = Objects.hashCode(this.type, this.serializedBody);
    this.id = ID_NOT_SET;
  }

  /**
   * For KryoNet serialization.
   */
  private ServerMessage() {}

  public Type getType() {
    return type;
  }

  public JsonObject getBody() {
    return Json.createReader(new StringReader(serializedBody)).readObject();
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  @Override
  public int hashCode() {
    return hashCode;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof ServerMessage)) {
      return false;
    }
    final ServerMessage otherServerMessage = (ServerMessage) obj;
    return type.equals(otherServerMessage.type) && serializedBody.equals(otherServerMessage.serializedBody);
  }

  @Override
  public String toString() {
    return "ServerMessage [type=" + type + ", serializedBody=" + serializedBody + ", hashCode="
        + hashCode + ", id=" + id + "]";
  }

}
