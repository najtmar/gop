package cz.wie.najtmar.gop.server;

import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.event.EventProcessingException;
import cz.wie.najtmar.gop.event.SerializationException;
import cz.wie.najtmar.gop.game.EventGeneratingExecutor;
import cz.wie.najtmar.gop.game.GameException;
import cz.wie.najtmar.gop.io.Channel;
import cz.wie.najtmar.gop.io.Message;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.Nonnull;

/**
 * Responsible for communication with clients via Channels. Only communicates with clients when an Event to be sent is
 * interruptive. Otherwise, keeps Events in a cache to be sent later.
 * @author najtmar
 */
public class ClientCommunicator implements EventGeneratingExecutor {

  /**
   * State of InteractiveEventExecutor.
   */
  private State state;

  /**
   * The Channel used for communication with the client.
   */
  private final Channel clientChannel;

  /**
   * Processes Events before sending them to clientChannel .
   */
  private final EventGeneratingExecutor eventProcessor;

  /**
   * Interactive Events read from the client.
   */
  private final List<Event> interactiveEvents = new LinkedList<>();

  /**
   * The instance of the Factory class that which created the Communicator.
   */
  private final Factory factory;

  /**
   * Whether a Message should be sent to the client and an answer should be be expected.
   */
  private boolean communicate;

  /**
   * Factory to create instances of class ClientCommunicator . Also keeps a cache of Event to be sent later.
   * @author najtmar
   */
  public static class Factory implements EventGeneratingExecutor.Factory<ClientCommunicator> {

    final Channel clientChannel;
    final EventGeneratingExecutor.Factory<?> eventProcessorFactory;

    /**
     * Cache of Events to be sent to the Client.
     */
    final List<Event> eventCache;

    /**
     * Constructor.
     * @param clientChannel the Channel used for communication with the client
     * @param eventProcessorFactory factory for the Event processor to process Event before sending them to
     *     clientChannel
     */
    public Factory(@Nonnull Channel clientChannel, @Nonnull EventGeneratingExecutor.Factory<?> eventProcessorFactory) {
      this.clientChannel = clientChannel;
      this.eventProcessorFactory = eventProcessorFactory;
      this.eventCache = new LinkedList<>();
    }

    @Override
    public ClientCommunicator create() throws GameException {
      return new ClientCommunicator(clientChannel, eventProcessorFactory.create(), this);
    }

    public List<Event> getEventCache() {
      return eventCache;
    }

  }

  /**
   * Constructor.
   * @param clientChannel the Channel used for communication with the client
   * @param eventProcessor processing Event before sending them to clientChannel
   * @param factory the instance of the Factory class that which created the Communicator
   */
  private ClientCommunicator(@Nonnull Channel clientChannel, @Nonnull EventGeneratingExecutor eventProcessor,
      @Nonnull Factory factory) {
    this.clientChannel = clientChannel;
    this.eventProcessor = eventProcessor;
    this.factory = factory;
    this.state = State.NOT_PREPARED;
  }

  @Override
  public State getState() {
    return state;
  }

  @Override
  public void prepare(List<Event> inputEvents) throws GameException, EventProcessingException {
    if (state != State.NOT_PREPARED) {
      throw new GameException("prepare() can only be executed in the NOT_PREPARED State (found " + state + ").");
    }
    communicate = false;
    for (Event event : inputEvents) {
      try {
        if (event.isInterrupt()) {
          communicate = true;
          break;
        }
      } catch (SerializationException exception) {
        throw new EventProcessingException("Processing " + event + " failed.");
      }
    }
    final List<Event> eventCache = factory.getEventCache();
    eventCache.addAll(inputEvents);

    if (communicate) {
      eventProcessor.prepare(new LinkedList<>(eventCache));
      eventProcessor.executeAndGenerate();
      final List<Event> processedEvents = eventProcessor.getEventsGenerated();
      final Event[] eventsToBeSent = processedEvents.toArray(new Event[]{});
      try {
        final Message message = new Message(eventsToBeSent);
        eventCache.clear();
        clientChannel.writeMessage(message);
      } catch (InterruptedException exception) {
        throw new GameException("Events failed to be sent to the client Channel: " + eventsToBeSent);
      }
    }
    state = State.PREPARED;
  }

  @Override
  public void executeAndGenerate() throws GameException {
    if (state != State.PREPARED) {
      throw new GameException("executeAndGenerate() can only be executed in the PREPARED State (found " + state + ").");
    }
    if (communicate) {
      try {
        interactiveEvents.addAll(Arrays.asList(clientChannel.readMessage().getEvents()));
      } catch (InterruptedException exception) {
        throw new GameException("Error reading Events from the client Channel.");
      }
    }
    state = State.GENERATED;
  }

  @Override
  public List<Event> getEventsGenerated() throws GameException {
    if (state != State.GENERATED) {
      throw new GameException("getEventsGenerated() can only be executed in the GENERATED State (found " + state
          + ").");
    }
    return interactiveEvents;
  }

}
