package cz.wie.najtmar.gop.server;

import com.google.inject.Inject;
import com.google.inject.name.Named;

import cz.wie.najtmar.gop.board.BoardException;
import cz.wie.najtmar.gop.board.BoardGenerator;
import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.event.EventProcessingException;
import cz.wie.najtmar.gop.event.SerializationException;
import cz.wie.najtmar.gop.game.BattleEventGeneratingExecutor;
import cz.wie.najtmar.gop.game.CityProductionEventGeneratingExecutor;
import cz.wie.najtmar.gop.game.EventFactory;
import cz.wie.najtmar.gop.game.EventGeneratingExecutor;
import cz.wie.najtmar.gop.game.Game;
import cz.wie.najtmar.gop.game.GameEvaluationEventGeneratingExecutor;
import cz.wie.najtmar.gop.game.GameException;
import cz.wie.najtmar.gop.game.InteractiveEventExecutor;
import cz.wie.najtmar.gop.game.MoveActionEventGeneratingExecutor;
import cz.wie.najtmar.gop.game.ObjectSerializer;
import cz.wie.najtmar.gop.game.Player;
import cz.wie.najtmar.gop.game.Prawn;
import cz.wie.najtmar.gop.game.PropertiesReader;
import cz.wie.najtmar.gop.game.SettlersUnit;
import cz.wie.najtmar.gop.game.User;
import cz.wie.najtmar.gop.game.VisibilityManager;
import cz.wie.najtmar.gop.game.Warrior;
import cz.wie.najtmar.gop.io.Channel;
import java8.util.stream.StreamSupport;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import javax.annotation.Nonnull;
import javax.json.JsonObject;

/**
 * A Session of the Game, started in a Server.
 * @author najtmar
 */
public class GameSession extends Thread {

  /**
   * The delta by which the Game time is increased in each round.
   */
  private long gameTimeDelta;

  /**
   * PropertiesReader instance used to read server-side Properties.
   */
  private final PropertiesReader propertiesReader;

  /**
   * BoardGenerator used in the Games started by the Server.
   */
  private final BoardGenerator boardGenerator;

  /**
   * Used to deploy Prawns on the Board at the beginning of the Game.
   */
  private final InitialPrawnDeployer prawnDeployer;

  /**
  * Creates the ConnectionManager used to manage connections.
  */
  private final ConnectionManager.Factory<?> connectionManagerFactory;

  /**
   * The Random seed used in the Server.
   */
  private final long seed;

  /**
   * The Game of the Session.
   */
  private Game game;

  /**
   * User for object serialization.
   */
  private ObjectSerializer objectSerializer;

  /**
   * Used for Event creation.
   */
  private EventFactory eventFactory;

  /**
   * Factories for a workflow of executors used in a Game.
   */
  private EventGeneratingExecutor.Factory<?>[][] executorFactories;

  /**
   * Factories of objects used to communicate with clients. Also takes part of executorFactories;
   */
  private ClientCommunicator.Factory[] clientCommunicatorFactories;

  /**
   * The most recent Events to be sent to the next Executors .
   */
  private List<Event> lastEvents;

  /**
   * Channels used to the communication with clients. Channel with index i is connected with the i-th client.
   */
  private Channel[] channels;

  /**
   * Constructor.
   * @param propertiesReader used to read Properties used in the Game
   * @param boardGenerator used to generate Boards
   * @param prawnDeployer used to deploy first Prawns on the Board at the Game
   * @param connectionManagerFactory used to create ConnectionManagers for the Game
   * @param seed the Random seed used in the Server
   */
  @SuppressWarnings("rawtypes")  // Because of Guice bindings.
  @Inject
  GameSession(PropertiesReader propertiesReader, BoardGenerator boardGenerator,
      InitialPrawnDeployer prawnDeployer, ConnectionManager.Factory connectionManagerFactory,
      @Named("random seed") long seed) {
    this.propertiesReader = propertiesReader;
    this.boardGenerator = boardGenerator;
    this.prawnDeployer = prawnDeployer;
    this.connectionManagerFactory = connectionManagerFactory;
    this.seed = seed;
  }

  @Override
  public void run() {
    try {
      if (!initializeGame()) {
        return;
      }
      createClientInitializationEvents();
      initializeExecutorFactories();
    } catch (ServerException exception) {
      throw new RuntimeException("Initialization process failed.", exception);
    }

    try {
      game.start();
    } catch (GameException exception) {
      throw new RuntimeException("The Game failed to start.", exception);
    }

    // Initial client communication.
    {
      final List<Event> currentEvents = new LinkedList<>();
      final ClientCommunicator[] initialClientCommunicators =
          new ClientCommunicator[clientCommunicatorFactories.length];
      for (int i = 0; i < clientCommunicatorFactories.length; ++i) {
        try {
          initialClientCommunicators[i] = clientCommunicatorFactories[i].create();
        } catch (GameException exception) {
          throw new RuntimeException("ClientCommunicator " + i + " preparation failed.", exception);
        }
      }
      for (int i = 0; i < initialClientCommunicators.length; ++i) {
        try {
          initialClientCommunicators[i].prepare(lastEvents);
        } catch (GameException | EventProcessingException exception) {
          throw new RuntimeException("Event processing preparation failed.", exception);
        }
      }
      for (int i = 0; i < initialClientCommunicators.length; ++i) {
        try {
          initialClientCommunicators[i].executeAndGenerate();
          currentEvents.addAll(initialClientCommunicators[i].getEventsGenerated());
        } catch (GameException exception) {
          throw new RuntimeException("Event processing preparation failed.", exception);
        }
      }
      lastEvents = currentEvents;
    }

    while (game.getState() != Game.State.FINISHED) {
      // Executor creation.
      final EventGeneratingExecutor[][] executorWorkflow = StreamSupport.stream(
          Arrays.asList(executorFactories)).map(
              factoryArray -> StreamSupport.stream(Arrays.asList(factoryArray)).map(
                  factory -> {
                    try {
                      return factory.create();
                    } catch (GameException exception) {
                      throw new RuntimeException(exception);
                    }
                  }).toArray(size -> new EventGeneratingExecutor[size]))
          .toArray(size -> new EventGeneratingExecutor[size][]);

      for (EventGeneratingExecutor[] parallelExecutors : executorWorkflow) {
        // Event processing preparation.
        for (EventGeneratingExecutor executor : parallelExecutors) {
          try {
            executor.prepare(lastEvents);
          } catch (GameException | EventProcessingException exception) {
            throw new RuntimeException("Event processing preparation failed.", exception);
          }
        }
        // Event generation.
        final List<Event> currentEvents = new LinkedList<>();
        for (EventGeneratingExecutor executor : parallelExecutors) {
          try {
            executor.executeAndGenerate();
            currentEvents.addAll(executor.getEventsGenerated());
          } catch (GameException | EventProcessingException exception) {
            throw new RuntimeException("Event processing preparation failed.", exception);
          }
        }
        lastEvents = currentEvents;
      }
      notifyIterationFinished();
      try {
        game.advanceTimeTo(game.getTime() + gameTimeDelta);
      } catch (GameException exception) {
        throw new RuntimeException("Advancing time failed.", exception);
      }
    }
  }

  /**
   * Creates and initializes the Game, or changes its State to INTENT_ABORTED .
   * @return true iff the Game was successfully initialized
   * @throws ServerException when Game initialization fails
   */
  boolean initializeGame() throws ServerException {
    try {
      game = new Game(propertiesReader.getGameProperties());
      gameTimeDelta = game.getGameProperties().getTimeDelta();
    } catch (GameException | IOException exception) {
      throw new ServerException("Game could not be created.", exception);
    }

    // Board.
    try {
      game.setBoard(boardGenerator.generate());
    } catch (GameException | BoardException exception) {
      throw new ServerException("Failed to initialize a Game with a Board.", exception);
    }

    // Connections and Players.
    final ConnectionManager connectionManager = connectionManagerFactory.create();
    channels = connectionManager.establishConnectionChannels(this);
    if (channels == null) {
      try {
        game.abortIntent();
      } catch (GameException exception) {
        throw new ServerException("Failed to abort the Game intent.", exception);
      }
      return false;
    }
    for (int i = 0; i < channels.length; ++i) {
      final Event[] events;
      try {
        events = channels[i].readMessage().getEvents();
      } catch (InterruptedException exception) {
        throw new ServerException("Communication with client " + i + " failed.", exception);
      }
      if (events.length != 1 || events[0].getType() != Event.Type.ADD_PLAYER) {
        throw new ServerException("Exactly one Event of type ADD_PLAYER expected (found " + events + ").");
      }
      final Player player;
      try {
        player = deserializeNewPlayer(events[0].getBody().getJsonObject("player"));
      } catch (SerializationException exception) {
        throw new ServerException("Failed to deserialize Player from Event " + events[0], exception);
      }
      try {
        game.addPlayer(player);
      } catch (GameException exception) {
        throw new ServerException("Adding " + player + " to the Game failed.", exception);
      }
    }

    // Game initialization.
    try {
      game.initialize();
    } catch (GameException exception) {
      throw new ServerException("" + game + "could not be initialized.", exception);
    }
    prawnDeployer.deployPrawns(game);

    try {
      objectSerializer = new ObjectSerializer(game);
    } catch (GameException exception) {
      throw new ServerException("ObjectSerializer could not be initialized.", exception);
    }
    eventFactory = new EventFactory(objectSerializer);
    return true;
  }

  /**
   * Given an INITIALIZED Game, create and store in lastEvents initial Events to be sent to all clients.
   * @throws ServerException when creating initialization Events fails
   */
  void createClientInitializationEvents() throws ServerException {
    if (game == null) {
      throw new ServerException("Game hasn't been created.");
    }
    if (game.getState() != Game.State.INITIALIZED) {
      throw new ServerException("Game should be in state INITIALIZED ( found " + game.getState() + ").");
    }
    final Event[] events = new Event[1 + game.getPrawns().size()];
    try {
      events[0] = eventFactory.createInitializeGameEvent();
      int eventIndex = 1;
      for (Prawn prawn : game.getPrawns()) {
        if (prawn instanceof SettlersUnit) {
          events[eventIndex++] =
              eventFactory.createProduceSettlersUnitEvent(prawn.getId(), prawn.getCurrentPosition(),
                  prawn.getPlayer());
        } else if (prawn instanceof Warrior) {
          final Warrior warrior = (Warrior) prawn;
          events[eventIndex++] = eventFactory.createProduceWarriorEvent(warrior.getId(), warrior.getCurrentPosition(),
              warrior.getWarriorDirection(), warrior.getPlayer());
        } else {
          throw new ServerException("Unknown Prawn type: " + prawn);
        }
      }
    } catch (EventProcessingException exception) {
      throw new ServerException("Initial Event creation failed.", exception);
    }
    lastEvents = Arrays.asList(events);
  }

  /**
   * Deserializes new objects of type Player.
   * @param serialized serialized object of type Player
   * @return serialized deserialized to an object of type Player
   * @throws SerializationException when deserialization process fails
   */
  Player deserializeNewPlayer(@Nonnull JsonObject serialized) throws SerializationException {
    final JsonObject serializedUser = serialized.getJsonObject("user");
    return new Player(new User(serializedUser.getString("name"), UUID.fromString(serializedUser.getString("uuid"))),
        serialized.getInt("index"), game);
  }

  long getGameTimeDelta() {
    return gameTimeDelta;
  }

  public Game getGame() {
    return game;
  }

  ObjectSerializer getObjectSerializer() {
    return objectSerializer;
  }

  List<Event> getLastEvents() {
    return lastEvents;
  }

  /**
   * Given an INITIALIZED Game, initialized all EventGeneratingExecutorFactories used in the GameSession.
   * @throws ServerException when client initialization fails
   */
  void initializeExecutorFactories() throws ServerException {
    if (game.getState() != Game.State.INITIALIZED) {
      throw new ServerException("Game should be in state INITIALIZED ( found " + game.getState() + ").");
    }
    clientCommunicatorFactories = new ClientCommunicator.Factory[channels.length];
    for (int i = 0; i < clientCommunicatorFactories.length; ++i) {
      clientCommunicatorFactories[i] =
          new ClientCommunicator.Factory(channels[i], new VisibilityManager.Factory(game, eventFactory, i));
    }
    executorFactories = new EventGeneratingExecutor.Factory<?>[][]{
      new EventGeneratingExecutor.Factory<?>[] {
        new InteractiveEventExecutor.Factory(game, eventFactory),
      },
      new EventGeneratingExecutor.Factory<?>[] {
        new MoveActionEventGeneratingExecutor.Factory(game, eventFactory),
      },
      new EventGeneratingExecutor.Factory<?>[] {
        new BattleEventGeneratingExecutor.Factory(game, eventFactory, new Random(seed)),
      },
      new EventGeneratingExecutor.Factory<?>[] {
        new CityProductionEventGeneratingExecutor.Factory(game, eventFactory),
      },
      new EventGeneratingExecutor.Factory<?>[] {
        new GameEvaluationEventGeneratingExecutor.Factory(game, eventFactory),
      },
      clientCommunicatorFactories,
    };
  }

  /**
   * Invoked after each iteration of a GameSession. Used for synchronization purposes in tests.
   */
  public void notifyIterationFinished() {
  }

}
