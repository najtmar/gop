package cz.wie.najtmar.gop.server;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import cz.wie.najtmar.gop.board.Board;
import cz.wie.najtmar.gop.board.Joint;
import cz.wie.najtmar.gop.board.RoadSection;
import cz.wie.najtmar.gop.common.Point2D;
import cz.wie.najtmar.gop.game.Game;
import cz.wie.najtmar.gop.game.GameProperties;
import cz.wie.najtmar.gop.game.Player;
import cz.wie.najtmar.gop.game.SettlersUnit;
import cz.wie.najtmar.gop.game.User;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.UUID;

public class CrossBoardPrawnDeployerTest {

  static final long START_TIME = 966;
  static final int BOARD_X_SIZE = 15;
  static final int BOARD_Y_SIZE = 10;
  static final double STEP_SIZE = 1.0;

  Board board;
  Joint[][] jointArray;
  Game game;
  Player[] players;
  CrossBoardPrawnDeployer testDeployer;

  /**
   * Method setUp().
   * @throws Exception whe something goes wrong
   */
  @Before
  public void setUp() throws Exception {
    final Properties simpleBoardPropertiesSet = new Properties();
    simpleBoardPropertiesSet.load(CrossBoardPrawnDeployerTest.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/board/simple-board.properties"));
    board = new Board(simpleBoardPropertiesSet);
    List<Joint> jointList = new ArrayList<>();
    LinkedList<Integer> jointIndexList = new LinkedList<>();
    jointArray = new Joint[BOARD_X_SIZE][BOARD_Y_SIZE];
    for (int j = 0; j < BOARD_Y_SIZE; ++j) {
      for (int i = 0; i < BOARD_X_SIZE; ++i) {
        jointIndexList.add(j * BOARD_X_SIZE + i);
      }
    }
    Collections.shuffle(jointIndexList, new Random(42));
    int roadSectionIndex = 0;
    for (int j = 0; j < BOARD_Y_SIZE; ++j) {
      for (int i = 0; i < BOARD_X_SIZE; ++i) {
        final int jointIndex = jointIndexList.removeFirst();
        final Joint joint = (new Joint.Builder())
            .setBoard(board)
            .setIndex(jointIndex)
            .setName("joint" + jointIndex)
            .setCoordinatesPair(new Point2D.Double(i * STEP_SIZE, j * STEP_SIZE))
            .build();
        jointList.add(joint);
        jointArray[i][j] = joint;
        // A RoadSection must have at least 3 Positions.
        if (j >= 3) {
          board.addRoadSection((new RoadSection.Builder())
              .setBoard(board)
              .setIndex(roadSectionIndex++)
              .setJoints(jointArray[i][j - 3], joint)
              .build());
        }
      }
    }
    Collections.sort(jointList, (joint0, joint1) -> Integer.compare(joint0.getIndex(), joint1.getIndex()));
    for (Joint joint : jointList) {
      board.addJoint(joint);
    }

    final Properties simpleGamePropertiesSet = new Properties();
    simpleGamePropertiesSet.load(CrossBoardPrawnDeployerTest.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/game/simple-game.properties"));
    simpleGamePropertiesSet.setProperty(GameProperties.MAX_PLAYER_COUNT_PROPERTY_NAME, "10");
    simpleGamePropertiesSet.setProperty(GameProperties.START_TIME_PROPERTY_NAME, "" + START_TIME);
    game = new Game(simpleGamePropertiesSet);
    game.setBoard(board);
    players = new Player[10];
    for (int i = 0; i < players.length; ++i) {
      players[i] = new Player(new User("player" + i, new UUID(12345, i)), i, game);
      game.addPlayer(players[i]);
    }
    game.initialize();
    testDeployer = new CrossBoardPrawnDeployer();
  }

  @Test
  public void testCrossBoardSettlersUnitDeploymentWorks() throws Exception {
    testDeployer.deployPrawns(game);
    for (int i = 0; i < 10; ++i) {
      assertEquals(1, game.getPlayerPrawns(players[0]).size());
      final SettlersUnit settlersUnit = (SettlersUnit) game.getPlayerPrawns(players[0]).iterator().next();
      assertNotNull(settlersUnit.getCurrentPosition().getJoint());
      assertEquals(START_TIME, settlersUnit.getLastMoveTime());
    }
    assertEquals(
        new Point2D.Double(14.0, 9.0),
        game.getPlayerPrawns(players[0]).iterator().next().getCurrentPosition().getJoint().getCoordinatesPair());
    assertEquals(
        new Point2D.Double(0.0, 0.0),
        game.getPlayerPrawns(players[1]).iterator().next().getCurrentPosition().getJoint().getCoordinatesPair());
    assertEquals(
        new Point2D.Double(14.0, 0.0),
        game.getPlayerPrawns(players[2]).iterator().next().getCurrentPosition().getJoint().getCoordinatesPair());
    assertEquals(
        new Point2D.Double(0.0, 9.0),
        game.getPlayerPrawns(players[3]).iterator().next().getCurrentPosition().getJoint().getCoordinatesPair());
    assertEquals(
        new Point2D.Double(7.0, 0.0),
        game.getPlayerPrawns(players[4]).iterator().next().getCurrentPosition().getJoint().getCoordinatesPair());
    assertEquals(
        new Point2D.Double(7.0, 9.0),
        game.getPlayerPrawns(players[5]).iterator().next().getCurrentPosition().getJoint().getCoordinatesPair());
    assertEquals(
        new Point2D.Double(0.0, 5.0),
        game.getPlayerPrawns(players[6]).iterator().next().getCurrentPosition().getJoint().getCoordinatesPair());
    assertEquals(
        new Point2D.Double(14.0, 5.0),
        game.getPlayerPrawns(players[7]).iterator().next().getCurrentPosition().getJoint().getCoordinatesPair());
    assertEquals(
        new Point2D.Double(3.0, 1.0),
        game.getPlayerPrawns(players[8]).iterator().next().getCurrentPosition().getJoint().getCoordinatesPair());
    assertEquals(
        new Point2D.Double(2.0, 8.0),
        game.getPlayerPrawns(players[9]).iterator().next().getCurrentPosition().getJoint().getCoordinatesPair());
  }

}
