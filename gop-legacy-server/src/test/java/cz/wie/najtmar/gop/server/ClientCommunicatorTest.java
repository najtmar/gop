package cz.wie.najtmar.gop.server;

import static org.junit.Assert.assertEquals;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.game.EventGeneratingExecutor;
import cz.wie.najtmar.gop.game.ForwardingEventGeneratingExecutor;
import cz.wie.najtmar.gop.io.Channel;
import cz.wie.najtmar.gop.io.Message;

import org.apache.commons.collections4.ListUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import javax.json.Json;


public class ClientCommunicatorTest {

  static final long START_TIME = 966;

  List<Event> nonInterruptiveInputEvents;
  List<Event> interruptiveInputEvents;
  Event[] outputEvents;
  Channel mockedChannel;
  ForwardingEventGeneratingExecutor.Factory eventProcessorFactory;
  ClientCommunicator.Factory clientCommunicatorFactory;
  ClientCommunicator clientCommunicator;

  /**
   * Method setUp().
   * @throws Exception when something goes wrong
   */
  @Before
  public void setUp() throws Exception {
    nonInterruptiveInputEvents = Arrays.asList(
        new Event(
            Json.createObjectBuilder()
            .add("type", "TEST")
            .add("time", START_TIME)
            .add("value", "test0")
            .build()),
        new Event(
            Json.createObjectBuilder()
            .add("type", "TEST")
            .add("time", START_TIME)
            .add("value", "test1")
            .build()));
    interruptiveInputEvents = Arrays.asList(
        new Event(
            Json.createObjectBuilder()
            .add("type", "TEST")
            .add("time", START_TIME)
            .add("value", "test2")
            .build()),
        new Event(
            Json.createObjectBuilder()
            .add("type", "CHANGE_PLAYER_STATE")
            .add("time", START_TIME)
            .add("player", 2)
            .add("state", "LOST")
            .build()));
    outputEvents = new Event[] {
        new Event(
            Json.createObjectBuilder()
            .add("type", "TEST")
            .add("time", START_TIME + 10)
            .add("value", "test3")
            .build()),
        new Event(
            Json.createObjectBuilder()
            .add("type", "TEST")
            .add("time", START_TIME + 10)
            .add("value", "test4")
            .build()),
        new Event(
            Json.createObjectBuilder()
            .add("type", "TEST")
            .add("time", START_TIME + 10)
            .add("value", "test5")
            .build()),
        };
    mockedChannel = mock(Channel.class);
    eventProcessorFactory = new ForwardingEventGeneratingExecutor.Factory();
    clientCommunicatorFactory = new ClientCommunicator.Factory(mockedChannel, eventProcessorFactory);
    clientCommunicator = clientCommunicatorFactory.create();
  }

  @Test
  public void testEventsProperlyCommunicatedBothWays() throws Exception {
    when(mockedChannel.readMessage()).thenReturn(new Message(outputEvents));

    // Non-interruptive Events only.
    assertEquals(EventGeneratingExecutor.State.NOT_PREPARED, clientCommunicator.getState());
    clientCommunicator.prepare(nonInterruptiveInputEvents);
    assertEquals(EventGeneratingExecutor.State.PREPARED, clientCommunicator.getState());
    verify(mockedChannel, times(0)).writeMessage(any(Message.class));
    clientCommunicator.executeAndGenerate();
    assertEquals(EventGeneratingExecutor.State.GENERATED, clientCommunicator.getState());
    assertEquals(Arrays.asList(), clientCommunicator.getEventsGenerated());
    verify(mockedChannel, times(0)).readMessage();
    // Event processor not invoked.
    {
      final EventGeneratingExecutor eventProcessor = eventProcessorFactory.getLastInstanceCreated();
      assertEquals(EventGeneratingExecutor.State.NOT_PREPARED, eventProcessor.getState());
    }

    // Also interruptive Events .
    clientCommunicator = clientCommunicatorFactory.create();
    assertEquals(EventGeneratingExecutor.State.NOT_PREPARED, clientCommunicator.getState());
    clientCommunicator.prepare(interruptiveInputEvents);
    assertEquals(EventGeneratingExecutor.State.PREPARED, clientCommunicator.getState());
    verify(mockedChannel, times(1)).writeMessage(new Message(
        ListUtils.union(nonInterruptiveInputEvents, interruptiveInputEvents).toArray(new Event[]{})));
    clientCommunicator.executeAndGenerate();
    assertEquals(EventGeneratingExecutor.State.GENERATED, clientCommunicator.getState());
    verify(mockedChannel, times(1)).readMessage();
    {
      final List<Event> interactiveEvents = clientCommunicator.getEventsGenerated();
      assertEquals(3, interactiveEvents.size());
      assertEquals("test3", interactiveEvents.get(0).getBody().getString("value"));
      assertEquals("test4", interactiveEvents.get(1).getBody().getString("value"));
      assertEquals("test5", interactiveEvents.get(2).getBody().getString("value"));
    }
    // Event processor invoked.
    {
      final EventGeneratingExecutor eventProcessor = eventProcessorFactory.getLastInstanceCreated();
      assertEquals(EventGeneratingExecutor.State.GENERATED, eventProcessor.getState());
      assertEquals(ListUtils.union(nonInterruptiveInputEvents, interruptiveInputEvents),
          eventProcessor.getEventsGenerated());
    }

    // Input hasn't changed.
    assertEquals(2, nonInterruptiveInputEvents.size());
    assertEquals("test0", nonInterruptiveInputEvents.get(0).getBody().getString("value"));
    assertEquals("test1", nonInterruptiveInputEvents.get(1).getBody().getString("value"));
    assertEquals(2, interruptiveInputEvents.size());
    assertEquals("test2", interruptiveInputEvents.get(0).getBody().getString("value"));
    assertEquals("LOST", interruptiveInputEvents.get(1).getBody().getString("state"));

    // Interruptive again, no caching this time.
    clientCommunicator = clientCommunicatorFactory.create();
    assertEquals(EventGeneratingExecutor.State.NOT_PREPARED, clientCommunicator.getState());
    clientCommunicator.prepare(interruptiveInputEvents);
    assertEquals(EventGeneratingExecutor.State.PREPARED, clientCommunicator.getState());
    verify(mockedChannel, times(1)).writeMessage(new Message(interruptiveInputEvents.toArray(new Event[]{})));
    clientCommunicator.executeAndGenerate();
    assertEquals(EventGeneratingExecutor.State.GENERATED, clientCommunicator.getState());
    verify(mockedChannel, times(2)).readMessage();
    {
      final List<Event> interactiveEvents = clientCommunicator.getEventsGenerated();
      assertEquals(3, interactiveEvents.size());
      assertEquals("test3", interactiveEvents.get(0).getBody().getString("value"));
      assertEquals("test4", interactiveEvents.get(1).getBody().getString("value"));
      assertEquals("test5", interactiveEvents.get(2).getBody().getString("value"));
    }
    // Event processor invoked.
    {
      final EventGeneratingExecutor eventProcessor = eventProcessorFactory.getLastInstanceCreated();
      assertEquals(EventGeneratingExecutor.State.GENERATED, eventProcessor.getState());
      assertEquals(interruptiveInputEvents, eventProcessor.getEventsGenerated());
    }
  }

}
