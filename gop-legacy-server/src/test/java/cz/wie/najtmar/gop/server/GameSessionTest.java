package cz.wie.najtmar.gop.server;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.name.Names;

import cz.wie.najtmar.gop.board.Board;
import cz.wie.najtmar.gop.board.BoardException;
import cz.wie.najtmar.gop.board.BoardGenerator;
import cz.wie.najtmar.gop.board.Joint;
import cz.wie.najtmar.gop.board.RoadSection;
import cz.wie.najtmar.gop.common.Point2D;
import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.game.Game;
import cz.wie.najtmar.gop.game.GameException;
import cz.wie.najtmar.gop.game.Player;
import cz.wie.najtmar.gop.game.Prawn;
import cz.wie.najtmar.gop.game.PropertiesReader;
import cz.wie.najtmar.gop.game.SettlersUnit;
import cz.wie.najtmar.gop.game.SimplePropertiesReader;
import cz.wie.najtmar.gop.io.Channel;
import cz.wie.najtmar.gop.io.Message;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.io.IOException;
import java.io.StringReader;
import java.util.Arrays;
import java.util.UUID;

import javax.annotation.Nonnull;
import javax.json.Json;

public class GameSessionTest {

    static class TestBoardGenerator implements BoardGenerator {

        private final PropertiesReader propertiesReader;

        @Inject
        TestBoardGenerator(@Nonnull PropertiesReader propertiesReader) {
            this.propertiesReader = propertiesReader;
        }

        @Override
        public Board generate() throws BoardException {
            final Board board;
            try {
                board = new Board(propertiesReader.getBoardProperties());
            } catch (IOException exception) {
                throw new BoardException("Reading Board Properties failed.", exception);
            }
            Joint joint0 = (new Joint.Builder())
                    .setBoard(board)
                    .setIndex(0)
                    .setName("joint0")
                    .setCoordinatesPair(new Point2D.Double(0.0, 0.0))
                    .build();
            Joint joint1 = (new Joint.Builder())
                    .setBoard(board)
                    .setIndex(1)
                    .setName("joint1")
                    .setCoordinatesPair(new Point2D.Double(3.0, 0.0))
                    .build();
            Joint joint2 = (new Joint.Builder())
                    .setBoard(board)
                    .setIndex(2)
                    .setName("joint2")
                    .setCoordinatesPair(new Point2D.Double(0.0, 3.0))
                    .build();
            RoadSection roadSection0 = (new RoadSection.Builder())
                    .setBoard(board)
                    .setIndex(0)
                    .setJoints(joint0, joint1)
                    .build();
            RoadSection roadSection1 = (new RoadSection.Builder())
                    .setBoard(board)
                    .setIndex(1)
                    .setJoints(joint0, joint2)
                    .build();
            board.addJoint(joint0);
            board.addJoint(joint1);
            board.addJoint(joint2);
            board.addRoadSection(roadSection0);
            board.addRoadSection(roadSection1);
            return board;
        }

    }

    static class TestPrawnDeployer implements InitialPrawnDeployer {

        TestPrawnDeployer() {
        }

        @Override
        public void deployPrawns(Game game) throws ServerException {
            try {
                SettlersUnit settlersUnitA = (new SettlersUnit.Builder())
                        .setCreationTime(game.getTime())
                        .setCurrentPosition(game.getBoard().getJoints().get(0).getNormalizedPosition())
                        .setGameProperties(game.getGameProperties())
                        .setPlayer(game.getPlayers().get(0))
                        .build();
                SettlersUnit settlersUnitB = (new SettlersUnit.Builder())
                        .setCreationTime(game.getTime())
                        .setCurrentPosition(game.getBoard().getJoints().get(1).getNormalizedPosition())
                        .setGameProperties(game.getGameProperties())
                        .setPlayer(game.getPlayers().get(1))
                        .build();
                game.addPrawn(settlersUnitA);
                game.addPrawn(settlersUnitB);
            } catch (GameException exception) {
                throw new ServerException(exception);
            }
        }

    }

    /**
     * Test implementation of ConnectionManager . establishConnectionChannels() returns the Channels provided as input,
     * or null if the Channels array is empty.
     *
     * @author najtmar
     */
    static class TestConnectionManager implements ConnectionManager {

        private State state;

        private final Channel[] channels;

        static class Factory implements ConnectionManager.Factory<TestConnectionManager> {

            private final Channel[] channels;

            @Inject
            public Factory(Channel[] channels) {
                this.channels = channels;
            }

            @Override
            public TestConnectionManager create() throws ServerException {
                return new TestConnectionManager(channels);
            }

        }

        private TestConnectionManager(Channel[] channels) {
            this.state = State.NOT_INITIALIZED;
            this.channels = channels;
        }

        @Override
        public Channel[] establishConnectionChannels(GameSession unusedGameSession) {
            if (channels.length > 0) {
                return channels;
            } else {
                return null;
            }
        }

        @Override
        public void initialize() throws ServerException {
            state = State.INITIALIZED;
        }

        @Override
        public State getState() {
            return state;
        }

    }

    Channel[] channels;
    Event addPlayerEvent0;
    Event addPlayerEvent1;

    volatile Exception sleepException;

    GameSession testGameSession;

    private Module getTestModule() {
        return new AbstractModule() {

            @Override
            protected void configure() {
                bind(PropertiesReader.class).to(SimplePropertiesReader.class);
                bind(BoardGenerator.class).to(TestBoardGenerator.class);
                bind(InitialPrawnDeployer.class).to(TestPrawnDeployer.class);
                bind(ConnectionManager.Factory.class).to(TestConnectionManager.Factory.class);
                bind(Channel[].class).toInstance(channels);
                bind(Long.class).annotatedWith(Names.named("random seed")).toInstance(45L);
            }

        };
    }

    /**
     * Method setUp().
     *
     * @throws Exception when something goes wrong
     */
    @Before
    public void setUp() throws Exception {
        addPlayerEvent0 = new Event(Json.createObjectBuilder()
                .add("type", "ADD_PLAYER")
                .add("time", 10)
                .add("player", Json.createObjectBuilder()
                        .add("user", Json.createObjectBuilder()
                                .add("name", "userA")
                                .add("uuid", "00000000-0000-3039-0000-000000010932")
                                .build())
                        .add("index", 0)
                        .build())
                .build());
        addPlayerEvent1 = new Event(Json.createObjectBuilder()
                .add("type", "ADD_PLAYER")
                .add("time", 10)
                .add("player", Json.createObjectBuilder()
                        .add("user", Json.createObjectBuilder()
                                .add("name", "userB")
                                .add("uuid", "00000000-0001-81cd-0000-00000000a8ca")
                                .build())
                        .add("index", 1)
                        .build())
                .build());
        channels = new Channel[]{mock(Channel.class), mock(Channel.class)};
        when(channels[0].readMessage()).thenReturn(new Message(new Event[]{addPlayerEvent0}));
        when(channels[1].readMessage()).thenReturn(new Message(new Event[]{addPlayerEvent1}));
        final Injector injector = Guice.createInjector(getTestModule());
        testGameSession = spy(injector.getInstance(GameSession.class));
        Prawn.resetFirstAvailableId();
    }

    @Test
    public void testGameSectionCreationWorks() throws Exception {
        assertEquals(0, testGameSession.getGameTimeDelta());
        testGameSession.initializeGame();
        assertEquals(1, testGameSession.getGameTimeDelta());
        final Game game = testGameSession.getGame();
        final UUID id = game.getId();
        final long timestamp = game.getTimestamp();
        assertEquals(
                Json.createReader(new StringReader(
                        "{\n"
                                + "  \"id\": \"" + id.toString() + "\", "
                                + "  \"timestamp\": " + timestamp + ", "
                                + "  \"board\": {\n"
                                + "    \"properties\": [\n"
                                + "      { "
                                + "        \"key\": \"Board.xsize\", "
                                + "        \"value\":\"15\" "
                                + "      }, "
                                + "      { "
                                + "        \"key\": \"Board.stepSize\", "
                                + "        \"value\":\"1.0\" "
                                + "      }, "
                                + "      { "
                                + "        \"key\":\"Board.backgroundImage\", "
                                + "        \"value\":\"/images/gop-background.jpg\" "
                                + "      }, "
                                + "      { "
                                + "        \"key\": \"Board.ysize\", "
                                + "        \"value\":\"10\" "
                                + "      } "
                                + "    ],\n"
                                + "    \"joints\": [\n"
                                + "      {\n"
                                + "        \"index\": 0,\n"
                                + "        \"name\": \"joint0\", "
                                + "        \"x\": 0.0,\n"
                                + "        \"y\": 0.0\n"
                                + "      },\n"
                                + "      {\n"
                                + "        \"index\": 1,\n"
                                + "        \"name\": \"joint1\", "
                                + "        \"x\": 3.0,\n"
                                + "        \"y\": 0.0\n"
                                + "      },\n"
                                + "      {\n"
                                + "        \"index\": 2,\n"
                                + "        \"name\": \"joint2\", "
                                + "        \"x\": 0.0,\n"
                                + "        \"y\": 3.0\n"
                                + "      }\n"
                                + "    ],\n"
                                + "    \"roadSections\": [\n"
                                + "      {\n"
                                + "        \"index\": 0,\n"
                                + "        \"joint0\": 0,\n"
                                + "        \"joint1\": 1\n"
                                + "      },\n"
                                + "      {\n"
                                + "        \"index\": 1,\n"
                                + "        \"joint0\": 0,\n"
                                + "        \"joint1\": 2\n"
                                + "      }\n"
                                + "    ]\n"
                                + "  },\n"
                                + "  \"players\": [ "
                                + "    { "
                                + "      \"index\": 0, "
                                + "      \"user\": { "
                                + "        \"name\":\"userA\", "
                                + "        \"uuid\": \"00000000-0000-3039-0000-000000010932\" "
                                + "      } "
                                + "    }, "
                                + "    { "
                                + "      \"index\": 1, "
                                + "      \"user\": { "
                                + "        \"name\":\"userB\", "
                                + "        \"uuid\": \"00000000-0001-81cd-0000-00000000a8ca\" "
                                + "      } "
                                + "    } "
                                + "  ]\n"
                                + "}")).readObject(),
                testGameSession.getObjectSerializer().serializeGameWithoutPrawnsOrCities());
        assertEquals(0, testGameSession.getGame().getTime());
        verify(channels[0], times(1)).readMessage();
        verify(channels[1], times(1)).readMessage();
    }

    @Test
    public void testCreatingInitializationEventsWorks() throws Exception {
        testGameSession.initializeGame();
        testGameSession.createClientInitializationEvents();
        verify(channels[0], times(1)).readMessage();
        verify(channels[1], times(1)).readMessage();
        final Game game = testGameSession.getGame();
        final UUID id = game.getId();
        final long timestamp = game.getTimestamp();
        assertEquals(
                Arrays.asList(
                        new Event(Json.createReader(new StringReader(
                                "{\n"
                                        + "  \"type\": \"INITIALIZE_GAME\",\n"
                                        + "  \"time\": 0,\n"
                                        + "  \"game\": {\n"
                                        + "    \"id\": \"" + id.toString() + "\", "
                                        + "    \"timestamp\": " + timestamp + ", "
                                        + "    \"board\": {\n"
                                        + "      \"properties\": [\n"
                                        + "        { "
                                        + "          \"key\": \"Board.xsize\", "
                                        + "          \"value\":\"15\" "
                                        + "        }, "
                                        + "        { "
                                        + "          \"key\": \"Board.stepSize\", "
                                        + "          \"value\":\"1.0\" "
                                        + "        }, "
                                        + "        { "
                                        + "          \"key\":\"Board.backgroundImage\", "
                                        + "          \"value\":\"/images/gop-background.jpg\" "
                                        + "        }, "
                                        + "        { "
                                        + "          \"key\": \"Board.ysize\", "
                                        + "          \"value\":\"10\" "
                                        + "        } "
                                        + "      ],\n"
                                        + "      \"joints\": [\n"
                                        + "        {\n"
                                        + "          \"index\": 0,\n"
                                        + "          \"name\": \"joint0\", "
                                        + "          \"x\": 0.0,\n"
                                        + "          \"y\": 0.0\n"
                                        + "        },\n"
                                        + "        {\n"
                                        + "          \"index\": 1,\n"
                                        + "          \"name\": \"joint1\", "
                                        + "          \"x\": 3.0,\n"
                                        + "          \"y\": 0.0\n"
                                        + "        },\n"
                                        + "        {\n"
                                        + "          \"index\": 2,\n"
                                        + "          \"name\": \"joint2\", "
                                        + "          \"x\": 0.0,\n"
                                        + "          \"y\": 3.0\n"
                                        + "        }\n"
                                        + "      ],\n"
                                        + "      \"roadSections\": [\n"
                                        + "        {\n"
                                        + "          \"index\": 0,\n"
                                        + "          \"joint0\": 0,\n"
                                        + "          \"joint1\": 1\n"
                                        + "        },\n"
                                        + "        {\n"
                                        + "          \"index\": 1,\n"
                                        + "          \"joint0\": 0,\n"
                                        + "          \"joint1\": 2\n"
                                        + "        }\n"
                                        + "      ]\n"
                                        + "    },\n"
                                        + "    \"players\": [ "
                                        + "      { "
                                        + "        \"index\": 0, "
                                        + "        \"user\": { "
                                        + "          \"name\":\"userA\", "
                                        + "          \"uuid\": \"00000000-0000-3039-0000-000000010932\" "
                                        + "        } "
                                        + "      }, "
                                        + "      { "
                                        + "        \"index\": 1, "
                                        + "        \"user\": { "
                                        + "          \"name\":\"userB\", "
                                        + "          \"uuid\": \"00000000-0001-81cd-0000-00000000a8ca\" "
                                        + "        } "
                                        + "      } "
                                        + "    ] "
                                        + "  }\n"
                                        + "}\n")).readObject()),
                        new Event(Json.createReader(new StringReader(
                                "{ "
                                        + "  \"type\": \"PRODUCE_SETTLERS_UNIT\", "
                                        + "  \"time\": 0, "
                                        + "  \"id\": 0, "
                                        + "  \"position\": { "
                                        + "    \"roadSection\": 0, "
                                        + "    \"index\": 0 "
                                        + "  }, "
                                        + "  \"player\": 0 "
                                        + "}")).readObject()),
                        new Event(Json.createReader(new StringReader(
                                "{ "
                                        + "  \"type\": \"PRODUCE_SETTLERS_UNIT\", "
                                        + "  \"time\": 0, "
                                        + "  \"id\": 1, "
                                        + "  \"position\": { "
                                        + "    \"roadSection\": 0, "
                                        + "    \"index\": 3 "
                                        + "  }, "
                                        + "  \"player\": 1 "
                                        + "}")).readObject())),
                testGameSession.getLastEvents());
    }

    @Test
    public void testGameIntentAborted() throws Exception {
        final Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(PropertiesReader.class).to(SimplePropertiesReader.class);
                bind(BoardGenerator.class).to(TestBoardGenerator.class);
                bind(InitialPrawnDeployer.class).to(TestPrawnDeployer.class);
                bind(ConnectionManager.Factory.class).to(TestConnectionManager.Factory.class);
                // To make establishConnectionChannels() return null.
                bind(Channel[].class).toInstance(new Channel[]{});
                bind(Long.class).annotatedWith(Names.named("random seed")).toInstance(45L);
            }
        });
        testGameSession = injector.getInstance(GameSession.class);
        assertFalse(testGameSession.initializeGame());
        assertEquals(Game.State.INTENT_ABORTED, testGameSession.getGame().getState());
    }

    @Test
    public void testStartingGameSessionWorks() throws Exception {
        when(channels[0].readMessage()).thenReturn(new Message(new Event[]{addPlayerEvent0}), new Message(new Event[]{}));
        when(channels[1].readMessage()).thenReturn(new Message(new Event[]{addPlayerEvent1}), new Message(new Event[]{}));
        doAnswer(new Answer<Void>() {
            public Void answer(InvocationOnMock unusedInvocation) {
                try {
                    testGameSession.getGame().getPlayers().get(0).setState(Player.State.ABANDONED);
                    testGameSession.getGame().getPlayers().get(1).setState(Player.State.WON);
                    testGameSession.getGame().finish();
                } catch (GameException exception) {
                    sleepException = exception;
                    throw new RuntimeException("GameSession thread failed.", exception);
                }
                return null;
            }
        }).when(testGameSession).notifyIterationFinished();
        testGameSession.start();
        testGameSession.join();
        assertNull(sleepException);
        verify(channels[0], times(3)).readMessage();
        verify(channels[1], times(3)).readMessage();
        verify(channels[0], times(2)).writeMessage(any(Message.class));
        final Game game = testGameSession.getGame();
        final UUID id = game.getId();
        final long timestamp = game.getTimestamp();
        verify(channels[0], times(1)).writeMessage(
                new Message(new Event[]{
                        new Event(Json.createReader(new StringReader(
                                "{\n"
                                        + "  \"type\": \"INITIALIZE_GAME\",\n"
                                        + "  \"time\": 0,\n"
                                        + "  \"game\": {\n"
                                        + "    \"id\": \"" + id.toString() + "\", "
                                        + "    \"timestamp\": " + timestamp + ", "
                                        + "    \"board\": {\n"
                                        + "      \"properties\": [\n"
                                        + "        { "
                                        + "          \"key\": \"Board.xsize\", "
                                        + "          \"value\":\"15\" "
                                        + "        }, "
                                        + "        { "
                                        + "          \"key\": \"Board.stepSize\", "
                                        + "          \"value\":\"1.0\" "
                                        + "        }, "
                                        + "        { "
                                        + "          \"key\":\"Board.backgroundImage\", "
                                        + "          \"value\":\"/images/gop-background.jpg\" "
                                        + "        }, "
                                        + "        { "
                                        + "          \"key\": \"Board.ysize\", "
                                        + "          \"value\":\"10\" "
                                        + "        } "
                                        + "      ],\n"
                                        + "      \"joints\": [\n"
                                        + "        {\n"
                                        + "          \"index\": 0,\n"
                                        + "          \"name\": \"joint0\", "
                                        + "          \"x\": 0.0,\n"
                                        + "          \"y\": 0.0\n"
                                        + "        },\n"
                                        + "        {\n"
                                        + "          \"index\": 1,\n"
                                        + "          \"name\": \"joint1\", "
                                        + "          \"x\": 3.0,\n"
                                        + "          \"y\": 0.0\n"
                                        + "        },\n"
                                        + "        {\n"
                                        + "          \"index\": 2,\n"
                                        + "          \"name\": \"joint2\", "
                                        + "          \"x\": 0.0,\n"
                                        + "          \"y\": 3.0\n"
                                        + "        }\n"
                                        + "      ],\n"
                                        + "      \"roadSections\": [\n"
                                        + "        {\n"
                                        + "          \"index\": 0,\n"
                                        + "          \"joint0\": 0,\n"
                                        + "          \"joint1\": 1\n"
                                        + "        },\n"
                                        + "        {\n"
                                        + "          \"index\": 1,\n"
                                        + "          \"joint0\": 0,\n"
                                        + "          \"joint1\": 2\n"
                                        + "        }\n"
                                        + "      ]\n"
                                        + "    },\n"
                                        + "    \"players\": [\n"
                                        + "      {\n"
                                        + "        \"index\": 0, "
                                        + "        \"user\": { "
                                        + "          \"name\":\"userA\", "
                                        + "          \"uuid\": \"00000000-0000-3039-0000-000000010932\" "
                                        + "        } "
                                        + "      },\n"
                                        + "      {\n"
                                        + "      \"index\": 1, "
                                        + "      \"user\": { "
                                        + "        \"name\":\"userB\", "
                                        + "        \"uuid\": \"00000000-0001-81cd-0000-00000000a8ca\" "
                                        + "      } "
                                        + "      }\n"
                                        + "    ]\n"
                                        + "  }\n"
                                        + "}\n")).readObject()),
                        new Event(Json.createReader(new StringReader(
                                "{ "
                                        + "  \"type\": \"PRODUCE_SETTLERS_UNIT\", "
                                        + "  \"time\": 0, "
                                        + "  \"id\": 0, "
                                        + "  \"position\": { "
                                        + "    \"roadSection\": 0, "
                                        + "    \"index\": 0 "
                                        + "  }, "
                                        + "  \"player\": 0 "
                                        + "}")).readObject()),
                        new Event(Json.createReader(new StringReader(
                                "{ "
                                        + "  \"type\": \"PRODUCE_SETTLERS_UNIT\", "
                                        + "  \"time\": 0, "
                                        + "  \"id\": 1, "
                                        + "  \"position\": { "
                                        + "    \"roadSection\": 0, "
                                        + "    \"index\": 3 "
                                        + "  }, "
                                        + "  \"player\": 1 "
                                        + "}")).readObject()),
                        new Event(Json.createReader(new StringReader(
                                "{ "
                                        + "  \"type\": \"ROAD_SECTION_HALF_DISCOVERED\", "
                                        + "  \"time\": 0, "
                                        + "  \"roadSectionHalf\": { "
                                        + "    \"roadSection\": 0, "
                                        + "     \"end\": \"BACKWARD\" "
                                        + "  }, "
                                        + "  \"player\": 0 "
                                        + "}")).readObject()),
                        new Event(Json.createReader(new StringReader(
                                "{ "
                                        + "  \"type\": \"ROAD_SECTION_HALF_DISCOVERED\", "
                                        + "  \"time\": 0, "
                                        + "  \"roadSectionHalf\": { "
                                        + "    \"roadSection\": 0, "
                                        + "     \"end\": \"FORWARD\" "
                                        + "  }, "
                                        + "  \"player\": 0 "
                                        + "}")).readObject()),
                        new Event(Json.createReader(new StringReader(
                                "{ "
                                        + "  \"type\": \"ROAD_SECTION_HALF_DISCOVERED\", "
                                        + "  \"time\": 0, "
                                        + "  \"roadSectionHalf\": { "
                                        + "    \"roadSection\": 1, "
                                        + "     \"end\": \"BACKWARD\" "
                                        + "  }, "
                                        + "  \"player\": 0 "
                                        + "}")).readObject()),
                        new Event(Json.createReader(new StringReader(
                                "{ "
                                        + "  \"type\": \"ROAD_SECTION_HALF_DISCOVERED\", "
                                        + "  \"time\": 0, "
                                        + "  \"roadSectionHalf\": { "
                                        + "    \"roadSection\": 1, "
                                        + "     \"end\": \"FORWARD\" "
                                        + "  }, "
                                        + "  \"player\": 0 "
                                        + "}")).readObject()),
                        new Event(Json.createReader(new StringReader(
                                "{ "
                                        + "  \"type\": \"PRAWN_APPEARS\", "
                                        + "  \"time\": 0, "
                                        + "  \"prawn\": 1, "
                                        + "  \"player\": 0 "
                                        + "}")).readObject()),
                }));
        verify(channels[1], times(2)).writeMessage(any(Message.class));
        verify(channels[1], times(1)).writeMessage(
                new Message(new Event[]{
                        new Event(Json.createReader(new StringReader(
                                "{\n"
                                        + "  \"type\": \"INITIALIZE_GAME\",\n"
                                        + "  \"time\": 0,\n"
                                        + "  \"game\": {\n"
                                        + "    \"id\": \"" + id.toString() + "\", "
                                        + "    \"timestamp\": " + timestamp + ", "
                                        + "    \"board\": {\n"
                                        + "      \"properties\": [\n"
                                        + "        { "
                                        + "          \"key\": \"Board.xsize\", "
                                        + "          \"value\":\"15\" "
                                        + "        }, "
                                        + "        { "
                                        + "          \"key\": \"Board.stepSize\", "
                                        + "          \"value\":\"1.0\" "
                                        + "        }, "
                                        + "        { "
                                        + "          \"key\":\"Board.backgroundImage\", "
                                        + "          \"value\":\"/images/gop-background.jpg\" "
                                        + "        }, "
                                        + "        { "
                                        + "          \"key\": \"Board.ysize\", "
                                        + "          \"value\":\"10\" "
                                        + "        } "
                                        + "      ],\n"
                                        + "      \"joints\": [\n"
                                        + "        {\n"
                                        + "          \"index\": 0,\n"
                                        + "          \"name\": \"joint0\", "
                                        + "          \"x\": 0.0,\n"
                                        + "          \"y\": 0.0\n"
                                        + "        },\n"
                                        + "        {\n"
                                        + "          \"index\": 1,\n"
                                        + "          \"name\": \"joint1\", "
                                        + "          \"x\": 3.0,\n"
                                        + "          \"y\": 0.0\n"
                                        + "        },\n"
                                        + "        {\n"
                                        + "          \"index\": 2,\n"
                                        + "          \"name\": \"joint2\", "
                                        + "          \"x\": 0.0,\n"
                                        + "          \"y\": 3.0\n"
                                        + "        }\n"
                                        + "      ],\n"
                                        + "      \"roadSections\": [\n"
                                        + "        {\n"
                                        + "          \"index\": 0,\n"
                                        + "          \"joint0\": 0,\n"
                                        + "          \"joint1\": 1\n"
                                        + "        },\n"
                                        + "        {\n"
                                        + "          \"index\": 1,\n"
                                        + "          \"joint0\": 0,\n"
                                        + "          \"joint1\": 2\n"
                                        + "        }\n"
                                        + "      ]\n"
                                        + "    },\n"
                                        + "    \"players\": [\n"
                                        + "      {\n"
                                        + "        \"index\": 0, "
                                        + "        \"user\": { "
                                        + "          \"name\":\"userA\", "
                                        + "          \"uuid\": \"00000000-0000-3039-0000-000000010932\" "
                                        + "        } "
                                        + "      },\n"
                                        + "      {\n"
                                        + "      \"index\": 1, "
                                        + "      \"user\": { "
                                        + "        \"name\":\"userB\", "
                                        + "        \"uuid\": \"00000000-0001-81cd-0000-00000000a8ca\" "
                                        + "      } "
                                        + "      }\n"
                                        + "    ]\n"
                                        + "  }\n"
                                        + "}\n")).readObject()),
                        new Event(Json.createReader(new StringReader(
                                "{ "
                                        + "  \"type\": \"PRODUCE_SETTLERS_UNIT\", "
                                        + "  \"time\": 0, "
                                        + "  \"id\": 0, "
                                        + "  \"position\": { "
                                        + "    \"roadSection\": 0, "
                                        + "    \"index\": 0 "
                                        + "  }, "
                                        + "  \"player\": 0 "
                                        + "}")).readObject()),
                        new Event(Json.createReader(new StringReader(
                                "{ "
                                        + "  \"type\": \"PRODUCE_SETTLERS_UNIT\", "
                                        + "  \"time\": 0, "
                                        + "  \"id\": 1, "
                                        + "  \"position\": { "
                                        + "    \"roadSection\": 0, "
                                        + "    \"index\": 3 "
                                        + "  }, "
                                        + "  \"player\": 1 "
                                        + "}")).readObject()),
                        new Event(Json.createReader(new StringReader(
                                "{ "
                                        + "  \"type\": \"ROAD_SECTION_HALF_DISCOVERED\", "
                                        + "  \"time\": 0, "
                                        + "  \"roadSectionHalf\": { "
                                        + "    \"roadSection\": 0, "
                                        + "     \"end\": \"BACKWARD\" "
                                        + "  }, "
                                        + "  \"player\": 1 "
                                        + "}")).readObject()),
                        new Event(Json.createReader(new StringReader(
                                "{ "
                                        + "  \"type\": \"ROAD_SECTION_HALF_DISCOVERED\", "
                                        + "  \"time\": 0, "
                                        + "  \"roadSectionHalf\": { "
                                        + "    \"roadSection\": 0, "
                                        + "     \"end\": \"FORWARD\" "
                                        + "  }, "
                                        + "  \"player\": 1 "
                                        + "}")).readObject()),
                        new Event(Json.createReader(new StringReader(
                                "{ "
                                        + "  \"type\": \"PRAWN_APPEARS\", "
                                        + "  \"time\": 0, "
                                        + "  \"prawn\": 0, "
                                        + "  \"player\": 1 "
                                        + "}")).readObject())
                }));
        assertEquals(Game.State.FINISHED, testGameSession.getGame().getState());
    }


    @Test
    public void testRunningAndFinishingGameSessionWorks() throws Exception {
        when(channels[0].readMessage())
                .thenReturn(new Message(new Event[]{addPlayerEvent0}))
                .thenReturn(new Message(new Event[]{
                        new Event(Json.createObjectBuilder()
                                .add("type", "ABANDON_PLAYER")
                                .add("time", 20)
                                .add("player", 0)
                                .build()),
                }))
                .thenReturn(new Message(new Event[]{}));
        when(channels[1].readMessage())
                .thenReturn(new Message(new Event[]{addPlayerEvent1}))
                .thenReturn(new Message(new Event[]{}));
        testGameSession.start();
        while (testGameSession.getGame() == null || testGameSession.getGame().getState() != Game.State.FINISHED) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException exception) {
                sleepException = exception;
                throw new RuntimeException("Failed while Thread.sleep()ing.", exception);
            }
        }
        testGameSession.join();
        final Game game = testGameSession.getGame();
        final UUID id = game.getId();
        final long timestamp = game.getTimestamp();
        assertNull(sleepException);
        verify(channels[0], times(3)).readMessage();
        verify(channels[1], times(3)).readMessage();
        verify(channels[0], times(2)).writeMessage(any(Message.class));
        verify(channels[0], times(1)).writeMessage(
                new Message(new Event[]{
                        new Event(Json.createReader(new StringReader(
                                "{\n"
                                        + "  \"type\": \"INITIALIZE_GAME\",\n"
                                        + "  \"time\": 0,\n"
                                        + "  \"game\": {\n"
                                        + "    \"id\": \"" + id.toString() + "\", "
                                        + "    \"timestamp\": " + timestamp + ", "
                                        + "    \"board\": {\n"
                                        + "      \"properties\": [\n"
                                        + "        { "
                                        + "          \"key\": \"Board.xsize\", "
                                        + "          \"value\":\"15\" "
                                        + "        }, "
                                        + "        { "
                                        + "          \"key\": \"Board.stepSize\", "
                                        + "          \"value\":\"1.0\" "
                                        + "        }, "
                                        + "        { "
                                        + "          \"key\":\"Board.backgroundImage\", "
                                        + "          \"value\":\"/images/gop-background.jpg\" "
                                        + "        }, "
                                        + "        { "
                                        + "          \"key\": \"Board.ysize\", "
                                        + "          \"value\":\"10\" "
                                        + "        } "
                                        + "      ],\n"
                                        + "      \"joints\": [\n"
                                        + "        {\n"
                                        + "          \"index\": 0,\n"
                                        + "          \"name\": \"joint0\", "
                                        + "          \"x\": 0.0,\n"
                                        + "          \"y\": 0.0\n"
                                        + "        },\n"
                                        + "        {\n"
                                        + "          \"index\": 1,\n"
                                        + "          \"name\": \"joint1\", "
                                        + "          \"x\": 3.0,\n"
                                        + "          \"y\": 0.0\n"
                                        + "        },\n"
                                        + "        {\n"
                                        + "          \"index\": 2,\n"
                                        + "          \"name\": \"joint2\", "
                                        + "          \"x\": 0.0,\n"
                                        + "          \"y\": 3.0\n"
                                        + "        }\n"
                                        + "      ],\n"
                                        + "      \"roadSections\": [\n"
                                        + "        {\n"
                                        + "          \"index\": 0,\n"
                                        + "          \"joint0\": 0,\n"
                                        + "          \"joint1\": 1\n"
                                        + "        },\n"
                                        + "        {\n"
                                        + "          \"index\": 1,\n"
                                        + "          \"joint0\": 0,\n"
                                        + "          \"joint1\": 2\n"
                                        + "        }\n"
                                        + "      ]\n"
                                        + "    },\n"
                                        + "    \"players\": [\n"
                                        + "      {\n"
                                        + "        \"index\": 0, "
                                        + "        \"user\": { "
                                        + "          \"name\":\"userA\", "
                                        + "          \"uuid\": \"00000000-0000-3039-0000-000000010932\" "
                                        + "        } "
                                        + "      },\n"
                                        + "      {\n"
                                        + "      \"index\": 1, "
                                        + "      \"user\": { "
                                        + "        \"name\":\"userB\", "
                                        + "        \"uuid\": \"00000000-0001-81cd-0000-00000000a8ca\" "
                                        + "      } "
                                        + "      }\n"
                                        + "    ]\n"
                                        + "  }\n"
                                        + "}\n")).readObject()),
                        new Event(Json.createReader(new StringReader(
                                "{ "
                                        + "  \"type\": \"PRODUCE_SETTLERS_UNIT\", "
                                        + "  \"time\": 0, "
                                        + "  \"id\": 0, "
                                        + "  \"position\": { "
                                        + "    \"roadSection\": 0, "
                                        + "    \"index\": 0 "
                                        + "  }, "
                                        + "  \"player\": 0 "
                                        + "}")).readObject()),
                        new Event(Json.createReader(new StringReader(
                                "{ "
                                        + "  \"type\": \"PRODUCE_SETTLERS_UNIT\", "
                                        + "  \"time\": 0, "
                                        + "  \"id\": 1, "
                                        + "  \"position\": { "
                                        + "    \"roadSection\": 0, "
                                        + "    \"index\": 3 "
                                        + "  }, "
                                        + "  \"player\": 1 "
                                        + "}")).readObject()),
                        new Event(Json.createReader(new StringReader(
                                "{ "
                                        + "  \"type\": \"ROAD_SECTION_HALF_DISCOVERED\", "
                                        + "  \"time\": 0, "
                                        + "  \"roadSectionHalf\": { "
                                        + "    \"roadSection\": 0, "
                                        + "     \"end\": \"BACKWARD\" "
                                        + "  }, "
                                        + "  \"player\": 0 "
                                        + "}")).readObject()),
                        new Event(Json.createReader(new StringReader(
                                "{ "
                                        + "  \"type\": \"ROAD_SECTION_HALF_DISCOVERED\", "
                                        + "  \"time\": 0, "
                                        + "  \"roadSectionHalf\": { "
                                        + "    \"roadSection\": 0, "
                                        + "     \"end\": \"FORWARD\" "
                                        + "  }, "
                                        + "  \"player\": 0 "
                                        + "}")).readObject()),
                        new Event(Json.createReader(new StringReader(
                                "{ "
                                        + "  \"type\": \"ROAD_SECTION_HALF_DISCOVERED\", "
                                        + "  \"time\": 0, "
                                        + "  \"roadSectionHalf\": { "
                                        + "    \"roadSection\": 1, "
                                        + "     \"end\": \"BACKWARD\" "
                                        + "  }, "
                                        + "  \"player\": 0 "
                                        + "}")).readObject()),
                        new Event(Json.createReader(new StringReader(
                                "{ "
                                        + "  \"type\": \"ROAD_SECTION_HALF_DISCOVERED\", "
                                        + "  \"time\": 0, "
                                        + "  \"roadSectionHalf\": { "
                                        + "    \"roadSection\": 1, "
                                        + "     \"end\": \"FORWARD\" "
                                        + "  }, "
                                        + "  \"player\": 0 "
                                        + "}")).readObject()),
                        new Event(Json.createReader(new StringReader(
                                "{ "
                                        + "  \"type\": \"PRAWN_APPEARS\", "
                                        + "  \"time\": 0, "
                                        + "  \"prawn\": 1, "
                                        + "  \"player\": 0 "
                                        + "}")).readObject()),
                }));
        verify(channels[1], times(2)).writeMessage(any(Message.class));
        verify(channels[1], times(1)).writeMessage(
                new Message(new Event[]{
                        new Event(Json.createReader(new StringReader(
                                "{\n"
                                        + "  \"type\": \"INITIALIZE_GAME\",\n"
                                        + "  \"time\": 0,\n"
                                        + "  \"game\": {\n"
                                        + "    \"id\": \"" + id.toString() + "\", "
                                        + "    \"timestamp\": " + timestamp + ", "
                                        + "    \"board\": {\n"
                                        + "      \"properties\": [\n"
                                        + "        { "
                                        + "          \"key\": \"Board.xsize\", "
                                        + "          \"value\":\"15\" "
                                        + "        }, "
                                        + "        { "
                                        + "          \"key\": \"Board.stepSize\", "
                                        + "          \"value\":\"1.0\" "
                                        + "        }, "
                                        + "        { "
                                        + "          \"key\":\"Board.backgroundImage\", "
                                        + "          \"value\":\"/images/gop-background.jpg\" "
                                        + "        }, "
                                        + "        { "
                                        + "          \"key\": \"Board.ysize\", "
                                        + "          \"value\":\"10\" "
                                        + "        } "
                                        + "      ],\n"
                                        + "      \"joints\": [\n"
                                        + "        {\n"
                                        + "          \"index\": 0,\n"
                                        + "          \"name\": \"joint0\", "
                                        + "          \"x\": 0.0,\n"
                                        + "          \"y\": 0.0\n"
                                        + "        },\n"
                                        + "        {\n"
                                        + "          \"index\": 1,\n"
                                        + "          \"name\": \"joint1\", "
                                        + "          \"x\": 3.0,\n"
                                        + "          \"y\": 0.0\n"
                                        + "        },\n"
                                        + "        {\n"
                                        + "          \"index\": 2,\n"
                                        + "          \"name\": \"joint2\", "
                                        + "          \"x\": 0.0,\n"
                                        + "          \"y\": 3.0\n"
                                        + "        }\n"
                                        + "      ],\n"
                                        + "      \"roadSections\": [\n"
                                        + "        {\n"
                                        + "          \"index\": 0,\n"
                                        + "          \"joint0\": 0,\n"
                                        + "          \"joint1\": 1\n"
                                        + "        },\n"
                                        + "        {\n"
                                        + "          \"index\": 1,\n"
                                        + "          \"joint0\": 0,\n"
                                        + "          \"joint1\": 2\n"
                                        + "        }\n"
                                        + "      ]\n"
                                        + "    },\n"
                                        + "    \"players\": [\n"
                                        + "      {\n"
                                        + "        \"index\": 0, "
                                        + "        \"user\": { "
                                        + "          \"name\":\"userA\", "
                                        + "          \"uuid\": \"00000000-0000-3039-0000-000000010932\" "
                                        + "        } "
                                        + "      },\n"
                                        + "      {\n"
                                        + "      \"index\": 1, "
                                        + "      \"user\": { "
                                        + "        \"name\":\"userB\", "
                                        + "        \"uuid\": \"00000000-0001-81cd-0000-00000000a8ca\" "
                                        + "      } "
                                        + "      }\n"
                                        + "    ]\n"
                                        + "  }\n"
                                        + "}\n")).readObject()),
                        new Event(Json.createReader(new StringReader(
                                "{ "
                                        + "  \"type\": \"PRODUCE_SETTLERS_UNIT\", "
                                        + "  \"time\": 0, "
                                        + "  \"id\": 0, "
                                        + "  \"position\": { "
                                        + "    \"roadSection\": 0, "
                                        + "    \"index\": 0 "
                                        + "  }, "
                                        + "  \"player\": 0 "
                                        + "}")).readObject()),
                        new Event(Json.createReader(new StringReader(
                                "{ "
                                        + "  \"type\": \"PRODUCE_SETTLERS_UNIT\", "
                                        + "  \"time\": 0, "
                                        + "  \"id\": 1, "
                                        + "  \"position\": { "
                                        + "    \"roadSection\": 0, "
                                        + "    \"index\": 3 "
                                        + "  }, "
                                        + "  \"player\": 1 "
                                        + "}")).readObject()),
                        new Event(Json.createReader(new StringReader(
                                "{ "
                                        + "  \"type\": \"ROAD_SECTION_HALF_DISCOVERED\", "
                                        + "  \"time\": 0, "
                                        + "  \"roadSectionHalf\": { "
                                        + "    \"roadSection\": 0, "
                                        + "     \"end\": \"BACKWARD\" "
                                        + "  }, "
                                        + "  \"player\": 1 "
                                        + "}")).readObject()),
                        new Event(Json.createReader(new StringReader(
                                "{ "
                                        + "  \"type\": \"ROAD_SECTION_HALF_DISCOVERED\", "
                                        + "  \"time\": 0, "
                                        + "  \"roadSectionHalf\": { "
                                        + "    \"roadSection\": 0, "
                                        + "     \"end\": \"FORWARD\" "
                                        + "  }, "
                                        + "  \"player\": 1 "
                                        + "}")).readObject()),
                        new Event(Json.createReader(new StringReader(
                                "{ "
                                        + "  \"type\": \"PRAWN_APPEARS\", "
                                        + "  \"time\": 0, "
                                        + "  \"prawn\": 0, "
                                        + "  \"player\": 1 "
                                        + "}")).readObject())
                }));
        assertEquals(Game.State.FINISHED, testGameSession.getGame().getState());
        assertEquals(1, testGameSession.getGame().getTime());
    }

}
