package cz.wie.najtmar.gop.server;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.name.Names;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.minlog.Log;

import cz.wie.najtmar.gop.board.BoardGenerator;
import cz.wie.najtmar.gop.event.Event;
import cz.wie.najtmar.gop.game.Game;
import cz.wie.najtmar.gop.game.Player;
import cz.wie.najtmar.gop.game.PropertiesReader;
import cz.wie.najtmar.gop.game.SimplePropertiesReader;
import cz.wie.najtmar.gop.game.User;
import cz.wie.najtmar.gop.io.Channel;
import cz.wie.najtmar.gop.io.Message;
import cz.wie.najtmar.gop.server.KryoNetCommunicator.GameInitializationState;
import cz.wie.najtmar.gop.server.KryoNetCommunicator.OverdueServerMessage;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.UUID;

import javax.json.Json;

public class KryoNetCommunicatorTest {

  Client client;
  KryoNetCommunicator testCommunicator;
  UUID localUserId;
  volatile UUID currentGameId;

  volatile Channel[] inputChannels;
  volatile boolean establishConnectionChannelsSucceeded;
  volatile boolean stagePassed;
  volatile Throwable throwableCaughtInCallback;
  List<ServerMessage> clientInput;
  Message testMessage0;
  Message testMessage1;
  Message finishGameMessage;

  private Module getTestModule() {
    return new AbstractModule() {

      @Override
      protected void configure() {
        bind(PropertiesReader.class).to(SimplePropertiesReader.class);
        bind(BoardGenerator.class).toInstance(mock(BoardGenerator.class));
        bind(InitialPrawnDeployer.class).toInstance(mock(InitialPrawnDeployer.class));
        bind(Long.class).annotatedWith(Names.named("random seed")).toInstance(45L);
        bind(ConnectionManager.class).to(KryoNetCommunicator.class);
      }

    };
  }

  /**
   * Method setUp().
   * @throws Exception when something unexpected happens
   */
  @Before
  public void setUp() throws Exception {
    inputChannels = null;
    establishConnectionChannelsSucceeded = true;

    final Injector injector = Guice.createInjector(getTestModule());
    KryoNetCommunicator.injectConnectionTimeoutMillis(5000);
    testCommunicator = (KryoNetCommunicator) spy(injector.getInstance(ConnectionManager.class));
    doAnswer(new Answer<Void>() {
      public Void answer(InvocationOnMock unusedInvocation) {
        stagePassed = true;
        return null;
      }
    }).when(testCommunicator).testNotify();
    localUserId = new UUID(12345, 67890);
    client = new Client();
    final Kryo kryo = client.getKryo();
    kryo.register(ClientMessage.class);
    kryo.register(ClientMessage.Type.class);
    kryo.register(ServerMessage.class);
    kryo.register(ServerMessage.Type.class);
    client.addListener(new Listener() {
      @Override
      public void received(Connection connection, Object object) {
        synchronized (KryoNetCommunicatorTest.this) {
          if (object instanceof ServerMessage) {
            final ServerMessage serverMessage = (ServerMessage) object;
            clientInput.add((ServerMessage) object);
            if (serverMessage.getType().equals(ServerMessage.Type.SEND_MESSAGE)) {
              final UUID gameId = UUID.fromString(serverMessage.getBody().getString("gameId"));
              final UUID userId = UUID.fromString(serverMessage.getBody().getString("userId"));
              final int hashCode = serverMessage.getBody().getInt("hashCode");
              connection.sendTCP(ClientMessage.createServerMessageResponse(gameId, userId, hashCode));
            }
          }
        }
      }
    });
    client.start();

    clientInput = new LinkedList<>();
    testMessage0 = new Message(new Event[]{
        new Event(Json.createObjectBuilder().add("type", "TEST").add("time", 0).add("value", 0).build())});
    testMessage1 = new Message(new Event[]{
        new Event(Json.createObjectBuilder().add("type", "TEST").add("time", 0).add("value", 1).build())});
    finishGameMessage = new Message(new Event[]{
        new Event(Json.createObjectBuilder().add("type", "FINISH_GAME").add("time", 0).add("body", "").build())});
    throwableCaughtInCallback = null;
  }

  /**
   * method tearDown().
   * @throws Exception when something unexpected happens
   */
  @After
  public void tearDown() throws Exception {
    client.close();
    testCommunicator.close();
    assertNull(throwableCaughtInCallback);
  }

  /**
   * Makes sure that ServerMessages sent from testCommunicator are confirmed automatically.
   */
  private void enableTestCommunicatorMessageAutoConfirmation() {
    doAnswer(new Answer<Void>() {
      public Void answer(InvocationOnMock invocation) {
        try {
          invocation.callRealMethod();
          final ServerMessage serverMessage = invocation.getArgumentAt(1, ServerMessage.class);
          final UUID gameId = invocation.getArgumentAt(2, UUID.class);
          final UUID userId = invocation.getArgumentAt(3, UUID.class);
          if (serverMessage.getId() != ServerMessage.ID_NOT_SET) {
            testCommunicator.confirmLastServerMessage(gameId, userId, serverMessage.getId());
          }
        } catch (Throwable throwable) {
          throwableCaughtInCallback = throwable;
        }
        return null;
      }
    }).when(testCommunicator).sendServerMessage(any(Connection.class), any(ServerMessage.class), any(UUID.class),
        any(UUID.class));
  }

  @Test
  public void testInstanceInitialized() throws Exception {
    assertEquals(ConnectionManager.State.NOT_INITIALIZED, testCommunicator.getState());
    testCommunicator.initialize();
    assertEquals(ConnectionManager.State.INITIALIZED, testCommunicator.getState());
    client.connect(KryoNetCommunicator.getConnectionTimeoutMillis(), InetAddress.getLocalHost(),
        testCommunicator.getServerTcpPort(), testCommunicator.getServerUdpPort());
    testCommunicator.startEstablishingLocalConnection();
    client.sendTCP(ClientMessage.createRegisterLocalClient(localUserId, "localUser"));
    while (!testCommunicator.getState().equals(ConnectionManager.State.STARTED)) {
      Thread.sleep(1);
    }
    assertEquals(localUserId, testCommunicator.getLocalUserId());
    assertNotNull(testCommunicator.getLocalClientConnection());
  }

  @Test
  public void testSomePortsAlreadyTaken() throws Exception {
    Log.info("testSomePortsAlreadyTaken()");
    ServerSocket portTcpReserved0 = null;
    ServerSocket portUdpReserved0 = null;
    int lastTakenPortOffset = 0;
    boolean tcpPortTaken = false;
    boolean udpPortTaken = false;
    for (lastTakenPortOffset = 0;
        (!tcpPortTaken || !udpPortTaken) && lastTakenPortOffset < KryoNetCommunicator.PORT_TRIED_COUNT;
        ++lastTakenPortOffset) {
      try {
        portTcpReserved0 = new ServerSocket(KryoNetCommunicator.FIRST_TCP_PORT + lastTakenPortOffset);
        tcpPortTaken = true;
      } catch (IOException exception) {
        Log.warn("TCP port " + (KryoNetCommunicator.FIRST_TCP_PORT + lastTakenPortOffset) + " failed to be taken.");
      }
      try {
        portUdpReserved0 = new ServerSocket(KryoNetCommunicator.FIRST_UDP_PORT + lastTakenPortOffset);
        udpPortTaken = true;
      } catch (IOException exception) {
        Log.warn("UDP port " + (KryoNetCommunicator.FIRST_UDP_PORT + lastTakenPortOffset) + " failed to be taken.");
      }
      if (tcpPortTaken && !udpPortTaken) {
        portTcpReserved0.close();
      } else if (udpPortTaken && !tcpPortTaken) {
        portUdpReserved0.close();
      }
    }
    assertTrue(tcpPortTaken);
    assertTrue(udpPortTaken);
    assertEquals(ConnectionManager.State.NOT_INITIALIZED, testCommunicator.getState());
    testCommunicator.initialize();
    assertEquals(ConnectionManager.State.INITIALIZED, testCommunicator.getState());
    boolean connected = false;

    for (int i = lastTakenPortOffset; !connected && i < KryoNetCommunicator.PORT_TRIED_COUNT; ++i) {
      try {
        client.connect(500, InetAddress.getLocalHost(),
            KryoNetCommunicator.FIRST_TCP_PORT + i, KryoNetCommunicator.FIRST_UDP_PORT + i);
        connected = true;
      } catch (IOException exception) {
        Log.info("Connection failed: " + exception);
        client.close();
      }
    }

    // Connecting the second time, just to make sure.
    client.close();
    connected = false;
    for (int i = lastTakenPortOffset; !connected && i < KryoNetCommunicator.PORT_TRIED_COUNT; ++i) {
      try {
        client.connect(500, InetAddress.getLocalHost(),
            KryoNetCommunicator.FIRST_TCP_PORT + i, KryoNetCommunicator.FIRST_UDP_PORT + i);
        connected = true;
      } catch (IOException exception) {
        Log.info("Connection failed: " + exception);
        client.close();
      }
    }

    assertTrue(connected);
    testCommunicator.startEstablishingLocalConnection();
    client.sendTCP(ClientMessage.createRegisterLocalClient(localUserId, "localUser"));
    while (!testCommunicator.getState().equals(ConnectionManager.State.STARTED)) {
      Thread.sleep(1);
    }
    assertEquals(localUserId, testCommunicator.getLocalUserId());
    assertNotNull(testCommunicator.getLocalClientConnection());

    if (portTcpReserved0 != null) {
      portTcpReserved0.close();
    }
    if (portUdpReserved0 != null) {
      portUdpReserved0.close();
    }
  }

  @Test
  public void testConnectionEstablishingWorks() throws Exception {
    enableTestCommunicatorMessageAutoConfirmation();
    assertEquals(ConnectionManager.State.NOT_INITIALIZED, testCommunicator.getState());
    testCommunicator.initialize();
    assertEquals(ConnectionManager.State.INITIALIZED, testCommunicator.getState());
    client.connect(KryoNetCommunicator.getConnectionTimeoutMillis(), InetAddress.getLocalHost(),
        testCommunicator.getServerTcpPort(), testCommunicator.getServerUdpPort());
    testCommunicator.startEstablishingLocalConnection();
    client.sendTCP(ClientMessage.createRegisterLocalClient(localUserId, "localUser"));

    currentGameId = new UUID(12345, 67890);
    final Game mockGame = mock(Game.class);
    final GameSession mockGameSession = mock(GameSession.class);
    when(mockGameSession.getGame()).thenReturn(mockGame);
    when(mockGame.getId()).thenReturn(new UUID(12345, 67890));

    while (!testCommunicator.getState().equals(ConnectionManager.State.STARTED)) {
      Thread.sleep(1);
    }

    final Thread connectionEstablishingThread = new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          inputChannels = testCommunicator.establishConnectionChannels(mockGameSession);
        } catch (ServerException exception) {
          exception.printStackTrace();
          establishConnectionChannelsSucceeded = false;
        }
      }
    });
    stagePassed = false;
    connectionEstablishingThread.start();
    while (!stagePassed) {
      Thread.sleep(1);
    }

    while (!testCommunicator.getState().equals(ConnectionManager.State.STARTED)) {
      Thread.sleep(1);
    }
    assertEquals(localUserId, testCommunicator.getLocalUserId());
    assertNotNull(testCommunicator.getLocalClientConnection());

    final UUID otherUserId = new UUID(98765, 43210);
    Client otherClient = new Client();
    otherClient.addListener(new Listener() {
      @Override
      public void received(Connection connection, Object object) {
        synchronized (KryoNetCommunicatorTest.this) {
          if (object instanceof ServerMessage) {
            clientInput.add((ServerMessage) object);
          }
        }
      }
    });
    final Kryo kryo = otherClient.getKryo();
    kryo.register(ClientMessage.class);
    kryo.register(ClientMessage.Type.class);
    kryo.register(ServerMessage.class);
    kryo.register(ServerMessage.Type.class);
    otherClient.start();
    otherClient.connect(KryoNetCommunicator.getConnectionTimeoutMillis(), InetAddress.getLocalHost(),
        testCommunicator.getServerTcpPort(), testCommunicator.getServerUdpPort());
    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createJoinGame(currentGameId, otherUserId, "otherUser"));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    otherClient.sendTCP(ClientMessage.createGetPendingGames(otherUserId));

    synchronized (this) {
      while (clientInput.size() < 3) {
        wait(1);
      }
      assertEquals(
          new HashSet<>(Arrays.asList(ServerMessage.createJoinGameResponse(currentGameId, otherUserId, true),
              ServerMessage.createOwnGameStartEnabledStatus(true,
                  Arrays.asList("otherUser", "localUser")),
              ServerMessage.createGetPendingGamesResponse(new GameSession[]{mockGameSession}, "localUser"))),
          new HashSet<>(clientInput));
    }

    stagePassed = false;
    client.sendTCP(ClientMessage.createInitializeGame(currentGameId, localUserId));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    connectionEstablishingThread.join();
    assertTrue(establishConnectionChannelsSucceeded);
    assertNotNull(inputChannels);
    assertEquals(2, inputChannels.length);
    assertNotNull(inputChannels[0]);
    assertNotNull(inputChannels[1]);

    synchronized (this) {
      while (clientInput.size() < 7) {
        wait(1);
      }
      assertEquals(
          new HashSet<>(Arrays.asList(ServerMessage.createJoinGameResponse(currentGameId, otherUserId, true),
              ServerMessage.createOwnGameStartEnabledStatus(true, Arrays.asList("otherUser", "localUser")),
              ServerMessage.createGetPendingGamesResponse(new GameSession[]{mockGameSession}, "localUser"),
              ServerMessage.createInitializeGameResponse(currentGameId, localUserId, true),
              ServerMessage.createStartGame(currentGameId, otherUserId),
              ServerMessage.createRequestAddPlayer(currentGameId, localUserId, 0),
              ServerMessage.createRequestAddPlayer(currentGameId, otherUserId, 1))), new HashSet<>(clientInput));
      assertEquals(
          StreamSupport.stream(Arrays.asList(1, 1, ServerMessage.ID_NOT_SET, 2, 2, 3, 3)).sorted()
              .collect(Collectors.toList()),
          StreamSupport.stream(clientInput).map(message -> message.getId()).sorted().collect(Collectors.toList()));
      final HashMap<UUID, Map<UUID, Integer>> expectedGameUserLastMessageIdMap = new HashMap<>();
      expectedGameUserLastMessageIdMap.put(currentGameId, new HashMap<>());
      expectedGameUserLastMessageIdMap.get(currentGameId).put(localUserId, 3);
      expectedGameUserLastMessageIdMap.get(currentGameId).put(otherUserId, 3);
      assertEquals(expectedGameUserLastMessageIdMap, testCommunicator.getGameUserLastMessageSentIdMap());
    }
  }

  @Test
  public void testCancelingGameIntentWorks() throws Exception {
    assertEquals(ConnectionManager.State.NOT_INITIALIZED, testCommunicator.getState());
    testCommunicator.initialize();
    assertEquals(ConnectionManager.State.INITIALIZED, testCommunicator.getState());
    client.connect(KryoNetCommunicator.getConnectionTimeoutMillis(), InetAddress.getLocalHost(),
        testCommunicator.getServerTcpPort(), testCommunicator.getServerUdpPort());
    testCommunicator.startEstablishingLocalConnection();
    client.sendTCP(ClientMessage.createRegisterLocalClient(localUserId, "localUser"));

    currentGameId = new UUID(12345, 67890);
    final Game mockGame = mock(Game.class);
    final GameSession mockGameSession = mock(GameSession.class);
    when(mockGameSession.getGame()).thenReturn(mockGame);
    when(mockGame.getId()).thenReturn(new UUID(12345, 67890));

    while (!testCommunicator.getState().equals(ConnectionManager.State.STARTED)) {
      Thread.sleep(1);
    }

    final Thread connectionEstablishingThread = new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          inputChannels = testCommunicator.establishConnectionChannels(mockGameSession);
        } catch (ServerException exception) {
          exception.printStackTrace();
          establishConnectionChannelsSucceeded = false;
        }
      }
    });
    stagePassed = false;
    connectionEstablishingThread.start();
    while (!stagePassed) {
      Thread.sleep(1);
    }

    while (!testCommunicator.getState().equals(ConnectionManager.State.STARTED)) {
      Thread.sleep(1);
    }
    assertEquals(localUserId, testCommunicator.getLocalUserId());
    assertNotNull(testCommunicator.getLocalClientConnection());

    final UUID otherUserId = new UUID(98765, 43210);
    Client otherClient = new Client();
    otherClient.addListener(new Listener() {
      @Override
      public void received(Connection connection, Object object) {
        synchronized (KryoNetCommunicatorTest.this) {
          if (object instanceof ServerMessage) {
            clientInput.add((ServerMessage) object);
          }
        }
      }
    });
    final Kryo kryo = otherClient.getKryo();
    kryo.register(ClientMessage.class);
    kryo.register(ClientMessage.Type.class);
    kryo.register(ServerMessage.class);
    kryo.register(ServerMessage.Type.class);
    otherClient.start();
    otherClient.connect(KryoNetCommunicator.getConnectionTimeoutMillis(), InetAddress.getLocalHost(),
        testCommunicator.getServerTcpPort(), testCommunicator.getServerUdpPort());
    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createJoinGame(currentGameId, otherUserId, "otherUser"));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    synchronized (this) {
      while (clientInput.size() < 2) {
        wait(1);
      }
      assertEquals(new HashSet<>(Arrays.asList(ServerMessage.createJoinGameResponse(currentGameId, otherUserId, true),
          ServerMessage.createOwnGameStartEnabledStatus(true,
              Arrays.asList("otherUser", "localUser")))), new HashSet<>(clientInput));
    }

    stagePassed = false;
    client.sendTCP(ClientMessage.createCancelGameIntent(currentGameId, localUserId));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    connectionEstablishingThread.join();
    assertTrue(establishConnectionChannelsSucceeded);
    assertNull(inputChannels);
    synchronized (this) {
      while (clientInput.size() < 4) {
        wait(1);
      }
      assertEquals(
          new HashSet<>(Arrays.asList(ServerMessage.createJoinGameResponse(currentGameId, otherUserId, true),
              ServerMessage.createOwnGameStartEnabledStatus(true, Arrays.asList("otherUser", "localUser")),
              ServerMessage.createCancelGameIntentResponse(currentGameId, localUserId, true),
              ServerMessage.createJoinGameResponse(currentGameId, otherUserId, false))),
          new HashSet<>(clientInput));
    }
  }

  @Test
  public void testLeavingGameIntentWorks() throws Exception {
    assertEquals(ConnectionManager.State.NOT_INITIALIZED, testCommunicator.getState());
    testCommunicator.initialize();
    assertEquals(ConnectionManager.State.INITIALIZED, testCommunicator.getState());
    client.connect(KryoNetCommunicator.getConnectionTimeoutMillis(), InetAddress.getLocalHost(),
        testCommunicator.getServerTcpPort(), testCommunicator.getServerUdpPort());
    testCommunicator.startEstablishingLocalConnection();
    client.sendTCP(ClientMessage.createRegisterLocalClient(localUserId, "localUser"));

    currentGameId = new UUID(12345, 67890);
    final Game mockGame = mock(Game.class);
    final GameSession mockGameSession = mock(GameSession.class);
    when(mockGameSession.getGame()).thenReturn(mockGame);
    when(mockGame.getId()).thenReturn(new UUID(12345, 67890));

    while (!testCommunicator.getState().equals(ConnectionManager.State.STARTED)) {
      Thread.sleep(1);
    }

    final Thread connectionEstablishingThread = new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          inputChannels = testCommunicator.establishConnectionChannels(mockGameSession);
        } catch (ServerException exception) {
          exception.printStackTrace();
          establishConnectionChannelsSucceeded = false;
        }
      }
    });
    stagePassed = false;
    connectionEstablishingThread.start();
    while (!stagePassed) {
      Thread.sleep(1);
    }

    while (!testCommunicator.getState().equals(ConnectionManager.State.STARTED)) {
      Thread.sleep(1);
    }
    assertEquals(localUserId, testCommunicator.getLocalUserId());
    assertNotNull(testCommunicator.getLocalClientConnection());

    final UUID otherUserId = new UUID(98765, 43210);
    Client otherClient = new Client();
    otherClient.addListener(new Listener() {
      @Override
      public void received(Connection connection, Object object) {
        synchronized (KryoNetCommunicatorTest.this) {
          if (object instanceof ServerMessage) {
            clientInput.add((ServerMessage) object);
          }
        }
      }
    });
    final Kryo kryo = otherClient.getKryo();
    kryo.register(ClientMessage.class);
    kryo.register(ClientMessage.Type.class);
    kryo.register(ServerMessage.class);
    kryo.register(ServerMessage.Type.class);
    otherClient.start();
    otherClient.connect(KryoNetCommunicator.getConnectionTimeoutMillis(), InetAddress.getLocalHost(),
        testCommunicator.getServerTcpPort(), testCommunicator.getServerUdpPort());
    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createJoinGame(currentGameId, otherUserId, "otherUser"));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    synchronized (this) {
      while (clientInput.size() < 2) {
        wait(1);
      }
      assertEquals(
          new HashSet<>(Arrays.asList(ServerMessage.createOwnGameStartEnabledStatus(true,
              Arrays.asList("otherUser", "localUser")),
              ServerMessage.createJoinGameResponse(currentGameId, otherUserId, true))), new HashSet<>(clientInput));
      assertEquals(new HashSet<>(Arrays.asList(localUserId, otherUserId)),
          testCommunicator.getGameUserConnectionMap().get(currentGameId).keySet());
    }

    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createLeaveGameIntent(currentGameId, otherUserId));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    synchronized (this) {
      final HashSet<ServerMessage> expectedClientInput = new HashSet<>(Arrays.asList(
          ServerMessage.createOwnGameStartEnabledStatus(true, Arrays.asList("otherUser", "localUser")),
          ServerMessage.createJoinGameResponse(currentGameId, otherUserId, true),
          ServerMessage.createOwnGameStartEnabledStatus(false, Arrays.asList("localUser")),
          ServerMessage.createLeaveGameIntentResponse(currentGameId, otherUserId, true)));

      Log.info("Waiting for LEAVE_GAME_INTENT_RESPONSE .");
      while (!expectedClientInput.equals(new HashSet<>(clientInput))) {
        wait(1);
      }
      assertEquals(new HashSet<>(Arrays.asList(localUserId)),
          testCommunicator.getGameUserConnectionMap().get(currentGameId).keySet());
    }
  }

  @Test
  public void testReceivingClientMessagesWorks() throws Exception {
    enableTestCommunicatorMessageAutoConfirmation();
    assertEquals(ConnectionManager.State.NOT_INITIALIZED, testCommunicator.getState());
    testCommunicator.initialize();
    assertEquals(ConnectionManager.State.INITIALIZED, testCommunicator.getState());
    client.connect(KryoNetCommunicator.getConnectionTimeoutMillis(), InetAddress.getLocalHost(),
        testCommunicator.getServerTcpPort(), testCommunicator.getServerUdpPort());
    testCommunicator.startEstablishingLocalConnection();
    client.sendTCP(ClientMessage.createRegisterLocalClient(localUserId, "localUser"));

    currentGameId = new UUID(12345, 67890);
    final Game mockGame = mock(Game.class);
    final GameSession mockGameSession = mock(GameSession.class);
    when(mockGameSession.getGame()).thenReturn(mockGame);
    when(mockGame.getId()).thenReturn(new UUID(12345, 67890));

    while (!testCommunicator.getState().equals(ConnectionManager.State.STARTED)) {
      Thread.sleep(1);
    }

    final Thread connectionEstablishingThread = new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          inputChannels = testCommunicator.establishConnectionChannels(mockGameSession);
        } catch (ServerException exception) {
          exception.printStackTrace();
          establishConnectionChannelsSucceeded = false;
        }
      }
    });
    stagePassed = false;
    connectionEstablishingThread.start();
    while (!stagePassed) {
      Thread.sleep(1);
    }

    while (!testCommunicator.getState().equals(ConnectionManager.State.STARTED)) {
      Thread.sleep(1);
    }
    assertEquals(localUserId, testCommunicator.getLocalUserId());
    assertNotNull(testCommunicator.getLocalClientConnection());

    final UUID otherUserId = new UUID(98765, 43210);
    Client otherClient = new Client();
    otherClient.addListener(new Listener() {
      @Override
      public void received(Connection connection, Object object) {
        synchronized (KryoNetCommunicatorTest.this) {
          if (object instanceof ServerMessage) {
            clientInput.add((ServerMessage) object);
          }
        }
      }
    });
    final Kryo kryo = otherClient.getKryo();
    kryo.register(ClientMessage.class);
    kryo.register(ClientMessage.Type.class);
    kryo.register(ServerMessage.class);
    kryo.register(ServerMessage.Type.class);
    otherClient.start();
    otherClient.connect(KryoNetCommunicator.getConnectionTimeoutMillis(), InetAddress.getLocalHost(),
        testCommunicator.getServerTcpPort(), testCommunicator.getServerUdpPort());
    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createJoinGame(currentGameId, otherUserId, "otherUser"));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createGetPendingGames(otherUserId));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    synchronized (this) {
      while (clientInput.size() < 2) {
        wait(1);
      }
    }

    stagePassed = false;
    client.sendTCP(ClientMessage.createInitializeGame(currentGameId, localUserId));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    connectionEstablishingThread.join();
    assertTrue(establishConnectionChannelsSucceeded);
    assertNotNull(inputChannels);
    assertEquals(2, inputChannels.length);
    assertNotNull(inputChannels[0]);
    assertNotNull(inputChannels[1]);

    synchronized (this) {
      while (clientInput.size() < 7) {
        wait(1);
      }
    }
    assertEquals(new HashSet<>(Arrays.asList(
        ServerMessage.createInitializeGameResponse(currentGameId, localUserId, true),
        ServerMessage.createStartGame(currentGameId, otherUserId),
        ServerMessage.createRequestAddPlayer(currentGameId, localUserId, 0),
        ServerMessage.createRequestAddPlayer(currentGameId, otherUserId, 1))),
        new HashSet<>(clientInput.subList(3, 7)));

    // Sending Messages .
    stagePassed = false;
    {
      final ClientMessage clientMessage = ClientMessage.createSendMessage(currentGameId, otherUserId, testMessage1);
      clientMessage.setId(1);
      otherClient.sendTCP(clientMessage);
    }
    while (!stagePassed) {
      Thread.sleep(1);
    }
    synchronized (this) {
      while (clientInput.size() < 8) {
        wait(1);
      }
      assertEquals(ServerMessage.createClientMessageResponse(testMessage1.hashCode(), true), clientInput.get(7));
    }
    assertEquals(testCommunicator.getOverdueMessageQueueMap().get(currentGameId), Arrays.asList());
    assertEquals(testMessage1, inputChannels[1].readMessage());

    // Check the state with a HEARTBEAT message .
    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createHeartbeat(1, currentGameId, otherUserId, 1));
    while (!stagePassed) {
      Thread.sleep(1);
    }
    synchronized (this) {
      while (clientInput.size() < 9) {
        wait(1);
      }
    }
    assertEquals(ServerMessage.createHeartbeatResponse(1, true, false, Arrays.asList(), 1), clientInput.get(8));

    stagePassed = false;
    client.sendTCP(ClientMessage.createSendMessage(currentGameId, localUserId, testMessage0));
    while (!stagePassed) {
      Thread.sleep(1);
    }
    synchronized (this) {
      while (clientInput.size() < 10) {
        wait(1);
      }
      assertEquals(ServerMessage.createClientMessageResponse(testMessage0.hashCode(), true), clientInput.get(9));
    }
    assertEquals(testMessage0, inputChannels[0].readMessage());
  }

  @Test
  public void testUnconfirmedMessagesQueuedAndResent() throws Exception {
    assertEquals(ConnectionManager.State.NOT_INITIALIZED, testCommunicator.getState());
    testCommunicator.initialize();
    assertEquals(ConnectionManager.State.INITIALIZED, testCommunicator.getState());
    client.connect(KryoNetCommunicator.getConnectionTimeoutMillis(), InetAddress.getLocalHost(),
        testCommunicator.getServerTcpPort(), testCommunicator.getServerUdpPort());
    testCommunicator.startEstablishingLocalConnection();
    client.sendTCP(ClientMessage.createRegisterLocalClient(localUserId, "localUser"));

    currentGameId = new UUID(12345, 67890);
    final Game mockGame = mock(Game.class);
    final GameSession mockGameSession = mock(GameSession.class);
    when(mockGameSession.getGame()).thenReturn(mockGame);
    when(mockGame.getId()).thenReturn(new UUID(12345, 67890));

    while (!testCommunicator.getState().equals(ConnectionManager.State.STARTED)) {
      Thread.sleep(1);
    }

    final Thread connectionEstablishingThread = new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          inputChannels = testCommunicator.establishConnectionChannels(mockGameSession);
        } catch (ServerException exception) {
          exception.printStackTrace();
          establishConnectionChannelsSucceeded = false;
        }
      }
    });
    stagePassed = false;
    connectionEstablishingThread.start();
    while (!stagePassed) {
      Thread.sleep(1);
    }

    while (!testCommunicator.getState().equals(ConnectionManager.State.STARTED)) {
      Thread.sleep(1);
    }
    assertEquals(localUserId, testCommunicator.getLocalUserId());
    assertNotNull(testCommunicator.getLocalClientConnection());

    final UUID otherUserId = new UUID(98765, 43210);
    Client otherClient = new Client();
    otherClient.addListener(new Listener() {
      @Override
      public void received(Connection connection, Object object) {
        synchronized (KryoNetCommunicatorTest.this) {
          if (object instanceof ServerMessage) {
            clientInput.add((ServerMessage) object);
          }
        }
      }
    });
    final Kryo kryo = otherClient.getKryo();
    kryo.register(ClientMessage.class);
    kryo.register(ClientMessage.Type.class);
    kryo.register(ServerMessage.class);
    kryo.register(ServerMessage.Type.class);
    otherClient.start();
    otherClient.connect(KryoNetCommunicator.getConnectionTimeoutMillis(), InetAddress.getLocalHost(),
        testCommunicator.getServerTcpPort(), testCommunicator.getServerUdpPort());
    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createJoinGame(currentGameId, otherUserId, "otherUser"));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createGetPendingGames(otherUserId));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    synchronized (this) {
      while (clientInput.size() < 2) {
        wait(1);
      }
    }

    stagePassed = false;
    client.sendTCP(ClientMessage.createInitializeGame(currentGameId, localUserId));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    connectionEstablishingThread.join();
    assertTrue(establishConnectionChannelsSucceeded);
    assertNotNull(inputChannels);
    assertEquals(2, inputChannels.length);
    assertNotNull(inputChannels[0]);
    assertNotNull(inputChannels[1]);

    synchronized (this) {
      while (clientInput.size() < 13) {
        wait(1);
      }
    }
    assertEquals(new HashSet<>(Arrays.asList(
        ServerMessage.createOwnGameStartEnabledStatus(true, Arrays.asList("otherUser", "localUser")),
        ServerMessage.createJoinGameResponse(currentGameId, otherUserId, true),
        ServerMessage.createGetPendingGamesResponse(new GameSession[]{mockGameSession}, "localUser"),
        ServerMessage.createOwnGameStartEnabledStatus(true, Arrays.asList("otherUser", "localUser")),
        ServerMessage.createJoinGameResponse(currentGameId, otherUserId, true),
        ServerMessage.createStartGame(currentGameId, otherUserId),
        ServerMessage.createInitializeGameResponse(currentGameId, localUserId, true),
        ServerMessage.createJoinGameResponse(currentGameId, otherUserId, true),
        ServerMessage.createStartGame(currentGameId, otherUserId),
        ServerMessage.createRequestAddPlayer(currentGameId, otherUserId, 1),
        ServerMessage.createOwnGameStartEnabledStatus(true, Arrays.asList("otherUser", "localUser")),
        ServerMessage.createInitializeGameResponse(currentGameId, localUserId, true),
        ServerMessage.createRequestAddPlayer(currentGameId, localUserId, 0))), new HashSet<>(clientInput));

    // Sending Messages .
    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createSendMessage(currentGameId, otherUserId, testMessage1));
    while (!stagePassed) {
      Thread.sleep(1);
    }
    synchronized (this) {
      while (clientInput.size() < 14) {
        wait(1);
      }
      assertEquals(ServerMessage.createClientMessageResponse(testMessage1.hashCode(), true), clientInput.get(13));
    }
    assertEquals(Arrays.asList(), testCommunicator.getOverdueMessageQueueMap().get(currentGameId));
    assertEquals(testMessage1, inputChannels[1].readMessage());

    // Check the state with a HEARTBEAT message .
    stagePassed = false;
    client.sendTCP(ClientMessage.createHeartbeat(1, currentGameId, localUserId, 1));
    while (!stagePassed) {
      Thread.sleep(1);
    }
    synchronized (this) {
      while (clientInput.size() < 15) {
        wait(1);
      }
    }
    assertEquals(Arrays.asList(
        ServerMessage.createInitializeGameResponse(currentGameId, localUserId, true),
        ServerMessage.createRequestAddPlayer(currentGameId, localUserId, 0),
        ServerMessage.createHeartbeatResponse(1, true, false, Arrays.asList(), 0)),
        clientInput.subList(14, 17));

    stagePassed = false;
    client.sendTCP(ClientMessage.createSendMessage(currentGameId, localUserId, testMessage0));
    while (!stagePassed) {
      Thread.sleep(1);
    }
    synchronized (this) {
      while (clientInput.size() < 18) {
        wait(1);
      }
      assertEquals(ServerMessage.createClientMessageResponse(testMessage0.hashCode(), true), clientInput.get(17));
    }
    assertEquals(testMessage0, inputChannels[0].readMessage());

    final Map<UUID, Map<UUID, Queue<OverdueServerMessage>>> expectedUnconfirmedMessageQueueMap =
        new HashMap<>();
    expectedUnconfirmedMessageQueueMap.put(currentGameId, new HashMap<>());
    expectedUnconfirmedMessageQueueMap.get(currentGameId).put(localUserId, new LinkedList<>(
        Arrays.asList(
            testCommunicator.new OverdueServerMessage(
                ServerMessage.createInitializeGameResponse(currentGameId, localUserId, true), localUserId),
            testCommunicator.new OverdueServerMessage(
                ServerMessage.createRequestAddPlayer(currentGameId, localUserId, 0), localUserId))));
    expectedUnconfirmedMessageQueueMap.get(currentGameId).put(otherUserId, new LinkedList<>(
        Arrays.asList(
            testCommunicator.new OverdueServerMessage(
                ServerMessage.createJoinGameResponse(currentGameId, otherUserId, true), otherUserId),
            testCommunicator.new OverdueServerMessage(
                ServerMessage.createStartGame(currentGameId, otherUserId), otherUserId),
            testCommunicator.new OverdueServerMessage(
                ServerMessage.createRequestAddPlayer(currentGameId, otherUserId, 1), otherUserId))));
    assertEquals(expectedUnconfirmedMessageQueueMap, testCommunicator.getUnconfirmedMessageQueueMap());

    // No unresponsive Users yet.
    stagePassed = false;
    client.sendTCP(ClientMessage.createHeartbeat(1, currentGameId, localUserId, 2));
    while (!stagePassed) {
      Thread.sleep(1);
    }
    synchronized (this) {
      while (clientInput.size() < 20) {
        wait(1);
      }
    }
    assertEquals(Arrays.asList(
        ServerMessage.createRequestAddPlayer(currentGameId, localUserId, 0),
        ServerMessage.createHeartbeatResponse(1, true, false, Arrays.asList(), 0)),
        clientInput.subList(18, 20));

    // User unresponsive due to the unresponsiveness threshold exceeded.
    KryoNetCommunicator.injectHeartbeatThreadInternalMillis(1);
    stagePassed = false;
    client.sendTCP(ClientMessage.createHeartbeat(1, currentGameId, localUserId, 2));
    while (!stagePassed) {
      Thread.sleep(1);
    }
    synchronized (this) {
      while (clientInput.size() < 22) {
        wait(1);
      }
    }
    assertEquals(Arrays.asList(
        ServerMessage.createRequestAddPlayer(currentGameId, localUserId, 0),
        ServerMessage.createHeartbeatResponse(1, true, false, Arrays.asList(otherUserId, localUserId), 0)),
        clientInput.subList(20, 22));
  }

  @Test
  public void testAbandoningGameWorks() throws Exception {
    enableTestCommunicatorMessageAutoConfirmation();
    assertEquals(ConnectionManager.State.NOT_INITIALIZED, testCommunicator.getState());
    testCommunicator.initialize();
    assertEquals(ConnectionManager.State.INITIALIZED, testCommunicator.getState());
    client.connect(KryoNetCommunicator.getConnectionTimeoutMillis(), InetAddress.getLocalHost(),
        testCommunicator.getServerTcpPort(), testCommunicator.getServerUdpPort());
    testCommunicator.startEstablishingLocalConnection();
    client.sendTCP(ClientMessage.createRegisterLocalClient(localUserId, "localUser"));

    currentGameId = new UUID(12345, 67890);
    final Game mockGame = mock(Game.class);
    final GameSession mockGameSession = mock(GameSession.class);
    when(mockGameSession.getGame()).thenReturn(mockGame);
    when(mockGame.getId()).thenReturn(new UUID(12345, 67890));

    while (!testCommunicator.getState().equals(ConnectionManager.State.STARTED)) {
      Thread.sleep(1);
    }

    final Thread connectionEstablishingThread = new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          inputChannels = testCommunicator.establishConnectionChannels(mockGameSession);
        } catch (ServerException exception) {
          exception.printStackTrace();
          establishConnectionChannelsSucceeded = false;
        }
      }
    });
    stagePassed = false;
    connectionEstablishingThread.start();
    while (!stagePassed) {
      Thread.sleep(1);
    }

    while (!testCommunicator.getState().equals(ConnectionManager.State.STARTED)) {
      Thread.sleep(1);
    }
    assertEquals(localUserId, testCommunicator.getLocalUserId());
    assertNotNull(testCommunicator.getLocalClientConnection());

    final UUID otherUserId = new UUID(98765, 43210);
    Client otherClient = new Client();
    otherClient.addListener(new Listener() {
      @Override
      public void received(Connection connection, Object object) {
        synchronized (KryoNetCommunicatorTest.this) {
          if (object instanceof ServerMessage) {
            final ServerMessage serverMessage = (ServerMessage) object;
            clientInput.add(serverMessage);
            if (serverMessage.getType().equals(ServerMessage.Type.SEND_MESSAGE)) {
              final UUID gameId = UUID.fromString(serverMessage.getBody().getString("gameId"));
              final UUID userId = UUID.fromString(serverMessage.getBody().getString("userId"));
              final int hashCode = serverMessage.getBody().getInt("hashCode");
              connection.sendTCP(ClientMessage.createServerMessageResponse(gameId, userId, hashCode));
            }
          }
        }
      }
    });
    final Kryo kryo = otherClient.getKryo();
    kryo.register(ClientMessage.class);
    kryo.register(ClientMessage.Type.class);
    kryo.register(ServerMessage.class);
    kryo.register(ServerMessage.Type.class);
    otherClient.start();
    otherClient.connect(KryoNetCommunicator.getConnectionTimeoutMillis(), InetAddress.getLocalHost(),
        testCommunicator.getServerTcpPort(), testCommunicator.getServerUdpPort());
    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createJoinGame(currentGameId, otherUserId, "otherUser"));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createGetPendingGames(otherUserId));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    synchronized (this) {
      while (clientInput.size() < 2) {
        wait(1);
      }
    }

    stagePassed = false;
    client.sendTCP(ClientMessage.createInitializeGame(currentGameId, localUserId));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    connectionEstablishingThread.join();
    assertTrue(establishConnectionChannelsSucceeded);
    assertNotNull(inputChannels);
    assertEquals(2, inputChannels.length);
    assertNotNull(inputChannels[0]);
    assertNotNull(inputChannels[1]);

    synchronized (this) {
      while (clientInput.size() < 7) {
        wait(1);
      }
    }
    assertEquals(new HashSet<>(Arrays.asList(
        ServerMessage.createInitializeGameResponse(currentGameId, localUserId, true),
        ServerMessage.createStartGame(currentGameId, otherUserId),
        ServerMessage.createRequestAddPlayer(currentGameId, localUserId, 0),
        ServerMessage.createRequestAddPlayer(currentGameId, otherUserId, 1))),
        new HashSet<>(clientInput.subList(3, 7)));

    // Sending Messages .
    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createSendMessage(currentGameId, otherUserId, testMessage1));
    while (!stagePassed) {
      Thread.sleep(1);
    }
    synchronized (this) {
      while (clientInput.size() < 8) {
        wait(1);
      }
      assertEquals(ServerMessage.createClientMessageResponse(testMessage1.hashCode(), true), clientInput.get(7));
    }
    assertEquals(testCommunicator.getOverdueMessageQueueMap().get(currentGameId), Arrays.asList());
    assertEquals(testMessage1, inputChannels[1].readMessage());

    // Check the state with a HEARTBEAT message .
    stagePassed = false;
    client.sendTCP(ClientMessage.createHeartbeat(1, currentGameId, localUserId, 1));
    while (!stagePassed) {
      Thread.sleep(1);
    }
    synchronized (this) {
      while (clientInput.size() < 9) {
        wait(1);
      }
    }
    assertEquals(ServerMessage.createHeartbeatResponse(1, true, false, Arrays.asList(), 0), clientInput.get(8));

    stagePassed = false;
    client.sendTCP(ClientMessage.createSendMessage(currentGameId, localUserId, testMessage0));
    while (!stagePassed) {
      Thread.sleep(1);
    }
    synchronized (this) {
      while (clientInput.size() < 10) {
        wait(1);
      }
      assertEquals(ServerMessage.createClientMessageResponse(testMessage0.hashCode(), true), clientInput.get(9));
    }
    assertEquals(testMessage0, inputChannels[0].readMessage());

    stagePassed = false;
    client.sendTCP(ClientMessage.createSendMessage(currentGameId, localUserId, testMessage0));
    while (!stagePassed) {
      Thread.sleep(1);
    }
    synchronized (this) {
      while (clientInput.size() < 11) {
        wait(1);
      }
      assertEquals(ServerMessage.createClientMessageResponse(testMessage0.hashCode(), true), clientInput.get(10));
    }
    assertEquals(testMessage0, inputChannels[0].readMessage());

    // Message 0
    stagePassed = false;
    inputChannels[0].writeMessage(testMessage0);
    synchronized (this) {
      while (clientInput.size() < 12) {
        wait(1);
      }
      assertEquals(ServerMessage.createSendMessage(currentGameId, localUserId, testMessage0), clientInput.get(11));
    }
    while (!stagePassed) {
      Thread.sleep(1);
    }
    assertEquals(new HashSet<>(), testCommunicator.getGameAbandonedUsersMap().get(currentGameId));

    // Message 1 (abandoning the Game)
    final Message abandoningGameMessage = new Message(new Event[]{
        new Event(Json.createObjectBuilder()
            .add("type", "CHANGE_PLAYER_STATE")
            .add("time", 1)
            .add("player", 1)
            .add("state", Player.State.ABANDONED.name())
            .build()),
    });
    stagePassed = false;
    inputChannels[1].writeMessage(abandoningGameMessage);
    synchronized (this) {
      while (clientInput.size() < 13) {
        wait(1);
      }
      assertEquals(ServerMessage.createSendMessage(currentGameId, otherUserId, abandoningGameMessage),
          clientInput.get(12));
    }
    while (!stagePassed) {
      Thread.sleep(1);
    }

    // Sending Messages again twice. otherUser has abandoned the Game, so it's treated as such.
    // Message 0
    stagePassed = false;
    inputChannels[0].writeMessage(testMessage0);
    synchronized (this) {
      while (clientInput.size() < 14) {
        wait(1);
      }
      assertEquals(ServerMessage.createSendMessage(currentGameId, localUserId, testMessage0), clientInput.get(13));
    }
    while (!stagePassed) {
      Thread.sleep(1);
    }
    assertEquals(new HashSet<>(Arrays.asList(otherUserId)),
        testCommunicator.getGameAbandonedUsersMap().get(currentGameId));

    // Message 0
    stagePassed = false;
    inputChannels[0].writeMessage(testMessage0);
    synchronized (this) {
      while (clientInput.size() < 15) {
        wait(1);
      }
      assertEquals(ServerMessage.createSendMessage(currentGameId, localUserId, testMessage0), clientInput.get(14));
    }
    while (!stagePassed) {
      Thread.sleep(1);
    }
    assertEquals(new HashSet<>(Arrays.asList(otherUserId)),
        testCommunicator.getGameAbandonedUsersMap().get(currentGameId));
  }

  @Test
  public void testForceQuittingGameWorks() throws Exception {
    enableTestCommunicatorMessageAutoConfirmation();
    assertEquals(ConnectionManager.State.NOT_INITIALIZED, testCommunicator.getState());
    testCommunicator.initialize();
    assertEquals(ConnectionManager.State.INITIALIZED, testCommunicator.getState());
    client.connect(KryoNetCommunicator.getConnectionTimeoutMillis(), InetAddress.getLocalHost(),
        testCommunicator.getServerTcpPort(), testCommunicator.getServerUdpPort());
    testCommunicator.startEstablishingLocalConnection();
    client.sendTCP(ClientMessage.createRegisterLocalClient(localUserId, "localUser"));

    currentGameId = new UUID(12345, 67890);
    final Game mockGame = mock(Game.class);
    final GameSession mockGameSession = mock(GameSession.class);
    final Player mockPlayer0 = mock(Player.class);
    final Player mockPlayer1 = mock(Player.class);
    when(mockGameSession.getGame()).thenReturn(mockGame);
    when(mockGame.getId()).thenReturn(new UUID(12345, 67890));
    when(mockGame.getPlayers()).thenReturn(Arrays.asList(mockPlayer0, mockPlayer1));
    when(mockPlayer0.getState()).thenReturn(Player.State.IN_GAME);
    when(mockPlayer1.getState()).thenReturn(Player.State.IN_GAME);

    while (!testCommunicator.getState().equals(ConnectionManager.State.STARTED)) {
      Thread.sleep(1);
    }

    final Thread connectionEstablishingThread = new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          inputChannels = testCommunicator.establishConnectionChannels(mockGameSession);
        } catch (ServerException exception) {
          exception.printStackTrace();
          establishConnectionChannelsSucceeded = false;
        }
      }
    });
    stagePassed = false;
    connectionEstablishingThread.start();
    while (!stagePassed) {
      Thread.sleep(1);
    }

    while (!testCommunicator.getState().equals(ConnectionManager.State.STARTED)) {
      Thread.sleep(1);
    }
    assertEquals(localUserId, testCommunicator.getLocalUserId());
    assertNotNull(testCommunicator.getLocalClientConnection());

    final UUID otherUserId = new UUID(98765, 43210);
    Client otherClient = new Client();
    otherClient.addListener(new Listener() {
      @Override
      public void received(Connection connection, Object object) {
        synchronized (KryoNetCommunicatorTest.this) {
          if (object instanceof ServerMessage) {
            clientInput.add((ServerMessage) object);
          }
        }
      }
    });
    final Kryo kryo = otherClient.getKryo();
    kryo.register(ClientMessage.class);
    kryo.register(ClientMessage.Type.class);
    kryo.register(ServerMessage.class);
    kryo.register(ServerMessage.Type.class);
    otherClient.start();
    otherClient.connect(KryoNetCommunicator.getConnectionTimeoutMillis(), InetAddress.getLocalHost(),
        testCommunicator.getServerTcpPort(), testCommunicator.getServerUdpPort());
    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createJoinGame(currentGameId, otherUserId, "otherUser"));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createGetPendingGames(otherUserId));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    synchronized (this) {
      while (clientInput.size() < 2) {
        wait(1);
      }
    }

    stagePassed = false;
    client.sendTCP(ClientMessage.createInitializeGame(currentGameId, localUserId));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    connectionEstablishingThread.join();
    assertTrue(establishConnectionChannelsSucceeded);
    assertNotNull(inputChannels);
    assertEquals(2, inputChannels.length);
    assertNotNull(inputChannels[0]);
    assertNotNull(inputChannels[1]);

    synchronized (this) {
      while (clientInput.size() < 7) {
        wait(1);
      }
    }
    assertEquals(new HashSet<>(Arrays.asList(
        ServerMessage.createInitializeGameResponse(currentGameId, localUserId, true),
        ServerMessage.createStartGame(currentGameId, otherUserId),
        ServerMessage.createRequestAddPlayer(currentGameId, localUserId, 0),
        ServerMessage.createRequestAddPlayer(currentGameId, otherUserId, 1))),
        new HashSet<>(clientInput.subList(3, 7)));

    // Sending Messages .
    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createSendMessage(currentGameId, otherUserId, testMessage1));
    while (!stagePassed) {
      Thread.sleep(1);
    }
    synchronized (this) {
      while (clientInput.size() < 8) {
        wait(1);
      }
      assertEquals(ServerMessage.createClientMessageResponse(testMessage1.hashCode(), true), clientInput.get(7));
    }
    assertEquals(testCommunicator.getOverdueMessageQueueMap().get(currentGameId), Arrays.asList());
    assertEquals(testMessage1, inputChannels[1].readMessage());

    // Check the state with a HEARTBEAT message .
    stagePassed = false;
    client.sendTCP(ClientMessage.createHeartbeat(1, currentGameId, localUserId, 1));
    while (!stagePassed) {
      Thread.sleep(1);
    }
    synchronized (this) {
      while (clientInput.size() < 9) {
        wait(1);
      }
    }
    assertEquals(ServerMessage.createHeartbeatResponse(1, true, false, Arrays.asList(), 0), clientInput.get(8));

    stagePassed = false;
    client.sendTCP(ClientMessage.createSendMessage(currentGameId, localUserId, testMessage0));
    while (!stagePassed) {
      Thread.sleep(1);
    }
    synchronized (this) {
      while (clientInput.size() < 10) {
        wait(1);
      }
      assertEquals(ServerMessage.createClientMessageResponse(testMessage0.hashCode(), true), clientInput.get(9));
    }
    assertEquals(testMessage0, inputChannels[0].readMessage());

    // otherUser force quits the Game .
    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createForceQuitGame(currentGameId, otherUserId));
    while (!stagePassed) {
      Thread.sleep(1);
    }
    verify(mockGame).finish();
    verify(mockGame).getPlayers();
    verify(mockPlayer0).setState(Player.State.UNDECIDED);
    verify(mockPlayer1).setState(Player.State.UNDECIDED);

    // Check the state with a HEARTBEAT message .
    when(mockGame.getState()).thenReturn(Game.State.FINISHED);
    when(mockGame.getResult()).thenReturn(Game.Result.UNDECIDED);
    stagePassed = false;
    client.sendTCP(ClientMessage.createHeartbeat(1, currentGameId, localUserId, 2));
    while (!stagePassed) {
      Thread.sleep(1);
    }
    synchronized (this) {
      while (clientInput.size() < 11) {
        wait(1);
      }
    }
    assertEquals(ServerMessage.createHeartbeatResponse(1, true, true, Arrays.asList(), 0), clientInput.get(10));
  }

  @Test
  public void testQueuingFailedResponsesWorks() throws Exception {
    enableTestCommunicatorMessageAutoConfirmation();
    assertEquals(ConnectionManager.State.NOT_INITIALIZED, testCommunicator.getState());
    testCommunicator.initialize();
    assertEquals(ConnectionManager.State.INITIALIZED, testCommunicator.getState());
    client.connect(KryoNetCommunicator.getConnectionTimeoutMillis(), InetAddress.getLocalHost(),
        testCommunicator.getServerTcpPort(), testCommunicator.getServerUdpPort());
    testCommunicator.startEstablishingLocalConnection();
    client.sendTCP(ClientMessage.createRegisterLocalClient(localUserId, "localUser"));

    currentGameId = new UUID(12345, 67890);
    final Game mockGame = mock(Game.class);
    final GameSession mockGameSession = mock(GameSession.class);
    when(mockGameSession.getGame()).thenReturn(mockGame);
    when(mockGame.getId()).thenReturn(new UUID(12345, 67890));

    while (!testCommunicator.getState().equals(ConnectionManager.State.STARTED)) {
      Thread.sleep(1);
    }

    final Thread connectionEstablishingThread = new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          inputChannels = testCommunicator.establishConnectionChannels(mockGameSession);
        } catch (ServerException exception) {
          exception.printStackTrace();
          establishConnectionChannelsSucceeded = false;
        }
      }
    });
    stagePassed = false;
    connectionEstablishingThread.start();
    while (!stagePassed) {
      Thread.sleep(1);
    }

    while (!testCommunicator.getState().equals(ConnectionManager.State.STARTED)) {
      Thread.sleep(1);
    }
    assertEquals(localUserId, testCommunicator.getLocalUserId());
    assertNotNull(testCommunicator.getLocalClientConnection());

    final UUID otherUserId = new UUID(98765, 43210);
    Client otherClient = new Client();
    otherClient.addListener(new Listener() {
      @Override
      public void received(Connection connection, Object object) {
        synchronized (KryoNetCommunicatorTest.this) {
          if (object instanceof ServerMessage) {
            clientInput.add((ServerMessage) object);
          }
        }
      }
    });
    final Kryo kryo = otherClient.getKryo();
    kryo.register(ClientMessage.class);
    kryo.register(ClientMessage.Type.class);
    kryo.register(ServerMessage.class);
    kryo.register(ServerMessage.Type.class);
    otherClient.start();
    otherClient.connect(KryoNetCommunicator.getConnectionTimeoutMillis(), InetAddress.getLocalHost(),
        testCommunicator.getServerTcpPort(), testCommunicator.getServerUdpPort());
    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createJoinGame(currentGameId, otherUserId, "otherUser"));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    synchronized (this) {
      while (clientInput.size() < 2) {
        wait(1);
      }
    }

    assertEquals(new HashSet<>(Arrays.asList(
        ServerMessage.createJoinGameResponse(currentGameId, otherUserId, true),
        ServerMessage.createOwnGameStartEnabledStatus(true, Arrays.asList("otherUser", "localUser")))),
        new HashSet<>(clientInput.subList(0, 2)));

    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createGetPendingGames(otherUserId));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    synchronized (this) {
      while (clientInput.size() < 3) {
        wait(1);
      }
    }

    stagePassed = false;
    client.sendTCP(ClientMessage.createInitializeGame(currentGameId, localUserId));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    connectionEstablishingThread.join();
    assertTrue(establishConnectionChannelsSucceeded);
    assertNotNull(inputChannels);
    assertEquals(2, inputChannels.length);
    assertNotNull(inputChannels[0]);
    assertNotNull(inputChannels[1]);

    synchronized (this) {
      while (clientInput.size() < 7) {
        wait(1);
      }
    }
    assertEquals(new HashSet<>(Arrays.asList(
        ServerMessage.createInitializeGameResponse(currentGameId, localUserId, true),
        ServerMessage.createStartGame(currentGameId, otherUserId),
        ServerMessage.createRequestAddPlayer(currentGameId, localUserId, 0),
        ServerMessage.createRequestAddPlayer(currentGameId, otherUserId, 1))),
        new HashSet<>(clientInput.subList(3, 7)));

    // Queue CLIENT_MESSAGE_RESPONSE Messages, even though they don't have ids set.
    doAnswer(new Answer<Void>() {
      public Void answer(InvocationOnMock invocation) {
        try {
          final UUID userId = invocation.getArgumentAt(2, UUID.class);
          final Message message = invocation.getArgumentAt(3, Message.class);
          testCommunicator.getOverdueMessageQueueMap().get(currentGameId).add(
              testCommunicator.new OverdueServerMessage(
                  ServerMessage.createClientMessageResponse(message.hashCode(), true), userId));
          invocation.callRealMethod();
        } catch (Throwable throwable) {
          throwableCaughtInCallback = throwable;
        }
        return null;
      }
    }).when(testCommunicator).onSendMessage(any(Connection.class), any(UUID.class), any(UUID.class),
        any(Message.class));

    // Close the connection just before sending a response.
    doAnswer(new Answer<Void>() {
      public Void answer(InvocationOnMock invocation) {
        try {
          final Connection connection = invocation.getArgumentAt(0, Connection.class);
          connection.close();
          while (connection.isConnected()) {
            Thread.sleep(10);
          }
          invocation.callRealMethod();
        } catch (Throwable throwable) {
          throwableCaughtInCallback = throwable;
        }
        return null;
      }
    }).when(testCommunicator).sendServerMessage(any(Connection.class), any(ServerMessage.class), any(UUID.class),
        any(UUID.class));

    // Sending Messages .
    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createSendMessage(currentGameId, otherUserId, testMessage1));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    // Check the Messages have been queued.
    final Map<UUID, Queue<OverdueServerMessage>> testCommunicatorMessageQueue =
        testCommunicator.getOverdueMessageQueueMap();
    assertEquals(Arrays.asList(
        testCommunicator.new OverdueServerMessage(
            ServerMessage.createClientMessageResponse(testMessage1.hashCode(), true), otherUserId)),
        testCommunicatorMessageQueue.get(currentGameId));

    // Check the state with a HEARTBEAT message .
    stagePassed = false;
    client.sendTCP(ClientMessage.createHeartbeat(1, currentGameId, localUserId, 1));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    synchronized (this) {
      while (clientInput.size() < 9) {
        wait(1);
      }
    }
    assertEquals(ServerMessage.createHeartbeatResponse(1, true, false, Arrays.asList(otherUserId), 0),
        clientInput.get(8));
  }

  @Test
  public void testReportingUnresponsiveUsersWorks() throws Exception {
    enableTestCommunicatorMessageAutoConfirmation();
    assertEquals(ConnectionManager.State.NOT_INITIALIZED, testCommunicator.getState());
    testCommunicator.initialize();
    assertEquals(ConnectionManager.State.INITIALIZED, testCommunicator.getState());
    client.connect(KryoNetCommunicator.getConnectionTimeoutMillis(), InetAddress.getLocalHost(),
        testCommunicator.getServerTcpPort(), testCommunicator.getServerUdpPort());
    testCommunicator.startEstablishingLocalConnection();
    client.sendTCP(ClientMessage.createRegisterLocalClient(localUserId, "localUser"));

    currentGameId = new UUID(12345, 67890);
    final Game mockGame = mock(Game.class);
    final GameSession mockGameSession = mock(GameSession.class);
    when(mockGameSession.getGame()).thenReturn(mockGame);
    when(mockGame.getId()).thenReturn(new UUID(12345, 67890));

    while (!testCommunicator.getState().equals(ConnectionManager.State.STARTED)) {
      Thread.sleep(1);
    }

    final Thread connectionEstablishingThread = new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          inputChannels = testCommunicator.establishConnectionChannels(mockGameSession);
        } catch (ServerException exception) {
          exception.printStackTrace();
          establishConnectionChannelsSucceeded = false;
        }
      }
    });
    stagePassed = false;
    connectionEstablishingThread.start();
    while (!stagePassed) {
      Thread.sleep(1);
    }

    while (!testCommunicator.getState().equals(ConnectionManager.State.STARTED)) {
      Thread.sleep(1);
    }
    assertEquals(localUserId, testCommunicator.getLocalUserId());
    assertNotNull(testCommunicator.getLocalClientConnection());

    final UUID otherUserId = new UUID(98765, 43210);
    Client otherClient = new Client();
    otherClient.addListener(new Listener() {
      @Override
      public void received(Connection connection, Object object) {
        synchronized (KryoNetCommunicatorTest.this) {
          if (object instanceof ServerMessage) {
            clientInput.add((ServerMessage) object);
          }
        }
      }
    });
    final Kryo kryo = otherClient.getKryo();
    kryo.register(ClientMessage.class);
    kryo.register(ClientMessage.Type.class);
    kryo.register(ServerMessage.class);
    kryo.register(ServerMessage.Type.class);
    otherClient.start();
    otherClient.connect(KryoNetCommunicator.getConnectionTimeoutMillis(), InetAddress.getLocalHost(),
        testCommunicator.getServerTcpPort(), testCommunicator.getServerUdpPort());
    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createJoinGame(currentGameId, otherUserId, "otherUser"));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createGetPendingGames(otherUserId));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    synchronized (this) {
      while (clientInput.size() < 2) {
        wait(1);
      }
    }

    stagePassed = false;
    client.sendTCP(ClientMessage.createInitializeGame(currentGameId, localUserId));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    connectionEstablishingThread.join();
    assertTrue(establishConnectionChannelsSucceeded);
    assertNotNull(inputChannels);
    assertEquals(2, inputChannels.length);
    assertNotNull(inputChannels[0]);
    assertNotNull(inputChannels[1]);

    synchronized (this) {
      while (clientInput.size() < 7) {
        wait(1);
      }
    }
    assertEquals(new HashSet<>(Arrays.asList(
        ServerMessage.createInitializeGameResponse(currentGameId, localUserId, true),
        ServerMessage.createStartGame(currentGameId, otherUserId),
        ServerMessage.createRequestAddPlayer(currentGameId, localUserId, 0),
        ServerMessage.createRequestAddPlayer(currentGameId, otherUserId, 1))),
        new HashSet<>(clientInput.subList(3, 7)));

    // Sending Messages .
    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createSendMessage(currentGameId, otherUserId, testMessage1));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    // Mark the other User as not responding for a long time.
    testCommunicator.getGameLastUserHeartbeatMap().get(currentGameId).put(otherUserId, 0L);

    // Check the state with a HEARTBEAT message .
    stagePassed = false;
    client.sendTCP(ClientMessage.createHeartbeat(1, currentGameId, localUserId, 1));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    synchronized (this) {
      while (clientInput.size() < 9) {
        wait(1);
      }
    }
    assertEquals(Arrays.asList(
        ServerMessage.createClientMessageResponse(testMessage1.hashCode(), true),
        ServerMessage.createHeartbeatResponse(1, true, false, Arrays.asList(otherUserId), 0)),
        clientInput.subList(7, 9));
  }

  @Test
  public void testReconnectingPlayerWorks() throws Exception {
    enableTestCommunicatorMessageAutoConfirmation();
    assertEquals(ConnectionManager.State.NOT_INITIALIZED, testCommunicator.getState());
    testCommunicator.initialize();
    assertEquals(ConnectionManager.State.INITIALIZED, testCommunicator.getState());
    client.connect(KryoNetCommunicator.getConnectionTimeoutMillis(), InetAddress.getLocalHost(),
        testCommunicator.getServerTcpPort(), testCommunicator.getServerUdpPort());
    testCommunicator.startEstablishingLocalConnection();
    client.sendTCP(ClientMessage.createRegisterLocalClient(localUserId, "localUser"));

    final User localUser = mock(User.class);
    final Player localPlayer = mock(Player.class);
    currentGameId = new UUID(23456, 78901);
    final Game mockGame = mock(Game.class);
    final GameSession mockGameSession = mock(GameSession.class);
    when(mockGameSession.getGame()).thenReturn(mockGame);
    when(mockGame.getId()).thenReturn(currentGameId);
    when(localPlayer.getUser()).thenReturn(localUser);
    when(localUser.getId()).thenReturn(localUserId);

    while (!testCommunicator.getState().equals(ConnectionManager.State.STARTED)) {
      Thread.sleep(1);
    }

    final Thread connectionEstablishingThread = new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          inputChannels = testCommunicator.establishConnectionChannels(mockGameSession);
        } catch (ServerException exception) {
          exception.printStackTrace();
          establishConnectionChannelsSucceeded = false;
        }
      }
    });
    stagePassed = false;
    connectionEstablishingThread.start();
    while (!stagePassed) {
      Thread.sleep(1);
    }

    while (!testCommunicator.getState().equals(ConnectionManager.State.STARTED)) {
      Thread.sleep(1);
    }
    assertEquals(localUserId, testCommunicator.getLocalUserId());
    assertNotNull(testCommunicator.getLocalClientConnection());

    final UUID otherUserId = new UUID(98765, 43210);
    final User otherUser = mock(User.class);
    final Player otherPlayer = mock(Player.class);
    when(mockGame.getPlayers()).thenReturn(Arrays.asList(localPlayer, otherPlayer));
    when(otherPlayer.getUser()).thenReturn(otherUser);
    when(otherUser.getId()).thenReturn(otherUserId);

    Client otherClient = new Client();
    otherClient.addListener(new Listener() {
      @Override
      public void received(Connection connection, Object object) {
        synchronized (KryoNetCommunicatorTest.this) {
          if (object instanceof ServerMessage) {
            clientInput.add((ServerMessage) object);
          }
        }
      }
    });
    final Kryo kryo = otherClient.getKryo();
    kryo.register(ClientMessage.class);
    kryo.register(ClientMessage.Type.class);
    kryo.register(ServerMessage.class);
    kryo.register(ServerMessage.Type.class);
    otherClient.start();
    otherClient.connect(KryoNetCommunicator.getConnectionTimeoutMillis(), InetAddress.getLocalHost(),
        testCommunicator.getServerTcpPort(), testCommunicator.getServerUdpPort());
    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createJoinGame(currentGameId, otherUserId, "otherUser"));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createGetPendingGames(otherUserId));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    synchronized (this) {
      while (clientInput.size() < 2) {
        wait(1);
      }
    }

    stagePassed = false;
    client.sendTCP(ClientMessage.createInitializeGame(currentGameId, localUserId));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    connectionEstablishingThread.join();
    assertTrue(establishConnectionChannelsSucceeded);
    assertNotNull(inputChannels);
    assertEquals(2, inputChannels.length);
    assertNotNull(inputChannels[0]);
    assertNotNull(inputChannels[1]);

    synchronized (this) {
      while (clientInput.size() < 7) {
        wait(1);
      }
    }
    assertEquals(new HashSet<>(Arrays.asList(
        ServerMessage.createInitializeGameResponse(currentGameId, localUserId, true),
        ServerMessage.createStartGame(currentGameId, otherUserId),
        ServerMessage.createRequestAddPlayer(currentGameId, localUserId, 0),
        ServerMessage.createRequestAddPlayer(currentGameId, otherUserId, 1))),
        new HashSet<>(clientInput.subList(3, 7)));

    // Queue CLIENT_MESSAGE_RESPONSE Messages, even though they don't have ids set.
    doAnswer(new Answer<Void>() {
      public Void answer(InvocationOnMock invocation) {
        try {
          final UUID userId = invocation.getArgumentAt(2, UUID.class);
          final Message message = invocation.getArgumentAt(3, Message.class);
          testCommunicator.getOverdueMessageQueueMap().get(currentGameId).add(
              testCommunicator.new OverdueServerMessage(
                  ServerMessage.createClientMessageResponse(message.hashCode(), true), userId));
          invocation.callRealMethod();
        } catch (Throwable throwable) {
          throwableCaughtInCallback = throwable;
        }
        return null;
      }
    }).when(testCommunicator).onSendMessage(any(Connection.class), any(UUID.class), any(UUID.class),
        any(Message.class));

    // Close the connection just before sending a response.
    doAnswer(new Answer<Void>() {
      public Void answer(InvocationOnMock invocation) {
        try {
          final Connection connection = invocation.getArgumentAt(0, Connection.class);
          connection.close();
          while (connection.isConnected()) {
            Thread.sleep(10);
          }
          invocation.callRealMethod();
        } catch (Throwable throwable) {
          throwableCaughtInCallback = throwable;
        }
        return null;
      }
    }).when(testCommunicator).sendServerMessage(any(Connection.class), any(ServerMessage.class), any(UUID.class),
        any(UUID.class));

    // Sending Messages .
    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createSendMessage(currentGameId, otherUserId, testMessage1));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    // Check that the CLIENT_MESSAGE_RESPONSE ServerMessage has been queued.
    assertEquals(Arrays.asList(
        testCommunicator.new OverdueServerMessage(
            ServerMessage.createClientMessageResponse(testMessage1.hashCode(), true), otherUserId)),
        testCommunicator.getOverdueMessageQueueMap().get(currentGameId));

    // Check the state with a HEARTBEAT message .
    stagePassed = false;
    client.sendTCP(ClientMessage.createHeartbeat(1, currentGameId, localUserId, 1));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    synchronized (this) {
      while (clientInput.size() < 9) {
        wait(1);
      }
    }
    assertEquals(
        Arrays.asList(
            ServerMessage.createClientMessageResponse(testMessage1.hashCode(), true),
            ServerMessage.createHeartbeatResponse(1, true, false, Arrays.asList(otherUserId), 0)),
        clientInput.subList(7, 9));

    // Find the matching Game .
    otherClient.close();
    otherClient.connect(KryoNetCommunicator.getConnectionTimeoutMillis(), InetAddress.getLocalHost(),
        testCommunicator.getServerTcpPort(), testCommunicator.getServerUdpPort());
    // Not matching Game id .
    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createGetMatchingGames(otherUserId, otherUserId));
    while (!stagePassed) {
      Thread.sleep(1);
    }
    synchronized (this) {
      while (clientInput.size() < 10) {
        wait(1);
      }
    }
    assertEquals(ServerMessage.createGetMatchingGamesResponse(new GameSession[]{}), clientInput.get(9));
    // Not matching User id .
    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createGetMatchingGames(currentGameId, currentGameId));
    while (!stagePassed) {
      Thread.sleep(1);
    }
    synchronized (this) {
      while (clientInput.size() < 11) {
        wait(1);
      }
    }
    assertEquals(ServerMessage.createGetMatchingGamesResponse(new GameSession[]{}), clientInput.get(10));
    // Both the Game and the User ids match.
    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createGetMatchingGames(currentGameId, otherUserId));
    while (!stagePassed) {
      Thread.sleep(1);
    }
    synchronized (this) {
      while (clientInput.size() < 12) {
        wait(1);
      }
    }
    assertEquals(ServerMessage.createGetMatchingGamesResponse(new GameSession[]{mockGameSession}), clientInput.get(11));

    // Reconnect otherClient .
    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createReconnectPlayer(currentGameId, otherUserId, "1.2.3.4"));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    // Confirm that the ServerMessage has successfully been resent.
    synchronized (this) {
      while (clientInput.size() < 14) {
        wait(1);
      }
      assertEquals(Arrays.asList(
          ServerMessage.createReconnectPlayerResponse(currentGameId, otherUserId, "1.2.3.4", true),
          ServerMessage.createClientMessageResponse(testMessage1.hashCode(), true)),
          clientInput.subList(12, 14));
    }
    assertEquals(testCommunicator.getOverdueMessageQueueMap().get(currentGameId), Arrays.asList());
    assertEquals(testMessage1, inputChannels[1].readMessage());

    // Check the state with a HEARTBEAT message .
    stagePassed = false;
    client.sendTCP(ClientMessage.createHeartbeat(1, currentGameId, localUserId, 1));
    while (!stagePassed) {
      Thread.sleep(1);
    }
    synchronized (this) {
      while (clientInput.size() < 15) {
        wait(1);
      }
    }
    assertEquals(ServerMessage.createHeartbeatResponse(1, true, false, Arrays.asList(), 0), clientInput.get(14));
  }

  @Test
  public void testFinishingGameSessionWorks() throws Exception {
    enableTestCommunicatorMessageAutoConfirmation();
    assertEquals(ConnectionManager.State.NOT_INITIALIZED, testCommunicator.getState());
    testCommunicator.initialize();
    assertEquals(ConnectionManager.State.INITIALIZED, testCommunicator.getState());
    client.connect(KryoNetCommunicator.getConnectionTimeoutMillis(), InetAddress.getLocalHost(),
        testCommunicator.getServerTcpPort(), testCommunicator.getServerUdpPort());
    testCommunicator.startEstablishingLocalConnection();
    client.sendTCP(ClientMessage.createRegisterLocalClient(localUserId, "localUser"));

    currentGameId = new UUID(12345, 67890);
    final Game mockGame = mock(Game.class);
    final GameSession mockGameSession = mock(GameSession.class);
    when(mockGameSession.getGame()).thenReturn(mockGame);
    when(mockGame.getId()).thenReturn(new UUID(12345, 67890));

    while (!testCommunicator.getState().equals(ConnectionManager.State.STARTED)) {
      Thread.sleep(1);
    }

    final Thread connectionEstablishingThread = new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          inputChannels = testCommunicator.establishConnectionChannels(mockGameSession);
        } catch (ServerException exception) {
          exception.printStackTrace();
          establishConnectionChannelsSucceeded = false;
        }
      }
    });
    stagePassed = false;
    connectionEstablishingThread.start();
    while (!stagePassed) {
      Thread.sleep(1);
    }

    while (!testCommunicator.getState().equals(ConnectionManager.State.STARTED)) {
      Thread.sleep(1);
    }
    assertEquals(localUserId, testCommunicator.getLocalUserId());
    assertNotNull(testCommunicator.getLocalClientConnection());

    final UUID otherUserId = new UUID(98765, 43210);
    Client otherClient = new Client();
    otherClient.addListener(new Listener() {
      @Override
      public void received(Connection connection, Object object) {
        synchronized (KryoNetCommunicatorTest.this) {
          if (object instanceof ServerMessage) {
            final ServerMessage serverMessage = (ServerMessage) object;
            clientInput.add((ServerMessage) object);
            if (serverMessage.getType() == ServerMessage.Type.SEND_MESSAGE) {
              final UUID gameId = UUID.fromString(serverMessage.getBody().getString("gameId"));
              final UUID userId = UUID.fromString(serverMessage.getBody().getString("userId"));
              final int hashCode = serverMessage.getBody().getInt("hashCode");
              connection.sendTCP(ClientMessage.createServerMessageResponse(gameId, userId, hashCode));
            }
          }
        }
      }
    });
    final Kryo kryo = otherClient.getKryo();
    kryo.register(ClientMessage.class);
    kryo.register(ClientMessage.Type.class);
    kryo.register(ServerMessage.class);
    kryo.register(ServerMessage.Type.class);
    otherClient.start();
    otherClient.connect(KryoNetCommunicator.getConnectionTimeoutMillis(), InetAddress.getLocalHost(),
        testCommunicator.getServerTcpPort(), testCommunicator.getServerUdpPort());
    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createJoinGame(currentGameId, otherUserId, "otherUser"));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    stagePassed = false;
    otherClient.sendTCP(ClientMessage.createGetPendingGames(otherUserId));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    synchronized (this) {
      while (clientInput.size() < 2) {
        wait(1);
      }
    }

    stagePassed = false;
    client.sendTCP(ClientMessage.createInitializeGame(currentGameId, localUserId));
    while (!stagePassed) {
      Thread.sleep(1);
    }

    connectionEstablishingThread.join();
    assertTrue(establishConnectionChannelsSucceeded);
    assertNotNull(inputChannels);
    assertEquals(2, inputChannels.length);
    assertNotNull(inputChannels[0]);
    assertNotNull(inputChannels[1]);

    synchronized (this) {
      while (clientInput.size() < 7) {
        wait(1);
      }
    }
    assertEquals(new HashSet<>(Arrays.asList(
        ServerMessage.createInitializeGameResponse(currentGameId, localUserId, true),
        ServerMessage.createStartGame(currentGameId, otherUserId),
        ServerMessage.createRequestAddPlayer(currentGameId, localUserId, 0),
        ServerMessage.createRequestAddPlayer(currentGameId, otherUserId, 1))),
        new HashSet<>(clientInput.subList(3, 7)));

    // Sending GAME_FINISHED Messages .
    // Message 0
    stagePassed = false;
    inputChannels[0].writeMessage(finishGameMessage);
    synchronized (this) {
      while (clientInput.size() < 8) {
        wait(1);
      }
      assertEquals(ServerMessage.createSendMessage(currentGameId, localUserId, finishGameMessage), clientInput.get(7));
    }
    while (!stagePassed) {
      Thread.sleep(1);
    }
    // Message 1
    stagePassed = false;
    inputChannels[1].writeMessage(finishGameMessage);
    synchronized (this) {
      while (clientInput.size() < 9) {
        wait(1);
      }
      assertEquals(ServerMessage.createSendMessage(currentGameId, otherUserId, finishGameMessage), clientInput.get(8));
    }
    while (!stagePassed) {
      Thread.sleep(1);
    }

    synchronized (this) {
      while (testCommunicator.getGameInitializationStateMap().get(currentGameId) != GameInitializationState.FINISHED) {
        wait(1);
      }
    }
  }

}
