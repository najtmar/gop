package cz.wie.najtmar.gop.common;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nullable;

/**
 * Class that encapsulates and provides access to test Properties .
 * @author najtmar
 */
public class TestProperties {

  private static final Logger LOGGER = Logger.getLogger(TestProperties.class.getName());

  // Literals denoting Properties names.
  public static final String GAME_MANAGER_SERVER_SOCKETS_PROPERTY_NAME = "GameManager.serverSocketAddresses";

  /**
   * Constructor.
   * @param testPropertiesSet the Properties to be read, or null if no test Properties are specified
   */
  public TestProperties(@Nullable Properties testPropertiesSet) {
    this.testPropertiesSet = testPropertiesSet;
    if (testPropertiesSet == null) {
      this.serverSocketAddresses = null;
      return;
    }

    final String serverSocketAddressesString = testPropertiesSet.getProperty(GAME_MANAGER_SERVER_SOCKETS_PROPERTY_NAME);
    if (serverSocketAddressesString != null) {
      if (LOGGER.isLoggable(Level.INFO)) {
        LOGGER.info(GAME_MANAGER_SERVER_SOCKETS_PROPERTY_NAME + " == " + serverSocketAddressesString);
      }
      final String[] serverSocketAddressesArray = serverSocketAddressesString.split(",");
      serverSocketAddresses = new ArrayList<>();
      for (String serverSocketAddress : serverSocketAddressesArray) {
        final String[] serverSocketAddressArray = serverSocketAddress.split(":");
        if (serverSocketAddressArray.length != 2) {
          throw new IllegalArgumentException("Expected server socket format <host_name>:<port_number> (found "
              + serverSocketAddress);
        }
        serverSocketAddresses.add(new InetSocketAddress(serverSocketAddressArray[0],
            Integer.parseInt(serverSocketAddressArray[1])));
      }
    } else {
      serverSocketAddresses = null;
    }
  }

  /**
   * Internal representation of test Properties.
   */
  private final Properties testPropertiesSet;

  /**
   * Socket addresses of the servers with which the client should connect.
   */
  private final List<InetSocketAddress> serverSocketAddresses;

  public List<InetSocketAddress> getServerSocketAddresses() {
    return serverSocketAddresses;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof TestProperties)) {
      return false;
    }
    final TestProperties otherTestProperties = (TestProperties) obj;
    return this.testPropertiesSet.equals(otherTestProperties.testPropertiesSet);
  }

}
