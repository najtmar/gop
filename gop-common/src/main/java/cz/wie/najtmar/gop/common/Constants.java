package cz.wie.najtmar.gop.common;

/**
 * Utility interface for definition of constants used throughout the codebase.
 * @author najtmar
 */
public interface Constants {

  /**
   * Delta used for comparison of doubles.
   */
  public static final double DOUBLE_DELTA = 0.0000000001;

}
