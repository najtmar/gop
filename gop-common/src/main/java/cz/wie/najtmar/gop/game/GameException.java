package cz.wie.najtmar.gop.game;

/**
 * Thrown when a problem is detected with a Game.
 * @author najtmar
 */
public class GameException extends RuntimeException {

  private static final long serialVersionUID = 5328286519778184109L;

  public GameException() {
    super();
  }

  public GameException(String message) {
    super(message);
  }

  public GameException(Throwable cause) {
    super(cause);
  }

  public GameException(String message, Throwable cause) {
    super(message, cause);
  }

  public GameException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

}
