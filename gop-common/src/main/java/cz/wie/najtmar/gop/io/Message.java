package cz.wie.najtmar.gop.io;

import cz.wie.najtmar.gop.event.Event;

import java.io.Serializable;
import java.util.Arrays;

import javax.annotation.Nonnull;

/**
 * Class used to exchange Events between a server and clients.
 * @author najtmar
 */
public class Message implements Serializable {

  private static final long serialVersionUID = -2641454242784853357L;

  /**
   * The Events comprising the message.
   */
  private final Event[] events;

  /**
   * Unique hash code.
   */
  private final int hashCode;

  private int calculateHashCode(@Nonnull Event[] events) {
    if (events.length == 0) {
      return 0;
    }
    return
        31 * (
            31 * (
                31 * events.length + Long.valueOf(events[0].getTime()).hashCode())
            + Long.valueOf(events[events.length - 1].getTime()).hashCode());
  }

  public Message(@Nonnull Event[] events) {
    this.events = events;
    this.hashCode = calculateHashCode(events);
  }

  public Event[] getEvents() {
    return events;
  }

  @Override
  public int hashCode() {
    return hashCode;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Message)) {
      return false;
    }
    final Message otherMessage = (Message) obj;
    return Arrays.equals(events, otherMessage.events);
  }

  @Override
  public String toString() {
    return "Message [events=" + Arrays.toString(events) + "]";
  }

  /*
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + Arrays.hashCode(events);
    result = prime * result + hashCode;
    return result;
  }
  */

}
