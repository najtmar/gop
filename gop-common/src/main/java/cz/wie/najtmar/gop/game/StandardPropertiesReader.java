package cz.wie.najtmar.gop.game;

import java.io.IOException;
import java.util.Properties;

/**
 * Reads Properties from *.properties files.
 * @author najtmar
 */
public class StandardPropertiesReader implements PropertiesReader {

  protected StandardPropertiesReader() {}

  @Override
  public Properties getBoardProperties() throws IOException {
    final Properties simpleBoardPropertiesSet = new Properties();
    simpleBoardPropertiesSet.load(StandardPropertiesReader.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/board/board.properties"));
    return simpleBoardPropertiesSet;
  }

  @Override
  public Properties getGameProperties() throws IOException {
    final Properties simpleGamePropertiesSet = new Properties();
    simpleGamePropertiesSet.load(StandardPropertiesReader.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/game/game.properties"));
    return simpleGamePropertiesSet;
  }

  @Override
  public Properties getClientViewProperties() throws IOException {
    final Properties simpleClientViewPropertiesSet = new Properties();
    simpleClientViewPropertiesSet.load(StandardPropertiesReader.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/client/view/client-view.properties"));
    return simpleClientViewPropertiesSet;
  }

  @Override
  public Properties getTestProperties() {
    return null;
  }

}
