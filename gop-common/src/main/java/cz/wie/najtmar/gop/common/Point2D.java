package cz.wie.najtmar.gop.common;

import java.io.Serializable;

/**
 * Equivalent of java.awt.geom.Point2D class, which can be used in the Android code.
 * @author najtmar
 *
 */
public class Point2D implements Serializable {

  /**
   * Generated serial version ID .
   */
  private static final long serialVersionUID = -4421667046994524506L;

  public Point2D() {
    // TODO Auto-generated constructor stub
  }

  /**
   * Equivalent of java.awt.geom.Point2D.Double class, which can be used in the Android code.
   * @author najtmar
   *
   */
  public static class Double implements Serializable {

    /**
     * Generated serial version ID .
     */
    private static final long serialVersionUID = 7271189821214383091L;
    private final double xcoord;
    private final double ycoord;

    public Double(double xcoord, double ycoord) {
      this.xcoord = xcoord;
      this.ycoord = ycoord;
    }

    public double getX() {
      return xcoord;
    }

    public double getY() {
      return ycoord;
    }

    /**
     * Calculated the distance between the current Point and pt .
     * @param pt the point from which the distance is measured
     * @return the distance between the current Point and pt
     */
    public double distance(Double pt) {
      final double xdelta = pt.xcoord - this.xcoord;
      final double ydelta = pt.ycoord - this.ycoord;
      return Math.sqrt(xdelta * xdelta + ydelta * ydelta);
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      long temp;
      temp = java.lang.Double.doubleToLongBits(xcoord);
      result = prime * result + (int) (temp ^ (temp >>> 32));
      temp = java.lang.Double.doubleToLongBits(ycoord);
      result = prime * result + (int) (temp ^ (temp >>> 32));
      return result;
    }

    @Override
    public boolean equals(Object obj) {
      if (!(obj instanceof cz.wie.najtmar.gop.common.Point2D.Double)) {
        return false;
      }
      final cz.wie.najtmar.gop.common.Point2D.Double otherDouble = (cz.wie.najtmar.gop.common.Point2D.Double) obj;
      return this.xcoord == otherDouble.xcoord && this.ycoord == otherDouble.ycoord;
    }

    @Override
    public String toString() {
      return "Point2D.Double[" + xcoord + ", " + ycoord + "]";
    }

  }

}
