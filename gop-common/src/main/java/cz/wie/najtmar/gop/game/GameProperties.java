package cz.wie.najtmar.gop.game;

import lombok.ToString;

import javax.annotation.Nonnull;
import java.util.Properties;

/**
 * Class that encapsulates and provides access to all Properties of a Game.
 * @author najtmar
 */
@ToString
public class GameProperties {

  // Literals denoting Properties names.
  public static final String MAX_PLAYER_COUNT_PROPERTY_NAME = "Game.maxPlayerCount";
  public static final String START_TIME_PROPERTY_NAME = "Game.startTime";
  public static final String TIME_DELTA_PROPERTY_NAME = "Game.timeDelta";
  public static final String INTERRUPT_TIME_PERIOD_PROPERTY_NAME = "Game.interruptTimePeriod";
  public static final String BASIC_VELOCITY_PROPERTY_NAME = "Prawn.basicVelocity";
  public static final String VELOCITY_DIR_CONTRIB_FACTOR_PROPERTY_NAME = "Prawn.velocityDirContribFactor";
  public static final String PRODUCE_ACTION_FACTOR_PROPERTY_NAME = "City.produceActionProgressFactor";
  public static final String REPAIR_ACTION_FACTOR_PROPERTY_NAME = "City.repairActionProgressFactor";
  public static final String GROW_ACTION_FACTOR_PROPERTY_NAME = "City.growActionProgressFactor";
  public static final String ATTACK_SUCCESS_PROBABILITY = "Prawn.attackSuccessProbability";
  public static final String MAX_ATTACK_STRENGTH = "Warrior.maxAttackStrength";
  public static final String MAX_PRODUCTION_DELTA = "Factory.maxProductionDelta";

  /**
   * Internal representation of Game Properties.
   */
  private final Properties gamePropertiesSet;

  /**
   * The maximum number of Players taking part in the Game.
   */
  private final int maxPlayerCount;

  /**
   * The time at which the Game starts.
   */
  private final long startTime;

  /**
   * The delta by which time is increased in each round.
   */
  private final long timeDelta;

  /**
   * The period time at which PERIODIC_INTERRUPT Events are generated.
   */
  private final long interruptTimePeriod;

  /**
   * Basic velocity that each Prawn has irrespective of their type or move direction.
   * velocity = basicVelocity + velocityDirContribFactor * velocityDirContrib
   */
  private final double basicVelocity;

  /**
   * A number by which the direction contribution value should be multiplied in order to get the contribution
   * to the velocity resulting from Prawn direction.
   * velocity = basicVelocity + velocityDirContribFactor * velocityDirContrib
   */
  private final double velocityDirContribFactor;

  /**
   * A number by which the progress should be multiplied to obtain the actual progress of an action to produce a Prawn.
   */
  private final double produceActionProgressFactor;

  /**
   * A number by which the progress should be multiplied to obtain the actual progress of an action to repair a Prawn.
   */
  private final double repairActionProgressFactor;

  /**
   * A number by which the progress should be multiplied to obtain the actual progress of an action to grow a city.
   */
  private final double growActionProgressFactor;

  /**
   * For a Prawn, a number from interval [0, 1] denoting the probability at which an attack succeeds.
   */
  private final double attackSuccessProbability;

  /**
   * For a Warrior, a nonnegative number denoting the maximum strength an attack may have.
   */
  private final double maxAttackStrength;

  /**
   * For a Factory, the production delta when a City has a full cultivable area.
   */
  private final double maxProductionDelta;

  /**
   * Constructor.
   * @param gamePropertiesSet Properties set to initialize the object from
   * @throws GameException when GameProperties cannot be created
   */
  public GameProperties(@Nonnull Properties gamePropertiesSet) throws GameException {
    this.gamePropertiesSet = gamePropertiesSet;
    this.maxPlayerCount = getIntegerProperty(MAX_PLAYER_COUNT_PROPERTY_NAME);
    this.startTime = getLongProperty(START_TIME_PROPERTY_NAME);
    this.timeDelta = getLongProperty(TIME_DELTA_PROPERTY_NAME);
    this.interruptTimePeriod = getLongProperty(INTERRUPT_TIME_PERIOD_PROPERTY_NAME);
    this.basicVelocity = getDoubleProperty(BASIC_VELOCITY_PROPERTY_NAME);
    this.velocityDirContribFactor = getDoubleProperty(VELOCITY_DIR_CONTRIB_FACTOR_PROPERTY_NAME);
    this.produceActionProgressFactor = getDoubleProperty(PRODUCE_ACTION_FACTOR_PROPERTY_NAME);
    this.repairActionProgressFactor = getDoubleProperty(REPAIR_ACTION_FACTOR_PROPERTY_NAME);
    this.growActionProgressFactor = getDoubleProperty(GROW_ACTION_FACTOR_PROPERTY_NAME);
    this.attackSuccessProbability = getDoubleProperty(ATTACK_SUCCESS_PROBABILITY);
    this.maxAttackStrength = getDoubleProperty(MAX_ATTACK_STRENGTH);
    this.maxProductionDelta = getDoubleProperty(MAX_PRODUCTION_DELTA);
  }

  /**
   * Parses and returns a double property.
   * @param key property
   * @return parsed double property
   * @throws GameException if property reading or parsing fails
   */
  private double getDoubleProperty(String key) throws GameException {
    try {
      return Double.parseDouble(gamePropertiesSet.getProperty(key));
    } catch (NumberFormatException | NullPointerException exception) {
      throw new GameException("Double property " + key + " could not be read.", exception);
    }
  }

  /**
   * Parses and returns an integer property.
   * @param key property
   * @return parsed integer property
   * @throws GameException if property reading or parsing fails
   */
  private int getIntegerProperty(String key) throws GameException {
    try {
      return Integer.parseInt(gamePropertiesSet.getProperty(key));
    } catch (NumberFormatException exception) {
      throw new GameException("Integer property " + key + " could not be read.", exception);
    }
  }

  /**
   * Parses and returns a long property.
   * @param key property
   * @return parsed long property
   * @throws GameException if property reading or parsing fails
   */
  private long getLongProperty(String key) throws GameException {
    try {
      return Long.parseLong(gamePropertiesSet.getProperty(key));
    } catch (NumberFormatException exception) {
      throw new GameException("Integer property " + key + " could not be read.", exception);
    }
  }

  public int getMaxPlayerCount() {
    return maxPlayerCount;
  }

  public long getStartTime() {
    return startTime;
  }

  public long getTimeDelta() {
    return timeDelta;
  }

  public long getInterruptTimePeriod() {
    return interruptTimePeriod;
  }

  public double getBasicVelocity() {
    return basicVelocity;
  }

  public double getVelocityDirContribFactor() {
    return velocityDirContribFactor;
  }

  public double getProduceActionProgressFactor() {
    return produceActionProgressFactor;
  }

  public double getRepairActionProgressFactor() {
    return repairActionProgressFactor;
  }

  public double getGrowActionProgressFactor() {
    return growActionProgressFactor;
  }

  public double getAttackSuccessProbability() {
    return attackSuccessProbability;
  }

  public double getMaxAttackStrength() {
    return maxAttackStrength;
  }

  public double getMaxProductionDelta() {
    return maxProductionDelta;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof GameProperties)) {
      return false;
    }
    final GameProperties otherGameProperties = (GameProperties) obj;
    return this.gamePropertiesSet.equals(otherGameProperties.gamePropertiesSet);
  }

}
