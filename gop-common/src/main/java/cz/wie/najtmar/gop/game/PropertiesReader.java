package cz.wie.najtmar.gop.game;

import java.io.IOException;
import java.util.Properties;

/**
 * Interface used to read server-side Properties.
 * @author najtmar
 */
public interface PropertiesReader {

  /**
   * Returns Board Properties .
   * @return an instance of Properties used for Board
   * @throws IOException when Properties cannot be read
   */
  public Properties getBoardProperties() throws IOException;

  /**
   * Returns Game Properties .
   * @return an instance of Properties used for Game
   * @throws IOException when Properties cannot be read
   */
  public Properties getGameProperties() throws IOException;

  /**
   * Returns Client view Properties .
   * @return an instance of Properties used for Client view
   * @throws IOException when Properties cannot be read
   */
  public Properties getClientViewProperties() throws IOException;

  /**
   * Returns Test Properties .
   * @return an instance of Properties used for tests, or null if it's not a test run
   */
  public Properties getTestProperties();

}
