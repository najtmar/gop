package cz.wie.najtmar.gop.event;

/**
 * Thrown when a problem is detected with serialization.
 * @author najtmar
 */
public class SerializationException extends RuntimeException {

  private static final long serialVersionUID = -4596142106815577936L;

  public SerializationException() {
    super();
  }

  public SerializationException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

  public SerializationException(String message, Throwable cause) {
    super(message, cause);
  }

  public SerializationException(String message) {
    super(message);
  }

  public SerializationException(Throwable cause) {
    super(cause);
  }

}
