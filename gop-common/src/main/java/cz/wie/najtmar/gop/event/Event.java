package cz.wie.najtmar.gop.event;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.StringReader;

import javax.annotation.Nonnull;
import javax.json.Json;
import javax.json.JsonNumber;
import javax.json.JsonObject;

/**
 * General-purpose class used to recording, replaying, and processing any type of events in GOP.
 * @author najtmar
 */
public class Event implements Comparable<Event>, Serializable {

  private static final long serialVersionUID = 1855426642467978623L;

  /**
   * Represents the type of event.
   * @author najtmar
   */
  public enum Type {
    /**
     * Initializes a Game.
     */
    INITIALIZE_GAME(
        false, true, true),

    /**
     * Restores a Game.
     */
    RESTORE_GAME(
            false, true, true),

    /**
     * Finish the Game.
     */
    FINISH_GAME(
        false, true, true),

    /**
     * Sent by the Server to notify that the Game will not start.
     */
    ABANDON_GAME_ATTEMPT(true, true, true),

    /**
     * Sent by the Server to notify that the Game got interrupted.
     */
    INTERRUPT_GAME(true, true, true),

    /**
     * Add a Player to a Game.
     */
    ADD_PLAYER(
        true, true, false),

    /**
     * A Player abandons the Game.
     */
    ABANDON_PLAYER(
        true, true, false),

    /**
     * Change the State of a Player.
     */
    CHANGE_PLAYER_STATE(
        false, true, true),

    /**
     * Move a Prawn.
     */
    MOVE_PRAWN(
        false, true, true),

    /**
     * Make a Prawn stay.
     */
    STAY_PRAWN(
        false, true, false),

    /**
     * Decrease the energy of a Prawn.
     */
    DECREASE_PRAWN_ENERGY(
        false, true, false),

    /**
     * Destroy a Prawn.
     */
    DESTROY_PRAWN(
        false, true, true),

    /**
     * Sets a Factory to an Action of producing a Warrior.
     */
    SET_PRODUCE_WARRIOR_ACTION(
        true, true, false),

    /**
     * Sets a Factory to an Action of producing a Settlers Unit.
     */
    SET_PRODUCE_SETTLERS_UNIT_ACTION(
        true, true, false),

    /**
     * Sets a Factory to an Action of repairing a Prawn.
     */
    SET_REPAIR_PRAWN_ACTION(
        true, true, false),

    /**
     * Sets a Factory to an Action of growing a City.
     */
    SET_GROW_CITY_ACTION(
        true, true, false),

    /**
     * Order creation of a City by a SettlersUnit located at a Joint .
     */
    ORDER_CITY_CREATION(
        true, true, false),

    /**
     * Notifies that a Factory Action State is set to FINISHED.
     */
    ACTION_FINISHED(
        false, false, true),

    /**
     * Produce a Warrior.
     */
    PRODUCE_WARRIOR(
        false, false, true),

    /**
     * Produce a Settlers Unit.
     */
    PRODUCE_SETTLERS_UNIT(
        false, false, true),

    /**
     * Repair a Prawn.
     */
    REPAIR_PRAWN(
        false, false, false),

    /**
     * Create a City.
     */
    CREATE_CITY(
        false, true, true),

    /**
     * Grow a City.
     */
    GROW_CITY(
        false, false, true),

    /**
     * Shrink or destroy a City.
     */
    SHRINK_OR_DESTROY_CITY(
        false, false, true),

    /**
     * Capture or destroy a City.
     */
    CAPTURE_OR_DESTROY_CITY(
        false, true, true),

    /**
     * Add a Battle.
     */
    ADD_BATTLE(
        false, true, true),

    /**
     * Remove a Battle.
     */
    REMOVE_BATTLE(
        false, true, true),

    /**
     * Increase the progress of an Action of a Factory in a City by a universal delta value.
     */
    INCREASE_PROGRESS_BY_UNIVERSAL_DELTA(
        false, true, false),

    /**
     * Set the progress of a Factory in a City to an absolute value.
     */
    SET_FACTORY_PROGRESS(
        false, false, false),

    /**
     * Set a Itinerary for a Prawn .
     */
    SET_PRAWN_ITINERARY(
        true, true, false),

    /**
     * Clear the Itinerary for a Prawn .
     */
    CLEAR_PRAWN_ITINERARY(
        true, true, false),

    /**
     * Notifies the Server that a Client has failed.
     */
    CLIENT_FAILURE(
        true, true, false),

    /**
     * Periodically generated Event, which triggers an interrupt for all Players .
     */
    PERIODIC_INTERRUPT(
        false, false, true),

    /**
     * An opponent's Prawn becomes visible to a Player .
     */
    PRAWN_APPEARS(
        false, false, false),

    /**
     * An existing opponent's Prawn stops being visible to a Player .
     */
    PRAWN_DISAPPEARS(
        false, false, false),

    /**
     * An opponent's City becomes visible to a Player .
     */
    CITY_APPEARS(
        false, false, false),

    /**
     * An opponent's City is shown to a Player to have been destroyed.
     */
    CITY_SHOWN_DESTROYED(
        false, false, false),

    /**
     * An invisible once discovered RoadSection.Half becomes visible to a Player .
     */
    ROAD_SECTION_HALF_APPEARS(
        false, false, false),

    /**
     * A visible RoadSection.Half becomes invisible to a Player .
     */
    ROAD_SECTION_HALF_DISAPPEARS(
        false, false, false),

    /**
     * A RoadSection.Half becomes discovered by a Player .
     */
    ROAD_SECTION_HALF_DISCOVERED(
        false, false, false),

    /**
     * Used in tests.
     */
    TEST(
        false, true, false),

    /**
     * Used in tests.
     */
    TEST_INTERACTIVE(
        true, true, false);

    /**
     * Specifies whether the Event type is interactive or not. An interactive Events is generated by an external source
     * (the user) rather than generated as a part of the Game process.
     */
    private final boolean interactive;

    /**
     * Specifies whether the Event is an engine Event or not. An engine Event should be executed by the Game engine to
     * replay the Game state rather than being automatically generated by executing other Events (this is the case for
     * UI-only Events).
     */
    private final boolean engine;

    /**
     * Specifies whether a given Event may trigger a Player's interrupt. Player's interrupts require Player's attention.
     */
    private final boolean interrupt;

    Type(boolean interactive, boolean engine, boolean interrupt) {
      this.interactive = interactive;
      this.engine = engine;
      this.interrupt = interrupt;
    }

    public boolean isInteractive() {
      return interactive;
    }

    public boolean isEngine() {
      return engine;
    }

  }

  /**
   * Type of the event. Matches body.type .
   */
  private final Type type;

  /**
   * The time of the Event. In milliseconds since the Epoch.
   */
  private final long time;

  // TODO(najtmar): Add unique increasing ID to enable comparing and sorting.

  /**
   * The body of the event, encoded as a JsonObject.
   */
  private transient JsonObject body;

  /**
   * Unique hash code of the Event.
   */
  private final int hashCode;

  /**
   * Constructor.
   * @param body JsonObject representation of the Event to be constructed
   * @throws EventProcessingException when Event cannot be constructed
   */
  public Event(@Nonnull JsonObject body) throws EventProcessingException {
    this.body = body;
    try {
      // type
      String typeName = body.getString("type");
      this.type = Type.valueOf(typeName);
      // time
      JsonNumber timeObject = body.getJsonNumber("time");
      this.time = timeObject.longValue();
    } catch (IllegalArgumentException | ClassCastException exception) {
      throw new EventProcessingException("Field could not be read.", exception);
    } catch (NullPointerException exception) {
      throw new EventProcessingException("Required field not specified in " + body, exception);
    }
    this.hashCode = this.body.hashCode();
  }

  public Type getType() {
    return type;
  }

  public long getTime() {
    return time;
  }

  public JsonObject getBody() {
    return body;
  }

  /**
   * Determines whether a given Events triggers a Player's interrupt.
   * @return iff the Event triggers a Player's interrupt
   * @throws SerializationException when the Event fails to be deserialized
   */
  public boolean isInterrupt() throws SerializationException {
    if (type.interrupt) {
      try {
        switch (type) {
          case MOVE_PRAWN: {
            return body.getJsonObject("move").getBoolean("stop");
          }
          default:
            break;
        }
      } catch (NullPointerException exception) {
        throw new SerializationException("Failed to read Event details: " + this, exception);
      }
    }
    return type.interrupt;
  }

  @Override
  public int hashCode() {
    return hashCode;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Event)) {
      return false;
    }
    Event otherEvent = (Event) obj;
    return this.time == otherEvent.time && this.hashCode == otherEvent.hashCode;
  }

  @Override
  public int compareTo(Event event) {
    final int compareTime = Long.compare(this.time, event.time);
    if (compareTime != 0) {
      return compareTime;
    }
    return Integer.compare(this.hashCode, event.hashCode);
  }

  @Override
  public String toString() {
    return "Event [type=" + type + ", time=" + time + ", body=" + body + "]";
  }

  private void writeObject(ObjectOutputStream out) throws IOException {
    out.defaultWriteObject();
    out.writeObject(body.toString());
  }

  private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
    in.defaultReadObject();
    this.body = Json.createReader(new StringReader((String) in.readObject())).readObject();
  }

}
