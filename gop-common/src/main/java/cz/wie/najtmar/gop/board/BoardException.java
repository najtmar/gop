package cz.wie.najtmar.gop.board;

/**
 * Thrown when a problem is detected with a Board.
 * @author najtmar
 */
public class BoardException extends RuntimeException {

  private static final long serialVersionUID = -3792907651892712977L;

  public BoardException() {
    super();
  }

  public BoardException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

  public BoardException(String message, Throwable cause) {
    super(message, cause);
  }

  public BoardException(String message) {
    super(message);
  }

  public BoardException(Throwable cause) {
    super(cause);
  }

}
