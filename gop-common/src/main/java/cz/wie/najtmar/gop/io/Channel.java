package cz.wie.najtmar.gop.io;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;

import javax.annotation.Nonnull;

/**
 * Channel used to exchange Messages between a server and clients.
 * @author najtmar
 */
public class Channel {

  /**
   * Queue used to store ingoing messages.
   */
  private final BlockingQueue<Message> inputQueue;

  /**
   * Queue used to store outgoing messages.
   */
  private final BlockingQueue<Message> outputQueue;

  public Channel(@Nonnull BlockingQueue<Message> inputQueue, @Nonnull BlockingQueue<Message> outputQueue)
      throws IOException {
    this.inputQueue = inputQueue;
    this.outputQueue = outputQueue;
  }

  /**
   * Reads a Message from the input stream.
   * @return a Message read from the input stream
   * @throws InterruptedException when reading the Message fails
   */
  public Message readMessage() throws InterruptedException {
    return inputQueue.take();
  }

  /**
   * Writes message to the output stream.
   * @throws InterruptedException when writing the Message fails
   */
  public void writeMessage(@Nonnull Message message) throws InterruptedException {
    outputQueue.put(message);
  }

}
