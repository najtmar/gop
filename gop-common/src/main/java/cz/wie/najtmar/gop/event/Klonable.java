package cz.wie.najtmar.gop.event;

public interface Klonable<T> {

    T klone();

}
