package cz.wie.najtmar.gop.game;

import java.io.IOException;
import java.util.Properties;

/**
 * Reads Properties from simple-*.properties files. Used in tests.
 * @author najtmar
 */
public class SimplePropertiesReader implements PropertiesReader {

  protected SimplePropertiesReader() {}

  @Override
  public Properties getBoardProperties() throws IOException {
    final Properties simpleBoardPropertiesSet = new Properties();
    simpleBoardPropertiesSet.load(SimplePropertiesReader.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/board/simple-board.properties"));
    return simpleBoardPropertiesSet;
  }

  @Override
  public Properties getGameProperties() throws IOException {
    final Properties simpleGamePropertiesSet = new Properties();
    simpleGamePropertiesSet.load(SimplePropertiesReader.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/game/simple-game.properties"));
    return simpleGamePropertiesSet;
  }

  @Override
  public Properties getClientViewProperties() throws IOException {
    final Properties simpleClientViewPropertiesSet = new Properties();
    simpleClientViewPropertiesSet.load(SimplePropertiesReader.class.getResourceAsStream(
        "/cz/wie/najtmar/gop/client/view/simple-client-view.properties"));
    return simpleClientViewPropertiesSet;
  }

  @Override
  public Properties getTestProperties() {
    return null;
  }

}
