package cz.wie.najtmar.gop.board;

import io.vavr.control.Try;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Optional;
import java.util.Properties;

import javax.annotation.Nonnull;

/**
 * Class that encapsulates and provides access to all Properties of a Board.
 * @author najtmar
 */
@EqualsAndHashCode
@ToString
public class BoardProperties {

  // Literals denoting Properties names.
  public static final String STEP_SIZE_PROPERTY_NAME = "Board.stepSize";
  public static final String BACKGROUND_IMAGE = "Board.backgroundImage";
  public static final String X_SIZE_PROPERTY_NAME = "Board.xsize";
  public static final String Y_SIZE_PROPERTY_NAME = "Board.ysize";
  public static final String LIB_GDX_PROPERTY_NAME = "Board.libGdx";

  /**
   * Internal representation of Board Properties.
   */
  private final Properties propertiesSet;

  /**
   * Length between two consecutive positions on the RoadSection.
   */
  private final double stepSize;

  /**
   * Path to the background image.
   */
  private final String backgroundImage;

  /**
   * X-size of the Board.
   */
  private final int xsize;

  /**
   * Y-size of the Board.
   */
  private final int ysize;

  /**
   * Whether the board uses libGdx coordinate system.
   */
  private final boolean libGdx;

  /**
   * Constructor.
   * @param propertiesSet Properties set to initialize the object from
   * @throws BoardException when BoardProperties cannot be created
   */
  public BoardProperties(@Nonnull Properties propertiesSet) throws BoardException {
    this.propertiesSet = propertiesSet;
    this.stepSize = getDoubleProperty(STEP_SIZE_PROPERTY_NAME);
    this.backgroundImage = getStringProperty(BACKGROUND_IMAGE);
    this.xsize = getIntegerProperty(X_SIZE_PROPERTY_NAME);
    this.ysize = getIntegerProperty(Y_SIZE_PROPERTY_NAME);
    this.libGdx = getBooleanProperty(LIB_GDX_PROPERTY_NAME);
  }

  /**
   * Parses and returns a double property.
   * @param key property
   * @return parsed double property
   * @throws BoardException if property reading or parsing fails
   */
  public double getDoubleProperty(String key) throws BoardException {
    try {
      return Double.parseDouble(propertiesSet.getProperty(key));
    } catch (NumberFormatException exception) {
      throw new BoardException("Double property " + key + " could not be read.", exception);
    }
  }

  /**
   * Parses and returns an integer property.
   * @param key property
   * @return parsed integer property
   * @throws BoardException if property reading or parsing fails
   */
  public int getIntegerProperty(String key) throws BoardException {
    try {
      return Integer.parseInt(propertiesSet.getProperty(key));
    } catch (NumberFormatException exception) {
      throw new BoardException("Integer property " + key + " could not be read.", exception);
    }
  }

  /**
   * Returns a String property.
   * @param key property
   * @return String property
   * @throws BoardException if property reading or parsing fails
   */
  public String getStringProperty(String key) throws BoardException {
    return propertiesSet.getProperty(key);
  }

  /**
   * Returns a Boolean property. false if not set.
   * @param key property
   * @return boolean property
   * @throws BoardException if property reading or parsing fails
   */
  public boolean getBooleanProperty(String key) throws BoardException {
    return Optional.ofNullable(propertiesSet.getProperty(key))
            .map(value -> Try.of(() -> Boolean.parseBoolean(value))
                    .getOrElseThrow(t -> new BoardException("Boolean property " + value + " could not be parsed.")))
            .orElse(false);
  }

  public Properties getPropertiesSet() {
    return propertiesSet;
  }

  public double getStepSize() {
    return stepSize;
  }

  public String getBackgroundImage() {
    return backgroundImage;
  }

  public int getXsize() {
    return xsize;
  }

  public int getYsize() {
    return ysize;
  }

  public boolean getLibGdx() {
    return libGdx;
  }

}
