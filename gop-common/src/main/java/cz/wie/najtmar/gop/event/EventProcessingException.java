package cz.wie.najtmar.gop.event;

/**
 * Thrown when a problem is detected with Event processing.
 * @author najtmar
 */
public class EventProcessingException extends Exception {

  private static final long serialVersionUID = 252525016084478222L;

  public EventProcessingException() {
    super();
  }

  public EventProcessingException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

  public EventProcessingException(String message, Throwable cause) {
    super(message, cause);
  }

  public EventProcessingException(String message) {
    super(message);
  }

  public EventProcessingException(Throwable cause) {
    super(cause);
  }

}
