package cz.wie.najtmar.gop.common;

import java.io.Serializable;

/**
 * Equivalent of java.awt.Point class, which can be used in the Android code.
 * @author najtmar
 *
 */
public class Point implements Serializable {

  /**
   * Generated serial version ID.
   */
  private static final long serialVersionUID = -8373783805793072733L;

  private final int xcoord;
  private final int ycoord;

  public Point(int xcoord, int ycoord) {
    this.xcoord = xcoord;
    this.ycoord = ycoord;
  }

  public int getX() {
    return xcoord;
  }

  public int getY() {
    return ycoord;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + xcoord;
    result = prime * result + ycoord;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Point)) {
      return false;
    }
    final Point otherObj = (Point) obj;
    return xcoord == otherObj.xcoord && ycoord == otherObj.ycoord;
  }

  @Override
  public String toString() {
    return "Point [xcoord=" + xcoord + ", ycoord=" + ycoord + "]";
  }

}
