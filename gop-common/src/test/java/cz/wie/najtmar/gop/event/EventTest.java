package cz.wie.najtmar.gop.event;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StringReader;

import javax.json.Json;

public class EventTest {

  @Before
  public void setUp() throws Exception {
  }

  @Test
  public void testSimpleEventCreated() throws Exception {
    final Event simpleEvent = new Event(Json.createObjectBuilder()
        .add("type", "TEST")
        .add("time", 123456)
        .add("key0", "value0")
        .add("key1", 12)
        .build());
    assertEquals(Event.Type.TEST, simpleEvent.getType());
    assertTrue(
        simpleEvent.getBody().equals(
            Json.createReader(new StringReader(
                "{"
              + "  \"type\": \"TEST\", "
              + "  \"time\": 123456, "
              + "  \"key0\": \"value0\", "
              + "  \"key1\": 12 "
              + "}"))
              .readObject()));

    assertTrue(!Event.Type.TEST.isInteractive());
    assertTrue(Event.Type.TEST.isEngine());
    assertTrue(!simpleEvent.isInterrupt());
  }

  @Test
  public void testInterruptCheckingWorks() throws Exception {
    assertTrue(
        (new Event(Json.createObjectBuilder()
            .add("type", "DESTROY_PRAWN")
            .add("time", 123456)
            .add("prawn", 123)
            .build())).isInterrupt());
    assertFalse(
        (new Event(Json.createObjectBuilder()
            .add("type", "DECREASE_PRAWN_ENERGY")
            .add("time", 123456)
            .add("prawn", 123)
            .add("delta", 0.25)
            .build())).isInterrupt());
    assertTrue(
        (new Event(Json.createObjectBuilder()
            .add("type", "MOVE_PRAWN")
            .add("time", 123456)
            .add("prawn", 123)
            .add("move", Json.createObjectBuilder()
                .add("position", Json.createObjectBuilder()
                    .add("roadSection", 0)
                    .add("index", 1)
                    .build())
                .add("moveTime", 123455)
                .add("stop", true)
                .build())
          .build())).isInterrupt());
    assertFalse(
        (new Event(Json.createObjectBuilder()
            .add("type", "MOVE_PRAWN")
            .add("time", 123456)
            .add("prawn", 123)
            .add("move", Json.createObjectBuilder()
                .add("position", Json.createObjectBuilder()
                    .add("roadSection", 0)
                    .add("index", 1)
                    .build())
                .add("moveTime", 123455)
                .add("stop", false)
                .build())
            .build())).isInterrupt());
  }

  @Test
  public void testEvenWithUnknownTypeFails() {
    try {
      new Event(Json.createObjectBuilder()
          .add("type", "UNKNOWN_TYPE")
          .add("time", 123456)
          .add("key0", "value0")
          .add("key1", 12)
          .build());
      fail("Should have thrown an exception.");
    } catch (EventProcessingException exception) {
      assertEquals("Field could not be read.", exception.getMessage());
    }
  }

  @Test
  public void testEvenWithNoTypeFails() {
    try {
      new Event(Json.createObjectBuilder()
          .add("time", 123456)
          .add("key0", "value0")
          .add("key1", 12)
          .build());
      fail("Should have thrown an exception.");
    } catch (EventProcessingException exception) {
      assertEquals("Required field not specified in {\"time\":123456,\"key0\":\"value0\",\"key1\":12}",
          exception.getMessage());
    }
  }

  @Test
  public void testSerializationWorks() throws Exception {
    final Event event = new Event(Json.createObjectBuilder()
        .add("type", "TEST")
        .add("time", 123456)
        .add("key0", "value0")
        .add("key1", 12)
        .build());
    final ByteArrayOutputStream output = new ByteArrayOutputStream();
    final ObjectOutputStream objectOutputStream = new ObjectOutputStream(output);
    objectOutputStream.writeObject(event);
    final ObjectInputStream input = new ObjectInputStream(new ByteArrayInputStream(output.toByteArray()));
    final Event readEvent = (Event) input.readObject();
    assertEquals(event, readEvent);
  }

}
