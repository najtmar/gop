package cz.wie.najtmar.gop.io;

import static org.junit.Assert.assertEquals;

import cz.wie.najtmar.gop.event.Event;

import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.json.Json;

public class MessageTest {

  @Before
  public void setUp() throws Exception {
  }

  @Test
  public void testSerializationWorks() throws Exception {
    final Message message = new Message(
        new Event[]{
            new Event(Json.createObjectBuilder()
                .add("type", "TEST")
                .add("time", 123456)
                .add("key0", "value0")
                .add("key1", 12)
                .build()),
            new Event(Json.createObjectBuilder()
                .add("type", "TEST")
                .add("time", 123457)
                .add("key2", "value2")
                .add("key3", 34)
                .build()),
            });
    final ByteArrayOutputStream output = new ByteArrayOutputStream();
    final ObjectOutputStream objectOutputStream = new ObjectOutputStream(output);
    objectOutputStream.writeObject(message);
    final ObjectInputStream input = new ObjectInputStream(new ByteArrayInputStream(output.toByteArray()));
    final Message readMessage = (Message) input.readObject();
    assertEquals(message, readMessage);
  }

}
