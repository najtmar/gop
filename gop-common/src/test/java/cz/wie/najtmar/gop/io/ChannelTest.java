package cz.wie.najtmar.gop.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import cz.wie.najtmar.gop.event.Event;

import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.LinkedBlockingQueue;

import javax.json.Json;

public class ChannelTest {

  static final int THREAD_COUNT = 10;
  LinkedBlockingQueue<Message> outgoingQueue;
  LinkedBlockingQueue<Message> ingoingQueue;
  Channel channel;
  volatile Exception[] threadExceptions;

  /**
   * Method setUp().
   * @throws Exception when something unexpected happens
   */
  @Before
  public void setUp() throws Exception {
    outgoingQueue = new LinkedBlockingQueue<>();
    ingoingQueue = new LinkedBlockingQueue<>();
    channel = new Channel(outgoingQueue, ingoingQueue);
    threadExceptions = new Exception[THREAD_COUNT];
  }

  @Test
  public void testMessageSuccessfullyTransferred() throws Exception {
    final Message message = new Message(
        new Event[]{
            new Event(Json.createObjectBuilder()
                .add("type", "TEST")
                .add("time", 123456)
                .add("key0", "value0")
                .add("key1", 12)
                .build()),
            new Event(Json.createObjectBuilder()
                .add("type", "TEST")
                .add("time", 123457)
                .add("key2", "value2")
                .add("key3", 34)
                .build()),
            });
    channel.writeMessage(message);
    final Message messageTransmitted = ingoingQueue.take();
    assertEquals(message, messageTransmitted);
    outgoingQueue.put(messageTransmitted);
    final Message messageRead = channel.readMessage();
    assertEquals(message, messageRead);
  }

  @Test
  public void testReadFirstSucceeds() throws Exception {
    final Message message = new Message(
        new Event[]{
            new Event(Json.createObjectBuilder()
                .add("type", "TEST")
                .add("time", 123456)
                .add("key0", "value0")
                .add("key1", 12)
                .build()),
            new Event(Json.createObjectBuilder()
                .add("type", "TEST")
                .add("time", 123457)
                .add("key2", "value2")
                .add("key3", 34)
                .build()),
            });
    final Thread[] threads = new Thread[THREAD_COUNT];
    for (int i = 0; i < threads.length; ++i) {
      final int index = i;
      threads[i] = (new Thread(new Runnable() {
        @Override
        public void run() {
          Message messageRead = null;
          try {
            messageRead = channel.readMessage();
          } catch (InterruptedException exception) {
            threadExceptions[index] = exception;
          }
          assertEquals(message, messageRead);
        }
      }));
      threads[i].start();
      outgoingQueue.put(message);
    }
    for (int i = 0; i < THREAD_COUNT; ++i) {
      threads[i].join();
      assertNull(threadExceptions[i]);
    }
  }

}
